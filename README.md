## 匯出
請先確認要匯出demo或正式站，請至lib/env.dart -> serverConfig裡確認修改項目
- android : 
  ```bash
  fvm flutter build appbundle --no-tree-shake-icons
  fvm flutter build apk --no-tree-shake-icons
  ```
- ios : 
  ```bash
  fvm flutter build ipa  --no-tree-shake-icons 
  open build/ios/archive/Runner.xcarchive
    ```
  
## environment
- flutter version 3.7.0
- xcode 15.2.0

## 測試帳號
- account : 8888888
- password : 0986525976
- UAT的測試帳號：3333333，密碼 0986525976

## version log
-v0.4.2
fix banner loading error
change banner UI
finish OTP
fix back stack bug
add release key
-v0.4.3
improve payment flow
change locale
skip if get banner error
change webView url
change app bar user name
if pv is empty show 0
login auto uppercase
add pv calculate
change blog style
add blog category
fix product's price and PV not align
change add to cart button text size
change checkout UI
-v0.4.4
change performance link
add link
change update user info layout
v0.5.3
qrcode now can refresh
change add address format
can add note when checkout
-v0.6.0
remove ios location request
change login error message
fix category bug
change calendar layout
remove location permission
add calendar background image
change calendar UI
add category name to blog item
fix calendar marker load error
change banner API
calendar can pull to refresh
can store checkout info
-v0.7.0
change contact page
fix calendar card size bug
change qr code UI
change qrcode and profile card UI
change order history listUI
import notify
change history order list UI
change history order detail UI
change shipping method UI
change add address UI
change bill UI
change payment method UI
can store qrcode
change icon
-v0.7.1
improve order history
change price format
add banner click
add celebration to calendar
webview back to events
add profile icon
add business stop
block other product category
finish wp_notify