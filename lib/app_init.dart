import 'dart:async';
import 'dart:io';

import 'package:app_tracking_transparency/app_tracking_transparency.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import 'common/config.dart';
import 'common/constants.dart';
import 'models/index.dart'
    show
        AppModel,
        CategoryModel,
        FilterAttributeModel,
        FilterTagModel,
        ListingLocationModel,
        NotificationModel,
        TagModel;
import 'models/user_model.dart';
import 'modules/dynamic_layout/config/app_config.dart';
import 'screens/base_screen.dart';
import 'services/dependency_injection.dart';
import 'services/index.dart';
import 'widgets/common/splash_screen.dart';

class AppInit extends StatefulWidget {
  const AppInit();

  @override
  _AppInitState createState() => _AppInitState();
}

class _AppInitState extends BaseScreen<AppInit> {
  bool isFirstSeen = false;
  bool isLoggedIn = false;
  bool hasLoadedData = false;
  bool hasLoadedSplash = false;

  late AppConfig? appConfig;

  /// check if the screen is already seen At the first time
  bool checkFirstSeen() {
    /// Ignore if OnBoardOnlyShowFirstTime is set to true.
    if (kAdvanceConfig['OnBoardOnlyShowFirstTime'] == false) {
      return false;
    }

    final _seen =
        injector<SharedPreferences>().getBool(LocalStorageKey.seen) ?? false;
    return _seen;
  }

  /// Check if the App is Login
  bool checkLogin() {
    final hasLogin =
        injector<SharedPreferences>().getBool(LocalStorageKey.loggedIn);
    return hasLogin ?? false;
  }

  Future<void> loadInitData() async {
    try {
      printLog('[AppState] Init Data 💫');
      isFirstSeen = checkFirstSeen();

      /// set the server config at first loading
      /// Load App model config
      if (Config().isBuilder) {
        Services().setAppConfig(serverConfig);
      }
      appConfig =
          await Provider.of<AppModel>(context, listen: false).loadAppConfig();

      await Provider.of<UserModel>(context, listen: false)
          .getUser(runLoaded: false);
       await Provider.of<UserModel>(context, listen: false)
          .refreshCookie();
      isLoggedIn = Provider.of<UserModel>(context, listen: false).user != null;

      Future.delayed(Duration.zero, () async {
        /// request app tracking to support io 14.5
        if (isIos) {
          requestAppTrackingTransparency();
        }

        /// Load more Category/Blog/Attribute Model beforehand
        final lang = Provider.of<AppModel>(context, listen: false).langCode;

        /// Request Categories
        Provider.of<CategoryModel>(context, listen: false).getCategories(
          lang: lang,
          sortingList: Provider.of<AppModel>(context, listen: false).categories,
          categoryLayout:
              Provider.of<AppModel>(context, listen: false).categoryLayout,
        );

        ///版本檢查
        var needUpdate = false;
        await Provider.of<AppModel>(context, listen: false)
            .checkVersion((value) {
          needUpdate = value;
        });
        if (needUpdate) {
          await showDialog(
            context: context,
            barrierDismissible: false,
            builder: (context) {
              return WillPopScope(
                  child: AlertDialog(
                    title: const Text('更新'),
                    content: const Text('新版本已釋出，請更新至最新版本。'),
                    actions: [
                      TextButton(
                        onPressed: () {
                          if (Platform.isIOS) {
                            launch(
                                'https://apps.apple.com/us/app/%E9%80%A3%E6%B3%95%E8%A1%8C%E5%8B%95go/id1606368083');
                          } else if (Platform.isAndroid) {
                            launch(
                                'https://play.google.com/store/apps/details?id=com.gcreate.lanfar');
                          }
                        },
                        child: const Text('更新'),
                      ),
                    ],
                  ),
                  onWillPop: () async {
                    return false;
                  });
            },
          );
        }
        hasLoadedData = true;
        if (hasLoadedSplash) {
          Provider.of<AppModel>(context, listen: false).setAfterSplash();
          goToNextScreen();
        }
      });

      /// Request more Async data which is not use on home screen
      Future.delayed(
        Duration.zero,
        () {
          Provider.of<TagModel>(context, listen: false).getTags();

          // Provider.of<ListBlogModel>(context, listen: false).getBlogs();

          Provider.of<FilterTagModel>(context, listen: false).getFilterTags();

          Provider.of<FilterAttributeModel>(context, listen: false)
              .getFilterAttributes();

          Provider.of<AppModel>(context, listen: false).loadCurrency();

          context.read<NotificationModel>().loadData();

          if (Config().isListingType) {
            Provider.of<ListingLocationModel>(context, listen: false)
                .getLocations();
          }

          /// init Facebook & Google Ads
          // Services()
          //     .advertisement
          //     .initAdvertise(context.read<AppModel>().advertisement);
        },
      );

      printLog('[AppState] InitData Finish');
    } catch (e, trace) {
      printLog(e.toString());
      printLog(trace.toString());
    }
  }

  void goToNextScreen() {
    // if (!isFirstSeen && !kIsWeb && appConfig != null) {
    //   if (onBoardingData.isNotEmpty) {
    //     Navigator.of(context).pushReplacementNamed(RouteList.onBoarding);
    //     return;
    //   }
    // }

    if (kLoginSetting['IsRequiredLogin'] && !isLoggedIn) {
      Navigator.of(context).pushReplacementNamed(RouteList.login);
      return;
    }

    Navigator.of(context).pushReplacementNamed(RouteList.dashboard);
  }

  void checkToShowNextScreen() {
    /// If the config was load complete then navigate to Dashboard
    hasLoadedSplash = true;
    if (hasLoadedData) {
      Provider.of<AppModel>(context, listen: false).setAfterSplash();
      goToNextScreen();
      return;
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> requestAppTrackingTransparency() async {
    try {
      await AppTrackingTransparency.requestTrackingAuthorization();

      /// AppTrackingTransparency.getAdvertisingIdentifier();
    } on PlatformException catch (e) {
      printLog('[App Tracking Transparency] Unexpected Platform Exception: $e');
    }
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    await loadInitData();
  }

  @override
  Widget build(BuildContext context) {
    var splashScreenType = kSplashScreen['type'];
    dynamic splashScreenImage = kSplashScreen['image'];
    var duration = kSplashScreen['duration'] ?? 2000;
    var logoSize = kSplashScreen['logoSize'];
    return SplashScreenIndex(
      imageUrl: splashScreenImage,
      splashScreenType: splashScreenType,
      actionDone: checkToShowNextScreen,
      duration: duration,
      logoSize: logoSize,
    );
  }
}
