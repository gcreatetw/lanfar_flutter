import 'package:flutter/material.dart';
import 'package:inspireui/inspireui.dart';
import 'package:provider/provider.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../models/index.dart';
import '../model/get_service_record_response.dart';
import '../provider/service_record_page_provider.dart';
import '../widget/round_border_textfield.dart';
import 'service_content_page.dart';

class ServiceRecordPage extends StatefulWidget {
  const ServiceRecordPage({Key? key}) : super(key: key);

  @override
  State<ServiceRecordPage> createState() => _ServiceRecordPageState();
}

class _ServiceRecordPageState extends State<ServiceRecordPage> {
  late ServiceRecordPageProvider serviceRecordPageProvider;

  @override
  void initState() {
    serviceRecordPageProvider = ServiceRecordPageProvider(
      user: context.read<UserModel>().user!,
      onError: () {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('發生錯誤，請稍後再試'),
          ),
        );
      },
    );
    serviceRecordPageProvider.fetch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: serviceRecordPageProvider,
      child: Consumer(
        builder: (context, ServiceRecordPageProvider provider, _) {
          return AutoHideKeyboard(
            child: Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                backgroundColor: Theme.of(context).backgroundColor,
                iconTheme: IconThemeData(
                    color: Theme.of(context).textTheme.headline6!.color),
                title: Text(
                  '客服紀錄',
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
              ),
              body: SafeArea(
                child: Column(
                  children: [
                    Row(
                      children: [
                        const SizedBox(width: 8),
                        Expanded(
                          child: RoundBorderTextField(
                            controller: provider.searchController,
                            hint: '請輸入關鍵字',
                            required: false,
                            textColor: Colors.black54,
                            enable: !provider.loading,
                          ),
                        ),
                        const SizedBox(width: 8),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: primaryColor,
                            elevation: 0,
                          ),
                          onPressed: provider.loading
                              ? null
                              : () {
                                  provider.fetch();
                                },
                          child: const Text(
                            '查詢',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                        const SizedBox(width: 8),
                      ],
                    ),
                    if (provider.loading)
                      Expanded(
                        child: Center(
                          child: kLoadingWidget(context),
                        ),
                      )
                    else if (provider.recordList.isNotEmpty)
                      Expanded(
                        child: ListView.separated(
                          padding: const EdgeInsets.all(8),
                          itemBuilder: (context, index) {
                            return _recordTitle(provider.recordList[index]);
                          },
                          separatorBuilder: (context, index) {
                            return const SizedBox(height: 8);
                          },
                          itemCount: provider.recordList.length,
                        ),
                      )
                    else
                      const Expanded(
                        child: Center(
                          child: Text(
                            '沒有紀錄',
                            style: TextStyle(
                              color: Colors.black54,
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _recordTitle(ServiceRecordBean model) {
    var statusColor = Colors.black54;
    switch (model.consultStatus) {
      case '待處理':
        statusColor = Colors.redAccent;
        break;
      case '處理中':
        statusColor = Colors.green;
        break;
    }
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => ServiceContentPage(
              model: model,
            ),
          ),
        );
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black26),
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
        ),
        child: Column(
          children: [
            const SizedBox(height: 8),
            Row(
              children: [
                const SizedBox(width: 16),
                Text(
                  model.consultType ?? '',
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: primaryColor,
                  ),
                ),
                const Spacer(),
                Container(
                  // padding: const EdgeInsets.all(4),
                  // decoration: BoxDecoration(
                  //   borderRadius: BorderRadius.circular(5),
                  //   border: Border.all(color: statusColor),
                  // ),
                  child: Text(
                    model.consultStatus ?? '',
                    style: TextStyle(
                      color: statusColor,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const SizedBox(width: 8),
              ],
            ),
            const SizedBox(height: 8),
            Row(
              children: [
                const SizedBox(width: 16),
                Expanded(
                  child: Text(
                    model.consultText ?? '',
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                      color: Colors.black54,
                      fontSize: 18,
                    ),
                  ),
                ),
                const SizedBox(width: 16),
              ],
            ),
            const Divider(),
            Row(
              children: [
                const SizedBox(width: 16),
                Expanded(
                  child: Text(
                    model.dateCreated ?? '',
                    style: const TextStyle(
                      color: Colors.black54,
                      fontSize: 14,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 8),
          ],
        ),
      ),
    );
  }
}
