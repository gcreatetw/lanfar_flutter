import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:inspireui/widgets/skeleton_widget/skeleton_widget.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../common/theme/colors.dart';
import '../../widgets/common/refresh_scroll_physics.dart';
import '../calendar_package/src/customization/calendar_builders.dart';
import '../calendar_package/src/customization/calendar_style.dart';
import '../calendar_package/src/customization/days_of_week_style.dart';
import '../calendar_package/src/customization/header_style.dart';
import '../calendar_package/src/shared/utils.dart';
import '../calendar_package/src/table_calendar.dart';
import '../calendar_package/table_calendar.dart';
import '../contants/constants.dart';
import '../model/calendar_event.dart';
import '../model/general_state.dart';
import '../widget/calendar_card.dart';

class CalendarPage extends StatefulWidget {
  const CalendarPage({Key? key}) : super(key: key);

  @override
  _CalendarPageState createState() => _CalendarPageState();
}

class _CalendarPageState extends State<CalendarPage> {
  CalendarFormat _calendarFormat = CalendarFormat.month;
  DateTime _selectedDay = DateTime.now();
  DateTime _focusedDay = DateTime.now();

  int currentIndex = 0;

  PageController pageController = PageController();

  bool haveEvent = false;

  List<CalendarEvent> eventList = [];

  var list = <Event>[];

  DateTime? currentDate;

  int currentCat = 0;

  RefreshController controller = RefreshController();

  ScrollController scrollController = ScrollController();

  int navigateIndex = 0;

  var keyList = <GlobalKey>[];

  bool shouldReset = true;

  bool subscribeLoading = false;

  Future<void> refresh() async {
    shouldReset = true;
    await Provider.of<CalendarEvent>(context, listen: false).fetch(
      [
        DateFormat('yyyyMM').format(DateTime(_focusedDay.year, _focusedDay.month))
      ],
      refresh: true,
    );
    if (currentIndex == 0) {
      await _checkHaveEvent(_selectedDay);
    }
    if (currentIndex != 0) {
      await resetCalendarList();
    }
    controller.refreshCompleted();
  }

  @override
  void initState() {
    _checkHaveEvent(DateTime.now());
    context.read<CalendarEvent>().getSubscription(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width - 48;
    return Consumer(builder: (context, CalendarEvent calendarEvent, _) {
      return Scaffold(
        backgroundColor: Theme.of(context).canvasColor,
        appBar: AppBar(
          backgroundColor: Theme.of(context).backgroundColor,
          iconTheme: IconThemeData(
              color: Theme.of(context).textTheme.headline6!.color),
          title: Text(
            '行事月曆',
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
        ),
        body: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Stack(
              alignment: Alignment.bottomCenter,
              children: [
                Container(
                  color: Colors.white,
                  width: double.maxFinite,
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        for (int i = 0;
                            i < Constants.calenderLocations.length;
                            i++)
                          TextButton(
                            onPressed: () {
                              shouldReset = true;
                              setState(() {
                                currentIndex = i;
                                pageController.jumpToPage(
                                  i,
                                );
                                pageController.animateToPage(
                                  i,
                                  duration: const Duration(milliseconds: 200),
                                  curve: Curves.ease,
                                );
                              });
                              if (currentIndex != 0) {
                                resetCalendarList();
                              }
                              if (currentIndex == 0) {
                                _checkHaveEvent(_selectedDay);
                              }
                            },
                            child: Text(
                              Constants.calenderLocations[i],
                              style: TextStyle(
                                  fontSize: 20,
                                  color: currentIndex == i
                                      ? Theme.of(context).primaryColor
                                      : Theme.of(context)
                                          .textTheme
                                          .subtitle2!
                                          .color),
                            ),
                          )
                      ],
                    ),
                  ),

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      const SizedBox(height: 3),
                      Container(
                        height: 1,
                        width: double.maxFinite,
                        color: Colors.black.withOpacity(0.0),
                      ),
                      AnimatedPositioned(
                        left: width / 5 * currentIndex,
                        duration: const Duration(milliseconds: 200),
                        child: Container(
                          width: width / 5,
                          height: 4.0,
                          color: primaryColor,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Expanded(
              child: Stack(
                children: [
                  Positioned.fill(
                    child: Wrap(
                      children: [
                        for (int i = 0; i < 3; i++)
                          Image.asset(
                            'assets/images/Bg1.png',
                            fit: BoxFit.fitWidth,
                          ),
                      ],
                    ),
                  ),
                  // Align(
                  //   alignment: Alignment.bottomCenter,
                  //   child: Container(
                  //     height:
                  //     MediaQuery.of(context).size.height / 3 * 2,
                  //     decoration: BoxDecoration(
                  //       borderRadius: const BorderRadius.only(
                  //         topRight: Radius.circular(25),
                  //         topLeft: Radius.circular(25),
                  //       ),
                  //     ),
                  //   ),
                  // ),
                  PageView.builder(
                    controller: pageController,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: Constants.calenderLocations.length,
                    itemBuilder: (context, index) {
                      if (index == 0) {
                        return _calender();
                      }
                      return CustomScrollView(
                        physics: const RefreshScrollPhysics(),
                        // controller: scrollController,
                        slivers: [
                          SliverPersistentHeader(
                            delegate: SubscribePersistent(
                              onTap: subscribeLoading
                                  ? null
                                  : () async {
                                      setState(() {
                                        subscribeLoading = true;
                                      });
                                      shouldReset = false;
                                      await calendarEvent.subscribe(
                                          context,
                                          Constants
                                              .calenderLocations[currentIndex]);
                                      setState(() {
                                        subscribeLoading = false;
                                      });
                                    },
                              calendarEvent: calendarEvent,
                              currentIndex: currentIndex,
                              loading: subscribeLoading,
                            ),
                            floating: true,
                            pinned: true,
                          ),
                          // CupertinoSliverRefreshControl(
                          //   onRefresh: refresh,
                          // ),
                          SliverList(
                            delegate: SliverChildBuilderDelegate(
                              (context, index) {
                                if (calendarEvent.state ==
                                    GeneralState.loading) {
                                  return Column(
                                    children: [
                                      const SizedBox(height: 8),
                                      for (int i = 0; i < 3; i++) ...[
                                        Container(
                                          color: Colors.white,
                                          margin: const EdgeInsets.symmetric(
                                              horizontal: 16),
                                          height: 120,
                                          child: Row(
                                            children: [
                                              const SizedBox(width: 16),
                                              const Flexible(
                                                flex: 1,
                                                child: Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 16),
                                                  child: Skeleton(
                                                    height: double.maxFinite,
                                                    width: double.maxFinite,
                                                  ),
                                                ),
                                              ),
                                              const SizedBox(width: 16),
                                              Flexible(
                                                flex: 4,
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    const SizedBox(height: 16),
                                                    Expanded(
                                                      child: Skeleton(
                                                        height:
                                                            double.maxFinite,
                                                        width: 50.0 +
                                                            Random()
                                                                .nextInt(70),
                                                      ),
                                                    ),
                                                    const SizedBox(height: 8),
                                                    Expanded(
                                                        child: Skeleton(
                                                      height: double.maxFinite,
                                                      width: 100.0 +
                                                          Random().nextInt(100),
                                                    )),
                                                    const SizedBox(height: 8),
                                                    Expanded(
                                                      child: Skeleton(
                                                        height:
                                                            double.maxFinite,
                                                        width: 100.0 +
                                                            Random()
                                                                .nextInt(100),
                                                      ),
                                                    ),
                                                    const SizedBox(height: 16),
                                                  ],
                                                ),
                                              ),
                                              const SizedBox(width: 8),
                                            ],
                                          ),
                                        ),
                                        const SizedBox(height: 8),
                                      ]
                                    ],
                                  );
                                }
                                if (calendarEvent.categoryList.isEmpty) {
                                  return const SizedBox();
                                }
                                return SafeArea(
                                  bottom: false,
                                  child: ListView.builder(
                                    shrinkWrap: true,
                                    controller: scrollController,
                                    // physics:
                                    //     const NeverScrollableScrollPhysics(),
                                    itemCount: calendarEvent
                                        .categoryList[currentIndex].length,
                                    itemBuilder: (context, index) {
                                      if (index == navigateIndex &&
                                          shouldReset) {
                                        Future.delayed(
                                            const Duration(milliseconds: 100),
                                            () {
                                          Scrollable.ensureVisible(
                                              keyList[navigateIndex]
                                                  .currentContext!);
                                        });
                                      }

                                      return Padding(
                                        key: keyList[index],
                                        padding: EdgeInsets.only(
                                            top: index == 0 ? 16 : 4,
                                            bottom: index ==
                                                    calendarEvent
                                                            .categoryList[
                                                                currentIndex]
                                                            .length -
                                                        1
                                                ? 16
                                                : 4,
                                            left: 16,
                                            right: 16),
                                        child: CalendarCard(
                                            key: UniqueKey(),
                                            data: calendarEvent
                                                    .categoryList[currentIndex]
                                                [index]),
                                      );
                                    },
                                  ),
                                );
                              },
                              childCount: 1,
                            ),
                          ),
                        ],
                      );
                    },
                  )
                ],
              ),
            ),
          ],
        ),
      );
    });
  }

  Widget _calender() {
    var calendarEvent = Provider.of<CalendarEvent>(context);
    return CustomScrollView(physics: const RefreshScrollPhysics(), slivers: [
      CupertinoSliverRefreshControl(
        onRefresh: refresh,
      ),
      SliverList(
        delegate: SliverChildBuilderDelegate(
          (context, index) {
            if (calendarEvent.state == GeneralState.loading) {
              return Column(
                children: [
                  Container(
                    color: Colors.white,
                    width: double.maxFinite,
                    height: MediaQuery.of(context).size.width,
                    child: Column(
                      children: const [
                        SizedBox(height: 16),
                        Skeleton(
                          width: 100,
                          height: 40,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 16, vertical: 16),
                          child: Skeleton(
                            width: double.maxFinite,
                            height: 30,
                          ),
                        ),
                        Expanded(
                            child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 16),
                          child: Skeleton(
                            width: double.maxFinite,
                          ),
                        )),
                        SizedBox(height: 16),
                      ],
                    ),
                  ),
                  const SizedBox(height: 16),
                  Container(
                    color: Colors.white,
                    margin: const EdgeInsets.symmetric(horizontal: 16),
                    height: 120,
                    child: Row(
                      children: [
                        const SizedBox(width: 16),
                        const Flexible(
                          flex: 1,
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 16),
                            child: Skeleton(
                              height: double.maxFinite,
                              width: double.maxFinite,
                            ),
                          ),
                        ),
                        const SizedBox(width: 16),
                        Flexible(
                          flex: 4,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(height: 16),
                              Expanded(
                                child: Skeleton(
                                  height: double.maxFinite,
                                  width: 50.0 + Random().nextInt(70),
                                ),
                              ),
                              const SizedBox(height: 8),
                              Expanded(
                                  child: Skeleton(
                                height: double.maxFinite,
                                width: 100.0 + Random().nextInt(100),
                              )),
                              const SizedBox(height: 8),
                              Expanded(
                                child: Skeleton(
                                  height: double.maxFinite,
                                  width: 100.0 + Random().nextInt(100),
                                ),
                              ),
                              const SizedBox(height: 16),
                            ],
                          ),
                        ),
                        const SizedBox(width: 8),
                      ],
                    ),
                  ),
                  const SizedBox(height: 8),
                ],
              );
            }
            return SingleChildScrollView(
              physics: const NeverScrollableScrollPhysics(),
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 1,
                          blurRadius: 5,
                          offset:
                              const Offset(0, 2), // changes position of shadow
                        ),
                      ],
                    ),
                    child: TableCalendar(
                      locale: 'zh_TW',
                      availableGestures: AvailableGestures.horizontalSwipe,
                      rowHeight: 50,
                      startingDayOfWeek: StartingDayOfWeek.monday,
                      headerStyle: HeaderStyle(
                          titleTextFormatter: (date, locale) =>
                              DateFormat('yyyy年 M月 ', 'zh_CN').format(date),
                          headerPadding:
                              const EdgeInsets.symmetric(vertical: 12),
                          decoration: const BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage('assets/images/Bg2.png'),
                                  fit: BoxFit.fitWidth)),
                          titleTextStyle: TextStyle(
                              fontSize: 21,
                              color:
                                  Theme.of(context).textTheme.subtitle2!.color),
                          formatButtonVisible: false,
                          titleCentered: true),
                      daysOfWeekHeight: 40,
                      daysOfWeekStyle: DaysOfWeekStyle(
                          decoration: const BoxDecoration(),
                          weekdayStyle: TextStyle(
                              fontSize: 18,
                              color:
                                  Theme.of(context).textTheme.subtitle2!.color),
                          weekendStyle: TextStyle(
                              fontSize: 18,
                              color: Theme.of(context)
                                  .textTheme
                                  .subtitle2!
                                  .color)),
                      calendarStyle: CalendarStyle(
                        canMarkersOverflow: true,
                        markersMaxCount: 4,
                        selectedDecoration: const BoxDecoration(
                            color: Colors.transparent, shape: BoxShape.circle),
                        todayDecoration: const BoxDecoration(
                            color: Colors.transparent, shape: BoxShape.circle),
                        defaultTextStyle: TextStyle(
                            fontSize: 21,
                            color:
                                Theme.of(context).textTheme.subtitle2!.color),
                        holidayTextStyle: TextStyle(
                            fontSize: 21,
                            color:
                                Theme.of(context).textTheme.subtitle2!.color),
                        todayTextStyle:
                            const TextStyle(fontSize: 21, color: null),
                        selectedTextStyle:
                            const TextStyle(fontSize: 21, color: primaryColor),
                        outsideTextStyle:
                            const TextStyle(fontSize: 21, color: Colors.grey),
                        weekendTextStyle: TextStyle(
                            fontSize: 21,
                            color:
                                Theme.of(context).textTheme.subtitle2!.color),
                      ),
                      firstDay: DateTime.utc(DateTime.now().year - 10, 10, 16),
                      lastDay: DateTime.utc(DateTime.now().year + 10, 3, 14),
                      focusedDay: _focusedDay,
                      calendarFormat: _calendarFormat,
                      selectedDayPredicate: (day) {
                        return isSameDay(_selectedDay, day);
                      },
                      onDaySelected: (selectedDay, focusedDay) {
                        setState(() {
                          _selectedDay = selectedDay;
                          _focusedDay = focusedDay;
                        });
                        _checkHaveEvent(selectedDay);
                      },
                      onFormatChanged: (format) {
                        if (_calendarFormat != format) {
                          setState(() {
                            _calendarFormat = format;
                          });
                        }
                      },
                      onPageChanged: (focusedDay) async {
                        _focusedDay = focusedDay;
                        await Provider.of<CalendarEvent>(context, listen: false).fetch(
                          [
                            DateFormat('yyyyMM').format(DateTime(_focusedDay.year, _focusedDay.month))
                          ],
                        );
                      },
                      eventLoader: _getEventsForDay,
                      calendarBuilders: CalendarBuilders(
                        defaultBuilder: (context, date, _) {
                          return AnimatedContainer(
                            duration: const Duration(milliseconds: 250),
                            margin: const CalendarStyle().cellMargin,
                            padding: const CalendarStyle().cellPadding,
                            // decoration: BoxDecoration(
                            //     color: list.isNotEmpty
                            //         ? Colors.black.withOpacity(0.1)
                            //         : Colors.white,
                            //     shape: BoxShape.circle),
                            alignment: const CalendarStyle().cellAlignment,
                            child: Text(date.day.toString(),
                                style: const CalendarStyle()
                                    .defaultTextStyle
                                    .copyWith(
                                      fontSize: 21,
                                    )),
                          );
                        },
                        singleMarkerBuilder: (context, date, _) {
                          var listDay = <String>[];
                          Color color = Colors.grey;
                          for (var element in calendarEvent.eventList) {
                            if (element.startDate!.year == date.year &&
                                element.startDate!.month == date.month &&
                                element.startDate!.day == date.day) {
                              listDay.add(element.category == null
                                  ? ''
                                  : element.category!.name ?? '');
                            }
                          }

                          var totalCat = <int>[];
                          for (var i = 0; i < listDay.length; i++) {
                            switch (listDay[i]) {
                              case '桃園':
                                if (totalCat.contains(1)) {
                                  break;
                                }
                                totalCat.add(1);
                                break;
                              case '台中':
                                if (totalCat.contains(2)) {
                                  break;
                                }
                                totalCat.add(2);
                                break;
                              case '台南':
                                if (totalCat.contains(3)) {
                                  break;
                                }
                                totalCat.add(3);
                                break;
                              case '高雄':
                                if (totalCat.contains(4)) {
                                  break;
                                }
                                totalCat.add(4);
                                break;
                              case '節日':
                                color = Colors.redAccent;
                                break;
                              default:
                                if (totalCat.contains(0)) {
                                  break;
                                }
                                totalCat.add(0);
                                break;
                            }
                          }

                          // for (var i = 0;
                          //     i < Constants.calenderLocations.length;
                          //     i++) {
                          //   if (listDay.contains(Constants.calenderLocations[i])) {
                          //     totalCat.add(i);
                          //   }
                          // }
                          // if (totalCat.length < listDay.length) {
                          //   for (var i = 0;
                          //       i < listDay.length - totalCat.length;
                          //       i++) {
                          //     totalCat.add(0);
                          //   }
                          // }

                          if (totalCat.isNotEmpty) {
                            ///fix calendar marker load error
                            if (currentDate == date) {
                              if (currentCat >= totalCat.length) {
                                currentCat = 0;
                              }
                              color = Color(Constants
                                  .calendarColors[totalCat[currentCat]]);
                              currentCat++;
                            } else {
                              currentCat = 0;
                              currentDate = date;
                              color = Color(Constants
                                  .calendarColors[totalCat[currentCat]]);
                              currentCat++;
                            }
                          }

                          return Column(
                            children: [
                              const SizedBox(height: 0),
                              Container(
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: color,
                                ),
                                //Change color
                                width: 8.0,
                                height: 8.0,
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 1.5),
                              )
                            ],
                          );
                        },
                      ),
                    ),
                  ),
                  const SizedBox(height: 12),
                  if (calendarEvent.categoryList.isNotEmpty &&
                      calendarEvent.categoryList[0].isNotEmpty)
                    ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: calendarEvent.categoryList[0].length,
                        itemBuilder: (context, index) {
                          print(index);
                          return Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 16, vertical: 4),
                            child: CalendarCard(
                                key: UniqueKey(),
                                data: calendarEvent.categoryList[0][index]),
                          );
                        }),
                  const SizedBox(height: 50),
                ],
              ),
            );
          },
          childCount: 1,
        ),
      ),
    ]);
  }

  List<Event> _getEventsForDay(DateTime day) {
    var calendarEvent = Provider.of<CalendarEvent>(context);
    list.clear();
    var listCheck = <String>[];
    for (var element in calendarEvent.eventList) {
      if (element.startDate!.year == day.year &&
          element.startDate!.month == day.month &&
          element.startDate!.day == day.day) {
        if (!listCheck.contains(
            element.category == null ? '' : element.category!.name ?? '')) {
          listCheck.add(
              element.category == null ? '' : element.category!.name ?? '');
          list.add(Event(date: day, title: element.category!.name));
        }
      }
    }
    return list;
  }

  Future<void> _checkHaveEvent(DateTime day) async {
    var calendarEvent = Provider.of<CalendarEvent>(context, listen: false);
    if (calendarEvent.categoryList.isNotEmpty) {
      calendarEvent.categoryList[currentIndex] = [];
      for (var element in calendarEvent.eventList) {
        if (element.startDate!.year == day.year &&
            element.startDate!.month == day.month &&
            element.startDate!.day == day.day) {
          calendarEvent.categoryList[currentIndex].add(element);
        }
      }
    }
  }

  Future<void> resetCalendarList() async {
    var calendarEvent = Provider.of<CalendarEvent>(context, listen: false);
    final today = DateTime.now();
    var keepGoing = true;
    if (calendarEvent.categoryList.isNotEmpty) {
      calendarEvent.categoryList[currentIndex].clear();
      keyList.clear();
      for (var element in calendarEvent.eventList) {
        if (element.category!.name ==
                Constants.calenderLocations[currentIndex] ||
            element.category!.name == '節日' ||
            element.category!.name == '全部') {
          calendarEvent.categoryList[currentIndex].add(element);
          keyList.add(GlobalKey());
          if (element.startDate!.year == today.year &&
              element.startDate!.month == today.month &&
              element.startDate!.day == today.day) {
            navigateIndex = calendarEvent.categoryList[currentIndex].length - 1;
            keepGoing = false;
          }
          if (element.startDate!.isAfter(today) && keepGoing) {
            navigateIndex = calendarEvent.categoryList[currentIndex].length - 1;
            keepGoing = false;
          }
          if (keepGoing) {
            navigateIndex = calendarEvent.categoryList[currentIndex].length - 1;
          }
        }
      }
    }
  }
}

class SubscribePersistent extends SliverPersistentHeaderDelegate {
  final CalendarEvent calendarEvent;
  final int currentIndex;
  final Function()? onTap;
  final bool loading;

  SubscribePersistent({
    required this.calendarEvent,
    required this.currentIndex,
    required this.onTap,
    required this.loading,
  });

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    // TODO: implement build
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 8,
      ),
      child: OutlinedButton(
        onPressed: onTap,
        style: OutlinedButton.styleFrom(
          backgroundColor: calendarEvent.subscribeMap[
                      Constants.calenderLocations[currentIndex]] ==
                  1
              ? primaryColor
              : null,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
          ),
        ),
        child: loading
            ? Padding(
                padding: const EdgeInsets.all(8),
                child: Center(
                  child: CircularProgressIndicator(
                    color: calendarEvent.subscribeMap[
                                Constants.calenderLocations[currentIndex]] !=
                            1
                        ? null
                        : Colors.white,
                  ),
                ),
              )
            : Text(
                calendarEvent.subscribeMap[
                            Constants.calenderLocations[currentIndex]] !=
                        1
                    ? '訂閱'
                    : '已訂閱',
                style: TextStyle(
                    color: calendarEvent.subscribeMap[
                                Constants.calenderLocations[currentIndex]] !=
                            1
                        ? null
                        : Colors.white),
              ),
      ),
    );
  }

  @override
  // TODO: implement maxExtent
  double get maxExtent => 60;

  @override
  // TODO: implement minExtent
  double get minExtent => 60;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) =>
      true;
}
