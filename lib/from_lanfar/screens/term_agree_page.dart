import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:http/http.dart' as http;
import 'package:lanfar/common/theme/colors.dart';
import 'package:lanfar/widgets/html/index.dart' as i;
import 'package:webview_flutter/webview_flutter.dart';

import '../../common/config.dart';
import '../../env.dart';

enum TermsType {
  user,
  privacy,
}

extension TermsTypeEx on TermsType {
  String get name {
    switch (this) {
      case TermsType.user:
        return 'terms';
      case TermsType.privacy:
        return 'privacy';
    }
  }
}

class TermAgreePage extends StatefulWidget {
  final TermsType type;
  final Function() check;

  const TermAgreePage({
    Key? key,
    required this.type,
    required this.check,
  }) : super(key: key);

  @override
  State<TermAgreePage> createState() => _TermAgreePageState();
}

class _TermAgreePageState extends State<TermAgreePage> {
  var html = '';
  int index = 0;
  String error = '';

  Future<void> getHtml() async {
    try {
      final uri = Uri.parse(
          '${environment['serverConfig']['url']}/wp-json/site/get_page_content');
      var request = http.Request(
        'GET',
        uri,
      );
      request.body = convert.jsonEncode({
        'type': widget.type.name,
      });

      request.headers.addAll({'Content-Type': 'application/json'});

      final response = await request.send();
      var bodyRaw = await response.stream.bytesToString();
      // var body = convert.jsonDecode(bodyRaw);
      // print(body);
      if (response.statusCode == 200) {
        setState(() {
          html = bodyRaw.toString();
        });
        return;
      }
      setState(() {
        index = 2;
        error = response.statusCode.toString();
      });
    } catch (err) {
      debugPrint('發生錯誤: $err');
      setState(() {
        index = 2;
        error = err.toString();
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getHtml();
  }

  late WebViewController controller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
      ),
      body: IndexedStack(
        index: index,
        children: [
          kLoadingWidget(context),
          SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: HtmlWidget(
                    html,
                    onLoadingBuilder: (context, e, __) {
                      return const SizedBox();
                    },
                    factoryBuilder: () => i.MyWidgetFactory(),
                    customStylesBuilder: (element) {
                      if (element.classes.contains('elementor-heading-title')) {
                        return {'color': '#49A2DE'};
                      }

                      return null;
                    },
                    customWidgetBuilder: (element) {
                      setState(() {
                        index = 1;
                      });
                      if (element.classes.contains('elementor elementor-6277')) {
                        return SizedBox();
                      }
                      if (element.classes.contains('elementor-heading-title')) {
                        return Text(
                          element.text,
                          style: const TextStyle(
                            color: Color(0xff49A2DE),
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          ),
                        );
                      }
                      if (element.classes
                          .contains('elementor-icon-list-items')) {
                        if (element.children[0].children.indexWhere((e) => e
                                .classes
                                .contains('elementor-icon-list-text')) !=
                            -1) {
                          return Row(
                            children: [
                              Image.asset(
                                'assets/icons/from_lanfar/baseline_arrow_circle_right_black_24dp.png',
                                color: const Color(0xff49A2DE),
                                width: 24,
                              ),
                              const SizedBox(width: 8),
                              Expanded(
                                  child: Text(
                                element.children[0].children
                                    .firstWhere((e) => e.classes
                                        .contains('elementor-icon-list-text'))
                                    .text,
                                style: const TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                ),
                              )),
                            ],
                          );
                        }
                      }
                      return null;
                    },
                  ),
                ),
                const SizedBox(height: 16,),
                Row(children: [
                  Expanded(
                    child: ButtonTheme(
                      height: 45,
                      child: ElevatedButton(
                        style: ButtonStyle(
                            padding: MaterialStateProperty.all<EdgeInsets>(
                                const EdgeInsets.all(16)),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.zero),
                            ),
                            backgroundColor:
                                MaterialStateProperty.all(primaryColor)),
                        onPressed: () {
                          widget.check.call();
                          Navigator.pop(context);
                        },
                        child: Padding(
                          padding: EdgeInsets.only(
                              bottom: MediaQuery.of(context).padding.bottom),
                          child: const Text(
                            '我同意',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                ]),
              ],
            ),
          ),
          Center(
            child: Text('發生錯誤 : $error'),
          )
        ],
      ),
    );
  }
}
