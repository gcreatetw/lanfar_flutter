import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:inspireui/widgets/auto_hide_keyboard.dart';
import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../models/index.dart';
import '../../widgets/html/index.dart';
import '../provider/marketing_page_provider.dart';
import '../widget/round_border_textfield.dart';
import 'marketing_group_page.dart';

class MarketingPage extends StatefulWidget {
  const MarketingPage({Key? key}) : super(key: key);

  @override
  State<MarketingPage> createState() => _MarketingPageState();
}

class _MarketingPageState extends State<MarketingPage> {
  late MarketingPageProvider marketingPageProvider;

  @override
  void initState() {
    marketingPageProvider = MarketingPageProvider(
      user: context.read<UserModel>().user!,
      onError: (e) {
        showSnack(e);
      },
    );
    marketingPageProvider.fetch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: marketingPageProvider,
      child: Consumer<MarketingPageProvider>(
        builder: (context, provider, __) {
          print(provider.description);
          return AutoHideKeyboard(
            child: Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                backgroundColor: Theme.of(context).colorScheme.background,
                iconTheme: IconThemeData(
                  color: Theme.of(context).textTheme.titleLarge!.color,
                ),
                title: Text(
                  '我的賣場',
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
              ),
              body: provider.loading
                  ? kLoadingWidget(context)
                  : provider.errorMsg != null
                      ? Container(
                          alignment: Alignment.center,
                          padding: const EdgeInsets.all(16),
                          child: Text(provider.errorMsg ?? ''),
                        )
                      : Stack(
                          children: [
                            SafeArea(
                              child: ListView(
                                padding: const EdgeInsets.all(8),
                                children: [
                                  // HtmlWidget(provider.description ?? '',launchImage: true,),
                                  Html(
                                    data: provider.description,
                                    onLinkTap: (url, c, _, __) {
                                      if (url != null) {
                                        launch(
                                          url,
                                          forceSafariVC: false,
                                        );
                                      }
                                    },
                                    // onImageTap: (url, c, _, __) {
                                    //   print(11111);
                                    //   if (url != null) {
                                    //     launch(
                                    //       url,
                                    //       forceSafariVC: false,
                                    //     );
                                    //   }
                                    // },
                                  ),
                                  // Image.asset(
                                  //     'assets/icons/from_lanfar/team.jpeg'),
                                  // Column(
                                  //   children: const [
                                  //     SizedBox(height: 16),
                                  //     Text(
                                  //       '你的連法商店專屬連結',
                                  //       style: TextStyle(
                                  //           color: primaryColor, fontSize: 18),
                                  //     ),
                                  //     Text(
                                  //       '分享網址跟自訂訊息',
                                  //       style: TextStyle(
                                  //           color: primaryColor, fontSize: 18),
                                  //     ),
                                  //     Text(
                                  //       '邀請大家從你的專屬商場網址下訂單',
                                  //       style: TextStyle(
                                  //           color: primaryColor, fontSize: 18),
                                  //     ),
                                  //     SizedBox(height: 16),
                                  //   ],
                                  // ),
                                  _messageCard(provider),
                                  _productCard(provider),
                                ],
                              ),
                            ),
                            if (provider.updating)
                              Container(
                                height: double.maxFinite,
                                width: double.maxFinite,
                                color: Colors.black54,
                                child: Center(
                                  child: kLoadingWidget(context),
                                ),
                              )
                          ],
                        ),
            ),
          );
        },
      ),
    );
  }

  Widget _messageCard(MarketingPageProvider provider) {
    return Card(
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              '自訂邀請訊息',
              style: TextStyle(
                color: primaryColor,
              ),
            ),
            const SizedBox(height: 8),
            RoundBorderTextField(
              controller: provider.inviteMessage,
              hint: '',
              required: false,
              radius: 10,
            ),
            Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(elevation: 0),
                    onPressed: () {
                      provider.update(
                        'text',
                        onSuccess: () {
                          showSnack('更新成功');
                        },
                      );
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(
                          Icons.save,
                          color: Colors.white,
                          size: 18,
                        ),
                        SizedBox(width: 4),
                        Text(
                          '儲存',
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                  ),
                ),
                const SizedBox(width: 8),
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(elevation: 0),
                    onPressed: () {
                      // provider.inviteMessage.text =
                      //     '您好！我是${provider.user.name},這是我的賣場！';
                      provider.update(
                        'rest',
                        onSuccess: () {
                          showSnack('更新成功');
                        },
                      );
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(
                          Icons.repeat_sharp,
                          color: Colors.white,
                          size: 18,
                        ),
                        SizedBox(width: 4),
                        Text(
                          '重置',
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 16),
            const Text(
              '我的賣場網址',
              style: TextStyle(
                color: primaryColor,
              ),
            ),
            const SizedBox(height: 8),
            RoundBorderTextField(
              controller: provider.linkController,
              hint: '',
              enable: false,
              required: false,
              radius: 10,
            ),
            // SelectableText(provider.link),
            Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(elevation: 0),
                    onPressed: () {
                      launch(
                        provider.link,
                        forceSafariVC: false,
                      );
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(
                          Icons.visibility,
                          color: Colors.white,
                          size: 18,
                        ),
                        SizedBox(width: 4),
                        Text(
                          '預覽',
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                  ),
                ),
                const SizedBox(width: 8),
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(elevation: 0),
                    onPressed: () {
                      Share.share(
                          '${provider.inviteMessage.text}\n${provider.link}');
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Transform(
                          transform: Matrix4.rotationY(pi),
                          alignment: Alignment.center,
                          child: const Icon(
                            Icons.share,
                            color: Colors.white,
                            size: 18,
                          ),
                        ),
                        const SizedBox(width: 4),
                        const Text(
                          '分享',
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 16),
            Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black12, width: 0.5),
              ),
              padding: const EdgeInsets.all(8),
              child: Column(
                children: const [
                  Text(
                    '※點擊「分享」後，系統將會自動分享邀請訊息與邀請連結，接著您就可以將此訊息分享給您的好友。',
                    style: TextStyle(color: Color(0xff009582)),
                  ),
                  SizedBox(height: 8),
                  Text(
                    '※自訂邀請訊息可以透過儲存來讓下次再使用。',
                    style: TextStyle(color: Color(0xff009582)),
                  ),
                  SizedBox(height: 8),
                  Text(
                    '※點擊重置後，系統將會重置自訂的邀請訊息為系統預設值。',
                    style: TextStyle(color: Color(0xff009582)),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _productCard(MarketingPageProvider provider) {
    return Card(
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              '預設邀請商品',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 8),
            Table(
              columnWidths: const {
                0: IntrinsicColumnWidth(),
                1: FlexColumnWidth(),
                2: IntrinsicColumnWidth(),
              },
              defaultVerticalAlignment: TableCellVerticalAlignment.middle,
              children: [
                const TableRow(
                  decoration: BoxDecoration(
                      // borderRadius: BorderRadius.only(
                      //   topLeft: Radius.circular(10),
                      //   topRight: Radius.circular(10),
                      // ),
                      border: Border(
                          bottom: BorderSide(color: primaryColor, width: 2))),
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                      child: Text(
                        '群組',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: primaryColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                      child: Text(
                        '分享的商品',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: primaryColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                      child: Text(
                        '動作',
                        // textAlign: TextAlign.center,
                        style: TextStyle(
                          color: primaryColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    )
                  ],
                ),
                for (int i = 0; i < provider.groups.length; i++)
                  TableRow(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 8),
                        child: Text(
                          (i + 1).toString(),
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            color: Colors.black54,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () async {
                          var result = await Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => MarketingGroupPage(
                                group: i + 1,
                                provider: provider,
                              ),
                            ),
                          );
                          if (result != null) {
                            provider.groups[i] = result;
                            await provider.update(
                              'product',
                              onSuccess: () {
                                showSnack('更新成功');
                              },
                            );
                          }
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.grey,
                              width: 0.5,
                            ),
                            color: Colors.white,
                          ),
                          padding: const EdgeInsets.all(8),
                          margin: const EdgeInsets.symmetric(vertical: 4),
                          child: provider.groups[i].isEmpty
                              ? const SizedBox(height: 20)
                              : Column(
                                  children: provider.groups[i]
                                      .map(
                                        (e) => _productTitle(
                                          e.name +
                                              (e.sku.isNotEmpty
                                                  ? '(${e.sku})'
                                                  : ''),
                                          () {
                                            provider.removeProduct(i, e.id);
                                          },
                                        ),
                                      )
                                      .toList(),
                                ),
                        ),
                      ),
                      // PopupMenuButton<String>(
                      //   child: Container(
                      //     decoration: BoxDecoration(
                      //         border: Border.all(color: Colors.grey),
                      //         color: Colors.white),
                      //     padding: const EdgeInsets.all(8),
                      //     margin: const EdgeInsets.symmetric(vertical: 4),
                      //     child: provider.groups[i].isEmpty
                      //         ? const SizedBox(height: 20)
                      //         : Column(
                      //       children: provider.groups[i]
                      //           .map(
                      //             (e) =>
                      //             _productTitle(
                      //               e.name +
                      //                   (e.sku.isNotEmpty
                      //                       ? '(${e.sku})'
                      //                       : ''),
                      //                   () {
                      //                 provider.removeProduct(i, e.id);
                      //               },
                      //             ),
                      //       )
                      //           .toList(),
                      //     ),
                      //   ),
                      //   onSelected: (value) {
                      //     provider.addProduct(i, value);
                      //   },
                      //   itemBuilder: (context) {
                      //     return provider.products
                      //         .map(
                      //           (e) =>
                      //           PopupMenuItem<String>(
                      //             value: e.id ?? '',
                      //             padding: EdgeInsets.zero,
                      //             child: Container(
                      //               padding: const EdgeInsets.all(16),
                      //               color: provider.groups[i].indexWhere(
                      //                       (element) =>
                      //                   element.id == e.id) !=
                      //                   -1
                      //                   ? Colors.grey.shade300
                      //                   : Colors.white,
                      //               child: Row(
                      //                 mainAxisAlignment:
                      //                 MainAxisAlignment.center,
                      //                 children: [
                      //                   Expanded(
                      //                     child: Text((e.name ?? '') +
                      //                         (e.sku != null &&
                      //                             e.sku!.isNotEmpty
                      //                             ? '(${e.sku})'
                      //                             : '')),
                      //                   ),
                      //                 ],
                      //               ),
                      //             ),
                      //           ),
                      //     )
                      //         .toList();
                      //   },
                      // ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 4),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(elevation: 0),
                          onPressed: () {
                            Share.share(
                                '${provider.inviteMessage.text}\n${provider.link}&pids=${provider.groups[i].map((e) => e.id).toList().join(',')}');
                          },
                          child: const Text(
                            '分享',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      )
                    ],
                  ),
              ],
            ),
            const Divider(),
            // ElevatedButton(
            //   style: ElevatedButton.styleFrom(elevation: 0),
            //   onPressed: () {
            //     provider.update(
            //       'product',
            //       onSuccess: () {
            //         showSnack('更新成功');
            //       },
            //     );
            //   },
            //   child: const Text(
            //     '更新',
            //     style: TextStyle(color: Colors.white),
            //   ),
            // ),
            const SizedBox(height: 8),
            Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black12, width: 0.5),
              ),
              padding: const EdgeInsets.all(8),
              child: Column(
                children: const [
                  Text(
                    '※您可以先設定好要分享的商品於上述群組中，設定完畢後請點擊「更新」按鈕。',
                    style: TextStyle(color: Color(0xff009582)),
                  ),
                  SizedBox(height: 8),
                  Text(
                    '※在設定好分享商品的後面找到「分享」按鈕，系統會自動將該群組中的商品附在分享連結中。',
                    style: TextStyle(color: Color(0xff009582)),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void showSnack(String content) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(content),
      ),
    );
  }

  Widget _productTitle(String name, Function() onRemove) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 2),
      padding: const EdgeInsets.all(4),
      decoration: BoxDecoration(
        border: Border.all(color: const Color(0xffdddddd)),
        borderRadius: BorderRadius.circular(5),
        color: const Color(0xfff5f5f5),
      ),
      child: Row(
        children: [
          GestureDetector(
            onTap: onRemove,
            child: const Icon(Icons.clear),
          ),
          Expanded(
            child: Text(name),
          ),
        ],
      ),
    );
  }
}
