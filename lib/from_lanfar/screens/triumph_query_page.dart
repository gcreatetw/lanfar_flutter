import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/src/provider.dart';

import '../../common/config.dart';
import '../../common/theme/colors.dart';
import '../../common/tools/price_tools.dart';
import '../../models/user_model.dart';
import '../model/get_triumph_query_response.dart';
import '../provider/triumph_query_page_provider.dart';

class TriumphQueryPage extends StatefulWidget {
  const TriumphQueryPage({Key? key}) : super(key: key);

  @override
  State<TriumphQueryPage> createState() => _TriumphQueryPageState();
}

class _TriumphQueryPageState extends State<TriumphQueryPage> {
  TriumphQueryPageProvider triumphQueryPageProvider =
      TriumphQueryPageProvider();

  @override
  void initState() {
    triumphQueryPageProvider.fetch(
      userId: context.read<UserModel>().user?.username ?? '',
      onError: (e) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(e),
            duration: const Duration(days: 1),
          ),
        );
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: triumphQueryPageProvider,
      child: Consumer(
        builder: (context, TriumphQueryPageProvider provider, _) {
          return WillPopScope(
            onWillPop: () async {
              ScaffoldMessenger.of(context).removeCurrentSnackBar();
              return true;
            },
            child: Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                backgroundColor: Theme.of(context).colorScheme.background,
                iconTheme: IconThemeData(
                  color: Theme.of(context).textTheme.titleLarge!.color,
                ),
                title: Text(
                  '業績試算',
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
              ),
              body: provider.loading
                  ? kLoadingWidget(context)
                  : ListView(
                      padding: const EdgeInsets.all(8),
                      children: [
                        topTitle(
                          title: '當月職級',
                          action: Text(
                              provider.getTriumphQueryResponse?.data?.grade ??
                                  ''),
                        ),
                        const Divider(),
                        topTitle(
                          title: '個人業績',
                          action: Text(PriceTools.getCurrencyFormatted(
                                (provider.getTriumphQueryResponse?.data?.pv2)
                                    .toString(),
                                null,
                                isPV: true,
                              ) ??
                              ''),
                        ),
                        const Divider(),
                        topTitle(
                          title: '整組業績',
                          action: Text(PriceTools.getCurrencyFormatted(
                                (provider.getTriumphQueryResponse?.data?.rPv)
                                    .toString(),
                                null,
                                isPV: true,
                              ) ??
                              ''),
                        ),
                        const Divider(),
                        topTitle(
                          title: '緊縮前個人業績',
                          action: Text(PriceTools.getCurrencyFormatted(
                                (provider.getTriumphQueryResponse?.data?.srPv)
                                    .toString(),
                                null,
                                isPV: true,
                              ) ??
                              ''),
                        ),
                        const Divider(),
                        topTitle(
                          title: '緊縮後個人業績',
                          action: Text(PriceTools.getCurrencyFormatted(
                                (provider.getTriumphQueryResponse?.data?.ssrPv)
                                    .toString(),
                                null,
                                isPV: true,
                              ) ??
                              ''),
                        ),
                        const Divider(),
                        const SizedBox(height: 16),
                        Text(
                          '試算時間${provider.getTriumphQueryResponse?.data?.runtime}',
                          style: const TextStyle(
                            color: Colors.red,
                            fontSize: 14,
                          ),
                        ),
                        const SizedBox(height: 8),
                        table(
                            provider.getTriumphQueryResponse?.data?.data ?? []),
                        const SizedBox(height: 50),
                      ],
                    ),
            ),
          );
        },
      ),
    );
  }

  Widget table(List<DataTriumph> dataTriumph) {
    return Table(
      children: [
        const TableRow(
          decoration: BoxDecoration(
            color: primaryColor,
          ),
          children: [
            Padding(
              padding: EdgeInsets.all(4),
              child: Text(
                '下線',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(4),
              child: Text(
                '姓名',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(4),
              child: Text(
                '當月',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(4),
              child: Text(
                '合格',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
        for (int i = 0; i < dataTriumph.length; i++)
          TableRow(
            decoration: BoxDecoration(
              color: i % 2 == 1 ? Colors.black12 : Colors.white,
            ),
            children: [
              Padding(
                padding: const EdgeInsets.all(4),
                child: Text(
                  dataTriumph[i].id.toString(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4),
                child: Text(
                  dataTriumph[i].name.toString(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4),
                child: Text(
                  dataTriumph[i].newGrade.toString(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4),
                child: Text(
                  dataTriumph[i].type.toString(),
                ),
              ),
            ],
          ),
      ],
    );
  }

  Widget topTitle({required String title, required Widget action}) {
    return IntrinsicHeight(
      child: Row(
        children: [
          Container(
            height: 30,
            width: 8,
            color: primaryColor,
          ),
          const SizedBox(width: 16),
          Expanded(
            child: Text(
              title,
              style: const TextStyle(fontSize: 18),
            ),
          ),
          const SizedBox(width: 16),
          action,
        ],
      ),
    );
  }
}
