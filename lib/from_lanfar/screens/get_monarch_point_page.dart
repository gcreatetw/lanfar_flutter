import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../models/user_model.dart';
import '../model/get_monarch_point_response.dart';
import '../provider/get_monarch_point_page_provider.dart';

class GetMonarchPointPage extends StatefulWidget {
  const GetMonarchPointPage({Key? key}) : super(key: key);

  @override
  State<GetMonarchPointPage> createState() => _GetMonarchPointPageState();
}

class _GetMonarchPointPageState extends State<GetMonarchPointPage> {
  GetMonarchPointPageProvider getMonarchPointPageProvider =
      GetMonarchPointPageProvider();

  @override
  void initState() {
    getMonarchPointPageProvider.fetch(
      userId: context.read<UserModel>().user?.username ?? '',
      onError: (e) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(e),
            duration: const Duration(days: 1),
          ),
        );
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: getMonarchPointPageProvider,
      child: Consumer(
        builder: (context, GetMonarchPointPageProvider provider, _) {
          return WillPopScope(
            onWillPop: () async {
              ScaffoldMessenger.of(context).removeCurrentSnackBar();
              return true;
            },
            child: Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                backgroundColor: Theme.of(context).colorScheme.background,
                iconTheme: IconThemeData(
                  color: Theme.of(context).textTheme.titleLarge!.color,
                ),
                title: Text(
                  '年度龍虎榜',
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
              ),
              body: provider.loading
                  ? kLoadingWidget(context)
                  : ListView(
                      padding: const EdgeInsets.all(8),
                      children: [
                        Row(
                          children: [
                            Container(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 16),
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey),
                              ),
                              child: DropdownButton<String>(
                                value: provider.currentYear,
                                underline: const SizedBox(),
                                dropdownColor: Colors.white,
                                onChanged: provider.bodyLoading
                                    ? null
                                    : (value) {
                                        provider.setDate(
                                          value ?? '',
                                          userId: context
                                                  .read<UserModel>()
                                                  .user
                                                  ?.username ??
                                              '',
                                          onError: (e) {
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(
                                              SnackBar(
                                                content: Text(e),
                                              ),
                                            );
                                          },
                                        );
                                      },
                                items: [
                                  for (var element in provider.yearList)
                                    DropdownMenuItem(
                                      value: element,
                                      child: Text(element),
                                    )
                                ],
                              ),
                            ),
                            const SizedBox(width: 16),
                            const Text('總點數：'),
                            if (!getMonarchPointPageProvider.bodyLoading)
                              Text(
                                '${getMonarchPointPageProvider.getMonarchPointResponse?.data?.sumpoint ?? 0}',
                                style: const TextStyle(color: primaryColor),
                              ),
                          ],
                        ),
                        if (provider.bodyLoading) ...[
                          SizedBox(
                            height:
                                MediaQuery.of(context).size.height / 2 - 150,
                          ),
                          kLoadingWidget(context),
                        ] else ...[
                          const SizedBox(height: 16),
                          if (provider.getMonarchPointResponse != null &&
                              provider.getMonarchPointResponse!.data != null)
                            table(
                                provider.getMonarchPointResponse!.data!.data ??
                                    []),
                        ],
                      ],
                    ),
            ),
          );
        },
      ),
    );
  }

  Widget table(List<DataDetail> list) {
    return Table(
      children: [
        const TableRow(
          decoration: BoxDecoration(
            color: primaryColor,
          ),
          children: [
            Padding(
              padding: EdgeInsets.all(4),
              child: Text(
                '月份',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(4),
              child: Text(
                '合格職級',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(4),
              child: Text(
                '龍虎榜點數',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
        for (int i = 0; i < list.length; i++)
          TableRow(
            decoration: BoxDecoration(
              color: i % 2 == 1 ? const Color(0xffecf6fc) : Colors.white,
            ),
            children: [
              Padding(
                padding: const EdgeInsets.all(4),
                child: Text(
                  list[i].mm.toString(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4),
                child: Text(
                  list[i].gradetitle.toString(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4),
                child: Text(
                  '${list[i].point ?? 0}',
                ),
              ),
            ],
          ),
      ],
    );
  }
}
