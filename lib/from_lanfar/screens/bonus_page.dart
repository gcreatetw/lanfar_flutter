import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools/price_tools.dart';
import '../../models/user_model.dart';
import '../model/get_bonus_detail_response.dart';
import '../provider/bonus_page_provider.dart';

class BonusPage extends StatefulWidget {
  const BonusPage({Key? key}) : super(key: key);

  @override
  State<BonusPage> createState() => _BonusPageState();
}

class _BonusPageState extends State<BonusPage> {
  BonusPageProvider bonusPageProvider = BonusPageProvider();
  ExpandableController expandableController = ExpandableController();
  bool expanded = false;

  @override
  void initState() {
    bonusPageProvider.fetch(
      context.read<UserModel>().user?.username ?? '',
      onError: (e) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(e),
            duration: const Duration(days: 1),
          ),
        );
      },
    );
    expandableController.expanded = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: bonusPageProvider,
      child: Consumer(
        builder: (context, BonusPageProvider provider, _) {
          return WillPopScope(
            onWillPop: () async {
              ScaffoldMessenger.of(context).removeCurrentSnackBar();
              return true;
            },
            child: Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                backgroundColor: Theme.of(context).colorScheme.background,
                iconTheme: IconThemeData(
                  color: Theme.of(context).textTheme.titleLarge!.color,
                ),
                title: Text(
                  '獎金資訊',
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
              ),
              body: provider.loading
                  ? kLoadingWidget(context)
                  : ListView(
                      padding: const EdgeInsets.all(8),
                      children: [
                        monthPicker(),
                        // TopTitle(
                        //   title: 'pv月份',
                        //   action: DropdownButton<String>(
                        //     value: provider.chooseDate,
                        //     underline: const SizedBox(),
                        //     dropdownColor: Colors.white,
                        //     onChanged: (value) {
                        //       provider.setDate(
                        //         value ?? '',
                        //         userId:
                        //             context.read<UserModel>().user?.username ??
                        //                 '',
                        //         onError: (e) {
                        //           ScaffoldMessenger.of(context).showSnackBar(
                        //             SnackBar(
                        //               content: Text(e),
                        //             ),
                        //           );
                        //         },
                        //       );
                        //     },
                        //     items: [
                        //       for (var element
                        //           in provider.getBonusDateResponse!.data ?? [])
                        //         DropdownMenuItem(
                        //           value: element,
                        //           child: Text(element),
                        //         )
                        //     ],
                        //   ),
                        // ),
                        if (provider.bodyLoading) ...[
                          SizedBox(
                            height:
                                MediaQuery.of(context).size.height / 2 - 150,
                          ),
                          kLoadingWidget(context),
                        ] else ...[
                          const SizedBox(height: 32),
                          personalInfo(),
                          const SizedBox(height: 32),
                          // const Divider(),
                          // TopTitle(
                          //   title: '上個月個人業績',
                          //   action: Text(PriceTools.getCurrencyFormatted(
                          //           (provider.getBonusDetailResponse!.data
                          //                       ?.personalPerformance ??
                          //                   '0')
                          //               .toString(),
                          //           null,
                          //           isPV: true) ??
                          //       ''),
                          // ),
                          // const Divider(),
                          // TopTitle(
                          //   title: '上個月整組業績',
                          //   action: Text(PriceTools.getCurrencyFormatted(
                          //           (provider.getBonusDetailResponse!.data
                          //                       ?.organizationalPerformance ??
                          //                   '0')
                          //               .toString(),
                          //           null,
                          //           isPV: true) ??
                          //       ''),
                          // ),
                          // const SizedBox(height: 32),
                          Row(
                            children: [
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  elevation: 0,
                                ),
                                onPressed: () {
                                  setState(
                                    () {
                                      if (expanded) {
                                        expanded = !expanded;
                                        organizationController.expanded = false;
                                        leadController.expanded = false;
                                        divideController.expanded = false;
                                        competitionController.expanded = false;
                                        travelController.expanded = false;
                                        totalController.expanded = false;
                                      } else {
                                        expanded = !expanded;
                                        organizationController.expanded = true;
                                        leadController.expanded = true;
                                        divideController.expanded = true;
                                        competitionController.expanded = true;
                                        travelController.expanded = true;
                                        totalController.expanded = true;
                                      }
                                    },
                                  );
                                },
                                child: Text(
                                  expanded ? '收合' : '展開',
                                  style: const TextStyle(color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 20),
                          body(),
                          // const Text(
                          //   '組織獎金',
                          //   style: TextStyle(
                          //     fontSize: 24,
                          //     fontWeight: FontWeight.bold,
                          //   ),
                          // ),
                          // const Divider(
                          //   color: primaryColor,
                          //   thickness: 2,
                          // ),
                          // organization(provider
                          //     .getBonusDetailResponse!.data!.userItems!),
                          // if (provider.getBonusDetailResponse!.data!.leadItems!
                          //     .isNotEmpty) ...[
                          //   const SizedBox(height: 32),
                          //   const Text(
                          //     '領導獎金',
                          //     style: TextStyle(
                          //       fontSize: 24,
                          //       fontWeight: FontWeight.bold,
                          //     ),
                          //   ),
                          //   const Divider(
                          //     color: primaryColor,
                          //     thickness: 2,
                          //   ),
                          //   lead(provider
                          //           .getBonusDetailResponse!.data?.leadItems ??
                          //       []),
                          //   const SizedBox(height: 8),
                          //   Text(
                          //     '*每一積分金額：${provider.getBonusDetailResponse?.data?.leadItems![0].leadershipPoints}',
                          //     style: const TextStyle(
                          //       color: Color(0xFFe90909),
                          //       fontSize: 14,
                          //     ),
                          //   )
                          // ],
                          // if (provider.getBonusDetailResponse!.data!.dividend!
                          //     .amountDueMonth!.isNotEmpty) ...[
                          //   const SizedBox(height: 32),
                          //   const Text(
                          //     '分紅獎金',
                          //     style: TextStyle(
                          //       fontSize: 24,
                          //       fontWeight: FontWeight.bold,
                          //     ),
                          //   ),
                          //   const Divider(
                          //     color: primaryColor,
                          //     thickness: 2,
                          //   ),
                          //   divide(provider
                          //       .getBonusDetailResponse!.data!.dividend!),
                          // ],
                          // if (provider
                          //     .getBonusDetailResponse!.data!.competitionBonus!
                          //     .toString()
                          //     .isNotEmpty) ...[
                          //   const SizedBox(height: 32),
                          //   const Text(
                          //     '競賽獎金',
                          //     style: TextStyle(
                          //       fontSize: 24,
                          //       fontWeight: FontWeight.bold,
                          //     ),
                          //   ),
                          //   const Divider(
                          //     color: primaryColor,
                          //     thickness: 2,
                          //   ),
                          //   competition(provider
                          //       .getBonusDetailResponse!.data!.competitionBonus
                          //       .toString()),
                          // ],
                          // if (provider.getBonusDetailResponse!.data!
                          //     .travelBonus!.amountQuarter
                          //     .toString()
                          //     .isNotEmpty) ...[
                          //   const SizedBox(height: 32),
                          //   const Text(
                          //     '旅遊獎金',
                          //     style: TextStyle(
                          //       fontSize: 24,
                          //       fontWeight: FontWeight.bold,
                          //     ),
                          //   ),
                          //   const Divider(
                          //     color: primaryColor,
                          //     thickness: 2,
                          //   ),
                          //   travel(provider
                          //       .getBonusDetailResponse!.data!.travelBonus!),
                          // ],
                          // if (provider.getBonusDetailResponse!.data!.payTotal!
                          //         .titles !=
                          //     null) ...[
                          //   const SizedBox(height: 32),
                          //   const Text(
                          //     '支付總額',
                          //     style: TextStyle(
                          //       fontSize: 24,
                          //       fontWeight: FontWeight.bold,
                          //     ),
                          //   ),
                          //   const Divider(
                          //     color: primaryColor,
                          //     thickness: 2,
                          //   ),
                          //   total(provider
                          //       .getBonusDetailResponse!.data!.payTotal!),
                          //   const Text(
                          //     '**本月支付您的金額如上，請確認。（如有錯誤，次月更正）**',
                          //     style: TextStyle(
                          //       color: Color(0xFFe90909),
                          //       fontSize: 14,
                          //     ),
                          //   ),
                          //   Text(
                          //     '<<您的佣金已存入--${provider.getBonusDetailResponse?.data?.accno}--帳戶>>',
                          //     style: const TextStyle(
                          //       color: Color(0xFFe90909),
                          //       fontSize: 14,
                          //     ),
                          //   )
                          // ],
                          const SizedBox(height: 50),
                        ],
                      ],
                    ),
            ),
          );
        },
      ),
    );
  }

  Widget monthPicker() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text(
          'PV月份',
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 8),
        SizedBox(
          height: 40,
          child: ListView.separated(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              var date = bonusPageProvider.getBonusDateResponse!.data![index];
              var year = date.substring(0, 4);
              var monthString = date.substring(4, 6);

              return InkWell(
                onTap: () {
                  organizationController.expanded = false;
                  leadController.expanded = false;
                  divideController.expanded = false;
                  competitionController.expanded = false;
                  travelController.expanded = false;
                  totalController.expanded = false;
                  expanded = false;

                  bonusPageProvider.setDate(
                    date,
                    userId: context.read<UserModel>().user?.username ?? '',
                    onError: (e) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text(e),
                        ),
                      );
                    },
                  );
                },
                customBorder: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                  side: const BorderSide(color: primaryColor),
                ),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: primaryColor),
                    color: bonusPageProvider.chooseDate != date
                        ? Colors.white
                        : primaryColor,
                  ),
                  padding: const EdgeInsets.symmetric(
                    vertical: 8,
                    horizontal: 16,
                  ),
                  child: Center(
                    child: Text(
                      '$year/${int.parse(monthString)}月',
                      style: TextStyle(
                        color: bonusPageProvider.chooseDate == date
                            ? Colors.white
                            : primaryColor,
                      ),
                    ),
                  ),
                ),
              );
            },
            separatorBuilder: (context, index) {
              return const SizedBox(width: 4);
            },
            itemCount: bonusPageProvider.getBonusDateResponse!.data!.length,
          ),
        ),
      ],
    );
  }

  Widget personalInfo() {
    var currentYear = int.parse(bonusPageProvider.chooseDate.substring(0, 4));
    var currentMonth = bonusPageProvider.chooseDate.substring(4, 6);
    return Column(
      children: [
        Text(
          '${currentYear - 1911}年${int.parse(currentMonth)}月獎金支付明細表',
          style: const TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 16),
        IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  children: [
                    const Text('本月職級'),
                    Expanded(
                      child: Center(
                        child: Text(
                          bonusPageProvider.getBonusDetailResponse?.data
                                  ?.userItems?.grade ??
                              '',
                          style: const TextStyle(
                            color: primaryColor,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  children: [
                    const Text('會員編號'),
                    const SizedBox(height: 8),
                    Text(
                      bonusPageProvider
                              .getBonusDetailResponse?.data?.userItems?.id ??
                          '',
                      style: const TextStyle(
                        color: primaryColor,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      bonusPageProvider
                              .getBonusDetailResponse?.data?.userItems?.name ??
                          '',
                      style: const TextStyle(
                        color: primaryColor,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  children: [
                    const Text('本月業績'),
                    Expanded(
                      child: Center(
                        child: Text(
                          PriceTools.getCurrencyFormatted(
                                (bonusPageProvider
                                        .getBonusDetailResponse?.data?.monthPV)
                                    .toString(),
                                null,
                                isPV: true,
                              ) ??
                              '',
                          style: const TextStyle(
                            color: primaryColor,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget body() {
    var currentMonth = bonusPageProvider.chooseDate.substring(4, 6);
    var totalPrize = '';
    try {
      totalPrize = bonusPageProvider
          .getBonusDetailResponse!.data!.payTotal!.values!.first
          .toString();
    } catch (_) {}
    return Container(
      padding: const EdgeInsets.only(
        left: 8,
        right: 8,
        bottom: 8,
      ),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(5),
      ),
      child: ExpandablePanel(
        controller: expandableController,
        theme: const ExpandableThemeData(
          headerAlignment: ExpandablePanelHeaderAlignment.center,
        ),
        header: Text(
          '${int.parse(currentMonth)}月佣金總額 : ${PriceTools.getCurrencyFormatted(
                totalPrize,
                null,
                isPV: true,
              ) ?? ''}',
          style: const TextStyle(
            color: primaryColor,
            fontWeight: FontWeight.bold,
          ),
        ),
        collapsed: const SizedBox(),
        expanded: Column(
          children: [
            ///組織獎金
            if (bonusPageProvider.getBonusDetailResponse?.data?.userItems !=
                null)
              organization(
                  bonusPageProvider.getBonusDetailResponse!.data!.userItems!),

            ///領導獎金
            if (bonusPageProvider.getBonusDetailResponse?.data?.leadItems !=
                    null &&
                bonusPageProvider
                    .getBonusDetailResponse!.data!.leadItems!.isNotEmpty) ...[
              const SizedBox(height: 8),
              lead(bonusPageProvider.getBonusDetailResponse!.data?.leadItems ??
                  [])
            ],

            ///分紅獎金
            if (bonusPageProvider.getBonusDetailResponse?.data?.dividend
                        ?.amountDueMonth !=
                    null &&
                bonusPageProvider.getBonusDetailResponse!.data!.dividend!
                    .amountDueMonth!.isNotEmpty) ...[
              const SizedBox(height: 8),
              divide(bonusPageProvider.getBonusDetailResponse!.data!.dividend!),
            ],

            ///競賽獎金
            if (bonusPageProvider
                        .getBonusDetailResponse?.data?.competitionBonus !=
                    null &&
                bonusPageProvider
                    .getBonusDetailResponse!.data!.competitionBonus!
                    .toString()
                    .isNotEmpty) ...[
              const SizedBox(height: 8),
              competition(bonusPageProvider
                  .getBonusDetailResponse!.data!.competitionBonus
                  .toString()),
            ],

            ///旅遊獎金
            if (bonusPageProvider.getBonusDetailResponse?.data?.travelBonus
                        ?.amountQuarter !=
                    null &&
                bonusPageProvider
                    .getBonusDetailResponse!.data!.travelBonus!.amountQuarter
                    .toString()
                    .isNotEmpty) ...[
              const SizedBox(height: 8),
              travel(
                  bonusPageProvider.getBonusDetailResponse!.data!.travelBonus!),
            ],

            ///支付總額
            if (bonusPageProvider
                    .getBonusDetailResponse?.data?.payTotal?.titles !=
                null) ...[
              const SizedBox(height: 8),
              total(bonusPageProvider.getBonusDetailResponse!.data!.payTotal!),
            ],
          ],
        ),
      ),
    );
  }

  ExpandableController organizationController = ExpandableController();

  Widget organization(UserItems userItems) {
    return Container(
      padding: const EdgeInsets.only(
        left: 4,
        right: 4,
        bottom: 4,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(color: Colors.grey),
      ),
      child: ExpandablePanel(
        key: UniqueKey(),
        controller: organizationController,
        theme: const ExpandableThemeData(
          headerAlignment: ExpandablePanelHeaderAlignment.center,
        ),
        header: Text(
          '組織獎金 : ${PriceTools.getCurrencyFormatted(
                userItems.pay9,
                null,
                isPV: true,
              ) ?? ''}',
        ),
        collapsed: const SizedBox(),
        expanded: Table(
          children: [
            TableRow(
              decoration: const BoxDecoration(color: primaryColor),
              children: [
                Container(
                  padding: const EdgeInsets.all(4),
                  child: const Text(
                    '個人CPV',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(4),
                  child: const Text(
                    '差額獎金',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(4),
                  child: const Text(
                    '組織獎金',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                Padding(
                  padding: const EdgeInsets.all(4),
                  child: Text(
                    PriceTools.getCurrencyFormatted(
                          userItems.personalCPV.toString(),
                          null,
                          isPV: true,
                        ) ??
                        '',
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(4),
                  child: Text(
                    PriceTools.getCurrencyFormatted(
                          userItems.pay2.toString(),
                          null,
                          isPV: true,
                        ) ??
                        '',
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(4),
                  child: Text(
                    PriceTools.getCurrencyFormatted(
                          userItems.pay9.toString(),
                          null,
                          isPV: true,
                        ) ??
                        '',
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
    // return Column(
    //   children: [
    //     organizationTitle(
    //       '會員編號',
    //       userItems.id ?? '',
    //       true,
    //     ),
    //     const SizedBox(height: 1),
    //     organizationTitle(
    //       '會員姓名',
    //       userItems.name ?? '',
    //       false,
    //     ),
    //     const SizedBox(height: 1),
    //     organizationTitle(
    //       '累積業績',
    //       PriceTools.getCurrencyFormatted(
    //           (userItems.cumulativePerformance).toString(), null,
    //           isPV: true) ??
    //           '',
    //       true,
    //     ),
    //     const SizedBox(height: 1),
    //     organizationTitle(
    //       '上月累積',
    //       PriceTools.getCurrencyFormatted(
    //           (userItems.cumulativeLastMonth).toString(), null,
    //           isPV: true) ??
    //           '',
    //       false,
    //     ),
    //     const SizedBox(height: 1),
    //     organizationTitle(
    //       '本月CPV',
    //       PriceTools.getCurrencyFormatted(
    //           (userItems.currentMonthCpv).toString(), null,
    //           isPV: true) ??
    //           '',
    //       true,
    //     ),
    //     const SizedBox(height: 1),
    //     organizationTitle(
    //       '個人CPV',
    //       PriceTools.getCurrencyFormatted(
    //           (userItems.personalCPV).toString(), null,
    //           isPV: true) ??
    //           '',
    //       false,
    //     ),
    //     const SizedBox(height: 1),
    //     organizationTitle(
    //       '差額獎金',
    //       PriceTools.getCurrencyFormatted((userItems.pay2).toString(), null,
    //           isPV: true) ??
    //           '',
    //       true,
    //     ),
    //     const SizedBox(height: 1),
    //     organizationTitle(
    //       '個人組織獎金',
    //       PriceTools.getCurrencyFormatted((userItems.pay9).toString(), null,
    //           isPV: true) ??
    //           '',
    //       false,
    //     ),
    //   ],
    // );
  }

  Widget organizationTitle(String title, String value, bool single,
      {double? width}) {
    return IntrinsicHeight(
      child: Row(
        children: [
          Container(
            padding: const EdgeInsets.all(8),
            width: width ?? 120,
            color: primaryColor,
            child: Text(
              title,
              style: const TextStyle(color: Colors.white),
            ),
          ),
          Expanded(
            child: Stack(
              children: [
                Container(
                  padding: const EdgeInsets.all(8),
                  color: single ? Colors.white : const Color(0xfff5f5f5),
                ),
                Container(
                  padding: const EdgeInsets.all(8),
                  child: Text(
                    value,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  ExpandableController leadController = ExpandableController();

  Widget lead(List<LeadItems> list) {
    var totalOrg = 0;
    var totalPoint = 0;
    if (list.isNotEmpty && list.first.pay3Data != null) {
      for (var element in list.first.pay3Data!) {
        totalOrg += element.orgpv3 ?? 0;
        totalPoint += element.integration ?? 0;
      }
    } else if (list.isEmpty) {
      return const SizedBox();
    }
    return Container(
      padding: const EdgeInsets.only(
        left: 4,
        right: 4,
        bottom: 4,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(color: Colors.grey),
      ),
      child: ExpandablePanel(
        key: UniqueKey(),
        controller: leadController,
        theme: const ExpandableThemeData(
          headerAlignment: ExpandablePanelHeaderAlignment.center,
        ),
        header: Text(
          '領導獎金 : ${PriceTools.getCurrencyFormatted(
                list.first.pay3,
                null,
                isPV: true,
              ) ?? ''}',
        ),
        collapsed: const SizedBox(),
        expanded: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Table(
                defaultColumnWidth: const IntrinsicColumnWidth(),
                children: [
                  TableRow(
                    decoration: const BoxDecoration(color: primaryColor),
                    children: [
                      Container(
                        decoration: const BoxDecoration(
                            border: Border.symmetric(vertical: BorderSide(color: Colors.white,width: 0.5))
                        ),
                        padding: const EdgeInsets.symmetric(vertical: 4,horizontal: 8),
                        child: const Text(
                          '下線會員',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        decoration: const BoxDecoration(
                            border: Border.symmetric(vertical: BorderSide(color: Colors.white,width: 0.5))
                        ),
                        padding: const EdgeInsets.symmetric(vertical: 4,horizontal: 8),
                        child: const Text(
                          '組織業績',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        decoration: const BoxDecoration(
                            border: Border.symmetric(vertical: BorderSide(color: Colors.white,width: 0.5))
                        ),
                        padding: const EdgeInsets.symmetric(vertical: 4,horizontal: 8),
                        child: const Text(
                          'Ｘ比率',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        decoration: const BoxDecoration(
                            border: Border.symmetric(vertical: BorderSide(color: Colors.white,width: 0.5))
                        ),
                        padding: const EdgeInsets.symmetric(vertical: 4,horizontal: 8),
                        child: const Text(
                          '= 積分',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        decoration: const BoxDecoration(
                            border: Border.symmetric(vertical: BorderSide(color: Colors.white,width: 0.5))
                        ),
                        padding: const EdgeInsets.symmetric(vertical: 4,horizontal: 8),
                        child: const Text(
                          '獎金',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                  for (int i = 0; i < list[0].pay3Data!.length; i++)
                    TableRow(
                      decoration: BoxDecoration(
                          color: i % 2 == 1 ? const Color(0xfff5f5f5) : Colors.white),
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 4,horizontal: 8),
                          child: Text(
                            list[0].pay3Data![i].gen.toString(),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 4,horizontal: 8),
                          child: Text(
                            PriceTools.getCurrencyFormatted(
                                  list[0].pay3Data![i].orgpv3.toString(),
                                  null,
                                  isPV: true,
                                ) ??
                                '',
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 4,horizontal: 8),
                          child: Text(
                            list[0].pay3Data![i].prcnt.toString() + '%',
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 4,horizontal: 8),
                          child: Text(
                            PriceTools.getCurrencyFormatted(
                                  list[0].pay3Data![i].integration.toString(),
                                  null,
                                  isPV: true,
                                ) ??
                                '',
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 4,horizontal: 8),
                          child: Text(
                            PriceTools.getCurrencyFormatted(
                                  list[0].pay3Data![i].pay3uni.toString(),
                                  null,
                                  isPV: true,
                                ) ??
                                '',
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  TableRow(
                    decoration: BoxDecoration(
                        color: list[0].pay3Data!.length % 2 == 1
                            ? const Color(0xfff5f5f5)
                            : Colors.white),
                    children: [
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 4,horizontal: 8),
                        child: Text(
                          '小計',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Color(0xFFe90909)),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 4,horizontal: 8),
                        child: Text(
                          PriceTools.getCurrencyFormatted(
                                totalOrg.toString(),
                                null,
                                isPV: true,
                              ) ??
                              '',
                          textAlign: TextAlign.center,
                          style: const TextStyle(color: Color(0xFFe90909)),
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 4,horizontal: 8),
                        child: Text(
                          '',
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 4,horizontal: 8),
                        child: Text(
                          PriceTools.getCurrencyFormatted(
                                totalPoint.toString(),
                                null,
                                isPV: true,
                              ) ??
                              '',
                          textAlign: TextAlign.center,
                          style: const TextStyle(color: Color(0xFFe90909)),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 4,horizontal: 8),
                        child: Text(
                          PriceTools.getCurrencyFormatted(
                                list.first.pay3.toString(),
                                null,
                                isPV: true,
                              ) ??
                              '',
                          textAlign: TextAlign.center,
                          style: const TextStyle(color: Color(0xFFe90909)),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Text(
              '*每一積分金額：${list.first.leadershipPoints}',
              style: const TextStyle(
                color: Color(0xFFe90909),
                fontSize: 14,
              ),
            ),
          ],
        ),
      ),
    );
    // return Table(
    //   children: [
    //     TableRow(
    //       decoration: const BoxDecoration(color: primaryColor),
    //       children: [
    //         Container(
    //           padding: const EdgeInsets.all(4),
    //           child: const Text(
    //             '下線會員',
    //             textAlign: TextAlign.center,
    //             style:
    //                 TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
    //           ),
    //         ),
    //         Container(
    //           padding: const EdgeInsets.all(4),
    //           child: const Text(
    //             '組織業績',
    //             textAlign: TextAlign.center,
    //             style:
    //                 TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
    //           ),
    //         ),
    //         Container(
    //           padding: const EdgeInsets.all(4),
    //           child: const Text(
    //             'Ｘ比率',
    //             textAlign: TextAlign.center,
    //             style:
    //                 TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
    //           ),
    //         ),
    //         Container(
    //           padding: const EdgeInsets.all(4),
    //           child: const Text(
    //             '= 積分',
    //             textAlign: TextAlign.center,
    //             style:
    //                 TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
    //           ),
    //         ),
    //         Container(
    //           padding: const EdgeInsets.all(4),
    //           child: const Text(
    //             '獎金',
    //             textAlign: TextAlign.center,
    //             style:
    //                 TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
    //           ),
    //         ),
    //       ],
    //     ),
    //     for (int i = 0; i < list[0].pay3Data!.length; i++)
    //       TableRow(
    //         decoration: BoxDecoration(
    //             color: i % 2 == 1 ? const Color(0xfff5f5f5) : Colors.white),
    //         children: [
    //           Padding(
    //             padding: const EdgeInsets.all(4),
    //             child: Text(
    //               list[0].pay3Data![i].gen.toString(),
    //               textAlign: TextAlign.center,
    //             ),
    //           ),
    //           Padding(
    //             padding: const EdgeInsets.all(4),
    //             child: Text(
    //               list[0].pay3Data![i].orgpv3.toString(),
    //               textAlign: TextAlign.center,
    //             ),
    //           ),
    //           Padding(
    //             padding: const EdgeInsets.all(4),
    //             child: Text(
    //               list[0].pay3Data![i].prcnt.toString() + '%',
    //               textAlign: TextAlign.center,
    //             ),
    //           ),
    //           Padding(
    //             padding: const EdgeInsets.all(4),
    //             child: Text(
    //               list[0].pay3Data![i].integration.toString(),
    //               textAlign: TextAlign.center,
    //             ),
    //           ),
    //           Padding(
    //             padding: const EdgeInsets.all(4),
    //             child: Text(
    //               list[0].pay3Data![i].pay3uni.toString(),
    //               textAlign: TextAlign.center,
    //             ),
    //           ),
    //         ],
    //       )
    //   ],
    // );
  }

  ExpandableController divideController = ExpandableController();

  Widget divide(Dividend dividend) {
    return Container(
      padding: const EdgeInsets.only(
        left: 4,
        right: 4,
        bottom: 4,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(color: Colors.grey),
      ),
      child: ExpandablePanel(
        key: UniqueKey(),
        controller: divideController,
        theme: const ExpandableThemeData(
          headerAlignment: ExpandablePanelHeaderAlignment.center,
        ),
        header: Text(
          '分紅獎金 : ${PriceTools.getCurrencyFormatted(
                dividend.amountMonth,
                null,
                isPV: true,
              ) ?? ''}',
        ),
        collapsed: const SizedBox(),
        expanded: Table(
          children: [
            TableRow(
              decoration: const BoxDecoration(color: primaryColor),
              children: [
                Container(
                  padding: const EdgeInsets.all(4),
                  child: const Text(
                    '本月實付金額',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                Padding(
                  padding: const EdgeInsets.all(4),
                  child: Text(
                    PriceTools.getCurrencyFormatted(
                          dividend.amountMonth.toString(),
                          null,
                          isPV: true,
                        ) ??
                        '',
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
    // return Column(
    //   children: [
    //     organizationTitle('本月應付金額', dividend.amountDueMonth.toString(), true),
    //     const SizedBox(height: 1),
    //     organizationTitle('本月實付金額', dividend.amountMonth.toString(), false),
    //   ],
    // );
  }

  ExpandableController competitionController = ExpandableController();

  Widget competition(String value) {
    return Container(
      padding: const EdgeInsets.only(
        left: 4,
        right: 4,
        bottom: 4,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(color: Colors.grey),
      ),
      child: ExpandablePanel(
        key: UniqueKey(),
        controller: competitionController,
        theme: const ExpandableThemeData(
          headerAlignment: ExpandablePanelHeaderAlignment.center,
        ),
        header: Text(
          '競賽獎金 : ${PriceTools.getCurrencyFormatted(
                value,
                null,
                isPV: true,
              ) ?? ''}',
        ),
        collapsed: const SizedBox(),
        expanded: Table(
          children: [
            TableRow(
              decoration: const BoxDecoration(color: primaryColor),
              children: [
                Container(
                  padding: const EdgeInsets.all(4),
                  child: const Text(
                    '本月實付金額',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                Padding(
                  padding: const EdgeInsets.all(4),
                  child: Text(
                    PriceTools.getCurrencyFormatted(
                          value,
                          null,
                          isPV: true,
                        ) ??
                        '',
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
    // return Column(
    //   children: [
    //     organizationTitle('本月實付金額', value, true),
    //   ],
    // );
  }

  ExpandableController travelController = ExpandableController();

  Widget travel(TravelBonus travelBonus) {
    return Container(
      padding: const EdgeInsets.only(
        left: 4,
        right: 4,
        bottom: 4,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(color: Colors.grey),
      ),
      child: ExpandablePanel(
        key: UniqueKey(),
        controller: travelController,
        theme: const ExpandableThemeData(
          headerAlignment: ExpandablePanelHeaderAlignment.center,
        ),
        header: Text(
          '旅遊獎金 : ${PriceTools.getCurrencyFormatted(
                travelBonus.showOnList,
                null,
                isPV: true,
              ) ?? ''} (季、年發放)',
        ),
        collapsed: const SizedBox(),
        expanded: Table(
          children: [
            TableRow(
              decoration: const BoxDecoration(color: primaryColor),
              children: [
                Container(
                  padding: const EdgeInsets.all(4),
                  child: const Text(
                    '本季金額(年發放)',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(4),
                  child: const Text(
                    '累積金額(年發放)',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
            TableRow(
              children: [
                Padding(
                  padding: const EdgeInsets.all(4),
                  child: Text(
                    PriceTools.getCurrencyFormatted(
                          travelBonus.amountQuarter.toString(),
                          null,
                          isPV: true,
                        ) ??
                        '',
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(4),
                  child: Text(
                    PriceTools.getCurrencyFormatted(
                          travelBonus.cumulativeAmount.toString(),
                          null,
                          isPV: true,
                        ) ??
                        '',
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
    // return Column(
    //   children: [
    //     organizationTitle(
    //       '本季金額(年發放)',
    //       travelBonus.amountQuarter.toString(),
    //       true,
    //       width: 150,
    //     ),
    //     const SizedBox(height: 1),
    //     organizationTitle(
    //       '累積金額(年發放)',
    //       travelBonus.cumulativeAmount.toString(),
    //       false,
    //       width: 150,
    //     ),
    //   ],
    // );
  }

  ExpandableController totalController = ExpandableController();

  Widget total(PayTotal payTotal) {
    payTotal.values?.removeWhere((element) => element == '');
    payTotal.titles?.removeWhere((element) => element == '');
    return Container(
      padding: const EdgeInsets.only(
        left: 4,
        right: 4,
        bottom: 4,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(color: Colors.grey),
      ),
      child: ExpandablePanel(
        key: UniqueKey(),
        controller: totalController,
        theme: const ExpandableThemeData(
          headerAlignment: ExpandablePanelHeaderAlignment.center,
        ),
        header: Text(
          '支付總額 : ${PriceTools.getCurrencyFormatted(
                payTotal.values?.last,
                null,
                isPV: true,
              ) ?? ''}',
        ),
        collapsed: const SizedBox(),
        expanded: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            for (int i = 0; i < payTotal.titles!.length; i++) ...[
              if (payTotal.titles![i].isNotEmpty) ...[
                organizationTitle(
                  payTotal.titles![i],
                  PriceTools.getCurrencyFormatted(
                        payTotal.values![i].toString(),
                        null,
                        isPV: true,
                      ) ??
                      '',
                  i % 2 == 1,
                ),
                const SizedBox(height: 1),
              ],
            ],
            const SizedBox(height: 4),
            const Text(
              '**本月支付您的金額如上，請確認。（如有錯誤，次月更正）**',
              style: TextStyle(
                color: Color(0xFFe90909),
                fontSize: 14,
              ),
            ),
            Text(
              '<<您的佣金已存入--${bonusPageProvider.getBonusDetailResponse?.data?.accno}--帳戶>>',
              style: const TextStyle(
                color: Color(0xFFe90909),
                fontSize: 14,
              ),
            )
          ],
        ),
      ),
    );
    // return Column(
    //   children: [
    //     for (int i = 0; i < payTotal.titles!.length; i++) ...[
    //       if (payTotal.titles![i].isNotEmpty)
    //         organizationTitle(
    //           payTotal.titles![i],
    //           payTotal.values![i].toString(),
    //           i % 2 == 1,
    //         ),
    //       const SizedBox(height: 1),
    //     ],
    //   ],
    // );
  }
}
