import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/constants.dart';
import '../../menu/maintab_delegate.dart';
import '../../models/user_model.dart';
import '../../screens/detail/product_detail_screen.dart';
import '../../screens/order_history/views/new_order_history_datail_screen.dart';
import '../../services/services.dart';
import '../model/get_service_record_response.dart';
import '../model/notification_response.dart';
import 'article_content_page.dart';
import 'article_list_page.dart';
import 'calender_page.dart';
import 'marketing_page.dart';
import 'points_page.dart';
import 'service_content_page.dart';
import 'webview_with_cookie.dart';

class NotificationDetailPage extends StatefulWidget {
  final NotificationModel model;
  final VoidCallback setRead;

  const NotificationDetailPage({
    Key? key,
    required this.model,
    required this.setRead,
  }) : super(key: key);

  @override
  State<NotificationDetailPage> createState() => _NotificationDetailPageState();
}

class _NotificationDetailPageState extends State<NotificationDetailPage> {
  @override
  void initState() {
    setRead();
    super.initState();
  }

  Future<void> setRead() async {
    var success = await Services().api.setNotificationRead(
        userId: Provider.of<UserModel>(context, listen: false).user?.id ?? '',
        notificationId: widget.model.id ?? '');
    if (success ?? false) {
      widget.setRead.call();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          '訊息通知',
          style: TextStyle(
            color: Theme.of(context).colorScheme.secondary,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    width: double.maxFinite,
                  ),
                  if (widget.model.image != null &&
                      widget.model.image!.isNotEmpty) ...[
                    Image.network(
                      widget.model.image ?? '',
                      width: double.maxFinite,
                    ),
                    const SizedBox(height: 16),
                  ],
                  Text(
                    widget.model.cat ?? '',
                    softWrap: true,
                    style: const TextStyle(
                      color: Colors.black54,
                    ),
                  ),
                  Text(
                    widget.model.title ?? '',
                    softWrap: true,
                    style: const TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  const SizedBox(height: 16),
                  Text(
                    widget.model.date ?? '',
                    softWrap: true,
                    style: const TextStyle(
                      fontSize: 14,
                      color: primaryColor,
                    ),
                  ),
                  const SizedBox(height: 16),
                  Text(
                    widget.model.content ?? '',
                    softWrap: true,
                    style: const TextStyle(
                      color: Colors.black54,
                    ),
                  ),
                  const SizedBox(height: 16),
                  if (widget.model.appLink != null &&
                      widget.model.appLink!.isNotEmpty &&
                      widget.model.appLink != 'none')
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        elevation: 0,
                        padding: const EdgeInsets.all(
                          12,
                        ),
                      ),
                      onPressed: () {
                        switch (widget.model.appLink) {
                          case 'account':
                            Navigator.of(context).popUntil(
                                ModalRoute.withName(RouteList.dashboard));
                            MainTabControlDelegate.getInstance().changeTab(
                                RouteList.profile.replaceFirst('/', ''));
                            break;
                          case 'shop':
                            Navigator.of(context).popUntil(
                                ModalRoute.withName(RouteList.dashboard));
                            MainTabControlDelegate.getInstance().changeTab(
                                RouteList.products.replaceFirst('/', ''));
                            break;
                          case 'sale_total':
                            Navigator.of(context).popUntil(
                                ModalRoute.withName(RouteList.dashboard));
                            MainTabControlDelegate.getInstance().changeTab(
                                RouteList.performance.replaceFirst('/', ''));
                            break;
                          case 'point':
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => const PointsPage(),
                              ),
                            );
                            break;
                          case 'calendar':
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => const CalendarPage(),
                              ),
                            );
                            break;
                          case 'post':
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => const ArticleListPage(),
                              ),
                            );
                            break;
                          case 'affiliate':
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => const MarketingPage(),
                              ),
                            );
                            break;
                          default:
                            if (widget.model.appLink!.contains('product')) {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => ProductDetailScreen(
                                    id: widget.model.appLink!
                                        .replaceAll('product:', ''),
                                  ),
                                ),
                              );
                            }
                            if (widget.model.appLink!.contains('order:')) {
                              openOrder();
                            }
                            if (widget.model.appLink!.contains('https')) {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => WebViewWithCookie(
                                    url: 'https://' +
                                        Uri.parse(widget.model.appLink!).host +
                                        Uri.parse(widget.model.appLink!).path,
                                    notDirectPop: true,
                                  ),
                                ),
                              );
                            }
                            if (widget.model.appLink!.contains('ticket:')) {
                              var list = widget.model.appLink?.split(':');
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => ServiceContentPage(
                                    model: ServiceRecordBean(
                                      formId: int.parse(list![1]),
                                      entryId: int.parse(list[2]),
                                    ),
                                  ),
                                ),
                              );
                            }
                            if (widget.model.appLink!.contains('post:')) {
                              var list = widget.model.appLink?.split(':');
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => ArticleContentPage(
                                      id: int.parse(list![1])),
                                ),
                              );
                            }
                        }
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Text(
                            '立即前往',
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> openOrder() async {
    final id = widget.model.appLink!.substring(
        widget.model.appLink!.indexOf(':') + 1, widget.model.appLink!.length);

    await Navigator.of(context).push(
      MaterialPageRoute(
          builder: (context) => NewOrderHistoryDetailScreen(orderId: id)),
    );
  }
}
