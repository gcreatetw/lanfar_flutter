import 'package:flutter/material.dart';
import '../../common/theme/colors.dart';
import '../provider/marketing_page_provider.dart';

class MarketingGroupPage extends StatefulWidget {
  final int group;
  final MarketingPageProvider provider;

  const MarketingGroupPage(
      {Key? key, required this.group, required this.provider})
      : super(key: key);

  @override
  State<MarketingGroupPage> createState() => _MarketingGroupPageState();
}

class _MarketingGroupPageState extends State<MarketingGroupPage> {
  List<GroupProductModel> choice = [];

  @override
  void initState() {
    for (var element in widget.provider.groups[widget.group - 1]) {
      choice.add(element);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text('群組 ${widget.group}'),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.separated(
              itemCount: widget.provider.products.length,
              itemBuilder: (context, index) {
                var product = widget.provider.products[index];
                return CheckboxListTile(
                  value: choice
                          .indexWhere((element) => element.id == product.id) !=
                      -1,
                  onChanged: (v) {
                    if (choice.indexWhere(
                            (element) => element.id == product.id) !=
                        -1) {
                      choice.removeWhere((element) => element.id == product.id);
                    } else {
                      choice.add(
                        GroupProductModel(
                          id: product.id ?? '',
                          name: product.name ?? '',
                          sku: product.sku ?? '',
                        ),
                      );
                    }
                    setState(() {});
                  },
                  activeColor: primaryColor,
                  tileColor: choice.indexWhere(
                              (element) => element.id == product.id) !=
                          -1
                      ? Colors.black12
                      : null,
                  selectedTileColor: Colors.black12,
                  title: Text((product.name ?? '') +
                      (product.sku != null && product.sku!.isNotEmpty
                          ? '(${product.sku})'
                          : '')),
                );
              },
              separatorBuilder: (context, index) {
                return Container(
                  width: double.maxFinite,
                  height: 0.5,
                  color: Colors.black12,
                );
              },
            ),
          ),
          Row(children: [
            Expanded(
              child: ButtonTheme(
                height: 45,
                child: ElevatedButton(
                  style: ButtonStyle(
                      padding: MaterialStateProperty.all<EdgeInsets>(
                          const EdgeInsets.all(16)),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        const RoundedRectangleBorder(
                            borderRadius: BorderRadius.zero),
                      ),
                      backgroundColor: MaterialStateProperty.all(primaryColor)),
                  onPressed: () {
                    Navigator.pop(context, choice);
                  },
                  child: Padding(
                    padding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).padding.bottom),
                    child: const Text(
                      '更新',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
          ]),
        ],
      ),
    );
  }
}
