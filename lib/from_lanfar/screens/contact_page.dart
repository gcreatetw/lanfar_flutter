import 'dart:math';

import 'package:flutter/material.dart';
import 'package:inspireui/utils/colors.dart';
import 'package:provider/src/provider.dart';

import '../../common/constants.dart';
import '../../models/index.dart';
import '../../services/services.dart';
import '../contants/constants.dart';
import '../widget/custom_dropdown_menu.dart';
import '../widget/round_border_textfield.dart';
import '../widget/vertification_code.dart';
import 'term_agree_page.dart';

enum ContactStatus {
  unSend,
  sending,
  send,
}

enum ContactType {
  normal,
  order,
}

extension ContactTypeEX on ContactType {
  String get name {
    switch (this) {
      case ContactType.normal:
        return '線上客服';
      case ContactType.order:
        return '訂單諮詢';
    }
  }

  List<DropdownMenuItem<String>> get dropdownList {
    switch (this) {
      case ContactType.normal:
        return Constants.normalContactTypes
            .map(
              (e) => DropdownMenuItem<String>(
                value: e,
                child: Text(e),
              ),
            )
            .toList();
      case ContactType.order:
        return Constants.orderContactTypes
            .map(
              (e) => DropdownMenuItem<String>(
                value: e,
                child: Text(e),
              ),
            )
            .toList();
    }
  }

  String get defaultValue {
    switch (this) {
      case ContactType.normal:
        return Constants.normalContactTypes.first;
      case ContactType.order:
        return Constants.orderContactTypes.first;
    }
  }

  int get id {
    switch (this) {
      case ContactType.normal:
        return 5;
      case ContactType.order:
        return 6;
    }
  }
}

class ContactPage extends StatefulWidget {
  final ContactType type;
  final String? orderId;

  const ContactPage({
    Key? key,
    required this.type,
    this.orderId,
  }) : super(key: key);

  @override
  _ContactPageState createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  final TextEditingController mailController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController contentController = TextEditingController();
  final TextEditingController orderIdController = TextEditingController();

  final FocusNode phoneNode = FocusNode();
  final FocusNode contentNode = FocusNode();

  String code = '';

  late Widget verificationCode;

  String? currentValue;

  bool agreePrivacy = false;

  ContactStatus status = ContactStatus.unSend;

  @override
  void initState() {
    super.initState();
    dropDownMenuList = widget.type.dropdownList;
    currentValue = widget.type.defaultValue;
    phoneController.text = context.read<UserModel>().user?.phone ?? '';
    mailController.text = context.read<UserModel>().user?.email ?? '';
    orderIdController.text = widget.orderId ?? '';
    // _createRandomCode();
    // verificationCode = HBCheckCode(
    //   width: 80,
    //   dotCount: 20,
    //   code: code,
    //   backgroundColor: Colors.white,
    // );
  }

  @override
  void dispose() {
    mailController.dispose();
    phoneController.dispose();
    contentController.dispose();
    orderIdController.dispose();
    phoneNode.dispose();
    contentNode.dispose();
    super.dispose();
  }

  List<DropdownMenuItem<String>> dropDownMenuList = [];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Theme.of(context).backgroundColor,
          iconTheme: IconThemeData(
              color: Theme.of(context).textTheme.headline6!.color),
          title: Text(
            widget.type.name,
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
        ),
        body: status == ContactStatus.send
            ? const Padding(
                padding: EdgeInsets.all(16),
                child: Center(
                  child: Text(
                    '感謝您與我們聯絡！我們將盡快與你聯繫。',
                    style: TextStyle(color: Colors.black54),
                  ),
                ),
              )
            : SingleChildScrollView(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    elevation: 5,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(
                            height: 24,
                            width: double.maxFinite,
                          ),
                          // const Text(
                          //   '聯絡客服',
                          //   style: TextStyle(
                          //       fontSize: 24,
                          //       fontWeight: FontWeight.bold,
                          //       color: primaryColor),
                          // ),
                          // const SizedBox(height: 8),
                          // Text(
                          //   '若您有任何需要我們服務的地方，請填寫以下表單表達您的意見，我們收到您的來信後，將儘速於 3～5 日內回覆（不含週六例假日）。',
                          //   style:
                          //       TextStyle(fontSize: 14, color: HexColor('#666666')),
                          //   textAlign: TextAlign.left,
                          // ),
                          // const SizedBox(height: 16),
                          RichText(
                            text: const TextSpan(
                              text: '諮詢類型',
                              style: TextStyle(
                                color: Colors.black54,
                                fontWeight: FontWeight.bold,
                              ),
                              children: [
                                TextSpan(
                                  text: ' *',
                                  style: TextStyle(
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(height: 8),
                          CustomDropDownMenu(
                            borderColor: HexColor('#EAEAEA'),
                            hintColor: HexColor('#666666'),
                            hintText: '諮詢類型',
                            items: dropDownMenuList,
                            onChange: status == ContactStatus.sending
                                ? null
                                : (String? value) {
                                    setState(() {
                                      currentValue = value;
                                    });
                                  },
                            currentValue: currentValue,
                          ),
                          if (widget.type == ContactType.order) ...[
                            const SizedBox(height: 16),
                            RichText(
                              text: const TextSpan(
                                text: '訂單編號',
                                style: TextStyle(
                                  color: Colors.black54,
                                  fontWeight: FontWeight.bold,
                                ),
                                children: [
                                  TextSpan(
                                    text: ' *',
                                    style: TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(height: 8),
                            RoundBorderTextField(
                              controller: orderIdController,
                              code: true,
                              hint: '訂單編號',
                              required: false,
                              textColor: HexColor('#AFAFAF'),
                              borderColor: HexColor('#EAEAEA'),
                              enable: false,
                            ),
                          ],
                          const SizedBox(height: 16),
                          RichText(
                            text: const TextSpan(
                              text: '電話',
                              style: TextStyle(
                                color: Colors.black54,
                                fontWeight: FontWeight.bold,
                              ),
                              children: [
                                TextSpan(
                                  text: ' *',
                                  style: TextStyle(
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(height: 8),
                          RoundBorderTextField(
                            controller: phoneController,
                            nowFocusNode: phoneNode,
                            nextFocusNode: contentNode,
                            code: true,
                            hint: '電話',
                            required: false,
                            textColor: HexColor('#AFAFAF'),
                            borderColor: HexColor('#EAEAEA'),
                            enable: status == ContactStatus.unSend,
                          ),

                          ///email
                          // const SizedBox(height: 16),
                          // RichText(
                          //   text: const TextSpan(
                          //     text: 'E-mail(選填)',
                          //     style: TextStyle(
                          //       color: Colors.black54,
                          //       fontWeight: FontWeight.bold,
                          //     ),
                          //   ),
                          // ),
                          // const SizedBox(height: 8),
                          // RoundBorderTextField(
                          //   controller: mailController,
                          //   nextFocusNode: phoneNode,
                          //   hint: 'E-mail(選填)',
                          //   required: false,
                          //   textColor: HexColor('#AFAFAF'),
                          //   borderColor: HexColor('#EAEAEA'),
                          //   enable: status == ContactStatus.unSend,
                          // ),
                          const SizedBox(height: 16),
                          RichText(
                            text: const TextSpan(
                              text: '諮詢內容',
                              style: TextStyle(
                                color: Colors.black54,
                                fontWeight: FontWeight.bold,
                              ),
                              children: [
                                TextSpan(
                                  text: ' *',
                                  style: TextStyle(
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(height: 8),
                          RoundBorderTextField(
                            controller: contentController,
                            hint: '諮詢內容',
                            nowFocusNode: contentNode,
                            required: false,
                            hasMinLines: true,
                            textColor: HexColor('#AFAFAF'),
                            borderColor: HexColor('#EAEAEA'),
                            enable: status == ContactStatus.unSend,
                          ),
                          const SizedBox(height: 16),
                          // Row(
                          //   children: [
                          //     Checkbox(
                          //       value: agreePrivacy,
                          //       onChanged: status == ContactStatus.sending
                          //           ? null
                          //           : (value) {
                          //               setState(() {
                          //                 agreePrivacy = value ?? false;
                          //               });
                          //             },
                          //     ),
                          //     RichText(
                          //       text: TextSpan(
                          //         text: '我已閱讀並同意',
                          //         style: const TextStyle(color: Colors.black54),
                          //         children: [
                          //           TextSpan(
                          //             text: '隱私權政策',
                          //             style:
                          //                 const TextStyle(color: Colors.blue),
                          //             recognizer: TapGestureRecognizer()
                          //               ..onTap = () {
                          //                 final privacyUrl =
                          //                     '${environment['serverConfig']['url']}/privacy/';
                          //                 FluxNavigate.push(
                          //                   MaterialPageRoute(
                          //                     builder: (context) =>
                          //                         WebViewWithCookie(
                          //                       url: privacyUrl,
                          //                       alwaysAddAppStyle: true,
                          //                       title: '隱私權政策',
                          //                     ),
                          //                   ),
                          //                   forceRootNavigator: true,
                          //                 );
                          //               },
                          //           ),
                          //         ],
                          //       ),
                          //     ),
                          //   ],
                          // ),
                          GestureDetector(
                            onTap: checkTerms,
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 0),
                              child: Row(
                                children: [
                                  Checkbox(
                                      activeColor: primaryColor,
                                      value: agreePrivacy,
                                      onChanged: (v) {
                                        checkTerms();
                                      }),
                                  Expanded(
                                    child: RichText(
                                      text: const TextSpan(
                                        text: '我已閱讀並同意',
                                        style: TextStyle(color: Colors.black54),
                                        children: [
                                          TextSpan(
                                            text: '隱私權政策',
                                            style:
                                                TextStyle(color: Colors.blue),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(height: 16),
                          ElevatedButton(
                            onPressed: status == ContactStatus.sending
                                ? null
                                : () async {
                                    if (phoneController.text.isEmpty) {
                                      showSnack('電話未填寫');
                                      return;
                                    }
                                    if (contentController.text.isEmpty) {
                                      showSnack('諮詢內容未填寫');
                                      return;
                                    }
                                    if (!agreePrivacy) {
                                      showSnack('請先同意隱私權政策');
                                      return;
                                    }
                                    setState(() {
                                      status = ContactStatus.sending;
                                    });
                                    var response =
                                        await Services().api.sendServiceForm(
                                              formId: widget.type.id,
                                              userName: context
                                                      .read<UserModel>()
                                                      .user!
                                                      .username ??
                                                  '',
                                              phone: phoneController.text,
                                              email: mailController.text,
                                              type: currentValue ?? '',
                                              content: contentController.text,
                                              order: widget.orderId,
                                            );
                                    if (response == null) {
                                      setState(() {
                                        status = ContactStatus.send;
                                      });
                                    } else {
                                      setState(() {
                                        status = ContactStatus.unSend;
                                      });
                                      showSnack(response['msg']);
                                    }
                                  },
                            style: ElevatedButton.styleFrom(
                              padding: const EdgeInsets.symmetric(vertical: 9),
                              elevation: 0,
                              primary: Theme.of(context).primaryColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                            ),
                            child: Container(
                              width: double.maxFinite,
                              alignment: Alignment.center,
                              child: Text(
                                status == ContactStatus.sending ? '提交中' : '提交',
                                style: const TextStyle(
                                    fontSize: 16, color: Colors.white),
                              ),
                            ),
                          ),
                          const SizedBox(height: 24),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
      ),
    );
  }

  void _createRandomCode() {
    setState(() {
      code = '';
      for (var i = 0; i < 4; i++) {
        code = code + Random().nextInt(9).toString();
      }
      verificationCode = HBCheckCode(
        width: 80,
        dotCount: 20,
        code: code,
        backgroundColor: Colors.white,
      );
    });
  }

  void showSnack(String content) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(content),
      ),
    );
  }

  void checkTerms() {
    if (agreePrivacy) {
      setState(() {
        agreePrivacy = false;
      });
    } else {
      showDialog(
        context: context,
        builder: (context) => Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          margin: const EdgeInsets.all(50),
          child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: TermAgreePage(
                type: TermsType.privacy,
                check: () {
                  setState(() {
                    agreePrivacy = true;
                  });
                },
              )),
        ),
      );
    }
  }
}
