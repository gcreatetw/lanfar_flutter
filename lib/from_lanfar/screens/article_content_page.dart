import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../models/text_style_model.dart';
import '../../services/index.dart';
import '../../widgets/blog/text_adjustment_button.dart';
import '../../widgets/html/index.dart';

class ArticleContentPage extends StatefulWidget {
  final int id;
  final bool isBanner;

  const ArticleContentPage({
    Key? key,
    required this.id,
    this.isBanner = false,
  }) : super(key: key);

  @override
  State<ArticleContentPage> createState() => _ArticleContentPageState();
}

class _ArticleContentPageState extends State<ArticleContentPage> {
  bool loading = true;
  String content = '';
  String title = '';
  String date = '';
  String link = '';
  String image = '';

  @override
  void initState() {
    getContent();
    super.initState();
  }

  Future<void> getContent() async {
    var response = await Services().api.getArticleContent(id: widget.id);
    if (response != null) {
      setState(() {
        title = response.postTitle ?? '';
        content = response.postContent ?? '';
        date = response.postDate ?? '';
        link = response.link ?? '';
        image = response.image ?? '';
        loading = false;
      });
      return;
    }
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text('發生錯誤，請稍後再試'),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        actions: [
          const TextAdjustmentButton(18.0),
          GestureDetector(
            onTap: () {
              // Services().firebase.shareDynamicLinkProduct(
              //       context: context,
              //       itemUrl: link,
              //     );
              Share.share(link);
            },
            child: Container(
              margin: const EdgeInsets.all(8.0),
              padding: const EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor.withOpacity(0.5),
                borderRadius: BorderRadius.circular(30.0),
              ),
              child: Icon(
                Icons.share,
                size: 18.0,
                color: Theme.of(context).colorScheme.secondary,
              ),
            ),
          ),
        ],
      ),
      body: loading
          ? kLoadingWidget(context)
          : ListView(
              padding: const EdgeInsets.all(15),
              children: [
                if (widget.isBanner && image.isNotEmpty) Image.network(image),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Text(
                    title,
                    softWrap: true,
                    style: const TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Text(
                  date,
                  softWrap: true,
                  style: TextStyle(
                    fontSize: 14,
                    color: Theme.of(context)
                        .colorScheme
                        .secondary
                        .withOpacity(0.45),
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(height: 16),
                Consumer<TextStyleModel>(
                  builder: (context, textStyleModel, child) {
                    var htmlWidget = HtmlWidget(
                      content,
                      textStyle:
                          Theme.of(context).textTheme.bodyText1!.copyWith(
                                fontSize: textStyleModel.contentTextSize,
                                height: 1.4,
                                color: kAdvanceConfig['DetailedBlogLayout'] ==
                                        kBlogLayout.fullSizeImageType
                                    ? Colors.white
                                    : null,
                              ),
                      // factoryBuilder: () => InstagramWidgetFactory(),
                    );

                    return htmlWidget;
                  },
                ),
                const SizedBox(height: 16),
              ],
            ),
    );
  }
}
