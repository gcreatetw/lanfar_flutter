import 'package:flutter/material.dart';
import 'package:lanfar/common/config.dart';
import 'package:provider/src/provider.dart';

import '../../models/index.dart';
import '../widget/round_border_textfield.dart';

class DeletePage extends StatefulWidget {
  const DeletePage({Key? key}) : super(key: key);

  @override
  State<DeletePage> createState() => _DeletePageState();
}

class _DeletePageState extends State<DeletePage> {
  TextEditingController account = TextEditingController();
  TextEditingController reason = TextEditingController();

  bool loading = false;

  @override
  void initState() {
    account.text = context.read<UserModel>().user?.username ?? '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Theme.of(context).backgroundColor,
          iconTheme: IconThemeData(
            color: Theme.of(context).textTheme.headline6!.color,
          ),
          title: Text(
            '刪除帳號',
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
        ),
        body: Stack(
          children: [
            ListView(
              padding: const EdgeInsets.all(16),
              children: [
                const Text(
                  '您的會員編號',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                const SizedBox(height: 8),
                RoundBorderTextField(
                  controller: account,
                  hint: '',
                  required: false,
                  enable: false,
                ),
                const SizedBox(height: 16),
                const Text(
                  '原因',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                const SizedBox(height: 8),
                RoundBorderTextField(
                  controller: reason,
                  hint: '',
                  required: false,
                  hasMinLines: true,
                ),
                const SizedBox(height: 30),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(elevation: 0),
                  onPressed: () {
                    setState(() {
                      loading = true;
                    });
                    Future.delayed(
                      const Duration(milliseconds: 1300),
                      () {
                        Navigator.pop(context, true);
                      },
                    );
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      SizedBox(width: 4),
                      Text(
                        '提交',
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            if (loading)
              Container(
                width: double.maxFinite,
                height: double.maxFinite,
                color: Colors.black54,
                child: Center(
                  child: kLoadingWidget(context),
                ),
              )
          ],
        ),
      ),
    );
  }
}
