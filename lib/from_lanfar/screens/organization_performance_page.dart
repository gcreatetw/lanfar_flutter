import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:inspireui/inspireui.dart';
import 'package:provider/provider.dart';
import 'package:provider/src/provider.dart';

import '../../common/config.dart';
import '../../models/index.dart';
import '../../screens/users/login_screen.dart';
import '../formatter/customized_length_formatter.dart';
import '../formatter/customized_text_formatter.dart';
import '../provider/organization_performance_page_provider.dart';
import '../widget/performance_treeview.dart';
import '../widget/round_border_textfield.dart';

class OrganizationPerformancePage extends StatefulWidget {
  const OrganizationPerformancePage({Key? key}) : super(key: key);

  @override
  State<OrganizationPerformancePage> createState() =>
      _OrganizationPerformancePageState();
}

class _OrganizationPerformancePageState
    extends State<OrganizationPerformancePage> {
  late OrganizationPerformancePageProvider organizationPerformancePageProvider;

  @override
  void initState() {
    organizationPerformancePageProvider = OrganizationPerformancePageProvider(
      user: context.read<UserModel>().user!,
      onError: (e) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(e),
          ),
        );
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        await SystemChrome.setPreferredOrientations([
          DeviceOrientation.portraitUp,
          DeviceOrientation.portraitDown,
        ]);
        return true;
      },
      child: ChangeNotifierProvider.value(
        value: organizationPerformancePageProvider,
        child: Consumer<OrganizationPerformancePageProvider>(
          builder: (context, provider, _) {
            return AutoHideKeyboard(
              child: Scaffold(
                backgroundColor: Colors.white,
                appBar: AppBar(
                  backgroundColor: Theme.of(context).backgroundColor,
                  iconTheme: IconThemeData(
                    color: Theme.of(context).textTheme.headline6!.color,
                  ),
                  title: Text(
                    '組織查詢',
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.secondary,
                    ),
                  ),
                  actions: [
                    IconButton(
                      onPressed: () {
                        if (MediaQuery.of(context).orientation ==
                            Orientation.portrait) {
                          SystemChrome.setPreferredOrientations([
                            DeviceOrientation.landscapeLeft,
                            DeviceOrientation.landscapeRight,
                          ]);
                        } else {
                          SystemChrome.setPreferredOrientations([
                            DeviceOrientation.portraitUp,
                            DeviceOrientation.portraitDown,
                          ]);
                        }
                      },
                      icon: const Icon(Icons.screen_rotation),
                    )
                  ],
                ),
                body: ListView(
                  padding: const EdgeInsets.all(8),
                  children: [
                    Row(
                      children: [
                        const Text('搜尋下線編號：'),
                        Expanded(
                          child: RoundBorderTextField(
                            controller: provider.searchIdController,
                            hint: '',
                            required: false,
                            inputFormatters: [
                              UpperCaseTextFormatter(),
                              CustomizedLengthTextInputFormatter(7),
                              CustomizedTextInputFormatter(
                                filterPattern: RegExp("[a-zA-Z]|[0-9]"),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 16),
                    Row(
                      children: [
                        const Text('搜尋下線姓名：'),
                        Expanded(
                          child: RoundBorderTextField(
                            controller: provider.searchNameController,
                            hint: '',
                            required: false,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 16),
                    Row(
                      children: [
                        const Text('搜尋顯示選項：'),
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: DropdownButton<int>(
                              underline: const SizedBox(),
                              isExpanded: true,
                              value: provider.promote,
                              onChanged: (value) {
                                setState(() {
                                  provider.promote = value;
                                });
                              },
                              hint: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: const [
                                  Text(
                                    '請選擇',
                                    textAlign: TextAlign.center,
                                  )
                                ],
                              ),
                              items: [
                                DropdownMenuItem<int>(
                                  value: 0,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: const [
                                      Text(
                                        '全部組織',
                                        textAlign: TextAlign.center,
                                      )
                                    ],
                                  ),
                                ),
                                DropdownMenuItem<int>(
                                  value: 1,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: const [
                                      Text(
                                        '未晉升男爵',
                                        textAlign: TextAlign.center,
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 16),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(elevation: 0),
                      onPressed: provider.state == SearchState.searching
                          ? null
                          : () {
                              provider.fetchData(context);
                            },
                      child: const Text(
                        '查詢',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    _content(provider),
                    SizedBox(height: MediaQuery.of(context).padding.bottom),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _content(OrganizationPerformancePageProvider provider) {
    switch (provider.state) {
      case SearchState.none:
        return const SizedBox();
      case SearchState.searching:
        return Column(
          children: [
            const SizedBox(height: 50),
            kLoadingWidget(context),
          ],
        );
      case SearchState.searched:
        return PerformanceTreeView(
          data: provider.dataList,
          tableData: provider.tableListFiltered,
          onSearch: provider.search,
        );
    }
  }
}
