import 'package:flutter/material.dart';
import 'package:inspireui/utils/encode.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../common/config.dart';
import '../../env.dart';
import '../../models/user_model.dart';

class CreditCardPage extends StatefulWidget {
  const CreditCardPage({Key? key}) : super(key: key);

  @override
  _CreditCardPageState createState() => _CreditCardPageState();
}

class _CreditCardPageState extends State<CreditCardPage> {
  int selectedIndex = 1;

  String body = '';

  late WebViewController controller;

  @override
  void initState() {
    // getUrl();
    super.initState();
    var user = Provider.of<UserModel>(context, listen: false);
    print(user.user!.cookie);
    controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (progress) {
            if (progress == 100) {
              setState(() {
                selectedIndex = 0;
              });
            }
          },
        ),
      )
      ..loadRequest(Uri.parse(
          '${environment['serverConfig']['url']}/my-accounts/ctcb-credit/?cookie=${EncodeUtils.encodeCookie(user.user!.cookie!)}'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).backgroundColor,
        elevation: 0.0,
      ),
      body: IndexedStack(
        index: selectedIndex,
        children: [
          WebViewWidget(
            controller: controller,
          ),
          Center(
            child: kLoadingWidget(context),
          )
        ],
      ),
    );
  }

  Future<void> getUrl() async {
    final user = Provider.of<UserModel>(context, listen: false).user;
    final token = user!.cookie;
    // var str = convert.jsonEncode({'token':token});
    // var bytes = convert.utf8.encode(str);
    // var base64Str = convert.base64.encode(bytes);
    //
    // final response =
    // await httpPost('${environment['serverConfig']['url']}wp-json/api/flutter_user'.toUri()!,
    //     body: convert.jsonEncode({
    //       'order': base64Str,
    //     }),
    //     headers: {'Content-Type': 'application/json'});
    // body = convert.jsonDecode(response.body);
    print(user.username);
  }
}
