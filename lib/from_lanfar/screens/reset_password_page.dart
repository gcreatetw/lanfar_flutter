import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/constants.dart';
import '../../common/theme/colors.dart';
import '../../common/tools/tools.dart';
import '../../generated/l10n.dart';
import '../../models/user_model.dart';
import '../../services/services.dart';
import 'qrcode_page.dart';

enum ResetPasswordState {
  begin,
  phoneSend,
  verified,
}

class ResetPasswordPage extends StatefulWidget {
  const ResetPasswordPage({Key? key}) : super(key: key);

  @override
  _ResetPasswordPageState createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  TextEditingController userCode = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController code = TextEditingController();
  TextEditingController passwordFirst = TextEditingController();
  TextEditingController passwordSecond = TextEditingController();

  final userCodeNode = FocusNode();
  final phoneNode = FocusNode();
  final codeNode = FocusNode();
  final passwordFirstNode = FocusNode();
  final passwordSecondNode = FocusNode();

  bool hideFirst = true;
  bool hideSecond = true;

  String smsCode = '';

  String verId = '';

  bool verify = false;

  bool verifying = false;

  String? userId;

  bool resetting = false;

  ResetPasswordState state = ResetPasswordState.begin;

  @override
  void dispose() {
    passwordFirst.dispose();
    passwordSecond.dispose();
    passwordFirstNode.dispose();
    passwordSecondNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ListenableProvider.value(
      value: Provider.of<UserModel>(context),
      child: Consumer<UserModel>(builder: (context, value, child) {
        return Scaffold(
          backgroundColor: Theme.of(context).backgroundColor,
          body: Builder(
            builder: (context) => GestureDetector(
              onTap: () => Tools.hideKeyboard(context),
              behavior: HitTestBehavior.opaque,
              child: SizedBox(
                height: double.maxFinite,
                child: SingleChildScrollView(
                  padding: const EdgeInsets.symmetric(horizontal: 0.0),
                  child: Container(
                      // height: MediaQuery.of(context).size.height,
                      alignment: Alignment.topCenter,
                      child: AutofillGroup(
                        child: Column(
                          children: [
                            Stack(
                              alignment: Alignment.bottomCenter,
                              children: [
                                Column(
                                  children: [
                                    Image.asset(
                                      'assets/icons/from_lanfar/Image 3@2x.png',
                                      fit: BoxFit.fitWidth,
                                    ),
                                    Container(
                                      height: 5,
                                      color: Theme.of(context).backgroundColor,
                                    ),
                                  ],
                                ),
                                Container(
                                  margin: const EdgeInsets.only(bottom: 5),
                                  height: 25,
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).backgroundColor,
                                    borderRadius: const BorderRadius.only(
                                      topRight: Radius.circular(15),
                                      topLeft: Radius.circular(15),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black.withOpacity(0.2),
                                        spreadRadius: 1,
                                        blurRadius: 3,
                                        offset: const Offset(0,
                                            -1), // changes position of shadow
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 10,
                                  color: Theme.of(context).backgroundColor,
                                ),
                              ],
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 16),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const <Widget>[
                                  SizedBox(width: double.maxFinite),
                                  Text(
                                    '重設密碼',
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(height: 4),
                                  Text(
                                    '請輸入您的新密碼並且再次確認密碼，往後請使用此密碼登入連法國際APP',
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Color(0xff666666),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(height: 40),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 16),
                              child: TextField(
                                key: const Key('userCode'),
                                controller: userCode,
                                enabled: state == ResetPasswordState.begin,
                                autofillHints: state == ResetPasswordState.begin
                                    ? const [AutofillHints.email]
                                    : null,
                                autocorrect: false,
                                enableSuggestions: false,
                                textInputAction: TextInputAction.next,
                                textCapitalization:
                                    TextCapitalization.characters,
                                inputFormatters: [
                                  UpperCaseTextFormatter(),
                                ],
                                onSubmitted: (_) => FocusScope.of(context)
                                    .requestFocus(phoneNode),
                                decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 16),
                                  prefixIcon: const Icon(
                                    Icons.person_outline,
                                    color: primaryColor,
                                  ),
                                  hintText: '請輸入經銷商編號',
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                        color: Colors.black,
                                        width: 0.5,
                                      ),
                                      borderRadius: BorderRadius.circular(15)),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                      color: Colors.black,
                                      width: 0.5,
                                    ),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                      color: Colors.black,
                                      width: 0.5,
                                    ),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(height: 20.0),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 16),
                              child: TextField(
                                key: const Key('phoneField'),
                                enabled: state == ResetPasswordState.begin,
                                focusNode: phoneNode,
                                controller: phone,
                                autofillHints: state == ResetPasswordState.begin
                                    ? const [AutofillHints.email]
                                    : null,
                                autocorrect: false,
                                enableSuggestions: false,
                                textInputAction: TextInputAction.next,
                                keyboardType: TextInputType.number,
                                onSubmitted: (_) => FocusScope.of(context)
                                    .requestFocus(codeNode),
                                decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 16),
                                  prefixIcon: const Icon(
                                    Icons.phone_iphone,
                                    color: primaryColor,
                                  ),
                                  hintText: '請輸入手機號碼',
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                        color: Colors.black,
                                        width: 0.5,
                                      ),
                                      borderRadius: BorderRadius.circular(15)),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                      color: Colors.black,
                                      width: 0.5,
                                    ),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                      color: Colors.black,
                                      width: 0.5,
                                    ),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                ),
                              ),
                            ),
                            if (state != ResetPasswordState.verified) ...[
                              const SizedBox(height: 20.0),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 16),
                                child: Material(
                                  color: value.otpCountDown == 0
                                      ? Theme.of(context).primaryColor
                                      : Theme.of(context)
                                          .primaryColor
                                          .withOpacity(0.5),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(15.0)),
                                  elevation: 0,
                                  child: MaterialButton(
                                    key: const Key('sendCode'),
                                    onPressed: value.otpCountDown > 0
                                        ? null
                                        : () async {
                                            if (value.smsLoading) return;
                                            value.setSMSLoading(true);
                                            var check = await checkUser();
                                            if (!check) {
                                              value.setSMSLoading(false);
                                              return;
                                            }
                                            var msg = await Services()
                                                .api
                                                .getOtp(phone: phone.text);
                                            value.setSMSLoading(false);
                                            if (msg == null || msg.isEmpty) {
                                              _snackBar('已發送驗證碼',
                                                  action: false);
                                              setState(() {
                                                verId = 'verificationId';
                                                verify = false;
                                                smsCode = '';
                                                state = ResetPasswordState
                                                    .phoneSend;
                                              });
                                              value.startTimer(
                                                () {},
                                              );
                                            } else {
                                              _snackBar(msg, action: false);
                                            }
                                            // FirebaseServices().verifyPhoneNumber(
                                            //   phoneNumber: '+886' +
                                            //       int.parse(phone.text).toString(),
                                            //   codeSent: (String verificationId,
                                            //       int? resendToken) async {
                                            //     value.setSMSLoading(false);
                                            //     debugPrint('codeSent');
                                            //     _snackBar('已發送驗證碼', action: false);
                                            //     setState(() {
                                            //       verId = verificationId;
                                            //       verify = false;
                                            //       smsCode = '';
                                            //       state = ResetPasswordState.phoneSend;
                                            //     });
                                            //     debugPrint(verId);
                                            //   },
                                            //   codeAutoRetrievalTimeout:
                                            //       (String verificationId) {
                                            //     value.setSMSLoading(false);
                                            //     debugPrint(
                                            //         'codeAutoRetrievalTimeout,verificationId = $verificationId');
                                            //   },
                                            //   verificationCompleted:
                                            //       (firebaseAuth.PhoneAuthCredential
                                            //           phoneAuthCredential) {
                                            //     value.setSMSLoading(false);
                                            //     setState(() {
                                            //       smsCode =
                                            //           phoneAuthCredential.smsCode ?? '';
                                            //     });
                                            //     debugPrint('verificationCompleted');
                                            //     // _snackBar('已發送驗證碼',action: false);
                                            //   },
                                            //   verificationFailed:
                                            //       (firebaseAuth.FirebaseAuthException
                                            //           error) {
                                            //     value.setSMSLoading(false);
                                            //     debugPrint('verificationFailed : $error');
                                            //     _snackBar('發送驗證碼失敗 : $error');
                                            //   },
                                            // );
                                          },
                                    elevation: 0.0,
                                    height: 42.0,
                                    child: SizedBox(
                                      width: double.maxFinite,
                                      child: Text(
                                        value.smsLoading
                                            ? '發送中...'
                                            : state != ResetPasswordState.begin
                                                ? '${value.otpCountDown > 0 ? '${value.otpCountDown}秒 ' : ''}重新發送驗證碼'
                                                : '${value.otpCountDown > 0 ? '${value.otpCountDown}秒 ' : ''}發送驗證碼',
                                        textAlign: TextAlign.center,
                                        style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                            const SizedBox(height: 20.0),
                            if (state != ResetPasswordState.begin) ...[
                              if (state == ResetPasswordState.phoneSend) ...[
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16),
                                  child: Row(
                                    children: [
                                      Expanded(
                                          child: TextField(
                                        key: const Key('codeField'),
                                        focusNode: codeNode,
                                        controller: code,
                                        autofillHints: const [
                                          AutofillHints.email
                                        ],
                                        autocorrect: false,
                                        enableSuggestions: false,
                                        textInputAction: TextInputAction.next,
                                        keyboardType: TextInputType.number,
                                        onSubmitted: (_) =>
                                            FocusScope.of(context).requestFocus(
                                                passwordFirstNode),
                                        decoration: InputDecoration(
                                          contentPadding:
                                              const EdgeInsets.symmetric(
                                                  vertical: 10, horizontal: 16),
                                          hintText: '請輸入手機驗證碼',
                                          focusedBorder: OutlineInputBorder(
                                              borderSide: const BorderSide(
                                                color: Colors.black,
                                                width: 0.5,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(15)),
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: const BorderSide(
                                              color: Colors.black,
                                              width: 0.5,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(15),
                                          ),
                                        ),
                                      )),
                                      const SizedBox(width: 8),
                                      Material(
                                        color: Theme.of(context).primaryColor,
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(15.0)),
                                        elevation: 0,
                                        child: MaterialButton(
                                          key: const Key('sendCode'),
                                          onPressed: verify
                                              ? null
                                              : () async {
                                                  if (code.text.isEmpty) {
                                                    _snackBar('請輸入驗證碼',
                                                        action: false);
                                                    return;
                                                  }
                                                  setState(() {
                                                    verifying = true;
                                                  });

                                                  var msg = await Services()
                                                      .api
                                                      .verifyOtp(
                                                        phone: phone.text,
                                                        code: code.text,
                                                      );

                                                  if (msg == null ||
                                                      msg.isEmpty) {
                                                    verify = true;
                                                    setState(() {
                                                      state = ResetPasswordState
                                                          .verified;
                                                      verifying = false;
                                                    });
                                                  } else {
                                                    _snackBar(msg,
                                                        action: false);
                                                  }
                                                  // try {
                                                  //   var auth = firebaseAuth
                                                  //       .FirebaseAuth.instance;
                                                  //   var credential = firebaseAuth
                                                  //           .PhoneAuthProvider
                                                  //       .credential(
                                                  //           verificationId: verId,
                                                  //           smsCode: code.text);
                                                  //   await auth.signInWithCredential(
                                                  //       credential);
                                                  //   smsCode = credential.smsCode!;
                                                  //   verify = true;
                                                  //   setState(() {
                                                  //     state = ResetPasswordState
                                                  //         .verified;
                                                  //     verifying = false;
                                                  //   });
                                                  // } catch (e) {
                                                  //   debugPrint(e.toString());
                                                  //   setState(() {
                                                  //     verifying = false;
                                                  //   });
                                                  //   _snackBar('驗證碼不符',
                                                  //       action: false);
                                                  //   return;
                                                  // }
                                                },
                                          elevation: 0.0,
                                          height: 42.0,
                                          child: Text(
                                            verifying
                                                ? '驗證中...'
                                                : state ==
                                                        ResetPasswordState
                                                            .phoneSend
                                                    ? '驗證'
                                                    : '已驗證',
                                            style: const TextStyle(
                                                color: Colors.white,
                                                fontSize: 14),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(height: 20),
                              ],
                              if (state == ResetPasswordState.verified) ...[
                                if (verId.isNotEmpty) ...[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16),
                                    child: TextField(
                                      key: const Key('loginPasswordFieldFirst'),
                                      autofillHints: const [
                                        AutofillHints.password
                                      ],
                                      obscureText: hideFirst,
                                      obscuringCharacter: '*',
                                      textInputAction: TextInputAction.next,
                                      keyboardType:
                                          TextInputType.visiblePassword,
                                      controller: passwordFirst,
                                      focusNode: passwordFirstNode,
                                      decoration: InputDecoration(
                                        contentPadding:
                                            const EdgeInsets.symmetric(
                                                vertical: 10, horizontal: 16),
                                        prefixIcon: const Icon(
                                          Icons.lock_outline,
                                          color: primaryColor,
                                        ),
                                        hintText: '請輸入您的新密碼',
                                        focusedBorder: OutlineInputBorder(
                                            borderSide: const BorderSide(
                                              color: Colors.black,
                                              width: 0.5,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(15)),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: const BorderSide(
                                            color: Colors.black,
                                            width: 0.5,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(15),
                                        ),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 20),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16),
                                    child: TextField(
                                      key:
                                          const Key('loginPasswordFieldSecond'),
                                      focusNode: passwordSecondNode,
                                      autofillHints: const [
                                        AutofillHints.password
                                      ],
                                      obscureText: hideSecond,
                                      obscuringCharacter: '*',
                                      textInputAction: TextInputAction.done,
                                      keyboardType:
                                          TextInputType.visiblePassword,
                                      controller: passwordSecond,
                                      decoration: InputDecoration(
                                        contentPadding:
                                            const EdgeInsets.symmetric(
                                                vertical: 10, horizontal: 16),
                                        prefixIcon: const Icon(
                                          Icons.lock_outline,
                                          color: primaryColor,
                                        ),
                                        suffixIcon: GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              if (hideSecond) {
                                                hideSecond = false;
                                              } else {
                                                hideSecond = true;
                                              }
                                            });
                                          },
                                          child: Icon(
                                            hideSecond
                                                ? Icons.visibility_off_outlined
                                                : Icons.visibility_outlined,
                                            color: Colors.grey,
                                          ),
                                        ),
                                        hintText: '再次輸入您的新密碼',
                                        focusedBorder: OutlineInputBorder(
                                            borderSide: const BorderSide(
                                              color: Colors.black,
                                              width: 0.5,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(15)),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: const BorderSide(
                                            color: Colors.black,
                                            width: 0.5,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(15),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                                const SizedBox(height: 20),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 16.0, horizontal: 16),
                                  child: Material(
                                    color: Theme.of(context).primaryColor,
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(50.0)),
                                    elevation: 0,
                                    child: MaterialButton(
                                      key: const Key('finishButton'),
                                      onPressed: () {
                                        if (verId.isEmpty) {
                                          _snackBar('請先完成手機驗證');
                                          return;
                                        }
                                        resetPassword();
                                      },
                                      minWidth: 400.0,
                                      elevation: 0.0,
                                      height: 42.0,
                                      child: resetting
                                          ? const Center(
                                              child: CircularProgressIndicator(
                                                color: Colors.white,
                                              ),
                                            )
                                          : const Text(
                                              '完成',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight:
                                                      FontWeight.normal),
                                            ),
                                    ),
                                  ),
                                ),
                              ],
                            ],
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 0.0, horizontal: 16),
                              child: Material(
                                color: Colors.black12,
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(50.0)),
                                elevation: 0,
                                child: MaterialButton(
                                  key: const Key('back'),
                                  onPressed: () {
                                    final canPop =
                                        ModalRoute.of(context)!.canPop;
                                    if (canPop) {
                                      Navigator.pop(context);
                                    } else {
                                      Navigator.of(context)
                                          .pushReplacementNamed(
                                              RouteList.login);
                                    }
                                  },
                                  minWidth: 400.0,
                                  elevation: 0.0,
                                  height: 42.0,
                                  child: const Text(
                                    '返回',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.normal),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(height: 20),
                          ],
                        ),
                      )),
                ),
              ),
            ),
          ),
        );
      }),
    );
  }

  void _snackBar(String text, {bool? action}) {
    if (mounted) {
      ScaffoldMessenger.of(context).removeCurrentSnackBar();
      final snackBar = SnackBar(
        content: Text(text),
        duration: Duration(seconds: action ?? true ? 10 : 4),
        action: action ?? true
            ? SnackBarAction(
                label: S.of(context).close,
                onPressed: () {
                  // Some code to undo the change.
                },
                textColor: Colors.white,
              )
            : null,
      );
      // ignore: deprecated_member_use
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  Future<bool> checkUser() async {
    if (userCode.text.isEmpty || phone.text.isEmpty) {
      _snackBar('請填寫經銷商編號與手機號碼');
      return false;
    }
    userId = await Services()
        .api
        .checkUser(userId: userCode.text, phone: phone.text);
    if (userId != null && userId!.isNotEmpty) {
      return true;
    }
    if (userId == null) {
      _snackBar('發生錯誤，請稍後再試');
      return false;
    }
    _snackBar('不存在的經銷商編號或手機號碼錯誤');
    return false;
  }

  Future<void> resetPassword() async {
    if (resetting) return;
    setState(() {
      resetting = true;
    });
    // if (verify) {
    //   if (code.text != smsCode) {
    //     _snackBar('驗證碼不符', action: false);
    //     setState(() {
    //       resetting = false;
    //     });
    //     return;
    //   }
    // } else {
    //   try {
    //     var auth = firebaseAuth.FirebaseAuth.instance;
    //     var credential = firebaseAuth.PhoneAuthProvider.credential(
    //         verificationId: verId, smsCode: code.text);
    //     await auth.signInWithCredential(credential);
    //     smsCode = credential.smsCode!;
    //     verify = true;
    //   } catch (e) {
    //     debugPrint(e.toString());
    //     _snackBar('驗證碼不符', action: false);
    //     setState(() {
    //       resetting = false;
    //     });
    //     return;
    //   }
    // }
    if (passwordFirst.text.isEmpty || passwordSecond.text.isEmpty) {
      _snackBar('請填寫密碼與確認密碼');
      setState(() {
        resetting = false;
      });
      return;
    }
    if (passwordFirst.text != passwordSecond.text) {
      _snackBar('密碼與確認密碼不符');
      setState(() {
        resetting = false;
      });
      return;
    }
    var check = await Services().api.resetPassword(
          userId: userId!,
          password: passwordFirst.text,
        );
    if (check == true) {
      ScaffoldMessenger.of(context).removeCurrentSnackBar();
      Navigator.pop(context, true);
      setState(() {
        resetting = false;
      });
      return;
    }
    if (check == null) {
      _snackBar('發生錯誤，請稍後再試');
      setState(() {
        resetting = false;
      });
      return;
    }
    _snackBar('密碼變更失敗，請稍後再試');
    setState(() {
      resetting = false;
    });
  }
}
