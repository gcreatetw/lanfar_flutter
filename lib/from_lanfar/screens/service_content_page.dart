import 'dart:async';

import 'package:chat_bubbles/bubbles/bubble_special_three.dart';
import 'package:flutter/material.dart';
import 'package:inspireui/inspireui.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../models/index.dart';
import '../model/get_service_record_response.dart';
import '../provider/service_content_page_provider.dart';
import '../widget/round_border_textfield.dart';

class ServiceContentPage extends StatefulWidget {
  final ServiceRecordBean model;

  const ServiceContentPage({
    Key? key,
    required this.model,
  }) : super(key: key);

  @override
  State<ServiceContentPage> createState() => _ServiceContentPageState();
}

class _ServiceContentPageState extends State<ServiceContentPage> {
  late ServiceContentPageProvider serviceContentPageProvider;

  Timer? timer;

  RefreshController refreshController = RefreshController();

  @override
  void initState() {
    serviceContentPageProvider = ServiceContentPageProvider(
      user: context.read<UserModel>().user!,
      onError: (e) {
        showSnack(e);
      },
      model: widget.model,
    );
    serviceContentPageProvider.fetch(true);
    timer = Timer.periodic(
      const Duration(seconds: 10),
      (timer) {
        serviceContentPageProvider.fetch(false);
      },
    );
    super.initState();
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: serviceContentPageProvider,
      child: Consumer(
        builder: (context, ServiceContentPageProvider provider, _) {
          return AutoHideKeyboard(
            child: Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                backgroundColor: Theme.of(context).backgroundColor,
                iconTheme: IconThemeData(
                    color: Theme.of(context).textTheme.headline6!.color),
                title: Text(
                  '客服紀錄',
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
              ),
              body: provider.loading
                  ? Center(
                      child: kLoadingWidget(context),
                    )
                  : Stack(
                      children: [
                        Column(
                          children: [
                            Expanded(
                              child: SmartRefresher(
                                controller: refreshController,
                                enablePullDown: true,
                                onRefresh: () async {
                                  await serviceContentPageProvider.fetch(false);
                                  refreshController.refreshCompleted();
                                },
                                header: const ClassicHeader(
                                  idleText: '下拉更新',
                                  releaseText: '放開更新',
                                  refreshingText: '更新中',
                                  completeText: '已更新',
                                ),
                                child: ListView(
                                  padding: const EdgeInsets.all(8),
                                  controller: serviceContentPageProvider.scrollController,
                                  children: [
                                    _title('日期：', provider.date),
                                    _title('諮詢類型：', provider.type),
                                    _title('諮詢內容：', provider.content),
                                    _title('狀態：', provider.status),
                                    const SizedBox(height: 8),
                                    Row(
                                      children: const [
                                        Icon(Icons.headset_mic),
                                        Text('客服回覆'),
                                      ],
                                    ),
                                    const SizedBox(height: 8),
                                    for (var element in provider.list)
                                      Column(
                                        crossAxisAlignment:
                                            element.isAdmin ?? false
                                                ? CrossAxisAlignment.start
                                                : CrossAxisAlignment.end,
                                        children: [
                                          BubbleSpecialThree(
                                            text: element.consultText ?? '',
                                            color: element.isAdmin ?? false
                                                ? primaryColor
                                                : const Color(0xFF8fbc8f),
                                            textStyle: const TextStyle(
                                                color: Colors.white),
                                            isSender:
                                                !(element.isAdmin ?? false),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 20),
                                            child: Text(
                                              element.dateCreated ?? '',
                                              style: const TextStyle(
                                                color: Colors.black54,
                                                fontSize: 12,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                  ],
                                ),
                              ),
                            ),
                            if (provider.status != '已結案')
                              Row(
                                children: [
                                  const SizedBox(width: 8),
                                  Expanded(
                                    child: RoundBorderTextField(
                                      controller: provider.messageController,
                                      hint: '內容',
                                      required: false,
                                      textColor: Colors.black54,
                                      onChange: (v) {
                                        setState(() {});
                                      },
                                    ),
                                  ),
                                  const SizedBox(width: 8),
                                  ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      primary: const Color(0xFF8fbc8f),
                                      elevation: 0,
                                    ),
                                    onPressed: !provider.canSend
                                        ? null
                                        : () {
                                            provider.send();
                                          },
                                    child: const Text(
                                      '回覆',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                  const SizedBox(width: 8),
                                ],
                              ),
                            SizedBox(
                              height: MediaQuery.of(context).padding.bottom + 8,
                            )
                          ],
                        ),
                        if (provider.sending)
                          Container(
                            color: Colors.black54,
                            width: double.maxFinite,
                            height: double.maxFinite,
                            child: Center(
                              child: kLoadingWidget(context),
                            ),
                          ),
                      ],
                    ),
            ),
          );
        },
      ),
    );
  }

  Widget _title(String title, String content) {
    return Column(
      children: [
        Row(
          children: [
            Text(
              title,
              style: const TextStyle(
                  color: primaryColor, fontWeight: FontWeight.bold),
            ),
            Expanded(
              child: Text(
                content,
                style: const TextStyle(color: Colors.black54),
              ),
            ),
          ],
        ),
        const Divider(),
      ],
    );
  }

  void showSnack(String content) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(content),
      ),
    );
  }
}
