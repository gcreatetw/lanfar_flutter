import 'package:flutter/material.dart';
import 'package:lanfar/from_lanfar/screens/bio_auth_page.dart';
import 'package:provider/provider.dart';

import '../../app.dart';
import '../../common/theme/colors.dart';
import '../../common/tools/navigate_tools.dart';
import '../../models/app_model.dart';
import '../../modules/dynamic_layout/config/app_config.dart';
import '../../modules/dynamic_layout/index.dart';
import '../../routes/flux_navigate.dart';
import '../contants/constants.dart';
import 'bonus_page.dart';
import 'get_monarch_point_page.dart';
import 'organization_performance_page.dart';
import 'performance_overview_page.dart';
import 'qrcode_page.dart';
import 'triumph_query_page.dart';
import 'unreport_performance_page.dart';
import 'webview_with_cookie.dart';

class PerformancePage extends StatefulWidget {
  const PerformancePage({Key? key}) : super(key: key);

  @override
  _PerformancePageState createState() => _PerformancePageState();
}

class _PerformancePageState extends State<PerformancePage> {
  AppBarConfig? get appBar =>
      Provider.of<AppModel>(context, listen: false).appConfig!.appBar;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Theme.of(context).backgroundColor,
        iconTheme: IconThemeData(
          color: Theme.of(context).textTheme.headline6!.color,
        ),
        title: Text(
          '業績查詢',
          style: Theme.of(context)
              .textTheme
              .headline6!
              .copyWith(fontWeight: FontWeight.w700),
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.qr_code_2,
              color: Theme.of(context).textTheme.headline6!.color,
            ),
            onPressed: () => FluxNavigate.push(
              MaterialPageRoute(
                builder: (context) => const QRCodePage(),
              ),
            ),
          ),
          IconButton(
            icon: Icon(
              Icons.menu,
              color: Theme.of(context).textTheme.headline6!.color,
            ),
            onPressed: () => NavigateTools.onTapOpenDrawerMenu(context),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 20),
            const Divider(
              color: Colors.black12,
              height: 1.0,
              indent: 75,
              //endIndent: 20,
            ),
            for (int i = 0; i < Constants.performanceTitle.length; i++) ...[
              Column(
                children: [
                  Card(
                    margin: const EdgeInsets.only(bottom: 2.0),
                    elevation: 0,
                    child: ListTile(
                      leading: Icon(
                        Constants.performanceIcon[i],
                        color: Theme.of(context).colorScheme.secondary,
                        size: 24,
                      ),
                      title: Text(
                        Constants.performanceTitle[i],
                        style: const TextStyle(fontSize: 16),
                      ),
                      trailing: const Icon(
                        Icons.arrow_forward_ios,
                        size: 18,
                        color: kGrey600,
                      ),
                      onTap: () {
                        switch (i) {
                          case 0:
                            Navigator.of(
                                    App.fluxStoreNavigatorKey.currentContext!)
                                .push(
                              MaterialPageRoute(
                                builder: (context) =>
                                    const PerformanceOverviewPage(),
                              ),
                            );
                            // openWebView(
                            //     '${environment['serverConfig']['url']}/my-accounts/member-relations/');
                            break;
                          case 1:
                            Navigator.of(
                                    App.fluxStoreNavigatorKey.currentContext!)
                                .push(
                              MaterialPageRoute(
                                builder: (context) =>
                                    const UnReportPerformancePage(),
                              ),
                            );
                            // openWebView(
                            //     '${environment['serverConfig']['url']}/my-accounts/purchasing-order/');
                            break;
                          case 2:
                            Navigator.of(
                                    App.fluxStoreNavigatorKey.currentContext!)
                                .push(
                              MaterialPageRoute(
                                builder: (context) =>
                                    BioAuthPage(onAuthFinish: () {
                                  Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                      builder: (context) => const BonusPage(),
                                    ),
                                  );
                                }),
                              ),
                            );
                            // openWebView(
                            //     '${environment['serverConfig']['url']}/my-accounts/bonus-info/');
                            break;
                          case 3:
                            Navigator.of(
                                    App.fluxStoreNavigatorKey.currentContext!)
                                .push(
                              MaterialPageRoute(
                                builder: (context) =>
                                    const GetMonarchPointPage(),
                              ),
                            );
                            // openWebView(
                            //     '${environment['serverConfig']['url']}/my-accounts/year-performance/');
                            break;
                          case 4:
                            Navigator.of(
                                    App.fluxStoreNavigatorKey.currentContext!)
                                .push(
                              MaterialPageRoute(
                                builder: (context) => const TriumphQueryPage(),
                              ),
                            );
                            // openWebView(
                            //     '${environment['serverConfig']['url']}/my-accounts/estimate-performance/');
                            break;
                          case 5:
                            Navigator.of(
                                    App.fluxStoreNavigatorKey.currentContext!)
                                .push(
                              MaterialPageRoute(
                                builder: (context) =>
                                    const OrganizationPerformancePage(),
                              ),
                            );
                            // openWebView(
                            //     '${environment['serverConfig']['url']}/my-accounts/organization-relations/');
                            break;
                        }
                      },
                    ),
                  ),
                  const Divider(
                    color: Colors.black12,
                    height: 1.0,
                    indent: 75,
                    //endIndent: 20,
                  ),
                ],
              ),
              // SizedBox(
              //   width: double.maxFinite,
              //   child: Padding(
              //     padding: const EdgeInsets.symmetric(vertical: 8),
              //     child: Row(
              //       crossAxisAlignment: CrossAxisAlignment.center,
              //       children: [
              //         Icon(Constants.performanceIcon[i],color: null,),
              //         const SizedBox(width: 13),
              //         Expanded(child: Text(
              //           Constants.performanceTitle[i],
              //           style: Theme.of(context).textTheme.subtitle2,
              //         ),),
              //         const Icon(Icons.chevron_right_outlined)
              //       ],
              //     ),
              //   ),
              // ),
              // if (i != Constants.performanceTitle.length - 1)
              //   Container(
              //     color: Theme.of(context).dividerColor,
              //     height: 1,
              //     width: double.maxFinite,
              //   ),
            ],
          ],
        ),
      ),
    );
  }
}
