import 'package:flutter/material.dart';
import 'package:inspireui/widgets/skeleton_widget/skeleton_widget.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../provider/article_list_page_provider.dart';
import 'article_content_page.dart';

class ArticleListPage extends StatefulWidget {
  final bool isNews;

  const ArticleListPage({
    Key? key,
    this.isNews = false,
  }) : super(key: key);

  @override
  State<ArticleListPage> createState() => _ArticleListPageState();
}

class _ArticleListPageState extends State<ArticleListPage> {
  ArticleListPageProvider articleListPageProvider = ArticleListPageProvider();

  @override
  void initState() {
    articleListPageProvider.isNews = widget.isNews;
    articleListPageProvider.fetch(
      onError: () {
        showError();
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: articleListPageProvider,
      child: Consumer<ArticleListPageProvider>(
        builder: (context, ArticleListPageProvider provider, _) {
          return Scaffold(
            appBar: AppBar(
              backgroundColor: Theme.of(context).backgroundColor,
              iconTheme: IconThemeData(
                  color: Theme.of(context).textTheme.headline6!.color),
              title: Text(
                widget.isNews ? '最新消息' : '文宣影片',
                style: TextStyle(
                  color: Theme.of(context).colorScheme.secondary,
                ),
              ),
              actions: provider.loading || widget.isNews
                  ? null
                  : [
                      PopupMenuButton<int>(
                        icon: const Icon(Icons.list),
                        offset: const Offset(0, 50),
                        onSelected: (id) {
                          provider.fetchArticles(
                            id: id,
                            taxonomy: ArticleType.cat,
                            page: 1,
                            onError: () {
                              showError();
                            },
                          );
                        },
                        itemBuilder: (context) => provider.cats
                            .map(
                              (e) => PopupMenuItem<int>(
                                value: e.id,
                                padding: EdgeInsets.zero,
                                child: Container(
                                  padding: const EdgeInsets.all(16),
                                  color: provider.currentCatId == e.id
                                      ? primaryColor
                                      : Colors.white,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        e.name,
                                        style: TextStyle(
                                          color: provider.currentCatId == e.id
                                              ? Colors.white
                                              : null,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )
                            .toList(),
                      ),
                    ],
            ),
            body: provider.loading
                ? kLoadingWidget(context)
                : ListView.separated(
                    itemBuilder: (context, index) {
                      if (index == provider.articles.length) {
                        return _pageIndicator(provider);
                      }
                      if (index == provider.articles.length + 1) {
                        if (widget.isNews) {
                          return const SizedBox();
                        }
                        return _tagList(provider);
                      }
                      return _articleTitle(provider.articles[index]);
                    },
                    separatorBuilder: (context, index) {
                      return const SizedBox(height: 8);
                    },
                    itemCount: provider.articles.length + 2,
                  ),
          );
        },
      ),
    );
  }

  Widget _articleTitle(ArticleModel model) {
    return GestureDetector(
      onTap: () {
        if (model.lock) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('您沒有權限閱讀此篇文章'),
            ),
          );
          return;
        }
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => ArticleContentPage(id: model.id),
          ),
        );
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Card(
          clipBehavior: Clip.hardEdge,
          child: Column(
            children: [
              Image.network(
                model.image,
                width: double.maxFinite,
                filterQuality: FilterQuality.low,
                loadingBuilder: (context, child, event) {
                  if (event == null) return child;
                  return Skeleton(
                    width: double.maxFinite,
                    height: MediaQuery.of(context).size.width - 16,
                  );
                },
                errorBuilder: (context, _, __) {
                  return SizedBox(
                    width: double.maxFinite,
                    height: MediaQuery.of(context).size.width - 16,
                  );
                },
              ),
              Padding(
                padding: const EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        const Icon(
                          Icons.calendar_today,
                          color: primaryColor,
                        ),
                        const SizedBox(width: 4),
                        Text(
                          model.date != null
                              ? DateFormat('yyyy-MM-dd').format(model.date!)
                              : '',
                          style: const TextStyle(color: primaryColor),
                        ),
                      ],
                    ),
                    const SizedBox(height: 8),
                    Row(
                      children: [
                        if (model.lock)
                          const Icon(
                            Icons.lock,
                            color: primaryColor,
                          ),
                        Expanded(
                          child: Text(
                            model.title,
                            style: const TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              if (model.tags.isNotEmpty) ...[
                const Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    const SizedBox(width: 16),
                    const Icon(
                      Icons.local_offer,
                      color: primaryColor,
                    ),
                    const SizedBox(width: 4),
                    Expanded(
                      child: Wrap(
                        spacing: 8,
                        children: model.tags
                            .map(
                              (e) => GestureDetector(
                                onTap: () {},
                                child: Text(e.name),
                              ),
                            )
                            .toList(),
                      ),
                    ),
                    const SizedBox(width: 16),
                  ],
                ),
              ],
              const SizedBox(height: 8),
            ],
          ),
        ),
      ),
    );
  }

  Widget _pageIndicator(ArticleListPageProvider provider) {
    var showDot = true;
    var totalPage = provider.totalPage;
    var endPage =
        ((totalPage > 5) ? 4 + provider.pageStart : totalPage).toInt();

    if (endPage >= totalPage) {
      showDot = false;
      endPage = totalPage;
      provider.pageStart = totalPage - 4;
    }
    if (provider.pageStart < 1) {
      provider.pageStart = 1;
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        for (int i = provider.pageStart; i <= endPage; i++)
          GestureDetector(
            onTap: () {
              setState(() {
                provider.currentPage = i;
                provider.pageStart =
                    provider.currentPage - 2 < 1 ? 1 : provider.currentPage - 2;
              });
              provider.fetchArticles(
                id: provider.currentCatId ?? (provider.currentTagId ?? 0),
                taxonomy: provider.type,
                page: i,
                onError: () {
                  showError();
                },
              );
            },
            child: Container(
              margin: const EdgeInsets.all(4),
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: provider.currentPage == i ? primaryColor : null,
              ),
              child: Text(
                i.toString(),
                style: TextStyle(
                    color: provider.currentPage == i
                        ? Colors.white
                        : primaryColor),
              ),
            ),
          ),
        if (showDot)
          const Text(
            '...',
            style: TextStyle(color: primaryColor),
          )
      ],
    );
  }

  Widget _tagList(ArticleListPageProvider provider) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            '熱門標籤',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 16),
          Wrap(
            spacing: 8,
            runSpacing: 8,
            children: provider.tags.map((e) => _tagTitle(provider, e)).toList(),
          )
        ],
      ),
    );
  }

  Widget _tagTitle(ArticleListPageProvider provider, ArticleCatModel model) {
    var chose = provider.currentTagId == model.id;
    return GestureDetector(
      onTap: () {
        if (chose) return;
        provider.fetchArticles(
          id: model.id,
          taxonomy: ArticleType.tag,
          page: 1,
          onError: () {
            showError();
          },
        );
      },
      child: Container(
        decoration: BoxDecoration(
          color: chose ? primaryColor : Colors.white,
          border: Border.all(color: primaryColor),
        ),
        padding: const EdgeInsets.all(8),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(
              Icons.local_offer,
              color: chose ? Colors.white : primaryColor,
            ),
            Text(
              model.name,
              style: TextStyle(
                color: chose ? Colors.white : primaryColor,
              ),
            )
          ],
        ),
      ),
    );
  }

  void showError() {
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text('發生錯誤，請稍後再試'),
      ),
    );
  }
}
