import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:inspireui/widgets/auto_hide_keyboard.dart';
import 'package:provider/provider.dart';

import '../../common/config.dart';
import '../../common/tools.dart';
import '../../models/index.dart';
import '../provider/performance_overview_page_provider.dart';
import '../widget/performance_treeview.dart';
import '../widget/top_title.dart';

class PerformanceOverviewPage extends StatefulWidget {
  const PerformanceOverviewPage({Key? key}) : super(key: key);

  @override
  State<PerformanceOverviewPage> createState() =>
      _PerformanceOverviewPageState();
}

class _PerformanceOverviewPageState extends State<PerformanceOverviewPage> {
  late PerformanceOverviewPageProvider performanceOverviewPageProvider;

  @override
  void initState() {
    performanceOverviewPageProvider = PerformanceOverviewPageProvider(
      user: context.read<UserModel>().user!,
      onError: (e) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(e),
          ),
        );
      },
    );
    performanceOverviewPageProvider.fetch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        await SystemChrome.setPreferredOrientations([
          DeviceOrientation.portraitUp,
          DeviceOrientation.portraitDown,
        ]);
        return true;
      },
      child: ChangeNotifierProvider.value(
        value: performanceOverviewPageProvider,
        child: Consumer<PerformanceOverviewPageProvider>(
          builder: (context, provider, _) {
            return AutoHideKeyboard(
              child: Scaffold(
                backgroundColor: Colors.white,
                appBar: AppBar(
                  backgroundColor: Theme.of(context).backgroundColor,
                  iconTheme: IconThemeData(
                    color: Theme.of(context).textTheme.headline6!.color,
                  ),
                  title: Text(
                    '業績概況',
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.secondary,
                    ),
                  ),
                  actions: [
                    IconButton(
                      onPressed: () {
                        if (MediaQuery.of(context).orientation ==
                            Orientation.portrait) {
                          SystemChrome.setPreferredOrientations([
                            DeviceOrientation.landscapeLeft,
                            DeviceOrientation.landscapeRight,
                          ]);
                        } else {
                          SystemChrome.setPreferredOrientations([
                            DeviceOrientation.portraitUp,
                            DeviceOrientation.portraitDown,
                          ]);
                        }
                      },
                      icon: const Icon(Icons.screen_rotation),
                    )
                  ],
                ),
                body: InteractiveViewer(
                  maxScale: 4,
                  minScale: 1,
                  child: ListView(
                    padding: const EdgeInsets.all(8),
                    children: [
                      TopTitle(
                        title: 'pv月份',
                        action: DropdownButton<String>(
                          value: provider.chooseDate,
                          underline: const SizedBox(),
                          dropdownColor: Colors.white,
                          onChanged: provider.loading
                              ? null
                              : (value) {
                                  provider.setDate(value ?? '');
                                },
                          items: [
                            for (var element in provider.dateList)
                              DropdownMenuItem(
                                value: element,
                                child: Text(element),
                              )
                          ],
                        ),
                      ),
                      const Divider(),
                      TopTitle(
                        title: '上個月個人業績',
                        action: Text(PriceTools.getCurrencyFormatted(
                              provider.personalPerformance.toString(),
                              null,
                              isPV: true,
                            ) ??
                            ''),
                      ),
                      const Divider(),
                      TopTitle(
                        title: '上個月整組業績',
                        action: Text(PriceTools.getCurrencyFormatted(
                              provider.organizationPerformance.toString(),
                              null,
                              isPV: true,
                            ) ??
                            ''),
                      ),
                      const SizedBox(height: 16),
                      if (provider.loading) ...[
                        const SizedBox(height: 150),
                        kLoadingWidget(context),
                      ] else if (provider.hasData)
                        PerformanceTreeView(
                          data: provider.dataList,
                          tableData: provider.tableListFiltered,
                          onSearch: provider.search,
                        )
                      else ...[
                        const SizedBox(height: 150),
                        const Center(
                          child: Text('目前沒有資料'),
                        ),
                      ],
                      const SizedBox(height: 50),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
