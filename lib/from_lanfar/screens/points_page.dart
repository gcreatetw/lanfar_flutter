import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../models/user_model.dart';
import '../../services/index.dart';
import '../model/point_history_response.dart';
import '../provider/points_page_provider.dart';

class PointsPage extends StatefulWidget {
  const PointsPage({Key? key}) : super(key: key);

  @override
  State<PointsPage> createState() => _PointsPageState();
}

class _PointsPageState extends State<PointsPage> with TickerProviderStateMixin {
  late TabController tabController;

  PointsPageProvider pointsPageProvider = PointsPageProvider();

  @override
  void initState() {
    tabController = TabController(length: 2, vsync: this);
    pointsPageProvider.fetchHistory(context.read<UserModel>().user?.cookie ?? '');
    pointsPageProvider.fetchExchanged(context.read<UserModel>().user?.cookie ?? '');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: ChangeNotifierProvider.value(
        value: pointsPageProvider,
        child: Consumer<PointsPageProvider>(
          builder: (context, provider, _) {
            return WillPopScope(
              onWillPop: () async {
                context.read<UserModel>().getUser();
                return true;
              },
              child: Scaffold(
                backgroundColor: Colors.white,
                appBar: AppBar(
                  backgroundColor: Colors.white,
                  title: Text(
                    '點數查詢',
                    style: TextStyle(
                      color: Theme.of(context).colorScheme.secondary,
                    ),
                  ),
                ),
                body: provider.loading
                    ? kLoadingWidget(context)
                    : Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                width: 50,
                                height: 40,
                                child: Image.asset(
                                  'assets/images/home_icon06@2x.png',
                                  width: 40,
                                ),
                              ),
                              Text(
                                provider.point,
                                style: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 40,
                                ),
                              )
                            ],
                          ),
                          Text(
                            '有效期限 : ${provider.expireDate}',
                            style: const TextStyle(fontSize: 14),
                          ),
                          TabBar(
                            controller: tabController,
                            indicatorColor: Theme.of(context).primaryColor,
                            labelStyle: const TextStyle(fontSize: 18),
                            unselectedLabelStyle: const TextStyle(fontSize: 18),
                            tabs: const [
                              Tab(
                                text: '獲得紀錄',
                              ),
                              Tab(
                                text: '點數兌換',
                              ),
                            ],
                          ),
                          Expanded(
                            child: SafeArea(
                              child: TabBarView(
                                controller: tabController,
                                children: [
                                  pointsRecord(provider),
                                  pointsExchange(provider,context),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget pointsRecord(PointsPageProvider provider) {
    return ListView.separated(
      padding: const EdgeInsets.all(16),
      itemBuilder: (context, index) {
        return pointRecordTitle(provider.earningList[index]);
      },
      separatorBuilder: (context, index) {
        return const Divider();
      },
      itemCount: provider.earningList.length,
    );
  }

  Widget pointRecordTitle(Earnings earnings) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          earnings.dateEarning ?? '',
          style: const TextStyle(color: Colors.black54),
        ),
        const SizedBox(height: 8),
        Text(
          earnings.description ?? '',
          style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        const SizedBox(height: 8),
        Text(
          earnings.amount ?? '',
          style: TextStyle(
              color: (earnings.amount ?? '').startsWith('-')
                  ? Colors.red
                  : Colors.green,
              fontSize: 18,
              fontWeight: FontWeight.bold),
        ),
      ],
    );
  }

  Widget pointsExchange(PointsPageProvider provider,BuildContext parentContext) {
    if(provider.exchangedLoading){
      return kLoadingWidget(context);
    }
    return ListView.separated(
      padding: const EdgeInsets.all(16),
      itemBuilder: (context, index) {
        if (index == 0) {
          return Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.black12),
                borderRadius: BorderRadius.circular(5)),
            padding: const EdgeInsets.all(8),
            margin: const EdgeInsets.only(bottom: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  '兌換優惠券代碼中的積分',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                const SizedBox(height: 8),
                const Text(
                  '你想分享你的觀點嗎？創建優惠券代碼並分享它以便使用',
                  style: TextStyle(color: Colors.black54),
                ),
                const SizedBox(height: 8),
                const Text(
                  '您的優惠券：',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                const SizedBox(height: 8),
                Row(
                  children: [
                    const Text(
                      '兌換',
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(width: 8),
                    Expanded(
                        child: TextField(
                      controller: provider.pointController,
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                              color: Colors.black54,
                              width: 0.5,
                            ),
                            borderRadius: BorderRadius.circular(5)),
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                            color: Colors.black54,
                            width: 0.5,
                          ),
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                    )),
                    const SizedBox(width: 8),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(elevation: 0),
                      onPressed: provider.exchanging
                          ? null
                          : () {
                              FocusScope.of(context).unfocus();
                              if (int.parse(provider.pointController.text) >
                                  int.parse(provider.point)) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                    content: Text('您的點數不足'),
                                  ),
                                );
                                return;
                              }
                              provider.exchange(
                                context.read<UserModel>().user?.cookie ?? '',
                                onError: (text) {
                                  ScaffoldMessenger.of(parentContext).showSnackBar(
                                    SnackBar(
                                      content: Text(text),
                                    ),
                                  );
                                },
                                onSuccess: (text) {
                                  Services().api.getUserInfo(
                                      parentContext.read<UserModel>().user?.cookie ??
                                          '');
                                  ScaffoldMessenger.of(parentContext).showSnackBar(
                                    SnackBar(
                                      content: Text(text),
                                    ),
                                  );
                                },
                              );
                            },
                      child: Text(
                        provider.exchanging ? '兌換中' : '兌換',
                        style: const TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                )
              ],
            ),
          );
        }
        return pointExchangeTitle(provider.exchangeList[index - 1]);
      },
      separatorBuilder: (context, index) {
        if (index == 0) {
          return const SizedBox();
        }
        return const Divider();
      },
      itemCount: provider.exchangeList.length + 1,
    );
  }

  Widget pointExchangeTitle(Exchanges exchanges) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '過期日：' +
              (exchanges.expireDate == null || exchanges.expireDate!.isEmpty
                  ? '無'
                  : exchanges.expireDate!),
          style: const TextStyle(color: Colors.black54),
        ),
        const SizedBox(height: 8),
        Text(
          '點數兌換「${exchanges.code}」',
          style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        const SizedBox(height: 8),
        Text(
          (exchanges.status ?? '') +
              (exchanges.usedOrderId != ''
                  ? ' (${exchanges.usedDisplayName},${exchanges.usedUsername},訂單編號：${exchanges.usedOrderId})'
                  : ''),
          style: const TextStyle(
              color: Colors.black54, fontSize: 18, fontWeight: FontWeight.bold),
        ),
        if ((exchanges.usedOrderId == null || exchanges.usedOrderId!.isEmpty) &&  exchanges.status != '已過期')
          Row(
            children: [
              const Spacer(),
              InkWell(
                onTap: () {
                  Clipboard.setData(
                    ClipboardData(text: exchanges.code ?? ''),
                  ).then(
                    (value) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          content: Text('已複製'),
                        ),
                      );
                    },
                  );
                },
                child: Row(
                  children: const [
                    Icon(
                      Icons.copy,
                      color: primaryColor,
                    ),
                    Text(
                      '複製',
                      style: TextStyle(color: primaryColor),
                    )
                  ],
                ),
              ),
              const SizedBox(width: 8),
              InkWell(
                onTap: () {
                  Share.share(
                      '連法國際  先生/小姐 參加活動獲得「贈品」一份，並將贈品優惠碼「${exchanges.code ?? ''}」資格分享給您，請您於官網/APP/櫃台進行兌換~!');
                },
                child: Row(
                  children: const [
                    Icon(
                      Icons.ios_share,
                      color: primaryColor,
                    ),
                    Text(
                      '分享',
                      style: TextStyle(color: primaryColor),
                    )
                  ],
                ),
              )
            ],
          )
      ],
    );
  }
}
