import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lanfar/common/theme/colors.dart';
import 'package:lanfar/from_lanfar/model/count_down_data.dart';
import 'package:lanfar/models/user_model.dart';
import 'package:provider/provider.dart';
import 'package:provider/src/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QrCodeDialog extends StatefulWidget {
  const QrCodeDialog({Key? key}) : super(key: key);

  @override
  State<QrCodeDialog> createState() => _QrCodeDialogState();
}

class _QrCodeDialogState extends State<QrCodeDialog> {
  UserModel get _user => context.read<UserModel>();

  var counter = CountDownData();

  QrImage qrImage = QrImage(data: '');

  @override
  void initState() {
    counter.countDown(setQrImage);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: counter,
      child: Container(
        color: Colors.black54,
        width: double.maxFinite,
        height: double.maxFinite,
        padding: const EdgeInsets.all(50),
        child: Center(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
            ),
            padding: const EdgeInsets.symmetric(
              horizontal: 32,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(height: 16),
                const Text(
                  '網路連線異常',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 16),
                const Text(
                  '會員QRCode',
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                qrImage,
                const SizedBox(height: 8),
                Consumer<CountDownData>(
                  builder: (context, data, _) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            setQrImage();
                            data.timer!.cancel();
                            data.countDown(setQrImage);
                          },
                          child: Row(
                            children: const [
                              Icon(
                                Icons.autorenew,
                                size: 20,
                                color: primaryColor,
                              ),
                              Text(
                                '重整',
                                style: TextStyle(color: primaryColor),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(width: 16),
                        Text(
                          '倒數${data.countDownValue}秒',
                          style: const TextStyle(color: Colors.black),
                        ),
                      ],
                    );
                  },
                ),
                const SizedBox(height: 32),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void setQrImage() {
    if (mounted) {
      setState(() {
        qrImage = QrImage(
          data:
              '${_user.user?.username}|${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}',
        );
      });
    }
  }
}
