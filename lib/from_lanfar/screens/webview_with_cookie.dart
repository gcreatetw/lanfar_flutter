import 'dart:convert';
import 'dart:io';

import 'package:crypto/crypto.dart' as crypto;
import 'package:flutter/material.dart';

// import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_android/webview_flutter_android.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools/tools.dart';
import '../../env.dart';
import '../../generated/l10n.dart';
import '../../menu/maintab_delegate.dart';
import '../../models/cart/cart_base.dart';
import '../../models/user_model.dart';
import '../../routes/flux_navigate.dart';
import '../../screens/checkout/checkout_screen.dart';
import '../../screens/checkout/purchase_method_screen.dart';
import '../../services/services.dart';
import '../../widgets/common/dialogs.dart';

class WebViewWithCookie extends StatefulWidget {
  final String url;
  final bool multiPram;
  final String end;
  final bool notDirectPop;
  final bool alwaysAddAppStyle;
  final String? title;
  final bool jumpOutWhenCheckOut;

  const WebViewWithCookie({
    Key? key,
    required this.url,
    this.multiPram = false,
    this.end = '',
    this.notDirectPop = false,
    this.alwaysAddAppStyle = false,
    this.title,
    this.jumpOutWhenCheckOut = true,
  }) : super(key: key);

  @override
  _WebViewWithCookieState createState() => _WebViewWithCookieState();
}

class _WebViewWithCookieState extends State<WebViewWithCookie> {
  int selectedIndex = 1;

  late WebViewController webViewController;

  // late InAppWebViewController inAppWebViewController;

  double webWidth = 0;

  // InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
  //   crossPlatform: InAppWebViewOptions(
  //     useShouldOverrideUrlLoading: true,
  //     mediaPlaybackRequiresUserGesture: false,
  //   ),
  //   android: AndroidInAppWebViewOptions(
  //     useShouldInterceptRequest: true,
  //     useHybridComposition: true,
  //   ),
  //   ios: IOSInAppWebViewOptions(
  //     allowsInlineMediaPlayback: true,
  //     useOnNavigationResponse: true,
  //   ),
  // );

  @override
  void initState() {
    var user = Provider.of<UserModel>(context, listen: false);
    final md5 = crypto.md5
        .convert(utf8
            .encode('gcreate${DateFormat('yyyyMMdd').format(DateTime.now())}'))
        .toString();
    late final PlatformWebViewControllerCreationParams params;
    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(
        allowsInlineMediaPlayback: true,
        mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{
          PlaybackMediaTypes.audio,
          PlaybackMediaTypes.video,
        },
      );
    } else {
      params = PlatformWebViewControllerCreationParams();
    }
    webViewController = WebViewController.fromPlatformCreationParams(params)
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(Colors.white)
      ..setNavigationDelegate(
        NavigationDelegate(
          onWebResourceError: (e) async {
            if (e.errorCode == -1009) {
              await showDialogNotInternet(context);
              Navigator.pop(context);
            }
          },
          onNavigationRequest: (request) async {
            if (request.url.endsWith('pdf')) {
              await launch(request.url, forceSafariVC: false);
              setState(() {
                selectedIndex = 0;
              });
              return NavigationDecision.prevent;
            }

            ///回所有活動頁
            if (request.url ==
                '${environment['serverConfig']['url']}/events/') {
              Navigator.pop(context); // Close current window
              return NavigationDecision.navigate;
            }

            ///開啟google日曆
            if (request.url.contains('calendar.google.com')) {
              await launch(request.url, forceSafariVC: false);
              setState(() {
                selectedIndex = 0;
              });
              return NavigationDecision.prevent;
            }

            ///文宣影片文章
            if (widget.alwaysAddAppStyle && !request.url.contains('appstyle')) {
              await webViewController.loadRequest(
                Uri.parse(
                  request.url +
                      (request.url.endsWith('/')
                          ? '?appstyle=1'
                          : '&appstyle=1'),
                ),
              );
            }
            return NavigationDecision.navigate;
          },
          onProgress: (progress) {
            if (progress == 100) {
              setState(() {
                selectedIndex = 0;
              });
            } else {
              setState(() {
                selectedIndex = 1;
              });
            }
          },
          onPageStarted: (url) {
            print(url);
            // if (url.contains('https://lanfar2.31app.tw') &&
            //     !url.contains('appstyle=1')) {
            //   webViewController.loadUrl(url + '?appstyle=1');
            // }
            if (url.contains('cart/?order_again')) {
              Navigator.of(context)
                  .popUntil(ModalRoute.withName(RouteList.dashboard));
              MainTabControlDelegate.getInstance()
                  .changeTab(RouteList.cart.replaceFirst('/', ''));
            }
          },
          onPageFinished: (url) async {
            // print(url);
            // if(url.contains('cart/')){
            //   Navigator.pop(context,'cart');
            // }
            if (url == 'https://www.google.com') {
              setState(() {
                selectedIndex = 1;
              });
              return;
            }

            if (url.contains('checkout-2') && widget.jumpOutWhenCheckOut) {
              setState(() {
                selectedIndex = 1;
              });

              final cartModel = context.read<CartModel>();
              await Services()
                  .widget
                  .syncCartFromWebsite(user.user?.cookie, cartModel, context);
              cartModel.purchaseMethod = PurchaseType.general;
              cartModel.fastPromote = false;
              Navigator.pop(context);
              await doCheckout();
              // Navigator.of(context).popUntil(
              //     ModalRoute.withName(RouteList.dashboard));
              // MainTabControlDelegate.getInstance().changeTab(
              //     RouteList.cart.replaceFirst('/', ''));
              return;
            }
            setState(() {
              selectedIndex = 0;
            });

            if (!url.contains('my-accounts/ctcb-credit')) {
              setState(() {
                webWidth = MediaQuery.of(context).size.width;
              });
            } else {
              webWidth = MediaQuery.of(context).size.width;
            }
            // _webViewController.evaluateJavascript('''
            // var email = document.getElementById("username");
            //       var password = document.getElementById("password");
            //       email.value = "${user.user!.username}";
            //       password.value = "${user.user!.password}";
            //
            //       document.getElementsByClassName('woocommerce-button button woocommerce-form-login__submit')[0].click();
            //     ''');
          },
        ),
      )
      ..loadRequest(
          Uri.parse(
              '${widget.url}${widget.multiPram ? '' : '?'}cookie=${EncodeUtils.encodeCookie(user.user!.cookie!)}&user_login=${context.read<UserModel>().user?.username ?? ''}&gc_nonce=$md5&appstyle=1${widget.end}'),
          headers: {'HTTP_SOURCE': '1'});
    if (webViewController.platform is WebKitWebViewController) {
      (webViewController.platform as WebKitWebViewController)
          .setAllowsBackForwardNavigationGestures(true);
    }
    Future.delayed(Duration.zero, () {
      // CookieManager().clearCookies();
      webWidth = MediaQuery.of(context).size.width +
          (widget.url.contains('my-accounts/ctcb-credit') ? 25 : 0);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        try {
          var canGOBack = await webViewController.canGoBack();
          if (!canGOBack && selectedIndex == 0) {
            return true;
          }
          if (widget.notDirectPop && canGOBack) {
            await webViewController.goBack();
            return false;
          }
          return true;
        } catch (e) {
          print(e);
          return true;
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: widget.title != null
              ? Text(
                  widget.title!,
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                )
              : null,
          backgroundColor: Theme.of(context).backgroundColor,
          elevation: 0.0,
        ),
        body: IndexedStack(
          alignment: Alignment.topCenter,
          index: selectedIndex,
          children: [
            SizedBox(
              width: webWidth,
              child:

                  // InAppWebView(
                  //   initialOptions: options,
                  //   initialUrlRequest: URLRequest(
                  //     url: Uri.parse(
                  //         '${widget.url}${widget.multiPram ? '' : '?'}cookie=${EncodeUtils.encodeCookie(user.user!.cookie!)}&user_login=${context.read<UserModel>().user?.username ?? ''}&gc_nonce=$md5&appstyle=1${widget.end}'),
                  //     headers: {'HTTP_SOURCE': '1'},
                  //   ),
                  //   androidShouldInterceptRequest: (c, r) async {
                  //     final url = r.url.toString();
                  //     // print(url);
                  //     if (url.endsWith('pdf')) {
                  //       await launch(url, forceSafariVC: false);
                  //       setState(() {
                  //         selectedIndex = 0;
                  //       });
                  //       return WebResourceResponse();
                  //     }
                  //
                  //     ///回所有活動頁
                  //     if (url ==
                  //         '${environment['serverConfig']['url']}/events/') {
                  //       Navigator.pop(context);
                  //       return WebResourceResponse();
                  //     }
                  //
                  //     ///開啟google日曆
                  //     if (url.contains('calendar.google.com')) {
                  //       await launch(url, forceSafariVC: false);
                  //       setState(() {
                  //         selectedIndex = 0;
                  //       });
                  //       return WebResourceResponse();
                  //     }
                  //
                  //     ///文宣影片文章
                  //     if (widget.alwaysAddAppStyle && !url.contains('appstyle')) {
                  //       inAppWebViewController.loadUrl(
                  //         urlRequest: URLRequest(
                  //             url: Uri.parse(url +
                  //                 (url.endsWith('/')
                  //                     ? '?appstyle=1'
                  //                     : '&appstyle=1'))),
                  //       );
                  //     }
                  //     return WebResourceResponse();
                  //   },
                  //   iosOnNavigationResponse: (c, r) async {
                  //     final url = r.response?.url.toString() ?? '';
                  //     // print(url);
                  //     if (url.endsWith('pdf')) {
                  //       await launch(url, forceSafariVC: false);
                  //       setState(() {
                  //         selectedIndex = 0;
                  //       });
                  //       return IOSNavigationResponseAction.CANCEL;
                  //     }
                  //
                  //     ///回所有活動頁
                  //     if (url ==
                  //         '${environment['serverConfig']['url']}/events/') {
                  //       Navigator.pop(context);
                  //       return IOSNavigationResponseAction.ALLOW;
                  //     }
                  //
                  //     ///開啟google日曆
                  //     if (url.contains('calendar.google.com')) {
                  //       await launch(url, forceSafariVC: false);
                  //       setState(() {
                  //         selectedIndex = 0;
                  //       });
                  //       return IOSNavigationResponseAction.CANCEL;
                  //     }
                  //
                  //     ///文宣影片文章
                  //     if (widget.alwaysAddAppStyle && !url.contains('appstyle')) {
                  //       inAppWebViewController.loadUrl(
                  //         urlRequest: URLRequest(
                  //             url: Uri.parse(url +
                  //                 (url.endsWith('/')
                  //                     ? '?appstyle=1'
                  //                     : '&appstyle=1'))),
                  //       );
                  //     }
                  //     return IOSNavigationResponseAction.ALLOW;
                  //   },
                  //   onWebViewCreated: (webController) {
                  //     print(13123123);
                  //     inAppWebViewController = webController;
                  //     inAppWebViewController.clearCache();
                  //   },
                  //   onLoadError: (c, uri, code, description) async {
                  //     print(code);
                  //     print(uri.toString());
                  //     print(description);
                  //     if (code == -1009) {
                  //       await showDialogNotInternet(context);
                  //       Navigator.pop(context);
                  //     }
                  //   },
                  //   onLoadStart: (c, uri) async {
                  //     final url = uri.toString();
                  //
                  //     if (url.contains('cart/?order_again')) {
                  //       Navigator.of(context)
                  //           .popUntil(ModalRoute.withName(RouteList.dashboard));
                  //       MainTabControlDelegate.getInstance()
                  //           .changeTab(RouteList.cart.replaceFirst('/', ''));
                  //     }
                  //   },
                  //   onProgressChanged: (c, progress) {
                  //     if (progress == 100) {
                  //       setState(() {
                  //         selectedIndex = 0;
                  //       });
                  //     } else {
                  //       setState(() {
                  //         selectedIndex = 1;
                  //       });
                  //     }
                  //   },
                  //   onLoadStop: (c, uri) async {
                  //     // print(url);
                  //     // if(url.contains('cart/')){
                  //     //   Navigator.pop(context,'cart');
                  //     // }
                  //     final url = uri.toString();
                  //     print(url);
                  //     // if (url == 'https://www.google.com') {
                  //     //   setState(() {
                  //     //     selectedIndex = 1;
                  //     //   });
                  //     //   return;
                  //     // }
                  //
                  //     if (url.contains('checkout-2') &&
                  //         widget.jumpOutWhenCheckOut) {
                  //       setState(() {
                  //         selectedIndex = 1;
                  //       });
                  //
                  //       final cartModel = context.read<CartModel>();
                  //       await Services().widget.syncCartFromWebsite(
                  //           user.user?.cookie, cartModel, context);
                  //       cartModel.purchaseMethod = PurchaseType.general;
                  //       cartModel.fastPromote = false;
                  //       Navigator.pop(context);
                  //       await doCheckout();
                  //       // Navigator.of(context).popUntil(
                  //       //     ModalRoute.withName(RouteList.dashboard));
                  //       // MainTabControlDelegate.getInstance().changeTab(
                  //       //     RouteList.cart.replaceFirst('/', ''));
                  //       return;
                  //     }
                  //     setState(() {
                  //       selectedIndex = 0;
                  //     });
                  //
                  //     if (!url.contains('my-accounts/ctcb-credit')) {
                  //       setState(() {
                  //         webWidth = MediaQuery.of(context).size.width;
                  //       });
                  //     } else {
                  //       webWidth = MediaQuery.of(context).size.width;
                  //     }
                  //     // _webViewController.evaluateJavascript('''
                  //     // var email = document.getElementById("username");
                  //     //       var password = document.getElementById("password");
                  //     //       email.value = "${user.user!.username}";
                  //     //       password.value = "${user.user!.password}";
                  //     //
                  //     //       document.getElementsByClassName('woocommerce-button button woocommerce-form-login__submit')[0].click();
                  //     //     ''');
                  //   },
                  // ),
                  WebViewWidget(
                controller: webViewController,
              ),
            ),
            Center(
              child: kLoadingWidget(context),
            )
          ],
        ),
      ),
    );
  }

  Future<void> doCheckout() async {
    await Services().widget.doCheckout(
      context,
      success: () async {
        await FluxNavigate.pushNamed(
          RouteList.checkout,
          arguments: CheckoutArgument(
            isModal: false,
            onRefresh: () {},
          ),
          forceRootNavigator: true,
        );
      },
      error: (message) async {
        if (message ==
            Exception('Token expired. Please logout then login again')
                .toString()) {
          //logout
          final userModel = Provider.of<UserModel>(context, listen: false);
          await userModel.logout();
          Services().firebase.signOut();

          _loginWithResult(context);
        } else {}
      },
      loading: (isLoading) {},
    );
  }

  void _loginWithResult(BuildContext context) async {
    await FluxNavigate.pushNamed(
      RouteList.login,
      forceRootNavigator: true,
    ).then((value) {
      final user = Provider.of<UserModel>(context, listen: false).user;
      if (user != null && user.name != null) {
        Tools.showSnackBar(ScaffoldMessenger.of(context),
            S.of(context).welcome + ' ${user.name} !');
        setState(() {});
      }
    });
  }
}
