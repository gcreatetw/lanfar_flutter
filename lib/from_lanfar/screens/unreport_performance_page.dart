import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools/price_tools.dart';
import '../../models/index.dart';
import '../model/get_change_order_response.dart';
import '../provider/unreport_performance_provider.dart';

class UnReportPerformancePage extends StatefulWidget {
  const UnReportPerformancePage({Key? key}) : super(key: key);

  @override
  State<UnReportPerformancePage> createState() =>
      _UnReportPerformancePageState();
}

class _UnReportPerformancePageState extends State<UnReportPerformancePage> {
  UnReportPerformanceProvider unReportPerformanceProvider =
      UnReportPerformanceProvider();

  bool expanded = false;

  @override
  void initState() {
    unReportPerformanceProvider.fetch(
      context.read<UserModel>().user?.username ?? '',
      onError: (e) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(e),
            duration: const Duration(days: 1),
          ),
        );
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: unReportPerformanceProvider,
      child: Consumer(
        builder: (context, UnReportPerformanceProvider provider, _) {
          return WillPopScope(
            onWillPop: () async {
              ScaffoldMessenger.of(context).removeCurrentSnackBar();
              return true;
            },
            child: Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                backgroundColor: Theme.of(context).backgroundColor,
                iconTheme: IconThemeData(
                  color: Theme.of(context).textTheme.headline6!.color,
                ),
                title: Text(
                  '代購未報業績',
                  style: TextStyle(
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
              ),
              body: provider.loading
                  ? Center(
                      child: kLoadingWidget(context),
                    )
                  : Padding(
                      padding: const EdgeInsets.all(8),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              const Text(
                                '未報換單ＰＶ總計：',
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              ),
                              const Spacer(),
                              Text(
                                PriceTools.getCurrencyFormatted(
                                      (provider.getChangeOrderResponse!.data
                                              ?.header?.sumPv)
                                          .toString(),
                                      null,
                                      isPV: true,
                                    ) ??
                                    '',
                                style: const TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    color: primaryColor),
                              ),
                            ],
                          ),
                          const SizedBox(height: 16),
                          Row(
                            children: [
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  elevation: 0,
                                ),
                                onPressed: () {
                                  setState(
                                    () {
                                      if (expanded) {
                                        expanded = !expanded;
                                        for (var element
                                            in provider.expandableControllers) {
                                          element.expanded = false;
                                        }
                                      } else {
                                        expanded = !expanded;
                                        for (var element
                                            in provider.expandableControllers) {
                                          element.expanded = true;
                                        }
                                      }
                                    },
                                  );
                                },
                                child: Text(
                                  expanded ? '收合' : '展開',
                                  style: const TextStyle(color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 20),
                          _table(provider),
                        ],
                      ),
                    ),
            ),
          );
        },
      ),
    );
  }

  List<ExpandableController> expandableControllers = [];

  Widget _table(UnReportPerformanceProvider provider) {
    return Expanded(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(5),
        child: ListView.separated(
          itemBuilder: (context, index) {
            if (index == 0) {
              return IntrinsicHeight(
                child: Row(
                  children: [
                    Container(
                      color: primaryColor,
                      width: 32,
                    ),
                    const SizedBox(width: 1),
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          provider.reOrder(
                              provider.order == UnReportPerformanceOrder.pv
                                  ? UnReportPerformanceOrder.pvR
                                  : UnReportPerformanceOrder.pv);
                        },
                        child: Container(
                          color: primaryColor,
                          padding: const EdgeInsets.symmetric(vertical: 8),
                          child: Row(
                            children: [
                              const Expanded(
                                child: Text(
                                  'PV分數',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              Icon(
                                provider.order.pvIcon,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(width: 1),
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          provider.reOrder(
                              provider.order == UnReportPerformanceOrder.name
                                  ? UnReportPerformanceOrder.nameR
                                  : UnReportPerformanceOrder.name);
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(vertical: 8),
                          color: primaryColor,
                          child: Row(
                            children: [
                              const Expanded(
                                child: Text(
                                  '代購訂單',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              Icon(
                                provider.order.nameIcon,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(width: 1),
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          provider.reOrder(
                              provider.order == UnReportPerformanceOrder.count
                                  ? UnReportPerformanceOrder.countR
                                  : UnReportPerformanceOrder.count);
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(vertical: 8),
                          color: primaryColor,
                          child: Row(
                            children: [
                              const Expanded(
                                child: Text(
                                  '訂購筆數',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              Icon(
                                provider.order.qtyIcon,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            }

            final data =
                provider.getChangeOrderResponse!.data!.datatable![index - 1];
            return ExpandableNotifier(
              controller: provider.expandableControllers[index - 1],
              child: Column(
                children: [
                  IntrinsicHeight(
                    child: Row(
                      children: [
                        SizedBox(
                          width: 32,
                          child: ExpandableButton(
                            child: const Icon(Icons.arrow_drop_down),
                          ),
                        ),
                        const SizedBox(width: 1),
                        Expanded(
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 4),
                            child: Text(
                              PriceTools.getCurrencyFormatted(
                                    data.ordPv.toString(),
                                    null,
                                    isPV: true,
                                  ) ??
                                  '',
                            ),
                          ),
                        ),
                        const SizedBox(width: 1),
                        Expanded(
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 4),
                            child: Text(
                              data.prodName ?? '',
                            ),
                          ),
                        ),
                        const SizedBox(width: 1),
                        Expanded(
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 4),
                            child: Text(
                              PriceTools.getCurrencyFormatted(
                                    data.sumQty.toString(),
                                    null,
                                    isPV: true,
                                  ) ??
                                  '',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  expandBody(data.details!, data.prodName ?? '')
                ],
              ),
            );
          },
          separatorBuilder: (context, index) {
            return Container(
              height: 1,
              color: Colors.grey,
              width: double.maxFinite,
            );
          },
          itemCount: (provider.getChangeOrderResponse!.data?.datatable == null
                  ? 0
                  : provider.getChangeOrderResponse!.data!.datatable!.length) +
              1,
        ),
      ),
    );
  }

  Widget expandBody(List<Details> details, String name) {
    return ExpandablePanel(
      collapsed: const SizedBox(),
      expanded: Container(
        color: Colors.black12,
        padding: const EdgeInsets.all(4),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              '訂單明細',
              style: TextStyle(fontSize: 18),
            ),
            const SizedBox(height: 8),
            Text(
              name,
            ),
            const SizedBox(height: 8),
            Container(
              decoration: const BoxDecoration(
                color: Colors.white,
              ),
              child: Table(
                border: TableBorder.all(color: Colors.white),
                children: [
                  TableRow(children: [
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 4),
                      color: Colors.black54,
                      child: const Text(
                        '訂單編號',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 4),
                      color: Colors.black54,
                      child: const Text(
                        '訂單日期',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 4),
                      color: Colors.black54,
                      child: const Text(
                        '發票號碼',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ]),
                  for (var element in details)
                    TableRow(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 4.0),
                          child: Text(
                            element.ordNoArea ?? '',
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 4.0),
                          child: Text(
                            (element.ordDate ?? '')
                                .substring(
                                    0, (element.ordDate ?? '').indexOf('T'))
                                .replaceAll('-', '/'),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 4.0),
                          child: Text(
                            element.invNo ?? '',
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
