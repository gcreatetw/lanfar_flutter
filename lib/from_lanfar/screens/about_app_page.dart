import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';

import '../../env.dart';

class AboutAPPPage extends StatefulWidget {
  const AboutAPPPage({Key? key}) : super(key: key);

  @override
  State<AboutAPPPage> createState() => _AboutAPPPageState();
}

class _AboutAPPPageState extends State<AboutAPPPage> {
  String nowVersion = '';
  String deviceOS = '';
  String oSVersion = '';
  String deviceType = '';

  @override
  void initState() {
    Future.microtask(() async {
      final packageInfo = await PackageInfo.fromPlatform();
      final deviceInfoPlugin = DeviceInfoPlugin();

      if (Platform.isIOS) {
        final deviceInfo = await deviceInfoPlugin.iosInfo;
        setState(() {
          nowVersion = packageInfo.version;
          deviceOS = deviceInfo.systemName ?? '';
          oSVersion = deviceInfo.systemVersion ?? '';

          deviceType = deviceInfo.utsname.machine ?? '';
        });
      } else {
        final deviceInfo = await deviceInfoPlugin.androidInfo;
        setState(() {
          nowVersion = packageInfo.version;
          deviceOS = Platform.operatingSystem;
          oSVersion = deviceInfo.version.release ?? '';

          deviceType = deviceInfo.model ?? '';
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '關於APP',
          style: TextStyle(
            color: Theme.of(context).colorScheme.secondary,
          ),
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        elevation: 0.0,
      ),
      body: Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(vertical: 8),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            margin: const EdgeInsets.all(16),
            child: Column(
              children: [
                _title(Icons.info_outline, '目前版本', nowVersion,
                    additionalInfo: environment['serverConfig']['domain']),
                _title(Icons.smartphone, '手機型號', deviceType),
                _title(Icons.app_settings_alt, '作業系統', deviceOS),
                _title(Icons.security_update_warning_outlined, '作業系統版本',
                    oSVersion),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _title(
    IconData icon,
    String title,
    String value, {
    String? additionalInfo,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Row(
        children: [
          Icon(icon),
          const SizedBox(width: 8),
          Text(title),
          const SizedBox(width: 8),
          Expanded(
            child: Text(value),
          ),
          if (additionalInfo != null) ...[
            const SizedBox(width: 8),
            Text(
              additionalInfo,
              style: const TextStyle(
                color: Colors.grey,
              ),
            ),
          ]
        ],
      ),
    );
  }
}
