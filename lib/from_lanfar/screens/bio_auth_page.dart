import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:inspireui/inspireui.dart';
import 'package:provider/provider.dart';

import '../../common/config.dart';
import '../../common/theme/colors.dart';
import '../../env.dart';
import '../../models/user_model.dart';
import '../utils/local_auth_utils.dart';
import '../utils/utils.dart';

class BioAuthPage extends StatefulWidget {
  final Function onAuthFinish;

  const BioAuthPage({
    Key? key,
    required this.onAuthFinish,
  }) : super(key: key);

  @override
  State<BioAuthPage> createState() => _BioAuthPageState();
}

class _BioAuthPageState extends State<BioAuthPage> {
  final localAuth = LocalAuthUtils();
  TextEditingController controller = TextEditingController();

  bool hide = true;

  bool loading = false;

  @override
  void initState() {
    bioAuth(true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AutoHideKeyboard(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
        ),
        body: Center(
          child: loading
              ? kLoadingWidget(context)
              : Padding(
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextField(
                        key: const Key('loginPasswordField'),
                        obscureText: hide,
                        obscuringCharacter: '*',
                        textInputAction: TextInputAction.done,
                        keyboardType: TextInputType.visiblePassword,
                        controller: controller,
                        onChanged: (v) {
                          setState(() {});
                        },
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 10, horizontal: 16),
                          prefixIcon: const Icon(
                            Icons.lock_outline,
                            color: primaryColor,
                          ),
                          suffixIcon: GestureDetector(
                            onTap: () {
                              setState(() {
                                if (hide) {
                                  hide = false;
                                } else {
                                  hide = true;
                                }
                              });
                            },
                            child: Icon(
                              hide
                                  ? Icons.visibility_off_outlined
                                  : Icons.visibility_outlined,
                              color: Colors.grey,
                            ),
                          ),
                          hintText: '請輸入您的密碼',
                          focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                color: Colors.black,
                                width: 0.5,
                              ),
                              borderRadius: BorderRadius.circular(15)),
                          enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                              color: Colors.black,
                              width: 0.5,
                            ),
                            borderRadius: BorderRadius.circular(15),
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                        ),
                        onPressed: controller.text.isEmpty
                            ? null
                            : () async {
                                setState(() {
                                  loading = true;
                                });
                                FocusScope.of(context).unfocus();
                                final user = Provider.of<UserModel>(context,
                                        listen: false)
                                    .user;
                                final response = await httpPost(
                                  '${environment['serverConfig']['url']}/wp-json/api/flutter_user/generate_auth_cookie/?insecure=cool'
                                      .toUri()!,
                                  body: jsonEncode({
                                    'seconds': 60,
                                    'username': user?.username ?? '',
                                    'password': controller.text,
                                  }),
                                  headers: {'Content-Type': 'application/json'},
                                );

                                final body = jsonDecode(response.body);
                                if (body['cookie'] != null &&
                                    body['cookie'].toString().isNotEmpty) {
                                  widget.onAuthFinish.call();
                                } else {
                                  Utils.showSnackBar(context, text: '密碼錯誤');
                                }
                                setState(() {
                                  loading = false;
                                });
                              },
                        child: const Text(
                          '送出',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      const SizedBox(height: 20),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          elevation: 0,
                          shadowColor: Colors.transparent,
                          primary: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                            side:
                                const BorderSide(color: primaryColor, width: 1),
                          ),
                        ),
                        onPressed: () {
                          bioAuth(false);
                        },
                        child: const Padding(
                          padding:
                              EdgeInsets.symmetric(vertical: 8, horizontal: 18),
                          child: Text(
                            '使用生物辨識',
                            style: TextStyle(color: primaryColor),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
        ),
      ),
    );
  }

  Future<void> bioAuth(bool init) async {
    var result = await localAuth.checkCanAuth();

    if (result) {
      var r = await localAuth.startAuth();
      switch (r) {
        case true:
          widget.onAuthFinish.call();
          break;
        case false:
          break;
        case null:
          if (init) return;
          await showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: const Text('生物辨識'),
              content: const Text('您尚未設定生物辨識'),
              actions: [
                // TextButton(
                //   onPressed: () {
                //     Navigator.pop(context);
                //   },
                //   child: const Text(
                //     '取消',
                //     style: TextStyle(
                //       color: Colors.black54,
                //     ),
                //   ),
                // ),
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    // AppSettings.openLockAndPasswordSettings();
                  },
                  child: const Text('確定'),
                ),
              ],
            ),
          );
          break;
      }
    } else {
      Utils.showSnackBar(context, text: '您的裝置無法使用生物辨識');
    }
  }
}
