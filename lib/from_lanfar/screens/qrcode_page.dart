import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:solid_bottom_sheet/solid_bottom_sheet.dart';
import 'package:syncfusion_flutter_barcodes/barcodes.dart';

import '../../common/constants.dart';
import '../../models/entities/index.dart';
import '../../models/user_model.dart';
import '../model/barcode_data.dart';
import '../model/count_down_data.dart';
import '../widget/profile_card.dart';

class QRCodePage extends StatefulWidget {
  const QRCodePage({Key? key}) : super(key: key);

  @override
  _QRCodePageState createState() => _QRCodePageState();
}

class _QRCodePageState extends State<QRCodePage> with TickerProviderStateMixin {
  SolidController solidController = SolidController();

  late AnimationController animationController;

  late UserModel userModel;

  late User? user;

  int countDownValue = 300;

  QrImage qrImage = QrImage(data: '');

  bool isCounting = true;

  var counter = CountDownData();

  @override
  void initState() {
    userModel = Provider.of<UserModel>(context, listen: false);
    user = userModel.user;
    setQrImage();
    animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );
    counter.countDown(setQrImage);
    final barcodeData = Provider.of<BarcodeData>(context, listen: false);
    barcodeData.read();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: const BackButton(),
        iconTheme:
            IconThemeData(color: Theme.of(context).textTheme.titleLarge!.color),
        backgroundColor: Theme.of(context).colorScheme.background,
        title: Text(
          '我的條碼',
          style: TextStyle(
            fontSize: 16,
            color: Theme.of(context).colorScheme.secondary,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: GestureDetector(
        onTap: () {
          solidController.hide();
          rotate();
        },
        child: ChangeNotifierProvider.value(
          value: counter,
          child: Stack(
            fit: StackFit.passthrough,
            children: [
              Positioned.fill(
                top: 70,
                child: Wrap(
                  children: [
                    Image.asset(
                      'assets/images/qrcode_background.png',
                    ),
                    Image.asset(
                      'assets/images/qrcode_background.png',
                    ),
                    Image.asset(
                      'assets/images/qrcode_background.png',
                    ),
                  ],
                ),
              ),
              SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const SizedBox(height: 16),
                    Center(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: ProfileCard(
                          userCode: user != null && user!.username != null
                              ? user!.username!
                              : '',
                          userName: user != null && user!.name != null
                              ? user!.name!
                              : '',
                          userGrade: user != null && user!.grade1title != null
                              ? user!.grade1title!
                              : '',
                          joinDate: user != null && user!.registered != null
                              ? user!.registered!
                              : '',
                          elevation: 4,
                          padding: 16,
                        ),
                      ),
                    ),
                    const SizedBox(height: 32),
                    GestureDetector(
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (context) => Column(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      padding: const EdgeInsets.all(16),
                                      color: Colors.white,
                                      child: qrImage,
                                    )
                                  ],
                                ));
                      },
                      child: Container(
                        constraints: const BoxConstraints(
                          maxHeight: 500,
                          maxWidth: 500,
                        ),
                        padding: const EdgeInsets.all(8),
                        color: Colors.white,
                        height: MediaQuery.of(context).size.width - 120,
                        width: MediaQuery.of(context).size.width - 120,
                        alignment: Alignment.center,
                        child: qrImage,
                      ),
                    ),
                    const SizedBox(height: 8),
                    Consumer<CountDownData>(builder: (context, data, _) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 60),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            // Text(
                            //     '0${Duration(seconds: data.countDownValue).inMinutes} : ${data.countDownValue % 60 < 10 ? '0' : ''}${data.countDownValue % 60}'),
                            Text('倒數${data.countDownValue}秒'),
                            const SizedBox(width: 16),
                            GestureDetector(
                              onTap: () {
                                setQrImage();
                                data.timer!.cancel();
                                data.countDown(setQrImage);
                              },
                              child: Row(
                                children: const [
                                  Icon(
                                    Icons.autorenew,
                                    size: 20,
                                    color: primaryColor,
                                  ),
                                  Text(
                                    '重整',
                                    style: TextStyle(color: primaryColor),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      );
                    }),
                    // const SizedBox(height: 32),
                    // GestureDetector(
                    //   onTap: () {
                    //     showDialog(
                    //       context: context,
                    //       builder: (context) => Column(
                    //         mainAxisSize: MainAxisSize.min,
                    //         mainAxisAlignment: MainAxisAlignment.center,
                    //         children: [
                    //           Container(
                    //               color: Colors.white,
                    //               padding: const EdgeInsets.symmetric(vertical: 32),
                    //               child: Column(
                    //                 mainAxisSize: MainAxisSize.min,
                    //                 mainAxisAlignment: MainAxisAlignment.center,
                    //                 children: [
                    //                   Container(
                    //                     height: 100,
                    //                     width: MediaQuery.of(context).size.width,
                    //                     alignment: Alignment.center,
                    //                     child: SfBarcodeGenerator(
                    //                       value: '38 7389 5428 3996 2831',
                    //                       showValue: true,
                    //                     ),
                    //                   ),
                    //                 ],
                    //               ))
                    //         ],
                    //       ),
                    //     );
                    //   },
                    //   child: Column(
                    //     children: [
                    //       Container(
                    //         height: 100,
                    //         width: MediaQuery.of(context).size.width - 80,
                    //         alignment: Alignment.center,
                    //         child: SfBarcodeGenerator(
                    //           value: '38 7389 5428 3996 2831',
                    //           showValue: true,
                    //         ),
                    //       ),
                    //       // const SizedBox(height: 4),
                    //       // const Text(
                    //       //   '38 7389 5428 3996 2831',
                    //       //   style: TextStyle(
                    //       //     height: 1.1,
                    //       //   ),
                    //       // ),
                    //     ],
                    //   ),
                    // ),
                    const SizedBox(height: 100),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      // bottomSheet: GestureDetector(
      //     onTap: () {
      //       showModalBottomSheet(
      //         isScrollControlled: true,
      //         backgroundColor: Colors.transparent,
      //         context: context,
      //         builder: (context) => SingleChildScrollView(
      //           child: Container(
      //             padding: EdgeInsets.only(
      //                 bottom: MediaQuery.of(context).viewInsets.bottom),
      //             child: const BarcodeGG(),
      //           ),
      //         ),
      //       );
      //     },
      //     child: Container(
      //       height: 50 + MediaQuery.of(context).padding.bottom,
      //       padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
      //       width: double.maxFinite,
      //       decoration: const BoxDecoration(
      //         color: Colors.white,
      //         borderRadius: BorderRadius.only(
      //           topLeft: Radius.circular(20.0),
      //           topRight: Radius.circular(20.0),
      //         ),
      //         boxShadow: [
      //           BoxShadow(
      //             color: Colors.grey,
      //             offset: Offset(0.0, 1.0), //(x,y)
      //             blurRadius: 6.0,
      //           ),
      //         ],
      //       ),
      //       child: Column(
      //         children: const [SizedBox(height: 16), Text('手機條碼載具')],
      //       ),
      //     ),
      //   ),
    );
  }

  void rotate() {
    if (solidController.isOpened) {
      animationController.forward();
    } else {
      animationController.reverse();
    }
  }

  void setQrImage() {
    if (mounted) {
      setState(() {
        qrImage = QrImage(
          data:
              '${user?.username}|${DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())}',
        );
      });
    }
  }
}

class BarcodeGG extends StatefulWidget {
  const BarcodeGG({Key? key}) : super(key: key);

  @override
  _BarcodeGGState createState() => _BarcodeGGState();
}

class _BarcodeGGState extends State<BarcodeGG> {
  TextEditingController textEditingController = TextEditingController();

  String codeNumber = '';

  bool correct = false;

  @override
  Widget build(BuildContext context) {
    return Consumer<BarcodeData>(builder: (context, BarcodeData data, _) {
      return GestureDetector(
        onTap: () {},
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 6.0,
                  ),
                ],
              ),
              child: Column(
                children: [
                  const SizedBox(height: 16),
                  const Text('手機條碼載具'),
                  const SizedBox(height: 20),
                  Center(
                    child: data.code.isEmpty
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                child: TextField(
                                  controller: textEditingController,
                                  style: const TextStyle(fontSize: 20),
                                  maxLength: 8,
                                  textCapitalization:
                                      TextCapitalization.characters,
                                  inputFormatters: [
                                    UpperCaseTextFormatter(),
                                  ],
                                  decoration: InputDecoration(
                                    counterText: '',
                                    labelText: '/請輸入載具號碼',
                                    alignLabelWithHint: true,
                                    errorText: correct ? null : '請輸入正確手機條碼載具號碼',
                                    errorStyle: const TextStyle(fontSize: 12),
                                    labelStyle: const TextStyle(fontSize: 20),
                                    floatingLabelStyle: const TextStyle(
                                      fontSize: 16,
                                    ),
                                  ),
                                  onChanged: (value) {
                                    setState(() {
                                      correct = checkInputError(value);
                                    });
                                  },
                                ),
                              ),
                              const SizedBox(width: 16),
                              ElevatedButton(
                                  onPressed: correct
                                      ? () async {
                                          setState(() {
                                            data.setCode(
                                                textEditingController.text);
                                          });

                                          textEditingController.text = '';
                                        }
                                      : null,
                                  style: ButtonStyle(
                                    shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(18.0),
                                      ),
                                    ),
                                  ),
                                  child: Text(
                                    '新增',
                                    style: TextStyle(
                                        color: correct
                                            ? Colors.white
                                            : Colors.black),
                                  ))
                            ],
                          )
                        : Column(
                            children: [
                              SizedBox(
                                height: 100,
                                width: double.maxFinite,
                                child: SfBarcodeGenerator(
                                    symbology: Code39(), value: data.code),
                              ),
                              const SizedBox(height: 8),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(data.code),
                                  GestureDetector(
                                      onTap: () {
                                        data.setCode('');
                                      },
                                      child: const Text(
                                        '刪除',
                                        style: TextStyle(
                                          decoration: TextDecoration.underline,
                                        ),
                                      )),
                                ],
                              )
                            ],
                          ),
                  ),
                  const SizedBox(height: 50),
                ],
              ),
            )
          ],
        ),
      );
    });
  }

  bool checkInputError(String value) {
    if (value[0] != '/') {
      return false;
    }
    if (value.length < 8) {
      return false;
    }
    return true;
  }
}

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toUpperCase(),
      selection: newValue.selection,
    );
  }
}
