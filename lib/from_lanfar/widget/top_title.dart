import 'package:flutter/material.dart';
import 'package:lanfar/common/theme/colors.dart';

class TopTitle extends StatelessWidget {
  final String title;
  final Widget action;

  const TopTitle({
    Key? key,
    required this.title, required this.action,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: Row(
        children: [
          Container(
            height: 30,
            width: 8,
            color: primaryColor,
          ),
          const SizedBox(width: 16),
          Expanded(
            child: Text(
              title,
              style: const TextStyle(fontSize: 18),
            ),
          ),
          const SizedBox(width: 16),
          action,
        ],
      ),
    );
  }
}
