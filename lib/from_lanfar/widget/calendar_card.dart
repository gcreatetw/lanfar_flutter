import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../common/constants.dart';
import '../contants/constants.dart';
import '../model/calendar_event.dart';
import '../screens/webview_with_cookie.dart';
import 'measure_size.dart';

class CalendarCard extends StatefulWidget {
  final CalendarEvent data;

  const CalendarCard({Key? key, required this.data}) : super(key: key);

  @override
  _CalendarCardState createState() => _CalendarCardState();
}

class _CalendarCardState extends State<CalendarCard> {
  var headerSize = Size.zero;

  var middleSize = Size.zero;

  double? decideCardHeight() {
    if (headerSize == Size.zero || middleSize == Size.zero) {
      return null;
    }
    if (headerSize.height > middleSize.height) {
      return headerSize.height;
    }
    if (headerSize.height < middleSize.height) {
      return middleSize.height;
    }
    return middleSize.height;
  }

  @override
  Widget build(BuildContext context) {
    var color = Colors.black.withOpacity(0.1);
    switch (widget.data.category!.name) {
      case '桃園':
        color = Color(Constants.calendarColors[1]);
        break;
      case '台中':
        color = Color(Constants.calendarColors[2]);
        break;
      case '台南':
        color = Color(Constants.calendarColors[3]);
        break;
      case '高雄':
        color = Color(Constants.calendarColors[4]);
        break;
      case '全部':
        color = Color(Constants.calendarColors[0]);
        break;
      case '節日':
        color = Colors.redAccent;
        break;
      default:
        break;
    }

    return GestureDetector(
      onTap: () {
        if (widget.data.url != null) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => WebViewWithCookie(
                url: widget.data.url!,
                notDirectPop: true,
              ),
            ),
          );
        }
      },
      child: Container(
        color: Colors.white,
        height: decideCardHeight(),
        child: Row(
          children: [
            MeasureSize(
              onChange: (size) {
                if (mounted) {
                  setState(() {
                    headerSize = size;
                  });
                }
              },
              child: Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      DateFormat('yyyy/MM', 'zh_TW')
                          .format(widget.data.startDate!),
                      style: const TextStyle(
                        fontSize: 14,
                        color: Colors.black54,
                      ),
                    ),
                    const SizedBox(height: 4),
                    Text(
                      DateFormat('EEE', 'zh_TW').format(widget.data.startDate!),
                      style: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(height: 4),
                    Text(
                      DateFormat('dd', 'zh_TW').format(widget.data.startDate!),
                      style: const TextStyle(fontSize: 36, color: primaryColor),
                    ),
                    const SizedBox(height: 4),

                    ///移除活動狀態
                    // Container(
                    //   padding: const EdgeInsets.symmetric(
                    //       horizontal: 6, vertical: 1),
                    //   decoration: BoxDecoration(
                    //       border: Border.all(color: widget.data.getColor()),
                    //       borderRadius: BorderRadius.circular(5)),
                    //   height: widget.data.category!.name != '節日' ?null : 0,
                    //   child: Text(
                    //     widget.data.checkEventStart(),
                    //     style: TextStyle(
                    //         fontSize: 12, color: widget.data.getColor()),
                    //   ),
                    // )
                  ],
                ),
              ),
            ),
            Flexible(
              flex: 2,
              child: MeasureSize(
                onChange: (size) {
                  if (mounted) {
                    setState(() {
                      middleSize = size;
                    });
                  }
                },
                child: Container(
                  decoration: const BoxDecoration(
                    border: Border(
                      left: BorderSide(color: Colors.black12),
                    ),
                  ),
                  child: Stack(
                    children: [
                      Positioned(
                        left: 0,
                        top: 0,
                        child: CustomPaint(
                          painter: _ShapesPainter(color),
                          child: const SizedBox(
                            height: 16,
                            width: 16,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 12, vertical: 12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              widget.data.title ?? '',
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            const SizedBox(height: 8),
                            if (widget.data.category!.name != '節日')
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Padding(
                                    padding: EdgeInsets.only(top: 3),
                                    child: Icon(
                                      Icons.location_city,
                                      color: Colors.black26,
                                      size: 16,
                                    ),
                                  ),
                                  const SizedBox(width: 4),
                                  Expanded(
                                    child: Text(
                                      widget.data.venue ?? '',
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                        fontSize: 14,
                                        color: Colors.black45,
                                      ),
                                      strutStyle: const StrutStyle(
                                          forceStrutHeight: true, leading: 0.5),
                                    ),
                                  ),
                                ],
                              ),
                            if (widget.data.category!.name != '節日')
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Padding(
                                    padding: EdgeInsets.only(top: 3),
                                    child: Icon(
                                      Icons.location_on,
                                      color: Colors.black26,
                                      size: 16,
                                    ),
                                  ),
                                  const SizedBox(width: 4),
                                  Expanded(
                                    child: Text(
                                      widget.data.address ?? '',
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                        fontSize: 14,
                                        color: Colors.black45,
                                      ),
                                      strutStyle: const StrutStyle(
                                        forceStrutHeight: true,
                                        leading: 0.5,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            if (widget.data.imageUrl != null &&
                widget.data.imageUrl!.isNotEmpty &&
                widget.data.imageUrl!.contains('http'))
              Flexible(
                flex: 1,
                child: Image.network(
                  widget.data.imageUrl!,
                  fit: BoxFit.cover,
                  height: decideCardHeight() != null ? double.maxFinite : 120,
                ),
              )
          ],
        ),
      ),
    );
    // return GestureDetector(
    //   onTap: () {
    //     if (widget.data.url != null) {
    //       Navigator.of(context).push(
    //         MaterialPageRoute(
    //           builder: (context) => WebViewWithCookie(url: widget.data.url!),
    //         ),
    //       );
    //     }
    //   },
    //   child: Container(
    //     color: Colors.white,
    //     child: Column(
    //       children: [
    //         const SizedBox(height: 16),
    //         Row(
    //           children: [
    //             const SizedBox(width: 16),
    //             if (widget.data.imageUrl!.isNotEmpty) ...[
    //               Container(
    //                 decoration: BoxDecoration(
    //                   border: Border.all(color: color, width: 2),
    //                   borderRadius: BorderRadius.circular(8),
    //                 ),
    //                 width: 100,
    //                 height: 82,
    //                 child: ClipRRect(
    //                   borderRadius: BorderRadius.circular(6),
    //                   child: Image.network(
    //                     widget.data.imageUrl!,
    //                     fit: BoxFit.cover,
    //                   ),
    //                 ),
    //               ),
    //               const SizedBox(width: 8),
    //             ],
    //             Expanded(
    //               child: Column(
    //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //                 crossAxisAlignment: CrossAxisAlignment.start,
    //                 children: [
    //                   Text(
    //                     buildTimeDuration(),
    //                     style: TextStyle(fontSize: 16, color: color),
    //                   ),
    //                   Text(
    //                     widget.data.title!,
    //                     style: TextStyle(
    //                         fontSize: 18,
    //                         fontWeight: FontWeight.bold,
    //                         color: color),
    //                   ),
    //                   const SizedBox(height: 4),
    //                   if (widget.data.venue!.isNotEmpty &&
    //                       widget.data.address!.isNotEmpty)
    //                     Text(
    //                       widget.data.venue! +
    //                           (widget.data.venue!.isNotEmpty &&
    //                                   widget.data.address!.isNotEmpty
    //                               ? ' : '
    //                               : '') +
    //                           widget.data.address!,
    //                       style: const TextStyle(fontSize: 14),
    //                     ),
    //                 ],
    //               ),
    //             ),
    //             const SizedBox(
    //               width: 16,
    //             ),
    //           ],
    //         ),
    //         const SizedBox(height: 16),
    //       ],
    //     ),
    //   ),
    // );
  }

  String buildTimeDuration() {
    var duration = '';
    duration +=
        '${widget.data.startDate!.year}/${widget.data.startDate!.month}/${widget.data.startDate!.day} ';
    duration += (widget.data.startDate!.hour < 12 ? 'AM ' : 'PM ') +
        '${dateFormatChanger(widget.data.startDate!.hour, changeHour: true)}:${dateFormatChanger(widget.data.startDate!.minute)}';
    return duration;
  }

  String dateFormatChanger(int value, {bool changeHour = false}) {
    if (changeHour && value > 12) {
      value -= 12;
    }
    if (value < 10) {
      return '0$value';
    }
    return value.toString();
  }
}

class _ShapesPainter extends CustomPainter {
  final Color color;

  _ShapesPainter(this.color);

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    paint.color = color;
    var path = Path();
    path.lineTo(size.width, 0);
    path.lineTo(0, size.width);
    path.close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
