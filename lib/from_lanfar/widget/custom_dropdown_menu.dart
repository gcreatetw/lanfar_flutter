import 'package:flutter/material.dart';
import '../../common/constants.dart';
import 'measure_size.dart';

class CustomDropDownMenu extends StatefulWidget {
  final Color borderColor;
  final List<DropdownMenuItem<String>> items;
  final String hintText;
  final Color hintColor;
  final String? currentValue;
  final Function(String?)? onChange;

  const CustomDropDownMenu({
    Key? key,
    this.borderColor = Colors.grey,
    required this.items,
    this.hintText = '',
    this.hintColor = Colors.grey,
    required this.currentValue,
    required this.onChange,
  }) : super(key: key);

  @override
  State<CustomDropDownMenu> createState() => _CustomDropDownMenuState();
}

class _CustomDropDownMenuState extends State<CustomDropDownMenu> {
  var height = Size.zero;

  double setHeight(size) {
    if (size == Size.zero) {
      return 50;
    }
    height = size;
    return height.height;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
            width: double.maxFinite,
            padding: const EdgeInsets.only(left: 16, right: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                border: Border.all(color: widget.borderColor)),
            child: Row(
              children: [
                Expanded(
                  child: MeasureSize(
                    child: Container(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        widget.hintText,
                        style: TextStyle(
                            color: widget.currentValue == null
                                ? widget.hintColor
                                : Colors.transparent),
                      ),
                    ),
                    onChange: (size) {
                      setState(() {
                        height = size;
                      });
                    },
                  ),
                ),
                Container(
                  height: setHeight(height),
                  padding: const EdgeInsets.only(left: 10),
                  decoration: BoxDecoration(
                    border: Border(
                      left: BorderSide(
                        color: widget.borderColor,
                        width: 1,
                      ),
                    ),
                  ),
                  child: const Icon(
                    Icons.keyboard_arrow_down_rounded,
                    color: primaryColor,
                  ),
                ),
              ],
            )),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: DropdownButton<String>(
            dropdownColor: Colors.white,
            iconSize: 0,
            underline: const SizedBox(),
            value: widget.currentValue,
            isExpanded: true,
            borderRadius: BorderRadius.circular(10),
            items: widget.items,
            onChanged: widget.onChange,
          ),
        ),
      ],
    );
  }
}
