// Copyright 2020 the Dart project authors.
//
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file or at
// https://developers.google.com/open-source/licenses/bsd

import 'package:flutter/material.dart';
import '../../../../common/constants.dart';
import '../../../provider/performance_overview_page_provider.dart';

import 'builder.dart';
import 'primitives/tree_controller.dart';
import 'primitives/tree_node.dart';

/// Widget that displays one [TreeNode] and its children.
class NodeWidget extends StatefulWidget {
  final TreeNode treeNode;
  final double? indent;
  final double? iconSize;
  final TreeController state;
  final bool isLast;
  final PerformanceModel model;

  const NodeWidget({
    Key? key,
    required this.treeNode,
    this.indent,
    required this.state,
    this.iconSize,
    required this.isLast,
    required this.model,
  }) : super(key: key);

  @override
  _NodeWidgetState createState() => _NodeWidgetState();
}

class _NodeWidgetState extends State<NodeWidget> with TickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<double> _animationOpacity;

  bool first = true;

  bool get _isLeaf {
    return widget.treeNode.children == null ||
        widget.treeNode.children!.isEmpty;
  }

  bool get _isExpanded {
    return widget.state.isNodeExpanded(widget.treeNode.key!);
  }

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 200),
    );
    _animationOpacity =
        Tween<double>(begin: 1, end: 0).animate(_animationController);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.model.gen >= 1 && first) {
      widget.state.collapseNode(widget.treeNode.key!);
      first = false;
    }

    if (_isExpanded) {
      _animationController.reverse();
    } else {
      _animationController.forward();
    }

    var icon = _isLeaf
        ? null
        : _isExpanded
            ? Icons.expand_more
            : Icons.chevron_right;

    var onIconPressed = _isLeaf
        ? null
        : () => setState(() {
              widget.state.toggleNodeExpanded(widget.treeNode.key!);
              if (_isExpanded) {
                _animationController.reverse();
              } else {
                _animationController.forward();
              }
            });

    return Container(
      decoration: widget.isLast
          ? null
          : const BoxDecoration(
              border: Border(
                left: BorderSide(color: primaryColor),
              ),
            ),
      child: Stack(
        children: [
          if (widget.isLast && widget.model.gen != 0)
            Container(
              color: primaryColor,
              width: 1,
              height: 30,
            ),
          if (widget.model.gen != 0)
            Container(
              margin: const EdgeInsets.only(
                left: 0,
                top: 30,
              ),
              color: primaryColor,
              height: 1,
              width: 15,
            ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 15),
              GestureDetector(
                onTap: onIconPressed,
                child: widget.treeNode.content,
              ),
              if (!_isLeaf && _isExpanded)
                Container(
                  padding: EdgeInsets.only(
                    left: widget.model.gen == 0 ? 25 : widget.indent!,
                    top: 0,
                  ),
                  child: SizeTransition(
                    sizeFactor: _animationOpacity,
                    child: FadeTransition(
                      opacity: _animationOpacity,
                      child: buildNodes(
                        widget.treeNode.children!,
                        widget.indent,
                        widget.state,
                        widget.iconSize,
                        widget.model.child,
                      ),
                    ),
                  ),
                )
            ],
          ),
        ],
      ),
    );
  }
}
