// Copyright 2020 the Dart project authors.
//
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file or at
// https://developers.google.com/open-source/licenses/bsd

import 'package:flutter/material.dart';
import 'package:lanfar/from_lanfar/provider/performance_overview_page_provider.dart';

import 'node_widget.dart';
import 'primitives/tree_controller.dart';
import 'primitives/tree_node.dart';

/// Builds set of [nodes] respecting [state], [indent] and [iconSize].
Widget buildNodes(
  Iterable<TreeNode> nodes,
  double? indent,
  TreeController state,
  double? iconSize,
    List<PerformanceModel> modelList,
) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      for (int i = 0; i < nodes.length; i++)
        NodeWidget(
          treeNode: nodes.toList()[i],
          indent: indent,
          state: state,
          iconSize: iconSize,
          isLast: i == nodes.length - 1,
          model: modelList[i],
        )
    ],
  );
}
