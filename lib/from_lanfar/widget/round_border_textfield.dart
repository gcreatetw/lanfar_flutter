import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class RoundBorderTextField extends StatefulWidget {
  final TextEditingController controller;
  final String hint;
  final bool required;
  final bool hasMinLines;
  final bool code;
  final Color? textColor;
  final Color borderColor;
  final FocusNode? nowFocusNode;
  final FocusNode? nextFocusNode;
  final Function(String)? onChange;
  final bool enable;
  final List<TextInputFormatter>? inputFormatters;
  final double? radius;

  const RoundBorderTextField({
    Key? key,
    required this.controller,
    required this.hint,
    this.required = true,
    this.hasMinLines = false,
    this.code = false,
    this.textColor,
    this.borderColor = Colors.grey,
    this.nowFocusNode,
    this.nextFocusNode,
    this.onChange,
    this.enable = true,
    this.inputFormatters,
    this.radius,
  }) : super(key: key);

  @override
  _RoundBorderTextFieldState createState() => _RoundBorderTextFieldState();
}

class _RoundBorderTextFieldState extends State<RoundBorderTextField> {
  final FocusNode focusNode = FocusNode();

  String text = '';

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topLeft,
      children: [
        if (text.isEmpty && widget.required)
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
            child: RichText(
              text: TextSpan(
                style: const TextStyle(color: Colors.red, fontSize: 16),
                text: '* ',
                children: [
                  TextSpan(
                    text: widget.hint,
                    style: TextStyle(
                      color: widget.textColor ??
                          Theme.of(context).textTheme.bodyText2!.color,
                      fontSize: 16,
                    ),
                  )
                ],
              ),
            ),
          ),
        TextField(
          key: widget.key,
          enabled: widget.enable,
          focusNode: widget.nowFocusNode,
          controller: widget.controller,
          onSubmitted: (_) {
            FocusScope.of(context).requestFocus(widget.nextFocusNode);
          },
          inputFormatters: widget.inputFormatters,
          textInputAction: widget.nextFocusNode != null
              ? TextInputAction.next
              : TextInputAction.done,
          style: const TextStyle(
            fontSize: 16,
          ),
          onChanged: (value) {
            setState(() {
              text = value;
            });
            widget.onChange?.call(value);
          },
          minLines: widget.hasMinLines ? 6 : null,
          maxLines: widget.hasMinLines ? 100 : null,
          keyboardType:
              widget.code ? TextInputType.number : TextInputType.multiline,
          decoration: InputDecoration(
            filled: true,
            fillColor: Colors.transparent,
            contentPadding:
                const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: widget.borderColor,
                  width: 1,
                ),
                borderRadius: BorderRadius.circular(widget.radius ?? 15)),
            floatingLabelBehavior: FloatingLabelBehavior.auto,
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: widget.borderColor,
                width: 1,
              ),
              borderRadius: BorderRadius.circular(widget.radius ?? 15),
            ),
            disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: widget.borderColor,
                width: 1,
              ),
              borderRadius: BorderRadius.circular(widget.radius ?? 15),
            ),
            hintText: widget.required ? '' : widget.hint,
            hintStyle: TextStyle(
              fontSize: 16,
              color: widget.textColor ??
                  Theme.of(context).textTheme.bodyText2!.color,
            ),
          ),
        ),
      ],
    );
  }
}
