import 'package:flutter/material.dart';
import 'package:lanfar/from_lanfar/widget/profile_card.dart';

class QRCodeDialog extends StatelessWidget {
  const QRCodeDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 24),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Theme.of(context).backgroundColor),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(height: 12),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: const Icon(Icons.close),
                    )
                  ],
                ),
              ),
              const SizedBox(height: 12),
              // const Padding(
              //   padding: EdgeInsets.symmetric(horizontal: 16),
              //   child: ProfileCard(elevation: 0, padding: 16),
              // ),
              const SizedBox(height: 24),
              Container(
                height: MediaQuery.of(context).size.width - 200,
                width: MediaQuery.of(context).size.width - 200,
                alignment: Alignment.center,
                child: Image.asset('assets/icons/from_lanfar/qr.ioi.tw.png'),
              ),
              const SizedBox(height: 30),
            ],
          ),
        )
      ],
    );
  }
}
