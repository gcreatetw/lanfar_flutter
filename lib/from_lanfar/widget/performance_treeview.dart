import 'package:flutter/material.dart';

import '../../common/constants.dart';
import '../../common/tools/price_tools.dart';
import '../provider/performance_overview_page_provider.dart';
import 'round_border_textfield.dart';
import 'tree_view/flutter_simple_treeview.dart';

class PerformanceTreeView extends StatefulWidget {
  final List<PerformanceModel> data;
  final List<PerformanceModel> tableData;
  final Function(String) onSearch;

  const PerformanceTreeView({
    Key? key,
    required this.data,
    required this.tableData,
    required this.onSearch,
  }) : super(key: key);

  @override
  State<PerformanceTreeView> createState() => _PerformanceTreeViewState();
}

class _PerformanceTreeViewState extends State<PerformanceTreeView>
    with TickerProviderStateMixin {
  late TabController controller;
  final TreeController _treeController = TreeController(allNodesExpanded: true);

  final TextEditingController searchController = TextEditingController();

  bool showRPV = false;
  bool showTPV = false;

  bool expand = true;

  List<String> columns = [
    'ID',
    '代',
    '下線',
    '姓名',
    '職級',
    '個人業績',
    '個人組織',
    '整組業績',
    '累積業績',
  ];

  @override
  void initState() {
    controller = TabController(
      length: 2,
      vsync: this,
    );
    controller.addListener(() {
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TabBar(
          labelColor: primaryColor,
          indicatorColor: primaryColor,
          controller: controller,
          tabs: const [
            Tab(
              text: '組織樹狀圖',
            ),
            Tab(
              text: '組織清單',
            ),
          ],
        ),
        IndexedStack(
          index: controller.index,
          children: [
            SizedBox(
              height: controller.index == 0 ? null : 1,
              child: Column(
                children: _treeView(),
              ),
            ),
            SizedBox(
              height: controller.index == 1 ? null : 1,
              child: Column(
                children: _tableView(),
              ),
            ),
          ],
        )
      ],
    );
  }

  List<Widget> _tableView() {
    return [
      const SizedBox(height: 16),
      RoundBorderTextField(
        controller: searchController,
        hint: '搜尋',
        required: false,
        textColor: Colors.black54,
        onChange: widget.onSearch,
      ),
      const SizedBox(height: 16),
      SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Table(
          defaultColumnWidth: const IntrinsicColumnWidth(),
          children: [
            TableRow(
              decoration: const BoxDecoration(
                color: Colors.grey,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                ),
              ),
              children: [
                for (var element in columns) _tableTitle(element),
              ],
            ),
            for (int i = 0; i < widget.tableData.length; i++)
              TableRow(
                decoration: BoxDecoration(
                  color: i % 2 == 0 ? Colors.white : const Color(0xffecf6fc),
                ),
                children: [
                  _tableChildTitle(widget.tableData[i].serno.toString()),
                  _tableChildTitle(widget.tableData[i].gen.toString()),
                  _tableChildTitle(widget.tableData[i].id),
                  _tableChildTitle(widget.tableData[i].name),
                  _tableChildTitle(widget.tableData[i].grade),
                  _tableChildTitle(PriceTools.getCurrencyFormatted(
                        widget.tableData[i].pv2.toString(),
                        null,
                        isPV: true,
                      ) ??
                      ''),
                  _tableChildTitle(PriceTools.getCurrencyFormatted(
                        widget.tableData[i].rPV.toString(),
                        null,
                        isPV: true,
                      ) ??
                      ''),
                  _tableChildTitle(PriceTools.getCurrencyFormatted(
                        widget.tableData[i].pv.toString(),
                        null,
                        isPV: true,
                      ) ??
                      ''),
                  _tableChildTitle(PriceTools.getCurrencyFormatted(
                        widget.tableData[i].tPV.toString(),
                        null,
                        isPV: true,
                      ) ??
                      ''),
                ],
              ),
          ],
        ),
      )
    ];
  }

  Widget _tableChildTitle(String content) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 16),
      child: Text(content),
    );
  }

  Widget _tableTitle(String title) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Text(
        title,
        textAlign: TextAlign.center,
        style: const TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  List<Widget> _treeView() {
    return [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          checkBoxTitle(
            title: '個人組織',
            value: showRPV,
            onTap: () {
              setState(() {
                showRPV = !showRPV;
              });
            },
          ),
          checkBoxTitle(
            title: '累積業績',
            value: showTPV,
            onTap: () {
              setState(() {
                showTPV = !showTPV;
              });
            },
          )
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          TextButton(
            onPressed: () {
              expand = true;
              setState(_treeController.expandAll);
            },
            child: Text(
              '全部展開',
              style: TextStyle(color: expand ? null : Colors.black54),
            ),
          ),
          TextButton(
            onPressed: () {
              expand = false;
              setState(_treeController.collapseAll);
            },
            child: Text(
              '全部收合',
              style: TextStyle(color: expand ? Colors.black54 : null),
            ),
          )
        ],
      ),
      _tree(),
    ];
  }

  Widget checkBoxTitle(
      {required String title, required bool value, required Function() onTap}) {
    return GestureDetector(
      onTap: onTap,
      child: Row(
        children: [
          Checkbox(
            value: value,
            onChanged: (value) {
              onTap.call();
            },
          ),
          Text(title),
        ],
      ),
    );
  }

  Widget _tree() {
    return Column(
      children: [
        const SizedBox(height: 16),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: TreeView(
            indent: 40,
            treeController: _treeController,
            nodes: widget.data.map(_treeNode).toList(),
            modelList: widget.data,
          ),
        ),
      ],
    );
  }

  TreeNode _treeNode(PerformanceModel model) {
    return TreeNode(
      content: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(
              left: model.gen == 0 ? 15 : 30,
              top: 15,
            ),
            padding: const EdgeInsets.all(8),
            decoration: BoxDecoration(
              border: Border.all(color: primaryColor),
              borderRadius: BorderRadius.circular(10),
              color: Colors.white,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('${model.id} | ${model.name} | ${model.grade}'),
                Text(
                  '個人業績：${PriceTools.getCurrencyFormatted(
                        model.pv2.toString(),
                        null,
                        isPV: true,
                      ) ?? ''}',
                  style: const TextStyle(color: Color(0xFF8fc31f)),
                ),
                if (showRPV)
                  Text(
                    '個人組織：${PriceTools.getCurrencyFormatted(
                          model.rPV.toString(),
                          null,
                          isPV: true,
                        ) ?? ''}',
                    style: const TextStyle(color: Color(0xFF8fc31f)),
                  ),
                Text(
                  '整組業績：${PriceTools.getCurrencyFormatted(
                        model.pv.toString(),
                        null,
                        isPV: true,
                      ) ?? ''}',
                  style: const TextStyle(color: Color(0xFF00A650)),
                ),
                if (showTPV)
                  Text(
                    '累積業績：${PriceTools.getCurrencyFormatted(
                          model.tPV.toString(),
                          null,
                          isPV: true,
                        ) ?? ''}',
                    style: const TextStyle(color: Color(0xFF00A650)),
                  ),
              ],
            ),
          ),
          Positioned(
            left: model.gen == 0 ? 0 : 15,
            top: 0,
            child: Container(
              height: 30,
              width: 30,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                color: primaryColor.withOpacity(0.3),
              ),
              alignment: Alignment.center,
              child: Container(
                height: 25,
                width: 25,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: primaryColor,
                ),
                alignment: Alignment.center,
                child: Text(
                  model.gen.toString(),
                  style: const TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
        ],
      ),
      children: model.child.map(_treeNode).toList(),
    );
  }
}
