class ArticleContentResponse {
  int? iD;
  String? postDate;
  String? postContent;
  String? postTitle;
  String? postStatus;
  String? postName;
  String? postType;
  String? image;
  List<Tags>? tags;
  bool? lock;
  String? link;

  ArticleContentResponse({
    this.iD,
    this.postDate,
    this.postContent,
    this.postTitle,
    this.postStatus,
    this.postName,
    this.postType,
    this.image,
    this.tags,
    this.lock,
    this.link,
  });

  ArticleContentResponse.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    postDate = json['post_date'];
    postContent = json['post_content'];
    postTitle = json['post_title'];
    postStatus = json['post_status'];
    postName = json['post_name'];
    postType = json['post_type'];
    image = json['image'].toString();
    if (json['tags'] != null) {
      tags = <Tags>[];
      json['tags'].forEach((v) {
        tags!.add(Tags.fromJson(v));
      });
    }
    lock = json['lock'];
    link = json['link'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = iD;
    data['post_date'] = postDate;
    data['post_content'] = postContent;
    data['post_title'] = postTitle;
    data['post_status'] = postStatus;
    data['post_name'] = postName;
    data['post_type'] = postType;
    data['image'] = image;
    if (tags != null) {
      data['tags'] = tags!.map((v) => v.toJson()).toList();
    }
    data['lock'] = lock;
    data['link'] = link;
    return data;
  }
}

class Tags {
  int? id;
  String? name;

  Tags({this.id, this.name});

  Tags.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    return data;
  }
}
