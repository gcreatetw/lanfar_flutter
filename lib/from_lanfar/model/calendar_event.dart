import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/src/provider.dart';

import '../../common/constants.dart';
import '../../models/index.dart';
import '../../services/services.dart';
import '../contants/constants.dart';
import 'general_state.dart';

class CalendarEvent extends ChangeNotifier {
  String? id;
  DateTime? startDate;
  DateTime? endDate;
  String? imageUrl;
  String? title;
  String? address;
  String? url;
  String? venue;
  CalendarCat? category;

  GeneralState state = GeneralState.loading;

  List<CalendarEvent> eventList = [];

  List<CalendarEvent> currentEventList = [];

  List<List<CalendarEvent>> categoryList = [];

  List<String> loadedMonths = [];

  Map<String, dynamic> subscribeMap = {
    '全部': 0,
    '桃園': 0,
    '台中': 0,
    '台南': 0,
    '高雄': 0,
  };

  final Services _service = Services();

  CalendarEvent({
    required this.id,
    required this.startDate,
    required this.endDate,
    required this.imageUrl,
    required this.title,
    required this.address,
    required this.venue,
    required this.category,
    required this.url,
  });

  factory CalendarEvent.init() {
    return CalendarEvent(
      id: '',
      startDate: null,
      endDate: null,
      imageUrl: '',
      title: '',
      address: '',
      venue: '',
      category: CalendarCat(id: '', name: ''),
      url: '',
    );
  }

  factory CalendarEvent.fromJson(Map<String, dynamic> json) {
    String? catId;
    String? catName;
    var all = false;
    var allCat = CalendarCat(id: '', name: '');
    var celebration = false;
    var celebrationCat = CalendarCat(id: '', name: '');
    for (var element in json['categories']) {
      catId = element['id'].toString();
      catName = element['name'];
      // if (element['name'] == '全部') {
      //   all = true;
      //   allCat.id = element['id'].toString();
      //   allCat.name = element['name'];
      // }
      if (element['name'] == '節日') {
        celebration = true;
        celebrationCat.id = element['id'].toString();
        celebrationCat.name = element['name'];
      }
    }
    if (all) {
      catId = allCat.id;
      catName = allCat.name;
    }
    if (celebration) {
      catId = celebrationCat.id;
      catName = celebrationCat.name;
    }

    return CalendarEvent(
      id: json['id'].toString(),
      startDate: DateTime.parse(json['start_date']),
      endDate: DateTime.parse(json['end_date']),
      imageUrl: json['image'] == false ? '' : json['image']['url'],
      title: json['title'],
      address: json['venue'].toString() == '[]' ? '' : json['venue']['address'],
      url: json['url'],
      venue: json['venue'].toString() == '[]' ? '' : json['venue']['venue'],
      category: CalendarCat(
        id: catId,
        name: catName,
      ),
    );
  }

  Future<void> fetch(List<String> months,{bool refresh = false}) async {
    print(loadedMonths);
    print(months);
    if(!refresh){
      for (var element in loadedMonths) {
        if (months.contains(element)) {
          months.remove(element);
        }
      }
      if (months.isEmpty) return;
    }
    loadedMonths.addAll(months);
    loadedMonths = loadedMonths.toSet().toList();
    var start =
        '${months.first.substring(0, 4)}-${months.first.substring(4, 6)}-1 00:00:00';
    var end =
        '${months.last.substring(0, 4)}-${int.parse(months.last.substring(4, 6)) + 1}-1 00:00:00';
    if (months.last.substring(4, 6) == '12') {
      end = '${int.parse(months.last.substring(0, 4)) + 1}-1-1 00:00:00';
    }

    try {
      var page = 1;
      var canLoadMore = true;
      state = GeneralState.loading;
      // eventList = [];
      notifyListeners();
      while (canLoadMore) {
        var temp = (await _service.api.getCalendarEvent(
          page: page,
          startDate: start,
          // DateFormat('yyyy-MM-dd kk:mm:ss').format(
          //   DateTime.now().subtract(
          //     const Duration(days: 180),
          //   ),
          // ),
          endDate: end,
          // DateFormat('yyyy-MM-dd kk:mm:ss').format(
          //   DateTime.now().add(
          //     const Duration(days: 180),
          //   ),
          // ),
        ))!;
        for (var element in temp) {
          eventList.removeWhere((e) => e.id == element.id);
        }
        eventList.addAll(temp);
        try{
          eventList.sort((a,b) => a.startDate!.compareTo(b.startDate!));
        }catch(_){

        }
        if (temp.length < 50) {
          canLoadMore = false;
        }
        page++;
      }
      // eventList = (await _service.api.getCalendarEvent(
      //     page: 1,
      //     startDate: '2022-01-01 00:00:00',
      //     endDate: '2024-02-28 23:59:59'))!;

      categoryList = [];
      for (var i = 0; i < Constants.calenderLocations.length; i++) {
        if (i == 0) {
          categoryList.add([]);
        } else {
          var list = <CalendarEvent>[];
          for (var element in eventList) {
            if (element.category!.name == Constants.calenderLocations[i]) {
              list.add(element);
            }
          }
          categoryList.add(list);
        }
      }
      state = GeneralState.finish;
      notifyListeners();
    } catch (err) {
      state = GeneralState.error;
      notifyListeners();
    }
  }

  Future<void> getSubscription(BuildContext context) async {
    try {
      var rawMap = await _service.api
          .getSubscribe(context.read<UserModel>().user?.cookie);
      if (rawMap != null) {
        subscribeMap = rawMap['subscriptions'];
      }
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }

  Future<void> subscribe(BuildContext context, String category) async {
    try {
      var rawMap = await _service.api
          .subscribe(context.read<UserModel>().user?.cookie, category);
      if (rawMap != null) {
        subscribeMap[category] = rawMap['subscribe_status'];
      }
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }

  String checkEventStart() {
    var now = DateTime.now();
    if (now.isBefore(startDate!)) {
      return '未開始';
    }
    if (now.isAfter(endDate!)) {
      return '已結束';
    }
    return '進行中';
  }

  Color getColor() {
    var now = DateTime.now();
    if (now.isBefore(startDate!)) {
      return Colors.black45;
    }
    if (now.isAfter(endDate!)) {
      return Colors.black45;
    }
    return primaryColor;
  }
}

class CalendarCat {
  String? id;
  String? name;

  CalendarCat({
    required this.id,
    required this.name,
  });
}
