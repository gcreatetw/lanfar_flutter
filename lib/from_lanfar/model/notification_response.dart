import 'package:intl/intl.dart';

class NotificationResponse {
  My? my;
  My? discount;
  My? announce;
  String? error;

  NotificationResponse({
    this.my,
    this.discount,
    this.announce,
    this.error,
  });

  NotificationResponse.fromJson(Map<String, dynamic> json) {
    my = json['0'] != null ? My.fromJson(json['0']) : null;
    discount = json['1'] != null ? My.fromJson(json['1']) : null;
    announce = json['2'] != null ? My.fromJson(json['2']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (my != null) {
      data['0'] = my!.toJson();
    }
    if (discount != null) {
      data['1'] = discount!.toJson();
    }
    if (announce != null) {
      data['2'] = announce!.toJson();
    }
    return data;
  }
}

class My {
  List<NotificationModel>? list;

  My({this.list});

  My.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      list = <NotificationModel>[];
      json['data'].forEach((v) {
        list!.add(NotificationModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (list != null) {
      data['data'] = list!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class NotificationModel {
  String? id;
  String? type;
  String? title;
  String? webLink;
  String? appLink;
  String? image;
  String? readStatus;
  String? content;
  String? date;
  String? cat;

  NotificationModel({
    this.id,
    this.type,
    this.title,
    this.webLink,
    this.appLink,
    this.image,
    this.readStatus,
    this.content,
    this.date,
    this.cat,
  });

  NotificationModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    webLink = json['web_link'];
    appLink = json['app_link'];
    image = json['image'];
    readStatus = json['read_status'];

    if (json['title'] != null) {
      title = json['title'].toString();
    }
    if (json['content'] != null) {
      content = json['content'].toString();
    }
    if (json['date'] != null) {
      try {
        date = DateFormat('yyyy/MM/dd')
            .format(DateFormat('yyyy-MM-dd').parse(json['date']));
      } catch (_) {
        date = json['date'];
      }
    }
    cat = json['cat'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['type'] = type;
    data['title'] = title;
    data['web_link'] = webLink;
    data['app_link'] = appLink;
    data['image'] = image;
    data['read_status'] = readStatus;
    data['content'] = content;
    data['date'] = date;
    data['cat'] = cat;
    return data;
  }
}
