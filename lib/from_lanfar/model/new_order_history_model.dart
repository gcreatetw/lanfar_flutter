import 'package:flutter/cupertino.dart';

import '../../services/services.dart';
import 'general_state.dart';

class NewOrderHistoryModel with ChangeNotifier {
  GeneralState state = GeneralState.loading;
  List<NewOrderHistoryConfig> orderList = [];

  final _services = Services();

  Future<void> fetch({
    required String id,
    required String startDate,
    required String endDate,
    required int type,
  }) async {
    state = GeneralState.loading;
    notifyListeners();
    orderList = await _services.api.getOrderHistory(
          id: id,
          startDate: startDate,
          endDate: endDate,
          type: type,
        ) ??
        [];
    state = GeneralState.finish;
    notifyListeners();
  }
}

class NewOrderHistoryConfig {
  String? ordDate;
  String? ordNoArea;
  int? price;
  String? ordP2;
  String? ordp2type;
  String? areatitle;
  String? alias;
  String? dname;
  String? dtelno;
  String? daddr;
  String? state;
  String? detailUrl;

  NewOrderHistoryConfig({
    this.ordDate,
    this.ordNoArea,
    this.price,
    this.ordP2,
    this.ordp2type,
    this.areatitle,
    this.alias,
    this.dname,
    this.dtelno,
    this.daddr,
    this.state,
    this.detailUrl,
  });

  NewOrderHistoryConfig.fromJson(Map<String, dynamic> json) {
    ordDate = json['created_date'];
    ordNoArea = json['order_id'].toString();
    price = int.parse(json['price'].toString().replaceAll(',', ''));
    ordP2 = json['shipping_method'] == '(宅配)免運費' ? '2' : '';
    ordp2type = json['shipping_method'];
    areatitle = json['the_title'];
    alias = json['alias'];
    dname = json['the_title'];
    dtelno = json['dtelno'];
    daddr = json['shipping_address_1'];
    state = json['status'];
    detailUrl = json['order_detail_url'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['created_date'] = ordDate;
    data['order_id'] = ordNoArea;
    data['price'] = price;
    data['ord_p2'] = ordP2;
    data['ordp2type'] = ordp2type;
    data['areatitle'] = areatitle;
    data['alias'] = alias;
    data['dname'] = dname;
    data['dtelno'] = dtelno;
    data['daddr'] = daddr;
    data['state'] = state;
    return data;
  }
}
