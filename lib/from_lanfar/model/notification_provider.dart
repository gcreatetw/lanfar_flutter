import 'package:flutter/material.dart';

import '../../services/services.dart';
import 'general_state.dart';
import 'notification_response.dart';

class NotificationProvider extends ChangeNotifier {
  GeneralState state = GeneralState.finish;

  List<NotificationModel> all = [];
  List<NotificationModel> myList = [];
  List<NotificationModel> discountList = [];
  List<NotificationModel> announceList = [];

  Future<void> fetch(
    BuildContext context, {
    required String userId,
  }) async {
    state = GeneralState.loading;
    notifyListeners();
    var response = await Services().api.getNotificationList(userId: userId);

    if (response?.error != null) {
      state = GeneralState.error;
      notifyListeners();
      return;
    }

    all = [];
    myList = response?.my?.list ?? [];
    discountList = response?.discount?.list ?? [];
    announceList = response?.announce?.list ?? [];
    all.addAll(myList);
    all.addAll(discountList);
    all.addAll(announceList);
    all.sort((a, b) => b.date!.compareTo(a.date!));

    state = GeneralState.finish;
    notifyListeners();
  }
}
