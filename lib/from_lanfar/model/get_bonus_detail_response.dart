class GetBonusDetailResponse {
  bool? error;
  String? msg;
  Data? data;

  GetBonusDetailResponse({this.error, this.msg, this.data});

  GetBonusDetailResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    msg = json['msg'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['error'] = error;
    data['msg'] = msg;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  int? personalPerformance;
  int? organizationalPerformance;
  int? monthPV;
  UserItems? userItems;
  List<LeadItems>? leadItems;
  PayTotal? payTotal;
  String? accno;
  Dividend? dividend;
  dynamic competitionBonus;
  TravelBonus? travelBonus;

  Data(
      {this.personalPerformance,
      this.organizationalPerformance,
      this.monthPV,
      this.userItems,
      this.leadItems,
      this.payTotal,
      this.accno,
      this.dividend,
      this.competitionBonus,
      this.travelBonus});

  Data.fromJson(Map<String, dynamic> json) {
    personalPerformance = json['personal_performance'];
    organizationalPerformance = json['organizational_performance'];
    monthPV = json['month_pv'];
    userItems = json['userItems'] != null
        ? UserItems.fromJson(json['userItems'])
        : null;
    if (json['leadItems'] != null) {
      leadItems = <LeadItems>[];
      json['leadItems'].forEach((v) {
        leadItems!.add(LeadItems.fromJson(v));
      });
    }
    payTotal =
        json['pay_total'] != null ? PayTotal.fromJson(json['pay_total']) : null;
    accno = json['accno'];
    dividend =
        json['dividend'] != null ? Dividend.fromJson(json['dividend']) : null;
    competitionBonus = json['competition_bonus'];
    travelBonus = json['travel_bonus'] != null
        ? TravelBonus.fromJson(json['travel_bonus'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['personal_performance'] = personalPerformance;
    data['organizational_performance'] = organizationalPerformance;
    data['month_pv'] = monthPV;
    if (userItems != null) {
      data['userItems'] = userItems!.toJson();
    }
    if (leadItems != null) {
      data['leadItems'] = leadItems!.map((v) => v.toJson()).toList();
    }
    if (payTotal != null) {
      data['pay_total'] = payTotal!.toJson();
    }
    data['accno'] = accno;
    if (dividend != null) {
      data['dividend'] = dividend!.toJson();
    }
    data['competition_bonus'] = competitionBonus;
    if (travelBonus != null) {
      data['travel_bonus'] = travelBonus!.toJson();
    }
    return data;
  }
}

class UserItems {
  String? id;
  String? name;
  String? grade;
  int? cumulativePerformance;
  int? cumulativeLastMonth;
  int? currentMonthCpv;
  int? personalCPV;
  int? pay2;
  int? pay9;

  UserItems(
      {this.id,
      this.name,
      this.grade,
      this.cumulativePerformance,
      this.cumulativeLastMonth,
      this.currentMonthCpv,
      this.personalCPV,
      this.pay2,
      this.pay9});

  UserItems.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    grade = json['grade'];
    cumulativePerformance = json['cumulative_performance'];
    cumulativeLastMonth = json['cumulative_last_month'];
    currentMonthCpv = json['current_month_cpv'];
    personalCPV = json['personal_CPV'];
    pay2 = json['pay_2'];
    pay9 = json['pay_9'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['grade'] = grade;
    data['cumulative_performance'] = cumulativePerformance;
    data['cumulative_last_month'] = cumulativeLastMonth;
    data['current_month_cpv'] = currentMonthCpv;
    data['personal_CPV'] = personalCPV;
    data['pay_2'] = pay2;
    data['pay_9'] = pay9;
    return data;
  }
}

class LeadItems {
  double? leadershipPoints;
  int? pay3;
  List<Pay3Data>? pay3Data;

  LeadItems({this.leadershipPoints, this.pay3, this.pay3Data});

  LeadItems.fromJson(Map<String, dynamic> json) {
    leadershipPoints = json['leadership_points'];
    pay3 = json['pay_3'];
    if (json['pay_3_data'] != null) {
      pay3Data = <Pay3Data>[];
      json['pay_3_data'].forEach((v) {
        pay3Data!.add(Pay3Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['leadership_points'] = leadershipPoints;
    data['pay_3'] = pay3;
    if (pay3Data != null) {
      data['pay_3_data'] = pay3Data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Pay3Data {
  int? gen;
  int? orgpv3;
  int? prcnt;
  int? integration;
  int? pay3uni;

  Pay3Data({this.gen, this.orgpv3, this.prcnt, this.integration, this.pay3uni});

  Pay3Data.fromJson(Map<String, dynamic> json) {
    gen = json['gen'];
    orgpv3 = json['orgpv_3'];
    prcnt = json['prcnt'];
    integration = json['integration'];
    pay3uni = json['pay3uni'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['gen'] = gen;
    data['orgpv_3'] = orgpv3;
    data['prcnt'] = prcnt;
    data['integration'] = integration;
    data['pay3uni'] = pay3uni;
    return data;
  }
}

class PayTotal {
  List<String>? titles;
  List<dynamic>? values;

  PayTotal({this.titles, this.values});

  PayTotal.fromJson(Map<String, dynamic> json) {
    titles = json['titles'].cast<String>();
    values = json['values'].cast<dynamic>();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['titles'] = titles;
    data['values'] = values;
    return data;
  }
}

class Dividend {
  dynamic amountMonth;
  String? amountDueMonth;

  Dividend({this.amountMonth, this.amountDueMonth});

  Dividend.fromJson(Map<String, dynamic> json) {
    amountMonth = json['amount_month'];
    amountDueMonth = json['amount_due_month'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['amount_month'] = amountMonth;
    data['amount_due_month'] = amountDueMonth;
    return data;
  }
}

class TravelBonus {
  dynamic amountQuarter;
  dynamic cumulativeAmount;
  dynamic showOnList;

  TravelBonus({
    this.amountQuarter,
    this.cumulativeAmount,
    this.showOnList,
  });

  TravelBonus.fromJson(Map<String, dynamic> json) {
    amountQuarter = json['amount_quarter'];
    cumulativeAmount = json['cumulative_amount'];
    showOnList = json['show_on_list'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['amount_quarter'] = amountQuarter;
    data['cumulative_amount'] = cumulativeAmount;
    data['show_on_list'] = showOnList;
    return data;
  }
}
