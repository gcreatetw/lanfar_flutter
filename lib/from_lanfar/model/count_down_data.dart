import 'dart:async';

import 'package:flutter/cupertino.dart';

class CountDownData extends ChangeNotifier {
  int countDownValue = 0;
  Timer? timer;

  void countDown(Function resetQrcode) {
    countDownValue = 300;
    notifyListeners();

    timer = Timer.periodic(
      const Duration(seconds: 1),
      (Timer timer) {
        countDownValue--;
        notifyListeners();
        if (countDownValue == 0) {
          timer.cancel();
          countDown(resetQrcode);
          resetQrcode();
          notifyListeners();
        }
      },
    );
  }
}
