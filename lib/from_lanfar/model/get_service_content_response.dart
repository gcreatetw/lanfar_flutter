class GetServiceContentResponse {
  bool? error;
  String? msg;
  Data? data;

  GetServiceContentResponse({this.error, this.msg, this.data});

  GetServiceContentResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    msg = json['msg'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['error'] = error;
    data['msg'] = msg;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  String? dateCreated;
  String? consultType;
  String? consultText;
  String? consultStatus;
  List<ServiceContentBean>? interactiveList;

  Data(
      {this.dateCreated,
      this.consultType,
      this.consultText,
      this.consultStatus,
      this.interactiveList});

  Data.fromJson(Map<String, dynamic> json) {
    dateCreated = json['date_created'];
    consultType = json['consult_type'];
    consultText = json['consult_text'];
    consultStatus = json['consult_status'];
    if (json['interactive_list'] != null) {
      interactiveList = <ServiceContentBean>[];
      json['interactive_list'].forEach((v) {
        interactiveList!.add(ServiceContentBean.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['date_created'] = dateCreated;
    data['consult_type'] = consultType;
    data['consult_text'] = consultText;
    data['consult_status'] = consultStatus;
    if (interactiveList != null) {
      data['interactive_list'] =
          interactiveList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ServiceContentBean {
  String? dateCreated;
  String? consultText;
  bool? isAdmin;

  ServiceContentBean({this.dateCreated, this.consultText, this.isAdmin});

  ServiceContentBean.fromJson(Map<String, dynamic> json) {
    dateCreated = json['date_created'];
    consultText = json['consult_text'];
    isAdmin = json['is_admin'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['date_created'] = dateCreated;
    data['consult_text'] = consultText;
    data['is_admin'] = isAdmin;
    return data;
  }
}
