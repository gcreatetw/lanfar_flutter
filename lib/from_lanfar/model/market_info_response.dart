class GetMarketingInfoResponse {
  bool? error;
  String? msg;
  Data? data;

  GetMarketingInfoResponse({this.error, this.msg, this.data});

  GetMarketingInfoResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    msg = json['msg'];
    if (json['data'].toString().startsWith('{')) {
      data = json['data'] != null ? Data.fromJson(json['data']) : null;
    } else {
      error = true;
      data = Data();
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['error'] = error;
    data['msg'] = msg;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  String? prefixUrl;
  String? inviteText;
  Groups? groups;
  Map<String, dynamic>? products;
  String? description;

  Data({
    this.inviteText,
    this.groups,
    this.products,
    this.description,
  });

  Data.fromJson(Map<String, dynamic> json) {
    prefixUrl = json['prefix_url'];
    inviteText = json['invite_text'];
    groups = json['groups'] != null ? Groups.fromJson(json['groups']) : null;
    products = json['products'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['prefix_url'] = prefixUrl;
    data['invite_text'] = inviteText;
    if (groups != null) {
      data['groups'] = groups!.toJson();
    }
    data['products'] = products;
    return data;
  }
}

class Groups {
  MarketingBean? one;
  MarketingBean? two;
  MarketingBean? three;
  MarketingBean? four;
  MarketingBean? five;

  Groups({
    this.one,
    this.two,
    this.three,
    this.four,
    this.five,
  });

  Groups.fromJson(Map<String, dynamic> json) {
    one = json['1'] != null ? MarketingBean.fromJson(json['1']) : null;
    two = json['2'] != null ? MarketingBean.fromJson(json['2']) : null;
    three = json['3'] != null ? MarketingBean.fromJson(json['3']) : null;
    four = json['4'] != null ? MarketingBean.fromJson(json['4']) : null;
    five = json['5'] != null ? MarketingBean.fromJson(json['5']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (one != null) {
      data['1'] = one!.toJson();
    }
    if (two != null) {
      data['2'] = two!.toJson();
    }
    if (three != null) {
      data['3'] = three!.toJson();
    }
    if (four != null) {
      data['4'] = four!.toJson();
    }
    if (five != null) {
      data['5'] = five!.toJson();
    }
    return data;
  }
}

class MarketingBean {
  List<Products>? products;

  MarketingBean({this.products});

  MarketingBean.fromJson(Map<String, dynamic> json) {
    if (json['products'] != null) {
      products = <Products>[];
      json['products'].forEach((v) {
        products!.add(Products.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (products != null) {
      data['products'] = products!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Products {
  String? id;
  String? sku;
  String? name;

  Products({this.id, this.sku, this.name});

  Products.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    sku = json['sku'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['sku'] = sku;
    data['name'] = name;
    return data;
  }
}
