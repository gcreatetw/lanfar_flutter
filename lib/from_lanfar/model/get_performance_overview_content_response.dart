class GetPerformanceOverviewContentResponse {
  bool? error;
  String? msg;
  Data? data;

  GetPerformanceOverviewContentResponse({this.error, this.msg, this.data});

  GetPerformanceOverviewContentResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    msg = json['msg'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['error'] = error;
    data['msg'] = msg;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  List<Tree>? tree;
  FirstData? firstData;
  List<Datatable>? datatable;

  Data({this.tree, this.firstData, this.datatable});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['tree'] != null) {
      tree = <Tree>[];
      json['tree'].forEach((v) {
        tree!.add(Tree.fromJson(v));
      });
    }
    firstData = json['first_data'] != null
        ? FirstData.fromJson(json['first_data'])
        : null;
    if (json['datatable'] != null) {
      datatable = <Datatable>[];
      json['datatable'].forEach((v) {
        datatable!.add(Datatable.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (tree != null) {
      data['tree'] = tree!.map((v) => v.toJson()).toList();
    }
    if (firstData != null) {
      data['first_data'] = firstData!.toJson();
    }
    if (datatable != null) {
      data['datatable'] = datatable!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Tree {
  int? serno;
  int? gen;
  String? id;
  String? name;
  String? grade1;
  String? pId;
  String? mark;
  int? pv2;
  int? tPv;
  int? pv;
  int? rPv;
  String? grade;
  String? nname;
  List<Tree>? child;

  Tree(
      {this.serno,
      this.gen,
      this.id,
      this.name,
      this.grade1,
      this.pId,
      this.mark,
      this.pv2,
      this.tPv,
      this.pv,
      this.rPv,
      this.grade,
      this.nname,
      this.child});

  Tree.fromJson(Map<String, dynamic> json) {
    serno = json['serno'];
    gen = json['gen'];
    id = json['id'];
    name = json['name'];
    grade1 = json['grade_1'];
    pId = json['p_id'];
    mark = json['mark'];
    pv2 = json['pv_2'];
    tPv = json['t_pv'];
    pv = json['pv'];
    rPv = json['r_pv'];
    grade = json['grade'];
    nname = json['nname'];
    if (json['child'] != null) {
      child = <Tree>[];
      json['child'].forEach((v) {
        child!.add(Tree.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['serno'] = serno;
    data['gen'] = gen;
    data['id'] = id;
    data['name'] = name;
    data['grade_1'] = grade1;
    data['p_id'] = pId;
    data['mark'] = mark;
    data['pv_2'] = pv2;
    data['t_pv'] = tPv;
    data['pv'] = pv;
    data['r_pv'] = rPv;
    data['grade'] = grade;
    data['nname'] = nname;
    if (child != null) {
      data['child'] = child!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class FirstData {
  String? id;
  String? name;
  String? grade;
  int? pv2;
  int? pv;
  String? pId;
  String? dateIv;
  int? dataCount;

  FirstData(
      {this.id,
      this.name,
      this.grade,
      this.pv2,
      this.pv,
      this.pId,
      this.dateIv,
      this.dataCount});

  FirstData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    grade = json['grade'];
    pv2 = json['pv_2'];
    pv = json['pv'];
    pId = json['p_id'];
    dateIv = json['date_iv'];
    dataCount = json['data_count'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['grade'] = grade;
    data['pv_2'] = pv2;
    data['pv'] = pv;
    data['p_id'] = pId;
    data['date_iv'] = dateIv;
    data['data_count'] = dataCount;
    return data;
  }
}

class Datatable {
  int? serno;
  int? gen;
  String? id;
  String? name;
  String? grade1;
  String? pId;
  String? mark;
  int? pv2;
  int? tPv;
  int? pv;
  int? rPv;
  String? grade;
  String? nname;

  Datatable(
      {this.serno,
      this.gen,
      this.id,
      this.name,
      this.grade1,
      this.pId,
      this.mark,
      this.pv2,
      this.tPv,
      this.pv,
      this.rPv,
      this.grade,
      this.nname});

  Datatable.fromJson(Map<String, dynamic> json) {
    serno = json['serno'];
    gen = json['gen'];
    id = json['id'];
    name = json['name'];
    grade1 = json['grade_1'];
    pId = json['p_id'];
    mark = json['mark'];
    pv2 = json['pv_2'];
    tPv = json['t_pv'];
    pv = json['pv'];
    rPv = json['r_pv'];
    grade = json['grade'];
    nname = json['nname'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['serno'] = serno;
    data['gen'] = gen;
    data['id'] = id;
    data['name'] = name;
    data['grade_1'] = grade1;
    data['p_id'] = pId;
    data['mark'] = mark;
    data['pv_2'] = pv2;
    data['t_pv'] = tPv;
    data['pv'] = pv;
    data['r_pv'] = rPv;
    data['grade'] = grade;
    data['nname'] = nname;
    return data;
  }
}
