class GetMonarchPointResponse {
  bool? error;
  String? msg;
  Data? data;

  GetMonarchPointResponse({this.error, this.msg, this.data});

  GetMonarchPointResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    msg = json['msg'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['error'] = error;
    data['msg'] = msg;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  String? id;
  String? yyy;
  int? dataCount;
  double? sumpoint;
  List<DataDetail>? data;

  Data({this.id, this.yyy, this.dataCount, this.sumpoint, this.data});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    yyy = json['yyy'];
    dataCount = json['data_count'];
    if(json['sumpoint'] != null){
      sumpoint = double.parse(json['sumpoint'].toString());
    }
    if (json['data'] != null) {
      data = <DataDetail>[];
      json['data'].forEach((v) {
        data!.add(DataDetail.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['yyy'] = yyy;
    data['data_count'] = dataCount;
    data['sumpoint'] = sumpoint;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataDetail {
  String? mm;
  String? grade;
  String? gradetitle;
  double? point;

  DataDetail({this.mm, this.grade, this.gradetitle, this.point});

  DataDetail.fromJson(Map<String, dynamic> json) {
    mm = json['mm'];
    grade = json['grade'];
    gradetitle = json['gradetitle'];
    if(json['point'] != null){
      point = double.parse(json['point'].toString());
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['mm'] = mm;
    data['grade'] = grade;
    data['gradetitle'] = gradetitle;
    data['point'] = point;
    return data;
  }
}
