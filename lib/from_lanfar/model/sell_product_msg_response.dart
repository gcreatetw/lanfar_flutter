class SellProductMsgResponse {
  bool? error;
  bool? allow;
  String? msg;

  SellProductMsgResponse({this.error, this.allow, this.msg});

  SellProductMsgResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    allow = json['allow'];
    msg = json['msg'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['error'] = error;
    data['allow'] = allow;
    data['msg'] = msg;
    return data;
  }
}
