import 'package:flutter/material.dart';

import '../../modules/dynamic_layout/config/banner_config.dart';
import '../../services/services.dart';
import 'general_state.dart';

class BannerData extends ChangeNotifier {
  List<BannerItemConfig> bannerList = [];
  GeneralState state = GeneralState.loading;

  final Services _service = Services();

  BannerItemConfig fromJson(Map<String, dynamic> json) {
    return BannerItemConfig(
      id: json['id'],
      image: json['image'],
      url: json['link'],
    );
  }

  Future<void> fetch() async {
    state = GeneralState.loading;
    notifyListeners();
    try {
      bannerList = (await _service.api.getBanner())!;
      state = GeneralState.finish;
      notifyListeners();
    } catch (e) {
      notifyListeners();
    }
  }
}
