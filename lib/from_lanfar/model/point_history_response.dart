class PointHistoryResponse {
  bool? error;
  String? msg;
  String? points;
  String? expireDate;
  List<Earnings>? earnings;
  List<Exchanges>? exchanges;

  PointHistoryResponse(
      {this.error,
        this.msg,
        this.points,
        this.expireDate,
        this.earnings,
        this.exchanges});

  PointHistoryResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    msg = json['msg'];
    points = json['points'].toString();
    expireDate = json['expire_date'];
    if (json['earnings'] != null) {
      earnings = <Earnings>[];
      json['earnings'].forEach((v) {
        earnings!.add(Earnings.fromJson(v));
      });
    }
    if (json['exchanges'] != null) {
      exchanges = <Exchanges>[];
      json['exchanges'].forEach((v) {
        exchanges!.add(Exchanges.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['error'] = error;
    data['msg'] = msg;
    data['points'] = points;
    data['expire_date'] = expireDate;
    if (earnings != null) {
      data['earnings'] = earnings!.map((v) => v.toJson()).toList();
    }
    if (exchanges != null) {
      data['exchanges'] = exchanges!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Earnings {
  String? id;
  String? userId;
  String? action;
  String? orderId;
  String? amount;
  String? dateEarning;
  // Null? cancelled;
  String? description;
  String? info;
  int? prevAmount;

  Earnings(
      {this.id,
        this.userId,
        this.action,
        this.orderId,
        this.amount,
        this.dateEarning,
        // this.cancelled,
        this.description,
        this.info,
        this.prevAmount});

  Earnings.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    action = json['action'];
    orderId = json['order_id'];
    amount = json['amount'];
    dateEarning = json['date_earning'];
    // cancelled = json['cancelled'];
    description = json['description'];
    info = json['info'];
    prevAmount = json['prev_amount'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['user_id'] = userId;
    data['action'] = action;
    data['order_id'] = orderId;
    data['amount'] = amount;
    data['date_earning'] = dateEarning;
    // data['cancelled'] = cancelled;
    data['description'] = description;
    data['info'] = info;
    data['prev_amount'] = prevAmount;
    return data;
  }
}

class Exchanges {
  String? code;
  String? status;
  String? expireDate;
  String? usedUserId;
  String? usedUsername;
  String? usedDisplayName;
  String? usedOrderId;

  Exchanges(
      {this.code,
        this.status,
        this.expireDate,
        this.usedUserId,
        this.usedUsername,
        this.usedDisplayName,
        this.usedOrderId});

  Exchanges.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    status = json['status'];
    expireDate = json['expire_date'];
    usedUserId = json['used_user_id'];
    usedUsername = json['used_username'];
    usedDisplayName = json['used_display_name'];
    usedOrderId = json['used_order_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['code'] = code;
    data['status'] = status;
    data['expire_date'] = expireDate;
    data['used_user_id'] = usedUserId;
    data['used_username'] = usedUsername;
    data['used_display_name'] = usedDisplayName;
    data['used_order_id'] = usedOrderId;
    return data;
  }
}
