class RemoveCouponResponseModel {
  String code;
  String message;

  RemoveCouponResponseModel({
    required this.code,
    required this.message,
  });
}
