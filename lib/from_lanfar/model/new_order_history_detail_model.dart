import 'package:flutter/cupertino.dart';
import '../../services/services.dart';

import 'general_state.dart';

class NewOrderHistoryDetailModel extends ChangeNotifier {
  GeneralState state = GeneralState.loading;
  final _services = Services();
  int count = 0;
  int totalPrice = 0;
  OrderDetail? orderList;
  List<Orderdata> products = [];
  List<CouponsData> coupons = [];
  String payAgain = '';

  Future<void> fetch(
    String orderId,
    String username,
  ) async {
    state = GeneralState.loading;
    notifyListeners();
    try {
      var response = await _services.api.getOrderHistoryDetail(
        id: orderId,
        username: username,
      );
      orderList = response?.order;
      if (orderList?.orderTotal != null) {
        totalPrice += int.parse(orderList!.orderTotal!);
      }
      // if (orderList?.shippingTotal != null) {
      //   totalPrice += int.parse(orderList!.shippingTotal!);
      // }
      products = [];
      count = 0;
      for (var element in response!.lines?.keys.toList() ?? []) {
        count += response.lines![element]['quantity'] as int;

        var pv = 0;
        if (response.lines![element]['pv'].toString().isNotEmpty) {
          pv = int.parse(response.lines![element]['pv'].toString());
        }
        products.add(
          Orderdata(
            prodNo: response.lines![element]['id'].toString(),
            prodName: response.lines![element]['name'],
            ordQty: response.lines![element]['quantity'],
            prodPrice: response.lines![element]['total'],
            prodPv: pv,
            image: response.lines![element]['image'],
            sku: response.lines![element]['sku'],
            type: response.lines![element]['type'],
          ),
        );
      }
      coupons = response.coupons ?? [];
      payAgain = response.payAgain!.isEmpty || response.payAgain == 'false'
          ? ''
          : response.payAgain.toString();
    } catch (e) {
      state = GeneralState.error;
      notifyListeners();
      return;
    }
    state = GeneralState.finish;
    notifyListeners();
  }
}

// class Result {
//   int? count;
//   List<Orderlist>? orderlist;
//
//   Result({this.count, this.orderlist});
//
//   Result.fromJson(Map<String, dynamic> json) {
//     count = json['count'];
//     if (json['orderlist'] != null) {
//       orderlist = <Orderlist>[];
//       json['orderlist'].forEach((v) {
//         orderlist!.add(Orderlist.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final data = <String, dynamic>{};
//     data['count'] = count;
//     if (orderlist != null) {
//       data['orderlist'] = orderlist!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }

class OrderResponse {
  bool? error;
  int? status;
  String? msg;
  OrderDetail? order;
  Map<String, dynamic>? lines;
  List<CouponsData>? coupons;
  String? payAgain;

  OrderResponse({
    this.error,
    this.status,
    this.msg,
    this.order,
    this.lines,
    this.coupons,
    this.payAgain,
  });

  OrderResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    status = json['status'];
    msg = json['msg'];
    order = json['order'] != null ? OrderDetail.fromJson(json['order']) : null;
    lines = json['lines'];
    if (json['coupons'] != null) {
      coupons = <CouponsData>[];
      json['coupons'].forEach((v) {
        coupons!.add(CouponsData.fromJson(v));
      });
    }
    payAgain =( json['pay_again'] ?? '').toString();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['error'] = error;
    data['status'] = status;
    data['msg'] = msg;
    if (order != null) {
      data['order'] = order!.toJson();
    }
    if (lines != null) {
      data['lines'] = lines;
    }
    if (coupons != null) {
      data['coupons'] = coupons!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OrderDetail {
  String? orderId;
  String? orderStatus;
  int? orderProcess;
  String? orderInvoiceNumber;
  String? orderDate;
  String? erpOrderNumber;
  String? userLogin;
  String? orderType;
  String? orderSource;
  String? shippingMethod;
  String? shippingName;
  String? shippingPhone;
  String? shippingAddress;
  String? billingInvoiceDelivery;
  String? customerNote;
  String? inviter;
  String? insteadOrder;
  String? billingInvoiceType;
  String? billingCarrierNumber;
  String? creditCardNumber;
  String? billingConfirmSpeedSw;
  String? orderTotal;
  String? pvTotal;
  String? paymentMethod;
  String? shippingTotal;
  String? orderCat;
  String? billingNeedBagAndDm;
  String? billingInvoiceNo;
  List<Map>? tickets;
  String? orderSubTotal;
  String? discountTotal;

  OrderDetail({
    this.orderId,
    this.orderStatus,
    this.orderProcess,
    this.orderInvoiceNumber,
    this.orderDate,
    this.erpOrderNumber,
    this.userLogin,
    this.orderType,
    this.orderSource,
    this.shippingMethod,
    this.shippingName,
    this.shippingPhone,
    this.shippingAddress,
    this.billingInvoiceDelivery,
    this.customerNote,
    this.inviter,
    this.insteadOrder,
    this.billingInvoiceType,
    this.billingCarrierNumber,
    this.creditCardNumber,
    this.billingConfirmSpeedSw,
    this.orderTotal,
    this.pvTotal,
    this.paymentMethod,
    this.shippingTotal,
    this.orderCat,
    this.billingNeedBagAndDm,
    this.tickets,
    this.orderSubTotal,
    this.discountTotal,
  });

  OrderDetail.fromJson(Map<String, dynamic> json) {
    orderId = json['order_id'].toString();
    orderStatus = json['order_status'].toString();
    orderProcess = json['order_process'];
    orderInvoiceNumber = json['order_invoice_number'].toString();
    orderDate = json['order_date'].toString();
    erpOrderNumber = json['erp_order_number'].toString();
    userLogin = json['user_login'].toString();
    orderType = json['order_type'].toString();
    orderSource = json['order_source'].toString();
    shippingMethod = json['shipping_method'].toString();
    shippingName = json['shipping_name'].toString();
    shippingPhone = json['shipping_phone'].toString();
    shippingAddress = json['shipping_address'].toString();
    billingInvoiceDelivery = json['billing_invoice_delivery'].toString();
    customerNote = json['customer_note'].toString();
    inviter = json['inviter'].toString();
    insteadOrder = json['instead_order'].toString();
    billingInvoiceType = json['billing_invoice_type'].toString();
    billingCarrierNumber = json['billing_carrier_number'].toString();
    creditCardNumber = json['credit_card_number'].toString();
    billingConfirmSpeedSw = json['billing_confirmSpeed_sw'].toString();
    orderTotal = json['order_total'].toString();
    pvTotal = json['pv_total'].toString();
    paymentMethod = json['payment_method'].toString();
    shippingTotal = json['shipping_total'].toString();
    orderCat = json['order_cat'].toString();
    billingNeedBagAndDm = json['billing_need_bag_and_dm'].toString();
    billingInvoiceNo = json['billing_invoice_no'].toString();
    orderSubTotal =  json['order_subtotal'].toString();
    discountTotal =  json['discount_total'].toString();
    if (json['tickets'] != null) {
      tickets = <Map>[];
      json['tickets'].forEach((v) {
        tickets!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['order_id'] = orderId;
    data['order_status'] = orderStatus;
    data['order_process'] = orderProcess;
    data['order_invoice_number'] = orderInvoiceNumber;
    data['order_date'] = orderDate;
    data['erp_order_number'] = erpOrderNumber;
    data['user_login'] = userLogin;
    data['order_type'] = orderType;
    data['order_source'] = orderSource;
    data['shipping_method'] = shippingMethod;
    data['shipping_name'] = shippingName;
    data['shipping_phone'] = shippingPhone;
    data['shipping_address'] = shippingAddress;
    data['billing_invoice_delivery'] = billingInvoiceDelivery;
    data['customer_note'] = customerNote;
    data['inviter'] = inviter;
    data['instead_order'] = insteadOrder;
    data['billing_invoice_type'] = billingInvoiceType;
    data['billing_carrier_number'] = billingCarrierNumber;
    data['credit_card_number'] = creditCardNumber;
    data['billing_confirmSpeed_sw'] = billingConfirmSpeedSw;
    data['order_total'] = orderTotal;
    data['pv_total'] = pvTotal;
    data['payment_method'] = paymentMethod;
    data['shipping_total'] = shippingTotal;
    data['order_cat'] = orderCat;
    data['billing_need_bag_and_dm'] = billingNeedBagAndDm;
    return data;
  }
}

// class Orderlist {
//   String? ordDate;
//   String? dateIv;
//   String? ordNoArea;
//   String? invNo;
//   String? ordp1type;
//   String? ordp1way;
//   String? ordP2;
//   String? ordp2type;
//   String? areatitle;
//   String? alias;
//   String? dname;
//   String? dtelno;
//   String? daddr;
//   String? ordp2invno;
//   String? ordp3title;
//   String? remark;
//   bool? ordSpeed;
//   int? price;
//   int? pv;
//   int? sumordQty;
//   int? ordPrice;
//   int? deliveryFee;
//   List<Orderdata>? orderdata;
//
//   Orderlist(
//       {this.ordDate,
//       this.dateIv,
//       this.ordNoArea,
//       this.invNo,
//       this.ordp1type,
//       this.ordp1way,
//       this.ordP2,
//       this.ordp2type,
//       this.areatitle,
//       this.alias,
//       this.dname,
//       this.dtelno,
//       this.daddr,
//       this.ordp2invno,
//       this.ordp3title,
//       this.remark,
//       this.ordSpeed,
//       this.price,
//       this.pv,
//       this.sumordQty,
//       this.ordPrice,
//       this.deliveryFee,
//       this.orderdata});
//
//   Orderlist.fromJson(Map<String, dynamic> json) {
//     ordDate = json['ord_date'];
//     dateIv = json['date_iv'];
//     ordNoArea = json['ord_no_area'];
//     invNo = json['inv_no'];
//     ordp1type = json['ordp1type'];
//     ordp1way = json['ordp1way'];
//     ordP2 = json['ord_p2'];
//     ordp2type = json['ordp2type'];
//     areatitle = json['areatitle'];
//     alias = json['alias'];
//     dname = json['dname'];
//     dtelno = json['dtelno'];
//     daddr = json['daddr'];
//     ordp2invno = json['ordp2invno'];
//     ordp3title = json['ordp3title'];
//     remark = json['remark'];
//     ordSpeed = json['ord_speed'];
//     price = json['price'];
//     pv = json['pv'];
//     sumordQty = json['sumord_qty'];
//     ordPrice = json['ord_price'];
//     deliveryFee = json['delivery_fee'];
//     if (json['orderdata'] != null) {
//       orderdata = <Orderdata>[];
//       json['orderdata'].forEach((v) {
//         orderdata!.add(Orderdata.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final data = <String, dynamic>{};
//     data['ord_date'] = ordDate;
//     data['date_iv'] = dateIv;
//     data['ord_no_area'] = ordNoArea;
//     data['inv_no'] = invNo;
//     data['ordp1type'] = ordp1type;
//     data['ordp1way'] = ordp1way;
//     data['ord_p2'] = ordP2;
//     data['ordp2type'] = ordp2type;
//     data['areatitle'] = areatitle;
//     data['alias'] = alias;
//     data['dname'] = dname;
//     data['dtelno'] = dtelno;
//     data['daddr'] = daddr;
//     data['ordp2invno'] = ordp2invno;
//     data['ordp3title'] = ordp3title;
//     data['remark'] = remark;
//     data['ord_speed'] = ordSpeed;
//     data['price'] = price;
//     data['pv'] = pv;
//     data['sumord_qty'] = sumordQty;
//     data['ord_price'] = ordPrice;
//     data['delivery_fee'] = deliveryFee;
//     if (orderdata != null) {
//       data['orderdata'] = orderdata!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
class Orderdata {
  String? prodNo;
  String? prodName;
  String? prodPrice;
  int? prodPv;
  int? ordQty;
  String? image;
  String? sku;
  String? type;

  Orderdata({
    this.prodNo,
    this.prodName,
    this.prodPrice,
    this.prodPv,
    this.ordQty,
    this.image,
    this.sku,
    this.type,
  });

  Orderdata.fromJson(Map<String, dynamic> json) {
    prodNo = json['prod_no'];
    prodName = json['prod_name'];
    prodPrice = json['prod_price'];
    prodPv = json['prod_pv'];
    ordQty = json['ord_qty'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['prod_no'] = prodNo;
    data['prod_name'] = prodName;
    data['prod_price'] = prodPrice;
    data['prod_pv'] = prodPv;
    data['ord_qty'] = ordQty;
    return data;
  }
}

class CouponsData {
  String? code;
  String? content;

  CouponsData({this.code, this.content});

  CouponsData.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    content = json['content'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['code'] = code;
    data['content'] = content;
    return data;
  }
}

class TicketData {
  String? name;
  String? seat;
  String? meal;

  TicketData({
    this.name,
    this.seat,
    this.meal,
  });

  TicketData.fromJson(Map<String, dynamic> json) {
    name = json['姓名'];
    seat = json['座位'];
    meal = json['餐點'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['姓名'] = name;
    data['座位'] = seat;
    data['餐點'] = meal;
    return data;
  }
}
