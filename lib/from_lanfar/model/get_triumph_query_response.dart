class GetTriumphQueryResponse {
  bool? error;
  String? msg;
  Data? data;

  GetTriumphQueryResponse({this.error, this.msg, this.data});

  GetTriumphQueryResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    msg = json['msg'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['error'] = error;
    data['msg'] = msg;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  String? id;
  String? name;
  String? grade;
  int? pv2;
  int? rPv;
  int? srPv;
  int? ssrPv;
  String? runtime;
  int? dataCount;
  List<DataTriumph>? data;

  Data(
      {this.id,
        this.name,
        this.grade,
        this.pv2,
        this.rPv,
        this.srPv,
        this.ssrPv,
        this.runtime,
        this.dataCount,
        this.data});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    grade = json['grade'];
    pv2 = json['pv_2'];
    rPv = json['r_pv'];
    srPv = json['sr_pv'];
    ssrPv = json['ssr_pv'];
    runtime = json['runtime'];
    dataCount = json['data_count'];
    if (json['data'] != null) {
      data = <DataTriumph>[];
      json['data'].forEach((v) {
        data!.add(DataTriumph.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['grade'] = grade;
    data['pv_2'] = pv2;
    data['r_pv'] = rPv;
    data['sr_pv'] = srPv;
    data['ssr_pv'] = ssrPv;
    data['runtime'] = runtime;
    data['data_count'] = dataCount;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataTriumph {
  String? id;
  String? name;
  String? oldGrade;
  String? newGrade;
  int? pv2;
  int? srPv;
  int? ssrPv;
  int? gen;
  String? dId;
  String? dName;
  String? dNewGrade;
  String? type;

  DataTriumph(
      {this.id,
        this.name,
        this.oldGrade,
        this.newGrade,
        this.pv2,
        this.srPv,
        this.ssrPv,
        this.gen,
        this.dId,
        this.dName,
        this.dNewGrade,
        this.type});

  DataTriumph.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    oldGrade = json['old_grade'];
    newGrade = json['new_grade'];
    pv2 = json['pv_2'];
    srPv = json['sr_pv'];
    ssrPv = json['ssr_pv'];
    gen = json['gen'];
    dId = json['d_id'];
    dName = json['d_name'];
    dNewGrade = json['d_new_grade'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['old_grade'] = oldGrade;
    data['new_grade'] = newGrade;
    data['pv_2'] = pv2;
    data['sr_pv'] = srPv;
    data['ssr_pv'] = ssrPv;
    data['gen'] = gen;
    data['d_id'] = dId;
    data['d_name'] = dName;
    data['d_new_grade'] = dNewGrade;
    data['type'] = type;
    return data;
  }
}
