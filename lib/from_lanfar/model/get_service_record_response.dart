class GetServiceRecordResponse {
  bool? error;
  List<ServiceRecordBean>? data;

  GetServiceRecordResponse({this.error, this.data});

  GetServiceRecordResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    if (json['data'] != null) {
      data = <ServiceRecordBean>[];
      json['data'].forEach((v) {
        data!.add(ServiceRecordBean.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['error'] = error;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ServiceRecordBean {
  int? entryId;
  int? formId;
  String? dateCreated;
  String? consultType;
  String? consultText;
  String? consultStatus;

  ServiceRecordBean(
      {this.entryId,
      this.formId,
      this.dateCreated,
      this.consultType,
      this.consultText,
      this.consultStatus});

  ServiceRecordBean.fromJson(Map<String, dynamic> json) {
    entryId = json['entry_id'];
    formId = json['form_id'];
    dateCreated = json['date_created'];
    consultType = json['consult_type'];
    consultText = json['consult_text'];
    consultStatus = json['consult_status'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['entry_id'] = entryId;
    data['form_id'] = formId;
    data['date_created'] = dateCreated;
    data['consult_type'] = consultType;
    data['consult_text'] = consultText;
    data['consult_status'] = consultStatus;
    return data;
  }
}
