class GetAddressResponse {
  bool? error;
  String? msg;
  List<AddressData>? data;

  GetAddressResponse({this.error, this.msg, this.data});

  GetAddressResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    msg = json['msg'];
    if (json['data'] != null) {
      data = <AddressData>[];
      json['data'].forEach((v) {
        data!.add(AddressData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['error'] = error;
    data['msg'] = msg;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AddressData {
  String? type;
  int? addressId;
  String? userId;
  String? shippingFirstName;
  String? shippingCountry;
  String? shippingAddress1;
  String? shippingPhone;
  String? addressInternalName;
  String? shippingIsDefaultAddress;
  String? shippingState;
  String? shippingCity;

  AddressData(
      {this.type,
        this.addressId,
        this.userId,
        this.shippingFirstName,
        this.shippingCountry,
        this.shippingAddress1,
        this.shippingPhone,
        this.addressInternalName,
        this.shippingIsDefaultAddress,
        this.shippingState,
        this.shippingCity});

  AddressData.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    addressId = int.parse(json['address_id'].toString());
    userId = json['user_id'].toString();
    shippingFirstName = json['shipping_first_name'];
    shippingCountry = json['shipping_country'];
    shippingAddress1 = json['shipping_address_1'];
    shippingPhone = json['shipping_phone'];
    addressInternalName = json['address_internal_name'];
    shippingIsDefaultAddress = json['shipping_is_default_address'];
    shippingState = json['shipping_state'];
    shippingCity = json['shipping_city'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['type'] = type;
    data['address_id'] = addressId;
    data['user_id'] = userId;
    data['shipping_first_name'] = shippingFirstName;
    data['shipping_country'] = shippingCountry;
    data['shipping_address_1'] = shippingAddress1;
    data['shipping_phone'] = shippingPhone;
    data['address_internal_name'] = addressInternalName;
    data['shipping_is_default_address'] = shippingIsDefaultAddress;
    data['shipping_state'] = shippingState;
    data['shipping_city'] = shippingCity;
    return data;
  }
}
