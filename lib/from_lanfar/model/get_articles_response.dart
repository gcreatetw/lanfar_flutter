class GetArticlesResponse {
  int? totalPages;
  String? paged;
  int? number;
  int? count;
  List<Posts>? posts;

  GetArticlesResponse(
      {this.totalPages, this.paged, this.number, this.count, this.posts});

  GetArticlesResponse.fromJson(Map<String, dynamic> json) {
    totalPages = json['total_pages'];
    paged = json['paged'];
    number = json['number'];
    count = json['count'];
    if (json['posts'] != null) {
      posts = <Posts>[];
      json['posts'].forEach((v) {
        posts!.add(Posts.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['total_pages'] = totalPages;
    data['paged'] = paged;
    data['number'] = number;
    data['count'] = count;
    if (posts != null) {
      data['posts'] = posts!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Posts {
  int? iD;
  String? postDate;
  String? postTitle;
  String? postStatus;
  String? postName;
  String? postType;
  String? image;
  List<Tags>? tags;
  bool? lock;

  Posts(
      {this.iD,
        this.postDate,
        this.postTitle,
        this.postStatus,
        this.postName,
        this.postType,
        this.image,
        this.tags,
        this.lock});

  Posts.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    postDate = json['post_date'];
    postTitle = json['post_title'];
    postStatus = json['post_status'];
    postName = json['post_name'];
    postType = json['post_type'];
    image = json['image'].toString();
    if (json['tags'] != null) {
      tags = <Tags>[];
      json['tags'].forEach((v) {
        tags!.add(Tags.fromJson(v));
      });
    }
    lock = json['lock'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = iD;
    data['post_date'] = postDate;
    data['post_title'] = postTitle;
    data['post_status'] = postStatus;
    data['post_name'] = postName;
    data['post_type'] = postType;
    data['image'] = image;
    if (tags != null) {
      data['tags'] = tags!.map((v) => v.toJson()).toList();
    }
    data['lock'] = lock;
    return data;
  }
}

class Tags {
  int? id;
  String? name;

  Tags({this.id, this.name});

  Tags.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    return data;
  }
}
