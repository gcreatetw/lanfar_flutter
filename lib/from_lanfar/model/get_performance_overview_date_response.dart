class GetPerformanceOverviewDateResponse {
  bool? error;
  String? msg;
  List<String>? data;

  GetPerformanceOverviewDateResponse({this.error, this.msg, this.data});

  GetPerformanceOverviewDateResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    msg = json['msg'];
    data = json['data'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['error'] = error;
    data['msg'] = msg;
    data['data'] = this.data;
    return data;
  }
}
