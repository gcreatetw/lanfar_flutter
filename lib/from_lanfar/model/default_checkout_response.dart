class DefaultCheckoutResponse {
  String? shippingMethod;
  String? invoiceDelivery;
  String? notify;

  DefaultCheckoutResponse({this.shippingMethod, this.invoiceDelivery});

  DefaultCheckoutResponse.fromJson(Map<String, dynamic> json) {
    shippingMethod = json['shipping_method'];
    invoiceDelivery = json['invoice_delivery'];
    notify = json['notify'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['shipping_method'] = shippingMethod;
    data['invoice_delivery'] = invoiceDelivery;
    return data;
  }
}
