import 'package:flutter/material.dart';
import 'package:lanfar/services/dependency_injection.dart';
import 'package:localstorage/localstorage.dart';

class BarcodeData extends ChangeNotifier{
  String code = '';

  void setCode(String value){
    code = value;
    notifyListeners();
    save(code);
  }

  Future<void> save(String code) async {
    final _storage = injector<LocalStorage>();
    await _storage.setItem('qrcode', code);
  }

  void read(){
    final _storage = injector<LocalStorage>();
    var rawCode = _storage.getItem('qrcode');
    if(rawCode != null){
      code = rawCode;
    }
    notifyListeners();
  }

}