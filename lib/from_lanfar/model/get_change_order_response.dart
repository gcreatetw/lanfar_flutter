class GetChangeOrderResponse {
  bool? error;
  String? msg;
  Data? data;

  GetChangeOrderResponse({this.error, this.msg, this.data});

  GetChangeOrderResponse.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    msg = json['msg'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['error'] = error;
    data['msg'] = msg;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  Header? header;
  List<Datatable>? datatable;

  Data({this.header, this.datatable});

  Data.fromJson(Map<String, dynamic> json) {
    header =
    json['header'] != null ? Header.fromJson(json['header']) : null;
    if (json['datatable'] != null) {
      datatable = <Datatable>[];
      json['datatable'].forEach((v) {
        datatable!.add(Datatable.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (header != null) {
      data['header'] = header!.toJson();
    }
    if (datatable != null) {
      data['datatable'] = datatable!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Header {
  int? dataCount;
  int? sumPv;

  Header({this.dataCount, this.sumPv});

  Header.fromJson(Map<String, dynamic> json) {
    dataCount = json['data_count'];
    sumPv = json['sum_pv'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['data_count'] = dataCount;
    data['sum_pv'] = sumPv;
    return data;
  }
}

class Datatable {
  int? ordPv;
  String? prodName;
  int? sumQty;
  List<Details>? details;

  Datatable({this.ordPv, this.prodName, this.sumQty, this.details});

  Datatable.fromJson(Map<String, dynamic> json) {
    ordPv = json['ord_pv'];
    prodName = json['prod_name'];
    sumQty = json['sum_qty'];
    if (json['details'] != null) {
      details = <Details>[];
      json['details'].forEach((v) {
        details!.add(Details.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ord_pv'] = ordPv;
    data['prod_name'] = prodName;
    data['sum_qty'] = sumQty;
    if (details != null) {
      data['details'] = details!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Details {
  String? ordNoArea;
  String? ordDate;
  String? invNo;

  Details({this.ordNoArea, this.ordDate, this.invNo});

  Details.fromJson(Map<String, dynamic> json) {
    ordNoArea = json['ord_no_area'];
    ordDate = json['ord_date'];
    invNo = json['inv_no'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ord_no_area'] = ordNoArea;
    data['ord_date'] = ordDate;
    data['inv_no'] = invNo;
    return data;
  }
}
