import 'dart:async';

import 'package:flutter/material.dart';

import '../../services/services.dart';
import '../model/point_history_response.dart';

class PointsPageProvider extends ChangeNotifier {
  bool loading = false;
  bool exchangedLoading = false;
  bool exchanging = false;
  String point = '';
  String expireDate = '';
  List<Earnings> earningList = [];
  List<Exchanges> exchangeList = [];
  TextEditingController pointController = TextEditingController();

  Future<void> fetchHistory(String cookie) async {
    loading = true;
    notifyListeners();
    var response = await Services().api.getPointHistory(cookie: cookie);
    point = response?.points ?? '0';
    pointController.text = point;
    expireDate = response?.expireDate ?? '';
    earningList = response?.earnings ?? [];
    // exchangeList = response?.exchanges ?? [];
    loading = false;
    notifyListeners();
  }

  Future<void> fetchExchanged(String cookie) async {
    exchangedLoading = true;
    notifyListeners();
    var response = await Services().api.getPointExchanged(cookie: cookie);
    exchangeList = response?.exchanges ?? [];
    exchangedLoading = false;
    notifyListeners();
  }

  Future<void> exchange(
    String cookie, {
    required Function(String) onError,
    required Function(String) onSuccess,
  }) async {
    exchanging = true;
    notifyListeners();
    var newPoint = int.parse(pointController.text);
    var response = await Services().api.exchangePoint(
          cookie: cookie,
          point: newPoint,
        );
    if (response['error']) {
      onError.call(response['msg']);
    } else {
      if (response['coupon']['code'] != null ||
          response['coupon']['code'].toString().isNotEmpty) {
        await fetchHistory(cookie);
        onSuccess.call('兌換成功');
        unawaited(fetchExchanged(cookie));
      } else {
        onSuccess.call(response['msg']);
      }
    }
    exchanging = false;
    notifyListeners();
  }

  @override
  void dispose() {
    pointController.dispose();
    super.dispose();
  }
}
