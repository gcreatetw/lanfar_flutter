import 'package:flutter/material.dart';
import '../../services/services.dart';

enum ArticleType {
  cat,
  tag,
}

extension ArticleTypeEx on ArticleType {
  String get name {
    switch (this) {
      case ArticleType.cat:
        return 'charging_cat';
      case ArticleType.tag:
        return 'charging_tag';
    }
  }
}

class ArticleListPageProvider with ChangeNotifier {
  bool loading = false;
  List<ArticleCatModel> cats = [];
  List<ArticleCatModel> tags = [];
  List<ArticleModel> articles = [];
  int totalPage = 1;
  int currentPage = 1;
  ArticleType type = ArticleType.cat;

  int? currentCatId = 130;
  int? currentTagId;

  bool isNews = false;

  var pageStart = 1;

  Future<void> fetch({required Function() onError}) async {
    loading = true;
    notifyListeners();
    await Future.wait([
      if (!isNews) fetchCats(onError: onError),
      if (!isNews) fetchTags(onError: onError),
      fetchArticles(
        id: currentCatId ?? 130,
        taxonomy: type,
        page: 1,
        onError: onError,
      ),
    ]);

    loading = false;
    notifyListeners();
  }

  Future<void> fetchCats({required Function() onError}) async {
    var response = await Services().api.getChargingCat();
    if (response == null) {
      onError.call();
      return;
    }
    cats = response;
  }

  Future<void> fetchTags({required Function() onError}) async {
    var response = await Services().api.getChargingTag();
    if (response == null) {
      onError.call();
      return;
    }
    tags = response;
  }

  Future<void> fetchArticles({
    required int id,
    required ArticleType taxonomy,
    required int page,
    required Function() onError,
  }) async {
    loading = true;
    notifyListeners();
    var response = await Services().api.getArticles(
          id: id,
          taxonomy: taxonomy.name,
          page: page,
          isNews: isNews,
        );
    if (response == null) {
      onError.call();
      return;
    }
    if (response.posts != null) {
      articles = [];
      for (var element in response.posts!) {
        articles.add(
          ArticleModel(
            id: element.iD ?? 0,
            date: element.postDate != null
                ? DateTime.parse(element.postDate!)
                : null,
            title: element.postTitle ?? '',
            status: element.postStatus ?? '',
            type: element.postType ?? '',
            image: element.image ?? '',
            lock: element.lock ?? true,
            tags: element.tags != null
                ? element.tags!
                    .map((e) => ArticleCatModel(
                          id: e.id ?? 0,
                          name: e.name ?? '',
                          taxonomy: 'charging_tag',
                        ))
                    .toList()
                : [],
          ),
        );
      }
    }
    currentPage = page;
    totalPage = response.totalPages ?? page;
    type = taxonomy;
    switch (taxonomy) {
      case ArticleType.cat:
        currentCatId = id;
        currentTagId = null;
        break;
      case ArticleType.tag:
        currentCatId = null;
        currentTagId = id;
        break;
    }
    loading = false;
    notifyListeners();
  }
}

class ArticleCatModel {
  int id;
  String name;
  String taxonomy;

  ArticleCatModel({
    required this.id,
    required this.name,
    required this.taxonomy,
  });
}

class ArticleModel {
  int id;
  DateTime? date;
  String title;
  String status;
  String type;
  String image;
  bool lock;
  List<ArticleCatModel> tags;

  ArticleModel({
    required this.id,
    required this.date,
    required this.title,
    required this.status,
    required this.type,
    required this.image,
    required this.lock,
    required this.tags,
  });
}
