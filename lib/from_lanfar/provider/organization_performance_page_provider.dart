import 'package:flutter/material.dart';

import '../../models/entities/index.dart';
import '../../services/services.dart';
import '../utils/utils.dart';
import 'performance_overview_page_provider.dart';

enum SearchState {
  none,
  searching,
  searched,
}

class OrganizationPerformancePageProvider with ChangeNotifier {
  User user;
  SearchState state = SearchState.none;

  List<PerformanceModel> dataList = [];
  List<PerformanceModel> tableList = [];
  List<PerformanceModel> tableListFiltered = [];

  int personalPerformance = 0;
  int organizationPerformance = 0;

  TextEditingController searchIdController = TextEditingController();
  TextEditingController searchNameController = TextEditingController();

  int? promote = 0;

  Function(String) onError;

  final Services _services = Services();

  OrganizationPerformancePageProvider({
    required this.user,
    required this.onError,
  });

  Future<void> fetchData(BuildContext context) async {
    if (searchIdController.text.isNotEmpty &&
        searchIdController.text.length != 7) {
      Utils.showSnackBar(context, text: '請輸入有效的下線編號');
      return;
    }
    if (searchNameController.text.isNotEmpty &&
        (searchNameController.text.length < 2 ||
            searchNameController.text.length > 7)) {
      Utils.showSnackBar(context, text: '請輸入有效的下線姓名');
      return;
    }
    state = SearchState.searching;
    notifyListeners();
    var response = await _services.api.getOrganizationPerformance(
      userName: user.username ?? '',
      grade: promote,
      id: searchIdController.text,
      name: searchNameController.text,
    );
    if (response!.error ?? true) {
      onError.call(response.msg ?? '');
      return;
    }
    if (response.data?.tree != null) {
      dataList = response.data!.tree!
          .map((e) => PerformanceModel.fromTree(e))
          .toList();
      tableList = [];
      for (var element in response.data!.datatable!) {
        tableList.add(
          PerformanceModel(
            serno: element.serno ?? 0,
            gen: element.gen ?? 0,
            id: element.id ?? '',
            name: element.name ?? '',
            grade: element.grade ?? '',
            pv2: element.pv2 ?? 0,
            pv: element.pv ?? 0,
            rPV: element.rPv ?? 0,
            tPV: element.tPv ?? 0,
            child: [],
          ),
        );
      }
      tableListFiltered = tableList;
      personalPerformance = response.data?.firstData?.pv2 ?? 0;
      organizationPerformance = response.data?.firstData?.pv ?? 0;
    }
    state = SearchState.searched;

    notifyListeners();
    return;
  }

  void search(String value) {
    tableListFiltered = tableList
        .where((element) =>
            element.serno.toString().contains(value) ||
            element.gen.toString().contains(value) ||
            element.id.toString().contains(value) ||
            element.name.toString().contains(value) ||
            element.grade.toString().contains(value) ||
            element.pv2.toString().contains(value) ||
            element.pv.toString().contains(value) ||
            element.rPV.toString().contains(value) ||
            element.tPV.toString().contains(value))
        .toList();
    notifyListeners();
  }
}
