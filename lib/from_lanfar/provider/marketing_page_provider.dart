import 'package:flutter/material.dart';

import '../../models/entities/index.dart';
import '../../services/index.dart';
import '../model/market_info_response.dart';

class MarketingPageProvider with ChangeNotifier {
  User user;

  bool updating = false;
  bool loading = false;
  TextEditingController inviteMessage = TextEditingController();
  String prefixUrl = 'https://www.lanfar.com/one_page/';
  String link = '';
  TextEditingController linkController = TextEditingController();

  List<Product> products = [];

  List<List<GroupProductModel>> groups = [];

  String? errorMsg;

  String? description;

  Future<void> fetch() async {
    loading = true;
    notifyListeners();
    await Future.wait([
      // fetchProducts(),
      fetchInfo(),
    ]);

    loading = false;
    notifyListeners();
  }

  // Future<void> fetchProducts() async {
  //   products = await Services().api.fetchProductsByCategory(
  //             categoryId: '61,77,59,62',
  //             page: 1,
  //           ) ??
  //       [];
  //   for(var i = 0;i < products.length;i++){
  //     if(products[i].attributes != null && products[i].attributes!.isNotEmpty){
  //       print(products[i].attributes);
  //     }
  //
  //
  //
  //   }
  // }

  Function(String) onError;

  MarketingPageProvider({
    required this.user,
    required this.onError,
  }) {
    link = '$prefixUrl?inviter=${user.username}';
    linkController.text = link;
  }

  Future<void> fetchInfo() async {
    var response =
        await Services().api.getMarketingInfo(userId: user.username ?? '');
    if (response != null) {
      if (response.error ?? false) {
        onError.call(response.msg ?? '');
        errorMsg = response.msg ?? '';
        return;
      }
      if (response.data?.products != null) {
        products = [];
        response.data?.products!.forEach((key, value) {
          var product = Product();
          product.id = key;
          product.name = value.toString();
          products.add(product);
        });
      }
      prefixUrl =
          response.data?.prefixUrl ?? 'https://www.lanfar.com/one_page/';
      link = '$prefixUrl?inviter=${user.username}';
      linkController.text = link;
      inviteMessage.text = response.data?.inviteText ?? '';
      groups.addAll([
        response.data!.groups!.one!.products!
            .map((e) => GroupProductModel.fromProduct(e))
            .toList(),
        response.data!.groups!.two!.products!
            .map((e) => GroupProductModel.fromProduct(e))
            .toList(),
        response.data!.groups!.three!.products!
            .map((e) => GroupProductModel.fromProduct(e))
            .toList(),
        response.data!.groups!.four!.products!
            .map((e) => GroupProductModel.fromProduct(e))
            .toList(),
        response.data!.groups!.five!.products!
            .map((e) => GroupProductModel.fromProduct(e))
            .toList(),
      ]);
      description = response.data?.description;
    }
  }

  void removeProduct(int index, String id) {
    groups[index].removeWhere((element) => element.id == id);
    notifyListeners();
  }

  void addProduct(int index, String id) {
    var product = products.firstWhere((element) => element.id == id);
    groups[index].add(
      GroupProductModel(
        name: product.name ?? '',
        id: product.id ?? '',
        sku: product.sku ?? '',
      ),
    );
    notifyListeners();
  }

  Future<void> update(
    String type, {
    required Function() onSuccess,
  }) async {
    updating = true;
    notifyListeners();
    var response = await Services().api.updateMarketingData(
          userName: user.username ?? '',
          type: type,
          inviteText: type == 'text' ? inviteMessage.text : null,
          products: type == 'product' ? groups : null,
        );
    print(response);
    if (response?['error'] == false) {
      onSuccess.call();
      updating = false;
      if (type == 'rest') {
        inviteMessage.text = response?['text'] ?? '';
      }

      notifyListeners();
      return;
    }
    onError.call('更新失敗，請稍後再試');
    updating = false;
    notifyListeners();
  }
}

class GroupProductModel {
  String name = '';
  String id = '';
  String sku = '';

  GroupProductModel({
    required this.name,
    required this.id,
    required this.sku,
  });

  GroupProductModel.fromProduct(Products products) {
    name = products.name ?? '';
    id = products.id ?? '';
    sku = products.sku ?? '';
  }
}
