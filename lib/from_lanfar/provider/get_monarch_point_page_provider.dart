import 'package:flutter/material.dart';

import '../../services/index.dart';
import '../model/get_monarch_point_response.dart';

class GetMonarchPointPageProvider with ChangeNotifier {
  bool loading = false;
  bool bodyLoading = false;
  String currentYear = DateTime.now().year.toString();
  List<String> yearList = [
    DateTime.now().year.toString(),
    (DateTime.now().year - 1).toString(),
    (DateTime.now().year - 2).toString()
  ];

  GetMonarchPointResponse? getMonarchPointResponse;

  Future<void> fetch({
    required String userId,
    required Function(String) onError,
    bool first = false,
  }) async {
    bodyLoading = true;
    if (first) {
      loading = true;
    }
    notifyListeners();
    getMonarchPointResponse = await Services().api.getMonarchPoint(
          userId: userId,
          date: currentYear,
        );

    if (getMonarchPointResponse?.error == true) {
      onError.call(getMonarchPointResponse?.msg ?? '');
      if (first) {
        loading = false;
      }
      bodyLoading = false;
      notifyListeners();
      return;
    }

    if (first) {
      loading = false;
    }
    bodyLoading = false;
    notifyListeners();
  }

  void setDate(
    String date, {
    required String userId,
    required Function(String) onError,
  }) {
    currentYear = date;
    fetch(userId: userId, onError: onError);
    notifyListeners();
  }
}
