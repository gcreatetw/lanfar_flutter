import 'package:flutter/material.dart';

import '../../models/entities/index.dart';
import '../../services/index.dart';
import '../model/get_service_record_response.dart';

class ServiceRecordPageProvider with ChangeNotifier {
  User user;
  Function() onError;

  ServiceRecordPageProvider({
    required this.user,
    required this.onError,
  });

  bool loading = false;

  List<ServiceRecordBean> recordList = [];
  TextEditingController searchController = TextEditingController();

  Future<void> fetch() async {
    loading = true;
    notifyListeners();
    var response = await Services().api.getServiceRecord(
          userId: user.username ?? '',
          search: searchController.text.isEmpty ? null : searchController.text,
        );

    if (response!.error ?? true) {
      onError.call();
      loading = false;
      notifyListeners();
      return;
    }

    recordList = response.data ?? [];
    loading = false;
    notifyListeners();
  }
}
