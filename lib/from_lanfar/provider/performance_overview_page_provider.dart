import 'package:flutter/material.dart';

import '../../models/entities/user.dart';
import '../../services/index.dart';
import '../model/get_performance_overview_content_response.dart';

class PerformanceOverviewPageProvider with ChangeNotifier {
  bool loading = false;
  String chooseDate = '即時業績';
  List<String> dateList = ['即時業績'];
  List<PerformanceModel> dataList = [];
  List<PerformanceModel> tableList = [];
  List<PerformanceModel> tableListFiltered = [];

  int personalPerformance = 0;
  int organizationPerformance = 0;

  User user;

  bool hasData = false;

  Function(String) onError;

  final Services _services = Services();

  PerformanceOverviewPageProvider({
    required this.user,
    required this.onError,
  });

  Future<void> fetch() async {
    loading = true;
    notifyListeners();
    await Future.wait([
      fetchDate(),
      fetchData(),
    ]);
    loading = false;
    notifyListeners();
  }

  Future<void> fetchDate() async {
    var response = await _services.api.getPerformanceOverviewDate();

    if (response!.error ?? true) {
      onError.call(response.msg ?? '');
      return;
    }
    dateList = response.data ?? [];
    dateList.insert(0, '即時業績');
    notifyListeners();
  }

  Future<void> fetchData() async {
    if (chooseDate == '即時業績') {
      var response = await _services.api
          .getCurrentPerformance(userName: user.username ?? '');
      if (response!.error ?? true) {
        onError.call(response.msg ?? '');
        return;
      }
      if (response.data?.tree != null) {
        if (response.data?.firstData?.dataCount != null &&
            response.data?.firstData?.dataCount != 0) {
          hasData = true;
        } else {
          hasData = false;
        }

        dataList = response.data!.tree!
            .map((e) => PerformanceModel.fromTree(e))
            .toList();
        tableList = [];
        for (var element in response.data?.datatable ?? []) {
          tableList.add(
            PerformanceModel(
              serno: element.serno ?? 0,
              gen: element.gen ?? 0,
              id: element.id ?? '',
              name: element.name ?? '',
              grade: element.grade ?? '',
              pv2: element.pv2 ?? 0,
              pv: element.pv ?? 0,
              rPV: element.rPv ?? 0,
              tPV: element.tPv ?? 0,
              child: [],
            ),
          );
        }
        tableListFiltered = tableList;
        personalPerformance = response.data?.firstData?.pv2 ?? 0;
        organizationPerformance = response.data?.firstData?.pv ?? 0;
      }
      notifyListeners();
      return;
    } else {
      var response = await _services.api.getMonthPerformance(
        userName: user.username ?? '',
        time: chooseDate,
      );
      if (response!.error ?? true) {
        onError.call(response.msg ?? '');
        return;
      }
      if (response.data?.tree != null) {
        if (response.data?.firstData?.dataCount != null &&
            response.data?.firstData?.dataCount != 0) {
          hasData = true;
        } else {
          hasData = false;
        }
        dataList = response.data!.tree!.map((e) {
          final model = PerformanceModel.fromTree(e, minus: true);
          model.gen--;
          return model;
        }).toList();
        tableList = [];
        for (var element in response.data?.datatable ?? []) {
          tableList.add(
            PerformanceModel(
              serno: element.serno ?? 0,
              gen: (element.gen ?? 0) - 1,
              id: element.id ?? '',
              name: element.name ?? '',
              grade: element.grade ?? '',
              pv2: element.pv2 ?? 0,
              pv: element.pv ?? 0,
              rPV: element.rPv ?? 0,
              tPV: element.tPv ?? 0,
              child: [],
            ),
          );
        }
        tableListFiltered = tableList;
        personalPerformance = response.data?.firstData?.pv2 ?? 0;
        organizationPerformance = response.data?.firstData?.pv ?? 0;
      }
      notifyListeners();
      return;
    }
  }

  Future<void> setDate(String value) async {
    chooseDate = value;
    loading = true;

    notifyListeners();
    await fetchData();
    loading = false;

    notifyListeners();
  }

  void search(String value) {
    tableListFiltered = tableList
        .where((element) =>
            element.serno.toString().contains(value) ||
            element.gen.toString().contains(value) ||
            element.id.toString().contains(value) ||
            element.name.toString().contains(value) ||
            element.grade.toString().contains(value) ||
            element.pv2.toString().contains(value) ||
            element.pv.toString().contains(value) ||
            element.rPV.toString().contains(value) ||
            element.tPV.toString().contains(value))
        .toList();
    notifyListeners();
  }
}

class PerformanceModel {
  int serno = 0;
  int gen = 0;
  String id = '';
  String name = '';
  String grade = '';

  ///個人業績
  int pv2 = 0;

  ///整組業績
  int pv = 0;

  ///個人組織
  int rPV = 0;

  ///累積業績
  int tPV = 0;

  List<PerformanceModel> child = [];

  PerformanceModel({
    required this.serno,
    required this.gen,
    required this.id,
    required this.name,
    required this.grade,
    required this.pv2,
    required this.pv,
    required this.rPV,
    required this.tPV,
    required this.child,
  });

  PerformanceModel.fromTree(Tree tree, {bool minus = false}) {
    serno = tree.serno ?? 0;
    gen = tree.gen ?? 0;
    id = tree.id ?? '';
    name = tree.name ?? '';
    grade = tree.grade ?? '';
    pv2 = tree.pv2 ?? 0;
    pv = tree.pv ?? 0;
    rPV = tree.rPv ?? 0;
    tPV = tree.tPv ?? 0;
    child = tree.child == null
        ? []
        : tree.child!.map((e) {
            final model = PerformanceModel.fromTree(e);
            if (minus) {
              model.gen--;
            }
            return model;
          }).toList();
  }
}
