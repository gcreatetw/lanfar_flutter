import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';

import '../../services/services.dart';
import '../model/get_change_order_response.dart';

enum UnReportPerformanceOrder {
  none,
  pv,
  pvR,
  name,
  nameR,
  count,
  countR,
}

extension UnReportPerformanceOrderEx on UnReportPerformanceOrder {
  IconData get pvIcon {
    switch (this) {
      case UnReportPerformanceOrder.pv:
        return Icons.keyboard_arrow_up;
      case UnReportPerformanceOrder.pvR:
        return Icons.keyboard_arrow_down;
      case UnReportPerformanceOrder.name:
      case UnReportPerformanceOrder.nameR:
      case UnReportPerformanceOrder.count:
      case UnReportPerformanceOrder.countR:
      case UnReportPerformanceOrder.none:
        return Icons.unfold_more;
    }
  }

  IconData get nameIcon {
    switch (this) {
      case UnReportPerformanceOrder.name:
        return Icons.keyboard_arrow_up;
      case UnReportPerformanceOrder.nameR:
        return Icons.keyboard_arrow_down;
      case UnReportPerformanceOrder.pv:
      case UnReportPerformanceOrder.pvR:
      case UnReportPerformanceOrder.count:
      case UnReportPerformanceOrder.countR:
      case UnReportPerformanceOrder.none:
        return Icons.unfold_more;
    }
  }

  IconData get qtyIcon {
    switch (this) {
      case UnReportPerformanceOrder.count:
        return Icons.keyboard_arrow_up;
      case UnReportPerformanceOrder.countR:
        return Icons.keyboard_arrow_down;
      case UnReportPerformanceOrder.name:
      case UnReportPerformanceOrder.nameR:
      case UnReportPerformanceOrder.pv:
      case UnReportPerformanceOrder.pvR:
      case UnReportPerformanceOrder.none:
        return Icons.unfold_more;
    }
  }
}

class UnReportPerformanceProvider with ChangeNotifier {
  bool loading = false;
  GetChangeOrderResponse? getChangeOrderResponse;
  UnReportPerformanceOrder order = UnReportPerformanceOrder.none;
  List<ExpandableController> expandableControllers = [];

  Future<void> fetch(
    String userId, {
    required Function(String) onError,
  }) async {
    loading = true;
    notifyListeners();

    getChangeOrderResponse =
        await Services().api.getChangeOrder(userId: userId);

    if (getChangeOrderResponse?.error == true) {
      onError.call(getChangeOrderResponse!.msg ?? '');
      return;
    }
    if(getChangeOrderResponse?.data?.datatable != null){
      expandableControllers = [];
      for(var element in getChangeOrderResponse!.data!.datatable!){
        expandableControllers.add(ExpandableController());
      }
    }


    loading = false;
    notifyListeners();
  }

  void reOrder(UnReportPerformanceOrder orderBy) {
    order = orderBy;
    if (getChangeOrderResponse!.data!.datatable != null) {
      switch (orderBy) {
        case UnReportPerformanceOrder.none:
          break;
        case UnReportPerformanceOrder.pv:
          getChangeOrderResponse!.data!.datatable!
              .sort((a, b) => a.ordPv!.compareTo(b.ordPv!));
          break;
        case UnReportPerformanceOrder.name:
          getChangeOrderResponse!.data!.datatable!
              .sort((a, b) => a.prodName!.compareTo(b.prodName!));
          break;
        case UnReportPerformanceOrder.count:
          getChangeOrderResponse!.data!.datatable!
              .sort((a, b) => a.sumQty!.compareTo(b.sumQty!));
          break;
        case UnReportPerformanceOrder.pvR:
          getChangeOrderResponse!.data!.datatable!
              .sort((a, b) => b.ordPv!.compareTo(a.ordPv!));
          break;
        case UnReportPerformanceOrder.nameR:
          getChangeOrderResponse!.data!.datatable!
              .sort((a, b) => b.prodName!.compareTo(a.prodName!));
          break;
        case UnReportPerformanceOrder.countR:
          getChangeOrderResponse!.data!.datatable!
              .sort((a, b) => b.sumQty!.compareTo(a.sumQty!));
          break;
      }
    }
    notifyListeners();
  }
}
