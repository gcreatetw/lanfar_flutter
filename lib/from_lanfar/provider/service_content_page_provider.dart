import 'package:flutter/material.dart';
import '../model/get_service_content_response.dart';
import 'package:lanfar/from_lanfar/model/get_service_record_response.dart';
import 'package:lanfar/models/entities/index.dart';
import 'package:lanfar/services/services.dart';

class ServiceContentPageProvider with ChangeNotifier {
  User user;
  ServiceRecordBean model;
  Function(String) onError;

  ServiceContentPageProvider({
    required this.user,
    required this.model,
    required this.onError,
  });

  TextEditingController messageController = TextEditingController();
  ScrollController scrollController = ScrollController();

  bool loading = false;
  bool sending = false;
  String date = '';
  String type = '';
  String content = '';
  String status = '';

  List<ServiceContentBean> list = [];

  Future<void> fetch(bool first) async {
    if (first) {
      loading = true;
      notifyListeners();
    }
    var response = await Services().api.getServiceContent(
          userId: user.username ?? '',
          entryId: model.entryId ?? 0,
          formId: model.formId ?? 0,
        );

    if (response!.error ?? true) {
      onError.call(response.msg ?? '發生錯誤');
      loading = false;
      notifyListeners();
      return;
    }
    date = response.data?.dateCreated ?? '';
    type = response.data?.consultType ?? '';
    content = response.data?.consultText ?? '';
    status = response.data?.consultStatus ?? '';
    var oldLength = list.length;
    list = response.data?.interactiveList ?? [];
    list = list.reversed.toList();
    loading = false;
    notifyListeners();
    if (!first && list.length > oldLength) {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        scrollController.animateTo(
          scrollController.position.maxScrollExtent,
          duration: const Duration(seconds: 1),
          curve: Curves.fastOutSlowIn,
        );
      });
    }
  }

  bool get canSend => messageController.text.isNotEmpty && !sending;

  Future<void> send() async {
    sending = true;
    notifyListeners();
    var response = await Services().api.sendServiceMessage(
          formId: model.formId ?? 0,
          userName: user.username ?? '',
          entryId: model.entryId ?? 0,
          content: messageController.text,
        );

    if (response != null) {
      onError.call(response['msg']);
      sending = false;
      notifyListeners();
      return;
    }

    messageController.text = '';
    await fetch(false);
    sending = false;
    notifyListeners();
  }
}
