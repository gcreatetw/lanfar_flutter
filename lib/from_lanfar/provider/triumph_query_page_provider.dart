import 'package:flutter/material.dart';

import '../../services/services.dart';
import '../model/get_triumph_query_response.dart';

class TriumphQueryPageProvider with ChangeNotifier {
  bool loading = false;
  GetTriumphQueryResponse? getTriumphQueryResponse;

  Future<void> fetch({
    required String userId,
    required Function(String) onError,
  }) async {
    loading = true;

    notifyListeners();
    getTriumphQueryResponse = await Services().api.getTriumphQuery(
          userId: userId,
        );

    if (getTriumphQueryResponse?.error == true) {
      onError.call(getTriumphQueryResponse?.msg ?? '');
      return;
    }

    loading = false;

    notifyListeners();
  }
}
