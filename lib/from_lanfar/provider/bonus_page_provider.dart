import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lanfar/from_lanfar/model/get_bonus_date_response.dart';
import 'package:lanfar/from_lanfar/model/get_bonus_detail_response.dart';
import 'package:lanfar/services/index.dart';

class BonusPageProvider with ChangeNotifier {
  bool loading = false;
  bool bodyLoading = false;
  GetBonusDateResponse? getBonusDateResponse;
  GetBonusDetailResponse? getBonusDetailResponse;
  String chooseDate = '';

  Future<void> fetch(
    String userId, {
    required Function(String) onError,
  }) async {
    loading = true;
    bodyLoading = true;
    notifyListeners();
    await fetchDate(userId, onError: onError);
    await fetchDetail(userId, onError: onError);
  }

  Future<void> fetchDate(
    String userId, {
    required Function(String) onError,
  }) async {
    getBonusDateResponse = await Services().api.getBonusDate(userId: userId);
    if (getBonusDateResponse!.data!.isNotEmpty) {
      chooseDate = getBonusDateResponse!.data![0];
    }
    if (chooseDate.isEmpty) {
      chooseDate = DateFormat('yyyyMM')
          .format(DateTime(DateTime.now().year, DateTime.now().month - 1));
    }

    if (getBonusDateResponse!.error == true) {
      onError.call(getBonusDateResponse!.msg ?? '');
      return;
    }
  }

  Future<void> fetchDetail(
    String userId, {
    required Function(String) onError,
  }) async {
    bodyLoading = true;
    notifyListeners();

    getBonusDetailResponse = await Services().api.getBonusDetail(
          userId: userId,
          date: chooseDate,
        );

    if (getBonusDetailResponse!.error == true) {
      onError.call(getBonusDetailResponse!.msg ?? '');
      return;
    }

    loading = false;
    bodyLoading = false;
    notifyListeners();
  }

  void setDate(
    String date, {
    required String userId,
    required Function(String) onError,
  }) {
    if (bodyLoading) return;
    chooseDate = date;
    notifyListeners();

    fetchDetail(userId, onError: onError);
  }
}
