import 'package:local_auth/local_auth.dart';
import 'package:local_auth_android/local_auth_android.dart';
import 'package:local_auth_ios/local_auth_ios.dart';

class LocalAuthUtils {
  final LocalAuthentication auth = LocalAuthentication();

  Future<bool> checkCanAuth() async {
    final canAuthenticateWithBiometrics = await auth.canCheckBiometrics;
    final canAuthenticate =
        canAuthenticateWithBiometrics || await auth.isDeviceSupported();

    return canAuthenticate;
  }

  Future<bool?> startAuth() async {
    final availableBiometrics = await auth.getAvailableBiometrics();

    if (availableBiometrics.isEmpty) {
      return null;
    }

    final didAuthenticate = await auth.authenticate(
      localizedReason: '生物辨識',
      authMessages: [
        const AndroidAuthMessages(
          signInTitle: '連法行動GO',
          cancelButton: '取消',
          biometricHint: '',
          biometricRequiredTitle: '',
          deviceCredentialsRequiredTitle: '',
          deviceCredentialsSetupDescription: '',
        ),
        const IOSAuthMessages(
          cancelButton: '取消',
          goToSettingsButton: '前往設定',
        ),
      ],
      options: const AuthenticationOptions(
        biometricOnly: true,
        sensitiveTransaction: false,
      ),

    );
    return didAuthenticate;
  }
}
