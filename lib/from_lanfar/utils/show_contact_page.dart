import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lanfar/from_lanfar/screens/contact_page.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../app.dart';
import '../../common/theme/colors.dart';
import '../../env.dart';
import '../../generated/l10n.dart';
import '../../screens/chat/chat_mixin.dart';
import '../screens/webview_with_cookie.dart';

class ShowContactPage with ChatMixin {
  ///contact event
  void showContact(BuildContext context) {
    showCupertinoModalPopup(
      context: context,
      barrierDismissible: true,
      useRootNavigator: true,
      builder: (popupContext) => CupertinoActionSheet(
        actions: [
          CupertinoActionSheetAction(
            onPressed: () {
              // Navigator.of(App.fluxStoreNavigatorKey.currentContext!).push(
              //   MaterialPageRoute(
              //     builder: (context) => WebViewWithCookie(
              //         url:
              //             '${environment['serverConfig']['url']}/my-accounts/online-customer-service/'),
              //   ),
              // );
              Navigator.of(context).pop();
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const ContactPage(
                    type: ContactType.normal,
                  ),
                ),
              );
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Icon(
                  Icons.sms_outlined,
                  color: primaryColor,
                ),
                const SizedBox(width: 8),
                Text(
                  '線上客服',
                  style: Theme.of(context).textTheme.caption!.copyWith(
                        color: Theme.of(context).colorScheme.secondary,
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                      ),
                ),
              ],
            ),
          ),
          // CupertinoActionSheetAction(
          //   onPressed: () {
          //     Navigator.of(App.fluxStoreNavigatorKey.currentContext!).push(
          //       MaterialPageRoute(
          //         builder: (context) => WebViewWithCookie(
          //           url:
          //               '${environment['serverConfig']['url']}/my-accounts/order-consultation-service/',
          //         ),
          //       ),
          //     );
          //   },
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.center,
          //     crossAxisAlignment: CrossAxisAlignment.center,
          //     children: [
          //       const Icon(
          //         Icons.shopping_cart_outlined,
          //         color: primaryColor,
          //       ),
          //       const SizedBox(width: 8),
          //       Text(
          //         '訂單諮詢',
          //         style: Theme.of(context).textTheme.caption!.copyWith(
          //               color: Theme.of(context).colorScheme.secondary,
          //               fontWeight: FontWeight.w600,
          //               fontSize: 16,
          //             ),
          //       ),
          //     ],
          //   ),
          // ),
          CupertinoActionSheetAction(
            onPressed: () {
              launch('tel://0800068003');
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Icon(
                  Icons.perm_phone_msg_outlined,
                  color: primaryColor,
                ),
                const SizedBox(width: 8),
                Text(
                  '客服專線',
                  style: Theme.of(context).textTheme.caption!.copyWith(
                        color: Theme.of(context).colorScheme.secondary,
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                      ),
                ),
              ],
            ),
          ),
        ],
        cancelButton: CupertinoActionSheetAction(
          onPressed: Navigator.of(popupContext).pop,
          isDestructiveAction: true,
          child: Text(S.of(context).cancel),
        ),
      ),
    );
    //   showModalBottomSheet(
    //     backgroundColor: Colors.transparent,
    //     barrierColor: Colors.transparent,
    //     context: context,
    //     builder: (context) {
    //       return Container(
    //         decoration: const BoxDecoration(
    //           color: Colors.white,
    //           borderRadius: BorderRadius.only(
    //             topLeft: Radius.circular(25.0),
    //             topRight: Radius.circular(25.0),
    //           ),
    //           boxShadow: [
    //             BoxShadow(
    //               color: Colors.grey,
    //               offset: Offset(0.0, 1.0),
    //               blurRadius: 6.0,
    //             ),
    //           ],
    //         ),
    //         child: Padding(
    //           padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 16),
    //           child: Wrap(
    //             children: [
    //               Row(
    //                 children: [
    //                   Expanded(
    //                     child: TextButton(
    //                       onPressed: () {
    //                         Navigator.of(context).pop();
    //                         Navigator.of(context).push(MaterialPageRoute(
    //                             builder: (context) => const ContactPage()));
    //                       },
    //                       style: ButtonStyle(
    //                         side: MaterialStateProperty.all(const BorderSide()),
    //                         overlayColor: MaterialStateColor.resolveWith(
    //                                 (states) => Colors.black38),
    //                       ),
    //                       child: const Text(
    //                         '文字客服',
    //                         style: TextStyle(fontSize: 20, color: Colors.black),
    //                       ),
    //                     ),
    //                   ),
    //                   const SizedBox(width: 13),
    //                   Expanded(
    //                     child: TextButton(
    //                       onPressed: () {},
    //                       style: ButtonStyle(
    //                         side: MaterialStateProperty.all(const BorderSide()),
    //                         overlayColor: MaterialStateColor.resolveWith(
    //                                 (states) => Colors.black38),
    //                       ),
    //                       child: const Text(
    //                         '客服專線',
    //                         style: TextStyle(fontSize: 20, color: Colors.black),
    //                       ),
    //                     ),
    //                   ),
    //                 ],
    //               )
    //             ],
    //           ),
    //         ),
    //       );
    //     },
    //   );
  }
}
