import 'package:flutter/material.dart';

class Utils {
  static void showSnackBar(
    BuildContext context, {
    required String text,
    bool close = false,
  }) {
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(text),
        action: close
            ? SnackBarAction(
                label: '關閉',
                onPressed: () {},
                textColor: Colors.white,
              )
            : null,
      ),
    );
  }
}
