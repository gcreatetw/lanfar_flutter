import 'dart:async';
import 'dart:convert';

import 'package:http_auth/http_auth.dart';
import 'package:localstorage/localstorage.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../common/config.dart';
import '../common/constants.dart';
import '../from_lanfar/model/calendar_event.dart';
import '../from_lanfar/model/default_checkout_response.dart';
import '../from_lanfar/model/get_address_response.dart';
import '../from_lanfar/model/get_article_content_response.dart';
import '../from_lanfar/model/get_articles_response.dart';
import '../from_lanfar/model/get_bonus_date_response.dart';
import '../from_lanfar/model/get_bonus_detail_response.dart';
import '../from_lanfar/model/get_change_order_response.dart';
import '../from_lanfar/model/get_monarch_point_response.dart';
import '../from_lanfar/model/get_performance_overview_content_response.dart';
import '../from_lanfar/model/get_performance_overview_date_response.dart';
import '../from_lanfar/model/get_service_content_response.dart';
import '../from_lanfar/model/get_service_record_response.dart';
import '../from_lanfar/model/get_triumph_query_response.dart';
import '../from_lanfar/model/market_info_response.dart';
import '../from_lanfar/model/new_order_history_detail_model.dart';
import '../from_lanfar/model/new_order_history_model.dart';
import '../from_lanfar/model/notification_response.dart';
import '../from_lanfar/model/point_history_response.dart';
import '../from_lanfar/model/remove_coupon_response_model.dart';
import '../from_lanfar/model/sell_product_msg_response.dart';
import '../from_lanfar/provider/article_list_page_provider.dart';
import '../from_lanfar/provider/marketing_page_provider.dart';
import '../models/comment.dart';
import '../models/entities/brand.dart';
import '../models/entities/listing_booking.dart';
import '../models/entities/order_delivery_date.dart';
import '../models/entities/paging_response.dart';
import '../models/entities/prediction.dart';
import '../models/entities/vacation_settings.dart';
import '../models/index.dart';
import '../models/vendor/store_model.dart';
import '../modules/dynamic_layout/config/banner_config.dart';
import 'dependency_injection.dart';
import 'wordpress/blognews_api.dart';

export '../models/entities/paging_response.dart';

abstract class BaseServices {
  final BlogNewsApi blogApi;

  final String domain;

  Map<String,String> header = {};

  BaseServices({
    required this.domain,
    String? blogDomain,
  }) : blogApi = BlogNewsApi(blogDomain ?? domain){
    getCookie();
  }

  Future<List<Category>?>? getCategories({lang}) async => const <Category>[];

  Future<List<Product>>? getProducts({userId}) => null;

  Future<List<Product>?> fetchProductsLayout(
          {required config, lang, userId, bool refreshCache = false}) async =>
      const <Product>[];

  Future<List<Product>?> fetchProductsByCategory({
    categoryId,
    tagId,
    required page,
    minPrice,
    maxPrice,
    orderBy,
    lang,
    order,
    featured,
    onSale,
    attribute,
    attributeTerm,
    listingLocation,
    userId,
    bool refreshCache = false,
  }) async =>
      const <Product>[];

  Future<void> getCookie() async {
    ///取cookie
    try {
      final _storage = injector<LocalStorage>();
      await _storage.ready;
      final json = _storage.getItem(kLocalKey['userInfo']!);
      final user = User.fromLocalJson(json);
      header ={
        'token':EncodeUtils.encodeCookie(user.cookie!)
      };
    } catch (e) {
      print(e);
    }
  }

  Future<User?>? loginFacebook({String? token}) => null;

  Future<User>? loginSMS({String? token}) => null;

  Future<bool> isUserExisted({String? phone, String? username}) async => true;

  Future<User?>? loginApple({String? token}) => null;

  Future<User?>? loginGoogle({String? token}) => null;

  Future<List<Review>>? getReviews(productId) => null;

  Future<List<ProductVariation>?>? getProductVariations(Product product,
          {String? lang}) =>
      null;

  Future<List<ShippingMethod>>? getShippingMethods(
          {CartModel? cartModel,
          String? token,
          String? checkoutId,
          Store? store}) =>
      null;

  Future<List<PaymentMethod>>? getPaymentMethods({
    CartModel? cartModel,
    ShippingMethod? shippingMethod,
    String? token,
  }) =>
      null;

  Future<Order>? createOrder({
    CartModel? cartModel,
    UserModel? user,
    bool? paid,
    String? transactionId,
  }) =>
      null;

  Future<PagingResponse<Order>>? getMyOrders({
    User? user,
    dynamic cursor,
  }) =>
      null;

  Future? updateOrder(orderId, {status, required token}) => null;

  Future<Order?>? cancelOrder({
    required Order? order,
    required String? userCookie,
  }) =>
      null;

  Future<PagingResponse<Product>>? searchProducts({
    name,
    categoryId,
    categoryName,
    tag,
    attribute,
    attributeId,
    required page,
    lang,
    listingLocation,
    userId,
  }) =>
      null;

  Future<User?>? getUserInfo(cookie) => null;

  Future<User?>? createUser({
    String? firstName,
    String? lastName,
    String? username,
    String? password,
    String? phoneNumber,
    String? identity,
    bool isVendor = false,
  }) =>
      null;

  Future<String?>? signDown({
    String? firstName,
    String? lastName,
    String? username,
    String? password,
    String? phoneNumber,
    String? identity,
    bool isVendor = false,
  }) =>
      null;

  Future<Map<String, dynamic>?>? updateUserInfo(
          Map<String, dynamic> json, String? token) =>
      null;

  Future<User?>? login({
    username,
    password,
  }) =>
      null;

  Future<Product?>? getProduct(id, {lang, int index = 0}) => null;

  Future<Coupons>? getCoupons(
          {int page = 1, String search = '', required String userId}) =>
      null;

  Future<List<OrderNote>>? getOrderNote({
    String? userId,
    String? orderId,
  }) =>
      null;

  Future? createReview(
          {String? productId, Map<String, dynamic>? data, String? token}) =>
      null;

  Future<Map<String, dynamic>?>? getHomeCache(String? lang) => null;

  Future<List<Blog>?> fetchBlogLayout({required config, lang}) async {
    try {
      var list = <Blog>[];
      var endPoint = 'posts?_embed';
      if (kAdvanceConfig['isMultiLanguages'] ?? false) {
        endPoint += '&lang=$lang';
      }
      if (config.containsKey('category')) {
        endPoint += "&categories=${config["category"]}";
      }
      if (config.containsKey('limit')) {
        endPoint += "&per_page=${config["limit"] ?? 20}";
      }
      if (config.containsKey('page')) {
        endPoint += "&page=${config["page"]}";
      }

      var response = await blogApi.getAsync(endPoint);

      if (response != null) {
        for (var item in response) {
          list.add(Blog.fromJson(item));
        }
      }
      return list;
    } catch (e) {
      rethrow;
    }
  }

  Future<Blog> getPageById(int? pageId) async {
    var response = await blogApi.getAsync('pages/$pageId?_embed');
    return Blog.fromJson(response);
  }

  Future? getCategoryWithCache() => null;

  Future<List<FilterAttribute>>? getFilterAttributes() => null;

  Future<List<SubAttribute>>? getSubAttributes({int? id}) => null;

  Future<List<FilterTag>>? getFilterTags() => null;

  Future<String>? getCheckoutUrl(Map<String, dynamic> params, String? lang) =>
      null;

  Future<String?>? submitForgotPassword({
    String? forgotPwLink,
    Map<String, dynamic>? data,
  }) =>
      null;

  Future? logout() => null;

  Future? checkoutWithCreditCard(String? vaultId, CartModel cartModel,
      Address address, PaymentSettingsModel paymentSettingsModel) {
    return null;
  }

  Future<PaymentSettings>? getPaymentSettings() {
    return null;
  }

  Future<PaymentSettings>? addCreditCard(
      PaymentSettingsModel paymentSettingsModel,
      CreditCardModel creditCardModel) {
    return null;
  }

  Future<Map<String, dynamic>?>? getCurrencyRate() => null;

  Future<Map<String, dynamic>?>? getCartInfo(String? token, int? orderType) => null;

  Future? syncCartToWebsite(CartModel cartModel, User? user) => null;

  Future<Map<String, dynamic>>? getCustomerInfo(String? id) => null;

  Future<Map<String, dynamic>?>? getTaxes(CartModel cartModel) => null;

  Future<Map<String, Tag>>? getTags({String? lang}) => null;

  Future? getCountries() => null;

  Future? getStatesByCountryId(countryId) => null;

  Future<Point?>? getMyPoint(String? token) => null;

  Future? updatePoints(String? token, Order? order) => null;

  //For vendor
  Future<Store?>? getStoreInfo(storeId) => null;

  Future<bool>? pushNotification(
    cookie, {
    receiverEmail,
    senderName,
    message,
  }) =>
      null;

  Future<List<Review>>? getReviewsStore({storeId, page, perPage}) => null;

  Future<List<Product>>? getProductsByStore(
          {storeId,
          int? page,
          int? perPage,
          int? catId,
          bool? onSale,
          String? order,
          String? orderBy,
          String? searchTerm,
          String lang = 'en'}) =>
      null;

  Future<List<Store>>? searchStores({
    String? keyword,
    int? page,
  }) =>
      null;

  Future<List<Store>>? getFeaturedStores() => null;

  Future<PagingResponse<Order>>? getVendorOrders({
    required User user,
    dynamic cursor,
  }) =>
      null;

  Future<Product>? createProduct(String? cookie, Map<String, dynamic> data) =>
      null;

  Future<void>? deleteProduct(
          {required String? cookie, required String? productId}) =>
      null;

  Future<List<Product>>? getOwnProducts(
    String? cookie, {
    int? page,
    int? perPage,
  }) =>
      null;

  Future<dynamic>? uploadImage(dynamic data, String? token) => null;

  Future<List<Prediction>>? getAutoCompletePlaces(
          String term, String? sessionToken) =>
      null;

  Future<Prediction>? getPlaceDetail(
          Prediction prediction, String? sessionToken) =>
      null;

  Future<List<Store>>? getNearbyStores(Prediction prediction,
          {int page = 1, int perPage = 10, int radius = 10, String? name}) =>
      null;

  Future<Product?> getProductByPermalink(String productPermalink) async {}

  Future<Category?> getProductCategoryByPermalink(
      String productCategoryPermalink) async {}

  Future<Store?> getStoreByPermalink(String storePermaLink) async {}

  Future<Blog?> getBlogByPermalink(String blogPermaLink) async {}

  Future<List<Brand>?> getBrands() async => null;

  Future<List<Product>?> fetchProductsByBrand(
          {dynamic page, String? lang, String? brandId}) async =>
      null;

  ///----FLUXSTORE LISTING----///
  Future<dynamic>? bookService({userId, value, message}) => null;

  Future<List<Product>>? getProductNearest(location) => null;

  Future<List<ListingBooking>>? getBooking({userId, page, perPage}) => null;

  Future<Map<String, dynamic>?>? checkBookingAvailability({data}) => null;

  Future<List<dynamic>>? getLocations() => null;

  /// BOOKING FEATURE
  Future<bool>? createBooking(dynamic bookingInfo) => null;

  Future<List<dynamic>>? getListStaff(String? idProduct) => null;

  Future<List<String>>? getSlotBooking(
          String? idProduct, String idStaff, String date) =>
      null;

  ///get blog
  Future<PagingResponse<Blog>>? getBlogs(dynamic cursor) async {
    try {
      ///取cookie
      final _storage = injector<LocalStorage>();
      await _storage.ready;
      final json = _storage.getItem(kLocalKey['userInfo']!);
      final user = User.fromLocalJson(json);

      ///取cookieName
      var prefs = injector<SharedPreferences>();
      final cookieName = prefs.getString('cookieName');
      final param = '_embed&page=$cursor';

      final response = await httpGet(
        '${blogApi.url}/wp-json/wp/v2/posts?$param'.toUri()!,
        headers: {'Cookie': '$cookieName=${user.cookie}'},
      );
      if (response.statusCode != 200) {
        return const PagingResponse();
      }
      List data = jsonDecode(response.body);
      return PagingResponse(data: data.map((e) => Blog.fromJson(e)).toList());
    } on Exception catch (_) {
      return const PagingResponse();
    }
  }

  /// RAZORPAY PAYMENT
  Future<String?> createRazorpayOrder(params) async {
    try {
      var client = BasicAuthClient(
          kRazorpayConfig['keyId'], kRazorpayConfig['keySecret']);
      final response = await client
          .post('https://api.razorpay.com/v1/orders'.toUri()!, body: params);
      final responseJson = jsonDecode(response.body);
      if (responseJson != null && responseJson['id'] != null) {
        return responseJson['id'];
      } else if (responseJson['message'] != null) {
        throw responseJson['message'];
      } else {
        throw "Can't create order for Razorpay";
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<void> updateOrderIdForRazorpay(paymentId, orderId) async {
    try {
      final token = base64.encode(latin1.encode(
          '${kRazorpayConfig['keyId']}:${kRazorpayConfig['keySecret']}'));

      final body = {
        'notes': {'woocommerce_order_id': orderId}
      };
      await httpPatch(
          'https://api.razorpay.com/v1/payments/$paymentId'.toUri()!,
          headers: {
            'Authorization': 'Basic ' + token.trim(),
            'Content-Type': 'application/json'
          },
          body: json.encode(body));
    } catch (e) {
      return;
    }
  }

  Future<List<Blog>> searchBlog({required String name}) async => const <Blog>[];

  Future<List<Blog>> fetchBlogsByCategory(
          {categoryId, page, lang, order = 'desc'}) async =>
      <Blog>[];

  Future<Blog> getBlogById(dynamic id) async {
    final response =
        await httpGet('${blogApi.url}/wp-json/wp/v2/posts/$id?_embed'.toUri()!);
    var body = jsonDecode(response.body);

    if (response.statusCode == 200) {
      return Blog.fromJson(body);
    }
    return Blog.empty(id);
  }

  Future<List<Category>> getCategoriesByPage(
          {lang, page, limit, storeId}) async =>
      [];

  Future<List<OrderDeliveryDate>> getListDeliveryDates({storeId}) async =>
      <OrderDeliveryDate>[];

  Future<Category?> getProductCategoryById(
          {required String categoryId}) async =>
      null;

  Future<VacationSettings?> getVacationSettings(String storeId) async => null;

  Future<bool?> setVacationSettings(
          String cookie, VacationSettings vacationSettings) async =>
      null;

  Future<List<Comment>?> getCommentsByPostId({postId}) async {
    try {
      var list = <Comment>[];

      var endPoint = 'comments?';
      if (postId != null) {
        endPoint += '&post=$postId';
      }

      var response = await blogApi.getAsync(endPoint);

      for (var item in response) {
        list.add(Comment.fromJson(item));
      }
      return list;
    } catch (e) {
      rethrow;
    }
  }

  Future<bool> createComment(
      {int? blogId,
      String? content,
      String? authorName,
      String? authorAvatar,
      String? userEmail,
      String? date}) async {
    try {
      //return true if comment created successful, false if otherwise
      var data = {
        'content': content,
        'author_name': authorName,
        'author_avatar_urls': authorAvatar,
        'email': userEmail,
        'date': date
      };
      var dataResponse = await blogApi.postAsync('comments?post=$blogId', data);
      if (dataResponse['id'] != null) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<List<Blog>?> getBlogsByCategory(int? cateId) async {
    try {
      var url = 'posts?_embed';
      if (cateId != null) {
        url = 'posts?_embed&categories=$cateId';
      }
      var response = await blogApi.getAsync(url);
      var list = <Blog>[];
      for (var item in response) {
        list.add(Blog.fromJson(item));
      }
      return list;
    } catch (e) {
      rethrow;
    }
  }

  ///custom
  Future<List<CalendarEvent>?>? getCalendarEvent({
    required int page,
    int perPage = 10,
    required String startDate,
    required String endDate,
    String status = 'publish',
  }) =>
      null;

  Future<Map<String, dynamic>?>? getSubscribe(String? token) => null;

  Future<Map<String, dynamic>?>? subscribe(String? token, String category) =>
      null;

  Future<List<BannerItemConfig>?>? getBanner() => null;

  Future<List<NewOrderHistoryConfig>?>? getOrderHistory({
    required String id,
    required String startDate,
    required String endDate,
    required int type,
  }) =>
      null;

  Future<OrderResponse?>? getOrderHistoryDetail({
    required String id,
    required String username,
  }) =>
      null;

  Future<bool?>? getUserStatus(String userId) => null;

  Future<NotificationResponse?>? getNotificationList({
    required String userId,
  }) =>
      null;

  Future<bool>? setNotificationRead({
    required String userId,
    required String notificationId,
  }) =>
      null;

  Future<String?>? checkUser({
    required String userId,
    required String phone,
  }) =>
      null;

  Future<bool>? resetPassword({
    required String userId,
    required String password,
  }) =>
      null;

  Future<Map<String, dynamic>?>? setConfirmSpeed({
    required String userId,
    required int check,
  }) =>
      null;

  Future<RemoveCouponResponseModel>? removeCoupon({
    required String userId,
    required String coupon,
  }) =>
      null;

  Future<PointHistoryResponse?>? getPointHistory({
    required String cookie,
  }) =>
      null;

  Future<PointHistoryResponse?>? getPointExchanged({
    required String cookie,
  }) =>
      null;

  Future<dynamic>? exchangePoint({
    required String cookie,
    required int point,
  }) =>
      null;

  Future<String?>? reOrder({
    required String userId,
    required String orderId,
  }) =>
      null;

  Future<List<dynamic>?>? getVersions() => null;

  Future<GetChangeOrderResponse?>? getChangeOrder({
    required String userId,
  }) =>
      null;

  Future<GetBonusDateResponse?>? getBonusDate({
    required String userId,
  }) =>
      null;

  Future<GetBonusDetailResponse?>? getBonusDetail({
    required String userId,
    required String date,
  }) =>
      null;

  Future<GetMonarchPointResponse?>? getMonarchPoint({
    required String userId,
    required String date,
  }) =>
      null;

  Future<GetTriumphQueryResponse?>? getTriumphQuery({
    required String userId,
  }) =>
      null;

  Future<GetAddressResponse?>? getAddress({
    required String userId,
  }) =>
      null;

  Future<String?>? updateAddress({
    required String userId,
    required String action,
    int? addressId,
    Map<String, dynamic>? addressData,
  }) =>
      null;

  Future<List<ArticleCatModel>?>? getChargingCat() => null;

  Future<List<ArticleCatModel>?>? getChargingTag() => null;

  Future<GetArticlesResponse?>? getArticles({
    required int id,
    required String taxonomy,
    required int page,
    String? search,
    bool isNews = false,
  }) =>
      null;

  Future<ArticleContentResponse?>? getArticleContent({
    required int id,
  }) =>
      null;

  Future<GetPerformanceOverviewDateResponse>? getPerformanceOverviewDate() =>
      null;

  Future<GetPerformanceOverviewContentResponse>? getCurrentPerformance({
    required String userName,
  }) =>
      null;

  Future<GetPerformanceOverviewContentResponse>? getMonthPerformance({
    required String userName,
    required String time,
  }) =>
      null;

  Future<GetPerformanceOverviewContentResponse>? getOrganizationPerformance({
    required String userName,
    required int? grade,
    required String? id,
    required String? name,
  }) =>
      null;

  Future<GetMarketingInfoResponse?>? getMarketingInfo({
    required String userId,
  }) =>
      null;

  Future<Map<String, dynamic>?>? updateMarketingData({
    required String userName,
    required String type,
    required String? inviteText,
    required List<List<GroupProductModel>>? products,
  }) =>
      null;

  Future<Map<String, dynamic>?>? sendServiceForm({
    required int formId,
    required String userName,
    required String phone,
    required String email,
    required String type,
    required String content,
    required String? order,
  }) =>
      null;

  Future<GetServiceRecordResponse?>? getServiceRecord({
    required String userId,
    required String? search,
  }) =>
      null;

  Future<GetServiceContentResponse?>? getServiceContent({
    required String userId,
    required int entryId,
    required int formId,
  }) =>
      null;

  Future<Map<String, dynamic>?>? sendServiceMessage({
    required int formId,
    required String userName,
    required int entryId,
    required String content,
  }) =>
      null;

  Future<DefaultCheckoutResponse?>? defaultCheckout({
    required String userId,
    required String productId,
  }) =>
      null;

  Future<SellProductMsgResponse?>? sellProductMsg({
    required String userId,
    required String productId,
  }) =>
      null;

  Future<String?>? getOtp({
    required String phone,
  }) =>
      null;

  Future<String?>? verifyOtp({
    required String phone,
    required String code,
  }) =>
      null;
}
