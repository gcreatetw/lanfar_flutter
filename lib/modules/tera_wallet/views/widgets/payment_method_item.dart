import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../common/config.dart';
import '../../../../models/index.dart' show AppModel, PaymentMethod;
import '../../../../screens/checkout/widgets/payment_method_item.dart' as base;
import 'warning_currency.dart';

class PaymentMethodItem extends StatelessWidget {
  const PaymentMethodItem({Key? key, required this.paymentMethod, required this.onSelected, this.selectedId, this.descWidget}) : super(key: key);
  final PaymentMethod paymentMethod;
  final Function(String?) onSelected;
  final String? selectedId;
  final Widget? descWidget;

  @override
  Widget build(BuildContext context) {
    final currency = Provider.of<AppModel>(context, listen: false).currency;
    final defaultCurrency = (kAdvanceConfig['DefaultCurrency'] as Map)['currencyCode'];
    final disablePayment = defaultCurrency.toString().toLowerCase() != currency.toString().toLowerCase();
    return base.PaymentMethodItem(paymentMethod: paymentMethod, onSelected: disablePayment ? null : onSelected, selectedId: selectedId, descWidget: disablePayment ? WarningCurrency(defaultCurrency: defaultCurrency) : null);
  }
}
