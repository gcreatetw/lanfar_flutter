import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import '../../../common/config.dart';

class CurrencyTextFormatter extends TextInputFormatter {
  final f = NumberFormat.currency(
      locale: kAdvanceConfig['DefaultLanguage'],
      name: '',
      symbol: '\$',
      decimalDigits: 0);

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    final symbol = f.currencySymbol;
    if (newValue.text.isEmpty ||
        (newValue.text.length == symbol.length &&
            oldValue.text.length == symbol.length + 1)) {
      return const TextEditingValue(
        text: '',
        selection: TextSelection.collapsed(
            offset: 0),
      );
    } else if (newValue.text.compareTo(oldValue.text) != 0) {
      final selectionIndexFromTheRight =
          newValue.text.length - newValue.selection.end;
      // final f = NumberFormat('#,###');
      final number = int.parse(newValue.text
          .replaceAll(f.symbols.GROUP_SEP, '')
          .replaceAll(symbol, ''));
      final newString = f.format(number);
      return TextEditingValue(
        text: newString,
        selection: TextSelection.collapsed(
            offset: newString.length - selectionIndexFromTheRight),
      );
    } else {
      return newValue;
    }
  }
}
