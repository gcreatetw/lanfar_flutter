import 'package:flutter/material.dart';
import 'package:inspireui/icons/icon_picker.dart' deferred as defer_icon;
import 'package:inspireui/inspireui.dart' show DeferredWidget, eventBus;
import 'package:lanfar/common/constants.dart';
import 'package:lanfar/from_lanfar/screens/qrcode_page.dart';
import 'package:lanfar/menu/maintab_delegate.dart';
import 'package:lanfar/models/user_model.dart';
import 'package:lanfar/routes/flux_navigate.dart';
import 'package:provider/provider.dart';

import '../../../widgets/common/flux_image.dart';
import '../config/logo_config.dart';

class LogoIcon extends StatelessWidget {
  final LogoConfig config;
  final Function onTap;
  final MenuIcon? menuIcon;
  final IconData? icon;

  const LogoIcon({
    Key? key,
    required this.config,
    required this.onTap,
    this.menuIcon,
    this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      key: const Key('drawerMenu'),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(
          config.iconRadius,
        ),
      ),
      elevation: 0,
      focusElevation: 0,
      hoverElevation: 0,
      hoverColor: Colors.transparent,
      highlightColor: Colors.transparent,
      focusColor: Colors.transparent,
      splashColor: Colors.transparent,
      fillColor: config.iconBackground != null
          ? config.iconBackground!.withOpacity(config.iconOpacity)
          : Theme.of(context).backgroundColor.withOpacity(config.iconOpacity),
      padding: const EdgeInsets.symmetric(
        horizontal: 6,
        vertical: 6,
      ),
      onPressed: () => onTap.call(),
      child: menuIcon != null
          ? DeferredWidget(
              defer_icon.loadLibrary,
              () => Icon(
                defer_icon.iconPicker(
                  menuIcon!.name!,
                  menuIcon!.fontFamily ?? 'CupertinoIcons',
                ),
                color: config.iconColor ??
                    Theme.of(context).textTheme.headline6!.color,
                size: config.iconSize,
              ),
            )
          : Icon(
              icon ?? Icons.menu,
              color: config.iconColor ??
                  Theme.of(context).textTheme.headline6!.color,
              size: config.iconSize,
            ),
    );
  }
}

class Logo extends StatelessWidget {
  final onSearch;
  final onCheckout;
  final onTapDrawerMenu;
  final onTapNotifications;
  final String? logo;
  final LogoConfig config;
  final int totalCart;
  final int notificationCount;

  const Logo({
    Key? key,
    required this.config,
    required this.onSearch,
    required this.onCheckout,
    required this.onTapDrawerMenu,
    required this.onTapNotifications,
    this.logo,
    this.totalCart = 0,
    this.notificationCount = 0,
  }) : super(key: key);

  Widget renderLogo(BuildContext context) {
    if (config.image != null) {
      if (config.image!.contains('http')) {
        return FluxImage(imageUrl: config.image!, height: 50);
      }
      return Image.asset(
        config.image!,
        height: 40,
        color: Theme.of(context).primaryColor,
      );
    }

    /// render from config to support dark/light theme
    if (logo != null) {
      return FluxImage(imageUrl: logo!, height: 40);
    }

    return Container();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final isRotate = screenSize.width > screenSize.height;
    final userModel = Provider.of<UserModel>(context);

    return Builder(
      builder: (context) {
        return SizedBox(
          width: double.maxFinite,
          child:  Container(
              width: double.maxFinite,
              constraints: const BoxConstraints(minHeight: 40.0),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: Row(
                  children: [
                    Expanded(
                      flex: 8,
                      child: Container(
                        constraints: const BoxConstraints(minHeight: 50),

                        ///change homepage app bar layout
                        ///add home page appbar event
                        alignment: Alignment.centerLeft,
                        child: Row(
                          children: [
                            InkWell(
                              onTap: () => pushNavigation(RouteList.profile),
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Row(
                                children: [
                                  Container(
                                    decoration: const BoxDecoration(
                                        // border:
                                        //     Border.all(color: Colors.black54),
                                        shape: BoxShape.circle),
                                    child: CircleAvatar(
                                      backgroundImage:
                                          AssetImage(config.image!),
                                    ),
                                  ),
                                  const SizedBox(width: 12),
                                  Text(
                                    userModel.user!.username!,
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Theme.of(context)
                                          .textTheme
                                          .bodyText2!
                                          .color,
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        // Center(
                        //   child: Row(
                        //     mainAxisAlignment: MainAxisAlignment.center,
                        //     crossAxisAlignment: CrossAxisAlignment.center,
                        //     children: <Widget>[
                        //       if (config.showLogo)
                        //         Center(child: renderLogo(context)),
                        //       if (config.name?.isNotEmpty ?? false) ...[
                        //         const SizedBox(width: 5),
                        //         Text(
                        //           config.name!,
                        //           style: TextStyle(
                        //             fontSize: 20,
                        //             fontWeight: FontWeight.bold,
                        //             color: Theme.of(context).primaryColor,
                        //           ),
                        //         )
                        //       ],
                        //     ],
                        //   ),
                        // ),
                      ),
                    ),
                    if (config.showCart)
                      Expanded(
                        child: Stack(
                          children: <Widget>[
                            LogoIcon(
                              menuIcon:
                                  config.cartIcon ?? MenuIcon(name: 'bag'),
                              onTap: onCheckout,
                              config: config,
                            ),
                            if (totalCart > 0)
                              Positioned(
                                right: 0,
                                top: 0,
                                child: Container(
                                  padding: const EdgeInsets.all(1),
                                  decoration: BoxDecoration(
                                    color: Colors.red,
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  constraints: const BoxConstraints(
                                    minWidth: 18,
                                    minHeight: 18,
                                  ),
                                  child: Text(
                                    totalCart.toString(),
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 12,
                                      height: 1.3,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              )
                          ],
                        ),
                      ),
                    if (config.showNotification)
                      Expanded(
                        child: Stack(
                          children: <Widget>[
                            LogoIcon(
                              menuIcon: config.notificationIcon ??
                                  MenuIcon(name: 'bell'),
                              onTap: onTapNotifications,
                              config: config,
                            ),
                            if (notificationCount > 0)
                              Positioned(
                                right: 0,
                                top: 0,
                                child: Container(
                                  padding: const EdgeInsets.all(1),
                                  decoration: BoxDecoration(
                                    color: Colors.red,
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  constraints: const BoxConstraints(
                                    minWidth: 18,
                                    minHeight: 18,
                                  ),
                                  child: Text(
                                    notificationCount.toString(),
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 12,
                                      height: 1.3,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              )
                          ],
                        ),
                      ),
                    // if (!config.showSearch &&
                    //     !config.showCart &&
                    //     !config.showNotification)
                    //   const Spacer(),
                    // Expanded(
                    //   child: (config.showMenu ?? false)
                    //       ? LogoIcon(
                    //           menuIcon: config.menuIcon,
                    //           onTap: _openQRCodePage,
                    //           config: config,
                    //           icon: Icons.qr_code_2,
                    //         )
                    //       : const SizedBox(),
                    // ),
                    //
                    // ///change home screen drawer button position
                    // Expanded(
                    //   child: (config.showMenu ?? false)
                    //       ? LogoIcon(
                    //           menuIcon: config.menuIcon,
                    //           onTap: onTapDrawerMenu,
                    //           config: config,
                    //         )
                    //       : const SizedBox(),
                    // ),
                  ],
                ),
              ),
            ),

        );
      },
    );
  }

  ///add home page appbar event
  void pushNavigation(String name) {
    MainTabControlDelegate.getInstance().changeTab(name.replaceFirst('/', ''));
  }

  void _openQRCodePage() {
    // showDialog(
    //     context: App.fluxStoreNavigatorKey.currentContext!,
    //     builder: (context) => const QRCodeDialog());

    FluxNavigate.push(
      MaterialPageRoute(
        builder: (context) => const QRCodePage(),
      ),
    );
  }
}
