import 'package:flutter/material.dart';
import 'package:inspireui/inspireui.dart' show StoryWidget;
import 'package:lanfar/from_lanfar/screens/article_list_page.dart';
import 'package:lanfar/from_lanfar/screens/points_page.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../app.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../env.dart';
import '../../from_lanfar/screens/calender_page.dart';
import '../../from_lanfar/screens/marketing_page.dart';
import '../../from_lanfar/screens/webview_with_cookie.dart';
import '../../from_lanfar/utils/show_contact_page.dart';
import '../../menu/maintab_delegate.dart';
import '../../models/index.dart';
import '../../routes/flux_navigate.dart';
import '../../screens/cart/cart_screen.dart';
import '../../services/index.dart';
import 'banner/banner_animate_items.dart';
import 'banner/banner_group_items.dart';
import 'banner/banner_slider.dart';
import 'blog/blog_grid.dart';
import 'brand/brand_layout.dart';
import 'category/category_icon.dart';
import 'category/category_image.dart';
import 'category/category_menu_with_products.dart';
import 'category/category_text.dart';
import 'config/brand_config.dart';
import 'config/index.dart';
import 'header/header_search.dart';
import 'header/header_text.dart';
import 'logo/logo.dart';
import 'product/product_list_simple.dart';
import 'product/product_recent_placeholder.dart';
import 'video/index.dart';

class DynamicLayout extends StatelessWidget {
  final config;
  final bool cleanCache;

  const DynamicLayout({this.config, this.cleanCache = false});

  @override
  Widget build(BuildContext context) {
    final appModel = Provider.of<AppModel>(context, listen: true);

    switch (config['layout']) {
      case 'logo':
        final themeConfig = appModel.themeConfig;
        return Logo(
          config: LogoConfig.fromJson(config),
          logo: themeConfig.logo,
          totalCart:
              Provider.of<CartModel>(context, listen: true).totalCartQuantity,
          notificationCount:
              Provider.of<NotificationModel>(context).unreadCount,
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
          onSearch: () => Navigator.of(context).pushNamed(RouteList.homeSearch),
          onCheckout: () {
            Navigator.push(
              context,
              MaterialPageRoute<void>(
                builder: (BuildContext context) => Scaffold(
                  backgroundColor: Theme.of(context).backgroundColor,
                  body: const CartScreen(isModal: true),
                ),
                fullscreenDialog: true,
              ),
            );
          },
          onTapNotifications: () {
            Navigator.of(context).pushNamed(RouteList.notify);
          },
          onTapDrawerMenu: () => NavigateTools.onTapOpenDrawerMenu(context),
        );

      case 'header_text':
        return HeaderText(
          config: HeaderConfig.fromJson(config),
          onSearch: () => Navigator.of(context).pushNamed(RouteList.homeSearch),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );

      case 'header_search':
        return HeaderSearch(
          config: HeaderConfig.fromJson(config),
          onSearch: () {
            Navigator.of(App.fluxStoreNavigatorKey.currentContext!)
                .pushNamed(RouteList.homeSearch);
          },
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );
      case 'featuredVendors':
        return Services().widget.renderFeatureVendor(config);
      case 'category':
        if (config['type'] == 'image') {
          return CategoryImages(
            config: CategoryConfig.fromJson(config),
            key: config['key'] != null ? Key(config['key']) : UniqueKey(),
          );
        }
        return Consumer<CategoryModel>(builder: (context, model, child) {
          var _config = CategoryConfig.fromJson(config);
          var _listCategoryName =
              model.categoryList.map((key, value) => MapEntry(key, value.name));
          void _onShowProductList(CategoryItemConfig item) {
            switch (item.name) {
              case '線上購物':
                pushNavigation(RouteList.products);
                // ProductModel.showList(
                //   config: item.jsonData,
                //   context: context,
                //   products: item.data != null ? item.data as List<Product> : [],
                // );
                break;
              case '業績查詢':
                pushNavigation(RouteList.performance);
                // FluxNavigate.push(
                //   MaterialPageRoute(
                //     builder: (context) => const PerformancePage(),
                //   ),
                // );
                break;
              case '聯絡客服':
                ShowContactPage()
                    .showContact(App.fluxStoreNavigatorKey.currentContext!);
                break;
              case '我的賣場':

                ///聯盟行銷
                Navigator.of(App.fluxStoreNavigatorKey.currentContext!).push(
                  MaterialPageRoute(
                    builder: (context) => const MarketingPage(),
                  ),
                );
                break;
              case '行事月曆':
                FluxNavigate.push(
                  MaterialPageRoute(
                    builder: (context) => const CalendarPage(),
                  ),
                );
                break;
              case '文宣影片':
                FluxNavigate.push(
                  MaterialPageRoute(
                    builder: (context) => const ArticleListPage(),
                  ),
                );
                // Navigator.of(App.fluxStoreNavigatorKey.currentContext!).push(
                //   MaterialPageRoute(
                //     builder: (context) => WebViewWithCookie(
                //         notDirectPop: true,
                //         alwaysAddAppStyle: true,
                //         url:
                //             '${environment['serverConfig']['url']}/%e6%95%b8%e4%bd%8d%e5%ad%b8%e7%bf%92/'),
                //   ),
                // );
                break;
              case '連法官網':
                launch(
                  environment['serverConfig']['url'],
                  forceSafariVC: false,
                );
                break;
              case '點數查詢':
                Navigator.of(App.fluxStoreNavigatorKey.currentContext!).push(
                  MaterialPageRoute(
                    builder: (context) => const PointsPage(),
                  ),
                );
                // Navigator.of(App.fluxStoreNavigatorKey.currentContext!).push(
                //   MaterialPageRoute(
                //     builder: (context) => WebViewWithCookie(
                //         url:
                //             '${environment['serverConfig']['url']}/my-accounts/my-points/'),
                //   ),
                // );
                break;
            }
          }

          if (config['type'] == 'menuWithProducts') {
            return CategoryMenuWithProducts(
              config: _config,
              listCategoryName: _listCategoryName,
              onShowProductList: _onShowProductList,
              key: config['key'] != null ? Key(config['key']) : UniqueKey(),
            );
          }

          if (config['type'] == 'text') {
            return CategoryTexts(
              config: _config,
              listCategoryName: _listCategoryName,
              onShowProductList: _onShowProductList,
              key: config['key'] != null ? Key(config['key']) : UniqueKey(),
            );
          }

          ///custom homepage icon list
          return Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.only(bottom: 32),
            child: FittedBox(
              child: CategoryIcons(
                config: _config,
                listCategoryName: _listCategoryName,
                onShowProductList: _onShowProductList,
                key: config['key'] != null ? Key(config['key']) : UniqueKey(),
              ),
            ),
          );
        });
      case 'bannerAnimated':
        if (kIsWeb) return const SizedBox();
        return BannerAnimated(
          config: BannerConfig.fromJson(config),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );

      case 'bannerImage':
        if (config['isSlider'] == true) {
          return BannerSlider(
            config: BannerConfig.fromJson(config),
            onTap: (itemConfig) {
              NavigateTools.onTapNavigateOptions(
                context: context,
                config: itemConfig,
              );
            },
            key: config['key'] != null ? Key(config['key']) : UniqueKey(),
          );
        }

        return BannerGroupItems(
          config: BannerConfig.fromJson(config),
          onTap: (itemConfig) {
            NavigateTools.onTapNavigateOptions(
              context: context,
              config: itemConfig,
            );
          },
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );

      case 'blog':
        return BlogGrid(
          config: config,
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );

      case 'video':
        return VideoLayout(
          config: config,
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );

      case 'story':
        return StoryWidget(
          config: config,
          onTapStoryText: (cfg) {
            NavigateTools.onTapNavigateOptions(context: context, config: cfg);
          },
        );
      case 'recentView':
        if (Config().isBuilder) {
          return ProductRecentPlaceholder();
        }
        return Services().widget.renderHorizontalListItem(config);
      case 'fourColumn':
      case 'threeColumn':
      case 'twoColumn':
      case 'staggered':
      case 'saleOff':
      case 'card':
      case 'listTile':
        return Services()
            .widget
            .renderHorizontalListItem(config, cleanCache: cleanCache);

      /// New product layout style.
      case 'largeCardHorizontalListItems':
      case 'largeCard':
        return Services().widget.renderLargeCardHorizontalListItems(config);
      case 'simpleVerticalListItems':
      case 'simpleList':
        return SimpleVerticalProductList(
          config: ProductConfig.fromJson(config),
          key: config['key'] != null ? Key(config['key']) : UniqueKey(),
        );

      case 'brand':
        return BrandLayout(
          config: BrandConfig.fromJson(config),
        );

      /// FluxNews
      case 'sliderList':
        return Services().widget.renderSliderList(config);
      case 'sliderItem':
        return Services().widget.renderSliderItem(config);

      case 'geoSearch':
        return Services().widget.renderGeoSearch(config);
      default:
        return const SizedBox();
    }
  }

  void pushNavigation(String name) {
    MainTabControlDelegate.getInstance().changeTab(name.replaceFirst('/', ''));
  }
}
