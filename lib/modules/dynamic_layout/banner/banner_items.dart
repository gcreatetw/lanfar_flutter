import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:lanfar/app.dart';
import 'package:lanfar/from_lanfar/screens/article_content_page.dart';
import 'package:lanfar/from_lanfar/screens/webview_with_cookie.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../common/tools.dart';
import '../config/banner_config.dart';

/// The Banner type to display the image
class BannerImageItem extends StatelessWidget {
  final BannerItemConfig config;
  final double? width;
  final double padding;
  final BoxFit? boxFit;
  final double radius;
  final double? height;
  final Function onTap;

  const BannerImageItem({
    key,
    required this.config,
    required this.padding,
    this.width,
    this.boxFit,
    required this.radius,
    this.height,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _padding = config.padding ?? padding;
    var _radius = config.radius ?? radius;

    final screenSize = MediaQuery.of(context).size;
    final screenWidth =
        screenSize.width / (2 / (screenSize.height / screenSize.width));
    final itemWidth = width ?? screenWidth;

    ///change home page banner
    return GestureDetector(
      onTap: () {
        if (config.url != null && config.url!.startsWith('http')) {
          launch(
            config.url!,
            forceSafariVC: false,
          );
          return;
        }
        if (config.id != null) {
          Navigator.of(App.fluxStoreNavigatorKey.currentContext!).push(
            MaterialPageRoute(
              builder: (context) => ArticleContentPage(
                id: config.id ?? 0,
                isBanner: true,
              ),
            ),
          );
        }
        // if (config.url != null && config.url!.contains('http')) {
        //   Navigator.push(
        //     App.fluxStoreNavigatorKey.currentContext!,
        //     MaterialPageRoute(
        //       builder: (context) {
        //         return WebViewWithCookie(url: config.url!);
        //       },
        //     ),
        //   );
        // }
      },
      child: Container(
        child: config.image.toString().contains('http')
            ? Image.network(
                config.image,
                fit: boxFit ?? BoxFit.cover,
              )
            : Image.asset(
                config.image,
                fit: boxFit ?? BoxFit.fitWidth,
                width: double.maxFinite,
              ),
        // Stack(
        //   alignment: Alignment.center,
        //   children: [
        //     ClipRRect(
        //
        //           child: config.image.toString().contains('http')
        //               ? ImageTools.image(
        //                   fit: boxFit ?? BoxFit.fitWidth,
        //                   url: config.image,
        //                   width: itemWidth,
        //                 )
        //               : Image.asset(
        //                   config.image,
        //                   fit: boxFit ?? BoxFit.fitWidth,
        //                   width: double.maxFinite,
        //                 ),
        //         ),
        //
        //
        //     if (config.description != null)
        //       Align(
        //         alignment: config.description?.alignment ?? Alignment.topCenter,
        //         child: Text(
        //           config.description?.text ?? '',
        //           style: Theme.of(context).textTheme.bodyText1?.copyWith(
        //             color: HexColor(config.description?.color),
        //             fontSize: config.description?.fontSize,
        //             fontFamily: config.description?.fontFamily,
        //             shadows: <Shadow>[
        //               if (config.description?.enableShadow ?? false)
        //                 const Shadow(
        //                   offset: Offset(2.0, 2.0),
        //                   blurRadius: 3.0,
        //                   color: Colors.black,
        //                 ),
        //             ],
        //           ),
        //         ),
        //       ),
        //     if (config.title != null)
        //       Align(
        //         alignment: config.title?.alignment ?? Alignment.topCenter,
        //         child: Text(
        //           config.title?.text ?? '',
        //           style: Theme.of(context).textTheme.bodyText1?.copyWith(
        //             color: HexColor(config.title?.color),
        //             fontSize: config.title?.fontSize,
        //             fontFamily: config.title?.fontFamily,
        //             fontWeight: FontWeight.bold,
        //             shadows: <Shadow>[
        //               if (config.title?.enableShadow ?? false)
        //                 const Shadow(
        //                   offset: Offset(2.0, 2.0),
        //                   blurRadius: 3.0,
        //                   color: Colors.black,
        //                 ),
        //             ],
        //           ),
        //         ),
        //       ),
        //     if (config.button != null)
        //       Align(
        //         alignment: config.button?.alignment ?? Alignment.topCenter,
        //         child: InkWell(
        //           onTap: () => onTap(config.jsonData),
        //           child: Container(
        //             padding: const EdgeInsets.symmetric(
        //                 horizontal: 15, vertical: 10),
        //             decoration: BoxDecoration(
        //                 color: HexColor(config.button?.backgroundColor),
        //                 borderRadius: BorderRadius.circular(5)),
        //             child: Text(
        //               config.button?.text ?? '',
        //               style: Theme.of(context).textTheme.caption?.copyWith(
        //                   color: HexColor(config.button?.textColor),
        //                   fontSize: 16),
        //             ),
        //           ),
        //         ),
        //       ),
        //   ],
        // ),
      ),
    );
  }
}
