import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../common/theme/colors.dart';
import '../../../models/category_model.dart';
import '../../../models/point_model.dart';
import '../config/box_shadow_config.dart';
import '../config/category_item_config.dart';

class CategoryIconItem extends StatelessWidget {
  final double? iconSize;
  final String? name;
  final bool? originalColor;
  final double? borderWidth;
  final double? radius;
  final double spacing;
  final bool? noBackground;
  final BoxShadowConfig? boxShadow;
  final Function? onTap;
  final CategoryItemConfig? itemConfig;
  final double paddingX;
  final double paddingY;
  final double marginX;
  final double marginY;
  final bool? isSelected;

  const CategoryIconItem({
    this.iconSize,
    this.name,
    this.originalColor,
    this.borderWidth,
    this.radius,
    this.spacing = 12.0,
    this.noBackground,
    this.onTap,
    this.itemConfig,
    this.boxShadow,
    this.isSelected,
    required this.paddingX,
    required this.paddingY,
    required this.marginX,
    required this.marginY,
  });

  @override
  Widget build(BuildContext context) {
    final disableBackground =
        ((noBackground ?? false) || (originalColor ?? false));
    final categoryList = Provider.of<CategoryModel>(context).categoryList;

    final id = itemConfig!.category.toString();
    final _name =
        name ?? (categoryList[id] != null ? categoryList[id]!.name : '');
    final image = itemConfig!.image ??
        (categoryList[id] != null ? categoryList[id]!.image : null);
    return GestureDetector(
      onTap: () => onTap?.call(),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        elevation: 5,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
          // constraints: BoxConstraints(maxWidth: iconSize! * 1.2),
          // decoration: borderWidth != null && borderWidth != 0
          //     ? BoxDecoration(
          //   border: Border(
          //     bottom: BorderSide(
          //       width: borderWidth!,
          //       color: Colors.black.withOpacity(0.05),
          //     ),
          //     right: BorderSide(
          //       width: borderWidth!,
          //       color: Colors.black.withOpacity(0.05),
          //     ),
          //   ),
          // )
          //     : null,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              if (image != null)
                Stack(
                  children: [
                    SizedBox(
                      width: 50,
                      height: 40,
                      // padding: const EdgeInsets.symmetric(
                      //   horizontal: 20,
                      //   vertical: 20,
                      // ),
                      // margin: EdgeInsets.symmetric(
                      //   horizontal: marginX,
                      //   vertical: marginY,
                      // ),
                      // decoration: BoxDecoration(
                      //   color: Theme.of(context).primaryColor,
                      //   borderRadius: BorderRadius.circular(radius!),
                      // ),
                      child: Image.asset(
                        itemConfig!.image!,
                        color: name == '點數查詢' ? null : primaryColor,
                        width: 40,
                        height: 40,
                      ),
                    ),
                    if (name == '點數查詢')
                      Positioned(
                        right: 0,
                        top: 0,
                        child: ConstrainedBox(
                          constraints: const BoxConstraints(minWidth: 20),
                          child: Container(
                            height: 20,
                            padding: const EdgeInsets.all(4),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.red,
                            ),
                            child: Text(
                              context.read<PointModel>().lanfarPoint.toString(),
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                  color: Colors.white, fontSize: 12),
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
              if (_name?.isNotEmpty ?? false) ...[
                const SizedBox(height: 6),
                Text(
                  _name!,
                  style: Theme.of(context).textTheme.subtitle1,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                ),
                if (isSelected != null)
                  AnimatedContainer(
                    decoration: BoxDecoration(
                      color: isSelected == true
                          ? Theme.of(context).primaryColor
                          : Colors.transparent,
                      borderRadius: BorderRadius.circular(2.0),
                    ),
                    margin: const EdgeInsets.only(
                      top: 4.0,
                    ),
                    height: 4.0,
                    width: 4.0,
                    duration: const Duration(
                      milliseconds: 400,
                    ),
                  ),
              ],
            ],
          ),
        ),
      ),
    );
  }
}
