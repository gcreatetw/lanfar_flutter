part of '../constants.dart';

/// Default Vendor image
const kDefaultStoreImage = 'assets/images/default-store-banner2.jpg';

/// Default status when Add New Product from app
const kNewProductStatus = 'draft'; // support 'draft', 'pending', 'public'
