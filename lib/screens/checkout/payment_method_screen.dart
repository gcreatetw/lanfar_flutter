import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../common/config.dart';
import '../../../common/constants.dart';
import '../../../env.dart';
import '../../../generated/l10n.dart';
import '../../../models/index.dart'
    show
        AppModel,
        CartModel,
        Order,
        PaymentMethodModel,
        ShippingMethod,
        TaxModel,
        UserModel;
import '../../../modules/native_payment/index.dart';
import '../../../services/index.dart';
import '../../from_lanfar/utils/utils.dart';

class PaymentMethodScreen extends StatefulWidget {
  final Function? onBack;
  final Function? onFinish;
  final Function(bool)? onLoading;

  const PaymentMethodScreen({this.onBack, this.onFinish, this.onLoading});

  @override
  _PaymentMethodScreenState createState() => _PaymentMethodScreenState();
}

class _PaymentMethodScreenState extends State<PaymentMethodScreen>
    with RazorDelegate {
  String? selectedId;
  bool isPaying = false;

  int dropDownValue = 1;

  String creditToken = '';

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      final cartModel = Provider.of<CartModel>(context, listen: false);
      final userModel = Provider.of<UserModel>(context, listen: false);
      final paymentMethodModel =
          Provider.of<PaymentMethodModel>(context, listen: false);
      // Provider.of<PaymentMethodModel>(context, listen: false).getPaymentMethods(
      //     cartModel: cartModel,
      //     shippingMethod: cartModel.shippingMethod,
      //     token: userModel.user != null ? userModel.user!.cookie : null);

      if (kPaymentConfig['EnableReview'] != true) {
        Provider.of<TaxModel>(context, listen: false)
            .getTaxes(Provider.of<CartModel>(context, listen: false),
                (taxesTotal, taxes) {
          Provider.of<CartModel>(context, listen: false).taxesTotal =
              taxesTotal;
          Provider.of<CartModel>(context, listen: false).taxes = taxes;
          setState(() {});
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final cartModel = Provider.of<CartModel>(context);
    final currencyRate = Provider.of<AppModel>(context).currencyRate;
    final paymentMethodModel = Provider.of<PaymentMethodModel>(context);
    final taxModel = Provider.of<TaxModel>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(
          '付款方式',
          style: TextStyle(
            color: Theme.of(context).colorScheme.secondary,
            fontWeight: FontWeight.w400,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: ListenableProvider.value(
        value: paymentMethodModel,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // Text(S.of(context).paymentMethods,
            //     style: const TextStyle(fontSize: 16)),
            // const SizedBox(height: 5),
            // Text(
            //   S.of(context).chooseYourPaymentMethod,
            //   style: TextStyle(
            //     fontSize: 12,
            //     color: Theme.of(context).colorScheme.secondary.withOpacity(0.6),
            //   ),
            // ),
            // Services().widget.renderPayByWallet(context),
            const SizedBox(height: 16),
            Expanded(
              child: Consumer<PaymentMethodModel>(
                builder: (context, model, child) {
                  if (model.isLoading) {
                    return SizedBox(
                        height: 100, child: kLoadingWidget(context));
                  }

                  if (model.message != null) {
                    return SizedBox(
                      height: 100,
                      child: Center(
                          child: Text(model.message!,
                              style: const TextStyle(color: kErrorRed))),
                    );
                  }

                  if (selectedId == null &&
                      paymentMethodModel.paymentMethods.isNotEmpty) {
                    selectedId = paymentMethodModel.paymentMethods
                        .firstWhere((item) => item.enabled!)
                        .id;

                    if (cartModel.paymentMethod != null &&
                        paymentMethodModel.paymentMethods.indexWhere(
                                (element) =>
                                    element.id ==
                                    cartModel.paymentMethod!.id) !=
                            -1) {
                      selectedId = cartModel.paymentMethod!.id;
                    }
                  }
                  return model.isLoading
                      ? kLoadingWidget(context)
                      : Column(
                          children: <Widget>[
                            for (int i = 0;
                                i < model.paymentMethods.length;
                                i++) ...[
                              Builder(builder: (context) {
                                if (cartModel.shippingMethod?.id != null &&
                                    !cartModel.shippingMethod!.id!
                                        .startsWith('betrs_shipping:3-') &&
                                    model.paymentMethods[i].id == 'cod') {
                                  return const SizedBox();
                                }

                                // if (!(model.paymentMethods[i].id ==
                                //     'other_payment')) {
                                //   if ((cartModel.shippingMethod?.id ==
                                //               'betrs_shipping:10-1' &&
                                //           model.paymentMethods[i].id!
                                //               .contains('ctcb')) ||
                                //       cartModel.shippingMethod?.id ==
                                //           'betrs_shipping:3-2' ||
                                //       cartModel.shippingMethod?.id ==
                                //           'betrs_shipping:3-1' ||
                                //       cartModel.ticketMeta != null) {
                                //     if (model.paymentMethods[i].id ==
                                //             environment['creditId'] &&
                                //         (model.paymentMethods[i].cards ==
                                //                 null ||
                                //             model.paymentMethods[i].cards!
                                //                 .isEmpty)) {
                                //       return const SizedBox();
                                //     }
                                //   }
                                // }

                                return Container(
                                  color: Colors.white,
                                  margin: const EdgeInsets.only(bottom: 8),
                                  child: Column(
                                    children: [
                                      model.paymentMethods[i].enabled!
                                          ? Services()
                                              .widget
                                              .renderPaymentMethodItem(context,
                                                  model.paymentMethods[i], (i) {
                                              setState(
                                                () {
                                                  selectedId = i;
                                                },
                                              );
                                            }, selectedId)
                                          : Container(),
                                      if (model.paymentMethods[i].id ==
                                              environment['creditId'] &&
                                          selectedId == environment['creditId'])
                                        Row(
                                          children: [
                                            const SizedBox(width: 20),
                                            const Icon(
                                              Icons.credit_card,
                                              color: primaryColor,
                                            ),
                                            const SizedBox(width: 16),
                                            Expanded(
                                              child: DropdownButton<int>(
                                                isExpanded: true,
                                                dropdownColor: Colors.white,
                                                value: dropDownValue,
                                                onChanged: (value) {
                                                  setState(() {
                                                    dropDownValue = value!;
                                                    if (dropDownValue == 0) {
                                                      creditToken = '';
                                                    } else {
                                                      creditToken = model
                                                              .paymentMethods[i]
                                                              .cards![
                                                          dropDownValue -
                                                              1]['CardToken'];
                                                    }
                                                  });
                                                },
                                                items: [
                                                  for (int j = 1;
                                                      j <
                                                          model
                                                                  .paymentMethods[
                                                                      i]
                                                                  .cards!
                                                                  .length +
                                                              1;
                                                      j++)
                                                    DropdownMenuItem<int>(
                                                      value: j,
                                                      child: Text(model
                                                                  .paymentMethods[i]
                                                                  .cards![j - 1]
                                                              ['CardNoMask'] +
                                                          '【${model.paymentMethods[i].cards![j - 1]['AliasName']}】'),
                                                    ),
                                                ],
                                              ),
                                            ),
                                            const SizedBox(width: 20),
                                          ],
                                        ),
                                    ],
                                  ),
                                );
                              }),
                              // if (!(model.paymentMethods[i].id ==
                              //     'other_payment'))
                              //   if ((cartModel.shippingMethod?.id ==
                              //               'betrs_shipping:10-1' &&
                              //           model.paymentMethods[i].id!
                              //               .contains('ctcb')) ||
                              //       cartModel.shippingMethod?.id ==
                              //           'betrs_shipping:3-2' ||
                              //       cartModel.shippingMethod?.id ==
                              //           'betrs_shipping:3-1' ||
                              //       cartModel.ticketMeta != null)
                              //     if (model.paymentMethods[i].id ==
                              //             environment['creditId'] &&
                              //         (model.paymentMethods[i].cards == null ||
                              //             model.paymentMethods[i].cards!
                              //                 .isEmpty))
                              //       const SizedBox()
                              //     else
                              //       Container(
                              //         color: Colors.white,
                              //         child: Column(
                              //           children: [
                              //             model.paymentMethods[i].enabled!
                              //                 ? Services()
                              //                     .widget
                              //                     .renderPaymentMethodItem(
                              //                         context,
                              //                         model.paymentMethods[i],
                              //                         (i) {
                              //                     setState(
                              //                       () {
                              //                         selectedId = i;
                              //                       },
                              //                     );
                              //                   }, selectedId)
                              //                 : Container(),
                              //             if (model.paymentMethods[i].id ==
                              //                     environment['creditId'] &&
                              //                 selectedId ==
                              //                     environment['creditId'])
                              //               Row(
                              //                 children: [
                              //                   const SizedBox(width: 20),
                              //                   const Icon(
                              //                     Icons.credit_card,
                              //                     color: primaryColor,
                              //                   ),
                              //                   const SizedBox(width: 16),
                              //                   Expanded(
                              //                     child: DropdownButton<int>(
                              //                       isExpanded: true,
                              //                       dropdownColor: Colors.white,
                              //                       value: dropDownValue,
                              //                       onChanged: (value) {
                              //                         setState(() {
                              //                           dropDownValue = value!;
                              //                           if (dropDownValue ==
                              //                               0) {
                              //                             creditToken = '';
                              //                           } else {
                              //                             creditToken = model
                              //                                     .paymentMethods[i]
                              //                                     .cards![
                              //                                 dropDownValue -
                              //                                     1]['CardToken'];
                              //                           }
                              //                         });
                              //                       },
                              //                       items: [
                              //                         for (int j = 1;
                              //                             j <
                              //                                 model
                              //                                         .paymentMethods[
                              //                                             i]
                              //                                         .cards!
                              //                                         .length +
                              //                                     1;
                              //                             j++)
                              //                           DropdownMenuItem<int>(
                              //                             value: j,
                              //                             child: Text(model
                              //                                         .paymentMethods[
                              //                                             i]
                              //                                         .cards![j - 1]
                              //                                     [
                              //                                     'CardNoMask'] +
                              //                                 '【${model.paymentMethods[i].cards![j - 1]['AliasName']}】'),
                              //                           ),
                              //                       ],
                              //                     ),
                              //                   ),
                              //                   const SizedBox(width: 20),
                              //                 ],
                              //               ),
                              //           ],
                              //         ),
                              //       ),
                            ]
                          ],
                        );
                },
              ),
            ),
            const SizedBox(height: 20),
            // Padding(
            //   padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     crossAxisAlignment: CrossAxisAlignment.center,
            //     children: <Widget>[
            //       Text(
            //         S.of(context).subtotal,
            //         style: TextStyle(
            //           fontSize: 14,
            //           color: Theme.of(context)
            //               .colorScheme
            //               .secondary
            //               .withOpacity(0.8),
            //         ),
            //       ),
            //       Text(
            //           PriceTools.getCurrencyFormatted(
            //               cartModel.getSubTotal(), currencyRate,
            //               currency: cartModel.currency)!,
            //           style: const TextStyle(fontSize: 14, color: kGrey400))
            //     ],
            //   ),
            // ),
            // Services().widget.renderShippingMethodInfo(context),
            // if (cartModel.getCoupon() != '')
            //   Padding(
            //     padding:
            //         const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
            //     child: Row(
            //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //       crossAxisAlignment: CrossAxisAlignment.center,
            //       children: <Widget>[
            //         Text(
            //           S.of(context).discount,
            //           style: TextStyle(
            //             fontSize: 14,
            //             color: Theme.of(context)
            //                 .colorScheme
            //                 .secondary
            //                 .withOpacity(0.8),
            //           ),
            //         ),
            //         Text(
            //           cartModel.getCoupon(),
            //           style: Theme.of(context).textTheme.subtitle1!.copyWith(
            //                 fontSize: 14,
            //                 color: Theme.of(context)
            //                     .colorScheme
            //                     .secondary
            //                     .withOpacity(0.8),
            //               ),
            //         )
            //       ],
            //     ),
            //   ),
            // Services().widget.renderTaxes(taxModel, context),
            // Services().widget.renderRewardInfo(context),
            // Services().widget.renderCheckoutWalletInfo(context),
            // Padding(
            //   padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     crossAxisAlignment: CrossAxisAlignment.center,
            //     children: <Widget>[
            //       Text(
            //         S.of(context).total,
            //         style: TextStyle(
            //             fontSize: 16,
            //             color: Theme.of(context).colorScheme.secondary),
            //       ),
            //       Text(
            //         PriceTools.getCurrencyFormatted(
            //             cartModel.getTotal(), currencyRate,
            //             currency: cartModel.currency)!,
            //         style: TextStyle(
            //           fontSize: 20,
            //           color: Theme.of(context).colorScheme.secondary,
            //           fontWeight: FontWeight.w600,
            //           decoration: TextDecoration.underline,
            //         ),
            //       )
            //     ],
            //   ),
            // ),
            const SizedBox(height: 20),
            Row(children: [
              Expanded(
                child: ButtonTheme(
                  height: 45,
                  child: ElevatedButton(
                    style: ButtonStyle(
                        padding: MaterialStateProperty.all<EdgeInsets>(
                            const EdgeInsets.all(16)),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          const RoundedRectangleBorder(
                              borderRadius: BorderRadius.zero),
                        ),
                        backgroundColor:
                            MaterialStateProperty.all(primaryColor)),
                    onPressed: () {
                      final paymentMethod = paymentMethodModel.paymentMethods
                          .firstWhere((item) => item.id == selectedId);
                      if (paymentMethod.id == environment['creditId']) {
                        if (dropDownValue == 0) {
                          Utils.showSnackBar(
                            context,
                            text: '請選擇信用卡',
                          );
                          return;
                        }
                      }
                      cartModel.setPaymentMethod(paymentMethodModel
                          .paymentMethods
                          .firstWhere((item) => item.id == selectedId));

                      widget.onBack!(dropDownValue);
                      Navigator.pop(context);
                    },
                    child: Padding(
                      padding: EdgeInsets.only(
                          bottom: MediaQuery.of(context).padding.bottom),
                      child: Text(
                        S.of(context).confirm.toUpperCase(),
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ),
            ]),
            // if (kPaymentConfig['EnableShipping'] ||
            //     kPaymentConfig['EnableAddress'] ||
            //     (kPaymentConfig['EnableReview'] ?? true))
            //   Center(
            //     child: TextButton(
            //       onPressed: () {
            //         isPaying ? showSnackbar : widget.onBack!();
            //       },
            //       child: Text(
            //         (kPaymentConfig['EnableReview'] ?? true)
            //             ? S.of(context).goBackToReview
            //             : kPaymentConfig['EnableShipping']
            //                 ? S.of(context).goBackToShipping
            //                 : S.of(context).goBackToAddress,
            //         textAlign: TextAlign.center,
            //         style: TextStyle(
            //           decoration: TextDecoration.underline,
            //           fontSize: 15,
            //           color: Theme.of(context).colorScheme.secondary,
            //         ),
            //       ),
            //     ),
            //   )
          ],
        ),
      ),
    );
  }

// void placeOrder(PaymentMethodModel paymentMethodModel, CartModel cartModel) {
//   widget.onLoading!(true);
//   isPaying = true;
//   if (paymentMethodModel.paymentMethods.isNotEmpty) {
//     final paymentMethod = paymentMethodModel.paymentMethods
//         .firstWhere((item) => item.id == selectedId);
//     var isSubscriptionProduct = cartModel.item.values.firstWhere(
//             (element) =>
//         element?.type == 'variable-subscription' ||
//             element?.type == 'subscription',
//         orElse: () => null) !=
//         null;
//     Provider.of<CartModel>(context, listen: false)
//         .setPaymentMethod(paymentMethod);
//
//     /// Use Native payment
//
//     /// Direct bank transfer (BACS)
//
//     if (!isSubscriptionProduct && paymentMethod.id!.contains('bacs')) {
//       widget.onLoading!(false);
//       isPaying = false;
//
//       showModalBottomSheet(
//           context: context,
//           builder: (sContext) => Container(
//             padding: const EdgeInsets.symmetric(
//                 horizontal: 20.0, vertical: 10.0),
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.stretch,
//               mainAxisSize: MainAxisSize.min,
//               children: [
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.end,
//                   children: [
//                     GestureDetector(
//                       onTap: () => Navigator.of(context).pop(),
//                       child: Text(
//                         S.of(context).cancel,
//                         style: Theme.of(context)
//                             .textTheme
//                             .caption!
//                             .copyWith(color: Colors.red),
//                       ),
//                     ),
//                   ],
//                 ),
//                 const SizedBox(height: 10),
//                 Text(
//                   paymentMethod.description!,
//                   style: Theme.of(context).textTheme.caption,
//                 ),
//                 const Expanded(child: SizedBox(height: 10)),
//                 ElevatedButton(
//                   onPressed: () {
//                     Navigator.pop(context);
//                     widget.onLoading!(true);
//                     isPaying = true;
//                     Services().widget.placeOrder(
//                       context,
//                       cartModel: cartModel,
//                       onLoading: widget.onLoading,
//                       paymentMethod: paymentMethod,
//                       success: (Order order) async {
//                         for (var item in order.lineItems) {
//                           var product =
//                           cartModel.getProductById(item.productId!);
//                           if (product?.bookingInfo != null) {
//                             product!.bookingInfo!.idOrder = order.id;
//                             var booking =
//                             await createBooking(product.bookingInfo)!;
//
//                             Tools.showSnackBar(
//                                 Scaffold.of(context),
//                                 booking
//                                     ? 'Booking success!'
//                                     : 'Booking error!');
//                           }
//                         }
//                         widget.onFinish!(order);
//                         widget.onLoading!(false);
//                         isPaying = false;
//                       },
//                       error: (message) {
//                         widget.onLoading!(false);
//                         if (message != null) {
//                           Tools.showSnackBar(
//                               Scaffold.of(context), message);
//                         }
//                         isPaying = false;
//                       },
//                     );
//                   },
//                   style: ElevatedButton.styleFrom(
//                     onPrimary: Colors.white,
//                     primary: Theme.of(context).primaryColor,
//                   ),
//                   child: Text(
//                     S.of(context).ok,
//                   ),
//                 ),
//                 const SizedBox(height: 10),
//               ],
//             ),
//           ));
//
//       return;
//     }
//
//     /// PayPal Payment
//     if (!isSubscriptionProduct &&
//         isNotBlank(kPaypalConfig['paymentMethodId']) &&
//         paymentMethod.id!.contains(kPaypalConfig['paymentMethodId']) &&
//         kPaypalConfig['enabled'] == true) {
//       Navigator.push(
//         context,
//         MaterialPageRoute(
//           builder: (context) => PaypalPayment(
//             onFinish: (number) {
//               if (number == null) {
//                 widget.onLoading!(false);
//                 isPaying = false;
//                 return;
//               } else {
//                 createOrder(paid: true).then((value) {
//                   widget.onLoading!(false);
//                   isPaying = false;
//                 });
//               }
//             },
//           ),
//         ),
//       );
//       return;
//     }
//
//     /// MercadoPago payment
//     if (!isSubscriptionProduct &&
//         isNotBlank(kMercadoPagoConfig['paymentMethodId']) &&
//         paymentMethod.id!.contains(kMercadoPagoConfig['paymentMethodId']) &&
//         kMercadoPagoConfig['enabled'] == true) {
//       Navigator.push(
//         context,
//         MaterialPageRoute(
//           builder: (context) => MercadoPagoPayment(
//             onFinish: (number) {
//               if (number == null) {
//                 widget.onLoading!(false);
//                 isPaying = false;
//                 return;
//               } else {
//                 createOrder(paid: true).then((value) {
//                   widget.onLoading!(false);
//                   isPaying = false;
//                 });
//               }
//             },
//           ),
//         ),
//       );
//       return;
//     }
//
//     /// RazorPay payment
//     if (!isSubscriptionProduct &&
//         paymentMethod.id!.contains(kRazorpayConfig['paymentMethodId']) &&
//         kRazorpayConfig['enabled'] == true) {
//       Services().api.createRazorpayOrder({
//         'amount': (cartModel.getTotal()! * 100).toInt().toString(),
//         'currency': 'INR'
//       }).then((value) {
//         final _razorServices = RazorServices(
//           amount: (cartModel.getTotal()! * 100).toInt().toString(),
//           keyId: kRazorpayConfig['keyId'],
//           delegate: this,
//           orderId: value,
//           userInfo: RazorUserInfo(
//             email: cartModel.address!.email,
//             phone: cartModel.address!.phoneNumber,
//             fullName:
//             '${cartModel.address!.firstName} ${cartModel.address!.lastName}',
//           ),
//         );
//         _razorServices.openPayment();
//       }).catchError((e) {
//         widget.onLoading!(false);
//         Tools.showSnackBar(Scaffold.of(context), e);
//         isPaying = false;
//       });
//       return;
//     }
//
//     if (paymentMethod.id == environment['creditId']) {
//       if (dropDownValue == 0) {
//         showSnackbar('請選擇信用卡');
//         widget.onLoading!(false);
//         isPaying = false;
//         return;
//       }
//       Services().widget.placeOrder(
//         context,
//         cartModel: cartModel,
//         onLoading: widget.onLoading,
//         paymentMethod: paymentMethod,
//         success: (Order order) async {
//           await Navigator.push(
//             context,
//             MaterialPageRoute(
//               builder: (context) => PaymentWebview(
//                 canGoBack: false,
//                 url:
//                 '${environment['serverConfig']['url']}/wc-api/ceompay?order_id=${order.id}&ctcb_ceom_credit=$creditToken',
//                 onFinish: (number) {
//                   if (number != null) {
//                     widget.onFinish!(Order(number: number));
//                   }
//                   widget.onLoading!(false);
//                   isPaying = false;
//                 },
//               ),
//             ),
//           );
//           for (var item in order.lineItems) {
//             var product = cartModel.getProductById(item.productId!);
//             if (product?.bookingInfo != null) {
//               product!.bookingInfo!.idOrder = order.id;
//               var booking = await createBooking(product.bookingInfo)!;
//
//               Tools.showSnackBar(Scaffold.of(context),
//                   booking ? 'Booking success!' : 'Booking error!');
//             }
//           }
//         },
//         error: (message) {
//           widget.onLoading!(false);
//           if (message != null) {
//             Tools.showSnackBar(Scaffold.of(context), message);
//           }
//           isPaying = false;
//         },
//       );
//       return;
//     }
//
//     /// Use WebView Payment per frameworks
//     Services().widget.placeOrder(
//       context,
//       cartModel: cartModel,
//       onLoading: widget.onLoading,
//       paymentMethod: paymentMethod,
//       success: (Order? order) async {
//         if (order != null) {
//           for (var item in order.lineItems) {
//             var product = cartModel.getProductById(item.productId!);
//             if (product?.bookingInfo != null) {
//               product!.bookingInfo!.idOrder = order.id;
//               var booking = await createBooking(product.bookingInfo)!;
//
//               Tools.showSnackBar(Scaffold.of(context),
//                   booking ? 'Booking success!' : 'Booking error!');
//             }
//           }
//           widget.onFinish!(order);
//         }
//         widget.onLoading!(false);
//         isPaying = false;
//       },
//       error: (message) {
//         widget.onLoading!(false);
//         if (message != null) {
//           Tools.showSnackBar(Scaffold.of(context), message);
//         }
//
//         isPaying = false;
//       },
//     );
//   }
// }

// Future<bool>? createBooking(BookingModel? bookingInfo) async {
//   return Services().api.createBooking(bookingInfo)!;
// }
//
// Future<void> createOrder(
//     {paid = false, bacs = false, cod = false, transactionId = ''}) async {
//   widget.onLoading!(true);
//   await Services().widget.createOrder(
//     context,
//     paid: paid,
//     cod: cod,
//     bacs: bacs,
//     transactionId: transactionId,
//     onLoading: widget.onLoading,
//     success: (Order order) async {
//       await Services()
//           .api
//           .updateOrderIdForRazorpay(transactionId, order.number);
//       widget.onFinish!(order);
//     },
//     error: (message) {
//       Tools.showSnackBar(Scaffold.of(context), message);
//     },
//   );
//   widget.onLoading!(false);
// }
//
// @override
// void handlePaymentSuccess(PaymentSuccessResponse response) {
//   createOrder(paid: true, transactionId: response.paymentId).then((value) {
//     widget.onLoading!(false);
//     isPaying = false;
//   });
// }
//
// @override
// void handlePaymentFailure(PaymentFailureResponse response) {
//   widget.onLoading!(false);
//   isPaying = false;
//   final body = convert.jsonDecode(response.message!);
//   if (body['error'] != null &&
//       body['error']['reason'] != 'payment_cancelled') {
//     Tools.showSnackBar(Scaffold.of(context), body['error']['description']);
//   }
//   printLog(response.message);
// }
}
