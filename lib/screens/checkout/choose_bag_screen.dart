import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/theme/colors.dart';
import '../../models/cart/cart_base.dart';
import '../../models/cart/mixin/cart_mixin.dart';

class ChooseBagScreen extends StatefulWidget {
  const ChooseBagScreen({Key? key}) : super(key: key);

  @override
  State<ChooseBagScreen> createState() => _ChooseBagScreenState();
}

class _ChooseBagScreenState extends State<ChooseBagScreen> {
  int value = 0;

  @override
  void initState() {
    final cartModel = Provider.of<CartModel>(context, listen: false);
    if (cartModel.purchaseBagType != null) {
      value = cartModel.purchaseBagType!.index;
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final cartModel = Provider.of<CartModel>(context, listen: false);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          '訂購方式',
          style: TextStyle(
            color: Theme.of(context).colorScheme.secondary,
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
      body: Column(
        children: [
          Expanded(
              child: SingleChildScrollView(
            child: ListView.separated(
              padding: const EdgeInsets.symmetric(vertical: 15),
              shrinkWrap: true,
              itemCount: PurchaseBagType.values.length,
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          value = index;
                        });
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 15, horizontal: 20),
                        color: value == index ? primaryColor : Colors.white,
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                PurchaseBagType.values[index].name,
                                style: TextStyle(
                                  color: value == index ? Colors.white : null,
                                ),
                              ),
                            ),
                            if (value == index)
                              const Icon(
                                Icons.check,
                                color: Colors.white,
                              ),
                          ],
                        ),
                      ),
                    ),
                  ],
                );
              },
              separatorBuilder: (BuildContext context, int index) {
                return const SizedBox(height: 15);
              },
            ),
          )),
          ElevatedButton(
            onPressed: () async {
              cartModel.purchaseBagType = PurchaseBagType.values[value];
              Navigator.of(context).pop();
            },
            style: ButtonStyle(
                padding: MaterialStateProperty.all<EdgeInsets>(
                    const EdgeInsets.all(16)),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  const RoundedRectangleBorder(borderRadius: BorderRadius.zero),
                ),
                backgroundColor: MaterialStateProperty.all(primaryColor)),
            child: Container(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).padding.bottom),
              width: double.maxFinite,
              child: const Text(
                '確認',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
