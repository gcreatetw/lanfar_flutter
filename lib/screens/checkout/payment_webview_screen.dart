import 'dart:convert';

import 'package:crypto/crypto.dart' as crypto;
import 'package:flutter/material.dart';
import 'package:inspireui/utils/encode.dart';
import 'package:intl/intl.dart';
import 'package:provider/src/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../common/config.dart';
import '../../env.dart';
import '../../models/user_model.dart';
import '../../services/index.dart';
import '../base_screen.dart';
// import 'payment_webview_plugin.dart';

const enableWebviewFlutter = true;

class PaymentWebview extends StatefulWidget {
  final String? url;
  final Function? onFinish;
  final Function? onClose;
  final bool canGoBack;

  const PaymentWebview(
      {this.onFinish, this.onClose, this.url, this.canGoBack = true});

  @override
  State<StatefulWidget> createState() {
    return PaymentWebviewState();
  }
}

class PaymentWebviewState extends BaseScreen<PaymentWebview> {
  int selectedIndex = 1;

  late WebViewController webViewController;

  @override
  void initState() {
    var checkoutMap = <dynamic, dynamic>{
      'url': '',
      'headers': <String, String>{}
    };

    if (widget.url != null) {
      checkoutMap['url'] = widget.url;
    } else {
      final paymentInfo = Services().widget.getPaymentUrl(context)!;
      checkoutMap['url'] = paymentInfo['url'];
      if (paymentInfo['headers'] != null) {
        checkoutMap['headers'] =
            Map<String, String>.from(paymentInfo['headers']);
      }
    }

    // // Enable webview payment plugin
    /// make sure to import 'payment_webview_plugin.dart';
    // return PaymentWebviewPlugin(
    //   url: checkoutMap['url'],
    //   headers: checkoutMap['headers'],
    //   onClose: widget.onClose,
    //   onFinish: widget.onFinish,
    // );
    final md5 = crypto.md5
        .convert(utf8
            .encode('gcreate${DateFormat('yyyyMMdd').format(DateTime.now())}'))
        .toString();
    webViewController = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setNavigationDelegate(NavigationDelegate(
        onProgress: (progress) {
          if (progress == 100) {}
        },
        onPageStarted: (url) {
          if (url.contains(environment['serverConfig']['url'])) {
            setState(() {
              selectedIndex = 1;
            });
          }
          // final md5 = crypto.md5
          //     .convert(utf8.encode(
          //     'gcreate${DateFormat('yyyyMMdd').format(DateTime.now())}'))
          //     .toString();
          // print(url + '?' +'user_login=${context.read<UserModel>().user?.username ?? ''}&gc_nonce=$md5&appstyle=1');
          // if(url.contains('${environment['serverConfig']['url']}') && !url.contains('user_login') &&!url.contains('result.php')){
          //   webViewController.loadUrl(url + (url.endsWith('/') ? '?': '&') + 'cookie=${EncodeUtils.encodeCookie(context.read<UserModel>().user!.cookie!)}' +'&user_login=${context.read<UserModel>().user?.username ?? ''}&gc_nonce=$md5&appstyle=1');
          // }
        },
        onPageFinished: (url) {
          print(url);
          if (url.contains(environment['serverConfig']['url'] + '/?cookie=')) {
            webViewController.loadRequest(Uri.parse(checkoutMap['url']));
            return;
          }
          setState(() {
            selectedIndex = 0;
          });
          if (url.contains('/order-received/')) {
            final items = url.split('/order-received/');
            if (items.length > 1) {
              final number = items[1].split('/')[0];
              Navigator.of(context).pop();
              widget.onFinish!(number);
            }
          }
          if (url.contains('checkout/success')) {
            Navigator.of(context).pop();
            widget.onFinish!('0');
          }

          // shopify url final checkout
          if (url.contains('thank_you')) {
            Navigator.of(context).pop();
            widget.onFinish!('0');
          }
        },
      ))
      ..loadRequest(
        Uri.parse(environment['serverConfig']['url'] +
            '?cookie=${EncodeUtils.encodeCookie(context.read<UserModel>().user!.cookie!)}&user_login=${context.read<UserModel>().user?.username ?? ''}&gc_nonce=$md5&appstyle=1'),
      );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: widget.canGoBack
            ? IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () {
                  widget.onFinish!(null);
                  Navigator.of(context).pop();

                  if (widget.onClose != null) {
                    widget.onClose!();
                  }
                })
            : null,
        backgroundColor: Theme.of(context).backgroundColor,
        elevation: 0.0,
      ),
      body: IndexedStack(
        index: selectedIndex,
        children: [
          WebViewWidget(
            controller: webViewController,
          ),
          Center(
            child: kLoadingWidget(context),
          )
        ],
      ),
    );
  }
}
