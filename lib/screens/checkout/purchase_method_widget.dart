import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/theme/colors.dart';
import '../../models/index.dart';
import 'purchase_method_screen.dart';

/** 需求:
 *    20250116 隱藏「分開列印」的選項，只留下「無分開列印」
 */

class PurchaseMethodWidget extends StatefulWidget {
  final Function(PurchaseType purchaseMethod) setStateClick;
  final CartModel cartModel;

  const PurchaseMethodWidget({
    Key? key,
    required this.setStateClick,
    required this.cartModel,
  }) : super(key: key);

  @override
  _PurchaseMethodWidgetState createState() => _PurchaseMethodWidgetState();
}

class _PurchaseMethodWidgetState extends State<PurchaseMethodWidget> with TickerProviderStateMixin {
  int purchaseMethodIndex = 0;
  int radioValue = 0;
  bool canPurchasingAgent = false;

  @override
  void initState() {
    super.initState();
    var cartModel = widget.cartModel;

    final userModel = Provider.of<UserModel>(context, listen: false);
    if (cartModel.purchaseMethod != null) {
      purchaseMethodIndex = cartModel.purchaseMethod!.index;
    }

    if (userModel.user!.grade_1! >= 60) {
      ///特殊折價權不能用代購
      if (cartModel.discount?.coupon != null) {
        if (!cartModel.discount!.coupon!.code!.startsWith('LANFAR')) {
          setState(() {
            canPurchasingAgent = true;
          });
        }
      } else {
        setState(() {
          canPurchasingAgent = true;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var cartModel = widget.cartModel;

    // if (!cartModel.canSeparate()) {
    //   setState(() {
    //     radioValue = 0;
    //   });
    // }

    return ListView.separated(
      physics: const NeverScrollableScrollPhysics(),
      // padding: const EdgeInsets.symmetric(vertical: 15),
      shrinkWrap: true,
      itemCount: PurchaseType.values.length,
      itemBuilder: (context, index) {
        if (!canPurchasingAgent && index == 2) {
          return const SizedBox();
        }

        return Column(
          children: [
            GestureDetector(
              onTap: () {
                setState(() {
                  purchaseMethodIndex = index;
                  if (PurchaseType.values[purchaseMethodIndex] != cartModel.purchaseMethod) {
                    cartModel.setShippingMethod(null);
                    cartModel.invoiceOffice = null;
                    cartModel.invoiceDeliverType = null;
                    cartModel.carrierNo = null;
                    Provider.of<CartModel>(context, listen: false).setAddress(
                      Address(
                        firstName: '',
                        lastName: '',
                        email: '',
                        street: '',
                        apartment: '',
                        block: '',
                        city: '',
                        state: '',
                        country: 'TW',
                        phoneNumber: '',
                        zipCode: '',
                        mapUrl: '',
                      ),
                    );
                  }
                  cartModel.purchaseMethod = PurchaseType.values[purchaseMethodIndex];

                  // if (PurchaseType.values[purchaseMethodIndex] == PurchaseType.golden) {
                  //   cartModel.invoiceType = InvoiceType.values[radioValue];
                  //   if (!cartModel.canSeparate()) {
                  //     radioValue = 0;
                  //   }
                  // } else {
                  //   cartModel.invoiceType = null;
                  // }
                });
                widget.setStateClick(PurchaseType.values[purchaseMethodIndex]);
              },
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                color: purchaseMethodIndex == index ? primaryColor : Colors.white,
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        PurchaseType.values[index].title,
                        style: TextStyle(
                          color: purchaseMethodIndex == index ? Colors.white : null,
                        ),
                      ),
                    ),
                    if (purchaseMethodIndex == index)
                      const Icon(
                        Icons.check,
                        color: Colors.white,
                      ),
                  ],
                ),
              ),
            ),
            if (index == 2)
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          // if (cartModel.purchaseMethod == PurchaseType.standin) {
                          //   setState(() {
                          //     radioValue = 0;
                          //   });
                          // }
                        },
                        child: Row(
                          children: [
                            Radio<int>(
                                value: 0,
                                groupValue: radioValue,
                                onChanged: (v) {
                                  if (cartModel.purchaseMethod == PurchaseType.standin) {
                                    setState(() {
                                      radioValue = v!;
                                    });
                                  }
                                }),
                            const Text('無分開列印')
                          ],
                        ),
                      ),
                      // GestureDetector(
                      //   onTap: cartModel.canSeparate()
                      //       ? () {
                      //           if (cartModel.purchaseMethod == PurchaseType.standin) {
                      //             setState(() {
                      //               radioValue = 1;
                      //             });
                      //           }
                      //         }
                      //       : null,
                      //   child: Row(
                      //     children: [
                      //       Radio<int>(
                      //           value: 1,
                      //           groupValue: radioValue,
                      //           onChanged: cartModel.canSeparate()
                      //               ? (v) {
                      //                   if (cartModel.purchaseMethod == PurchaseType.standin) {
                      //                     setState(() {
                      //                       radioValue = v!;
                      //                     });
                      //                   }
                      //                 }
                      //               : null),
                      //       Text('分開列印', style: TextStyle(color: !cartModel.canSeparate() ? HexColor('#999999') : null))
                      //     ],
                      //   ),
                      // ),
                    ],
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Text('※限制不可以搭配行銷活動、優惠券、課程點數'),
                    // child: Text('※如.「分開列印」限購物車品項一個\n※限制不可以搭配行銷活動、優惠券、課程點數'),
                  )
                ],
              ),
          ],
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return const SizedBox(height: 4);
      },
    );
  }
}
