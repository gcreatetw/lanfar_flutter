import 'package:country_pickers/country.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';

import '../../common/constants.dart';
import '../../from_lanfar/contants/constants.dart';
import '../../models/entities/address.dart';
import '../../models/user_model.dart';
import '../../services/index.dart';

class AddAddressScreen extends StatefulWidget {
  final Address? address;
  final int? maxNumber;

  const AddAddressScreen({
    Key? key,
    this.address,
    this.maxNumber,
  }) : super(key: key);

  @override
  _AddAddressScreenState createState() => _AddAddressScreenState();
}

class _AddAddressScreenState extends State<AddAddressScreen> {
  final _formKey = GlobalKey<FormState>();

  bool cityNotFill = false;
  bool townNotFill = false;

  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _cityController = TextEditingController();
  final TextEditingController _streetController = TextEditingController();
  final TextEditingController _zipController = TextEditingController();
  final TextEditingController _countryController = TextEditingController();

  final _lastNameNode = FocusNode();
  final _firstNameNode = FocusNode();
  final _phoneNode = FocusNode();
  final _cityNode = FocusNode();
  final _streetNode = FocusNode();
  final _zipNode = FocusNode();
  final _countryNode = FocusNode();

  Address address = Address();
  List<Country>? countries = [];
  List<dynamic> states = [];

  bool saving = false;

  Future<void> saveDataToLocal() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      if (address.state == null || address.city == null) {
        if (address.city == null) {
          setState(() {
            townNotFill = true;
          });
        } else {
          setState(() {
            townNotFill = false;
          });
        }
        if (address.state == null) {
          setState(() {
            cityNotFill = true;
          });
        } else {
          setState(() {
            cityNotFill = false;
          });
        }
        return;
      }
      setState(() {
        townNotFill = false;
      });

      setState(() {
        cityNotFill = false;
      });
      if (_streetController.text.length < 5) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('地址不得少於5個字'),
        ));
        return;
      }
      setState(() {
        saving = true;
      });
      address.lastName = _lastNameController.text;
      address.firstName = _firstNameController.text;
      address.phoneNumber = _phoneController.text;
      address.country = 'TW';
      address.street = _streetController.text;
      address.zipCode = _zipController.text;
      final response = await Services().api.updateAddress(
            userId: context.read<UserModel>().user?.username ?? '',
            action: widget.address == null ? 'insert' : 'update',
            addressId: widget.address?.id,
            addressData: Address().toApiJson(address),
          );
      if (response != null) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(response),
          ),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('儲存成功'),
          ),
        );
        Navigator.of(context).pop(true);
      }
      setState(() {
        saving = false;
      });

      // final storage = LocalStorage('address');
      // var _list = <Address?>[];
      // if (widget.address == null) {
      //   _list.add(address);
      // }
      //
      // try {
      //   final ready = await storage.ready;
      //   if (ready) {
      //     var data = storage.getItem('data');
      //     if (data != null) {
      //       var _data = data as List;
      //       for (var item in _data) {
      //         final add = Address.fromLocalJson(item);
      //         _list.add(add);
      //       }
      //     }
      //     if (widget.address != null) {
      //       _list[widget.index!] = address;
      //     }
      //     await storage.setItem(
      //         'data',
      //         _list.map((item) {
      //           return item!.toJsonEncodable();
      //         }).toList());
      //     Navigator.of(context).pop(true);
      //   }
      // } catch (err) {
      //   printLog(err);
      // }
    } else {
      if (address.state == null || address.city == null) {
        if (address.city == null) {
          setState(() {
            townNotFill = true;
          });
        } else {
          setState(() {
            townNotFill = false;
          });
        }
        if (address.state == null) {
          setState(() {
            cityNotFill = true;
          });
        } else {
          setState(() {
            cityNotFill = false;
          });
        }
        return;
      }
      setState(() {
        townNotFill = false;
      });

      setState(() {
        cityNotFill = false;
      });
    }
  }

  Future<void> readData(Address addr) async {
    _lastNameController.text = addr.lastName ?? '';
    _firstNameController.text = addr.firstName ?? '';
    _phoneController.text = addr.phoneNumber ?? '';
    _streetController.text = addr.street ?? '';
    _zipController.text = addr.zipCode ?? '';
    address.state = addr.state ?? '';
    address.city = addr.city ?? '';
    address.defaultAddress = addr.defaultAddress;
    setState(() {});
    // final storage = LocalStorage('address');
    // var _list = <Address?>[];
    // try {
    //   final ready = await storage.ready;
    //   if (ready) {
    //     var data = storage.getItem('data');
    //     if (data != null) {
    //       var _data = data as List;
    //       for (var item in _data) {
    //         final add = Address.fromLocalJson(item);
    //         _list.add(add);
    //       }
    //     }
    //     _lastNameController.text = addr.lastName ?? '';
    //     _firstNameController.text = addr.firstName ?? '';
    //     _phoneController.text = addr.phoneNumber ?? '';
    //     _streetController.text = addr.street ?? '';
    //     _zipController.text = addr.zipCode ?? '';
    //     address.state = addr.state ?? '';
    //     address.city = addr.city ?? '';
    //   }
    //   setState(() {});
    // } catch (err) {
    //   printLog(err);
    // }
  }

  @override
  void initState() {
    super.initState();
    _countryController.text = '台灣';
    if (widget.address != null) {
      readData(widget.address!);
    }else{
      _lastNameController.text = '預設地址-${widget.maxNumber}';
      setState(() {});
    }
  }

  @override
  void dispose() {
    _lastNameController.dispose();
    _firstNameController.dispose();
    _phoneController.dispose();
    _cityController.dispose();
    _streetController.dispose();
    _zipController.dispose();
    _countryController.dispose();

    _lastNameNode.dispose();
    _firstNameNode.dispose();
    _phoneNode.dispose();
    _cityNode.dispose();
    _streetNode.dispose();
    _zipNode.dispose();
    _countryNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text(
            '新增收件資訊',
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        body: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    const SizedBox(height: 8),
                    textField(
                        controller: _lastNameController,
                        node: _lastNameNode,
                        nextNode: _firstNameNode,
                        hint: '別名'),
                    Form(
                      key: _formKey,
                      child: AutofillGroup(
                        child: Column(
                          children: [
                            textField(
                                controller: _firstNameController,
                                node: _firstNameNode,
                                nextNode: _phoneNode,
                                hint: '收件人姓名'),
                            textField(
                                controller: _phoneController,
                                node: _phoneNode,
                                nextNode: _streetNode,
                                textInputType: TextInputType.number,
                                hint: '收件人電話/手機'),
                            // textField(
                            //     controller: _countryController,
                            //     node: _countryNode,
                            //     nextNode: _firstNameNode,
                            //     hint: '國家',
                            //     enable: false),
                            const SizedBox(height: 8),
                            Row(
                              children: [
                                Expanded(
                                    child: Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 16),
                                      child: renderStateInput(),
                                    ),
                                    if (townNotFill || cityNotFill)
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 16),
                                        child: Text(
                                          '*此為必填欄位',
                                          style: TextStyle(
                                              color: cityNotFill
                                                  ? Colors.red
                                                  : Colors.transparent,
                                              fontSize: 14),
                                        ),
                                      ),
                                  ],
                                )),
                                Expanded(
                                    child: Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 16),
                                      child: renderTownInput(),
                                    ),
                                    if (townNotFill || cityNotFill)
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 16),
                                        child: Text(
                                          '*此為必填欄位',
                                          style: TextStyle(
                                              color: townNotFill
                                                  ? Colors.red
                                                  : Colors.transparent,
                                              fontSize: 14),
                                        ),
                                      ),
                                  ],
                                ))
                              ],
                            ),
                            const SizedBox(height: 8),
                            textField(
                                controller: _streetController,
                                node: _streetNode,
                                end: true,
                                hint: '地址'),
                            // textField(
                            //     controller: _zipController,
                            //     node: _zipNode,
                            //     textInputType: TextInputType.number,
                            //     end: true,
                            //     hint: '郵遞區號'),
                            Row(
                              children: [
                                const SizedBox(width: 16),
                                const Text(
                                  '預設地址',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                const Spacer(),
                                Switch(
                                  value: address.defaultAddress ?? false,
                                  onChanged: (v) {
                                    setState(() {
                                      address.defaultAddress = v;
                                    });
                                  },
                                ),
                                const SizedBox(width: 16),
                              ],
                            ),
                            const SizedBox(height: 50),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            ElevatedButton(
              onPressed: saving
                  ? null
                  : () async {
                      await saveDataToLocal();
                    },
              style: ButtonStyle(
                  padding: MaterialStateProperty.all<EdgeInsets>(
                      const EdgeInsets.all(16)),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    const RoundedRectangleBorder(
                        borderRadius: BorderRadius.zero),
                  ),
                  backgroundColor: MaterialStateProperty.all(primaryColor)),
              child: Container(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).padding.bottom),
                width: double.maxFinite,
                child: Text(
                  saving ? '儲存中...' : '儲存',
                  textAlign: TextAlign.center,
                  style: const TextStyle(color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget renderStateInput() {
    var items = <DropdownMenuItem>[];
    var cities = <String>[];
    for (var item in Constants.citiesWithZipCode) {
      cities.add(item['name']);
      items.add(
        DropdownMenuItem(
          value: item['name'],
          child: Text(item['name']),
        ),
      );
    }
    String? value;

    if (address.state != null && cities.contains(address.state)) {
      value = address.state;
    }

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(3)),
      child: DropdownButton(
        underline: const SizedBox(),
        icon: const Icon(
          Icons.keyboard_arrow_down,
          color: primaryColor,
        ),
        items: items,
        value: value,
        onChanged: (dynamic val) {
          setState(() {
            address.state = val;
            cityNotFill = false;
          });
        },
        isExpanded: true,
        hint: const Text('選擇縣市'),
      ),
    );
  }

  Widget renderTownInput() {
    var items = <DropdownMenuItem>[];
    var towns = <Map<String, dynamic>>[];
    var cities = <String>[];
    for (var item in Constants.citiesWithZipCode) {
      cities.add(item['name']);
    }
    if (address.state != null && cities.contains(address.state)) {
      towns = Constants.citiesWithZipCode
          .where((element) => element['name'] == address.state)
          .single['districts'];
      for (var item in towns) {
        items.add(
          DropdownMenuItem(
            value: item['name'],
            child: Text(item['name']),
          ),
        );
      }
    }

    String? value;

    if (towns.indexWhere((element) => element['name'] == address.city) != -1) {
      value = address.city;
    }

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(3)),
      child: DropdownButton(
        underline: const SizedBox(),
        icon: Icon(
          Icons.keyboard_arrow_down,
          color: items.isEmpty ? Colors.grey : primaryColor,
        ),
        items: items,
        value: value,
        onChanged: (dynamic val) {
          setState(() {
            address.city = val;
            address.zipCode =
                towns.firstWhere((element) => element['name'] == val)['zip'];
            townNotFill = false;
          });
        },
        isExpanded: true,
        hint: const Text('選擇地區'),
      ),
    );
  }

  Widget textField({
    required TextEditingController controller,
    required FocusNode node,
    TextInputType? textInputType,
    FocusNode? nextNode,
    required String hint,
    bool end = false,
    bool enable = true,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: TextFormField(
        enabled: enable,
        controller: controller,
        focusNode: node,
        textInputAction: end ? TextInputAction.done : TextInputAction.next,
        keyboardType: textInputType,
        onFieldSubmitted: (_) =>
            end ? null : FocusScope.of(context).requestFocus(nextNode),
        validator: (val) {
          return val!.isEmpty ? '*此為必填欄位' : null;
        },
        decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            contentPadding:
                const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(
                color: Colors.transparent,
                width: 0,
              ),
              borderRadius: BorderRadius.circular(3),
            ),
            floatingLabelBehavior: FloatingLabelBehavior.auto,
            enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(
                color: Colors.transparent,
                width: 0,
              ),
              borderRadius: BorderRadius.circular(3),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderSide: const BorderSide(
                color: Colors.transparent,
                width: 0,
              ),
              borderRadius: BorderRadius.circular(3),
            ),
            errorBorder: OutlineInputBorder(
              borderSide: const BorderSide(
                color: Colors.transparent,
                width: 0,
              ),
              borderRadius: BorderRadius.circular(3),
            ),
            border: InputBorder.none,
            labelText: hint),
      ),
    );
  }
}
