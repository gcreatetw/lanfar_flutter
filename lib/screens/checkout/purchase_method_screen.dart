import 'package:flutter/material.dart';
import 'package:inspireui/utils/colors.dart';
import 'package:provider/provider.dart';

import '../../common/theme/colors.dart';
import '../../from_lanfar/contants/constants.dart';
import '../../models/index.dart';

enum PurchaseType {
  general,
  golden,
  standin,
}

extension PurchaseTypeNo on PurchaseType {
  String get num {
    switch (this) {
      case PurchaseType.general:
        return 'general';
      case PurchaseType.standin:
        return 'standin';
      case PurchaseType.golden:
        return 'golden';
    }
  }
}

extension PurchaseTypeTitle on PurchaseType {
  String get title {
    switch (this) {
      case PurchaseType.general:
        return '一般訂單(當月行銷活動)';
      case PurchaseType.golden:
        return '點石成金(享3/6/12期好禮)';
      case PurchaseType.standin:
        return '代購下單';

    }
  }
}

enum InvoiceType {
  noseparate,
  separate,
}

extension InvoiceTypeNo on InvoiceType {
  String get num {
    switch (this) {
      case InvoiceType.noseparate:
        return 'noseparate';
      case InvoiceType.separate:
        return 'separate';
    }
  }
}

extension InvoiceTypeTitle on InvoiceType {
  String get title {
    switch (this) {
      case InvoiceType.noseparate:
        return '無分開列印';
      case InvoiceType.separate:
        return '分開列印';
    }
  }
}

class PurchaseMethodScreen extends StatefulWidget {
  final Function setStateClick;

  const PurchaseMethodScreen({
    Key? key,
    required this.setStateClick,
  }) : super(key: key);

  @override
  _PurchaseMethodScreenState createState() => _PurchaseMethodScreenState();
}

class _PurchaseMethodScreenState extends State<PurchaseMethodScreen>
    with TickerProviderStateMixin {
  int purchaseMethodValue = 0;

  List<PurchaseType> purchaseMethodTitles = [
    PurchaseType.general,
    PurchaseType.standin,
  ];

  late AnimationController animationController;

  late CurvedAnimation curvedAnimation;

  int radioValue = 0;

  bool canPurchasingAgent = false;
  bool canSeparate = false;

  @override
  void initState() {
    super.initState();
    final cartModel = Provider.of<CartModel>(context, listen: false);
    final userModel = Provider.of<UserModel>(context, listen: false);
    if (cartModel.purchaseMethod != null) {
      purchaseMethodValue = cartModel.purchaseMethod!.index;
    }

    animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 200),
    );
    curvedAnimation = CurvedAnimation(
      parent: animationController,
      curve: Curves.fastOutSlowIn,
    );
    if (purchaseMethodValue == 1) {
      animationController.forward();
    }
    if (userModel.user!.grade_1! >= 60) {
      ///特殊折價權不能用代購
      if (cartModel.discount?.coupon != null) {
        if (!cartModel.discount!.coupon!.code!.startsWith('LANFAR')) {
          setState(() {
            canPurchasingAgent = true;
          });
        }
      } else {
        setState(() {
          canPurchasingAgent = true;
        });
      }
    }
    if (cartModel.productsInCart.keys.toList().length == 1) {
      if (cartModel.productsInCart[cartModel.productsInCart.keys.toList()[0]]!.toInt() > 1 &&
          cartModel.giftGroupList.indexWhere((element) => element.chooseId != null) == -1) {
        if (cartModel.giftsInCart.isEmpty) {
          if (cartModel.discount?.gifts != null && cartModel.discount!.gifts.isEmpty) {
            setState(() {
              canSeparate = true;
            });
          } else if (cartModel.discount?.gifts == null) {
            setState(() {
              canSeparate = true;
            });
          }
          if(cartModel.discount?.coupon != null){
            setState(() {
              canSeparate = false;
            });
          }
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final cartModel = Provider.of<CartModel>(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          '訂購方式',
          style: TextStyle(
            color: Theme.of(context).colorScheme.secondary,
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
      body: Column(
        children: [
          Expanded(
              child: SingleChildScrollView(
            child: ListView.separated(
              padding: const EdgeInsets.symmetric(vertical: 15),
              shrinkWrap: true,
              itemCount: PurchaseType.values.length,
              itemBuilder: (context, index) {
                if (!canPurchasingAgent && index == 1) {
                  return const SizedBox();
                }
                return Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          purchaseMethodValue = index;
                        });
                        if (index == 1) {
                          animationController.forward();
                        } else {
                          animationController.reverse();
                        }
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 15, horizontal: 20),
                        color: purchaseMethodValue == index
                            ? primaryColor
                            : Colors.white,
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                PurchaseType.values[index].title,
                                style: TextStyle(
                                  color: purchaseMethodValue == index
                                      ? Colors.white
                                      : null,
                                ),
                              ),
                            ),
                            if (purchaseMethodValue == index)
                              const Icon(
                                Icons.check,
                                color: Colors.white,
                              ),
                          ],
                        ),
                      ),
                    ),
                    if (index == 1)
                      SizeTransition(
                        sizeFactor: animationController,
                        axisAlignment: 1.0,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      radioValue = 0;
                                    });
                                  },
                                  child: Row(
                                    children: [
                                      Radio<int>(
                                          value: 0,
                                          groupValue: radioValue,
                                          onChanged: (v) {
                                            setState(() {
                                              radioValue = v!;
                                            });
                                          }),
                                      const Text('無分開列印')
                                    ],
                                  ),
                                ),
                                GestureDetector(
                                  onTap: canSeparate
                                      ? () {
                                          setState(() {
                                            radioValue = 1;
                                          });
                                        }
                                      : null,
                                  child: Row(
                                    children: [
                                      Radio<int>(
                                          value: 1,
                                          groupValue: radioValue,
                                          onChanged: canSeparate
                                              ? (v) {
                                                  setState(() {
                                                    radioValue = v!;
                                                  });
                                                }
                                              : null),
                                      Text(
                                        '分開列印',
                                        style: TextStyle(
                                            color: !canSeparate
                                                ? HexColor('#999999')
                                                : null),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const Padding(
                              padding: EdgeInsets.symmetric(horizontal: 16),
                              child: Text(
                                  '※如.「分開列印」限購物車品項一個\n※限制不可以搭配行銷活動、優惠券、課程點數'),
                            )
                          ],
                        ),
                      ),
                  ],
                );
              },
              separatorBuilder: (BuildContext context, int index) {
                return const SizedBox(height: 15);
              },
            ),
          )),
          ElevatedButton(
            onPressed: () async {
              if (purchaseMethodTitles[purchaseMethodValue] !=
                  cartModel.purchaseMethod) {
                cartModel.setShippingMethod(null);
                cartModel.invoiceOffice = null;
                cartModel.invoiceDeliverType = null;
                cartModel.carrierNo = null;
                Provider.of<CartModel>(context, listen: false).setAddress(
                  Address(
                    firstName: '',
                    lastName: '',
                    email: '',
                    street: '',
                    apartment: '',
                    block: '',
                    city: '',
                    state: '',
                    country: 'TW',
                    phoneNumber: '',
                    zipCode: '',
                    mapUrl: '',
                  ),
                );
              }
              cartModel.purchaseMethod = purchaseMethodTitles[purchaseMethodValue];
              if (purchaseMethodValue == 1) {
                cartModel.invoiceType = InvoiceType.values[radioValue];
              } else {
                cartModel.invoiceType = null;
              }
              widget.setStateClick();
              Navigator.of(context).pop();
            },
            style: ButtonStyle(
                padding: MaterialStateProperty.all<EdgeInsets>(const EdgeInsets.all(16)),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  const RoundedRectangleBorder(borderRadius: BorderRadius.zero),
                ),
                backgroundColor: MaterialStateProperty.all(primaryColor)),
            child: Container(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
              width: double.maxFinite,
              child: const Text(
                '確認',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
