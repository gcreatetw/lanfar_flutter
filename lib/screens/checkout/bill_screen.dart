import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/theme/colors.dart';
import '../../from_lanfar/contants/constants.dart';
import '../../from_lanfar/model/barcode_data.dart';
import '../../from_lanfar/screens/qrcode_page.dart';
import '../../from_lanfar/utils/utils.dart';
import '../../models/cart/cart_model.dart';
import '../../models/user_model.dart';
import 'purchase_method_screen.dart';

//TODO 移除載具
enum InvoiceDeliverType {
  included,
  // carrier,
  businessOffice,
}

extension InvoiceTypeNo on InvoiceDeliverType {
  //TODO 移除載具
  String get num {
    switch (this) {
      case InvoiceDeliverType.included:
        return 'included';
      // case InvoiceDeliverType.carrier:
      //   return 'carrier';
      case InvoiceDeliverType.businessOffice:
        return 'business_office';
    }
  }
}

extension InvoiceTypeTitle on InvoiceDeliverType {
  //TODO 移除載具
  String get title {
    switch (this) {
      case InvoiceDeliverType.included:
        return '隨貨附送';
      // case InvoiceDeliverType.carrier:
      //   return '載具';
      case InvoiceDeliverType.businessOffice:
        return '營業處自取';
    }
  }
}

enum OfficeType {
  hq,
  ty,
  tc,
  tn,
  kc,
}

extension OfficeTypeNo on OfficeType {
  String get num {
    switch (this) {
      case OfficeType.hq:
        return 'hq';
      case OfficeType.ty:
        return 'tp';
      case OfficeType.tc:
        return 'tc';
      case OfficeType.tn:
        return 'tn';
      case OfficeType.kc:
        return 'kc';
    }
  }
}

extension OfficeTypeTitle on OfficeType {
  String get title {
    switch (this) {
      case OfficeType.hq:
        return '總公司';
      case OfficeType.ty:
        return '桃園區營業處';
      case OfficeType.tc:
        return '台中區營業處';
      case OfficeType.tn:
        return '台南區營業處';
      case OfficeType.kc:
        return '高雄區營業處';
    }
  }

  OfficeType? type(String? name) {
    switch (name) {
      case 'hq':
        return OfficeType.hq;
      case 'ty':
        return OfficeType.ty;
      case 'tp':
        return OfficeType.ty;
      case 'tc':
        return OfficeType.tc;
      case 'tn':
        return OfficeType.tn;
      case 'kc':
        return OfficeType.kc;
      default:
        return null;
    }
  }
}

class BillScreen extends StatefulWidget {
  final Function setBill;

  const BillScreen({
    Key? key,
    required this.setBill,
  }) : super(key: key);

  @override
  _BillScreenState createState() => _BillScreenState();
}

class _BillScreenState extends State<BillScreen> with TickerProviderStateMixin {
  int billTypeValue = 0;
  int? businessIndex;

  TextEditingController uniformNumber = TextEditingController();
  TextEditingController phoneCode = TextEditingController();

  int printValue = 0;
  int billMethodValue = 0;

  bool correct = false;

  bool canOfficeChoose = false;

  bool canChooseInclude = false;
  bool canChooseCarrier = false;
  bool canChooseBusiness = false;

  List<AnimationController> folderExpandController = [];

  List<Animation<double>> animation = [];

  List<bool> expand = [];

  @override
  void dispose() {
    uniformNumber.dispose();
    phoneCode.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    final cartModel = Provider.of<CartModel>(context, listen: false);
    final userModel = Provider.of<UserModel>(context, listen: false);
    Future.microtask(() async {
      final barcodeData = Provider.of<BarcodeData>(context, listen: false);
      barcodeData.read();
      phoneCode.text = barcodeData.code;
      setState(() {
        correct = checkInputError(phoneCode.text);
      });
      for (var i = 0; i < InvoiceDeliverType.values.length; i++) {
        folderExpandController.add(
          AnimationController(
            vsync: this,
            duration: const Duration(milliseconds: 300),
          ),
        );
        animation.add(
          CurvedAnimation(
            parent: folderExpandController[i],
            curve: Curves.fastOutSlowIn,
          ),
        );
        expand.add(false);
      }

      if (cartModel.invoiceDeliverType != null) {
        billTypeValue = InvoiceDeliverType.values
            .indexWhere((element) => element == cartModel.invoiceDeliverType);
      }
      if (cartModel.user?.officeType != null) {
        businessIndex = OfficeType.values
            .indexWhere((element) => element == cartModel.invoiceOffice);
        if (businessIndex == -1 && cartModel.user!.officeType.length == 1) {
          businessIndex = OfficeType.values.indexWhere(
              (element) => element == cartModel.user!.officeType.first);
        }
        businessIndex = businessIndex == -1 ? null : businessIndex;
      }
      if (cartModel.shippingMethod!.id != null &&
          cartModel.shippingMethod!.id!.startsWith('betrs_shipping:3')) {
        canOfficeChoose = true;
      }
      print(cartModel.invoiceType);
      print(cartModel.shippingMethod?.id);
      if (userModel.user!.freeTax == 1) {
        if (cartModel.purchaseMethod == PurchaseType.general) {
          if (cartModel.shippingMethod!.id != null &&
              cartModel.shippingMethod!.id!.startsWith('betrs_shipping:3')) {
            canChooseInclude = true;
            canChooseCarrier = true;
            canChooseBusiness = false;
          } else {
            canChooseInclude = false;
            canChooseCarrier = true;
            canChooseBusiness = true;
          }
        } else {
          if (cartModel.shippingMethod!.id != null &&
              cartModel.shippingMethod!.id!.startsWith('betrs_shipping:3')) {
            canChooseInclude = true;
            canChooseCarrier = false;
            canChooseBusiness = true;
          } else {
            canChooseInclude = false;
            canChooseCarrier = false;
            canChooseBusiness = true;
          }
        }
      } else {
        if (cartModel.purchaseMethod == PurchaseType.general) {
          if (cartModel.shippingMethod!.id != null &&
              cartModel.shippingMethod!.id!.startsWith('betrs_shipping:3')) {
            canChooseInclude = true;
            canChooseCarrier = false;
            canChooseBusiness = true;
          } else {
            canChooseInclude = false;
            canChooseCarrier = false;
            canChooseBusiness = true;
          }
        } else {
          if (cartModel.shippingMethod!.id != null &&
              cartModel.shippingMethod!.id!.startsWith('betrs_shipping:3')) {
            canChooseInclude = true;
            canChooseCarrier = false;
            canChooseBusiness = true;
          } else {
            canChooseInclude = false;
            canChooseCarrier = false;
            canChooseBusiness = true;
          }
        }
      }
      setState(() {});
      await folderExpandController[billTypeValue].forward();
    });
  }

  @override
  Widget build(BuildContext context) {
    final barcodeData = Provider.of<BarcodeData>(context);
    final cartModel = Provider.of<CartModel>(context);
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text(
            '發票寄送',
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        body: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    ListView.separated(
                      padding: const EdgeInsets.symmetric(vertical: 15),
                      shrinkWrap: true,
                      itemCount: InvoiceDeliverType.values.length,
                      itemBuilder: (context, index) {
                        if (!canChooseInclude &&
                            InvoiceDeliverType.values[index] ==
                                InvoiceDeliverType.included) {
                          return const SizedBox();
                        }
                        //TODO 移除載具
                        // if (!canChooseCarrier && InvoiceDeliverType.values[index] == InvoiceDeliverType.carrier) {
                        //   return const SizedBox();
                        // }
                        if (!canChooseBusiness &&
                            InvoiceDeliverType.values[index] ==
                                InvoiceDeliverType.businessOffice) {
                          return const SizedBox();
                        }

                        return GestureDetector(
                          onTap: () {
                            setState(() {
                              billTypeValue = index;
                            });
                            for (var i = 0;
                                i < InvoiceDeliverType.values.length;
                                i++) {
                              if (i == index) {
                                folderExpandController[i].forward();
                              } else {
                                folderExpandController[i].reverse();
                              }
                            }
                          },
                          child: Column(
                            children: [
                              Container(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 15, horizontal: 20),
                                color: billTypeValue == index
                                    ? primaryColor
                                    : Colors.white,
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        InvoiceDeliverType.values[index].title,
                                        style: TextStyle(
                                          color: billTypeValue == index
                                              ? Colors.white
                                              : null,
                                        ),
                                      ),
                                    ),
                                    if (billTypeValue == index)
                                      const Icon(
                                        Icons.check,
                                        color: Colors.white,
                                      ),
                                  ],
                                ),
                              ),
                              if (animation.isNotEmpty)
                                SizeTransition(
                                  sizeFactor: animation[index],
                                  axisAlignment: 1.0,
                                  child: Builder(
                                    builder: (context) {
                                      if (InvoiceDeliverType.values[index] ==
                                          InvoiceDeliverType.included) {
                                        return const SizedBox();
                                      }
                                      //TODO 移除載具
                                      // if (InvoiceDeliverType.values[index] == InvoiceDeliverType.carrier) {
                                      //   return Row(
                                      //     children: [
                                      //       Expanded(
                                      //         child: textField(
                                      //             controller: phoneCode,
                                      //             hint: '請輸入載具號碼',
                                      //             checkError: checkInputError),
                                      //       ),
                                      //       GestureDetector(
                                      //         onTap: () {
                                      //           if (correct) {
                                      //             barcodeData
                                      //                 .setCode(phoneCode.text);
                                      //           }
                                      //           ScaffoldMessenger.of(context)
                                      //               .showSnackBar(
                                      //             SnackBar(
                                      //               content:
                                      //                   const Text('載具條碼已儲存'),
                                      //               action: SnackBarAction(
                                      //                 label:
                                      //                     S.of(context).close,
                                      //                 onPressed: () {
                                      //                   // Some code to undo the change.
                                      //                 },
                                      //                 textColor: Colors.white,
                                      //               ),
                                      //             ),
                                      //           );
                                      //         },
                                      //         child: Container(
                                      //           padding:
                                      //               const EdgeInsets.symmetric(
                                      //                   horizontal: 16,
                                      //                   vertical: 8),
                                      //           decoration: BoxDecoration(
                                      //               color: primaryColor,
                                      //               borderRadius:
                                      //                   BorderRadius.circular(
                                      //                       5)),
                                      //           child: const Text(
                                      //             '儲存',
                                      //             style: TextStyle(
                                      //                 color: Colors.white),
                                      //           ),
                                      //         ),
                                      //       ),
                                      //       const SizedBox(width: 16),
                                      //     ],
                                      //   );
                                      // }

                                      print(canOfficeChoose);

                                      ///不能選擇營業處
                                      return Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 16, vertical: 8),
                                        child: Container(
                                          color: Colors.white,
                                          width: double.maxFinite,
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 16),
                                          child: DropdownButton<int>(
                                            value: businessIndex,
                                            dropdownColor: Colors.white,
                                            // icon: const SizedBox(),
                                            icon: const Icon(
                                              Icons.keyboard_arrow_down,
                                              color: primaryColor,
                                            ),
                                            isExpanded: true,
                                            underline: const SizedBox(),
                                            hint: const Text('請選擇營業處'),
                                            onChanged: canOfficeChoose
                                                ? (v) {
                                                    setState(() {
                                                      businessIndex = v;
                                                    });
                                                  }
                                                : null,
                                            items: [
                                              for (int i = 0;
                                                  i <
                                                      cartModel.user!.officeType
                                                          .length;
                                                  i++)
                                                DropdownMenuItem(
                                                  value: cartModel.user!
                                                      .officeType[i]!.index,
                                                  child: Text(
                                                    cartModel.user!
                                                        .officeType[i]!.title,
                                                  ),
                                                ),
                                            ],
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                ),
                            ],
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        if (!canChooseInclude &&
                            InvoiceDeliverType.values[index] ==
                                InvoiceDeliverType.included) {
                          return const SizedBox();
                        }
                        //TODO 移除載具
                        // if (!canChooseCarrier && InvoiceDeliverType.values[index] == InvoiceDeliverType.carrier) {
                        //   return const SizedBox();
                        // }
                        if (!canChooseBusiness &&
                            InvoiceDeliverType.values[index] ==
                                InvoiceDeliverType.businessOffice) {
                          return const SizedBox();
                        }
                        return const SizedBox(height: 15);
                      },
                    ),
                    // textField(controller: uniformNumber, hint: '統一編號'),
                    // Row(
                    //   children: [
                    //     for (int i = 0;
                    //         i < Constants.billPrintMethod.length;
                    //         i++)
                    //       Row(
                    //         children: [
                    //           Radio<int>(
                    //               value: i,
                    //               groupValue: printValue,
                    //               onChanged: (value) {
                    //                 setState(() {
                    //                   printValue = value!;
                    //                 });
                    //               }),
                    //           Text(
                    //             Constants.billPrintMethod[i],
                    //             style: const TextStyle(fontSize: 14),
                    //           )
                    //         ],
                    //       )
                    //   ],
                    // ),
                    // Row(
                    //   children: [
                    //     for (int i = 0;
                    //         i < Constants.billSendingMethods.length;
                    //         i++)
                    //       Row(
                    //         children: [
                    //           Radio<int>(
                    //               value: i,
                    //               groupValue: billMethodValue,
                    //               onChanged: (value) {
                    //                 setState(() {
                    //                   billMethodValue = value!;
                    //                 });
                    //               }),
                    //           Text(
                    //             Constants.billSendingMethods[i],
                    //             style: const TextStyle(fontSize: 14),
                    //           )
                    //         ],
                    //       )
                    //   ],
                    // ),
                    // if (billMethodValue == 0)
                    //   textField(
                    //       controller: phoneCode,
                    //       hint: '手機條碼',
                    //       checkError: checkInputError),
                  ],
                ),
              ),
            ),
            ElevatedButton(
              onPressed: () async {
                //TODO 移除載具
                // if (InvoiceDeliverType.values[billTypeValue] ==InvoiceDeliverType.carrier && !correct) {
                //   ScaffoldMessenger.of(context).showSnackBar(
                //     SnackBar(
                //       content: const Text('請輸入載具條碼'),
                //       action: SnackBarAction(
                //         label: S.of(context).close,
                //         onPressed: () {
                //           // Some code to undo the change.
                //         },
                //         textColor: Colors.white,
                //       ),
                //     ),
                //   );
                //   return;
                // }
                if (InvoiceDeliverType.values[billTypeValue] ==
                        InvoiceDeliverType.businessOffice &&
                    businessIndex == null) {
                  Utils.showSnackBar(
                    context,
                    text: '請選擇營業所',
                  );
                  return;
                }
                cartModel.invoiceDeliverType =
                    InvoiceDeliverType.values[billTypeValue];
                //TODO 移除載具
                // if (InvoiceDeliverType.values[billTypeValue] ==InvoiceDeliverType.carrier) {
                //   cartModel.carrierNo = phoneCode.text;
                // } else {
                //   cartModel.carrierNo = null;
                // }

                if (InvoiceDeliverType.values[billTypeValue] ==
                    InvoiceDeliverType.businessOffice) {
                  cartModel.invoiceOffice = OfficeType.values[businessIndex!];
                } else {
                  // cartModel.invoiceOffice = null;
                }

                widget.setBill(
                  BillModel(
                    billType: InvoiceDeliverType.values[billTypeValue].title,
                    uniformNumbers: uniformNumber.text,
                    printMethod: Constants.billPrintMethod[printValue],
                    billMethod: InvoiceDeliverType.values[billTypeValue].title,
                    phoneCode: phoneCode.text,
                  ),
                );
                Navigator.of(context).pop();
              },
              style: ButtonStyle(
                  padding: MaterialStateProperty.all<EdgeInsets>(
                      const EdgeInsets.all(16)),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    const RoundedRectangleBorder(
                        borderRadius: BorderRadius.zero),
                  ),
                  backgroundColor: MaterialStateProperty.all(primaryColor)),
              child: Container(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).padding.bottom),
                width: double.maxFinite,
                child: const Text(
                  '確認',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget textField({
    required TextEditingController controller,
    FocusNode? node,
    TextInputType? textInputType,
    FocusNode? nextNode,
    required String hint,
    bool end = false,
    bool enable = true,
    Function? checkError,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: TextFormField(
        enabled: enable,
        controller: controller,
        focusNode: node,
        textInputAction: end ? TextInputAction.done : TextInputAction.next,
        keyboardType: textInputType,
        onFieldSubmitted: (_) =>
            end ? null : FocusScope.of(context).requestFocus(nextNode),
        validator: (val) {
          return val!.isEmpty ? '*此為必填欄位' : null;
        },
        style: const TextStyle(fontSize: 14),
        onChanged: checkError != null
            ? (text) {
                setState(() {
                  correct = checkInputError(text);
                });
              }
            : null,
        maxLength: checkError != null ? 8 : null,
        textCapitalization: checkError != null
            ? TextCapitalization.characters
            : TextCapitalization.none,
        inputFormatters: checkError != null
            ? [
                UpperCaseTextFormatter(),
              ]
            : null,
        decoration: InputDecoration(
          counterText: '',
          errorText: checkError != null
              ? correct
                  ? null
                  : '請輸入正確載具號碼'
              : null,
          contentPadding:
              const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
          floatingLabelBehavior: FloatingLabelBehavior.auto,
          labelText: hint,
        ),
      ),
    );
  }

  bool checkInputError(String value) {
    if (value.length < 8) {
      return false;
    }
    if (value[0] != '/') {
      return false;
    }
    return true;
  }
}
