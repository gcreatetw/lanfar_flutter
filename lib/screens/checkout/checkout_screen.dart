import 'dart:async';

import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools/tools.dart';
import '../../env.dart';
import '../../from_lanfar/contants/constants.dart';
import '../../from_lanfar/model/default_checkout_response.dart';
import '../../from_lanfar/utils/utils.dart';
import '../../generated/l10n.dart';
import '../../menu/maintab_delegate.dart';
import '../../models/booking/booking_model.dart';
import '../../models/cart/mixin/cart_mixin.dart';
import '../../models/index.dart'
    show
        Address,
        CartModel,
        Order,
        PaymentMethodModel,
        ShippingMethodModel,
        UserModel;
import '../../models/tera_wallet/wallet_model.dart';
import '../../services/index.dart';
import '../../widgets/product/product_bottom_sheet.dart';
import '../base_screen.dart';
import '../order_history/views/new_order_history_datail_screen.dart';
import 'bill_screen.dart';
import 'payment_webview_screen.dart';
import 'purchase_method_screen.dart';
import 'review_screen.dart';
import 'widgets/custom_checkout_main.dart';
import 'widgets/payment_methods.dart';
import 'widgets/success.dart';

class CheckoutArgument {
  final bool? isModal;
  final Function onRefresh;

  const CheckoutArgument({
    this.isModal,
    required this.onRefresh,
  });
}

class Checkout extends StatefulWidget {
  final bool? isModal;
  final Function onRefresh;

  const Checkout({this.isModal, required this.onRefresh});

  @override
  _CheckoutState createState() => _CheckoutState();
}

class _CheckoutState extends BaseScreen<Checkout> {
  int tabIndex = 0;
  Order? newOrder;
  bool isPayment = false;
  bool isLoading = false;
  bool enabledShipping = kPaymentConfig['EnableShipping'];
  bool isPaying = false;

  TextEditingController noteController = TextEditingController();

  int paymentDropdownValue = 1;

  List<Address?> listAddress = [];

  BillModel billModel = BillModel(
    billType: InvoiceDeliverType.values[0].title,
    uniformNumbers: '',
    printMethod: Constants.billPrintMethod[0],
    billMethod: InvoiceDeliverType.values[0].title,
    phoneCode: '',
  );

  // Future<void> getDatafromLocal() async {
  //   final storage = LocalStorage('address');
  //   var _list = <Address?>[];
  //   try {
  //     final ready = await storage.ready;
  //     if (ready) {
  //       var data = storage.getItem('data');
  //       if (data != null) {
  //         for (var item in data as List) {
  //           final add = Address.fromLocalJson(item);
  //           _list.add(add);
  //         }
  //       }
  //     }
  //     setState(() {
  //       listAddress = _list;
  //     });
  //   } catch (_) {}
  // }

  DefaultCheckoutResponse defaultCheckoutResponse = DefaultCheckoutResponse();

  Future<void> getDefaultCheckout() async {
    final model = context.read<CartModel>();
    defaultCheckoutResponse = await Services().api.defaultCheckout(
              userId: model.user?.username ?? '',
              productId: model.item[model.productsInCart.keys.first]?.id ?? '',
            ) ??
        DefaultCheckoutResponse();
  }

  Future<void> getDatafromLocal() async {
    final response = await Services()
        .api
        .getAddress(userId: context.read<UserModel>().user?.username ?? '');

    if (response?.error == true) {
    } else {
      var _list = <Address?>[];
      if (response?.data != null) {
        for (var element in response!.data!) {
          final add = Address.fromApi(element);
          if (add.defaultAddress == true) {
            _list.insert(0, add);
          } else {
            _list.add(add);
          }
        }
      }
      setState(() {
        listAddress = _list;
      });
    }
  }

  void checkInvoice(CartModel cartModel, String defaultValue) {
    if (defaultValue.isEmpty) return;
    var canOfficeChoose = false;
    var canChooseInclude = false;
    var canChooseCarrier = false;
    var canChooseBusiness = false;
    if (cartModel.shippingMethod!.id == 'betrs_shipping:3-2' ||
        cartModel.shippingMethod!.id == 'betrs_shipping:3-1') {
      canOfficeChoose = true;
    }
    print(cartModel.invoiceType);
    if (cartModel.user!.freeTax == 1) {
      if (cartModel.purchaseMethod == PurchaseType.general) {
        if (cartModel.shippingMethod?.id == 'betrs_shipping:3-2' ||
            cartModel.shippingMethod!.id == 'betrs_shipping:3-1') {
          canChooseInclude = true;
          canChooseCarrier = true;
          canChooseBusiness = false;
        } else {
          canChooseInclude = false;
          canChooseCarrier = true;
          canChooseBusiness = true;
        }
      } else {
        if (cartModel.shippingMethod?.id == 'betrs_shipping:3-2' ||
            cartModel.shippingMethod!.id == 'betrs_shipping:3-1') {
          canChooseInclude = true;
          canChooseCarrier = false;
          canChooseBusiness = true;
        } else {
          canChooseInclude = false;
          canChooseCarrier = false;
          canChooseBusiness = true;
        }
      }
    } else {
      if (cartModel.purchaseMethod == PurchaseType.general) {
        if (cartModel.shippingMethod?.id == 'betrs_shipping:3-2' ||
            cartModel.shippingMethod!.id == 'betrs_shipping:3-1') {
          canChooseInclude = true;
          canChooseCarrier = false;
          canChooseBusiness = true;
        } else {
          canChooseInclude = false;
          canChooseCarrier = false;
          canChooseBusiness = true;
        }
      } else {
        if (cartModel.shippingMethod?.id == 'betrs_shipping:3-2' ||
            cartModel.shippingMethod!.id == 'betrs_shipping:3-1') {
          canChooseInclude = true;
          canChooseCarrier = false;
          canChooseBusiness = true;
        } else {
          canChooseInclude = false;
          canChooseCarrier = false;
          canChooseBusiness = true;
        }
      }
    }
    if (defaultValue == InvoiceDeliverType.included.num && canChooseInclude) {
      cartModel.invoiceDeliverType = InvoiceDeliverType.included;
    }
    //TODO 移除載具
    // if (defaultValue == InvoiceDeliverType.carrier.num && canChooseCarrier) {
    //   final barcodeData = Provider.of<BarcodeData>(context, listen: false);
    //   barcodeData.read();
    //   if (barcodeData.code.isNotEmpty) {
    //     cartModel.invoiceDeliverType = InvoiceDeliverType.carrier;
    //     cartModel.carrierNo = barcodeData.code;
    //   }
    // }
    if (defaultValue == InvoiceDeliverType.businessOffice.num &&
        canChooseBusiness) {
      cartModel.invoiceDeliverType = InvoiceDeliverType.businessOffice;
      cartModel.invoiceOffice = cartModel.user!.officeType.isEmpty ? null:cartModel.user?.officeType.first;
    }
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () async {
      setState(() {
        isLoading = true;
      });
      final cartModel = Provider.of<CartModel>(context, listen: false);
      final shippingMethod =
          Provider.of<ShippingMethodModel>(context, listen: false);
      final paymentMethodModel =
          Provider.of<PaymentMethodModel>(context, listen: false);
      final userModel = Provider.of<UserModel>(context, listen: false);

      cartModel.setAddress(Address(country: 'TW'));

      cartModel.setShippingMethod(null);
      cartModel.setPaymentMethod(null);
      cartModel.invoiceDeliverType = null;
      // cartModel.purchaseMethod = null;
      // cartModel.invoiceType = null;
      cartModel.carrierNo = null;
      cartModel.invoiceOffice = null;
      cartModel.purchaseBagType = PurchaseBagType.noNeed;

      ///另外取得運送及付款方式
      await Future.wait([
        shippingMethod.getShippingMethods(
          cartModel: cartModel,
          token: Provider.of<UserModel>(context, listen: false).user != null
              ? Provider.of<UserModel>(context, listen: false).user!.cookie
              : null,
        ),
        paymentMethodModel.getPaymentMethods(
          cartModel: cartModel,
          shippingMethod: cartModel.shippingMethod,
          token: userModel.user != null ? userModel.user!.cookie : null,
        ),
        getDefaultCheckout(),
        getDatafromLocal(),
      ]);

      ///虛擬商品只能信用卡付款
      cartModel.item.forEach((key, value) {
        if (value?.virtual == true) {
          paymentMethodModel.paymentMethods
              .removeWhere((element) => !element.id!.contains('ctcb'));
        }
      });

      ///載入預設結帳資訊
      if (cartModel.ticketMeta == null) {
        try {
          if (shippingMethod.shippingMethods?.indexWhere((element) =>
                  element.id!.contains(
                      defaultCheckoutResponse.shippingMethod ?? 'null')) !=
              -1) {
            cartModel.setShippingMethod(
              shippingMethod.shippingMethods?.firstWhere(
                (element) => element.id!
                    .contains(defaultCheckoutResponse.shippingMethod ?? ''),
              ),
            );
            if (cartModel.shippingMethod!.id!.contains('betrs_shipping:3') &&
                listAddress.isNotEmpty) {
              cartModel.setAddress(listAddress.first);
            }
            if (cartModel.shippingMethod!.id!.contains('betrs_shipping:10')) {
              cartModel.setAddress(Address(
                firstName: '',
                lastName: '',
                email: '',
                street: '',
                apartment: '',
                block: '',
                city: '',
                state: '',
                country: 'TW',
                phoneNumber: '',
                zipCode: '',
                mapUrl: '',
              ),);
              cartModel.invoiceOffice = cartModel.user?.officeType.first;
              cartModel.invoiceDeliverType = InvoiceDeliverType.businessOffice;
            }
          }
          checkInvoice(
              cartModel, defaultCheckoutResponse.invoiceDelivery ?? '');
        } catch (_) {}
      }

      ///結帳頁提示訊息
      cartModel.checkoutNotify = defaultCheckoutResponse.notify;


      ///不再儲存付款資訊
      // if (cartModel.shippingMethod == null) {
      //   await shippingMethod
      //       .getShippingMethods(
      //         cartModel: cartModel,
      //         token: Provider.of<UserModel>(context, listen: false).user != null
      //             ? Provider.of<UserModel>(context, listen: false).user!.cookie
      //             : null,
      //       )
      //       .then((value) => {
      //             if (shippingMethod.shippingMethods != null &&
      //                 shippingMethod.shippingMethods!.isNotEmpty &&
      //                 cartModel.shippingMethod == null)
      //               {
      //                 cartModel
      //                     .setShippingMethod(shippingMethod.shippingMethods![0])
      //               }
      //           });
      // } else {
      //   await shippingMethod.getShippingMethods(
      //     cartModel: cartModel,
      //     token: Provider.of<UserModel>(context, listen: false).user != null
      //         ? Provider.of<UserModel>(context, listen: false).user!.cookie
      //         : null,
      //   );
      //   if (shippingMethod.shippingMethods == null ||
      //       shippingMethod.shippingMethods!.isEmpty) {
      //     cartModel.setShippingMethod(null);
      //   } else {
      //     if (shippingMethod.shippingMethods?.indexWhere(
      //             (element) => element.id == cartModel.shippingMethod!.id) ==
      //         -1) {
      //       cartModel.setShippingMethod(shippingMethod.shippingMethods![0]);
      //     }
      //   }
      // }
      //
      // await getDatafromLocal().then((value) => {
      //       if (listAddress.isNotEmpty) {cartModel.setAddress(listAddress[0])}
      //     });
      // if (cartModel.paymentMethod == null) {
      //   await paymentMethodModel
      //       .getPaymentMethods(
      //         cartModel: cartModel,
      //         shippingMethod: cartModel.shippingMethod,
      //         token: userModel.user != null ? userModel.user!.cookie : null,
      //       )
      //       .then((value) => {
      //             if (paymentMethodModel.paymentMethods.isNotEmpty)
      //               {
      //                 cartModel.setPaymentMethod(
      //                     paymentMethodModel.paymentMethods[0])
      //               }
      //           });
      // } else {
      //   await paymentMethodModel.getPaymentMethods(
      //       cartModel: cartModel,
      //       shippingMethod: cartModel.shippingMethod,
      //       token: userModel.user != null ? userModel.user!.cookie : null);
      //   if (paymentMethodModel.paymentMethods.isEmpty) {
      //     cartModel.setPaymentMethod(null);
      //   } else {
      //     if (paymentMethodModel.paymentMethods.indexWhere(
      //             (element) => element.id == cartModel.paymentMethod!.id) ==
      //         -1) {
      //       cartModel.setPaymentMethod(paymentMethodModel.paymentMethods[0]);
      //     }
      //   }
      // }
      // await readCartInfo();
      setState(() {
        enabledShipping = cartModel.isEnabledShipping();
        isLoading = false;
      });
    });
  }

  void setLoading(bool loading) {
    setState(() {
      isLoading = loading;
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    if (!kPaymentConfig['EnableAddress']) {
      setState(() {
        tabIndex = 1;
      });
      if (!enabledShipping) {
        setState(() {
          tabIndex = 2;
        });
        if (!kPaymentConfig['EnableReview']) {
          setState(() {
            tabIndex = 3;
            isPayment = true;
          });
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    // Widget progressBar = Row(
    //   children: <Widget>[
    //     kPaymentConfig['EnableAddress']
    //         ? Expanded(
    //             child: GestureDetector(
    //               onTap: () {
    //                 setState(() {
    //                   tabIndex = 0;
    //                 });
    //               },
    //               child: Column(
    //                 children: <Widget>[
    //                   Padding(
    //                     padding: const EdgeInsets.symmetric(vertical: 13),
    //                     child: Text(
    //                       S.of(context).address.toUpperCase(),
    //                       style: TextStyle(
    //                           color: tabIndex == 0
    //                               ? Theme.of(context).primaryColor
    //                               : Theme.of(context).colorScheme.secondary,
    //                           fontSize: 12,
    //                           fontWeight: FontWeight.bold),
    //                     ),
    //                   ),
    //                   tabIndex >= 0
    //                       ? ClipRRect(
    //                           borderRadius: const BorderRadius.only(
    //                               topLeft: Radius.circular(2.0),
    //                               bottomLeft: Radius.circular(2.0)),
    //                           child: Container(
    //                               height: 3.0,
    //                               color: Theme.of(context).primaryColor),
    //                         )
    //                       : Divider(
    //                           height: 2,
    //                           color: Theme.of(context).colorScheme.secondary)
    //                 ],
    //               ),
    //             ),
    //           )
    //         : Container(),
    //     enabledShipping
    //         ? Expanded(
    //             child: GestureDetector(
    //               onTap: () {
    //                 // if (cartModel.address != null &&
    //                 //     cartModel.address!.isValid()) {
    //                 //   setState(() {
    //                 //     tabIndex = 1;
    //                 //   });
    //                 // }
    //               },
    //               child: Column(
    //                 children: <Widget>[
    //                   Padding(
    //                     padding: const EdgeInsets.symmetric(vertical: 13),
    //                     child: Text(
    //                       S.of(context).shipping.toUpperCase(),
    //                       style: TextStyle(
    //                           color: tabIndex == 1
    //                               ? Theme.of(context).primaryColor
    //                               : Theme.of(context).colorScheme.secondary,
    //                           fontSize: 12,
    //                           fontWeight: FontWeight.bold),
    //                     ),
    //                   ),
    //                   tabIndex >= 1
    //                       ? Container(
    //                           height: 3.0,
    //                           color: Theme.of(context).primaryColor)
    //                       : Divider(
    //                           height: 2,
    //                           color: Theme.of(context).colorScheme.secondary)
    //                 ],
    //               ),
    //             ),
    //           )
    //         : Container(),
    //     kPaymentConfig['EnableReview']
    //         ? Expanded(
    //             child: GestureDetector(
    //               onTap: () {
    //                 // if (cartModel.shippingMethod != null) {
    //                 //   setState(() {
    //                 //     tabIndex = 2;
    //                 //   });
    //                 // }
    //               },
    //               child: Column(
    //                 children: <Widget>[
    //                   Padding(
    //                     padding: const EdgeInsets.symmetric(vertical: 13),
    //                     child: Text(
    //                       S.of(context).review.toUpperCase(),
    //                       style: TextStyle(
    //                         color: tabIndex == 2
    //                             ? Theme.of(context).primaryColor
    //                             : Theme.of(context).colorScheme.secondary,
    //                         fontSize: 12,
    //                         fontWeight: FontWeight.bold,
    //                       ),
    //                     ),
    //                   ),
    //                   tabIndex >= 2
    //                       ? Container(
    //                           height: 3.0,
    //                           color: Theme.of(context).primaryColor)
    //                       : Divider(
    //                           height: 2,
    //                           color: Theme.of(context).colorScheme.secondary)
    //                 ],
    //               ),
    //             ),
    //           )
    //         : Container(),
    //     Expanded(
    //       child: GestureDetector(
    //         onTap: () {
    //           // if (cartModel.shippingMethod != null) {
    //           //   setState(() {
    //           //     tabIndex = 3;
    //           //   });
    //           // }
    //         },
    //         child: Column(
    //           children: <Widget>[
    //             Padding(
    //               padding: const EdgeInsets.symmetric(vertical: 13),
    //               child: Text(
    //                 S.of(context).payment.toUpperCase(),
    //                 style: TextStyle(
    //                   color: tabIndex == 3
    //                       ? Theme.of(context).primaryColor
    //                       : Theme.of(context).colorScheme.secondary,
    //                   fontSize: 12,
    //                   fontWeight: FontWeight.bold,
    //                 ),
    //               ),
    //             ),
    //             tabIndex >= 3
    //                 ? ClipRRect(
    //                     borderRadius: const BorderRadius.only(
    //                         topRight: Radius.circular(2.0),
    //                         bottomRight: Radius.circular(2.0)),
    //                     child: Container(
    //                         height: 3.0, color: Theme.of(context).primaryColor),
    //                   )
    //                 : Divider(
    //                     height: 2,
    //                     color: Theme.of(context).colorScheme.secondary)
    //           ],
    //         ),
    //       ),
    //     )
    //   ],
    // );

    return WillPopScope(
      onWillPop: () async {
        if (tabIndex != 0) {
          setState(() {
            tabIndex = 0;
          });
          return false;
        }
        final cartModel = Provider.of<CartModel>(context, listen: false);
        if(cartModel.ticketMeta != null){
          MainTabControlDelegate.getInstance()
              .changeTab(RouteList.home.replaceFirst('/', ''));
        }
        return true;
      },
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              onPressed: () {
                if (tabIndex != 0) {
                  setState(() {
                    tabIndex = 0;
                  });
                  return;
                }
                final cartModel = Provider.of<CartModel>(context, listen: false);
                if(cartModel.ticketMeta != null){
                  MainTabControlDelegate.getInstance()
                      .changeTab(RouteList.home.replaceFirst('/', ''));
                }
                Navigator.pop(context);
              },
              icon: const Icon(Icons.arrow_back_outlined),
            ),
            automaticallyImplyLeading: false,
            backgroundColor: Theme.of(context).backgroundColor,
            title: Text(
              S.of(context).checkout,
              style: TextStyle(
                color: Theme.of(context).colorScheme.secondary,
                fontWeight: FontWeight.w400,
              ),
            ),
            actions: <Widget>[
              if (widget.isModal != null && widget.isModal == true)
                IconButton(
                  icon: const Icon(Icons.close, size: 24),
                  onPressed: () {
                    if (Navigator.of(context).canPop()) {
                      Navigator.popUntil(
                          context, (Route<dynamic> route) => route.isFirst);
                    } else {
                      ExpandingBottomSheet.of(context, isNullOk: true)?.close();
                    }
                  },
                ),
            ],
          ),
          body: Stack(
            children: <Widget>[
              Builder(
                builder: (context) => SafeArea(
                  bottom: false,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 0),
                          child: newOrder != null
                              ? OrderedSuccess(order: newOrder)
                              : Column(
                                  children: <Widget>[
                                    // !isPayment ? progressBar : Container(),
                                    Expanded(
                                      child: ListView(
                                        key:
                                            const Key('checkOutScreenListView'),
                                        padding: const EdgeInsets.only(
                                            top: 10, bottom: 10),
                                        children: <Widget>[renderContent()],
                                      ),
                                    ),
                                    if (tabIndex == 0)
                                      ElevatedButton(
                                        onPressed: () async {
                                          placeOrder(
                                            context,
                                            Provider.of<CartModel>(context,
                                                listen: false),
                                          );
                                        },
                                        style: ButtonStyle(
                                            padding: MaterialStateProperty.all<
                                                    EdgeInsets>(
                                                const EdgeInsets.all(16)),
                                            shape: MaterialStateProperty.all<
                                                RoundedRectangleBorder>(
                                              const RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.zero),
                                            ),
                                            backgroundColor:
                                                MaterialStateProperty.all(
                                                    primaryColor)),
                                        child: Container(
                                          padding: EdgeInsets.only(
                                              bottom: MediaQuery.of(context)
                                                  .padding
                                                  .bottom),
                                          width: double.maxFinite,
                                          child: Text(
                                            S.of(context).checkout,
                                            textAlign: TextAlign.center,
                                            style: const TextStyle(
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                  ],
                                ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              isLoading
                  ? Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.white.withOpacity(0.36),
                      child: kLoadingWidget(context),
                    )
                  : Container()
            ],
          ),
        ),
      ),
    );
  }

  Widget renderContent() {
    switch (tabIndex) {
      case 0:
        return CustomCheckoutMain(
          paymentDropdownValue: paymentDropdownValue,
          billModel: billModel,
          noteController: noteController,
          toShipping: () {
            goToShippingTab(false);
          },
          toReceipt: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => BillScreen(
                  setBill: (BillModel bill) {
                    setState(() {
                      billModel = bill;
                    });
                  },
                ),
              ),
            );
          },
          toPayment: () {
            goToPaymentTab(false);
          },
          setStateClick: () {
            setState(() {});
          },
          paymentOnBack: (value) {
            setState(() {
              // tabIndex = 0;
              paymentDropdownValue = value;
            });
          },
        );
      // return ShippingAddress(onNext: () {
      //   Future.delayed(Duration.zero, goToShippingTab);
      // });
      case 1:
        return Services().widget.renderShippingMethods(context, onBack: () {
          goToAddressTab(true);
        }, onNext: () {
          goToAddressTab(true);
          // goToReviewTab();
        });
      case 2:
        return ReviewScreen(onBack: () {
          goToShippingTab(true);
        }, onNext: () {
          goToPaymentTab();
        });
      case 3:
      default:
        return PaymentMethods(
            onBack: (value) {
              setState(() {
                tabIndex = 0;
                paymentDropdownValue = value;
              });
            },
            onFinish: (order) async {
              setState(() {
                newOrder = order;
              });
              Provider.of<CartModel>(context, listen: false).clearCart();
              unawaited(context.read<WalletModel>().refreshWallet());
              await Services().widget.updateOrderAfterCheckout(context, order);
            },
            onLoading: setLoading);
    }
  }

  void onFinish(Order order) async {
    Provider.of<CartModel>(context, listen: false).clearCart();
    setState(() {
      newOrder = order;
    });
    Navigator.pop(context);
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => NewOrderHistoryDetailScreen(
          orderId: order.number ?? '',
          onRefresh: widget.onRefresh,
        ),
      ),
    );

    //
    // unawaited(context.read<WalletModel>().refreshWallet());
    // await Services().widget.updateOrderAfterCheckout(context, order);
  }

  /// tabIndex: 0
  void goToAddressTab([bool isGoingBack = false]) {
    if (kPaymentConfig['EnableAddress']) {
      setState(() {
        tabIndex = 0;
      });
    } else {
      if (!isGoingBack) {
        goToShippingTab(isGoingBack);
      }
    }
  }

  /// tabIndex: 1
  void goToShippingTab([bool isGoingBack = false]) {
    if (enabledShipping) {
      setState(() {
        tabIndex = 1;
      });
    } else {
      if (isGoingBack) {
        goToAddressTab(isGoingBack);
      } else {
        goToReviewTab(isGoingBack);
      }
    }
  }

  /// tabIndex: 2
  void goToReviewTab([bool isGoingBack = false]) {
    if (kPaymentConfig['EnableReview'] ?? true) {
      setState(() {
        tabIndex = 2;
      });
    } else {
      if (isGoingBack) {
        goToShippingTab(isGoingBack);
      } else {
        goToPaymentTab(isGoingBack);
      }
    }
  }

  /// tabIndex: 3
  void goToPaymentTab([bool isGoingBack = false]) {
    if (!isGoingBack) {
      setState(() {
        tabIndex = 3;
      });
    }
  }

  ///place order
  void placeOrder(BuildContext context, CartModel cartModel) {
    if (cartModel.paymentMethod != null &&
        (cartModel.ticketMeta != null ||
            (cartModel.shippingMethod != null &&
                cartModel.purchaseMethod != null &&
                cartModel.invoiceDeliverType != null))) {
      setLoading(true);
      isPaying = true;
      final paymentMethod = cartModel.paymentMethod!;
      var isSubscriptionProduct = cartModel.item.values.firstWhere(
              (element) =>
                  element?.type == 'variable-subscription' ||
                  element?.type == 'subscription',
              orElse: () => null) !=
          null;
      // Provider.of<CartModel>(context, listen: false)
      //     .setPaymentMethod(paymentMethod);

      if (cartModel.purchaseBagType == null) {
        Utils.showSnackBar(
          context,
          text: '請選擇DM',
          close: true,
        );
        setLoading(false);
        isPaying = false;
        return;
      }
      //TODO check if need address
      if (cartModel.ticketMeta == null) {
        if (cartModel.address == null || cartModel.address!.street == null) {
          Utils.showSnackBar(
            context,
            text: '請選擇寄送地址',
            close: true,
          );
          setLoading(false);
          isPaying = false;
          return;
        }
      }

      if (billModel.billType == '載具' && billModel.phoneCode!.isEmpty) {
        Utils.showSnackBar(
          context,
          text: '請輸入載具條碼',
          close: true,
        );
        setLoading(false);
        isPaying = false;
        return;
      }

      if (cartModel.invoiceDeliverType == InvoiceDeliverType.businessOffice &&
          cartModel.invoiceOffice == null) {
        Utils.showSnackBar(
          context,
          text: '請選擇營業所',
          close: true,
        );
        setLoading(false);
        isPaying = false;
      }

      storeCartInfo();

      /// Use Native payment

      /// Direct bank transfer (BACS)
      if (!isSubscriptionProduct && paymentMethod.id!.contains('bacs')) {
        setLoading(false);
        isPaying = false;

        showModalBottomSheet(
            context: context,
            builder: (sContext) => Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          GestureDetector(
                            onTap: () => Navigator.of(context).pop(),
                            child: Text(
                              S.of(context).cancel,
                              style: Theme.of(context)
                                  .textTheme
                                  .caption!
                                  .copyWith(color: Colors.red),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 10),
                      Text(
                        paymentMethod.description!,
                        style: Theme.of(context).textTheme.caption,
                      ),
                      const Expanded(child: SizedBox(height: 10)),
                      ElevatedButton(
                        onPressed: () {
                          Navigator.pop(context);
                          setLoading(true);
                          isPaying = true;
                          Services().widget.placeOrder(
                            context,
                            cartModel: cartModel,
                            onLoading: setLoading,
                            paymentMethod: paymentMethod,
                            success: (Order order) async {
                              for (var item in order.lineItems) {
                                var product =
                                    cartModel.getProductById(item.productId!);
                                if (product?.bookingInfo != null) {
                                  product!.bookingInfo!.idOrder = order.id;
                                  var booking =
                                      await createBooking(product.bookingInfo)!;

                                  Tools.showSnackBar(
                                      ScaffoldMessenger.of(context),
                                      booking
                                          ? 'Booking success!'
                                          : 'Booking error!');
                                }
                              }
                              onFinish(order);
                              setLoading(false);
                              isPaying = false;
                            },
                            error: (message) {
                              setLoading(false);
                              if (message != null) {
                                Tools.showSnackBar(
                                    ScaffoldMessenger.of(context), message);
                              }
                              isPaying = false;
                            },
                          );
                        },
                        style: ElevatedButton.styleFrom(
                          onPrimary: Colors.white,
                          primary: Theme.of(context).primaryColor,
                        ),
                        child: Text(
                          S.of(context).ok,
                        ),
                      ),
                      const SizedBox(height: 10),
                    ],
                  ),
                ));

        return;
      }

      /// PayPal Payment
      // if (!isSubscriptionProduct &&
      //     isNotBlank(kPaypalConfig['paymentMethodId']) &&
      //     paymentMethod.id!.contains(kPaypalConfig['paymentMethodId']) &&
      //     kPaypalConfig['enabled'] == true) {
      //   Navigator.push(
      //     context,
      //     MaterialPageRoute(
      //       builder: (context) => PaypalPayment(
      //         onFinish: (number) {
      //           if (number == null) {
      //             setLoading(false);
      //             isPaying = false;
      //             return;
      //           } else {
      //             createOrder(paid: true).then((value) {
      //               setLoading(false);
      //               isPaying = false;
      //             });
      //           }
      //         },
      //       ),
      //     ),
      //   );
      //   return;
      // }

      /// MercadoPago payment
      // if (!isSubscriptionProduct &&
      //     isNotBlank(kMercadoPagoConfig['paymentMethodId']) &&
      //     paymentMethod.id!.contains(kMercadoPagoConfig['paymentMethodId']) &&
      //     kMercadoPagoConfig['enabled'] == true) {
      //   Navigator.push(
      //     context,
      //     MaterialPageRoute(
      //       builder: (context) => MercadoPagoPayment(
      //         onFinish: (number) {
      //           if (number == null) {
      //             setLoading(false);
      //             isPaying = false;
      //             return;
      //           } else {
      //             createOrder(paid: true).then((value) {
      //               setLoading(false);
      //               isPaying = false;
      //             });
      //           }
      //         },
      //       ),
      //     ),
      //   );
      //   return;
      // }

      /// RazorPay payment
      /// Check below link for parameters:
      /// https://razorpay.com/docs/payment-gateway/web-integration/standard/#step-2-pass-order-id-and-other-options
      // if (!isSubscriptionProduct &&
      //     paymentMethod.id!.contains(kRazorpayConfig['paymentMethodId']) &&
      //     kRazorpayConfig['enabled'] == true) {
      //   Services().api.createRazorpayOrder({
      //     'amount': (cartModel.getTotal()! * 100).toInt().toString(),
      //     'currency': cartModel.currency,
      //   }).then((value) {
      //     final _razorServices = RazorServices(
      //       amount: (cartModel.getTotal()! * 100).toInt().toString(),
      //       keyId: kRazorpayConfig['keyId'],
      //       delegate: this,
      //       orderId: value,
      //       userInfo: RazorUserInfo(
      //         email: cartModel.address!.email,
      //         phone: cartModel.address!.phoneNumber,
      //         fullName:
      //         '${cartModel.address!.firstName} ${cartModel.address!.lastName}',
      //       ),
      //     );
      //     _razorServices.openPayment(cartModel.currency!);
      //   }).catchError((e) {
      //     setLoading(false);
      //     Tools.showSnackBar(Scaffold.of(context), e);
      //     isPaying = false;
      //   });
      //   return;
      // }

      /// PayTm payment.
      /// Check below link for parameters:
      /// https://developer.paytm.com/docs/all-in-one-sdk/hybrid-apps/flutter/
      // final availablePayTm = kPayTmConfig['paymentMethodId'] != null &&
      //     (kPayTmConfig['enabled'] ?? false) &&
      //     paymentMethod.id!.contains(kPayTmConfig['paymentMethodId']);
      // if (!isSubscriptionProduct && availablePayTm) {
      //   createOrderOnWebsite(
      //       paid: false,
      //       onFinish: (Order? order) async {
      //         if (order != null) {
      //           final _paytmServices = PayTmServices(
      //             amount: cartModel.getTotal()!.toString(),
      //             orderId: order.id!,
      //             email: cartModel.address!.email,
      //           );
      //           try {
      //             await _paytmServices.openPayment();
      //             widget.onFinish!(order);
      //           } catch (e) {
      //             Tools.showSnackBar(Scaffold.of(context), e.toString());
      //             isPaying = false;
      //           }
      //         }
      //       });
      //   return;
      // }

      //TODO credit card payment
      if (paymentMethod.id == environment['creditId'] ||
          paymentMethod.id == environment['creditIdUnbind']) {
        if (paymentMethod.id == environment['creditId'] &&
            paymentDropdownValue == 0) {
          Utils.showSnackBar(
            context,
            text: '請選擇信用卡卡號',
            close: true,
          );
          setLoading(false);
          isPaying = false;
          return;
        }
        Services().widget.placeOrder(
          context,
          cartModel: cartModel,
          onLoading: setLoading,
          paymentMethod: paymentMethod,
          success: (Order order) async {
            var url = paymentMethod.id == environment['creditId']
                ? '${environment['serverConfig']['url']}/wc-api/ceompay?order_id=${order.id}&ctcb_ceom_credit=${cartModel.paymentMethod!.cards![paymentDropdownValue - 1]['CardToken']}'
                : '${environment['serverConfig']['url']}/wc-api/wc_ctcb_credit?order_id=${order.id}';

            await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => PaymentWebview(
                  canGoBack: false,
                  url: url,
                  onFinish: (number) {
                    if (number != null) {
                      onFinish(Order(number: number));
                    }
                    setLoading(false);
                    isPaying = false;
                  },
                ),
              ),
            );
            for (var item in order.lineItems) {
              var product = cartModel.getProductById(item.productId!);
              if (product?.bookingInfo != null) {
                product!.bookingInfo!.idOrder = order.id;
                var booking = await createBooking(product.bookingInfo)!;

                Tools.showSnackBar(ScaffoldMessenger.of(context),
                    booking ? 'Booking success!' : 'Booking error!');
              }
            }
            setLoading(false);
          },
          error: (message) {
            setLoading(false);
            if (message != null) {
              Tools.showSnackBar(ScaffoldMessenger.of(context), message);
            }
            isPaying = false;
          },
        );
        return;
      }

      /// Use WebView Payment per frameworks
      Services().widget.placeOrder(
        context,
        cartModel: cartModel,
        onLoading: setLoading,
        paymentMethod: paymentMethod,
        success: (Order? order) async {
          if (order != null) {
            for (var item in order.lineItems) {
              var product = cartModel.getProductById(item.productId!);
              if (product?.bookingInfo != null) {
                product!.bookingInfo!.idOrder = order.id;
                var booking = await createBooking(product.bookingInfo)!;

                Tools.showSnackBar(ScaffoldMessenger.of(context),
                    booking ? 'Booking success!' : 'Booking error!');
              }
            }
            onFinish(order);
          }
          setLoading(false);
          isPaying = false;
        },
        error: (message) {
          setLoading(false);
          if (message != null) {
            Tools.showSnackBar(ScaffoldMessenger.of(context), message);
          }

          isPaying = false;
        },
      );
    } else {
      if (cartModel.purchaseMethod == null) {
        Utils.showSnackBar(
          context,
          text: '請選擇訂購類別',
          close: true,
        );
        return;
      }

      if (cartModel.shippingMethod == null && cartModel.ticketMeta == null) {
        Utils.showSnackBar(
          context,
          text: '請選擇取貨方式',
          close: true,
        );
        return;
      }
      if ((cartModel.address == null || cartModel.address!.street == null) && cartModel.ticketMeta == null) {
        Utils.showSnackBar(
          context,
          text: '請選擇寄送地址',
          close: true,
        );
        return;
      }
      if (cartModel.invoiceDeliverType == null && cartModel.ticketMeta == null) {
        Utils.showSnackBar(
          context,
          text: '請選擇發票寄送方式',
          close: true,
        );
        return;
      }
      if (cartModel.paymentMethod == null) {
        Utils.showSnackBar(
          context,
          text: '請選擇付款方式',
          close: true,
        );
        return;
      }
    }
  }

  Future<void> storeCartInfo() async {
    final cartModel = Provider.of<CartModel>(context, listen: false);
    final _storage = injector<LocalStorage>();
    await _storage.setItem('shippingMethod', cartModel.shippingMethod?.id);
    await _storage.setItem('paymentMethod', cartModel.paymentMethod?.id);

    // await _storage.setItem(
    //     'userCard',
    //     mjUserModel
    //         .mjUserInfoModel!.cardtype![mjUserModel.cardIndex ?? 0].custno!);
  }

  Future<void> readCartInfo() async {
    final cartModel = Provider.of<CartModel>(context, listen: false);

    final paymentMethodModel =
        Provider.of<PaymentMethodModel>(context, listen: false);
    final shippingMethod =
        Provider.of<ShippingMethodModel>(context, listen: false);
    final _storage = injector<LocalStorage>();
    var shippingId = _storage.getItem('shippingMethod');
    var paymentId = _storage.getItem('paymentMethod');

    if (shippingMethod.shippingMethods
            ?.indexWhere((element) => element.id == shippingId) !=
        -1) {
      cartModel.setShippingMethod(shippingMethod.shippingMethods
          ?.firstWhere((element) => element.id == shippingId));
    }
    if (paymentMethodModel.paymentMethods
            .indexWhere((element) => element.id == paymentId) !=
        -1) {
      cartModel.setPaymentMethod(paymentMethodModel.paymentMethods
          .firstWhere((element) => element.id == paymentId));
    }
  }

  Future<bool>? createBooking(BookingModel? bookingInfo) async {
    return Services().api.createBooking(bookingInfo)!;
  }

// Future<void> createOrder(
//     {paid = false, bacs = false, cod = false, transactionId = ''}) async {
//   await createOrderOnWebsite(
//       paid: paid,
//       bacs: bacs,
//       cod: cod,
//       transactionId: transactionId,
//       onFinish: (Order? order) async {
//         if (!transactionId.toString().isEmptyOrNull && order != null) {
//           await Services()
//               .api
//               .updateOrderIdForRazorpay(transactionId, order.number);
//         }
//         onFinish(order);
//       });
// }
//
// Future<void> createOrderOnWebsite(
//     {paid = false,
//       bacs = false,
//       cod = false,
//       transactionId = '',
//       required Function(Order?) onFinish}) async {
//   setLoading(true);
//   await Services().widget.createOrder(
//     context,
//     paid: paid,
//     cod: cod,
//     bacs: bacs,
//     transactionId: transactionId,
//     onLoading: setLoading,
//     success: onFinish,
//     error: (message) {
//       Tools.showSnackBar(Scaffold.of(context), message);
//     },
//   );
//   setLoading(false);
// }
}
