import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';
import 'package:quiver/strings.dart';

import '../../../common/config.dart'
    show kAdvanceConfig, kLoadingWidget, kPaymentConfig;
import '../../../common/constants.dart';
import '../../../common/tools.dart';
import '../../../generated/l10n.dart';
import '../../../models/cart/cart_model.dart';
import '../../../models/entities/order_delivery_date.dart';
import '../../../models/index.dart'
    show Address, CartModel, Country, User, UserModel;
import '../../../models/shipping_method_model.dart';
import '../../../services/index.dart';
import '../add_address_screen.dart';
import 'date_time_picker.dart';

class ShippingMethods extends StatefulWidget {
  final Function? onBack;
  final Function? onNext;

  const ShippingMethods({this.onBack, this.onNext});

  @override
  _ShippingMethodsState createState() => _ShippingMethodsState();
}

class _ShippingMethodsState extends State<ShippingMethods> {
  int? selectedIndex = 0;
  int? selectedAddressIndex = 0;

  ShippingMethodModel get shippingMethodModel =>
      Provider.of<ShippingMethodModel>(context, listen: false);

  CartModel get cartModel => Provider.of<CartModel>(context, listen: false);

  List<Address?> listAddress = [];
  User? user;
  bool isLoading = true;

  Future<void> getUserInfo() async {
    final storage = injector<LocalStorage>();
    final userJson = storage.getItem(kLocalKey['userInfo']!);
    if (userJson != null) {
      final user = await Services().api.getUserInfo(userJson['cookie']);
      if (user != null) {
        user.isSocial = userJson['isSocial'] ?? false;
        setState(() {
          this.user = user;
        });
      }
    }
  }

  Future<void> getDatafromLocal() async {
    final storage = LocalStorage('address');
    var _list = <Address?>[];
    try {
      final ready = await storage.ready;
      if (ready) {
        var data = storage.getItem('data');
        if (data != null) {
          for (var item in data as List) {
            final add = Address.fromLocalJson(item);
            _list.add(add);
          }
        }
      }
      setState(() {
        listAddress = _list;
      });
    } catch (_) {}
  }

  Widget convertToCard(BuildContext context, Address address) {
    final s = S.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 10.0),
        Text(
          address.lastName! + address.firstName!,
        ),
        const SizedBox(height: 4.0),
        Text(
          '(+886)' + address.phoneNumber!,
          style: TextStyle(color: HexColor('#999999')),
        ),
        const SizedBox(height: 4.0),
        Text(
          address.state! + address.city! + address.street!,
          style: TextStyle(color: HexColor('#999999')),
        ),
        const SizedBox(height: 10.0),
      ],
    );
  }

  void removeData(int index) {
    final storage = LocalStorage('address');
    try {
      var data = storage.getItem('data');
      if (data != null) {
        (data as List).removeAt(index);
      }
      storage.setItem('data', data);
    } catch (_) {}
    getDatafromLocal();
  }

  final TextEditingController _cityController = TextEditingController();
  final TextEditingController _streetController = TextEditingController();
  final TextEditingController _blockController = TextEditingController();
  final TextEditingController _zipController = TextEditingController();
  final TextEditingController _stateController = TextEditingController();
  final TextEditingController _countryController = TextEditingController();
  final TextEditingController _apartmentController = TextEditingController();

  final _lastNameNode = FocusNode();
  final _phoneNode = FocusNode();
  final _emailNode = FocusNode();
  final _cityNode = FocusNode();
  final _streetNode = FocusNode();
  final _blockNode = FocusNode();
  final _zipNode = FocusNode();
  final _stateNode = FocusNode();
  final _countryNode = FocusNode();
  final _apartmentNode = FocusNode();

  Address? address;
  List<Country>? countries = [];
  List<dynamic> states = [];

  @override
  void dispose() {
    _cityController.dispose();
    _streetController.dispose();
    _blockController.dispose();
    _zipController.dispose();
    _stateController.dispose();
    _countryController.dispose();
    _apartmentController.dispose();

    _lastNameNode.dispose();
    _phoneNode.dispose();
    _emailNode.dispose();
    _cityNode.dispose();
    _streetNode.dispose();
    _blockNode.dispose();
    _zipNode.dispose();
    _stateNode.dispose();
    _countryNode.dispose();
    _apartmentNode.dispose();

    super.dispose();
  }

  Future<void> updateState(Address? address) async {
    setState(() {
      _cityController.text = address?.city ?? '';
      _streetController.text = address?.street ?? '';
      _zipController.text = address?.zipCode ?? '';
      _stateController.text = address?.state ?? '';
      _countryController.text = address?.country ?? '';
      this.address?.country = address?.country ?? '';
      _apartmentController.text = address?.apartment ?? '';
      _blockController.text = address?.block ?? '';
    });
  }

  @override
  void initState() {
    super.initState();
    getDatafromLocal();
    final loggedIn = Provider.of<UserModel>(context, listen: false).loggedIn;
    if (loggedIn) {
      getUserInfo().then((_) async {
        if (mounted) {
          setState(() {
            isLoading = false;
          });
        }
      });
    } else {
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
    Future.delayed(
      Duration.zero,
      () async {
        final shippingMethod = cartModel.shippingMethod;
        final shippingMethods = shippingMethodModel.shippingMethods;
        if (shippingMethods != null &&
            shippingMethods.isNotEmpty &&
            shippingMethod != null) {
          final index = shippingMethods
              .indexWhere((element) => element.id == shippingMethod.id);
          if (index > -1) {
            setState(() {
              selectedIndex = index;
            });
          }
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final shippingMethodModel = Provider.of<ShippingMethodModel>(context);
    final cartModel = Provider.of<CartModel>(context);
    final currency = Provider.of<CartModel>(context).currency;
    final currencyRates = Provider.of<CartModel>(context).currencyRates;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const Text(
          '取貨方式',
          style: TextStyle(fontSize: 16),
        ),
        const SizedBox(height: 20),
        ListenableProvider.value(
          value: shippingMethodModel,
          child: Consumer<ShippingMethodModel>(
            builder: (context, model, child) {
              if (model.isLoading) {
                return SizedBox(height: 100, child: kLoadingWidget(context));
              }

              if (model.message != null) {
                return SizedBox(
                  height: 100,
                  child: Center(
                      child: Text(model.message!,
                          style: const TextStyle(color: kErrorRed))),
                );
              }

              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  for (int i = 0; i < model.shippingMethods!.length; i++)
                    Column(
                      children: <Widget>[
                        GestureDetector(
                          onTap: (){
                            setState(() {
                              selectedIndex = i;
                            });
                          },
                          child: Container(
                            decoration: const BoxDecoration(
                              color: Colors.white,
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 15, horizontal: 10),
                              child: Row(
                                children: <Widget>[
                                  Radio(
                                    value: i,
                                    activeColor: Theme.of(context).primaryColor,
                                    groupValue: selectedIndex,
                                    onChanged: (dynamic i) {
                                      setState(() {
                                        selectedIndex = i;
                                      });
                                    },
                                  ),
                                  const SizedBox(width: 10),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Services()
                                            .widget
                                            .renderShippingPaymentTitle(context,
                                            model.shippingMethods![i].title!),
                                        const SizedBox(height: 5),
                                        if (model.shippingMethods![i].cost! >
                                            0.0 ||
                                            !isNotBlank(model
                                                .shippingMethods![i].classCost))
                                          Text(
                                            PriceTools.getCurrencyFormatted(
                                                model.shippingMethods![i].cost,
                                                currencyRates,
                                                currency: currency)!,
                                            style: const TextStyle(
                                                fontSize: 14, color: kGrey400),
                                          ),
                                        if (model.shippingMethods![i].cost ==
                                            0.0 &&
                                            isNotBlank(model
                                                .shippingMethods![i].classCost))
                                          Text(
                                            model.shippingMethods![i].classCost!,
                                            style: const TextStyle(
                                                fontSize: 14, color: kGrey400),
                                          )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        i < model.shippingMethods!.length - 1
                            ? const Divider(height: 1)
                            : Container()
                      ],
                    ),
                  const SizedBox(height: 20),
                  buildDeliveryDate(),
                ],
              );
            },
          ),
        ),

        const Text(
          '郵寄地址',
          style: TextStyle(fontSize: 16),
        ),
        const SizedBox(height: 20),
        ...List.generate(listAddress.length, (index) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 1),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(3),
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    selectedAddressIndex = index;
                  });
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: const BoxDecoration(color: Colors.white),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 4),
                    child: Row(
                      children: <Widget>[
                        Radio(
                          activeColor: Theme.of(context).primaryColor,
                          value: index,
                          groupValue: selectedAddressIndex,
                          onChanged: (dynamic i) {
                            setState(() {
                              selectedAddressIndex = i;
                            });
                          },
                        ),
                        const SizedBox(width: 10),
                        Expanded(
                          child: convertToCard(context, listAddress[index]!),
                        ),
                        GestureDetector(
                          onTap: () {
                            removeData(index);
                          },
                          child: Icon(
                            Icons.delete,
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                        const SizedBox(width: 10),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        }),
        const SizedBox(height: 8),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              onTap: () async {
                var save = await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const AddAddressScreen(),
                  ),
                );
                if (save ?? false) {
                  await getDatafromLocal();
                  await showDialog(
                    context: context,
                    useRootNavigator: false,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title:
                            Text(S.of(context).youHaveBeenSaveAddressYourLocal),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              S.of(context).ok,
                              style: TextStyle(
                                  color: Theme.of(context).primaryColor),
                            ),
                          )
                        ],
                      );
                    },
                  );
                }
              },
              child: Row(
                children: [
                  Text(
                    '新增收件資訊',
                    style: TextStyle(color: Theme.of(context).primaryColor),
                  ),
                  Icon(Icons.keyboard_arrow_right_outlined,
                      color: Theme.of(context).primaryColor)
                ],
              ),
            )
          ],
        ),
        const SizedBox(height: 50),
        Row(
          children: [
            Expanded(
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  elevation: 0,
                  onPrimary: Colors.white,
                  primary: Theme.of(context).primaryColor,
                ),
                onPressed: () {
                  if (shippingMethodModel.shippingMethods?.isNotEmpty ??
                      false) {
                    Provider.of<CartModel>(context, listen: false)
                        .setShippingMethod(shippingMethodModel
                            .shippingMethods![selectedIndex!]);
                    Provider.of<CartModel>(context, listen: false)
                        .setAddress(listAddress[selectedAddressIndex!]);
                    widget.onNext!();
                  }
                },
                child: Text(((kPaymentConfig['EnableReview'] ?? true)
                        ? S.of(context).bookingConfirm
                        : S.of(context).continueToPayment)
                    .toUpperCase()),
              ),
            ),
          ],
        ),
        // if (kPaymentConfig['EnableAddress'])
        //   Center(
        //     child: TextButton(
        //       onPressed: () {
        //         widget.onBack!();
        //       },
        //       child: Text(
        //         S.of(context).goBackToAddress,
        //         textAlign: TextAlign.center,
        //         style: const TextStyle(
        //             decoration: TextDecoration.underline,
        //             fontSize: 15,
        //             color: kGrey400),
        //       ),
        //     ),
        //   )
      ],
    );
  }

  Widget buildDeliveryDate() {
    if (!(kAdvanceConfig['EnableDeliveryDateOnCheckout'] ?? false)) {
      return const SizedBox();
    }

    Widget deliveryWidget = DateTimePicker(
      onChanged: (DateTime datetime) {
        cartModel.selectedDate = OrderDeliveryDate(datetime);
      },
      minimumDate: DateTime.now(),
      initDate: cartModel.selectedDate?.dateTime,
      border: const OutlineInputBorder(),
    );

    // if (shippingMethodModel.deliveryDates?.isNotEmpty ?? false) {
    //   deliveryWidget =
    //       DeliveryCalendar(dates: shippingMethodModel.deliveryDates!);
    // }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
              right: Tools.isRTL(context) ? 12.0 : 0.0,
              left: !Tools.isRTL(context) ? 12.0 : 0.0),
          child: Text(S.of(context).deliveryDate,
              style: Theme.of(context).textTheme.caption!.copyWith(
                  color: Theme.of(context)
                      .colorScheme
                      .secondary
                      .withOpacity(0.7))),
        ),
        const SizedBox(height: 10),
        deliveryWidget,
        const SizedBox(height: 20),
      ],
    );
  }
}
