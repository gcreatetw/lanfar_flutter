import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';
import 'package:quiver/strings.dart';

import '../../../common/config.dart'
    show kAdvanceConfig, kLoadingWidget, kPaymentConfig;
import '../../../common/constants.dart';
import '../../../common/tools.dart';
import '../../../from_lanfar/utils/utils.dart';
import '../../../generated/l10n.dart';
import '../../../models/entities/order_delivery_date.dart';
import '../../../models/index.dart'
    show Address, CartModel, Country, User, UserModel;
import '../../../models/shipping_method_model.dart';
import '../../../services/index.dart';
import '../add_address_screen.dart';
import '../bill_screen.dart';
import 'date_time_picker.dart';

class ShippingMethodsScreen extends StatefulWidget {
  final Function? onFinish;

  const ShippingMethodsScreen({
    Key? key,
    this.onFinish,
  }) : super(key: key);

  @override
  State<ShippingMethodsScreen> createState() => _ShippingMethodsScreenState();
}

class _ShippingMethodsScreenState extends State<ShippingMethodsScreen>
    with TickerProviderStateMixin {
  int? selectedIndex = 0;
  int? selectedAddressIndex = 0;
  int? businessIndex;
  int maxNumber = 1;

  ShippingMethodModel get shippingMethodModel =>
      Provider.of<ShippingMethodModel>(context, listen: false);

  CartModel get cartModel => Provider.of<CartModel>(context, listen: false);

  List<Address?> listAddress = [];
  User? user;
  bool isLoading = true;

  List<AnimationController> folderExpandController = [];

  List<Animation<double>> animation = [];

  List<bool> expand = [];

  Future<void> getUserInfo() async {
    final storage = injector<LocalStorage>();
    final userJson = storage.getItem(kLocalKey['userInfo']!);
    if (userJson != null) {
      final user = await Services().api.getUserInfo(userJson['cookie']);
      if (user != null) {
        user.isSocial = userJson['isSocial'] ?? false;
        setState(() {
          this.user = user;
        });
      }
    }
  }

  Future<void> getDatafromLocal() async {
    setState(() {
      isLoading = true;
    });
    final response = await Services()
        .api
        .getAddress(userId: context.read<UserModel>().user?.username ?? '');

    if (response?.error == true) {
      final storage = LocalStorage('address');
      var _list = <Address?>[];
      try {
        final ready = await storage.ready;
        if (ready) {
          var data = storage.getItem('data');
          if (data != null) {
            for (var item in data as List) {
              final add = Address.fromLocalJson(item);
              if (add.defaultAddress == true) {
                _list.insert(0, add);
              } else {
                _list.add(add);
              }
              if (add.id != null && add.id! > maxNumber) {
                maxNumber = add.id! + 1;
              }
            }
          }
        }
        setState(() {
          listAddress = _list;
        });
      } catch (_) {}
    } else {
      var _list = <Address?>[];
      if (response?.data != null) {
        for (var element in response!.data!) {
          final add = Address.fromApi(element);
          if (add.defaultAddress == true) {
            _list.insert(0, add);
          } else {
            _list.add(add);
          }
          if (add.id != null && add.id! > maxNumber) {
            maxNumber = add.id! + 1;
          }
        }
      }
      setState(() {
        listAddress = _list;
      });
    }
    setState(() {
      isLoading = false;
    });
  }

  Widget convertToCard(BuildContext context, Address address) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const SizedBox(height: 10.0),
              Text(
                address.lastName! + ' ' + (address.firstName ?? ''),
              ),
              const SizedBox(height: 4.0),
              Text(
                '(+886)' + (address.phoneNumber ?? ''),
                style: TextStyle(color: HexColor('#999999')),
              ),
              const SizedBox(height: 4.0),
              Text(
                (address.state ?? '') +
                    (address.city ?? '') +
                    (address.street ?? ''),
                style: TextStyle(color: HexColor('#999999')),
              ),
              const SizedBox(height: 10.0),
            ],
          ),
        ),
        if (address.defaultAddress == true)
          const Padding(
            padding: EdgeInsets.only(
              top: 10,
              right: 16,
            ),
            child: Text(
              '預設',
              style: TextStyle(color: primaryColor),
            ),
          ),
      ],
    );
  }

  Future<void> removeData(int index) async {
    setState(() {
      isLoading = true;
    });
    final response = await Services().api.updateAddress(
          userId: context.read<UserModel>().user?.username ?? '',
          action: 'delete',
          addressId: listAddress[index]?.id,
        );
    setState(() {
      isLoading = false;
    });
    if (response == null) {
      Utils.showSnackBar(
        context,
        text: '刪除成功',
      );
      await getDatafromLocal();
    } else {
      Utils.showSnackBar(
        context,
        text: response,
      );
    }
    // final storage = LocalStorage('address');
    // try {
    //   var data = storage.getItem('data');
    //   if (data != null) {
    //     (data as List).removeAt(index);
    //   }
    //   storage.setItem('data', data);
    // } catch (_) {}
    // getDatafromLocal();
  }

  final TextEditingController _cityController = TextEditingController();
  final TextEditingController _streetController = TextEditingController();
  final TextEditingController _blockController = TextEditingController();
  final TextEditingController _zipController = TextEditingController();
  final TextEditingController _stateController = TextEditingController();
  final TextEditingController _countryController = TextEditingController();
  final TextEditingController _apartmentController = TextEditingController();

  final _lastNameNode = FocusNode();
  final _phoneNode = FocusNode();
  final _emailNode = FocusNode();
  final _cityNode = FocusNode();
  final _streetNode = FocusNode();
  final _blockNode = FocusNode();
  final _zipNode = FocusNode();
  final _stateNode = FocusNode();
  final _countryNode = FocusNode();
  final _apartmentNode = FocusNode();

  Address? address;
  List<Country>? countries = [];
  List<dynamic> states = [];

  @override
  void dispose() {
    _cityController.dispose();
    _streetController.dispose();
    _blockController.dispose();
    _zipController.dispose();
    _stateController.dispose();
    _countryController.dispose();
    _apartmentController.dispose();

    _lastNameNode.dispose();
    _phoneNode.dispose();
    _emailNode.dispose();
    _cityNode.dispose();
    _streetNode.dispose();
    _blockNode.dispose();
    _zipNode.dispose();
    _stateNode.dispose();
    _countryNode.dispose();
    _apartmentNode.dispose();

    super.dispose();
  }

  Future<void> updateState(Address? address) async {
    setState(() {
      _cityController.text = address?.city ?? '';
      _streetController.text = address?.street ?? '';
      _zipController.text = address?.zipCode ?? '';
      _stateController.text = address?.state ?? '';
      _countryController.text = address?.country ?? '';
      this.address?.country = address?.country ?? '';
      _apartmentController.text = address?.apartment ?? '';
      _blockController.text = address?.block ?? '';
    });
  }

  @override
  void initState() {
    super.initState();
    final loggedIn = Provider.of<UserModel>(context, listen: false).loggedIn;
    if (loggedIn) {
      getUserInfo().then((_) async {
        // if (mounted) {
        //   setState(() {
        //     isLoading = false;
        //   });
        // }
      });
    } else {
      // if (mounted) {
      //   setState(() {
      //     isLoading = false;
      //   });
      // }
    }
    Future.delayed(
      Duration.zero,
      () async {
        final shippingMethod = cartModel.shippingMethod;
        final shippingMethods = shippingMethodModel.shippingMethods;
        if (shippingMethods != null && shippingMethods.isNotEmpty) {
          if (shippingMethod != null) {
            final index = shippingMethods
                .indexWhere((element) => element.id == shippingMethod.id);
            if (index > -1) {
              setState(() {
                selectedIndex = index;
              });
            }
          }

          for (var i = 0;
              i < shippingMethodModel.shippingMethods!.length;
              i++) {
            folderExpandController.add(
              AnimationController(
                vsync: this,
                duration: const Duration(milliseconds: 300),
              ),
            );
            animation.add(
              CurvedAnimation(
                parent: folderExpandController[i],
                curve: Curves.fastOutSlowIn,
              ),
            );
            expand.add(false);
          }
          await folderExpandController[selectedIndex ?? 0].forward();
        }
        await getDatafromLocal();
        var addressIndex = listAddress.indexWhere((element) {
          if (element!.lastName == cartModel.address!.lastName &&
              element.firstName == cartModel.address!.firstName &&
              element.phoneNumber == cartModel.address!.phoneNumber &&
              element.state == cartModel.address!.state &&
              element.city == cartModel.address!.city &&
              element.street == cartModel.address!.street) {
            return true;
          }
          return false;
        });
        if (addressIndex != -1) {
          setState(() {
            selectedAddressIndex = addressIndex;
          });
        }
        if (cartModel.invoiceOffice != null) {
          setState(() {
            businessIndex = OfficeType.values
                .indexWhere((element) => element == cartModel.invoiceOffice);
          });
        } else {
          if (cartModel.user!.officeType.isNotEmpty) {
            setState(() {
              businessIndex = OfficeType.values.indexWhere(
                  (element) => element == cartModel.user!.officeType.first);
            });
          }
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final shippingMethodModel = Provider.of<ShippingMethodModel>(context);
    final cartModel = Provider.of<CartModel>(context);
    final currency = Provider.of<CartModel>(context).currency;
    final currencyRates = Provider.of<CartModel>(context).currencyRates;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.background,
        title: Text(
          '取貨方式',
          style: TextStyle(
            color: Theme.of(context).colorScheme.secondary,
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // const Text(
          //   '取貨方式',
          //   style: TextStyle(fontSize: 16),
          // ),
          const SizedBox(height: 20),
          Expanded(
              child: ListenableProvider.value(
            value: shippingMethodModel,
            child: Consumer<ShippingMethodModel>(
              builder: (context, model, child) {
                if (model.isLoading) {
                  return SizedBox(height: 100, child: kLoadingWidget(context));
                }

                if (model.message != null) {
                  return SizedBox(
                    height: 100,
                    child: Center(
                        child: Text(model.message!,
                            style: const TextStyle(color: kErrorRed))),
                  );
                }

                return ListView.separated(
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          setState(() {
                            selectedIndex = index;
                          });
                          for (var i = 0;
                              i < shippingMethodModel.shippingMethods!.length;
                              i++) {
                            if (i == index) {
                              folderExpandController[i].forward();
                            } else {
                              folderExpandController[i].reverse();
                            }
                          }
                        },
                        child: Column(
                          children: [
                            Container(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 15, horizontal: 20),
                              color: selectedIndex == index
                                  ? primaryColor
                                  : Colors.white,
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Text(
                                      model.shippingMethods![index].title!,
                                      style: TextStyle(
                                        color: selectedIndex == index
                                            ? Colors.white
                                            : null,
                                      ),
                                    ),
                                  ),
                                  if (model.shippingMethods![index].cost! >
                                          0.0 ||
                                      !isNotBlank(model
                                          .shippingMethods![index].classCost))
                                    Text(
                                      '\$' +
                                          model.shippingMethods![index].cost!
                                              .toInt()
                                              .toString(),
                                      style: TextStyle(
                                        color: selectedIndex == index
                                            ? Colors.white
                                            : null,
                                      ),
                                    ),
                                  const SizedBox(width: 8),
                                  Icon(
                                    Icons.check,
                                    color: selectedIndex == index
                                        ? Colors.white
                                        : Colors.transparent,
                                  ),
                                ],
                              ),
                            ),
                            if (animation.isNotEmpty)
                              SizeTransition(
                                sizeFactor: animation[index],
                                axisAlignment: 1.0,
                                child: Builder(
                                  builder: (context) {
                                    if (model.shippingMethods![index].id !=
                                            null &&
                                        model.shippingMethods![index].id!
                                            .startsWith('betrs_shipping:3')) {
                                      if (isLoading) {
                                        return Column(
                                          children: [
                                            const SizedBox(height: 30),
                                            kLoadingWidget(context),
                                            const SizedBox(height: 30),
                                          ],
                                        );
                                      }
                                      return addressList();
                                    }

                                    ///營業所不再可以選擇
                                    // return Padding(
                                    //   padding: const EdgeInsets.symmetric(
                                    //       horizontal: 16, vertical: 8),
                                    //   child: Container(
                                    //     color: Colors.white,
                                    //     width: double.maxFinite,
                                    //     padding: const EdgeInsets.symmetric(
                                    //       horizontal: 16,
                                    //       vertical: 16,
                                    //     ),
                                    //     child: Text(
                                    //       cartModel.user?.officeType?.title ??
                                    //           '沒有可選擇的營業所',
                                    //       style: const TextStyle(fontSize: 18),
                                    //     ),
                                    //   ),
                                    // );
                                    if (isLoading) {
                                      return Column(
                                        children: [
                                          const SizedBox(height: 30),
                                          kLoadingWidget(context),
                                          const SizedBox(height: 30),
                                        ],
                                      );
                                    }
                                    return Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 16, vertical: 8),
                                      child: Container(
                                        color: Colors.white,
                                        width: double.maxFinite,
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 16),
                                        child: DropdownButton<int>(
                                          value: businessIndex,
                                          dropdownColor: Colors.white,
                                          icon: const Icon(
                                            Icons.keyboard_arrow_down,
                                            color: primaryColor,
                                          ),
                                          isExpanded: true,
                                          underline: const SizedBox(),
                                          hint: const Text('請選擇營業處'),
                                          onChanged: (v) {
                                            setState(() {
                                              businessIndex = v;
                                            });
                                          },
                                          items: [
                                            for (int i = 0;
                                                i < user!.officeType.length;
                                                i++)
                                              DropdownMenuItem(
                                                value:
                                                    user!.officeType[i]!.index,
                                                child: Text(
                                                  user!.officeType[i]!.title,
                                                ),
                                              )
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                          ],
                        ),
                      );
                    },
                    separatorBuilder: (BuildContext context, int index) {
                      return const SizedBox(height: 15);
                    },
                    itemCount: model.shippingMethods!.length);
                // return Column(
                //   crossAxisAlignment: CrossAxisAlignment.start,
                //   children: <Widget>[
                //     for (int i = 0; i < model.shippingMethods!.length; i++)
                //       Column(
                //         children: <Widget>[
                //           GestureDetector(
                //             onTap: () {
                //               setState(() {
                //                 selectedIndex = i;
                //               });
                //             },
                //             child:
                //             // Container(
                //             //   decoration: const BoxDecoration(
                //             //     color: Colors.white,
                //             //   ),
                //             //   child: Padding(
                //             //     padding: const EdgeInsets.symmetric(
                //             //         vertical: 15, horizontal: 10),
                //             //     child: Row(
                //             //       children: <Widget>[
                //             //         Radio(
                //             //           value: i,
                //             //           activeColor: Theme.of(context).primaryColor,
                //             //           groupValue: selectedIndex,
                //             //           onChanged: (dynamic i) {
                //             //             setState(() {
                //             //               selectedIndex = i;
                //             //             });
                //             //           },
                //             //         ),
                //             //         const SizedBox(width: 10),
                //             //         Expanded(
                //             //           child: Column(
                //             //             crossAxisAlignment:
                //             //             CrossAxisAlignment.start,
                //             //             children: <Widget>[
                //             //               Services()
                //             //                   .widget
                //             //                   .renderShippingPaymentTitle(context,
                //             //                   model.shippingMethods![i].title!),
                //             //               const SizedBox(height: 5),
                //             //               if (model.shippingMethods![i].cost! >
                //             //                   0.0 ||
                //             //                   !isNotBlank(model
                //             //                       .shippingMethods![i].classCost))
                //             //                 Text(
                //             //                   PriceTools.getCurrencyFormatted(
                //             //                       model.shippingMethods![i].cost,
                //             //                       currencyRates,
                //             //                       currency: currency)!,
                //             //                   style: const TextStyle(
                //             //                       fontSize: 14, color: kGrey400),
                //             //                 ),
                //             //               if (model.shippingMethods![i].cost ==
                //             //                   0.0 &&
                //             //                   isNotBlank(model
                //             //                       .shippingMethods![i].classCost))
                //             //                 Text(
                //             //                   model.shippingMethods![i].classCost!,
                //             //                   style: const TextStyle(
                //             //                       fontSize: 14, color: kGrey400),
                //             //                 )
                //             //             ],
                //             //           ),
                //             //         )
                //             //       ],
                //             //     ),
                //             //   ),
                //             // ),
                //           ),
                //           i < model.shippingMethods!.length - 1
                //               ? const Divider(height: 1)
                //               : Container()
                //         ],
                //       ),
                //     const SizedBox(height: 20),
                //     buildDeliveryDate(),
                //   ],
                // );
              },
            ),
          )),

          // const SizedBox(height: 50),
          Builder(builder: (context) {
            return Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                    style: ButtonStyle(
                        padding: MaterialStateProperty.all<EdgeInsets>(
                            const EdgeInsets.all(16)),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          const RoundedRectangleBorder(
                              borderRadius: BorderRadius.zero),
                        ),
                        backgroundColor:
                            MaterialStateProperty.all(primaryColor)),
                    onPressed: () {
                      if (shippingMethodModel.shippingMethods?.isNotEmpty ??
                          false) {
                        if (selectedIndex == null) {
                          return;
                        }
                        if (shippingMethodModel
                                    .shippingMethods![selectedIndex!].id ==
                                'betrs_shipping:3-2' ||
                            shippingMethodModel
                                    .shippingMethods![selectedIndex!].id ==
                                'betrs_shipping:3-1') {
                          if (selectedAddressIndex == null) {
                            return;
                          }
                        }
                        if (shippingMethodModel
                                .shippingMethods![selectedIndex!].id ==
                            'betrs_shipping:10-1') {
                          // if (businessIndex == null) {
                          //   ScaffoldMessenger.of(context).showSnackBar(
                          //     SnackBar(
                          //       content: const Text('請選擇營業所'),
                          //       action: SnackBarAction(
                          //         label: S.of(context).close,
                          //         onPressed: () {
                          //           // Some code to undo the change.
                          //         },
                          //       ),
                          //     ),
                          //   );
                          //   return;
                          // }
                          if (cartModel.user!.officeType.isEmpty) {
                            Utils.showSnackBar(
                              context,
                              text: '沒有可選擇的營業所',
                            );
                            return;
                          }
                          if (businessIndex == null) {
                            Utils.showSnackBar(
                              context,
                              text: '請選擇營業所',
                            );
                            return;
                          }
                        }
                        if (shippingMethodModel
                                .shippingMethods![selectedIndex!] !=
                            cartModel.shippingMethod) {
                          cartModel.invoiceOffice = null;
                          cartModel.invoiceDeliverType = null;
                          cartModel.carrierNo = null;
                          cartModel.paymentMethod = null;
                        }
                        Provider.of<CartModel>(context, listen: false)
                            .setShippingMethod(shippingMethodModel
                                .shippingMethods![selectedIndex!]);
                        if (shippingMethodModel
                                    .shippingMethods![selectedIndex!].id ==
                                'betrs_shipping:3-2' ||
                            shippingMethodModel
                                    .shippingMethods![selectedIndex!].id ==
                                'betrs_shipping:3-1') {
                          if (listAddress.isEmpty) {
                            Utils.showSnackBar(
                              context,
                              text: '請先新增收件資訊',
                            );
                            return;
                          }
                          Provider.of<CartModel>(context, listen: false)
                              .invoiceDeliverType = InvoiceDeliverType.included;
                          Provider.of<CartModel>(context, listen: false)
                              .setAddress(listAddress[selectedAddressIndex!]);
                          Provider.of<CartModel>(context, listen: false)
                              .invoiceOffice = null;
                        } else {
                          Provider.of<CartModel>(context, listen: false)
                                  .invoiceOffice =
                              OfficeType.values[businessIndex!];
                          // user?.officeType[businessIndex!];
                          // Provider.of<CartModel>(context, listen: false)
                          //     .invoiceOffice = cartModel.user?.officeType;
                          cartModel.invoiceDeliverType =
                              InvoiceDeliverType.businessOffice;
                          cartModel.invoiceOffice =
                              OfficeType.values[businessIndex!];
                          // user?.officeType[businessIndex!];
                          Provider.of<CartModel>(context, listen: false)
                              .setAddress(
                            Address(
                              firstName: '',
                              lastName: '',
                              email: '',
                              street: '',
                              apartment: '',
                              block: '',
                              city: '',
                              state: '',
                              country: 'TW',
                              phoneNumber: '',
                              zipCode: '',
                              mapUrl: '',
                            ),
                          );
                        }

                        Navigator.pop(context);
                      }
                    },
                    child: Padding(
                      padding: EdgeInsets.only(
                          bottom: MediaQuery.of(context).padding.bottom),
                      child: Text(
                        ((kPaymentConfig['EnableReview'] ?? true)
                                ? S.of(context).bookingConfirm
                                : S.of(context).continueToPayment)
                            .toUpperCase(),
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ],
            );
          }),
          // if (kPaymentConfig['EnableAddress'])
          //   Center(
          //     child: TextButton(
          //       onPressed: () {
          //         widget.onBack!();
          //       },
          //       child: Text(
          //         S.of(context).goBackToAddress,
          //         textAlign: TextAlign.center,
          //         style: const TextStyle(
          //             decoration: TextDecoration.underline,
          //             fontSize: 15,
          //             color: kGrey400),
          //       ),
          //     ),
          //   )
        ],
      ),
    );
  }

  Widget buildDeliveryDate() {
    if (!(kAdvanceConfig['EnableDeliveryDateOnCheckout'] ?? false)) {
      return const SizedBox();
    }

    Widget deliveryWidget = DateTimePicker(
      onChanged: (DateTime datetime) {
        cartModel.selectedDate = OrderDeliveryDate(datetime);
      },
      minimumDate: DateTime.now(),
      initDate: cartModel.selectedDate?.dateTime,
      border: const OutlineInputBorder(),
    );

    // if (shippingMethodModel.deliveryDates?.isNotEmpty ?? false) {
    //   deliveryWidget =
    //       DeliveryCalendar(dates: shippingMethodModel.deliveryDates!);
    // }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
              right: Tools.isRTL(context) ? 12.0 : 0.0,
              left: !Tools.isRTL(context) ? 12.0 : 0.0),
          child: Text(S.of(context).deliveryDate,
              style: Theme.of(context).textTheme.caption!.copyWith(
                  color: Theme.of(context)
                      .colorScheme
                      .secondary
                      .withOpacity(0.7))),
        ),
        const SizedBox(height: 10),
        deliveryWidget,
        const SizedBox(height: 20),
      ],
    );
  }

  Widget addressList() {
    return Column(
      children: [
        ...List.generate(listAddress.length, (index) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 1),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(3),
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    selectedAddressIndex = index;
                  });
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: const BoxDecoration(color: Colors.white),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 4),
                    child: Row(
                      children: <Widget>[
                        Radio(
                          activeColor: Theme.of(context).primaryColor,
                          value: index,
                          groupValue: selectedAddressIndex,
                          onChanged: (dynamic i) {
                            setState(() {
                              selectedAddressIndex = i;
                            });
                          },
                        ),
                        const SizedBox(width: 10),
                        Expanded(
                            child: Column(
                          children: [
                            convertToCard(context, listAddress[index]!),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                const Spacer(),
                                GestureDetector(
                                  onTap: () async {
                                    final result = await showDialog(
                                      context: context,
                                      useRootNavigator: false,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          title: const Text('複製地址'),
                                          content: const Text('地址會重複，你確定嗎？'),
                                          actions: <Widget>[
                                            TextButton(
                                              onPressed: () async {
                                                Navigator.of(context).pop();
                                              },
                                              child: const Text(
                                                '取消',
                                                style: TextStyle(
                                                    color: Colors.black54),
                                              ),
                                            ),
                                            TextButton(
                                              onPressed: () async {
                                                Navigator.of(context).pop(true);
                                              },
                                              child: Text(
                                                '確認',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .primaryColor),
                                              ),
                                            )
                                          ],
                                        );
                                      },
                                    );
                                    if (result == true) {
                                      setState(() {
                                        isLoading = true;
                                      });
                                      final response = await Services()
                                          .api
                                          .updateAddress(
                                            userId: context
                                                    .read<UserModel>()
                                                    .user
                                                    ?.username ??
                                                '',
                                            action: 'insert',
                                            addressData: Address()
                                                .toApiJson(listAddress[index]!),
                                          );
                                      setState(() {
                                        isLoading = false;
                                      });
                                      if (response == null) {
                                        Utils.showSnackBar(
                                          context,
                                          text: '新增成功',
                                        );
                                        await getDatafromLocal();
                                      } else {
                                        Utils.showSnackBar(
                                          context,
                                          text: response,
                                        );
                                      }
                                    }
                                  },
                                  child: const Text(
                                    '複製',
                                    style: TextStyle(color: primaryColor),
                                  ),
                                ),
                                const SizedBox(width: 16),
                                GestureDetector(
                                  onTap: () async {
                                    var save = await Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => AddAddressScreen(
                                          address: listAddress[index],
                                        ),
                                      ),
                                    );
                                    if (save ?? false) {
                                      await getDatafromLocal();
                                    }
                                  },
                                  child: const Text(
                                    '編輯',
                                    style: TextStyle(color: primaryColor),
                                  ),
                                ),
                                const SizedBox(width: 16),
                                GestureDetector(
                                  onTap: () async {
                                    await showDialog(
                                      context: context,
                                      useRootNavigator: false,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          title: const Text('是否要刪除？'),
                                          actions: <Widget>[
                                            TextButton(
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                              child: Text(
                                                '取消',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .primaryColor),
                                              ),
                                            ),
                                            TextButton(
                                              onPressed: () {
                                                removeData(index);
                                                Navigator.of(context).pop();
                                              },
                                              child: Text(
                                                '確認',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .primaryColor),
                                              ),
                                            )
                                          ],
                                        );
                                      },
                                    );
                                  },
                                  child: const Text(
                                    '刪除',
                                    style: TextStyle(color: primaryColor),
                                  ),
                                ),
                                const SizedBox(
                                  width: 16,
                                ),
                              ],
                            ),
                            const SizedBox(height: 10),
                          ],
                        )),

                        // GestureDetector(
                        //   onTap: () {
                        //     removeData(index);
                        //   },
                        //   child: Icon(
                        //     Icons.delete,
                        //     color: Theme.of(
                        //             context)
                        //         .primaryColor,
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        }),
        const SizedBox(height: 8),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              onTap: () async {
                var save = await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AddAddressScreen(
                      maxNumber: maxNumber,
                    ),
                  ),
                );
                if (save ?? false) {
                  await getDatafromLocal();
                  await showDialog(
                    context: context,
                    useRootNavigator: false,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title:
                            Text(S.of(context).youHaveBeenSaveAddressYourLocal),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              '確認',
                              style: TextStyle(
                                  color: Theme.of(context).primaryColor),
                            ),
                          )
                        ],
                      );
                    },
                  );
                }
              },
              child: Row(
                children: [
                  Text(
                    '新增收件資訊',
                    style: TextStyle(color: Theme.of(context).primaryColor),
                  ),
                  Icon(Icons.keyboard_arrow_right_outlined,
                      color: Theme.of(context).primaryColor)
                ],
              ),
            )
          ],
        ),
      ],
    );
  }
}
