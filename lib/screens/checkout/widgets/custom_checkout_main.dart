import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:provider/provider.dart';

import '../../../common/tools.dart';
import '../../../env.dart';
import '../../../from_lanfar/contants/constants.dart';
import '../../../from_lanfar/utils/utils.dart';
import '../../../models/cart/mixin/cart_mixin.dart';
import '../../../models/index.dart'
    show AppModel, CartModel, PaymentMethodModel, Product, TaxModel;
import '../../../widgets/product/cart_item.dart';
import '../bill_screen.dart';
import '../choose_bag_screen.dart';
import '../payment_method_screen.dart';
import '../purchase_method_screen.dart';
import 'shipping_methods_screen.dart';

class CustomCheckoutMain extends StatefulWidget {
  final Function? toShipping;
  final Function? toReceipt;
  final Function? toPayment;
  final Function setStateClick;
  final BillModel billModel;
  final int paymentDropdownValue;
  final TextEditingController noteController;
  final Function? paymentOnBack;

  const CustomCheckoutMain({
    Key? key,
    this.toShipping,
    this.toReceipt,
    this.toPayment,
    required this.setStateClick,
    required this.paymentDropdownValue,
    required this.billModel,
    required this.noteController,
    this.paymentOnBack,
  }) : super(key: key);

  @override
  _State createState() => _State();
}

class _State extends State<CustomCheckoutMain> {
  bool canShippingTap = false;
  bool canBillingTap = false;

  @override
  Widget build(BuildContext context) {
    var cartModel = Provider.of<CartModel>(context);
    if (cartModel.purchaseMethod != null) {
      canShippingTap = true;
    } else {
      canShippingTap = false;
    }
    if (cartModel.shippingMethod != null) {
      canBillingTap = true;
    } else {
      canBillingTap = false;
    }
    return Consumer<CartModel>(builder: (context, model, child) {
      return GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Column(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  if (model.checkoutNotify != null &&
                      model.checkoutNotify!.isNotEmpty)
                    Html(data: model.checkoutNotify),

                  ///product list
                  ...getProducts(model, context),
                  ...getGiftsInCart(model, context),
                  ...getGifts(model, context),
                  ...getGiftsCoupon(model, context),

                  ///purchase method
                  const SizedBox(height: 8),
                  GestureDetector(
                    // onTap: () {
                    //   Navigator.of(context).push(
                    //     MaterialPageRoute(
                    //       builder: (context) => PurchaseMethodScreen(
                    //         setStateClick: widget.setStateClick,
                    //       ),
                    //     ),
                    //   );
                    // },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 16, horizontal: 8),
                      color: Colors.white,
                      child: Row(
                        children: [
                          Image.asset(
                            'assets/icons/from_lanfar/Artboard – 3.png',
                            color: Theme.of(context).primaryColor,
                            height: 24,
                            width: 24,
                          ),
                          const SizedBox(width: 8),
                          Expanded(
                            child: Text(
                              '訂購方式',
                              style: TextStyle(
                                color: HexColor('#999999'),
                              ),
                            ),
                          ),
                          Column(
                            children: [
                              Text(
                                model.purchaseMethod != null
                                    ? model.purchaseMethod!.title
                                    : '請選擇',
                                style: TextStyle(
                                  color: HexColor('#999999'),
                                ),
                              ),
                              if (model.purchaseMethod == PurchaseType.standin)
                                Text(
                                  model.invoiceType?.title ?? '',
                                  style: TextStyle(
                                    color: HexColor('#999999'),
                                  ),
                                ),
                            ],
                          ),
                          Icon(
                            Icons.keyboard_arrow_right,
                            color: HexColor('#999999'),
                          )
                        ],
                      ),
                    ),
                  ),

                  ///DM及購物袋
                  if (cartModel.ticketMeta == null) ...[
                    const SizedBox(height: 2),
                    GestureDetector(
                      onTap: () async {
                        await Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => const ChooseBagScreen(),
                          ),
                        );
                        setState(() {});
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                          vertical: 16,
                          horizontal: 8,
                        ),
                        color: Colors.white,
                        child: Row(
                          children: [
                            Icon(
                              Icons.shopping_bag,
                              color: Theme.of(context).primaryColor,
                            ),
                            const SizedBox(width: 8),
                            const Text(
                              'DM及提袋',
                            ),
                            const SizedBox(width: 8),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    model.purchaseBagType != null
                                        ? model.purchaseBagType!.name
                                        : '請選擇',
                                  ),
                                ],
                              ),
                            ),
                            Icon(
                              Icons.keyboard_arrow_right,
                              color: Theme.of(context).primaryColor,
                            )
                          ],
                        ),
                      ),
                    ),
                  ],

                  ///shipping
                  if (cartModel.ticketMeta == null) ...[
                    const SizedBox(height: 2),
                    GestureDetector(
                      onTap: () async {
                        if (!canShippingTap) {
                          Utils.showSnackBar(
                            context,
                            text: '請先選擇訂購方式',
                            close: true,
                          );
                          return;
                        }
                        await Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => const ShippingMethodsScreen(),
                          ),
                        );
                        setState(() {});
                        // widget.toShipping!();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                          vertical: 16,
                          horizontal: 8,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: model.shippingMethod == null
                              ? Border.all(color: Colors.redAccent)
                              : null,
                        ),
                        child: Row(
                          children: [
                            Image.asset(
                              'assets/icons/from_lanfar/Artboard – 4.png',
                              color: Theme.of(context).primaryColor,
                              height: 24,
                              width: 24,
                            ),
                            const SizedBox(width: 8),
                            const Expanded(
                              child: Text('取貨方式'),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(model.shippingMethod != null
                                    ? '${model.shippingMethod?.title} \$${model.shippingMethod?.cost?.round()}'
                                    : '請選擇'),
                                if (model.shippingMethod != null &&
                                    (model.shippingMethod!.id ==
                                            'betrs_shipping:3-2' ||
                                        model.shippingMethod!.id ==
                                            'betrs_shipping:3-1'))
                                  Text(
                                    model.address != null &&
                                            model.address!.lastName != null &&
                                            model.address!.firstName != null
                                        ? model.address!.lastName! +
                                            '  ' +
                                            model.address!.firstName!
                                        : '未選擇',
                                    style:
                                        TextStyle(color: HexColor('#999999')),
                                  ),
                                if (model.shippingMethod != null &&
                                    (model.shippingMethod!.id !=
                                            'betrs_shipping:3-2' &&
                                        model.shippingMethod!.id !=
                                            'betrs_shipping:3-1'))
                                  Text(
                                    model.invoiceOffice?.title ?? '',
                                    style: TextStyle(
                                      color: HexColor('#999999'),
                                    ),
                                  ),
                              ],
                            ),
                            Icon(
                              Icons.keyboard_arrow_right,
                              color: Theme.of(context).primaryColor,
                            )
                          ],
                        ),
                      ),
                    ),
                  ],

                  ///bill
                  const SizedBox(height: 2),
                  GestureDetector(
                    onTap: () {
                      if (cartModel.ticketMeta != null) {
                        return;
                      }
                      if (!canShippingTap) {
                        Utils.showSnackBar(
                          context,
                          text: '請先選擇訂購方式',
                          close: true,
                        );

                        return;
                      }
                      if (!canBillingTap) {
                        Utils.showSnackBar(
                          context,
                          text: '請先選擇取貨方式',
                          close: true,
                        );
                        return;
                      }
                      widget.toReceipt!();
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 16, horizontal: 8),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: cartModel.invoiceDeliverType == null &&
                                cartModel.ticketMeta == null
                            ? Border.all(color: Colors.redAccent)
                            : null,
                      ),
                      child: Row(
                        children: [
                          Image.asset(
                            'assets/icons/from_lanfar/Artboard – 6.png',
                            color: Theme.of(context).primaryColor,
                            height: 24,
                            width: 24,
                          ),
                          const SizedBox(width: 8),
                          const Expanded(
                            child: Text('發票寄送'),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              if (cartModel.ticketMeta == null)
                                Text(cartModel.invoiceDeliverType?.title ??
                                    '請選擇')
                              else
                                const Text('課後附送'),
                              //TODO 移除載具
                              // if (cartModel.invoiceDeliverType ==
                              //     InvoiceDeliverType.carrier)
                              //   Text(
                              //     cartModel.carrierNo ?? '',
                              //     style: TextStyle(color: HexColor('#999999')),
                              //   ),
                              if (cartModel.invoiceDeliverType ==
                                  InvoiceDeliverType.businessOffice)
                                Text(
                                  cartModel.invoiceOffice?.title ??
                                      (cartModel.invoiceOffice != null
                                          ? cartModel.invoiceOffice!.title
                                          : ''),
                                  style: TextStyle(
                                    color: HexColor('#999999'),
                                  ),
                                ),
                            ],
                          ),
                          Icon(
                            Icons.keyboard_arrow_right,
                            color: Theme.of(context).primaryColor,
                          )
                        ],
                      ),
                    ),
                  ),

                  ///note
                  const SizedBox(height: 2),
                  Container(
                    padding:
                        const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                    color: Colors.white,
                    child: Row(
                      children: [
                        Icon(
                          Icons.message,
                          color: Theme.of(context).primaryColor,
                        ),
                        const SizedBox(width: 8),
                        const Text('備註'),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: TextField(
                              controller: widget.noteController,
                              textAlign: TextAlign.end,
                              textInputAction: TextInputAction.done,
                              onChanged: (value) {
                                cartModel.setOrderNotes(value);
                              },
                              decoration: InputDecoration(
                                hintText: '要留言給我們什麼?',
                                hintStyle: TextStyle(
                                  color: HexColor('#999999'),
                                  fontSize: 16,
                                ),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  ///total
                  const SizedBox(height: 8),
                  Container(
                    padding:
                        const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
                    color: Colors.white,
                    child: Row(
                      children: [
                        Expanded(
                          child: Text('訂單金額(${model.totalCartQuantity} 商品)'),
                        ),
                        Text(
                          PriceTools.getCurrencyFormatted(
                                  cartModel.getSubTotal()!, null) ??
                              '',
                          style:
                              TextStyle(color: Theme.of(context).primaryColor),
                        )
                      ],
                    ),
                  ),

                  ///pay methods,checkout,total
                  const SizedBox(height: 8),
                  Container(
                    height: 8,
                    width: double.maxFinite,
                    color: Colors.white,
                  ),
                  GestureDetector(
                    onTap: () async {
                      if (cartModel.shippingMethod == null &&
                          cartModel.ticketMeta == null) {
                        Utils.showSnackBar(
                          context,
                          text: '請先選擇取貨方式',
                          close: true,
                        );
                        return;
                      }
                      await Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => PaymentMethodScreen(
                            onBack: widget.paymentOnBack,
                          ),
                        ),
                      );
                      setState(() {});
                      // widget.toPayment!();
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8, horizontal: 8),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: cartModel.paymentMethod == null
                            ? Border.all(color: Colors.redAccent)
                            : null,
                      ),
                      child: Row(
                        children: [
                          Icon(
                            Icons.monetization_on,
                            color: Theme.of(context).primaryColor,
                          ),
                          const SizedBox(width: 8),
                          const Expanded(
                            child: Text('付款方式'),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                cartModel.paymentMethod == null
                                    ? '請選擇付款方式'
                                    : cartModel.paymentMethod!.title!,
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor),
                              ),
                              if (cartModel.paymentMethod != null &&
                                  cartModel.paymentMethod!.id ==
                                      environment['creditId'])
                                Text(
                                  cartModel.paymentMethod!.cards![
                                          widget.paymentDropdownValue - 1]
                                      ['CardNoMask'],
                                  style: TextStyle(color: HexColor('#999999')),
                                )
                            ],
                          ),
                          Icon(
                            Icons.keyboard_arrow_right,
                            color: Theme.of(context).primaryColor,
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(
                      bottom: 16,
                      left: 8,
                      right: 8,
                    ),
                    color: Colors.white,
                    child: Column(
                      children: [
                        const SizedBox(height: 8),
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                '商品總金額',
                                style: TextStyle(color: HexColor('#999999')),
                              ),
                            ),
                            Text(
                              PriceTools.getCurrencyFormatted(
                                      (cartModel.getSubTotal()!).round(),
                                      null) ??
                                  '',
                              style: TextStyle(
                                color: HexColor('#999999'),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 8),
                        if (cartModel.getCouponCost() != 0) ...[
                          Row(
                            children: [
                              Expanded(
                                child: Text(
                                  '折扣金額',
                                  style: TextStyle(color: HexColor('#999999')),
                                ),
                              ),
                              Text(
                                '-' +
                                    (PriceTools.getCurrencyFormatted(
                                            cartModel.getCouponCost(), null) ??
                                        ''),
                                style: TextStyle(
                                  color: HexColor('#999999'),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 8),
                        ],
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                '運費總金額',
                                style: TextStyle(color: HexColor('#999999')),
                              ),
                            ),
                            Text(
                              PriceTools.getCurrencyFormatted(
                                      cartModel.getShippingCost()?.round(),
                                      null) ??
                                  '',
                              style: TextStyle(
                                color: HexColor('#999999'),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 16),
                        Container(
                          color: Theme.of(context).primaryColor,
                          height: 1,
                          width: double.maxFinite,
                        ),
                        const SizedBox(height: 16),
                        Row(
                          children: [
                            const Expanded(
                              child: Text('總付款金額'),
                            ),
                            Text(
                              PriceTools.getCurrencyFormatted(
                                      cartModel.getTotal()?.round(), null) ??
                                  '',
                              style: TextStyle(
                                  color: Theme.of(context).primaryColor),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 100),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }

  List<Widget> getProducts(CartModel model, BuildContext context) {
    return model.productsInCart.keys.map(
      (key) {
        ///key調整
        var productId = Product.cleanProductID(key);
        var product = model.getProductById(key);
        if (product?.bundledBy != null) {
          return const SizedBox();
        }

        var bundleList = <Widget>[];
        model.productsInCart.forEach((key, value) {
          var p = model.getProductById(key);
          if (p?.bundledBy == product?.cartKey) {
            bundleList.add(ShoppingCartRow(
              addonsOptions: model.productAddonsOptionsInCart[key],
              product: p,
              variation: model.getProductVariationById(key),
              quantity: model.productsInCart[key],
              options: model.productsMetaDataInCart[key],
              onCheckout: true,
            ));
          }
        });

        var tickets = <Map>[];
        try {
          if (model.ticketMeta != null) {
            var meta = model.ticketMeta as Map;
            var elements = meta.values.toList().first as List<dynamic>;
            for (var element in elements) {
              tickets.add(element);
            }
          }
        } catch (_) {}

        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  ShoppingCartRow(
                    addonsOptions: model.productAddonsOptionsInCart[key],
                    product: product,
                    variation: model.getProductVariationById(key),
                    quantity: model.productsInCart[key],
                    options: model.productsMetaDataInCart[key],
                    onCheckout: true,
                  ),
                  ...bundleList,
                  for (var element in tickets) ticketWidget(element)
                ],
              ),
            ),
          ),
        );
      },
    ).toList();
  }

  List<Widget> getGifts(CartModel model, BuildContext context) {
    return List.generate(
      model.giftGroupList.length,
      (index) {
        if (model.giftGroupList[index].list.indexWhere((element) =>
                element.id == model.giftGroupList[index].chooseId) ==
            -1) return const SizedBox();
        final giftModel = model.giftGroupList[index].list.firstWhere(
            (element) => element.id == model.giftGroupList[index].chooseId);
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: LayoutBuilder(
                builder: (context, constraints) {
                  return Column(
                    children: [
                      Row(
                        key: ValueKey(giftModel.id),
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(
                                  width: constraints.maxWidth * 0.20,
                                  height: constraints.maxWidth * 0.25,
                                  child:
                                      ImageTools.image(url: giftModel.imageUrl),
                                ),
                                const SizedBox(width: 16.0),
                                Expanded(
                                  child: SingleChildScrollView(
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          giftModel.name,
                                          style: const TextStyle(
                                              color: Colors.black,
                                              fontSize: 18,
                                              fontWeight: FontWeight.w600),
                                          maxLines: 4,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        const SizedBox(height: 2),
                                        Row(
                                          children: [
                                            Expanded(
                                              child: RichText(
                                                text: TextSpan(
                                                  children: [
                                                    TextSpan(
                                                      text: '贈品',
                                                      style: TextStyle(
                                                          color:
                                                              Theme.of(context)
                                                                  .colorScheme
                                                                  .secondary,
                                                          fontSize: 12),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            const SizedBox(height: 10),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 8),
                                Text('x${giftModel.giftQTY}'),
                              ],
                            ),
                          ),
                          const SizedBox(width: 16.0),
                        ],
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
        );
      },
    );
  }

  List<Widget> getGiftsInCart(CartModel model, BuildContext context) {
    return List.generate(
      model.giftsInCart.length,
      (index) {
        final giftModel = model.giftsInCart[index];
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: LayoutBuilder(
                builder: (context, constraints) {
                  return Column(
                    children: [
                      Row(
                        key: ValueKey(giftModel.id),
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(
                                  width: constraints.maxWidth * 0.20,
                                  height: constraints.maxWidth * 0.25,
                                  child:
                                      ImageTools.image(url: giftModel.imageUrl),
                                ),
                                const SizedBox(width: 16.0),
                                Expanded(
                                  child: SingleChildScrollView(
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          giftModel.name,
                                          style: const TextStyle(
                                              color: Colors.black,
                                              fontSize: 18,
                                              fontWeight: FontWeight.w600),
                                          maxLines: 4,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        const SizedBox(height: 2),
                                        Row(
                                          children: [
                                            Expanded(
                                              child: RichText(
                                                text: TextSpan(
                                                  children: [
                                                    TextSpan(
                                                      text: '贈品',
                                                      style: TextStyle(
                                                          color:
                                                              Theme.of(context)
                                                                  .colorScheme
                                                                  .secondary,
                                                          fontSize: 12),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            const SizedBox(height: 10),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 8),
                                Text('x${giftModel.giftQTY}'),
                              ],
                            ),
                          ),
                          const SizedBox(width: 16.0),
                        ],
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
        );
      },
    );
  }

  List<Widget> getGiftsCoupon(CartModel model, BuildContext context) {
    if (model.discount == null) return [];
    return List.generate(
      model.discount!.gifts.length,
      (index) {
        final giftModel = model.discount!.gifts[index];
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: LayoutBuilder(
                builder: (context, constraints) {
                  return Column(
                    children: [
                      Row(
                        key: ValueKey(giftModel.id),
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(
                                  width: constraints.maxWidth * 0.20,
                                  height: constraints.maxWidth * 0.25,
                                  child: ImageTools.image(url: giftModel.image),
                                ),
                                const SizedBox(width: 16.0),
                                Expanded(
                                  child: SingleChildScrollView(
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          giftModel.name,
                                          style: const TextStyle(
                                              color: Colors.black,
                                              fontSize: 18,
                                              fontWeight: FontWeight.w600),
                                          maxLines: 4,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        const SizedBox(height: 2),
                                        Row(
                                          children: [
                                            Expanded(
                                              child: RichText(
                                                text: TextSpan(
                                                  children: [
                                                    TextSpan(
                                                      text: '贈品',
                                                      style: TextStyle(
                                                          color:
                                                              Theme.of(context)
                                                                  .colorScheme
                                                                  .secondary,
                                                          fontSize: 12),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            const SizedBox(height: 10),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 8),
                                Text('x${giftModel.quantity}'),
                              ],
                            ),
                          ),
                          const SizedBox(width: 16.0),
                        ],
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
        );
      },
    );
  }

  Widget ticketWidget(Map data) {
    return Container(
      width: double.maxFinite,
      margin: const EdgeInsets.only(
        top: 4,
        left: 60,
      ),
      child: DottedBorder(
        padding: const EdgeInsets.all(8),
        radius: const Radius.circular(5),
        borderType: BorderType.RRect,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            for (var element in data.keys) Text('$element:${data[element]}'),
          ],
        ),
      ),
    );
  }
}
