import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:lanfar/screens/checkout/purchase_method_screen.dart';
import 'package:provider/provider.dart';

import '../../../app.dart';
import '../../../common/config.dart';
import '../../../common/constants.dart';
import '../../../common/tools.dart';
import '../../../generated/l10n.dart';
import '../../../models/index.dart'
    show AppModel, CartModel, Coupons, Discount, UserModel;
import '../../../services/index.dart';
import '../../checkout/purchase_method_widget.dart';
import 'coupon_list.dart';
import 'point_reward.dart';

class ShoppingCartSummary extends StatefulWidget {
  final Function(Discount) onApplySuccess;
  final Function(bool) setLoading;

  const ShoppingCartSummary(this.onApplySuccess, this.setLoading);

  @override
  _ShoppingCartSummaryState createState() => _ShoppingCartSummaryState();
}

class _ShoppingCartSummaryState extends State<ShoppingCartSummary> {
  final services = Services();
  Coupons? coupons;

  String _productsInCartJson = '';
  final _debounceApplyCouponTag = 'debounceApplyCouponTag';
  Map<String, dynamic>? defaultCurrency = kAdvanceConfig['DefaultCurrency'];

  CartModel get cartModel => Provider.of<CartModel>(context, listen: false);

  final couponController = TextEditingController();

  @override
  void initState() {
    super.initState();
    getCoupon();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      // if (cartModel.couponObj != null && cartModel.couponObj!.amount! > 0) {
      //   final savedCoupon = cartModel.savedCoupon;
      //   couponController.text = savedCoupon ?? '';
      // }
      final savedCoupon = cartModel.savedCoupon;
      // couponController.text = savedCoupon ?? '';
      _productsInCartJson = jsonEncode(cartModel.productsInCart);
    });
  }

  /*
  *
  void _onProductInCartChange(CartModel cartModel) {
    // If app success a coupon before
    // Need to apply again when any change in cart
    EasyDebounce.debounce(
        _debounceApplyCouponTag, const Duration(milliseconds: 300), () {
      if (cartModel.productsInCart.isEmpty) {
        removeCoupon(cartModel);
        return;
      }
      final newData = jsonEncode(cartModel.productsInCart);
      if (_productsInCartJson != newData) {
        _productsInCartJson = newData;
        checkCoupon(couponController.text, cartModel);
      }
    });
  }
  */
  @override
  void dispose() {
    couponController.dispose();
    super.dispose();
  }

  Future<void> getCoupon() async {
    try {
      coupons = await services.api.getCoupons(userId: context.read<UserModel>().user?.username ?? '');
    } catch (e) {
      printLog('ShoppingCartSummary getCoupon error = ${e.toString()}');
    }
  }

  void showError(String message) {
    final snackBar = SnackBar(
      // content: Text(S.of(context).warning(message)),
      content:  Text(message),
      duration: const Duration(seconds: 3),
      action: SnackBarAction(
        label: S.of(context).close,
        onPressed: () {},
        textColor: Colors.white,
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  /// Check coupon code
  void checkCoupon(String couponCode, CartModel cartModel) {
    if (couponCode.isEmpty) {
      showError(S.of(context).pleaseFillCode);
      return;
    }

    cartModel.setLoadingDiscount();

    Services().widget.applyCoupon(
      context,
      coupons: coupons,
      code: couponCode,
      success: (Discount discount) async {
        await cartModel.updateDiscount(discount: discount);
        cartModel.setLoadedDiscount();
        widget.onApplySuccess.call(discount);
      },
      error: (String errMess) {
        if (cartModel.couponObj != null) {
          removeCoupon(cartModel);
        }
        cartModel.setLoadedDiscount();
        showError(errMess.replaceAll('警告:Exception:', ''));
      },
    );
  }

  Future<void> removeCoupon(CartModel cartModel) async {
    // await Services().widget.removeCoupon(context);
    cartModel.resetCoupon();
    cartModel.discountAmount = 0.0;
  }

  @override
  Widget build(BuildContext context) {
    final currency = Provider.of<AppModel>(context).currency;
    final currencyRate = Provider.of<AppModel>(context).currencyRate;
    final smallAmountStyle = TextStyle(color: Theme.of(context).colorScheme.secondary);
    final largeAmountStyle = TextStyle(color: Theme.of(context).colorScheme.secondary, fontSize: 16);
    // final formatter = NumberFormat.currency(
    //   locale: 'en',
    //   symbol: defaultCurrency!['symbol'],
    //   decimalDigits: defaultCurrency!['decimalDigits'],
    // );
    // final screenSize = MediaQuery.of(context).size;

    return Consumer<CartModel>(builder: (context, cartModel, child) {
      var couponMsg = '';
      var isApplyCouponSuccess = false;
      if (cartModel.couponObj != null) {
        isApplyCouponSuccess = true;
        // _onProductInCartChange(cartModel);
        couponController.text = cartModel.couponObj!.code ?? '';
        couponMsg = '優惠券使用成功！';
        // if (cartModel.couponObj!.discountType == 'percent') {
        //   couponMsg += ' ${cartModel.couponObj!.amount}%';
        // } else if (cartModel.couponObj!.discountType == 'free_gift') {
        // } else {
        //   couponMsg += ' - ${formatter.format(cartModel.couponObj!.amount)}';
        // }
      } else {
        couponController.clear();
      }
      if (cartModel.productsInCart.isEmpty) {
        return const SizedBox();
      }
      var enableCoupon = (kAdvanceConfig['EnableCouponCode'] ?? true) &&
          !cartModel.isWalletCart();
      final enablePointReward = !cartModel.isWalletCart();
      if (cartModel.giftGroupList.isNotEmpty ||
          cartModel.giftsInCart.isNotEmpty) {
        enableCoupon = false;
      }
      return SizedBox(
        width: double.maxFinite,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            if (enableCoupon && cartModel.ticketMeta == null)
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                        decoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.background,
                          border: Border.all(
                            width: 0.5,
                            color: Colors.black26,
                          ),
                        ),
                        child: GestureDetector(
                          onTap: (kAdvanceConfig['ShowCouponList'] ?? false)
                              ? () {
                                  Navigator.of(App.fluxStoreNavigatorKey.currentContext!).push(
                                    MaterialPageRoute(
                                      fullscreenDialog: true,
                                      builder: (context) => CouponList(
                                        isFromCart: true,
                                        coupons: coupons,
                                        onSelect: (String couponCode) {
                                          Future.delayed(const Duration(milliseconds: 0), () {
                                            couponController.text = couponCode;
                                            checkCoupon(couponController.text, cartModel);
                                          });
                                        },
                                      ),
                                      ),
                                    );
                                  }
                                : null,
                            child: AbsorbPointer(absorbing: (kAdvanceConfig['ShowCouponList'] ?? false),
                            child: TextField(
                              controller: couponController,
                              autocorrect: false,
                              enabled: !isApplyCouponSuccess && !cartModel.calculatingDiscount,
                              decoration: InputDecoration(
                                prefixIcon: (kAdvanceConfig['ShowCouponList'] ?? false)
                                    ? Icon(Icons.local_offer, color: Theme.of(context).primaryColor)
                                    : null,
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                labelText: '優惠券',
                                //hintStyle: TextStyle(color: _enable ? Colors.grey : Colors.black),
                                contentPadding: const EdgeInsets.all(2),
                              ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 10),
                      ElevatedButton(
                        style: ButtonStyle(
                            elevation: MaterialStateProperty.all(0),
                            padding: MaterialStateProperty.all<EdgeInsets>(
                                const EdgeInsets.all(12)),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.zero),
                            ),
                            backgroundColor:
                                MaterialStateProperty.all(primaryColor)),
                        onPressed: !cartModel.calculatingDiscount
                            ? () async {
                                if (!isApplyCouponSuccess) {
                                  checkCoupon(couponController.text, cartModel);
                                } else {
                                  widget.setLoading.call(true);
                                  final result = await Services()
                                      .api
                                      .removeCoupon(
                                          userId:
                                              context
                                                      .read<UserModel>()
                                                      .user
                                                      ?.username ??
                                                  '',
                                          coupon:
                                              cartModel.couponObj?.code ?? '');
                                  widget.setLoading.call(false);
                                  if (result?.code != 'fail') {
                                    await removeCoupon(cartModel);
                                    return;
                                  }

                                  ScaffoldMessenger.of(context)
                                      .clearSnackBars();
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(SnackBar(
                                    content: Text(result?.message ?? ''),
                                ));
                              }
                            }
                          : null,
                      child: Text(
                        cartModel.calculatingDiscount
                            ? S.of(context).loading
                            : !isApplyCouponSuccess
                                ? '使用優惠券'
                                : S.of(context).remove,
                        style: const TextStyle(color: Colors.white),
                      ),
                    )
                  ],
                ),
              ),
            if (isApplyCouponSuccess)
              Padding(
                padding: const EdgeInsets.only(
                  left: 40,
                  right: 40,
                  bottom: 15,
                ),
                child: Text(
                  couponMsg,
                  style: TextStyle(color: Theme.of(context).primaryColor),
                  textAlign: TextAlign.center,
                ),
              ),
            if (enablePointReward) const PointReward(),
            // GestureDetector(
            //   onTap: () {
            //     Navigator.of(App.fluxStoreNavigatorKey.currentContext!).push(
            //       MaterialPageRoute(
            //         builder: (context) => PurchaseMethodScreen(
            //           setStateClick: () {
            //             setState(() {});
            //           },
            //         ),
            //       ),
            //     );
            //   },
            //   child: Container(
            //     padding:
            //         const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
            //     color: Colors.white,
            //     child: Row(
            //       children: [
            //         Image.asset(
            //           'assets/icons/from_lanfar/Artboard – 20.png',
            //           color: Theme.of(context).primaryColor,
            //           height: 24,
            //           width: 24,
            //         ),
            //         const SizedBox(width: 8),
            //         const Expanded(
            //           child: Text(
            //             '訂購類別',
            //             style: TextStyle(
            //               color: Colors.black,
            //               fontSize: 16,
            //               fontWeight: FontWeight.bold,
            //             ),
            //           ),
            //         ),
            //         Column(
            //           children: [
            //             Text(
            //               cartModel.purchaseMethod != null
            //                   ? cartModel.purchaseMethod!.title
            //                   : '請選擇',
            //               style: TextStyle(
            //                 color: Colors.black,
            //                 fontSize: 16,
            //                 fontWeight: cartModel.purchaseMethod != null
            //                     ? FontWeight.bold
            //                     : null,
            //               ),
            //             ),
            //             if (cartModel.purchaseMethod == PurchaseType.standin)
            //               Text(
            //                 cartModel.invoiceType?.title ?? '',
            //                 style: TextStyle(
            //                   color: HexColor('#999999'),
            //                 ),
            //               ),
            //           ],
            //         ),
            //         Icon(
            //           Icons.keyboard_arrow_right,
            //           color: Theme.of(context).primaryColor,
            //         )
            //       ],
            //     ),
            //   ),
            // ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
              color: Colors.white,
              child: Row(
                children: [
                  Image.asset(
                    'assets/icons/from_lanfar/Artboard – 20.png',
                    color: Theme.of(context).primaryColor,
                    height: 24,
                    width: 24,
                  ),
                  const SizedBox(width: 8),
                  const Expanded(
                    child: Text(
                      '訂購類別',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            PurchaseMethodWidget(
              cartModel: cartModel,
              setStateClick: (PurchaseType purchaseMethod) async {
                await Services().widget.syncCartFromWebsite(cartModel.user?.cookie, cartModel, context);
              },
            ),
            const SizedBox(height: 1),
            Container(
              color: Colors.white,
              padding: const EdgeInsets.symmetric(
                horizontal: 8.0,
                vertical: 10.0,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Image.asset(
                        'assets/icons/from_lanfar/Artboard – 3.png',
                        color: Theme.of(context).primaryColor,
                        height: 24,
                        width: 24,
                      ),
                      const SizedBox(width: 8),
                      const Expanded(
                        child: Text(
                          '購物車總計',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                  // Row(
                  //   children: [
                  //     Expanded(
                    //       child: Text(
                    //         S.of(context).products,
                    //         style: smallAmountStyle,
                    //       ),
                    //     ),
                    //     Text(
                    //       'x${cartModel.totalCartQuantity}',
                    //       style: smallAmountStyle,
                    //     ),
                    //   ],
                    // ),
                    if (cartModel.rewardTotal > 0) ...[
                      const SizedBox(height: 12),
                      Row(
                        children: [
                          Expanded(
                            child: Text(S.of(context).cartDiscount,
                                style: smallAmountStyle),
                          ),
                          Text(
                            PriceTools.getCurrencyFormatted(
                                cartModel.rewardTotal, currencyRate,
                                currency: currency)!,
                            style: smallAmountStyle,
                          ),
                        ],
                      ),
                    ],
                    const SizedBox(height: 10),
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            '訂單金額',
                            style: largeAmountStyle,
                          ),
                        ),
                        cartModel.calculatingDiscount
                            ? const SizedBox(
                                width: 20,
                                height: 20,
                                child: CircularProgressIndicator(
                                  strokeWidth: 2.0,
                                ),
                              )
                            : Text(
                                PriceTools.getCurrencyFormatted(
                                    cartModel.getSubTotal()! ,
                                    currencyRate,
                                    currency: cartModel.isWalletCart()
                                        ? defaultCurrency!['currencyCode']
                                        : currency)!,
                                style: largeAmountStyle,
                              ),
                      ],
                    ),
                    if(cartModel.getCouponCost() != 0)
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            '折扣金額',
                            style: largeAmountStyle,
                          ),
                        ),
                        cartModel.calculatingDiscount
                            ? const SizedBox(
                          width: 20,
                          height: 20,
                          child: CircularProgressIndicator(
                            strokeWidth: 2.0,
                          ),
                        )
                            : Text( '-' +
                          PriceTools.getCurrencyFormatted(
                              cartModel.getCouponCost(),
                              currencyRate,
                              currency: cartModel.isWalletCart()
                                  ? defaultCurrency!['currencyCode']
                                  : currency)!,
                          style: largeAmountStyle,
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            'PV分數',
                            style: largeAmountStyle,
                          ),
                        ),
                        cartModel.calculatingDiscount
                            ? const SizedBox(
                                width: 20,
                                height: 20,
                                child: CircularProgressIndicator(
                                  strokeWidth: 2.0,
                                ),
                              )
                            : Text(
                                PriceTools.getCurrencyFormatted(
                                    cartModel.getTotalPV(), currencyRate,
                                    isPV: true,
                                    currency: cartModel.isWalletCart()
                                        ? defaultCurrency!['currencyCode']
                                        : currency)!,
                                style: largeAmountStyle,
                              ),
                        Text(
                          ' PV',
                          style: largeAmountStyle,
                        ),
                      ],
                    ),
                    const SizedBox(height: 14),
                    Container(
                      height: 1,
                      width: double.maxFinite,
                      color: Theme.of(context).primaryColor,
                    ),
                    const SizedBox(height: 14),
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            '總金額',
                            style: largeAmountStyle.copyWith(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                              color: Colors.black,
                            ),
                          ),
                        ),
                        cartModel.calculatingDiscount
                            ? const SizedBox(
                                width: 20,
                                height: 20,
                                child: CircularProgressIndicator(
                                  strokeWidth: 2.0,
                                ),
                              )
                            : Text(
                          PriceTools.getCurrencyFormatted(
                                  cartModel.getTotal()! - cartModel.getShippingCost()!, currencyRate,
                                  currency: cartModel.isWalletCart() ? defaultCurrency!['currencyCode'] : currency)!,
                              style: largeAmountStyle.copyWith(
                                color: Theme.of(context).primaryColor,
                                fontSize: 18,
                              ),
                            ),
                      ],
                    ),
                  ],
                ),
              ),
              Services().widget.renderRecurringTotals(context)
            ],
          ),

      );
    });
  }
}
