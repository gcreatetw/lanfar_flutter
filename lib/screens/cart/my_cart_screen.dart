import 'dart:async';

import 'package:extended_image/extended_image.dart';
import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:inspireui/inspireui.dart' show AutoHideKeyboard, printLog;
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../from_lanfar/screens/qrcode_page.dart';
import '../../generated/l10n.dart';
import '../../menu/index.dart' show MainTabControlDelegate;
import '../../models/cart/mixin/cart_mixin.dart';
import '../../models/entities/user.dart';
import '../../models/index.dart'
    show AppModel, CartModel, CouponGift, Product, UserModel;
import '../../models/user_model.dart';
import '../../routes/flux_navigate.dart';
import '../../services/index.dart';
import '../../widgets/product/cart_item.dart';
import '../../widgets/product/product_bottom_sheet.dart';
import '../checkout/checkout_screen.dart';
import '../checkout/purchase_method_screen.dart';
import 'widgets/empty_cart.dart';
import 'widgets/shopping_cart_sumary.dart';

class MyCart extends StatefulWidget {
  final bool? isModal;
  final bool? isBuyNow;

  const MyCart({
    this.isModal,
    this.isBuyNow = false,
  });

  @override
  _MyCartState createState() => _MyCartState();
}

///load cart everytime click cart
class _MyCartState extends State<MyCart> with SingleTickerProviderStateMixin implements UserModelDelegate {
  bool isLoading = false;
  bool reloadCart = false;
  bool pageLoading = false;
  String errMsg = '';

  ///快速晉升
  bool showFastPromote = false;
  bool fastPromote = false;

  ///購物車數量更改
  bool qtyChanging = false;
  Timer? changingTimer;

  final RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async {
    _refreshController.refreshCompleted();
    setState(() {
      reloadCart = true;
    });
    await onLoaded(cartModel.user);
    setState(() {
      reloadCart = false;
    });
  }

  CartModel get cartModel => Provider.of<CartModel>(context, listen: false);

  List<Widget> _createShoppingCartRows(CartModel model, BuildContext context) {
    return model.productsInCart.keys.map(
      (key) {
        ///key調整
        var productId = Product.cleanProductID(key);
        var product = model.getProductById(key);

        return ShoppingCartRow(
          product: product!,
          addonsOptions: model.productAddonsOptionsInCart[key],
          variation: model.getProductVariationById(key),
          quantity: model.productsInCart[key],
          options: model.productsMetaDataInCart[key],
          onRemove: () async {
            model.removeItemFromCart(key);
            if (model.productsInCart.isEmpty) {
              unawaited(Services().widget.syncCartToWebsite(cartModel)!);
              return;
            }
            setState(() {
              reloadCart = true;
            });
            await Services().widget.syncCartToWebsite(cartModel);
            await onLoaded(cartModel.user);
            setState(() {
              reloadCart = false;
            });
          },
          onChangeQuantity: (val) async {
            setState(() {
              qtyChanging = true;
            });
            var message = model.updateQuantity(product, key, val, context: context);
            changingTimer?.cancel();

            ///購物車數量更改
            changingTimer = Timer(
              const Duration(seconds: 2),
              () async {
                if (message.isNotEmpty) {
                  final snackBar = SnackBar(
                    content: Text(message),
                    duration: const Duration(seconds: 1),
                  );
                  Future.delayed(
                    const Duration(milliseconds: 300),
                    // ignore: deprecated_member_use
                    () => ScaffoldMessenger.of(context).showSnackBar(snackBar),
                  );
                }
                setState(() {
                  reloadCart = true;
                });
                await Services().widget.syncCartToWebsite(model);
                await onLoaded(cartModel.user);
                setState(() {
                  qtyChanging = false;
                  reloadCart = false;
                });
              },
            );
          },
        );
      },
    ).toList();
  }

  void _loginWithResult(BuildContext context) async {
    // final result = await Navigator.push(
    //   context,
    //   MaterialPageRoute(
    //     builder: (context) => LoginScreen(
    //       fromCart: true,
    //     ),
    //     fullscreenDialog: kIsWeb,
    //   ),
    // );
    await FluxNavigate.pushNamed(
      RouteList.login,
      forceRootNavigator: true,
    ).then((value) {
      final user = Provider.of<UserModel>(context, listen: false).user;
      if (user != null && user.name != null) {
        Tools.showSnackBar(ScaffoldMessenger.of(context),
            S.of(context).welcome + ' ${user.name} !');
        setState(() {});
      }
    });
  }

  @override
  void initState() {
    Future.delayed(Duration.zero, () async {
      setState(() {
        pageLoading = true;
      });
      // await getFastPromote();
      cartModel.user = Provider.of<UserModel>(context, listen: false).user;
      cartModel.purchaseMethod = PurchaseType.general;

      await Future.wait([
        getFastPromote(),
        onLoaded(cartModel.user, firstCall: true),
        Provider.of<UserModel>(context, listen: false).getUser(runLoaded: false),
      ]);
      setState(() {
        pageLoading = false;
      });
    });
    super.initState();
  }

  Future<void> getFastPromote() async {
    showFastPromote =
        await Services().api.getUserStatus(cartModel.user!.username!) ?? false;
  }

  @override
  void dispose() {
    clearMemoryImageCache();
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ///load cart everytime click cart
    printLog('[My Cart Screen] build');

    var layoutType = Provider.of<AppModel>(context).productDetailLayout;
    final ModalRoute<dynamic>? parentRoute = ModalRoute.of(context);
    final canPop = parentRoute?.canPop ?? false;

    return Stack(
      children: [
        Scaffold(
          appBar: AppBar(
            leading: widget.isModal == true
                ? CloseButton(
                    onPressed: () {
                      if (widget.isBuyNow!) {
                        Navigator.of(context).pop();
                        return;
                      }

                      if (Navigator.of(context).canPop() &&
                          layoutType != 'simpleType') {
                        Navigator.of(context).pop();
                      } else {
                        ExpandingBottomSheet.of(context, isNullOk: true)
                            ?.close();
                      }
                    },
                  )
                : canPop
                    ? const BackButton()
                    : null,
            backgroundColor: Theme.of(context).backgroundColor,
            title: Text(
              S.of(context).myCart,
              style: Theme.of(context)
                  .textTheme
                  .headline6!
                  .copyWith(fontWeight: FontWeight.w700),
            ),
            actions: [
              IconButton(
                icon: Icon(
                  Icons.qr_code_2,
                  color: Theme.of(context).textTheme.headline6!.color,
                ),
                onPressed: () => FluxNavigate.push(
                  MaterialPageRoute(
                    builder: (context) => const QRCodePage(),
                  ),
                ),
              ),
              IconButton(
                icon: Icon(
                  Icons.menu,
                  color: Theme.of(context).textTheme.headline6!.color,
                ),
                onPressed: () => NavigateTools.onTapOpenDrawerMenu(context),
              ),
            ],
          ),
          floatingActionButton: pageLoading || cartModel.disable
              ? null
              : Selector<CartModel, bool>(
                  selector: (_, cartModel) => cartModel.calculatingDiscount,
                  builder: (context, calculatingDiscount, child) {
                    return FloatingActionButton.extended(
                      onPressed: calculatingDiscount || qtyChanging
                          ? null
                          : () {
                              if (kAdvanceConfig['AlwaysShowTabBar'] ?? false) {
                                MainTabControlDelegate.getInstance()
                                    .changeTab('cart');
                                // return;
                              }
                              onCheckout(cartModel);
                            },
                      isExtended: true,
                      backgroundColor: qtyChanging
                          ? Colors.grey
                          : Theme.of(context).primaryColor,
                      foregroundColor: Colors.white,
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      icon: const Icon(Icons.payment, size: 20),
                      label: child!,
                    );
                  },
                  child: Selector<CartModel, int>(
                    selector: (_, carModel) => cartModel.totalCartQuantity,
                    builder: (context, totalCartQuantity, child) {
                      return totalCartQuantity > 0
                          ? (isLoading
                              ? Text(S.of(context).loading.toUpperCase())
                              : const Text('前往結帳'))
                          : Text(S.of(context).startShopping.toUpperCase());
                    },
                  ),
                ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerFloat,
          body: SmartRefresher(
            enablePullDown: true,
            controller: _refreshController,
            onRefresh: _onRefresh,
            header: const ClassicHeader(
              idleText: '下拉更新',
              releaseText: '放開更新',
              refreshingText: '更新中',
              completeText: '已更新',
            ),
            child: pageLoading
                ? Center(
                    child: kLoadingWidget(context),
                  )
                : CustomScrollView(
                    slivers: [
                      // SliverAppBar(
                      //   pinned: true,
                      //   centerTitle: false,
                      //   leading: widget.isModal == true
                      //       ? CloseButton(
                      //           onPressed: () {
                      //             if (widget.isBuyNow!) {
                      //               Navigator.of(context).pop();
                      //               return;
                      //             }
                      //
                      //             if (Navigator.of(context).canPop() &&
                      //                 layoutType != 'simpleType') {
                      //               Navigator.of(context).pop();
                      //             } else {
                      //               ExpandingBottomSheet.of(context, isNullOk: true)
                      //                   ?.close();
                      //             }
                      //           },
                      //         )
                      //       : canPop
                      //           ? const BackButton()
                      //           : null,
                      //   backgroundColor: Theme.of(context).backgroundColor,
                      //   title: Text(
                      //     S.of(context).myCart,
                      //     style: Theme.of(context)
                      //         .textTheme
                      //         .headline6!
                      //         .copyWith(fontWeight: FontWeight.w700),
                      //   ),
                      // ),
                      SliverToBoxAdapter(
                        child: Consumer<CartModel>(
                          builder: (context, model, child) {
                            return AutoHideKeyboard(
                              child: SingleChildScrollView(
                                child: Column(
                                  children: [
                                    if (model.notifyText != null && model.notifyText!.isNotEmpty)
                                      Html(
                                        data: model.notifyText,
                                        onLinkTap: (url, c, _, __) {
                                          if (url != null) {
                                            launch(url, forceSafariVC: false);
                                          }
                                        },
                                        // onImageTap: (url, c, _, __) {
                                        //   if (url != null) {
                                        //     launch(url, forceSafariVC: false);
                                        //   }
                                        // },
                                      ),
                                    _cartBody(model),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      )
                    ],
                  ),
          ),
        ),
        if (reloadCart)
          Container(
            width: double.maxFinite,
            height: double.maxFinite,
            color: Colors.black54,
            alignment: Alignment.center,
            child: kLoadingWidget(context),
          ),
      ],
    );
  }

  Widget _cartBody(CartModel model) {
    if (model.disable) {
      return Html(data: model.disableMessage);
    }
    return Column(
      children: [
        ///數量 清空購物車
        if (model.totalCartQuantity > 0) ...clearCart(model),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const SizedBox(height: 16.0),
            if (model.totalCartQuantity > 0) ...[
              productList(model),
              const SizedBox(height: 16),
            ],

            ///快速晉升
            if (model.totalCartQuantity > 0 && showFastPromote && cartModel.getTotalPV()! >= 60000) ...[
              const SizedBox(height: 16),
              InkWell(
                onTap: () async {
                  setState(() {
                    fastPromote = !fastPromote;
                    reloadCart = true;
                  });
                  await onLoaded(model.user);
                  setState(() {
                    reloadCart = false;
                  });
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 16,
                    vertical: 10,
                  ),
                  child: Row(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: 16,
                        height: 16,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(width: 0.5),
                          borderRadius: BorderRadius.circular(2),
                        ),
                        child: Checkbox(
                          value: fastPromote,
                          onChanged: (value) async {
                            setState(() {
                              fastPromote = value!;
                              reloadCart = true;
                            });
                            await onLoaded(model.user);
                            setState(() {
                              reloadCart = false;
                            });
                          },
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(2),
                          ),
                          checkColor: Theme.of(context).primaryColor,
                          fillColor:
                              MaterialStateProperty.all(Colors.transparent),
                          side: const BorderSide(
                              width: 0.5, color: Colors.transparent),
                        ),
                      ),
                      const SizedBox(width: 8),
                      const Text('此訂單做'),
                      const Text(
                        '「快速晉升訂單」',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
            ] else if (model.totalCartQuantity > 0 && showFastPromote && cartModel.getTotalPV()! < 60000)
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                child: Text('快速晉升訂單條件必需符合60000PV以上。'),
              ),

            ///贈品
            if (cartModel.couponObj == null) giftGroups(cartModel),

            ShoppingCartSummary(
              (discount) {
                setState(() {
                  cartModel.discount = discount;
                  if (cartModel.invoiceType == InvoiceType.separate) {
                    cartModel.invoiceType = InvoiceType.noseparate;
                  }
                });
                if (cartModel.discount?.gifts != null &&
                    cartModel.discount!.gifts.isNotEmpty &&
                    cartModel.invoiceType == InvoiceType.separate) {
                  cartModel.invoiceType = null;
                  cartModel.purchaseMethod = null;
                }

                ///特殊折價權不能用代購
                if (discount.coupon!.code!.contains('LANFAR')) {
                  cartModel.invoiceType = null;
                  cartModel.purchaseMethod = PurchaseType.general;
                }
              },
              (loading) {
                setState(() {
                  reloadCart = loading;
                });
              },
            ),
            if (model.totalCartQuantity == 0) EmptyCart(),
            if (errMsg.isNotEmpty)
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 15,
                  vertical: 10,
                ),
                child: Text(
                  errMsg,
                  style: const TextStyle(color: Colors.red),
                  textAlign: TextAlign.center,
                ),
              ),
            // WishList()
          ],
        ),
        if (model.totalCartQuantity > 0)
          Container(
            height: 120,
            color: Colors.white,
          ),
      ],
    );
  }

  Widget productList(CartModel model) {
    return Card(
      elevation: 2,
      color: Colors.white,
      margin: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: _createShoppingCartRows(model, context).length,
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 8),
                child: _createShoppingCartRows(model, context)[index],
              );
            },
            separatorBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Row(
                  children: List.generate(
                    150 ~/ 1,
                    (index) => Expanded(
                      child: Container(
                        color: index % 2 == 0
                            ? Colors.transparent
                            : Colors.black12,
                        height: 1,
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
          for (var element in model.giftsInCart) ...[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Row(
                children: List.generate(
                  150 ~/ 1,
                  (index) => Expanded(
                    child: Container(
                      color:
                          index % 2 == 0 ? Colors.transparent : Colors.black12,
                      height: 1,
                    ),
                  ),
                ),
              ),
            ),
            LayoutBuilder(
              builder: (context, constraints) {
                return Row(
                  children: [
                    const IconButton(
                      icon: Icon(
                        Icons.clear,
                        color: Colors.transparent,
                      ),
                      onPressed: null,
                    ),
                    SizedBox(
                      width: constraints.maxWidth * 0.20,
                      height: constraints.maxWidth * 0.25,
                      child: ImageTools.image(url: element.imageUrl),
                    ),
                    const SizedBox(width: 16.0),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(height: 8.0),
                          Text(
                            element.name,
                            style: const TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                            ),
                            maxLines: 4,
                            overflow: TextOverflow.ellipsis,
                          ),
                          const SizedBox(height: 8),
                          Row(
                            children: [
                              const SizedBox(width: 26),
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(0),
                                  border: Border.all(
                                    width: 1.0,
                                    color: kGrey200,
                                  ),
                                ),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Container(
                                      height: 30,
                                      width: 35,
                                      alignment: Alignment.center,
                                      child: Text(
                                        element.giftQTY.toString(),
                                        style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .secondary),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(width: 26),
                            ],
                          ),
                          const SizedBox(height: 8.0),
                        ],
                      ),
                    ),
                    const SizedBox(width: 16.0),
                  ],
                );
              },
            ),
          ],
          for (CouponGift element in cartModel.discount?.gifts ?? []) ...[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Row(
                children: List.generate(
                  150 ~/ 1,
                  (index) => Expanded(
                    child: Container(
                      color:
                          index % 2 == 0 ? Colors.transparent : Colors.black12,
                      height: 1,
                    ),
                  ),
                ),
              ),
            ),
            LayoutBuilder(
              builder: (context, constraints) {
                return Row(
                  children: [
                    const IconButton(
                      icon: Icon(
                        Icons.clear,
                        color: Colors.transparent,
                      ),
                      onPressed: null,
                    ),
                    SizedBox(
                      width: constraints.maxWidth * 0.20,
                      height: constraints.maxWidth * 0.25,
                      child: ImageTools.image(url: element.image),
                    ),
                    const SizedBox(width: 16.0),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(height: 8.0),
                          Text(
                            element.name,
                            style: const TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                            ),
                            maxLines: 4,
                            overflow: TextOverflow.ellipsis,
                          ),
                          const SizedBox(height: 8),
                          Row(
                            children: [
                              const SizedBox(width: 26),
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(0),
                                  border: Border.all(
                                    width: 1.0,
                                    color: kGrey200,
                                  ),
                                ),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Container(
                                      height: 30,
                                      width: 35,
                                      alignment: Alignment.center,
                                      child: Text(
                                        element.quantity.toString(),
                                        style: TextStyle(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .secondary),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(width: 26),
                            ],
                          ),
                          const SizedBox(height: 8.0),
                        ],
                      ),
                    ),
                    const SizedBox(width: 16.0),
                  ],
                );
              },
            ),
          ]
        ],
      ),
    );
  }

  ///贈品
  Widget giftGroups(CartModel cartModel) {
    return ListView.builder(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: cartModel.giftGroupList.length,
        itemBuilder: (context, index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ///贈品說明
              if (cartModel.giftGroupList[index].list.isNotEmpty &&
                  cartModel.totalCartQuantity > 0) ...[
                const SizedBox(height: 16),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Row(
                    children: [
                      Container(
                        width: 4,
                        height: 50,
                        color: Theme.of(context).primaryColor,
                      ),
                      const SizedBox(width: 8),
                      Expanded(
                        child: RichText(
                          text: TextSpan(
                            children: [
                              const TextSpan(text: '您的購物車因符合贈品條件'),
                              TextSpan(
                                text:
                                    '【${cartModel.giftGroupList[index].name}】',
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor),
                              ),
                              const TextSpan(text: '，因此可從'),
                              for (int i = 0;
                                  i <
                                      cartModel
                                          .giftGroupList[index].list.length;
                                  i++)
                                TextSpan(
                                  text: cartModel
                                          .giftGroupList[index].list[i].name +
                                      (i <
                                              cartModel.giftGroupList[index]
                                                      .list.length -
                                                  1
                                          ? '、'
                                          : ''),
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColor),
                                ),
                              const TextSpan(text: '中選擇'),
                              // TextSpan(
                              //   text: '4個',
                              //   style: TextStyle(
                              //       color: Theme.of(context).primaryColor),
                              // ),
                              const TextSpan(text: '做為贈品。'),
                            ],
                            style: const TextStyle(color: Colors.black87),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 20),

                ///贈品列
                if (cartModel.giftGroupList[index].list.isNotEmpty &&
                    cartModel.totalCartQuantity > 0)
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          '請選擇贈品',
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(height: 10),
                        SizedBox(
                          height: 180,
                          child: ListView.separated(
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            itemCount: cartModel.giftGroupList[index].list.length,
                            itemBuilder: (context, i) {
                              return GiftCard(
                                onTap: () {
                                  if (cartModel.giftGroupList[index].chooseId == cartModel.giftGroupList[index].list[i].id) {
                                    setState(() {
                                      cartModel.giftGroupList[index].chooseId = null;
                                    });
                                  } else {
                                    setState(() {
                                      if (cartModel.invoiceType == InvoiceType.separate) {
                                        cartModel.purchaseMethod = null;
                                      }
                                      cartModel.giftGroupList[index].chooseId = cartModel.giftGroupList[index].list[i].id;
                                    });
                                  }
                                },
                                model: cartModel.giftGroupList[index].list[i],
                                chooseId: cartModel.giftGroupList[index].chooseId,
                              );
                            },
                            separatorBuilder: (BuildContext context, int i) {
                              return const SizedBox(width: 14);
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
              ],
            ],
          );
        });
  }

  void onCheckout(CartModel model) {
    var isLoggedIn = Provider.of<UserModel>(context, listen: false).loggedIn;
    final currencyRate = Provider.of<AppModel>(context, listen: false).currencyRate;
    final currency = Provider.of<AppModel>(context, listen: false).currency;
    var message;

    if (isLoading) return;

    if (kCartDetail['minAllowTotalCartValue'] != null) {
      if (kCartDetail['minAllowTotalCartValue'].toString().isNotEmpty) {
        var totalValue = model.getSubTotal() ?? 0;
        var minValue = PriceTools.getCurrencyFormatted(kCartDetail['minAllowTotalCartValue'], currencyRate, currency: currency);
        if (totalValue < kCartDetail['minAllowTotalCartValue'] && model.totalCartQuantity > 0) {
          message = '${S.of(context).totalCartValue} $minValue';
        }
      }
    }

    if ((kVendorConfig['DisableMultiVendorCheckout'] ?? false) && Config().isVendorType()) {
      if (!model.isDisableMultiVendorCheckoutValid(model.productsInCart, model.getProductById)) {
        message = S.of(context).youCanOnlyOrderSingleStore;
      }
    }

    if (message != null) {
      showFlash(
        context: context,
        duration: const Duration(seconds: 3),
        persistent: !Config().isBuilder,
        builder: (context, controller) {
          return SafeArea(
            child: Flash(
              borderRadius: BorderRadius.circular(3.0),
              backgroundColor: Theme.of(context).errorColor,
              controller: controller,
              behavior: FlashBehavior.fixed,
              position: FlashPosition.top,
              horizontalDismissDirection: HorizontalDismissDirection.horizontal,
              child: FlashBar(
                icon: const Icon(
                  Icons.check,
                  color: Colors.white,
                ),
                content: Text(
                  message,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ),
          );
        },
      );

      return;
    }

    if (model.totalCartQuantity == 0) {
      if (widget.isModal == true) {
        try {
          ExpandingBottomSheet.of(context)!.close();
        } catch (e) {
          Navigator.of(context).pop();
          // Navigator.of(context).pushNamed(RouteList.dashboard);
        }
      } else {
        final modalRoute = ModalRoute.of(context);
        if (modalRoute?.canPop ?? false) {
          Navigator.of(context).pop();
          return;
        }
        // MainTabControlDelegate.getInstance().tabAnimateTo(0);
        pushNavigation(RouteList.products);
      }
    } else if (isLoggedIn || kPaymentConfig['GuestCheckout'] == true) {
      if (cartModel.purchaseMethod == null) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: const Text('請選擇訂購類別'),
            action: SnackBarAction(
              label: S.of(context).close,
              onPressed: () {
                // Some code to undo the change.
              },
              textColor: Colors.white,
            ),
          ),
        );
        return;
      }
      // for (var element in cartModel.giftGroupList) {
      //   if (element.chooseId == null) {
      //     ScaffoldMessenger.of(context).showSnackBar(
      //       SnackBar(
      //         content: const Text('請選擇贈品'),
      //         action: SnackBarAction(
      //           label: S.of(context).close,
      //           onPressed: () {
      //             // Some code to undo the change.
      //           },
      //         ),
      //       ),
      //     );
      //     return;
      //   }
      // }
      cartModel.fastPromote = fastPromote;
      doCheckout();
    } else {
      _loginWithResult(context);
    }
  }

  Future<void> doCheckout() async {
    showLoading();

    await Services().widget.doCheckout(
      context,
      success: () async {
        hideLoading('');
        await FluxNavigate.pushNamed(
          RouteList.checkout,
          arguments: CheckoutArgument(
            isModal: widget.isModal,
            onRefresh: () {
              onLoaded(cartModel.user);
            },
          ),
          forceRootNavigator: true,
        );
      },
      error: (message) async {
        if (message == Exception('Token expired. Please logout then login again').toString()) {
          setState(() {
            isLoading = false;
          });
          //logout
          final userModel = Provider.of<UserModel>(context, listen: false);
          await userModel.logout();
          Services().firebase.signOut();

          _loginWithResult(context);
        } else {
          hideLoading(message);
          Future.delayed(const Duration(seconds: 3), () {
            setState(() => errMsg = '');
          });
        }
      },
      loading: (isLoading) {
        setState(() {
          this.isLoading = isLoading;
        });
      },
    );
  }

  void showLoading() {
    setState(() {
      isLoading = true;
      errMsg = '';
    });
  }

  void hideLoading(error) {
    setState(() {
      isLoading = false;
      errMsg = error;
    });
  }

  ///load cart everytime click cart
  @override
  Future<void> onLoaded(User? user, {bool firstCall = false}) async {
    if (user?.cookie != null && (kAdvanceConfig['EnableSyncCartFromWebsite'] ?? true)) {
      if (!reloadCart) {
        setState(() {
          pageLoading = true;
        });
      }

      if (showFastPromote || firstCall) {
        var result = await Services().api.setConfirmSpeed(userId: user?.username ?? '', check: fastPromote ? 1 : 0);
        if (result == null || result['error'] != 0) {
          fastPromote = !fastPromote;
        }
      }

      await Services().widget.syncCartFromWebsite(user?.cookie, cartModel, context);
      // if (cartModel.paymentMethod == null) {
      //   printLog('ben my cart screen cartModel.paymentMethod == null ');
      //   cartModel.purchaseMethod = PurchaseType.general;
      // }
      cartModel.invoiceType = null;
      setState(() {
        pageLoading = false;
      });

      if (cartModel.ticketMeta != null) {
        await doCheckout();
      }
    }
  }

  @override
  void onLoggedIn(User user) {
    // TODO: implement onLoggedIn
  }

  @override
  void onLogout(User? user) {
    // TODO: implement onLogout
  }

  void pushNavigation(String name) {
    eventBus.fire(const EventCloseNativeDrawer());
    MainTabControlDelegate.getInstance().changeTab(name.replaceFirst('/', ''));
  }

  List<Widget> clearCart(CartModel model) {
    final localTheme = Theme.of(context);
    final screenSize = MediaQuery.of(context).size;
    return [
      Container(
        // decoration: BoxDecoration(
        //     color: Theme.of(context).primaryColorLight),
        padding: const EdgeInsets.only(
          right: 15.0,
          top: 4.0,
        ),
        child: SizedBox(
          width: screenSize.width,
          child: SizedBox(
            width:
                screenSize.width / (2 / (screenSize.height / screenSize.width)),
            child: Row(
              children: [
                const SizedBox(width: 25.0),
                Text(
                  S.of(context).total.toUpperCase(),
                  style: localTheme.textTheme.subtitle1!.copyWith(
                    fontWeight: FontWeight.w600,
                    color: Theme.of(context).primaryColor,
                    fontSize: 16,
                  ),
                ),
                const SizedBox(width: 8.0),
                Text(
                  '${model.totalCartQuantity} ${S.of(context).items}',
                  style: TextStyle(color: Theme.of(context).primaryColor),
                ),
                Expanded(
                  child: Align(
                    alignment: Tools.isRTL(context)
                        ? Alignment.centerLeft
                        : Alignment.centerRight,
                    child: TextButton(
                      onPressed: () {
                        if (model.totalCartQuantity > 0) {
                          showDialog(
                            context: context,
                            useRootNavigator: false,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                content: Text(S.of(context).confirmClearTheCart),
                                actions: [
                                  TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: const Text('取消'),
                                  ),
                                  ElevatedButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                      model.clearCart();
                                    },
                                    child: Text(
                                      S.of(context).clear,
                                      style: const TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                              );
                            },
                          );
                        }
                      },
                      child: Text(
                        S.of(context).clearCart.toUpperCase(),
                        style: const TextStyle(
                          color: Colors.redAccent,
                          fontSize: 14,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      const Divider(
        height: 1,
        // indent: 25,
      ),
    ];
  }
}

class GiftCard extends StatefulWidget {
  final int? chooseId;
  final GiftModel model;
  final VoidCallback onTap;

  const GiftCard({
    Key? key,
    required this.model,
    required this.onTap,
    required this.chooseId,
  }) : super(key: key);

  @override
  State<GiftCard> createState() => _GiftCardState();
}

class _GiftCardState extends State<GiftCard> {
  int amount = 1;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: widget.onTap,
          child: Container(
            width: 140,
            decoration: BoxDecoration(
              border: Border.all(
                  color: widget.model.id == widget.chooseId
                      ? primaryColor
                      : Colors.transparent),
              color: Colors.white,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(height: 5),
                Image.network(
                  widget.model.imageUrl,
                  width: 113,
                  height: 113,
                ),
                const SizedBox(height: 7),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: Text(
                    widget.model.name + ' X ${widget.model.giftQTY}',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: widget.model.id == widget.chooseId
                            ? primaryColor
                            : null),
                  ),
                ),
                const SizedBox(height: 7),
                // Container(
                //   decoration: BoxDecoration(
                //     borderRadius: BorderRadius.circular(0),
                //     border: Border.all(
                //       width: 1.0,
                //       color: kGrey200,
                //     ),
                //   ),
                //   child: Row(
                //     mainAxisSize: MainAxisSize.min,
                //     children: [
                //       Expanded(
                //         child: InkWell(
                //           onTap: () {
                //             if (amount > 1) {
                //               setState(() {
                //                 amount--;
                //               });
                //             }
                //           },
                //           child: Container(
                //             alignment: Alignment.center,
                //             child: Text(
                //               '－',
                //               style: TextStyle(
                //                   color: Theme.of(context).colorScheme.secondary),
                //             ),
                //           ),
                //         ),
                //       ),
                //       Container(
                //         height: 30,
                //         width: 1,
                //         color: kGrey200,
                //       ),
                //       Expanded(
                //         child: Container(
                //           alignment: Alignment.center,
                //           child: Text(
                //             amount.toString(),
                //             style: TextStyle(
                //                 color: Theme.of(context).colorScheme.secondary),
                //           ),
                //         ),
                //       ),
                //       Container(
                //         height: 30,
                //         width: 1,
                //         color: kGrey200,
                //       ),
                //       Expanded(
                //         child: InkWell(
                //           onTap: () {
                //             if (amount <
                //                 int.parse(widget.model.pwNumberGiftAllowed)) {
                //               setState(() {
                //                 amount++;
                //               });
                //             }
                //           },
                //           child: Container(
                //             height: 30,
                //             width: 25,
                //             alignment: Alignment.center,
                //             child: Text(
                //               '＋',
                //               style: TextStyle(
                //                   color: Theme.of(context).colorScheme.secondary),
                //             ),
                //           ),
                //         ),
                //       ),
                //     ],
                //   ),
                // ),
              ],
            ),
          ),
        ),
        const SizedBox(height: 16),
        // InkWell(
        //   onTap: () {},
        //   child: Container(
        //     decoration: BoxDecoration(
        //       border: Border.all(color: Theme.of(context).primaryColor),
        //       color: Colors.white,
        //     ),
        //     padding: const EdgeInsets.symmetric(vertical: 8),
        //     alignment: Alignment.center,
        //     width: 123,
        //     child: Text(
        //       '加入贈品',
        //       style: TextStyle(color: Theme.of(context).primaryColor),
        //     ),
        //   ),
        // ),
      ],
    );
  }
}
