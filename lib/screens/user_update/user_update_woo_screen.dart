import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../common/constants.dart' show primaryColor, printLog;
import '../../common/tools.dart';
import '../../models/user_model.dart';
import '../../services/dependency_injection.dart';
import '../checkout/bill_screen.dart';
import 'user_update_model.dart';

class UserUpdateWooScreen extends StatefulWidget {
  @override
  _UserUpdateScreenState createState() => _UserUpdateScreenState();
}

class _UserUpdateScreenState extends State<UserUpdateWooScreen>
    with TickerProviderStateMixin {
  String? avatar;
  bool isLoading = false;
  bool hideFirst = true;
  bool hideSecond = true;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  late TabController controller;

  @override
  void initState() {
    controller = TabController(
      length: 2,
      vsync: this,
    );

    controller.addListener(() {
      setState(() {});
    });

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      var arguments =
          ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>?;
      if (arguments != null && arguments['changePassword']) {
        controller.animateTo(1);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserModel>(context, listen: false);
    return ChangeNotifierProvider<UserUpdateModel>(
      create: (_) => UserUpdateModel(user.user),
      lazy: false,
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Theme.of(context).backgroundColor,
        floatingActionButton: controller.index == 0
            ? null
            : Consumer<UserUpdateModel>(
                builder: (_, model, __) => ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Theme.of(context).primaryColor),
                  onPressed: model.state == UserUpdateState.loading
                      ? null
                      : () async {
                          if (model.newPassword.text.isEmpty ||
                              model.confirmPassword.text.isEmpty) {
                            ScaffoldMessenger.of(context)
                                .removeCurrentSnackBar();
                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(
                                content: Text('新密碼與確認新密碼請勿空白'),
                              ),
                            );
                            return;
                          }
                          if (model.newPassword.text !=
                              model.confirmPassword.text) {
                            ScaffoldMessenger.of(context)
                                .removeCurrentSnackBar();
                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(
                                content: Text('新密碼與確認新密碼不相符'),
                              ),
                            );
                            return;
                          }
                          var success = await model.updatePassword(
                            userId: user.user?.id ?? '',
                            password: model.newPassword.text,
                          );
                          if (success) {
                            var prefs = injector<SharedPreferences>();
                            await prefs.setString(
                                'password', model.newPassword.text);
                            ScaffoldMessenger.of(context)
                                .removeCurrentSnackBar();
                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(
                                content: Text('密碼更新成功！'),
                              ),
                            );
                          } else {
                            ScaffoldMessenger.of(context)
                                .removeCurrentSnackBar();
                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(
                                content: Text('密碼更新失敗，請稍後再試'),
                              ),
                            );
                          }
                          // model.updateProfile().then((value) {
                          //   if (value == null) {
                          //     ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                          //       content: Text('Update failed!'),
                          //       duration: Duration(seconds: 2),
                          //     ));
                          //   } else {
                          //     ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                          //       content: Text('Update Successfully!'),
                          //       duration: Duration(seconds: 2),
                          //     ));
                          //     if (Config().isListingType) {
                          //       user.user =
                          //           User.fromListingJson(value as Map<String, dynamic>);
                          //     } else {
                          //       user.user = User.fromAuthUser(
                          //           value as Map<String, dynamic>, user.user!.cookie);
                          //     }
                          //
                          //     user.saveUser(user.user);
                          //     Future.delayed(const Duration(seconds: 2)).then((value) {
                          //       if (mounted) {
                          //         try {
                          //           final navigator = Navigator.of(context);
                          //           if (navigator.canPop()) {
                          //             navigator.pop();
                          //           }
                          //         } catch (err) {
                          //           printLog(err);
                          //         }
                          //       }
                          //     });
                          //   }
                          // });
                        },
                  child: const Text(
                    '更新密碼',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        appBar: AppBar(
          backgroundColor: Theme.of(context).backgroundColor,
          iconTheme: IconThemeData(
            color: Theme.of(context).textTheme.headline6!.color,
          ),
          title: Text(
            '會員資料',
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
          bottom: TabBar(
            controller: controller,
            indicatorColor: primaryColor,
            labelStyle: TextStyle(
              fontSize: 18,
              color: Theme.of(context).textTheme.subtitle2!.color,
            ),
            unselectedLabelStyle: TextStyle(
              fontSize: 18,
              color: Theme.of(context).textTheme.subtitle2!.color,
            ),
            tabs: const [
              Tab(
                text: '我的資料',
              ),
              Tab(
                text: '修改密碼',
              ),
            ],
          ),
        ),
        body: GestureDetector(
          onTap: () {
            Tools.hideKeyboard(context);
          },
          child: Stack(
            children: [
              TabBarView(
                physics: const NeverScrollableScrollPhysics(),
                controller: controller,
                children: [
                  myData(),
                  changePassword(),
                ],
              ),
              Consumer<UserUpdateModel>(
                builder: (_, model, __) =>
                    model.state == UserUpdateState.loading
                        ? Container(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height,
                            color: Colors.black.withOpacity(0.5),
                            child: const Center(
                              child: SpinKitCircle(
                                color: Colors.white,
                                size: 20.0,
                              ),
                            ),
                          )
                        : Container(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget myData() {
    final user = Provider.of<UserModel>(context, listen: false);
    return Consumer<UserModel>(
      builder: (context, UserModel model, _) {
        return SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                // const Text(
                //   '經銷商資料',
                //   style: TextStyle(
                //     fontSize: 18,
                //     fontWeight: FontWeight.w600,
                //     color: Colors.black54,
                //   ),
                // ),
                // const SizedBox(height: 16),
                Text(
                  '經銷商編號 : ${user.user?.username ?? ''}',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
                const SizedBox(height: 16),
                Text(
                  '經銷商姓名 : ${user.user?.firstName ?? ''}',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
                const SizedBox(height: 16),
                Text(
                  '加入區域 : ${user.user?.defaultAssignArea == null ? '' : user.user?.defaultAssignArea!.title}',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
                const SizedBox(height: 16),
                Text(
                  '加入日期 : ${user.user?.registered ?? ''}',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
                const SizedBox(height: 16),
                Text(
                  '最高職級 : ${user.user?.grade1title ?? ''}',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
                const SizedBox(height: 16),
                Text(
                  '匯款方式 : ${user.user?.bank ?? ''}',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
                const SizedBox(height: 16),
                Text(
                  '匯款帳號 : ${user.user?.bankAccount ?? ''}',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
                const SizedBox(height: 16),
                const Text(
                  '(以上資料若需變更請洽營業處櫃檯)',
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.black54,
                  ),
                ),
                const SizedBox(height: 50),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget changePassword() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          // const SizedBox(height: 16),
          // const Text(
          //   '變更密碼',
          //   style: TextStyle(
          //     fontSize: 18,
          //     fontWeight: FontWeight.w600,
          //     color: Colors.black54,
          //   ),
          // ),
          const SizedBox(height: 16),
          Text(
            '新密碼',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
          const SizedBox(height: 8),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(
                    color: Theme.of(context).primaryColorLight, width: 1.5)),
            child: Consumer<UserUpdateModel>(
              builder: (_, model, __) => TextField(
                obscureText: hideFirst,
                obscuringCharacter: '*',
                decoration: InputDecoration(
                  border: InputBorder.none,
                  suffixIcon: GestureDetector(
                    onTap: () {
                      setState(() {
                        if (hideFirst) {
                          hideFirst = false;
                        } else {
                          hideFirst = true;
                        }
                      });
                    },
                    child: Icon(
                      hideFirst
                          ? Icons.visibility_off_outlined
                          : Icons.visibility_outlined,
                      color: Colors.grey,
                    ),
                  ),
                ),
                controller: model.newPassword,
              ),
            ),
          ),
          const SizedBox(height: 16),
          Text(
            '確認新密碼',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
          const SizedBox(height: 8),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(
                    color: Theme.of(context).primaryColorLight, width: 1.5)),
            child: Consumer<UserUpdateModel>(
              builder: (_, model, __) => TextField(
                obscureText: hideSecond,
                obscuringCharacter: '*',
                decoration: InputDecoration(
                  border: InputBorder.none,
                  suffixIcon: GestureDetector(
                    onTap: () {
                      setState(() {
                        if (hideSecond) {
                          hideSecond = false;
                        } else {
                          hideSecond = true;
                        }
                      });
                    },
                    child: Icon(
                      hideSecond
                          ? Icons.visibility_off_outlined
                          : Icons.visibility_outlined,
                      color: Colors.grey,
                    ),
                  ),
                ),
                controller: model.confirmPassword,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
