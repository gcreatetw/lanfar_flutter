import 'dart:async';

import 'package:country_pickers/country_pickers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:intl/intl.dart';
import 'package:lanfar/from_lanfar/screens/webview_with_cookie.dart';
import 'package:lanfar/menu/maintab_delegate.dart';
import 'package:provider/provider.dart';

import '../../../app.dart';
import '../../../common/config.dart';
import '../../../common/constants.dart';
import '../../../common/tools.dart';
import '../../../env.dart';
import '../../../generated/l10n.dart';
import '../../../models/index.dart' show AppModel, CartModel;
import '../../../models/order/order.dart';
import '../../../services/index.dart';
import '../../../widgets/common/box_comment.dart';
import '../../../widgets/common/webview.dart';
import '../../base_screen.dart';
import '../models/order_history_detail_model.dart';
import 'widgets/order_price.dart';
import 'widgets/product_order_item.dart';

class OrderHistoryDetailScreen extends StatefulWidget {
  const OrderHistoryDetailScreen();

  @override
  _OrderHistoryDetailScreenState createState() =>
      _OrderHistoryDetailScreenState();
}

class _OrderHistoryDetailScreenState
    extends BaseScreen<OrderHistoryDetailScreen> {
  OrderHistoryDetailModel get orderHistoryModel =>
      Provider.of<OrderHistoryDetailModel>(context, listen: false);

  @override
  void afterFirstLayout(BuildContext context) {
    super.afterFirstLayout(context);
    // orderHistoryModel.getTracking();
    orderHistoryModel.getOrderNote();
  }

  void cancelOrder() {
    orderHistoryModel.cancelOrder();
  }

  void _onNavigate(context, OrderHistoryDetailModel model) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => WebView(
          url:
              "${afterShip['tracking_url']}/${model.order.aftershipTracking!.slug}/${model.order.aftershipTracking!.trackingNumber}",
          appBar: AppBar(
            systemOverlayStyle: SystemUiOverlayStyle.light,
            leading: GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: const Icon(Icons.arrow_back_ios),
            ),
            title: Text(S.of(context).trackingPage),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final currencyRate = Provider.of<AppModel>(context).currencyRate;

    return Consumer<OrderHistoryDetailModel>(builder: (context, model, child) {
      final order = model.order;
      return Scaffold(
        // backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle.light,
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                size: 20,
                color: Theme.of(context).colorScheme.secondary,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              }),
          title: const Text('訂單明細'),
          // Text(
          //   S.of(context).orderNo + ' #${order.number}',
          //   style: TextStyle(color: Theme.of(context).colorScheme.secondary),
          // ),
          backgroundColor: Theme.of(context).backgroundColor,
          elevation: 0.0,
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 0.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              const SizedBox(height: 16),

              ///process
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 48),
                child: Builder(builder: (context) {
                  var finText = '完成';
                  switch (order.status) {
                    case OrderStatus.cancelled:
                      finText = '取消';
                      break;
                    case OrderStatus.refunded:
                      finText = '已退款';
                      break;
                    case OrderStatus.failed:
                      finText = '失敗';
                      break;
                    case OrderStatus.canceled:
                      finText = '取消';
                      break;
                    default:
                      finText = '完成';
                      break;
                  }
                  return Stack(
                    children: [
                      Row(
                        children: [
                          const SizedBox(width: 12),
                          Expanded(
                              child: Container(
                            margin: const EdgeInsets.only(top: 12.5),
                            height: 2,
                            width: double.maxFinite,
                            color: order.status == OrderStatus.pending
                                ? Colors.grey
                                : Theme.of(context).primaryColor,
                          )),
                          Expanded(
                              child: Container(
                            margin: const EdgeInsets.only(top: 12.5),
                            height: 2,
                            width: double.maxFinite,
                            color: order.status == OrderStatus.pending ||
                                    order.status == OrderStatus.processing
                                ? Colors.grey
                                : Theme.of(context).primaryColor,
                          )),
                          const SizedBox(width: 12),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              Container(
                                width: 25,
                                height: 25,
                                decoration: BoxDecoration(
                                  color: Theme.of(context).primaryColor,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                alignment: Alignment.center,
                                child: const Text(
                                  '1',
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              const SizedBox(height: 4),
                              Text(
                                '待付款',
                                style: Theme.of(context)
                                    .textTheme
                                    .caption
                                    ?.copyWith(
                                        color: Theme.of(context).primaryColor),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Container(
                                width: 25,
                                height: 25,
                                decoration: BoxDecoration(
                                  color: order.status == OrderStatus.pending
                                      ? Colors.grey
                                      : Theme.of(context).primaryColor,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                alignment: Alignment.center,
                                child: const Text(
                                  '2',
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              const SizedBox(height: 4),
                              Text(
                                '處理中',
                                style: Theme.of(context)
                                    .textTheme
                                    .caption
                                    ?.copyWith(
                                        color: order.status ==
                                                OrderStatus.pending
                                            ? Colors.grey
                                            : Theme.of(context).primaryColor),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Container(
                                width: 25,
                                height: 25,
                                decoration: BoxDecoration(
                                  color: order.status ==
                                              OrderStatus.completed ||
                                          order.status == OrderStatus.failed ||
                                          order.status ==
                                              OrderStatus.canceled ||
                                          order.status ==
                                              OrderStatus.cancelled ||
                                          order.status == OrderStatus.refunded
                                      ? Theme.of(context).primaryColor
                                      : Colors.grey,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                alignment: Alignment.center,
                                child: const Text(
                                  '3',
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              const SizedBox(height: 4),
                              Text(
                                finText,
                                style: Theme.of(context)
                                    .textTheme
                                    .caption
                                    ?.copyWith(
                                      color: order.status ==
                                                  OrderStatus.completed ||
                                              order.status ==
                                                  OrderStatus.failed ||
                                              order.status ==
                                                  OrderStatus.canceled ||
                                              order.status ==
                                                  OrderStatus.cancelled ||
                                              order.status ==
                                                  OrderStatus.refunded
                                          ? Theme.of(context).primaryColor
                                          : Colors.grey,
                                    ),
                              ),
                            ],
                          )
                        ],
                      )
                    ],
                  );
                }),
              ),
              const SizedBox(height: 16),

              ///order base
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 48),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '訂單日期 : ${DateFormat('yyyy-MM-dd').format(order.createdAt!)}',
                      style: Theme.of(context).textTheme.caption!.copyWith(
                            fontSize: 14.0,
                            color: Theme.of(context)
                                .colorScheme
                                .secondary
                                .withOpacity(0.8),
                          ),
                    ),
                    const SizedBox(height: 4),
                    Row(
                      children: List.generate(
                          150 ~/ 1,
                          (index) => Expanded(
                                child: Container(
                                  color: index % 2 == 0
                                      ? Colors.transparent
                                      : Colors.grey,
                                  height: 1,
                                ),
                              )),
                    ),
                    const SizedBox(height: 4),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Text(
                            S.of(context).orderNo + order.number.toString(),
                            style:
                                Theme.of(context).textTheme.caption!.copyWith(
                                      fontSize: 14.0,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary
                                          .withOpacity(0.8),
                                    ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 4),
                    Row(
                      children: List.generate(
                          150 ~/ 1,
                          (index) => Expanded(
                                child: Container(
                                  color: index % 2 == 0
                                      ? Colors.transparent
                                      : Colors.grey,
                                  height: 1,
                                ),
                              )),
                    ),
                    const SizedBox(height: 4),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Text(
                            '發票號碼 : ${order.invoceNumber ?? ''}',
                            style:
                                Theme.of(context).textTheme.caption!.copyWith(
                                      fontSize: 14.0,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary
                                          .withOpacity(0.8),
                                    ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 16),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Card(
                    child: ListView.separated(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) {
                          final _item = order.lineItems[index];
                          return Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8, vertical: 8),
                            child: ProductOrderItem(
                              orderId: order.id!,
                              orderStatus: order.status!,
                              product: _item,
                              index: index,
                            ),
                          );
                        },
                        separatorBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8),
                            child: Row(
                              children: List.generate(
                                  150 ~/ 1,
                                  (index) => Expanded(
                                        child: Container(
                                          color: index % 2 == 0
                                              ? Colors.transparent
                                              : Colors.grey,
                                          height: 1,
                                        ),
                                      )),
                            ),
                          );
                        },
                        itemCount: order.lineItems.length)),
              ),
              const SizedBox(height: 16),
              Container(
                color: Colors.white,
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Row(
                  children: [
                    Image.asset(
                      'assets/icons/from_lanfar/Artboard – 20.png',
                      color: Theme.of(context).primaryColor,
                      height: 24,
                      width: 24,
                    ),
                    const SizedBox(width: 4),
                    const Text('訂購類別'),
                    const SizedBox(width: 8),
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerRight,
                        child: const Text(
                          '一般訂單',
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 1),
              Container(
                color: Colors.white,
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Row(
                  children: [
                    Image.asset(
                      'assets/icons/from_lanfar/Artboard – 3.png',
                      color: Theme.of(context).primaryColor,
                      height: 24,
                      width: 24,
                    ),
                    const SizedBox(width: 4),
                    const Text('訂購方式'),
                    const SizedBox(width: 8),
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerRight,
                        child: const Text(
                          'APP',
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 1),
              Container(
                color: Colors.white,
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Image.asset(
                          'assets/icons/from_lanfar/Artboard – 4.png',
                          color: Theme.of(context).primaryColor,
                          height: 24,
                          width: 24,
                        ),
                        const SizedBox(width: 4),
                        const Text('取貨方式'),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              order.shippingMethodTitle ?? '',
                            ),
                          ),
                        ),
                      ],
                    ),
                    if (order.shippingId == 'betrs_shipping')
                      Container(
                        padding: const EdgeInsets.only(left: 28, top: 16),
                        width: double.maxFinite,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(order.shipping?.firstName ?? ''),
                            Text(
                              order.shipping?.phoneNumber ?? '',
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                            ),
                            Text(
                              order.shipping!.state! +
                                  order.shipping!.city! +
                                  order.shipping!.street!,
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                            )
                          ],
                        ),
                      ),
                  ],
                ),
              ),
              const SizedBox(height: 1),
              Container(
                color: Colors.white,
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Row(
                  children: [
                    Image.asset(
                      'assets/icons/from_lanfar/Artboard – 6.png',
                      color: Theme.of(context).primaryColor,
                      height: 24,
                      width: 24,
                    ),
                    const SizedBox(width: 4),
                    const Text('發票寄送'),
                    const SizedBox(width: 8),
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerRight,
                        child: const Text(
                          '隨貨附送',
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 1),
              Container(
                color: Colors.white,
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Row(
                  children: [
                    Icon(
                      Icons.message,
                      color: Theme.of(context).primaryColor,
                    ),
                    const SizedBox(width: 4),
                    const Text('備註'),
                    const SizedBox(width: 8),
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerRight,
                        child: Text(
                          order.customerNote!,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 16),
              Container(
                color: Colors.white,
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Row(
                  children: [
                    Expanded(
                      child: Text('訂單金額(${order.quantity}商品)'),
                    ),
                    Expanded(
                      child: Text(
                        PriceTools.getCurrencyFormatted(
                                order.total!.toInt().toString(), null) ??
                            '',
                        style: const TextStyle(color: primaryColor),
                        textAlign: TextAlign.end,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 1),
              Builder(
                builder: (context) {
                  var totalPV = 0;
                  for (var element in order.lineItems) {
                    totalPV += int.parse(element.pv ?? '0');
                  }
                  return Container(
                    color: Colors.white,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16, vertical: 16),
                    child: Row(
                      children: [
                        const Expanded(
                          child: Text('PV分數'),
                        ),
                        Expanded(
                          child: Text(
                            '${PriceTools.getCurrencyFormatted(totalPV, null, isPV: true) ?? ''}PV',
                            style: const TextStyle(color: primaryColor),
                            textAlign: TextAlign.end,
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
              const SizedBox(height: 16),
              Container(
                padding: const EdgeInsets.all(16),
                color: Colors.white,
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Icon(Icons.monetization_on, color: primaryColor),
                        const SizedBox(width: 4),
                        const Expanded(
                          child: Text('付款方式'),
                        ),
                        Expanded(
                          child: Text(
                            order.paymentMethodTitle ?? '',
                            style: const TextStyle(color: primaryColor),
                            textAlign: TextAlign.end,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 16),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '商品總金額',
                          style: Theme.of(context).textTheme.caption!.copyWith(
                                fontSize: 14.0,
                                color: Theme.of(context)
                                    .colorScheme
                                    .secondary
                                    .withOpacity(0.8),
                              ),
                        ),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              PriceTools.getCurrencyFormatted(
                                      (order.total! - order.totalShipping!)
                                          .toInt()
                                          .toString(),
                                      null) ??
                                  '',
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 4),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '運費總金額',
                          style: Theme.of(context).textTheme.caption!.copyWith(
                                fontSize: 14.0,
                                color: Theme.of(context)
                                    .colorScheme
                                    .secondary
                                    .withOpacity(0.8),
                              ),
                        ),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              PriceTools.getCurrencyFormatted(
                                      order.totalShipping!.toInt().toString(),
                                      null) ??
                                  '',
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 8),
                    Container(
                      width: double.maxFinite,
                      height: 1,
                      color: Theme.of(context).primaryColor,
                    ),
                    const SizedBox(height: 8),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '總金額',
                          style: Theme.of(context).textTheme.caption!.copyWith(
                                fontSize: 16.0,
                              ),
                        ),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              PriceTools.getCurrencyFormatted(
                                      order.total!.toInt().toString(), null) ??
                                  '',
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 16.0,
                                        color: Theme.of(context).primaryColor,
                                      ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16),
                child: Row(
                  children: [
                    Expanded(
                      child: GestureDetector(
                        onTap: () async {
                          unawaited(
                            showDialog(
                              barrierDismissible: false,
                              context: context,
                              builder: (context) => WillPopScope(
                                  child: kLoadingWidget(context),
                                  onWillPop: () async {
                                    return false;
                                  }),
                            ),
                          );
                          for (var element in order.lineItems) {
                            var product = await Services()
                                .api
                                .getProduct(element.productId);
                            if (product != null) {
                              var cartModel = Provider.of<CartModel>(context,
                                  listen: false);
                              cartModel.addProductToCart(
                                  context: context,
                                  product: product,
                                  quantity: element.quantity,
                                  sync: false);
                              await Services()
                                  .widget
                                  .syncCartToWebsite(cartModel);
                            }
                          }
                          Navigator.of(context).popUntil(
                              ModalRoute.withName(RouteList.dashboard));
                          MainTabControlDelegate.getInstance()
                              .changeTab(RouteList.cart.replaceFirst('/', ''));
                        },
                        child: Container(
                          alignment: Alignment.center,
                          padding: const EdgeInsets.symmetric(vertical: 12),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                                color: Theme.of(context).primaryColor),
                          ),
                          child: Text(
                            '再次下單',
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 16),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(width: 16),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(
                                  App.fluxStoreNavigatorKey.currentContext!)
                              .push(
                            MaterialPageRoute(
                              builder: (context) => WebViewWithCookie(
                                url:
                                    '${environment['serverConfig']['url']}/my-accounts/new-ticket/?order_id=${order.id}&',
                                multiPram: true,
                              ),
                            ),
                          );
                        },
                        child: Container(
                          alignment: Alignment.center,
                          padding: const EdgeInsets.symmetric(vertical: 12),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                                color: Theme.of(context).primaryColor),
                          ),
                          child: Text(
                            '訂單諮詢',
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 16),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 50),

              /// Render the Cancel and Refund
              // if (kPaymentConfig['EnableRefundCancel'])
              //   Services()
              //       .widget
              //       .renderButtons(context, order, cancelOrder, refundOrder),
              //
              // const SizedBox(height: 20),

              // if (order.billing != null) ...[
              //   Text(S.of(context).shippingAddress,
              //       style: const TextStyle(
              //           fontSize: 18, fontWeight: FontWeight.bold)),
              //   const SizedBox(height: 10),
              //   Text(
              //     ((order.billing!.apartment?.isEmpty ?? true)
              //             ? ''
              //             : '${order.billing!.apartment} ') +
              //         ((order.billing!.block?.isEmpty ?? true)
              //             ? ''
              //             : '${(order.billing!.apartment?.isEmpty ?? true) ? '' : '- '} ${order.billing!.block}, ') +
              //         order.billing!.street! +
              //         ', ' +
              //         order.billing!.city! +
              //         ', ' +
              //         getCountryName(order.billing!.country),
              //   ),
              // ],
              // if (order.status == OrderStatus.processing &&
              //     kPaymentConfig['EnableRefundCancel'])
              //   Column(
              //     children: <Widget>[
              //       const SizedBox(height: 30),
              //       Row(
              //         children: [
              //           Expanded(
              //             child: ButtonTheme(
              //               height: 45,
              //               child: ElevatedButton(
              //                   style: ElevatedButton.styleFrom(
              //                     onPrimary: Colors.white,
              //                     primary: HexColor('#056C99'),
              //                   ),
              //                   onPressed: refundOrder,
              //                   child: Text(
              //                       S.of(context).refundRequest.toUpperCase(),
              //                       style: const TextStyle(
              //                           fontWeight: FontWeight.w700))),
              //             ),
              //           )
              //         ],
              //       ),
              //       const SizedBox(height: 10),
              //     ],
              //   ),
              // if (kPaymentConfig['ShowOrderNotes'] ?? true)
              //   Padding(
              //     padding: const EdgeInsets.only(top: 20.0),
              //     child: Builder(
              //       builder: (context) {
              //         final listOrderNote = model.listOrderNote;
              //         if (listOrderNote?.isEmpty ?? true) {
              //           return const SizedBox();
              //         }
              //         return Column(
              //           crossAxisAlignment: CrossAxisAlignment.start,
              //           children: <Widget>[
              //             Text(
              //               S.of(context).orderNotes,
              //               style: const TextStyle(
              //                   fontSize: 18, fontWeight: FontWeight.bold),
              //             ),
              //             const SizedBox(height: 10),
              //             Column(
              //               crossAxisAlignment: CrossAxisAlignment.start,
              //               children: [
              //                 ...List.generate(
              //                   listOrderNote!.length,
              //                   (index) {
              //                     return Padding(
              //                       padding: const EdgeInsets.only(bottom: 15),
              //                       child: Column(
              //                         crossAxisAlignment:
              //                             CrossAxisAlignment.start,
              //                         children: <Widget>[
              //                           CustomPaint(
              //                             painter: BoxComment(
              //                                 color: Theme.of(context)
              //                                     .primaryColor),
              //                             child: SizedBox(
              //                               width: MediaQuery.of(context)
              //                                   .size
              //                                   .width,
              //                               child: Padding(
              //                                 padding: const EdgeInsets.only(
              //                                     left: 10,
              //                                     right: 10,
              //                                     top: 15,
              //                                     bottom: 25),
              //                                 child: Linkify(
              //                                   text:
              //                                       listOrderNote[index].note!,
              //                                   style: const TextStyle(
              //                                       color: Colors.white,
              //                                       fontSize: 13,
              //                                       height: 1.2),
              //                                   onOpen: (link) async {
              //                                     await Tools.launchURL(
              //                                         link.url);
              //                                   },
              //                                 ),
              //                               ),
              //                             ),
              //                           ),
              //                           Text(
              //                             formatTime(DateTime.parse(
              //                                 listOrderNote[index]
              //                                     .dateCreated!)),
              //                             style: const TextStyle(fontSize: 13),
              //                           )
              //                         ],
              //                       ),
              //                     );
              //                   },
              //                 ),
              //                 const SizedBox(height: 100),
              //               ],
              //             ),
              //           ],
              //         );
              //       },
              //     ),
              //   ),
              //
              // const SizedBox(height: 50)
            ],
          ),
        ),
      );
    });
  }

  String getCountryName(country) {
    try {
      return CountryPickerUtils.getCountryByIsoCode(country).name;
    } catch (err) {
      return country;
    }
  }

  Future<void> refundOrder() async {
    _showLoading();
    try {
      await orderHistoryModel.createRefund();
      _hideLoading();
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(S.of(context).refundOrderSuccess)));
    } catch (err) {
      _hideLoading();

      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(S.of(context).refundOrderFailed)));
    }
  }

  void _showLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Center(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white30,
              borderRadius: BorderRadius.circular(5.0),
            ),
            padding: const EdgeInsets.all(50.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                kLoadingWidget(context),
                // const SizedBox(height: 48),
                ElevatedButton(
                  onPressed: Navigator.of(context).pop,
                  child: Text(S.of(context).cancel),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void _hideLoading() {
    Navigator.of(context).pop();
  }

  String formatTime(DateTime time) {
    return DateFormat('dd/MM/yyyy, HH:mm').format(time);
  }
}
