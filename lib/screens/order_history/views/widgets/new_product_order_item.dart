import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:inspireui/widgets/skeleton_widget/skeleton_widget.dart';
import '../../../../common/constants.dart';
import '../../../../common/tools/image_tools.dart';
import '../../../../common/tools/price_tools.dart';
import '../../../../from_lanfar/model/new_order_history_detail_model.dart';
import '../../../../models/entities/index.dart';
import '../../../../services/services.dart';

class NewProductOrderItem extends StatefulWidget {
  final Orderdata orderData;
  final List<Map>? tickets;

  const NewProductOrderItem({
    Key? key,
    required this.orderData,
    this.tickets,
  }) : super(key: key);

  @override
  _NewProductOrderItemState createState() => _NewProductOrderItemState();
}

class _NewProductOrderItemState extends State<NewProductOrderItem> {
  Product? product;

  bool loading = false;

  String imageFeatured = kDefaultImage;

  Product jsonParser(item) {
    var product = Product.fromJson(item);
    if (item['store'] != null) {
      if (item['store']['errors'] == null) {
        product = Services().widget.updateProductObject(product, item);
      }
    }
    return product;
  }

  @override
  void initState() {
    super.initState();
    // Future.microtask(() async {
    //   product = await Services().api.getProduct(widget.orderData.prodNo);
    //   // var endPoint = 'products?status=publish&page=1&per_page=10';
    //   // final wcApi = WooCommerceAPI(
    //   //     environment['serverConfig']['url'],
    //   //     environment['serverConfig']['consumerKey'],
    //   //     environment['serverConfig']['consumerSecret']);
    //   // var response =
    //   //     await wcApi.getAsync('$endPoint&sku=${widget.orderData.sku}');
    //   // product = jsonParser(response[0]);
    //   if (product != null) {
    //     setState(() {
    //       imageFeatured = product!.imageFeature ?? kDefaultImage;
    //     });
    //   }
    //
    //   setState(() {
    //     loading = false;
    //   });
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: navigateToProductDetail,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              if (widget.orderData.type == 'bundled') const SizedBox(width: 60),
              ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: Container(
                  width: 80,
                  height: 80,
                  color: Colors.grey.withOpacity(0.2),
                  child: loading
                      ? const Skeleton(
                          width: 80,
                          height: 80,
                        )
                      : ImageTools.image(
                          url: widget.orderData.image ?? '',
                          fit: BoxFit.cover,
                        ),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      widget.orderData.prodName ?? '',
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(fontWeight: FontWeight.bold),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    const SizedBox(height: 5),
                    if ((PriceTools.getCurrencyFormatted(
                                widget.orderData.prodPrice, null) ??
                            '0 元') !=
                        '0 元')
                      Row(
                        children: [
                          Expanded(
                            child: Row(
                              children: [
                                Text(
                                  PriceTools.getCurrencyFormatted(
                                          widget.orderData.prodPrice, null) ??
                                      '',
                                  style: Theme.of(context)
                                      .textTheme
                                      .caption!
                                      .copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                                  strutStyle: const StrutStyle(
                                      forceStrutHeight: true, leading: 0.5),
                                ),
                                if (widget.tickets == null) ...[
                                  Text(
                                    ' | ',
                                    style: Theme.of(context)
                                        .textTheme
                                        .caption!
                                        .copyWith(
                                          fontSize: 14.0,
                                          color: Theme.of(context)
                                              .colorScheme
                                              .secondary
                                              .withOpacity(0.8),
                                        ),
                                    strutStyle: const StrutStyle(
                                        forceStrutHeight: true, leading: 0.5),
                                  ),
                                  Text(
                                    PriceTools.getCurrencyFormatted(
                                            widget.orderData.prodPv, null,
                                            isPV: true) ??
                                        '',
                                    style: Theme.of(context)
                                        .textTheme
                                        .caption!
                                        .copyWith(
                                          fontSize: 14.0,
                                          color: primaryColor,
                                        ),
                                    strutStyle: const StrutStyle(
                                        forceStrutHeight: true, leading: 0.5),
                                  ),
                                  Text(
                                    ' PV',
                                    style: Theme.of(context)
                                        .textTheme
                                        .caption!
                                        .copyWith(
                                          fontSize: 14.0,
                                          color: Theme.of(context)
                                              .colorScheme
                                              .secondary
                                              .withOpacity(0.8),
                                        ),
                                    strutStyle: const StrutStyle(
                                        forceStrutHeight: true, leading: 0.5),
                                  ),
                                ],
                              ],
                            ),
                          ),
                        ],
                      ),
                  ],
                ),
              ),
              const SizedBox(width: 10),
              Text(
                'x${widget.orderData.ordQty}',
                style: Theme.of(context)
                    .textTheme
                    .subtitle1!
                    .copyWith(fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
        for (var element in widget.tickets ?? []) ticketWidget(element),
      ],
    );
  }

  void navigateToProductDetail() async {
    // if (product == null) {
    //   product = await Services().api.getProduct(widget.orderData.prodNo);
    //   // var endPoint = 'products?status=publish&page=1&per_page=10';
    //   // final wcApi = WooCommerceAPI(
    //   //     environment['serverConfig']['url'],
    //   //     environment['serverConfig']['consumerKey'],
    //   //     environment['serverConfig']['consumerSecret']);
    //   // var response =
    //   //     await wcApi.getAsync('$endPoint&sku=${widget.orderData.prodNo}');
    //   // product = jsonParser(response[0]);
    // }
    // await Navigator.of(context).pushNamed(
    //   RouteList.productDetail,
    //   arguments: product,
    // );
  }

  Widget ticketWidget(Map data) {
    return Container(
      width: double.maxFinite,
      margin: const EdgeInsets.only(
        top: 4,
        left: 60,
      ),
      child: DottedBorder(
        padding: const EdgeInsets.all(8),
        radius: const Radius.circular(5),
        borderType: BorderType.RRect,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            for (var element in data.keys) Text('$element:${data[element]}'),
          ],
        ),
      ),
    );
  }
}
