import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../../../common/constants.dart';
import '../../../../common/tools.dart';
import '../../../../generated/l10n.dart';
import '../../../../models/order/order.dart';
import '../../models/order_history_detail_model.dart';

class OrderListItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Center(
      child: Consumer<OrderHistoryDetailModel>(builder: (_, model, __) {
        final order = model.order;
        return GestureDetector(
          onTap: () {
            Navigator.of(context).pushNamed(
              RouteList.orderDetail,
              arguments: model,
            );
          },
          child: Container(
            width: size.width,
            height: 150,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: const [
                BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0, 2),
                  blurRadius: 6,
                )
              ],
            ),
            margin: const EdgeInsets.only(
              top: 5.0,
              left: 15.0,
              right: 15.0,
              bottom: 10.0,
            ),
            child: Column(
              children: [
                Expanded(
                  flex: 2,
                  child: Container(
                    padding: const EdgeInsets.only(left: 10.0, right: 15.0),
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(5.0),
                        topRight: Radius.circular(5.0),
                      ),
                      color: Theme.of(context).backgroundColor,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Image.asset(
                              order.shippingId == 'betrs_shipping'
                                  ? 'assets/icons/from_lanfar/Artboard – 4.png'
                                  : 'assets/icons/from_lanfar/Artboard – 19.png',
                              color: primaryColor,
                              width: 20,
                              height: 20,
                            ),
                            const SizedBox(width: 4),
                            Text(
                              order.shippingId == 'betrs_shipping'
                                  ? '宅配取貨'
                                  : '營業處取貨',
                              style: const TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(width: 16),
                          ],
                        ),
                        Container(
                          width: 1,
                          height: double.maxFinite,
                          color: primaryColor,
                          margin: const EdgeInsets.only(bottom: 30, top: 30),
                        ),
                        const SizedBox(width: 10),
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: Text(
                                      '訂單日期 : ${DateFormat('yyyy-MM-dd').format(order.createdAt!)}',
                                      style: Theme.of(context)
                                          .textTheme
                                          .caption!
                                          .copyWith(
                                            fontSize: 12.0,
                                            color: Theme.of(context)
                                                .colorScheme
                                                .secondary
                                                .withOpacity(0.8),
                                          ),
                                      strutStyle: const StrutStyle(
                                          forceStrutHeight: true, leading: 0.5),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: Row(
                                      children: [
                                        Text(
                                          S.of(context).orderNo,
                                          style: Theme.of(context)
                                              .textTheme
                                              .caption!
                                              .copyWith(
                                                fontSize: 12.0,
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .secondary
                                                    .withOpacity(0.8),
                                              ),
                                          strutStyle: const StrutStyle(
                                              forceStrutHeight: true,
                                              leading: 0.5),
                                        ),
                                        Text(
                                          order.number.toString(),
                                          style: Theme.of(context)
                                              .textTheme
                                              .caption!
                                              .copyWith(
                                                fontSize: 12.0,
                                                color: primaryColor,
                                              ),
                                          strutStyle: const StrutStyle(
                                              forceStrutHeight: true,
                                              leading: 0.5),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: Row(
                                      children: [
                                        Text(
                                          '訂單金額 : ',
                                          style: Theme.of(context)
                                              .textTheme
                                              .caption!
                                              .copyWith(
                                                fontSize: 12.0,
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .secondary
                                                    .withOpacity(0.8),
                                              ),
                                          strutStyle: const StrutStyle(
                                              forceStrutHeight: true,
                                              leading: 0.5),
                                        ),
                                        Text(
                                          PriceTools.getCurrencyFormatted(
                                                  order.total!
                                                      .toInt()
                                                      .toString(),
                                                  null) ??
                                              '',
                                          style: Theme.of(context)
                                              .textTheme
                                              .caption!
                                              .copyWith(
                                                fontSize: 12.0,
                                                color: Colors.redAccent,
                                              ),
                                          strutStyle: const StrutStyle(
                                              forceStrutHeight: true,
                                              leading: 0.5),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Stack(
                          alignment: Alignment.centerRight,
                          children: [
                            if (order.status != null)
                              Column(
                                children: [
                                  const SizedBox(height: 8),
                                  OrderStatusWidget(
                                    title: S.of(context).status,
                                    detail:
                                        order.status == OrderStatus.unknown &&
                                                order.orderStatus != null
                                            ? order.orderStatus
                                            : order.status!.content,
                                  )
                                ],
                              ),
                            Icon(
                              Icons.chevron_right,
                              size: 32,
                              color: Theme.of(context)
                                  .colorScheme
                                  .secondary
                                  .withOpacity(0.8),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                // Divider(
                //   height: 1,
                //   color: Theme.of(context).primaryColorLight,
                // ),
                const Divider(height: 0.5),

                Container(
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(5.0),
                      bottomRight: Radius.circular(5.0),
                    ),
                    color: Theme.of(context).backgroundColor,
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      // color: Theme.of(context).primaryColorLight,
                    ),
                    margin: const EdgeInsets.all(8),
                    padding: EdgeInsets.only(
                      right: 7,
                      left: !Tools.isRTL(context) ? 0.0 : 20,
                      top: 0,
                      bottom: 0,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Container(
                            padding: const EdgeInsets.only(left: 30),
                            child: Text(
                              order.shippingId == 'betrs_shipping'
                                  ? '${order.billing?.firstName}'
                                  : '${order.shippingMethodTitle}',
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 12.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              order.shippingId == 'betrs_shipping'
                                  ? '${order.billing?.state}${order.billing?.city}${order.billing?.street}'
                                  : '',
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 12.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                            ),
                          ),
                        ),
                        // OrderStatusWidget(
                        //   title: S.of(context).total,
                        //   detail: PriceTools.getCurrencyFormatted(
                        //       order.total, null),
                        // ),
                        // OrderStatusWidget(
                        //   title: S.of(context).tax,
                        //   detail: PriceTools.getCurrencyFormatted(
                        //       order.totalTax, null),
                        // ),
                        // OrderStatusWidget(
                        //   title: S.of(context).Qty,
                        //   detail: order.quantity.toString(),
                        // ),
                        // if (order.status != null)
                        //   OrderStatusWidget(
                        //     title: S.of(context).status,
                        //     detail: order.status == OrderStatus.unknown &&
                        //             order.orderStatus != null
                        //         ? order.orderStatus
                        //         : order.status!.content,
                        //   ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}

class OrderStatusWidget extends StatelessWidget {
  final String? title;
  final String? detail;

  const OrderStatusWidget({Key? key, this.title, this.detail})
      : super(key: key);

  String getTitleStatus(String status, BuildContext context) {
    switch (status.toLowerCase()) {
      case 'onhold':
        return S.of(context).orderStatusOnHold;
      case 'pending':
        return S.of(context).orderStatusPendingPayment;
      case 'failed':
        return S.of(context).orderStatusFailed;
      case 'processing':
        return S.of(context).orderStatusProcessing;
      case 'completed':
        return S.of(context).orderStatusCompleted;
      case 'cancelled':
        return S.of(context).orderStatusCancelled;
      case 'refunded':
        return S.of(context).orderStatusRefunded;
      default:
        return status;
    }
  }

  @override
  Widget build(BuildContext context) {
    var statusOrderColor = Colors.black;
    switch (detail!.toLowerCase()) {
      case 'pending':
        {
          statusOrderColor = Colors.red;
          break;
        }
      case 'processing':
        {
          statusOrderColor = Colors.orange;
          break;
        }
      case 'completed':
        {
          statusOrderColor = Colors.green;
          break;
        }
    }

    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: statusOrderColor),
          borderRadius: BorderRadius.circular(5)),
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 1),
      child: Text(
        getTitleStatus(detail!, context).capitalize(),
        style: Theme.of(context).textTheme.subtitle1!.copyWith(
              color: statusOrderColor,
              fontWeight: FontWeight.w700,
              fontSize: 12.0,
            ),
        overflow: TextOverflow.ellipsis,
      ),
    );

    // return Expanded(
    //   child: Column(
    //     mainAxisAlignment: MainAxisAlignment.center,
    //     crossAxisAlignment: CrossAxisAlignment.center,
    //     children: [
    //       const SizedBox(
    //         height: 10.0,
    //       ),
    //       Expanded(
    //         child: Text(
    //           title.toString(),
    //           style: Theme.of(context)
    //               .textTheme
    //               .caption!
    //               .copyWith(
    //                 fontSize: 14.0,
    //                 color: Theme.of(context)
    //                     .colorScheme
    //                     .secondary
    //                     .withOpacity(0.7),
    //                 fontWeight: FontWeight.w700,
    //               )
    //               .apply(fontSizeFactor: 0.9),
    //         ),
    //       ),
    //       Expanded(
    //         child: Text(
    //           getTitleStatus(detail!, context).capitalize(),
    //           style: Theme.of(context).textTheme.subtitle1!.copyWith(
    //                 color: statusOrderColor,
    //                 fontWeight: FontWeight.w700,
    //                 fontSize: 14.0,
    //               ),
    //           overflow: TextOverflow.ellipsis,
    //         ),
    //       ),
    //     ],
    //   ),
    // );
  }
}
