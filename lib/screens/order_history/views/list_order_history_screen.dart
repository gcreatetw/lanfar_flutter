import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../../common/constants.dart';
import '../../../common/tools.dart';
import '../../../from_lanfar/model/general_state.dart';
import '../../../from_lanfar/model/new_order_history_model.dart';
import '../../../generated/l10n.dart';
import '../../../models/index.dart';
import '../models/list_order_history_model.dart';
import '../models/order_history_detail_model.dart';
import 'widgets/new_order_list_item.dart';
import 'widgets/order_list_loading_item.dart';

class ListOrderHistoryScreen extends StatefulWidget {
  @override
  _ListOrderHistoryScreenState createState() => _ListOrderHistoryScreenState();
}

class _ListOrderHistoryScreenState extends State<ListOrderHistoryScreen> {
  ListOrderHistoryModel get listOrderViewModel =>
      Provider.of<ListOrderHistoryModel>(context, listen: false);

  DateTime startDate = DateTime.now().subtract(const Duration(days: 30));
  DateTime endDate = DateTime.now();

  DateFormat dateFormat = DateFormat('yyyy-MM-dd');
  DateFormat dateFormatForShow = DateFormat('yyyy/MM/dd');

  var mapOrderHistoryDetailModel = <int, OrderHistoryDetailModel>{};

  int onePageCount = 10;

  int currentPage = 1;

  int pageStart = 1;

  ScrollController controller = ScrollController();

  List<String> typeList = ['一般訂單', '賣場訂單'];

  int currentValue = 0;

  @override
  void initState() {
    super.initState();
    final user = Provider.of<UserModel>(context, listen: false).user ?? User();
    final newOrderHistoryModel =
        Provider.of<NewOrderHistoryModel>(context, listen: false);
    Future.microtask(() async {
      await newOrderHistoryModel.fetch(
        id: user.username!,
        endDate: dateFormat.format(endDate),
        startDate: dateFormat.format(startDate),
        type: currentValue,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserModel>(context, listen: false).user ?? User();
    final newOrderHistoryModel = Provider.of<NewOrderHistoryModel>(context);
    return WillPopScope(
      onWillPop: () async {
        newOrderHistoryModel.orderList.clear();
        return true;
      },
      child: Scaffold(
        // backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          title: Text(
            S.of(context).orderHistory,
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
          backgroundColor: Theme.of(context).backgroundColor,
        ),
        body: Column(
          children: [
            const SizedBox(height: 8),
            Row(
              children: [
                const SizedBox(width: 16),
                Expanded(
                  child: Row(
                    children: [
                      Builder(
                        builder: (context) {
                          return Expanded(
                            child: InkWell(
                              onTap: () async {
                                final startDateTemp = (await showDatePicker(
                                      context: context,
                                      initialDate: startDate,
                                      firstDate: DateTime(
                                        DateTime.now().year - 10,
                                      ),
                                      lastDate: DateTime.now(),
                                      helpText: '請選擇開始時間',
                                      builder: (context, child) {
                                        return Theme(
                                          data: Theme.of(context).copyWith(
                                            colorScheme:
                                                const ColorScheme.light(
                                              primary: primaryColor,
                                              onPrimary: Colors.white,
                                            ),
                                          ),
                                          child: child!,
                                        );
                                      },
                                    )) ??
                                    startDate;
                                if (startDateTemp.isAfter(endDate)) {
                                  Tools.showSnackBar(
                                      ScaffoldMessenger.of(context), '結束時間不得早於開始時間');
                                  return;
                                }
                                startDate = startDateTemp;
                                setState(() {});
                              },
                              child: Container(
                                color: Colors.white,
                                padding: const EdgeInsets.only(
                                    top: 8, bottom: 8, left: 8, right: 4),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        dateFormatForShow.format(startDate),
                                        style: Theme.of(context)
                                            .textTheme
                                            .caption!
                                            .copyWith(
                                              fontSize: 14.0,
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .secondary
                                                  .withOpacity(0.8),
                                            ),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                    const Icon(
                                      Icons.keyboard_arrow_down,
                                      size: 20,
                                      color: primaryColor,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                      const SizedBox(width: 8),
                      const Text('~'),
                      const SizedBox(width: 8),
                      Builder(
                        builder: (context) {
                          return Expanded(
                            child: InkWell(
                              onTap: () async {
                                final endDateTemp = (await showDatePicker(
                                      context: context,
                                      initialDate: endDate,
                                      firstDate: DateTime.now()
                                          .subtract(const Duration(days: 3000)),
                                      lastDate: DateTime.now(),
                                      helpText: '請選擇結束時間',
                                      builder: (context, child) {
                                        return Theme(
                                          data: Theme.of(context).copyWith(
                                            colorScheme:
                                                const ColorScheme.light(
                                              primary: primaryColor,
                                              onPrimary: Colors.white,
                                            ),
                                          ),
                                          child: child!,
                                        );
                                      },
                                    )) ??
                                    endDate;
                                if (endDateTemp.isBefore(startDate)) {
                                  Tools.showSnackBar(
                                      ScaffoldMessenger.of(context), '結束時間不得早於開始時間');
                                  return;
                                }
                                endDate = endDateTemp;
                                setState(() {});
                              },
                              child: Container(
                                color: Colors.white,
                                padding: const EdgeInsets.only(
                                    top: 8, bottom: 8, left: 8, right: 4),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        dateFormatForShow.format(endDate),
                                        style: Theme.of(context)
                                            .textTheme
                                            .caption!
                                            .copyWith(
                                              fontSize: 14.0,
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .secondary
                                                  .withOpacity(0.8),
                                            ),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                    const Icon(
                                      Icons.keyboard_arrow_down,
                                      size: 20,
                                      color: primaryColor,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 16),
              ],
            ),
            const SizedBox(height: 8),
            Row(
              children: [
                const SizedBox(width: 16),
                Expanded(
                  child: ColoredBox(
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 8,
                        right: 4,
                        top: 6,
                        bottom: 6,
                      ),
                      child: DropdownButton<int>(
                        dropdownColor: Colors.white,
                        value: currentValue,
                        isExpanded: true,
                        underline: const SizedBox(),
                        isDense: true,
                        style: Theme.of(context).textTheme.caption!.copyWith(
                              fontSize: 14.0,
                              color: Theme.of(context)
                                  .colorScheme
                                  .secondary
                                  .withOpacity(0.8),
                            ),
                        icon: const Icon(
                          Icons.keyboard_arrow_down,
                          color: primaryColor,
                          size: 20,
                        ),
                        onChanged: (v) {
                          setState(() {
                            currentValue = v ?? 0;
                          });
                        },
                        items: [
                          for (int i = 0; i < typeList.length; i++)
                            DropdownMenuItem(
                              value: i,
                              child: Text(
                                typeList[i],
                              ),
                            )
                        ],
                      ),
                    ),
                  ),
                ),
                const SizedBox(width: 8),
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              currentPage = 1;
                            });
                            newOrderHistoryModel.fetch(
                              id: user.username ?? '',
                              startDate: dateFormat.format(startDate),
                              endDate: dateFormat.format(endDate),
                              type: currentValue,
                            );
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(color: primaryColor),
                              color: primaryColor,
                            ),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 6),
                            child: const Text(
                              '搜尋',
                              style: TextStyle(color: Colors.white),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 8),
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            setState(
                              () {
                                startDate = DateTime.now()
                                    .subtract(const Duration(days: 30));
                                endDate = DateTime.now();
                              },
                            );
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(color: primaryColor),
                              color: Colors.white,
                            ),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 6),
                            child: const Text(
                              '重設',
                              style: TextStyle(color: primaryColor),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 16),
              ],
            ),
            const SizedBox(height: 3),
            if (newOrderHistoryModel.state != GeneralState.loading)
              Expanded(
                child: Builder(builder: (context) {
                  if (newOrderHistoryModel.orderList.isEmpty) {
                    return const Center(
                      child: Text('無歷史訂單'),
                    );
                  }
                  return ListView.builder(
                    controller: controller,
                    itemCount: (newOrderHistoryModel.orderList.length -
                                (currentPage - 1) * onePageCount) >
                            onePageCount
                        ? onePageCount
                        : newOrderHistoryModel.orderList.length -
                            (currentPage - 1) * onePageCount,
                    itemBuilder: (context, index) {
                      final adjustIndex =
                          index + (currentPage - 1) * onePageCount;
                      return NewOrderListItem(
                        data: newOrderHistoryModel.orderList[adjustIndex],
                      );
                    },
                  );
                }),
              ),
            if (newOrderHistoryModel.state == GeneralState.loading)
              Expanded(
                child: ListView.builder(
                  itemCount: 3,
                  itemBuilder: (context, index) {
                    return const OrderListLoadingItem();
                  },
                ),
              ),
            const SizedBox(height: 8),
            Builder(builder: (context) {
              var showDot = true;
              var totalPage =
                  newOrderHistoryModel.orderList.length ~/ onePageCount;
              if (newOrderHistoryModel.orderList.length % onePageCount != 0) {
                totalPage++;
              }
              var endPage =
                  ((totalPage > 5) ? 4 + pageStart : totalPage).toInt();

              if (endPage >= totalPage) {
                showDot = false;
                endPage = totalPage;
                pageStart = totalPage - 4;
              }
              if (pageStart < 1) {
                pageStart = 1;
              }
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  for (int i = pageStart; i <= endPage; i++)
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          currentPage = i;
                          pageStart = currentPage - 2 < 1 ? 1 : currentPage - 2;
                        });
                        controller.animateTo(0,
                            duration: const Duration(milliseconds: 200),
                            curve: Curves.ease);
                      },
                      child: Container(
                        margin: const EdgeInsets.all(4),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 8, vertical: 4),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          color: currentPage == i ? primaryColor : null,
                        ),
                        child: Text(
                          i.toString(),
                          style: TextStyle(
                              color: currentPage == i
                                  ? Colors.white
                                  : primaryColor),
                        ),
                      ),
                    ),
                  if (showDot)
                    const Text(
                      '...',
                      style: TextStyle(color: primaryColor),
                    )
                ],
              );
            }),

            const SizedBox(height: 16),
            // Expanded(
            //   child: PagingList<ListOrderHistoryModel, Order>(
            //     onRefresh: mapOrderHistoryDetailModel.clear,
            //     itemBuilder: (_, order, index) {
            //       if (mapOrderHistoryDetailModel[index] == null) {
            //         final orderHistoryDetailModel = OrderHistoryDetailModel(
            //           order: order,
            //           user: user,
            //         );
            //         mapOrderHistoryDetailModel[index] = orderHistoryDetailModel;
            //       }
            //       return ChangeNotifierProvider<OrderHistoryDetailModel>.value(
            //         value: mapOrderHistoryDetailModel[index]!,
            //         child: OrderListItem(),
            //       );
            //     },
            //     lengthLoadingWidget: 3,
            //     loadingWidget: const OrderListLoadingItem(),
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
