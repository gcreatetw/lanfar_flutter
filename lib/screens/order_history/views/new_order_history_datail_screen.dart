import 'dart:async';

import 'package:country_pickers/country_pickers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:lanfar/from_lanfar/utils/utils.dart';
import 'package:provider/provider.dart';

import '../../../app.dart';
import '../../../common/config.dart';
import '../../../common/constants.dart';
import '../../../common/tools.dart';
import '../../../from_lanfar/model/general_state.dart';
import '../../../from_lanfar/model/new_order_history_detail_model.dart';
import '../../../from_lanfar/screens/contact_page.dart';
import '../../../generated/l10n.dart';
import '../../../menu/maintab_delegate.dart';
import '../../../models/index.dart' show Product, UserModel;
import '../../../services/index.dart';
import '../../checkout/payment_webview_screen.dart';
import 'widgets/new_product_order_item.dart';

class NewOrderHistoryDetailScreen extends StatefulWidget {
  final String orderId;
  final Function? onRefresh;

  const NewOrderHistoryDetailScreen({
    required this.orderId,
    this.onRefresh,
  });

  @override
  _NewOrderHistoryDetailScreenState createState() =>
      _NewOrderHistoryDetailScreenState();
}

class _NewOrderHistoryDetailScreenState
    extends State<NewOrderHistoryDetailScreen> {
  NewOrderHistoryDetailModel newOrderHistoryDetailModel =
      NewOrderHistoryDetailModel();

  @override
  void initState() {
    super.initState();
    newOrderHistoryDetailModel.fetch(
        widget.orderId, context.read<UserModel>().user?.username ?? '');
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: newOrderHistoryDetailModel,
      child: Scaffold(
        // backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle.light,
          title: Text(
            '訂單明細',
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
          // Text(
          //   S.of(context).orderNo + ' #${order.number}',
          //   style: TextStyle(color: Theme.of(context).colorScheme.secondary),
          // ),
          backgroundColor: Theme.of(context).backgroundColor,
          elevation: 0.0,
        ),
        body: Consumer<NewOrderHistoryDetailModel>(
          builder: (context, model, _) {
            if (model.state == GeneralState.loading) {
              return Center(
                child: kLoadingWidget(context),
              );
            }
            var order = model.orderList!;
            return SingleChildScrollView(
              padding: const EdgeInsets.symmetric(horizontal: 0.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  const SizedBox(height: 16),

                  ///process
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 48),
                    child: Builder(builder: (context) {
                      var finText = '完成';
                      if (order.orderProcess == 0) {
                        finText = '取消';
                      }
                      return Stack(
                        children: [
                          Row(
                            children: [
                              const SizedBox(width: 12),
                              Expanded(
                                child: Container(
                                  margin: const EdgeInsets.only(top: 12.5),
                                  height: 1,
                                  width: double.maxFinite,
                                  color: Colors.grey,
                                ),
                              ),
                              const SizedBox(width: 12),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                children: [
                                  Container(
                                    width: 25,
                                    height: 25,
                                    decoration: BoxDecoration(
                                      color: order.orderProcess != 1
                                          ? Colors.grey
                                          : Theme.of(context).primaryColor,
                                      borderRadius: BorderRadius.circular(20),
                                    ),
                                    alignment: Alignment.center,
                                    child: const Text(
                                      '1',
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w700,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 4),
                                  Text(
                                    '待付款',
                                    style: Theme.of(context)
                                        .textTheme
                                        .caption
                                        ?.copyWith(
                                          color: order.orderProcess != 1
                                              ? Colors.grey
                                              : Theme.of(context).primaryColor,
                                        ),
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  Container(
                                    width: 25,
                                    height: 25,
                                    decoration: BoxDecoration(
                                      color: order.orderProcess != 2
                                          ? Colors.grey
                                          : Theme.of(context).primaryColor,
                                      borderRadius: BorderRadius.circular(20),
                                    ),
                                    alignment: Alignment.center,
                                    child: const Text(
                                      '2',
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w700,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 4),
                                  Text(
                                    '處理中',
                                    style: Theme.of(context)
                                        .textTheme
                                        .caption
                                        ?.copyWith(
                                          color: order.orderProcess != 2
                                              ? Colors.grey
                                              : Theme.of(context).primaryColor,
                                        ),
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  Container(
                                    width: 25,
                                    height: 25,
                                    decoration: BoxDecoration(
                                      color: order.orderProcess != 3 &&
                                              order.orderProcess != 0
                                          ? Colors.grey
                                          : Theme.of(context).primaryColor,
                                      borderRadius: BorderRadius.circular(20),
                                    ),
                                    alignment: Alignment.center,
                                    child: const Text(
                                      '3',
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w700,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 4),
                                  Text(
                                    finText,
                                    style: Theme.of(context)
                                        .textTheme
                                        .caption
                                        ?.copyWith(
                                          color: order.orderProcess != 3 &&
                                                  order.orderProcess != 0
                                              ? Colors.grey
                                              : Theme.of(context).primaryColor,
                                        ),
                                  ),
                                ],
                              )
                            ],
                          )
                        ],
                      );
                    }),
                  ),
                  const SizedBox(height: 16),

                  ///order base
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 48),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '訂單日期 : ${order.orderDate}',
                          style: Theme.of(context).textTheme.caption!.copyWith(
                                fontSize: 14.0,
                                color: Theme.of(context)
                                    .colorScheme
                                    .secondary
                                    .withOpacity(0.8),
                              ),
                        ),
                        const SizedBox(height: 4),
                        Row(
                          children: List.generate(
                              150 ~/ 1,
                              (index) => Expanded(
                                    child: Container(
                                      color: index % 2 == 0
                                          ? Colors.transparent
                                          : Colors.grey,
                                      height: 1,
                                    ),
                                  )),
                        ),
                        const SizedBox(height: 4),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Text(
                                S.of(context).orderNo + (order.orderId ?? ''),
                                style: Theme.of(context)
                                    .textTheme
                                    .caption!
                                    .copyWith(
                                      fontSize: 14.0,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary
                                          .withOpacity(0.8),
                                    ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 4),
                        Row(
                          children: List.generate(
                            150 ~/ 1,
                            (index) => Expanded(
                              child: Container(
                                color: index % 2 == 0
                                    ? Colors.transparent
                                    : Colors.grey,
                                height: 1,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 4),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Text(
                                '發票號碼 : ${order.orderInvoiceNumber ?? ''}',
                                style: Theme.of(context)
                                    .textTheme
                                    .caption!
                                    .copyWith(
                                      fontSize: 14.0,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary
                                          .withOpacity(0.8),
                                    ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 4),
                        Row(
                          children: List.generate(
                              150 ~/ 1,
                              (index) => Expanded(
                                    child: Container(
                                      color: index % 2 == 0
                                          ? Colors.transparent
                                          : Colors.grey,
                                      height: 1,
                                    ),
                                  )),
                        ),
                        const SizedBox(height: 4),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Text(
                                '訂單狀態 : ${order.orderStatus ?? ''}',
                                style: Theme.of(context)
                                    .textTheme
                                    .caption!
                                    .copyWith(
                                      fontSize: 14.0,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary
                                          .withOpacity(0.8),
                                    ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 16),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Card(
                      child: ListView.separated(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8, vertical: 8),
                            child: NewProductOrderItem(
                              orderData: model.products[index],
                              tickets: model.orderList?.tickets,
                            ),
                          );
                        },
                        separatorBuilder: (context, index) {
                          if (index != model.products.length - 1 &&
                              model.products[index + 1].type == 'bundled') {
                            return const SizedBox();
                          }
                          return Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8),
                            child: Row(
                              children: List.generate(
                                150 ~/ 1,
                                (index) => Expanded(
                                  child: Container(
                                    color: index % 2 == 0
                                        ? Colors.transparent
                                        : Colors.grey,
                                    height: 1,
                                  ),
                                ),
                              ),
                            ),
                          );
                        },
                        itemCount: model.products.length,
                      ),
                    ),
                  ),
                  if (order.billingConfirmSpeedSw == '是')
                    Row(
                      children: const [
                        Spacer(),
                        Checkbox(value: true, onChanged: null),
                        Text(
                          '此訂單做',
                          style: TextStyle(color: Colors.black54),
                        ),
                        Text(
                          '「快速晉升訂單」',
                          style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  const SizedBox(height: 16),
                  if (model.coupons.isNotEmpty) ...[
                    Container(
                      color: Colors.white,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 16),
                      child: Table(
                        children: [
                          const TableRow(
                            decoration: BoxDecoration(
                                color: Colors.black54,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10),
                                  topRight: Radius.circular(10),
                                )),
                            children: [
                              Padding(
                                padding: EdgeInsets.all(8),
                                child: Text(
                                  '優惠券代碼',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.all(8),
                                child: Text(
                                  '內容',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                          for (var element in model.coupons)
                            TableRow(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8),
                                  child: Text(
                                    element.code ?? '',
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8),
                                  child: Text(
                                    element.content ?? '',
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            )
                        ],
                      ),
                    ),
                    const SizedBox(height: 16),
                  ],

                  Container(
                    color: Colors.white,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16, vertical: 16),
                    child: Row(
                      children: [
                        Image.asset(
                          'assets/icons/from_lanfar/Artboard – 20.png',
                          color: Theme.of(context).primaryColor,
                          height: 24,
                          width: 24,
                        ),
                        const SizedBox(width: 4),
                        const Text('訂購類別'),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              order.orderType ?? '',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 1),
                  Container(
                    color: Colors.white,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16, vertical: 16),
                    child: Row(
                      children: [
                        Image.asset(
                          'assets/icons/from_lanfar/Artboard – 3.png',
                          color: Theme.of(context).primaryColor,
                          height: 24,
                          width: 24,
                        ),
                        const SizedBox(width: 4),
                        const Text('訂購方式'),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              order.orderSource ?? '',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 1),
                  Container(
                    color: Colors.white,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16, vertical: 16),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Image.asset(
                              'assets/icons/from_lanfar/Artboard – 4.png',
                              color: Theme.of(context).primaryColor,
                              height: 24,
                              width: 24,
                            ),
                            const SizedBox(width: 4),
                            const Text('取貨方式'),
                            const SizedBox(width: 8),
                            Expanded(
                              child: Container(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  order.shippingMethod ?? '',
                                ),
                              ),
                            ),
                          ],
                        ),
                        if (order.shippingName != null &&
                            order.shippingName!.isNotEmpty)
                          Container(
                            padding: const EdgeInsets.only(left: 28, top: 16),
                            width: double.maxFinite,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('${order.shippingName}'
                                    // ' ${order.alias!.isNotEmpty ? '/' : ''} ${order.alias}'
                                    ),
                                Text(
                                  order.shippingPhone ?? '',
                                  style: Theme.of(context)
                                      .textTheme
                                      .caption!
                                      .copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                                ),
                                Text(
                                  order.shippingAddress ?? '',
                                  style: Theme.of(context)
                                      .textTheme
                                      .caption!
                                      .copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                                )
                              ],
                            ),
                          ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 1),
                  Container(
                    color: Colors.white,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16, vertical: 16),
                    child: Row(
                      children: [
                        Image.asset(
                          'assets/icons/from_lanfar/Artboard – 6.png',
                          color: Theme.of(context).primaryColor,
                          height: 24,
                          width: 24,
                        ),
                        const SizedBox(width: 4),
                        const Text('發票寄送'),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              order.billingInvoiceDelivery ?? '',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 1),
                  Container(
                    color: Colors.white,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16, vertical: 16),
                    child: Row(
                      children: [
                        Icon(
                          Icons.receipt_long,
                          color: Theme.of(context).primaryColor,
                        ),
                        const SizedBox(width: 4),
                        const Text('發票列印'),
                        const SizedBox(width: 16),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              order.billingInvoiceNo ?? '',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 1),
                  Container(
                    color: Colors.white,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16, vertical: 16),
                    child: Row(
                      children: [
                        Icon(
                          Icons.local_mall_outlined,
                          color: Theme.of(context).primaryColor,
                        ),
                        const SizedBox(width: 4),
                        const Text('DM及提袋'),
                        const SizedBox(width: 16),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              order.billingNeedBagAndDm ?? '',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 1),
                  Container(
                    color: Colors.white,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16, vertical: 16),
                    child: Row(
                      children: [
                        Icon(
                          Icons.message,
                          color: Theme.of(context).primaryColor,
                        ),
                        const SizedBox(width: 4),
                        const Text('備註'),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              order.customerNote ?? '',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 16),
                  Container(
                    color: Colors.white,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16, vertical: 16),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text('訂單金額(${model.count}商品)'),
                        ),
                        Expanded(
                          child: Text(
                            PriceTools.getCurrencyFormatted(
                                    order.orderSubTotal!, null) ??
                                '',
                            style: const TextStyle(color: primaryColor),
                            textAlign: TextAlign.end,
                          ),
                        ),
                      ],
                    ),
                  ),
                  if (order.tickets == null) ...[
                    const SizedBox(height: 1),
                    Container(
                      color: Colors.white,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 16),
                      child: Row(
                        children: [
                          const Expanded(
                            child: Text('PV分數'),
                          ),
                          Expanded(
                            child: Text(
                              '${PriceTools.getCurrencyFormatted(order.pvTotal, null, isPV: true) ?? ''} PV',
                              style: const TextStyle(color: primaryColor),
                              textAlign: TextAlign.end,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],

                  const SizedBox(height: 16),
                  Container(
                    padding: const EdgeInsets.all(16),
                    color: Colors.white,
                    child: Column(
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const Icon(Icons.monetization_on,
                                color: primaryColor),
                            const SizedBox(width: 4),
                            const Expanded(
                              child: Text('付款方式'),
                            ),
                            Expanded(
                              child: Text(
                                order.paymentMethod ?? '',
                                style: const TextStyle(color: primaryColor),
                                textAlign: TextAlign.end,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 16),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '商品總金額',
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                            ),
                            const SizedBox(width: 8),
                            Expanded(
                              child: Container(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  PriceTools.getCurrencyFormatted(
                                          order.orderSubTotal, null) ??
                                      '',
                                  style: Theme.of(context)
                                      .textTheme
                                      .caption!
                                      .copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 4),
                        if (order.discountTotal != null &&
                            order.discountTotal!.isNotEmpty &&
                            order.discountTotal != '0') ...[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                '折扣金額',
                                style: Theme.of(context)
                                    .textTheme
                                    .caption!
                                    .copyWith(
                                      fontSize: 14.0,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary
                                          .withOpacity(0.8),
                                    ),
                              ),
                              const SizedBox(width: 8),
                              Expanded(
                                child: Container(
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    '-' +
                                        (PriceTools.getCurrencyFormatted(
                                                order.discountTotal ?? 0,
                                                null) ??
                                            ''),
                                    style: Theme.of(context)
                                        .textTheme
                                        .caption!
                                        .copyWith(
                                          fontSize: 14.0,
                                          color: Theme.of(context)
                                              .colorScheme
                                              .secondary
                                              .withOpacity(0.8),
                                        ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 4),
                        ],
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '運費總金額',
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                            ),
                            const SizedBox(width: 8),
                            Expanded(
                              child: Container(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  PriceTools.getCurrencyFormatted(
                                          order.shippingTotal, null) ??
                                      '',
                                  style: Theme.of(context)
                                      .textTheme
                                      .caption!
                                      .copyWith(
                                        fontSize: 14.0,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary
                                            .withOpacity(0.8),
                                      ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 8),
                        Container(
                          width: double.maxFinite,
                          height: 1,
                          color: Theme.of(context).primaryColor,
                        ),
                        const SizedBox(height: 8),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '總金額',
                              style:
                                  Theme.of(context).textTheme.caption!.copyWith(
                                        fontSize: 16.0,
                                      ),
                            ),
                            const SizedBox(width: 8),
                            Expanded(
                              child: Container(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  PriceTools.getCurrencyFormatted(
                                          model.totalPrice, null) ??
                                      '',
                                  style: Theme.of(context)
                                      .textTheme
                                      .caption!
                                      .copyWith(
                                        fontSize: 16.0,
                                        color: Theme.of(context).primaryColor,
                                      ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16),
                    child: Row(
                      children: [
                        if (order.orderCat != 'CourseOrder')
                          Expanded(
                            child: GestureDetector(
                              onTap: () async {
                                if (newOrderHistoryDetailModel
                                    .payAgain.isNotEmpty) {
                                  await Navigator.of(App.fluxStoreNavigatorKey
                                          .currentContext!)
                                      .push(
                                    MaterialPageRoute(
                                      builder: (context) => PaymentWebview(
                                        url:
                                            newOrderHistoryDetailModel.payAgain,
                                        onFinish: (v) {
                                          if (v != null) {
                                            Utils.showSnackBar(context,
                                                text: '付款成功！');
                                            newOrderHistoryDetailModel.fetch(
                                              widget.orderId,
                                              context
                                                      .read<UserModel>()
                                                      .user
                                                      ?.username ??
                                                  '',
                                            );
                                          }
                                        },
                                      ),
                                    ),
                                  );
                                } else {
                                  unawaited(
                                    showDialog(
                                      barrierDismissible: false,
                                      context: context,
                                      builder: (context) => WillPopScope(
                                          child: kLoadingWidget(context),
                                          onWillPop: () async {
                                            return false;
                                          }),
                                    ),
                                  );
                                  var result = await Services().api.reOrder(
                                        userId: context
                                                .read<UserModel>()
                                                .user
                                                ?.username ??
                                            '',
                                        orderId: widget.orderId,
                                      );
                                  if (result == null) {
                                  } else {
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(SnackBar(
                                      content: Text(result),
                                    ));
                                  }
                                  widget.onRefresh?.call();
                                  Navigator.of(context).popUntil(
                                      ModalRoute.withName(RouteList.dashboard));
                                  MainTabControlDelegate.getInstance()
                                      .changeTab(
                                          RouteList.cart.replaceFirst('/', ''));
                                }
                              },
                              child: Container(
                                alignment: Alignment.center,
                                padding:
                                    const EdgeInsets.symmetric(vertical: 12),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                      color: Theme.of(context).primaryColor),
                                ),
                                child: Text(
                                  newOrderHistoryDetailModel.payAgain.isNotEmpty
                                      ? '再次付款'
                                      : '再次下單',
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColor,
                                      fontSize: 16),
                                ),
                              ),
                            ),
                          ),
                        const SizedBox(width: 16),
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              // Navigator.of(
                              //         App.fluxStoreNavigatorKey.currentContext!)
                              //     .push(
                              //   MaterialPageRoute(
                              //     builder: (context) => WebViewWithCookie(
                              //       url:
                              //           '${environment['serverConfig']['url']}/my-accounts/order-consultation-service/?order_id=${order.orderId}&',
                              //       multiPram: true,
                              //     ),
                              //   ),
                              // );
                              Navigator.of(
                                      App.fluxStoreNavigatorKey.currentContext!)
                                  .push(
                                MaterialPageRoute(
                                  builder: (context) => ContactPage(
                                    type: ContactType.order,
                                    orderId: widget.orderId,
                                  ),
                                ),
                              );
                            },
                            child: Container(
                              alignment: Alignment.center,
                              padding: const EdgeInsets.symmetric(vertical: 12),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(
                                    color: Theme.of(context).primaryColor),
                              ),
                              child: Text(
                                '訂單諮詢',
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                    fontSize: 16),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  const SizedBox(height: 50),

                  /// Render the Cancel and Refund
                  // if (kPaymentConfig['EnableRefundCancel'])
                  //   Services()
                  //       .widget
                  //       .renderButtons(context, order, cancelOrder, refundOrder),
                  //
                  // const SizedBox(height: 20),

                  // if (order.billing != null) ...[
                  //   Text(S.of(context).shippingAddress,
                  //       style: const TextStyle(
                  //           fontSize: 18, fontWeight: FontWeight.bold)),
                  //   const SizedBox(height: 10),
                  //   Text(
                  //     ((order.billing!.apartment?.isEmpty ?? true)
                  //             ? ''
                  //             : '${order.billing!.apartment} ') +
                  //         ((order.billing!.block?.isEmpty ?? true)
                  //             ? ''
                  //             : '${(order.billing!.apartment?.isEmpty ?? true) ? '' : '- '} ${order.billing!.block}, ') +
                  //         order.billing!.street! +
                  //         ', ' +
                  //         order.billing!.city! +
                  //         ', ' +
                  //         getCountryName(order.billing!.country),
                  //   ),
                  // ],
                  // if (order.status == OrderStatus.processing &&
                  //     kPaymentConfig['EnableRefundCancel'])
                  //   Column(
                  //     children: <Widget>[
                  //       const SizedBox(height: 30),
                  //       Row(
                  //         children: [
                  //           Expanded(
                  //             child: ButtonTheme(
                  //               height: 45,
                  //               child: ElevatedButton(
                  //                   style: ElevatedButton.styleFrom(
                  //                     onPrimary: Colors.white,
                  //                     primary: HexColor('#056C99'),
                  //                   ),
                  //                   onPressed: refundOrder,
                  //                   child: Text(
                  //                       S.of(context).refundRequest.toUpperCase(),
                  //                       style: const TextStyle(
                  //                           fontWeight: FontWeight.w700))),
                  //             ),
                  //           )
                  //         ],
                  //       ),
                  //       const SizedBox(height: 10),
                  //     ],
                  //   ),
                  // if (kPaymentConfig['ShowOrderNotes'] ?? true)
                  //   Padding(
                  //     padding: const EdgeInsets.only(top: 20.0),
                  //     child: Builder(
                  //       builder: (context) {
                  //         final listOrderNote = model.listOrderNote;
                  //         if (listOrderNote?.isEmpty ?? true) {
                  //           return const SizedBox();
                  //         }
                  //         return Column(
                  //           crossAxisAlignment: CrossAxisAlignment.start,
                  //           children: <Widget>[
                  //             Text(
                  //               S.of(context).orderNotes,
                  //               style: const TextStyle(
                  //                   fontSize: 18, fontWeight: FontWeight.bold),
                  //             ),
                  //             const SizedBox(height: 10),
                  //             Column(
                  //               crossAxisAlignment: CrossAxisAlignment.start,
                  //               children: [
                  //                 ...List.generate(
                  //                   listOrderNote!.length,
                  //                   (index) {
                  //                     return Padding(
                  //                       padding: const EdgeInsets.only(bottom: 15),
                  //                       child: Column(
                  //                         crossAxisAlignment:
                  //                             CrossAxisAlignment.start,
                  //                         children: <Widget>[
                  //                           CustomPaint(
                  //                             painter: BoxComment(
                  //                                 color: Theme.of(context)
                  //                                     .primaryColor),
                  //                             child: SizedBox(
                  //                               width: MediaQuery.of(context)
                  //                                   .size
                  //                                   .width,
                  //                               child: Padding(
                  //                                 padding: const EdgeInsets.only(
                  //                                     left: 10,
                  //                                     right: 10,
                  //                                     top: 15,
                  //                                     bottom: 25),
                  //                                 child: Linkify(
                  //                                   text:
                  //                                       listOrderNote[index].note!,
                  //                                   style: const TextStyle(
                  //                                       color: Colors.white,
                  //                                       fontSize: 13,
                  //                                       height: 1.2),
                  //                                   onOpen: (link) async {
                  //                                     await Tools.launchURL(
                  //                                         link.url);
                  //                                   },
                  //                                 ),
                  //                               ),
                  //                             ),
                  //                           ),
                  //                           Text(
                  //                             formatTime(DateTime.parse(
                  //                                 listOrderNote[index]
                  //                                     .dateCreated!)),
                  //                             style: const TextStyle(fontSize: 13),
                  //                           )
                  //                         ],
                  //                       ),
                  //                     );
                  //                   },
                  //                 ),
                  //                 const SizedBox(height: 100),
                  //               ],
                  //             ),
                  //           ],
                  //         );
                  //       },
                  //     ),
                  //   ),
                  //
                  // const SizedBox(height: 50)
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  String getCountryName(country) {
    try {
      return CountryPickerUtils.getCountryByIsoCode(country).name;
    } catch (err) {
      return country;
    }
  }

  void _showLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Center(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white30,
              borderRadius: BorderRadius.circular(5.0),
            ),
            padding: const EdgeInsets.all(50.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                kLoadingWidget(context),
                // const SizedBox(height: 48),
                ElevatedButton(
                  onPressed: Navigator.of(context).pop,
                  child: Text(S.of(context).cancel),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void _hideLoading() {
    Navigator.of(context).pop();
  }

  String formatTime(DateTime time) {
    return DateFormat('dd/MM/yyyy, HH:mm').format(time);
  }

  Product jsonParser(item) {
    var product = Product.fromJson(item);
    if (item['store'] != null) {
      if (item['store']['errors'] == null) {
        product = Services().widget.updateProductObject(product, item);
      }
    }
    return product;
  }
}
