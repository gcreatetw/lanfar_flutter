import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:the_apple_sign_in/the_apple_sign_in.dart';
import 'package:wp_notify/models/responses/WPStoreTokenResponse.dart';
import 'package:wp_notify/models/responses/WPUpdateTokenResponse.dart';
import 'package:wp_notify/wp_notify.dart';

import '../../app.dart';
import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../from_lanfar/screens/reset_password_page.dart';
import '../../from_lanfar/utils/local_auth_utils.dart';
import '../../from_lanfar/utils/utils.dart';
import '../../generated/l10n.dart';
import '../../models/index.dart';
import '../../modules/firebase/firebase_service.dart';
import '../../modules/sms_login/sms_login.dart';
import '../../services/index.dart';
import '../../widgets/common/login_animation.dart';
import '../../widgets/common/webview.dart';
import '../base_screen.dart';
import 'forgot_password_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen();

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends BaseScreen<LoginScreen>
    with TickerProviderStateMixin {
  late AnimationController _loginButtonController;
  final TextEditingController username = TextEditingController();
  final TextEditingController password = TextEditingController();

  final usernameNode = FocusNode();
  final passwordNode = FocusNode();

  late var parentContext;
  bool isLoading = false;
  bool isAvailableApple = false;
  bool isActiveAudio = false;

  bool hide = true;

  AudioService get audioPlayerService => injector<AudioService>();

  WPStoreTokenResponse? wpStoreTokenResponse;
  WPUpdateTokenResponse? wpUpdateTokenResponse;

  @override
  void initState() {
    super.initState();
    _loginButtonController = AnimationController(
        duration: const Duration(milliseconds: 3000), vsync: this);
    var prefs = injector<SharedPreferences>();
    final u = prefs.getString('username');
    final p = prefs.getString('password');
    username.text = u ?? '';
    // password.text = p ?? '';
    if (!Provider.of<UserModel>(context, listen: false).showBio) return;
    bioAuth(isStart: true);
  }

  @override
  Future<void> afterFirstLayout(BuildContext context) async {
    if (audioPlayerService.isStickyAudioWidgetActive) {
      isActiveAudio = true;
      audioPlayerService
        ..pause()
        ..updateStateStickyAudioWidget(false);
    }
    try {
      isAvailableApple = await TheAppleSignIn.isAvailable();
      setState(() {});
    } catch (e) {
      printLog('[Login] afterFirstLayout error');
    }
  }

  @override
  void dispose() async {
    _loginButtonController.dispose();
    username.dispose();
    password.dispose();
    usernameNode.dispose();
    passwordNode.dispose();
    super.dispose();
  }

  Future _playAnimation() async {
    try {
      setState(() {
        isLoading = true;
      });
      await _loginButtonController.forward();
    } on TickerCanceled {
      printLog('[_playAnimation] error');
    }
  }

  Future _stopAnimation() async {
    try {
      await _loginButtonController.reverse();
      setState(() {
        isLoading = false;
      });
    } on TickerCanceled {
      printLog('[_stopAnimation] error');
    }
  }

  Future _welcomeMessage(user, context) async {
    // if (widget.fromCart) {
    //   Navigator.of(context).pop();
    // } else {
    //   if (user.name != null) {
    //     Tools.showSnackBar(
    //         Scaffold.of(context), S.of(context).welcome + ' ${user.name} !');
    //   }
    //   final canPop = ModalRoute.of(context)!.canPop;
    //   if (canPop) {
    //     // When not required login
    //     Navigator.of(context).pop();
    //   } else {
    //     // When required login
    //     await Navigator.of(App.fluxStoreNavigatorKey.currentContext!)
    //         .pushReplacementNamed(RouteList.dashboard);
    //   }
    // }
    // if (user.name != null) {
    //   Tools.showSnackBar(
    //       Scaffold.of(context), S.of(context).welcome + ' ${user.name} !');
    // }
    final canPop = ModalRoute.of(context)!.canPop;
    if (canPop) {
      // When not required login
      Navigator.of(context).pop();
    } else {
      // When required login
      await Navigator.of(App.fluxStoreNavigatorKey.currentContext!)
          .pushReplacementNamed(
        RouteList.dashboard,
        arguments: {'isLogin': username.text == password.text ? true : false},
      );
    }
  }

  void _failMessage(message, context) {
    /// Showing Error messageSnackBarDemo
    /// Ability so close message
    final snackBar = SnackBar(
      content: Text(message),
      duration: const Duration(seconds: 30),
      action: SnackBarAction(
        label: S.of(context).close,
        onPressed: () {
          // Some code to undo the change.
        },
        textColor: Colors.white,
      ),
    );

    ScaffoldMessenger.of(context)
      // ignore: deprecated_member_use
      ..removeCurrentSnackBar()
      // ignore: deprecated_member_use
      ..showSnackBar(snackBar);
  }

  void _loginFacebook(context) async {
    //showLoading();
    await _playAnimation();
    await Provider.of<UserModel>(context, listen: false).loginFB(
        success: (user) {
          //hideLoading();
          _stopAnimation();
          _welcomeMessage(user, context);
        },
        fail: (message) {
          //hideLoading();
          _stopAnimation();
          _failMessage(message, context);
        },
        context: context);
  }

  void _loginApple(context) async {
    await _playAnimation();
    await Provider.of<UserModel>(context, listen: false).loginApple(
        success: (user) {
          _stopAnimation();
          _welcomeMessage(user, context);
        },
        fail: (message) {
          _stopAnimation();
          _failMessage(message, context);
        },
        context: context);
  }

  @override
  Widget build(BuildContext context) {
    parentContext = context;
    final appModel = Provider.of<AppModel>(context);
    // final screenSize = MediaQuery.of(context).size;
    // final themeConfig = appModel.themeConfig;
    //
    // var forgetPasswordUrl = Config().forgetPassword;

    var prefs = injector<SharedPreferences>();
    final p = prefs.getString('password');
    final u = prefs.getString('username');

    Future launchForgetPassworddWebView(String url) async {
      await Navigator.push(
        context,
        MaterialPageRoute<void>(
          builder: (BuildContext context) =>
              WebView(url: url, title: S.of(context).resetPassword),
          fullscreenDialog: true,
        ),
      );
    }

    void launchForgetPasswordURL(String? url) async {
      if (url != null && url != '') {
        /// show as webview
        await launchForgetPassworddWebView(url);
      } else {
        /// show as native
        await Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ForgotPasswordScreen()),
        );
      }
    }

    void _login(context) async {
      if (username.text.isEmpty || password.text.isEmpty) {
        Tools.showSnackBar(
            ScaffoldMessenger.of(context), S.of(context).pleaseInput);
      } else {
        await _playAnimation();

        await Provider.of<UserModel>(context, listen: false).login(
          username: username.text.trim(),
          password: password.text.trim(),
          success: (user) async {
            if (username.text == password.text) {
              await showDialog(
                context: context,
                useRootNavigator: false,
                builder: (BuildContext context) {
                  return AlertDialog(
                    // title: const Text('複製地址'),
                    content: const Text('由於您的帳號與密碼一樣，為了安全起見，建議您修改密碼。'),
                    actions: <Widget>[
                      TextButton(
                        onPressed: () async {
                          Navigator.of(context).pop(true);
                        },
                        child: Text(
                          '確認',
                          style:
                              TextStyle(color: Theme.of(context).primaryColor),
                        ),
                      )
                    ],
                  );
                },
              );
            }
            var fcmToken = await FirebaseServices().getMessagingToken();
            try {
              wpStoreTokenResponse = await WPNotifyAPI.instance.api(
                (request) => request.wpNotifyStoreToken(
                  token: fcmToken!,
                  userId: int.parse(
                    Provider.of<UserModel>(context, listen: false).user!.id!,
                  ),
                ),
              );
              await _welcomeMessage(user, context);
              await _stopAnimation();
            } on Exception catch (e) {
              print(e);
              await _welcomeMessage(user, context);
              await _stopAnimation();
            }
            try{
              await WPNotifyAPI.instance.api((request) =>
                  request.wpNotifyUpdateToken(token: fcmToken!, status: true));
            }catch(_){

            }
          },
          fail: (message) {
            _stopAnimation();
            _failMessage(message, context);
            // if (message.toString().contains('401')) {
            //   _failMessage('經銷商編號或密碼錯誤', context);
            // } else {
            //   _failMessage('發生錯誤，請稍後再試', context);
            // }
          },
        );
      }
    }

    void _loginSMS(context) {
      final supportedPlatforms = [
        'wcfm',
        'dokan',
        'delivery',
        'vendorAdmin',
        'woo'
      ].contains(serverConfig['type']);
      if (supportedPlatforms &&
          (kAdvanceConfig['EnableNewSMSLogin'] ?? false)) {
        final model = Provider.of<UserModel>(context, listen: false);
        Navigator.of(context).push(
          MaterialPageRoute(
              builder: (_) => SMSLoginScreen(onSuccess: (user) async {
                    await model.saveSMSUser(user);
                    Navigator.of(context).pop();
                    await _welcomeMessage(user, context);
                  })),
        );
        return;
      }

      Navigator.of(context).pushNamed(
        RouteList.loginSMS,
        // arguments: widget.fromCart,
      );
    }

    void _loginGoogle(context) async {
      await _playAnimation();
      await Provider.of<UserModel>(context, listen: false).loginGoogle(
          success: (user) {
            //hideLoading();
            _stopAnimation();
            _welcomeMessage(user, context);
          },
          fail: (message) {
            //hideLoading();
            _stopAnimation();
            _failMessage(message, context);
          },
          context: context);
    }

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Builder(
        builder: (context) => GestureDetector(
          onTap: () => Tools.hideKeyboard(context),
          behavior: HitTestBehavior.opaque,
          child: ListenableProvider.value(
            value: Provider.of<UserModel>(context),
            child: Consumer<UserModel>(builder: (context, model, child) {
              return SingleChildScrollView(
                padding: const EdgeInsets.symmetric(horizontal: 0.0),
                child: Container(
                  height: MediaQuery.of(context).size.height < 600
                      ? 600
                      : MediaQuery.of(context).size.height,
                  alignment: Alignment.topCenter,
                  child: AutofillGroup(
                    child: Column(
                      children: <Widget>[
                        Stack(
                          alignment: Alignment.bottomCenter,
                          children: [
                            Column(
                              children: [
                                Image.asset(
                                  'assets/icons/from_lanfar/Image 3@2x.png',
                                  fit: BoxFit.fitWidth,
                                ),
                                Container(
                                  height: 5,
                                  color: Theme.of(context).backgroundColor,
                                ),
                              ],
                            ),
                            Container(
                              margin: const EdgeInsets.only(bottom: 5),
                              height: 25,
                              decoration: BoxDecoration(
                                color: Theme.of(context).backgroundColor,
                                borderRadius: const BorderRadius.only(
                                  topRight: Radius.circular(15),
                                  topLeft: Radius.circular(15),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black.withOpacity(0.2),
                                    spreadRadius: 1,
                                    blurRadius: 3,
                                    offset: const Offset(
                                        0, -1), // changes position of shadow
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              height: 10,
                              color: Theme.of(context).backgroundColor,
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const <Widget>[
                              SizedBox(width: double.maxFinite),
                              Text(
                                '登入',
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 4),
                              Text(
                                '連法國際APP 歡迎您，提供優質保健產品，期許有更多人因接觸賜多利而獲得全方位健康',
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Color(0xff666666),
                                ),
                              ),
                              Text(
                                '若經開通完成後欲變更手機號碼，請透過客服諮詢進行申請',
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Color(0xff666666),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(height: 20.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: TextField(
                            key: const Key('loginEmailField'),
                            controller: username,
                            autocorrect: false,
                            enableSuggestions: false,
                            textInputAction: TextInputAction.next,
                            textCapitalization: TextCapitalization.characters,
                            inputFormatters: [
                              UpperCaseTextFormatter(),
                            ],
                            onSubmitted: (_) => FocusScope.of(context)
                                .requestFocus(passwordNode),
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 16),
                              prefixIcon: const Icon(
                                Icons.person_outline,
                                color: primaryColor,
                              ),
                              hintText: '請輸入經銷商編號',
                              focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                    color: Colors.black,
                                    width: 0.5,
                                  ),
                                  borderRadius: BorderRadius.circular(15)),
                              enabledBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                  color: Colors.black,
                                  width: 0.5,
                                ),
                                borderRadius: BorderRadius.circular(15),
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 20.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: TextField(
                            key: const Key('loginPasswordField'),
                            obscureText: hide,
                            obscuringCharacter: '*',
                            textInputAction: TextInputAction.done,
                            keyboardType: TextInputType.visiblePassword,
                            controller: password,
                            focusNode: passwordNode,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 16),
                              prefixIcon: const Icon(
                                Icons.lock_outline,
                                color: primaryColor,
                              ),
                              suffixIcon: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    if (hide) {
                                      hide = false;
                                    } else {
                                      hide = true;
                                    }
                                  });
                                },
                                child: Icon(
                                  hide
                                      ? Icons.visibility_off_outlined
                                      : Icons.visibility_outlined,
                                  color: Colors.grey,
                                ),
                              ),
                              hintText: '請輸入您的密碼',
                              focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                    color: Colors.black,
                                    width: 0.5,
                                  ),
                                  borderRadius: BorderRadius.circular(15)),
                              enabledBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                  color: Colors.black,
                                  width: 0.5,
                                ),
                                borderRadius: BorderRadius.circular(15),
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 4),
                        Container(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          alignment: Alignment.centerRight,
                          child: GestureDetector(
                            onTap: () async {
                              var changed = await Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) =>
                                      const ResetPasswordPage(),
                                ),
                              );

                              ScaffoldMessenger.of(context)
                                  .removeCurrentSnackBar();
                              if (changed ?? false) {
                                Tools.showSnackBar(
                                    ScaffoldMessenger.of(context),
                                    '密碼已更新，請重新登入');
                              }
                            },
                            child: Text(
                              '您是否忘記密碼?',
                              style: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontSize: 14),
                            ),
                          ),
                        ),
                        const SizedBox(height: 30.0),
                        StaggerAnimation(
                          key: const Key('loginSubmitButton'),
                          titleButton: S.of(context).signInWithEmail,
                          buttonController: _loginButtonController.view
                              as AnimationController,
                          onTap: () {
                            if (!isLoading) {
                              _login(context);
                            }
                          },
                        ),
                        const SizedBox(height: 20),
                        if (!(u == null ||
                                p == null ||
                                u.isEmpty ||
                                p.isEmpty) &&
                            !isLoading)
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              elevation: 0,
                              shadowColor: Colors.transparent,
                              primary: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                                side: const BorderSide(
                                    color: primaryColor, width: 1),
                              ),
                            ),
                            onPressed: bioAuth,
                            child: const Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 18),
                              child: Text(
                                '使用生物辨識登入',
                                style: TextStyle(color: primaryColor),
                              ),
                            ),
                          ),
                        // Stack(
                        //   alignment: AlignmentDirectional.center,
                        //   children: <Widget>[
                        //     SizedBox(
                        //         height: 50.0,
                        //         width: 200.0,
                        //         child: Divider(
                        //             color: Colors.grey.shade300)),
                        //     Container(
                        //         height: 30,
                        //         width: 40,
                        //         color:
                        //             Theme.of(context).backgroundColor),
                        //     if (kLoginSetting['showFacebook'] ||
                        //         kLoginSetting['showSMSLogin'] ||
                        //         kLoginSetting['showGoogleLogin'] ||
                        //         kLoginSetting['showAppleLogin'])
                        //       Text(
                        //         S.of(context).or,
                        //         style: TextStyle(
                        //             fontSize: 12,
                        //             color: Colors.grey.shade400),
                        //       )
                        //   ],
                        // ),
                        // Row(
                        //   mainAxisAlignment:
                        //       MainAxisAlignment.spaceAround,
                        //   children: <Widget>[
                        //     if (kLoginSetting['showAppleLogin'] &&
                        //         isAvailableApple)
                        //       InkWell(
                        //         onTap: () => _loginApple(context),
                        //         child: Container(
                        //           padding: const EdgeInsets.all(12),
                        //           decoration: BoxDecoration(
                        //             borderRadius:
                        //                 BorderRadius.circular(40),
                        //             color: Colors.black87,
                        //           ),
                        //           child: Image.asset(
                        //             'assets/icons/logins/apple.png',
                        //             width: 26,
                        //             height: 26,
                        //           ),
                        //         ),
                        //       ),
                        //     if (kLoginSetting['showFacebook'])
                        //       InkWell(
                        //         onTap: () => _loginFacebook(context),
                        //         child: Container(
                        //           padding: const EdgeInsets.all(8),
                        //           decoration: BoxDecoration(
                        //             borderRadius:
                        //                 BorderRadius.circular(40),
                        //             color: const Color(0xFF4267B2),
                        //           ),
                        //           child: const Icon(
                        //             Icons.facebook_rounded,
                        //             color: Colors.white,
                        //             size: 34.0,
                        //           ),
                        //         ),
                        //       ),
                        //     if (kLoginSetting['showGoogleLogin'])
                        //       InkWell(
                        //         onTap: () => _loginGoogle(context),
                        //         child: Container(
                        //           padding: const EdgeInsets.all(12),
                        //           decoration: BoxDecoration(
                        //             borderRadius:
                        //                 BorderRadius.circular(40),
                        //             color: Colors.grey.shade300,
                        //           ),
                        //           child: Image.asset(
                        //             'assets/icons/logins/google.png',
                        //             width: 28,
                        //             height: 28,
                        //           ),
                        //         ),
                        //       ),
                        //     if (kLoginSetting['showSMSLogin'])
                        //       InkWell(
                        //         onTap: () => _loginSMS(context),
                        //         child: Container(
                        //           padding: const EdgeInsets.all(12),
                        //           decoration: BoxDecoration(
                        //             borderRadius:
                        //                 BorderRadius.circular(40),
                        //             color: Colors.lightBlue.shade50,
                        //           ),
                        //           child: Image.asset(
                        //             'assets/icons/logins/sms.png',
                        //             width: 28,
                        //             height: 28,
                        //           ),
                        //         ),
                        //       ),
                        //   ],
                        // ),
                        // const SizedBox(
                        //   height: 30.0,
                        // ),
                        const SizedBox(height: 30.0),
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Center(
                                child: RichText(
                                  text: TextSpan(
                                    text: '經銷商首次登入，請先',
                                    style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 16,
                                    ),
                                    children: [
                                      TextSpan(
                                        text: '開通會員',
                                        style: TextStyle(
                                          color: Theme.of(context).primaryColor,
                                        ),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () async {
                                            if (kAdvanceConfig[
                                                    'EnableMembershipUltimate'] ==
                                                true) {
                                              await Navigator.of(context)
                                                  .pushNamed(RouteList
                                                      .memberShipUltimatePlans);
                                            } else {
                                              await Navigator.of(context)
                                                  .pushNamed(
                                                      RouteList.register);
                                            }
                                            ScaffoldMessenger.of(context)
                                                .removeCurrentSnackBar();
                                          },
                                      ),
                                      const TextSpan(text: '。'),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                  height: 16 +
                                      MediaQuery.of(context).padding.bottom)
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }),
          ),
        ),
      ),

      // bottomNavigationBar: Column(
      //   mainAxisSize: MainAxisSize.min,
      //   children: <Widget>[
      //     Row(
      //       mainAxisAlignment: MainAxisAlignment.center,
      //       children: <Widget>[
      //         Text(S.of(context).dontHaveAccount),
      //         GestureDetector(
      //           onTap: () {
      //             if (kAdvanceConfig['EnableMembershipUltimate'] == true) {
      //               Navigator.of(context)
      //                   .pushNamed(RouteList.memberShipUltimatePlans);
      //             } else {
      //               Navigator.of(context).pushNamed(RouteList.register);
      //             }
      //           },
      //           child: Text(
      //             ' 註冊成為經銷商',
      //             style: TextStyle(
      //               color: Theme.of(context).primaryColor,
      //             ),
      //           ),
      //         ),
      //         const Text('吧 !'),
      //         const SizedBox(height: 60.0),
      //       ],
      //     ),
      //   ],
      // ),
    );
  }

  void showLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Center(
            child: Container(
          padding: const EdgeInsets.all(50.0),
          child: kLoadingWidget(context),
        ));
      },
    );
  }

  void hideLoading() {
    Navigator.of(context).pop();
  }

  final localAuth = LocalAuthUtils();

  Future<void> bioAuth({bool isStart = false}) async {
    var prefs = injector<SharedPreferences>();
    final password = prefs.getString('password');
    final username = prefs.getString('username');

    if (username == null ||
        password == null ||
        username.isEmpty ||
        password.isEmpty) return;
    var result = await localAuth.checkCanAuth();

    if (result) {
      var r = await localAuth.startAuth();
      switch (r) {
        case true:
          await _playAnimation();
          await Provider.of<UserModel>(context, listen: false).login(
            username: username,
            password: password,
            success: (user) async {
              var fcmToken = await FirebaseServices().getMessagingToken();
              try {
                wpStoreTokenResponse = await WPNotifyAPI.instance.api(
                  (request) => request.wpNotifyStoreToken(
                    token: fcmToken!,
                    userId: int.parse(
                      Provider.of<UserModel>(context, listen: false).user!.id!,
                    ),
                  ),
                );
                await _welcomeMessage(user, context);
                await _stopAnimation();
              } on Exception catch (e) {
                print(e);
                await _welcomeMessage(user, context);
                await _stopAnimation();
              }
              try{
                await WPNotifyAPI.instance.api((request) =>
                    request.wpNotifyUpdateToken(token: fcmToken!, status: true));
              }catch(_){

              }
            },
            fail: (message) {
              _stopAnimation();
              _failMessage(message, context);
              // if (message.toString().contains('401')) {
              //   _failMessage('經銷商編號或密碼錯誤', context);
              // } else {
              //   _failMessage('發生錯誤，請稍後再試', context);
              // }
            },
          );
          break;
        case false:
          break;
        case null:
          if (isStart) return;
          await showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: const Text('生物辨識'),
              content: const Text('您尚未設定生物辨識'),
              actions: [
                // TextButton(
                //   onPressed: () {
                //     Navigator.pop(context);
                //   },
                //   child: const Text(
                //     '取消',
                //     style: TextStyle(
                //       color: Colors.black54,
                //     ),
                //   ),
                // ),
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    // AppSettings.openLockAndPasswordSettings();
                  },
                  child: const Text('確定'),
                ),
              ],
            ),
          );
          break;
      }
    } else {
      Utils.showSnackBar(context, text: '您的裝置無法使用生物辨識');
    }
  }
}

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toUpperCase(),
      selection: newValue.selection,
    );
  }
}
