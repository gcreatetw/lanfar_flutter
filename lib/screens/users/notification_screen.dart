import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../from_lanfar/model/general_state.dart';
import '../../from_lanfar/model/notification_provider.dart';
import '../../from_lanfar/model/notification_response.dart';
import '../../from_lanfar/screens/notification_detail_page.dart';
import '../../models/user_model.dart';

class NotificationScreenNew extends StatefulWidget {
  const NotificationScreenNew({Key? key}) : super(key: key);

  @override
  State<NotificationScreenNew> createState() => _NotificationScreenNewState();
}

class _NotificationScreenNewState extends State<NotificationScreenNew>
    with TickerProviderStateMixin {
  final NotificationProvider notificationProvider = NotificationProvider();

  late TabController tabController;

  @override
  void initState() {
    tabController = TabController(length: 4, vsync: this);
    notificationProvider.fetch(
      context,
      userId: context.read<UserModel>().user!.id!,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: notificationProvider,
      child: Consumer(
          builder: (BuildContext context, NotificationProvider provider, _) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text(
              '訊息通知',
              style: TextStyle(
                color: Theme.of(context).colorScheme.secondary,
              ),
            ),
            bottom: TabBar(
              controller: tabController,
              indicatorColor: primaryColor,
              tabs: const [
                Tab(
                  text: '全部',
                ),
                Tab(
                  text: '我的通知',
                ),
                Tab(
                  text: '優惠訊息',
                ),
                Tab(
                  text: '公告訊息',
                ),
              ],
            ),
          ),
          body: body(provider),
        );
      }),
    );
  }

  Widget body(NotificationProvider provider) {
    switch (provider.state) {
      case GeneralState.loading:
        return Center(
          child: kLoadingWidget(context),
        );
      case GeneralState.finish:
        return TabBarView(
          controller: tabController,
          children: [
            _notificationBody(provider.all),
            _notificationBody(provider.myList),
            _notificationBody(provider.discountList),
            _notificationBody(provider.announceList),
          ],
        );

      case GeneralState.error:
        return const Center(
          child: Text('發生錯誤，請稍後再試'),
        );
    }
  }

  Widget _notificationBody(List<NotificationModel> list) {
    if (list.isEmpty) {
      return const Center(
        child: Text('目前沒有任何訊息'),
      );
    }
    return SafeArea(
      child: ListView.builder(
        padding: const EdgeInsets.only(
          top: 8,
          left: 8,
          right: 8,
        ),
        itemCount: list.length,
        itemBuilder: (context, index) {
          return NotificationListTitle(model: list[index]);
        },
      ),
    );
  }
}

class NotificationListTitle extends StatefulWidget {
  final NotificationModel model;

  const NotificationListTitle({
    Key? key,
    required this.model,
  }) : super(key: key);

  @override
  State<NotificationListTitle> createState() => _NotificationListTitleState();
}

class _NotificationListTitleState extends State<NotificationListTitle> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => NotificationDetailPage(
              model: widget.model,
              setRead: () {
                setState(() {
                  widget.model.readStatus = '1';
                });
              },
            ),
          ),
        );
      },
      child: Card(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Row(
            children: <Widget>[
              if (widget.model.image != null &&
                  widget.model.image!.isNotEmpty) ...[
                Image.network(
                  widget.model.image!,
                  height: 100.0,
                  width: 100.0,
                  fit: BoxFit.cover,
                ),
                const SizedBox(
                  width: 12.0,
                ),
              ],
              Expanded(
                child: SizedBox(
                  height: 100,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              widget.model.title ?? '',
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                fontSize: 16,
                                fontFamily: 'NotoSansCJKtc',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          if (widget.model.readStatus == '0')
                            Container(
                              width: 10,
                              height: 10,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.red,
                              ),
                            ),
                        ],
                      ),
                      const SizedBox(height: 8),
                      Expanded(
                        child: Text(
                          widget.model.content ?? '',
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                            color: Colors.black54,
                            fontSize: 14,
                            fontFamily: 'NotoSansCJKtc',
                          ),
                        ),
                      ),
                      const SizedBox(height: 8),
                      Row(
                        children: [
                          Text(
                            widget.model.cat ?? '',
                            style: const TextStyle(
                              color: Colors.black54,
                              fontSize: 14,
                              fontFamily: 'NotoSansCJKtc',
                            ),
                          ),
                          Expanded(
                            child: Text(
                              widget.model.date ?? '',
                              textAlign: TextAlign.end,
                              style: const TextStyle(
                                color: primaryColor,
                                fontSize: 11,
                                fontFamily: 'NotoSansCJKtc',
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
