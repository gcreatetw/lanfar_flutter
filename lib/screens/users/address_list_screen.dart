import 'package:flutter/material.dart';
import 'package:inspireui/utils/colors.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/src/provider.dart';

import '../../../generated/l10n.dart';
import '../../common/config.dart';
import '../../common/theme/colors.dart';
import '../../models/entities/address.dart';
import '../../models/index.dart';
import '../../services/index.dart';
import '../checkout/add_address_screen.dart';

class AddressListScreen extends StatefulWidget {
  const AddressListScreen({Key? key}) : super(key: key);

  @override
  State<AddressListScreen> createState() => _AddressListScreenState();
}

class _AddressListScreenState extends State<AddressListScreen> {
  List<Address?> listAddress = [];
  int maxNumber = 1;

  bool loading = false;
  bool deleting = false;

  @override
  void initState() {
    getDatafromLocal();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '配送地址',
          style: TextStyle(
            color: Theme.of(context).colorScheme.secondary,
          ),
        ),
        backgroundColor: Theme.of(context).backgroundColor,
      ),
      body: loading
          ? kLoadingWidget(context)
          : Stack(
              children: [
                ListView.separated(
                    itemCount: listAddress.length + 1,
                    separatorBuilder: (context, index) {
                      return const SizedBox(height: 1);
                    },
                    itemBuilder: (context, index) {
                      if (index == listAddress.length) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 16),
                              child: GestureDetector(
                                onTap: () async {
                                  var save = await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                           AddAddressScreen(maxNumber: maxNumber,),
                                    ),
                                  );
                                  if (save ?? false) {
                                    await getDatafromLocal();
                                    await showDialog(
                                      context: context,
                                      useRootNavigator: false,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          title: Text(S
                                              .of(context)
                                              .youHaveBeenSaveAddressYourLocal),
                                          actions: <Widget>[
                                            TextButton(
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                              child: Text(
                                                '確認',
                                                style: TextStyle(
                                                    color: Theme.of(context)
                                                        .primaryColor),
                                              ),
                                            )
                                          ],
                                        );
                                      },
                                    );
                                  }
                                },
                                child: Row(
                                  children: [
                                    Text(
                                      '新增收件資訊',
                                      style: TextStyle(
                                          color:
                                              Theme.of(context).primaryColor),
                                    ),
                                    Icon(Icons.keyboard_arrow_right_outlined,
                                        color: Theme.of(context).primaryColor)
                                  ],
                                ),
                              ),
                            )
                          ],
                        );
                      }

                      return Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: const BoxDecoration(color: Colors.white),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            vertical: 4,
                            horizontal: 16,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              convertToCard(context, listAddress[index]!),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  const Spacer(),
                                  GestureDetector(
                                    onTap: () async {
                                      final result = await showDialog(
                                        context: context,
                                        useRootNavigator: false,
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                            title: const Text('複製地址'),
                                            content: const Text('地址會重複，你確定嗎？'),
                                            actions: <Widget>[
                                              TextButton(
                                                onPressed: () async {
                                                  Navigator.of(context).pop();
                                                },
                                                child: const Text(
                                                  '取消',
                                                  style: TextStyle(
                                                      color: Colors.black54),
                                                ),
                                              ),
                                              TextButton(
                                                onPressed: () async {
                                                  Navigator.of(context)
                                                      .pop(true);
                                                },
                                                child: Text(
                                                  '確認',
                                                  style: TextStyle(
                                                      color: Theme.of(context)
                                                          .primaryColor),
                                                ),
                                              )
                                            ],
                                          );
                                        },
                                      );
                                      if (result == true) {
                                        setState(() {
                                          deleting = true;
                                        });
                                        final response =
                                            await Services().api.updateAddress(
                                                  userId: context
                                                          .read<UserModel>()
                                                          .user
                                                          ?.username ??
                                                      '',
                                                  action: 'insert',
                                                  addressData: Address()
                                                      .toApiJson(
                                                          listAddress[index]!),
                                                );
                                        setState(() {
                                          deleting = false;
                                        });
                                        if (response == null) {
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(
                                            const SnackBar(
                                              content: Text('新增成功'),
                                            ),
                                          );
                                          await getDatafromLocal();
                                        } else {
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(
                                            SnackBar(
                                              content: Text(response),
                                            ),
                                          );
                                        }
                                      }
                                    },
                                    child: const Text(
                                      '複製',
                                      style: TextStyle(color: primaryColor),
                                    ),
                                  ),
                                  const SizedBox(width: 16),
                                  GestureDetector(
                                    onTap: () async {
                                      var save = await Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              AddAddressScreen(
                                            address: listAddress[index]!,
                                          ),
                                        ),
                                      );
                                      if (save ?? false) {
                                        await getDatafromLocal();
                                      }
                                    },
                                    child: const Text(
                                      '編輯',
                                      style: TextStyle(color: primaryColor),
                                    ),
                                  ),
                                  const SizedBox(width: 16),
                                  GestureDetector(
                                    onTap: () async {
                                      await showDialog(
                                        context: context,
                                        useRootNavigator: false,
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                            title: const Text('是否要刪除？'),
                                            actions: <Widget>[
                                              TextButton(
                                                onPressed: () {
                                                  Navigator.of(context).pop();
                                                },
                                                child: Text(
                                                  '取消',
                                                  style: TextStyle(
                                                      color: Theme.of(context)
                                                          .primaryColor),
                                                ),
                                              ),
                                              TextButton(
                                                onPressed: () {
                                                  removeData(index);
                                                  Navigator.of(context).pop();
                                                },
                                                child: Text(
                                                  '確認',
                                                  style: TextStyle(
                                                      color: Theme.of(context)
                                                          .primaryColor),
                                                ),
                                              )
                                            ],
                                          );
                                        },
                                      );
                                    },
                                    child: const Text(
                                      '刪除',
                                      style: TextStyle(color: primaryColor),
                                    ),
                                  )
                                ],
                              ),
                              // GestureDetector(
                              //   onTap: () {
                              //     removeData(index);
                              //   },
                              //   child: Icon(
                              //     Icons.delete,
                              //     color: Theme.of(
                              //             context)
                              //         .primaryColor,
                              //   ),
                              // ),
                              const SizedBox(height: 10),
                            ],
                          ),
                        ),
                      );
                    }),
                if (deleting)
                  Container(
                    width: double.maxFinite,
                    height: double.maxFinite,
                    color: Colors.black54,
                    child: Center(
                      child: kLoadingWidget(context),
                    ),
                  ),
              ],
            ),
    );
  }

  Future<void> getDatafromLocal() async {
    setState(() {
      loading = true;
    });
    final response = await Services()
        .api
        .getAddress(userId: context.read<UserModel>().user?.username ?? '');

    if (response?.error == true) {
      final storage = LocalStorage('address');
      var _list = <Address?>[];
      try {
        final ready = await storage.ready;
        if (ready) {
          var data = storage.getItem('data');
          if (data != null) {
            for (var item in data as List) {
              final add = Address.fromLocalJson(item);
              if (add.defaultAddress == true) {
                _list.insert(0, add);
              } else {
                _list.add(add);
              }
              if (add.id != null && add.id! > maxNumber) {
                maxNumber = add.id! + 1;
              }
            }
          }
        }
        setState(() {
          listAddress = _list;
        });
      } catch (_) {}
    } else {
      var _list = <Address?>[];
      if (response?.data != null) {
        for (var element in response!.data!) {
          final add = Address.fromApi(element);
          if (add.defaultAddress == true) {
            _list.insert(0, add);
          } else {
            _list.add(add);
          }
          if (add.id != null && add.id! > maxNumber) {
            maxNumber = add.id! + 1;
          }
        }
      }
      setState(() {
        listAddress = _list;
      });
    }
    setState(() {
      loading = false;
    });
  }

  Widget convertToCard(BuildContext context, Address address) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const SizedBox(height: 10.0),
              Text(
                (address.lastName ?? '') + ' ' + (address.firstName ?? ''),
              ),
              const SizedBox(height: 4.0),
              Text(
                '(+886)' + (address.phoneNumber ?? ''),
                style: TextStyle(color: HexColor('#999999')),
              ),
              const SizedBox(height: 4.0),
              Text(
                (address.state ?? '') +
                    (address.city ?? '') +
                    (address.street ?? ''),
                style: TextStyle(color: HexColor('#999999')),
              ),
              const SizedBox(height: 10.0),
            ],
          ),
        ),
        if (address.defaultAddress == true)
          const Padding(
            padding: EdgeInsets.only(top: 10),
            child: Text(
              '預設',
              style: TextStyle(color: primaryColor),
            ),
          ),
      ],
    );
  }

  Future<void> removeData(int index) async {
    setState(() {
      deleting = true;
    });
    final response = await Services().api.updateAddress(
          userId: context.read<UserModel>().user?.username ?? '',
          action: 'delete',
          addressId: listAddress[index]?.id,
        );
    setState(() {
      deleting = false;
    });
    if (response == null) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('刪除成功'),
        ),
      );
      await getDatafromLocal();
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(response),
        ),
      );
    }
    // final storage = LocalStorage('address');
    // try {
    //   var data = storage.getItem('data');
    //   if (data != null) {
    //     (data as List).removeAt(index);
    //   }
    //   storage.setItem('data', data);
    // } catch (_) {}
  }
}
