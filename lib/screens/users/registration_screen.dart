import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../from_lanfar/screens/term_agree_page.dart';
import '../../generated/l10n.dart';
import '../../models/index.dart'
    show AppModel, CartModel, PointModel, User, UserModel;
import '../../services/index.dart';
import 'login_screen.dart';

enum RegisterState { begin, phoneSend, verified }

class RegistrationScreen extends StatefulWidget {
  const RegistrationScreen();

  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  // final _auth = firebase_auth.FirebaseAuth.instance;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController _emailController = TextEditingController();

  String? firstName, lastName, emailAddress, phoneNumber, password;
  bool? isVendor = false;
  bool isChecked = false;

  bool canSignDown = false;
  bool isSignDown = false;

  final bool showPhoneNumberWhenRegister =
      kLoginSetting['showPhoneNumberWhenRegister'] ?? false;
  final bool requirePhoneNumberWhenRegister =
      kLoginSetting['requirePhoneNumberWhenRegister'] ?? false;

  final firstNameNode = FocusNode();
  final lastNameNode = FocusNode();
  final phoneNumberNode = FocusNode();
  final emailNode = FocusNode();
  final passwordNode = FocusNode();

  final idCodeNode = FocusNode();
  final phoneNode = FocusNode();
  final codeNode = FocusNode();
  final passwordFirstNode = FocusNode();
  final passwordSecondNode = FocusNode();

  TextEditingController userCode = TextEditingController();

  TextEditingController idCode = TextEditingController();

  TextEditingController phone = TextEditingController();

  TextEditingController code = TextEditingController();

  TextEditingController passwordFirst = TextEditingController();

  TextEditingController passwordSecond = TextEditingController();

  bool hideFirst = true;
  bool hideSecond = true;

  String smsCode = '';

  String verId = '';

  bool verify = false;

  bool verifying = false;

  bool agree = false;

  RegisterState state = RegisterState.begin;

  void _welcomeDiaLog(User user) {
    Provider.of<CartModel>(context, listen: false).setUser(user);
    Provider.of<PointModel>(context, listen: false).getMyPoint(user.cookie);
    var email = user.email;
    _snackBar(S.of(context).welcome + ' $email!');
    if (kLoginSetting['IsRequiredLogin'] ?? false) {
      Navigator.of(context).pushReplacementNamed(RouteList.dashboard);
      return;
    }
    var routeFound = false;
    var routeNames = [RouteList.dashboard, RouteList.productDetail];
    Navigator.popUntil(context, (route) {
      if (routeNames
          .any((element) => route.settings.name?.contains(element) ?? false)) {
        routeFound = true;
      }
      return routeFound || route.isFirst;
    });

    if (!routeFound) {
      Navigator.of(context).pushReplacementNamed(RouteList.dashboard);
    }
  }

  @override
  void dispose() {
    _emailController.dispose();
    firstNameNode.dispose();
    lastNameNode.dispose();
    emailNode.dispose();
    passwordNode.dispose();
    phoneNumberNode.dispose();
    userCode.dispose();
    idCode.dispose();
    phone.dispose();
    code.dispose();
    passwordFirst.dispose();
    passwordSecond.dispose();
    idCodeNode.dispose();
    phoneNode.dispose();
    codeNode.dispose();
    passwordFirstNode.dispose();
    passwordSecondNode.dispose();
    super.dispose();
  }

  void _snackBar(String text, {bool? action}) {
    if (mounted) {
      ScaffoldMessenger.of(context).removeCurrentSnackBar();
      final snackBar = SnackBar(
        content: Text(text),
        duration: Duration(seconds: action ?? true ? 10 : 4),
        action: action ?? true
            ? SnackBarAction(
                label: S.of(context).close,
                onPressed: () {
                  // Some code to undo the change.
                },
                textColor: Colors.white,
              )
            : null,
      );
      // ignore: deprecated_member_use
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  Future<void> _submitRegister({
    String? firstName,
    String? lastName,
    String? phoneNumber,
    String? emailAddress,
    String? password,
    bool? isVendor,
  }) async {
    if (userCode.text.isEmpty ||
        idCode.text.isEmpty ||
        phone.text.isEmpty ||
        passwordFirst.text.isEmpty ||
        passwordSecond.text.isEmpty) {
      _snackBar(S.of(context).pleaseInputFillAllFields);
    } else {
      final identityValid = RegExp(r'[^0-9]');

      if (identityValid.hasMatch(idCode.text) || idCode.text.length < 4) {
        _snackBar('請輸入身分證末四碼', action: false);
        return;
      }

      // if (verify) {
      //   if (code.text != smsCode) {
      //     _snackBar('驗證碼不符', action: false);
      //     return;
      //   }
      // } else {
      //   try {
      //     var auth = firebaseAuth.FirebaseAuth.instance;
      //     var credential = firebaseAuth.PhoneAuthProvider.credential(
      //         verificationId: verId, smsCode: code.text);
      //     await auth.signInWithCredential(credential);
      //     smsCode = credential.smsCode!;
      //     verify = true;
      //   } catch (e) {
      //     debugPrint(e.toString());
      //     _snackBar('驗證碼不符', action: false);
      //     return;
      //   }
      // }

      if (passwordFirst.text != passwordSecond.text) {
        _snackBar('確認密碼不符', action: false);
        return;
      }

      if (isSignDown) {
        var result = await Services().api.signDown(
              username: userCode.text,
              password: passwordFirst.text,
              identity: idCode.text,
              firstName: firstName,
              lastName: lastName,
              phoneNumber: phone.text,
            );
        if (result != null) {
          _snackBar(result);
        } else {
          Navigator.pop(context);
          _snackBar('您已送交開通申請，請等待客服開通帳號。');
        }
      } else {
        await Provider.of<UserModel>(context, listen: false).createUser(
          username: userCode.text,
          password: passwordFirst.text,
          identity: idCode.text,
          firstName: firstName,
          lastName: lastName,
          phoneNumber: phone.text,
          success: _welcomeDiaLog,
          fail: _snackBar,
          isVendor: isVendor,
        );
      }
    }

    // if (firstName == null ||
    //     lastName == null ||
    //     emailAddress == null ||
    //     password == null ||
    //     (showPhoneNumberWhenRegister &&
    //         requirePhoneNumberWhenRegister &&
    //         phoneNumber == null)) {
    //   _snackBar(S.of(context).pleaseInputFillAllFields);
    // } else if (isChecked == false) {
    //   _snackBar(S.of(context).pleaseAgreeTerms);
    // } else {
    //   final emailValid = RegExp(
    //       r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
    //
    //   if (!emailValid.hasMatch(emailAddress)) {
    //     _snackBar(S.of(context).errorEmailFormat);
    //     return;
    //   }
    //
    //   if (password.length < 8) {
    //     _snackBar(S.of(context).errorPasswordFormat);
    //     return;
    //   }
    //
    //   await Provider.of<UserModel>(context, listen: false).createUser(
    //     username: userCode.text,
    //     password: passwordFirst.text,
    //     identity: idCode.text,
    //     firstName: firstName,
    //     lastName: lastName,
    //     phoneNumber: phoneNumber,
    //     success: _welcomeDiaLog,
    //     fail: _snackBar,
    //     isVendor: isVendor,
    //   );
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Theme.of(context).backgroundColor,
      body: GestureDetector(
        onTap: () => Tools.hideKeyboard(context),
        child: ListenableProvider.value(
          value: Provider.of<UserModel>(context),
          child: Consumer<UserModel>(
            builder: (context, value, child) {
              return Column(
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: EdgeInsets.only(
                            bottom: MediaQuery.of(context).padding.bottom),
                        child: AutofillGroup(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Stack(
                                alignment: Alignment.bottomCenter,
                                children: [
                                  Column(
                                    children: [
                                      Image.asset(
                                        'assets/icons/from_lanfar/Image 3@2x.png',
                                        fit: BoxFit.fitWidth,
                                      ),
                                      Container(
                                        height: 5,
                                        color:
                                            Theme.of(context).backgroundColor,
                                      ),
                                    ],
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(bottom: 5),
                                    height: 25,
                                    decoration: BoxDecoration(
                                      color: Theme.of(context).backgroundColor,
                                      borderRadius: const BorderRadius.only(
                                        topRight: Radius.circular(15),
                                        topLeft: Radius.circular(15),
                                      ),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black.withOpacity(0.2),
                                          spreadRadius: 1,
                                          blurRadius: 3,
                                          offset: const Offset(0,
                                              -1), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    height: 10,
                                    color: Theme.of(context).backgroundColor,
                                  ),
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 16),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: const <Widget>[
                                    SizedBox(width: double.maxFinite),
                                    Text(
                                      '會員開通',
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(height: 4),
                                    Text(
                                      '連法國際APP 歡迎您，提供優質保健產品，期許有更多人因接觸賜多利而獲得全方位健康',
                                      style: TextStyle(
                                        fontSize: 14,
                                        color: Color(0xff666666),
                                      ),
                                    ),
                                    Text(
                                      '若經開通完成後欲變更手機號碼，請透過客服諮詢進行申請',
                                      style: TextStyle(
                                        fontSize: 14,
                                        color: Color(0xff666666),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(height: 20.0),

                              ///電話
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 16),
                                child: TextField(
                                  key: const Key('phoneField'),
                                  enabled: !verify &&
                                      value.otpCountDown == 0 &&
                                      !value.smsLoading,
                                  focusNode: phoneNode,
                                  controller: phone,
                                  autofillHints: !verify &&
                                          value.otpCountDown == 0 &&
                                          !value.smsLoading
                                      ? const [AutofillHints.email]
                                      : null,
                                  autocorrect: false,
                                  enableSuggestions: false,
                                  textInputAction: TextInputAction.next,
                                  keyboardType: TextInputType.number,
                                  onSubmitted: (_) => FocusScope.of(context)
                                      .requestFocus(codeNode),
                                  style: verify
                                      ? const TextStyle(color: Colors.black54)
                                      : null,
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 16),
                                    prefixIcon: const Icon(
                                      Icons.phone_iphone,
                                      color: primaryColor,
                                    ),
                                    hintText: '請輸入手機號碼',
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                          color: Colors.black,
                                          width: 0.5,
                                        ),
                                        borderRadius:
                                            BorderRadius.circular(15)),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                        color: Colors.black,
                                        width: 0.5,
                                      ),
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                        color: Colors.black,
                                        width: 0.5,
                                      ),
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                  ),
                                ),
                              ),
                              if (!verify) ...[
                                const SizedBox(height: 20.0),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16),
                                  child: Material(
                                    color: value.otpCountDown == 0
                                        ? Theme.of(context).primaryColor
                                        : Theme.of(context)
                                            .primaryColor
                                            .withOpacity(0.5),
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(15.0)),
                                    elevation: 0,
                                    child: MaterialButton(
                                      key: const Key('sendCode'),
                                      onPressed: value.otpCountDown > 0
                                          ? null
                                          : () async {
                                              if (value.smsLoading) return;
                                              if (phone.text.isEmpty) {
                                                _snackBar('請輸入手機號碼',
                                                    action: false);
                                                return;
                                              }
                                              var reg = RegExp(r'^09\d{8}$');
                                              if (!reg.hasMatch(phone.text)) {
                                                _snackBar('手機號碼格式錯誤',
                                                    action: false);
                                                return;
                                              }
                                              value.setSMSLoading(true);
                                              var msg = await Services()
                                                  .api
                                                  .getOtp(phone: phone.text);
                                              value.setSMSLoading(false);
                                              if (msg == null || msg.isEmpty) {
                                                _snackBar('已發送驗證碼',
                                                    action: false);
                                                verify = false;
                                                smsCode = '';
                                                code.text = '';
                                                setState(() {
                                                  state =
                                                      RegisterState.phoneSend;
                                                });
                                                value.startTimer(
                                                  () {
                                                    setState(() {
                                                      canSignDown = true;
                                                    });
                                                  },
                                                );
                                              } else {
                                                setState(() {
                                                  canSignDown = true;
                                                });
                                                _snackBar(msg);
                                              }

                                              // FirebaseServices().verifyPhoneNumber(
                                              //   phoneNumber: '+886' +
                                              //       int.parse(phone.text).toString(),
                                              //   codeSent: (String verificationId,
                                              //       int? resendToken) async {
                                              //     value.setSMSLoading(false);
                                              //     debugPrint('codeSent');
                                              //     _snackBar('已發送驗證碼', action: false);
                                              //     verId = verificationId;
                                              //     verify = false;
                                              //     smsCode = '';
                                              //     code.text = '';
                                              //     setState(() {
                                              //       state = RegisterState.phoneSend;
                                              //     });
                                              //     debugPrint(verId);
                                              //   },
                                              //   codeAutoRetrievalTimeout:
                                              //       (String verificationId) {
                                              //     value.setSMSLoading(false);
                                              //     debugPrint(
                                              //         'codeAutoRetrievalTimeout,verificationId = $verificationId');
                                              //   },
                                              //   verificationCompleted:
                                              //       (firebaseAuth.PhoneAuthCredential
                                              //           phoneAuthCredential) {
                                              //     value.setSMSLoading(false);
                                              //     smsCode =
                                              //         phoneAuthCredential.smsCode ?? '';
                                              //     debugPrint('verificationCompleted');
                                              //     // _snackBar('已發送驗證碼',action: false);
                                              //   },
                                              //   verificationFailed:
                                              //       (firebaseAuth.FirebaseAuthException
                                              //           error) {
                                              //     value.setSMSLoading(false);
                                              //     debugPrint('verificationFailed : $error');
                                              //     _snackBar('發送驗證碼失敗 : $error');
                                              //   },
                                              // );
                                            },
                                      elevation: 0.0,
                                      height: 42.0,
                                      child: Text(
                                        value.smsLoading
                                            ? '發送中...'
                                            : state != RegisterState.begin
                                                ? '${value.otpCountDown > 0 ? '${value.otpCountDown}秒 ' : ''}重新發送驗證碼'
                                                : '${value.otpCountDown > 0 ? '${value.otpCountDown}秒 ' : ''}發送驗證碼',
                                        style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                              const SizedBox(height: 20.0),
                              if (state != RegisterState.begin) ...[
                                if (!verify) ...[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16),
                                    child: Row(
                                      children: [
                                        Expanded(
                                            child: TextField(
                                          key: const Key('codeField'),
                                          focusNode: codeNode,
                                          controller: code,
                                          autofillHints: const [
                                            AutofillHints.email
                                          ],
                                          autocorrect: false,
                                          enableSuggestions: false,
                                          textInputAction: TextInputAction.next,
                                          keyboardType: TextInputType.number,
                                          onSubmitted: (_) => FocusScope.of(
                                                  context)
                                              .requestFocus(passwordFirstNode),
                                          decoration: InputDecoration(
                                            contentPadding:
                                                const EdgeInsets.symmetric(
                                                    vertical: 10,
                                                    horizontal: 16),
                                            hintText: '請輸入手機驗證碼',
                                            focusedBorder: OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                  color: Colors.black,
                                                  width: 0.5,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(15)),
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: const BorderSide(
                                                color: Colors.black,
                                                width: 0.5,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(15),
                                            ),
                                          ),
                                        )),
                                        const SizedBox(width: 8),
                                        Material(
                                          color: Theme.of(context).primaryColor,
                                          borderRadius: const BorderRadius.all(
                                              Radius.circular(15.0)),
                                          elevation: 0,
                                          child: MaterialButton(
                                            key: const Key('sendCode'),
                                            onPressed: verify
                                                ? null
                                                : () async {
                                                    if (code.text.isEmpty) {
                                                      _snackBar('請輸入驗證碼',
                                                          action: false);
                                                      return;
                                                    }
                                                    setState(() {
                                                      verifying = true;
                                                    });
                                                    var msg = await Services()
                                                        .api
                                                        .verifyOtp(
                                                          phone: phone.text,
                                                          code: code.text,
                                                        );
                                                    if (msg == null ||
                                                        msg.isEmpty) {
                                                      smsCode = code.text;
                                                      verify = true;
                                                      setState(() {
                                                        state = RegisterState
                                                            .verified;
                                                        verifying = false;
                                                      });
                                                    } else {
                                                      _snackBar(msg,
                                                          action: false);
                                                    }
                                                    // try {
                                                    //   var auth = firebaseAuth
                                                    //       .FirebaseAuth.instance;
                                                    //   var credential = firebaseAuth
                                                    //           .PhoneAuthProvider
                                                    //       .credential(
                                                    //           verificationId: verId,
                                                    //           smsCode: code.text);
                                                    //   await auth.signInWithCredential(
                                                    //       credential);
                                                    //   smsCode = credential.smsCode!;
                                                    //   verify = true;
                                                    //   setState(() {
                                                    //     state =
                                                    //         RegisterState.verified;
                                                    //     verifying = false;
                                                    //   });
                                                    // } catch (e) {
                                                    //   debugPrint(e.toString());
                                                    //   setState(() {
                                                    //     verifying = false;
                                                    //   });
                                                    //   _snackBar('驗證碼不符',
                                                    //       action: false);
                                                    //   return;
                                                    // }
                                                  },
                                            elevation: 0.0,
                                            height: 42.0,
                                            child: Text(
                                              verifying
                                                  ? '驗證中...'
                                                  : state ==
                                                          RegisterState
                                                              .phoneSend
                                                      ? '驗證'
                                                      : '已驗證',
                                              style: const TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 14),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(height: 20.0),
                                ],
                              ],
                              if (canSignDown &&
                                  state != RegisterState.verified) ...[
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16),
                                  child: Material(
                                    color: Theme.of(context).primaryColor,
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(15.0)),
                                    elevation: 0,
                                    child: MaterialButton(
                                      key: const Key('sendCode'),
                                      onPressed: () async {
                                        setState(() {
                                          state = RegisterState.verified;
                                          verify = true;
                                          isSignDown = true;
                                        });
                                      },
                                      elevation: 0.0,
                                      height: 42.0,
                                      child: const Text(
                                        '進行離線開通',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 16),
                              ],

                              if (state != RegisterState.begin) ...[
                                if (state == RegisterState.verified) ...[
                                  if (isSignDown) ...[
                                    const Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 16),
                                      child: Text(
                                        '您仍可繼續完成開通流程，您的帳號開通結果將由客服在一個工作日內為您處理。',
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Color(0xff666666),
                                        ),
                                      ),
                                    ),
                                    const SizedBox(height: 16),
                                  ],

                                  ///經銷商編號
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16),
                                    child: TextField(
                                      key: const Key('userCodeField'),
                                      controller: userCode,
                                      // autofillHints: const [AutofillHints.email],
                                      autocorrect: false,
                                      enableSuggestions: false,
                                      textInputAction: TextInputAction.next,
                                      textCapitalization:
                                          TextCapitalization.characters,
                                      inputFormatters: [
                                        UpperCaseTextFormatter(),
                                      ],
                                      onSubmitted: (_) => FocusScope.of(context)
                                          .requestFocus(idCodeNode),
                                      decoration: InputDecoration(
                                        contentPadding:
                                            const EdgeInsets.symmetric(
                                                vertical: 10, horizontal: 16),
                                        prefixIcon: const Icon(
                                          Icons.person_outline,
                                          color: primaryColor,
                                        ),
                                        hintText: '請輸入經銷商編號',
                                        focusedBorder: OutlineInputBorder(
                                            borderSide: const BorderSide(
                                              color: Colors.black,
                                              width: 0.5,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(15)),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: const BorderSide(
                                            color: Colors.black,
                                            width: 0.5,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(15),
                                        ),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 20.0),

                                  ///身分證
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16),
                                    child: TextField(
                                      key: const Key('IdField'),
                                      focusNode: idCodeNode,
                                      controller: idCode,
                                      // autofillHints: const [AutofillHints.email],
                                      autocorrect: false,
                                      enableSuggestions: false,
                                      textInputAction: TextInputAction.next,
                                      keyboardType: TextInputType.number,
                                      onSubmitted: (_) => FocusScope.of(context)
                                          .requestFocus(phoneNode),
                                      maxLength: 4,
                                      decoration: InputDecoration(
                                        counterText: '',
                                        contentPadding:
                                            const EdgeInsets.symmetric(
                                                vertical: 10, horizontal: 16),
                                        prefixIcon: const Icon(
                                          Icons.person_outline,
                                          color: primaryColor,
                                        ),
                                        hintText: '請輸入身分證末四碼',
                                        focusedBorder: OutlineInputBorder(
                                            borderSide: const BorderSide(
                                              color: Colors.black,
                                              width: 0.5,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(15)),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: const BorderSide(
                                            color: Colors.black,
                                            width: 0.5,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(15),
                                        ),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 20.0),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16),
                                    child: TextField(
                                      key: const Key('loginPasswordFieldFirst'),
                                      focusNode: passwordFirstNode,
                                      // autofillHints: const [AutofillHints.password],
                                      obscureText: hideFirst,
                                      obscuringCharacter: '*',
                                      textInputAction: TextInputAction.next,
                                      keyboardType:
                                          TextInputType.visiblePassword,
                                      controller: passwordFirst,
                                      onSubmitted: (_) => FocusScope.of(context)
                                          .requestFocus(passwordSecondNode),
                                      decoration: InputDecoration(
                                        contentPadding:
                                            const EdgeInsets.symmetric(
                                                vertical: 10, horizontal: 16),
                                        prefixIcon: const Icon(
                                          Icons.lock_outline,
                                          color: primaryColor,
                                        ),
                                        suffixIcon: GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              if (hideFirst) {
                                                hideFirst = false;
                                              } else {
                                                hideFirst = true;
                                              }
                                            });
                                          },
                                          child: Icon(
                                            hideFirst
                                                ? Icons.visibility_off_outlined
                                                : Icons.visibility_outlined,
                                            color: Colors.grey,
                                          ),
                                        ),
                                        hintText: '請輸入您的密碼',
                                        focusedBorder: OutlineInputBorder(
                                            borderSide: const BorderSide(
                                              color: Colors.black,
                                              width: 0.5,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(15)),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: const BorderSide(
                                            color: Colors.black,
                                            width: 0.5,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(15),
                                        ),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(height: 20.0),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16),
                                    child: TextField(
                                      key:
                                          const Key('loginPasswordFieldSecond'),
                                      focusNode: passwordSecondNode,
                                      // autofillHints: const [AutofillHints.password],
                                      obscureText: hideSecond,
                                      obscuringCharacter: '*',
                                      textInputAction: TextInputAction.done,
                                      keyboardType:
                                          TextInputType.visiblePassword,
                                      controller: passwordSecond,
                                      decoration: InputDecoration(
                                        contentPadding:
                                            const EdgeInsets.symmetric(
                                                vertical: 10, horizontal: 16),
                                        prefixIcon: const Icon(
                                          Icons.lock_outline,
                                          color: primaryColor,
                                        ),
                                        suffixIcon: GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              if (hideSecond) {
                                                hideSecond = false;
                                              } else {
                                                hideSecond = true;
                                              }
                                            });
                                          },
                                          child: Icon(
                                            hideSecond
                                                ? Icons.visibility_off_outlined
                                                : Icons.visibility_outlined,
                                            color: Colors.grey,
                                          ),
                                        ),
                                        hintText: '再次輸入確認密碼',
                                        focusedBorder: OutlineInputBorder(
                                            borderSide: const BorderSide(
                                              color: Colors.black,
                                              width: 0.5,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(15)),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: const BorderSide(
                                            color: Colors.black,
                                            width: 0.5,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(15),
                                        ),
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: checkTerms,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 16),
                                      child: Row(
                                        children: [
                                          Checkbox(
                                              activeColor: primaryColor,
                                              value: agree,
                                              onChanged: (v) {
                                                checkTerms();
                                              }),
                                          const Text('我已同意會員條款'),
                                        ],
                                      ),
                                    ),
                                  ),

                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 16.0, horizontal: 16),
                                    child: Material(
                                      color: Theme.of(context).primaryColor,
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(50.0)),
                                      elevation: 0,
                                      child: MaterialButton(
                                        key: const Key('registerSubmitButton'),
                                        onPressed: value.loading == true
                                            ? null
                                            : () async {
                                                if (!agree) {
                                                  ScaffoldMessenger.of(context)
                                                      .showSnackBar(
                                                    const SnackBar(
                                                      content: Text('請先同意會員條款'),
                                                    ),
                                                  );
                                                  return;
                                                }
                                                value.setLoading(true);
                                                await _submitRegister(
                                                  firstName: firstName,
                                                  lastName: lastName,
                                                  phoneNumber: phoneNumber,
                                                  emailAddress: emailAddress,
                                                  password: password,
                                                  isVendor: isVendor,
                                                );
                                                value.setLoading(false);
                                              },
                                        minWidth: 400.0,
                                        elevation: 0.0,
                                        height: 42.0,
                                        child: Text(
                                          value.loading == true
                                              ? S.of(context).loading
                                              : '會員開通',
                                          style: const TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.normal),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ],
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 0.0, horizontal: 16),
                                child: Material(
                                  color: Colors.black12,
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(50.0)),
                                  elevation: 0,
                                  child: MaterialButton(
                                    key: const Key('back'),
                                    onPressed: () {
                                      final canPop =
                                          ModalRoute.of(context)!.canPop;
                                      if (canPop) {
                                        Navigator.pop(context);
                                      } else {
                                        Navigator.of(context)
                                            .pushReplacementNamed(
                                                RouteList.login);
                                      }
                                    },
                                    minWidth: 400.0,
                                    elevation: 0.0,
                                    height: 42.0,
                                    child: const Text(
                                      '返回',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.normal),
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(height: 20),
                              // Padding(
                              //   padding: const EdgeInsets.symmetric(vertical: 10.0),
                              //   child: Row(
                              //     mainAxisAlignment: MainAxisAlignment.center,
                              //     children: <Widget>[
                              //       Text(
                              //         S.of(context).or + ' ',
                              //         style: const TextStyle(color: Colors.black45),
                              //       ),
                              //       InkWell(
                              //         onTap: () {
                              //           final canPop = ModalRoute.of(context)!.canPop;
                              //           if (canPop) {
                              //             Navigator.pop(context);
                              //           } else {
                              //             Navigator.of(context)
                              //                 .pushReplacementNamed(RouteList.login);
                              //           }
                              //         },
                              //         child: Text(
                              //           S.of(context).loginToYourAccount,
                              //           style: TextStyle(
                              //             color: Theme.of(context).primaryColor,
                              //             decoration: TextDecoration.underline,
                              //             fontSize: 15,
                              //           ),
                              //         ),
                              //       ),
                              //     ],
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  const Text(
                    'PS. APP與官網擇一開通即可',
                    style: TextStyle(
                      fontSize: 14,
                      color: Color(0xff666666),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).padding.bottom,
                  )
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  void checkTerms() {
    if (agree) {
      setState(() {
        agree = false;
      });
    } else {
      showDialog(
        context: context,
        builder: (context) => Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          margin: const EdgeInsets.all(50),
          child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: TermAgreePage(
                type: TermsType.user,
                check: () {
                  setState(() {
                    agree = true;
                  });
                },
              )
              // WebView(
              //   url: '${environment['serverConfig']['url']}/terms?appstyle=1',
              //   getHeight: true,
              //   onFinish: () {
              //     setState(() {
              //       agree = true;
              //     });
              //   },
              //   inApp: true,
              // )
              ),
        ),
      );
    }
  }
}

class PrivacyScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle.light,
        title: Text(
          S.of(context).agreeWithPrivacy,
          style: const TextStyle(
            fontSize: 16.0,
            color: Colors.white,
          ),
        ),
        leading: GestureDetector(
          onTap: () => Navigator.pop(context),
          child: const Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Text(
            S.of(context).privacyTerms,
            style: const TextStyle(fontSize: 16.0, height: 1.4),
            textAlign: TextAlign.justify,
          ),
        ),
      ),
    );
  }
}
