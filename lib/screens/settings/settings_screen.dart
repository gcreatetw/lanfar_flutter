import 'dart:io';

import 'package:flutter/cupertino.dart' show CupertinoIcons;
import 'package:flutter/material.dart';
import 'package:inspireui/icons/icon_picker.dart';
import 'package:localstorage/localstorage.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

import '../../app.dart';
import '../../common/config.dart';
import '../../common/config/configuration_utils.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../env.dart';
import '../../from_lanfar/screens/about_app_page.dart';
import '../../from_lanfar/screens/bio_auth_page.dart';
import '../../from_lanfar/screens/bonus_page.dart';
import '../../from_lanfar/screens/delete_page.dart';
import '../../from_lanfar/screens/marketing_page.dart';
import '../../from_lanfar/screens/qrcode_page.dart';
import '../../from_lanfar/screens/service_record_page.dart';
import '../../from_lanfar/screens/webview_with_cookie.dart';
import '../../from_lanfar/utils/show_contact_page.dart';
import '../../from_lanfar/utils/utils.dart';
import '../../from_lanfar/widget/profile_card.dart';
import '../../generated/l10n.dart';
import '../../menu/maintab_delegate.dart';
import '../../models/index.dart'
    show AppModel, CartModel, ProductWishListModel, User, UserModel;
import '../../models/notification_model.dart';
import '../../routes/flux_navigate.dart';
import '../../services/index.dart';
import '../../widgets/common/index.dart';
import '../../widgets/overlay/mixin/smart_chat_mixin.dart';
import '../common/app_bar_mixin.dart';
import '../index.dart';
import '../users/address_list_screen.dart';
import '../users/notification_screen.dart';
import '../users/user_point_screen.dart';

const itemPadding = 15.0;

class SettingScreen extends StatefulWidget {
  final List<dynamic>? settings;
  final Map? subGeneralSetting;
  final String? background;
  final Map? drawerIcon;
  final VoidCallback? onLogout;

  const SettingScreen({
    this.onLogout,
    this.settings,
    this.subGeneralSetting,
    this.background,
    this.drawerIcon,
  });

  @override
  SettingScreenState createState() {
    return SettingScreenState();
  }
}

class SettingScreenState extends State<SettingScreen>
    with
        SmartChatMixin,
        TickerProviderStateMixin,
        AutomaticKeepAliveClientMixin<SettingScreen>,
        AppBarMixin {
  @override
  bool get wantKeepAlive => false;

  User? get user => Provider.of<UserModel>(context, listen: false).user;
  bool isAbleToPostManagement = false;

  String nowVersion = '';

  final bannerHigh = 150.0;

  // final RateMyApp _rateMyApp = RateMyApp(
  //     // rate app on store
  //     minDays: 7,
  //     minLaunches: 10,
  //     remindDays: 7,
  //     remindLaunches: 10,
  //     googlePlayIdentifier: kStoreIdentifier['android'],
  //     appStoreIdentifier: kStoreIdentifier['ios']);

  // void showRateMyApp() {
  //   _rateMyApp.showRateDialog(
  //     context,
  //     title: S.of(context).rateTheApp,
  //     // The dialog title.
  //     message: S.of(context).rateThisAppDescription,
  //     // The dialog message.
  //     rateButton: S.of(context).rate.toUpperCase(),
  //     // The dialog 'rate' button text.
  //     noButton: S.of(context).noThanks.toUpperCase(),
  //     // The dialog 'no' button text.
  //     laterButton: S.of(context).maybeLater.toUpperCase(),
  //     // The dialog 'later' button text.
  //     listener: (button) {
  //       // The button click listener (useful if you want to cancel the click event).
  //       switch (button) {
  //         case RateMyAppDialogButton.rate:
  //           break;
  //         case RateMyAppDialogButton.later:
  //           break;
  //         case RateMyAppDialogButton.no:
  //           break;
  //       }
  //
  //       return true; // Return false if you want to cancel the click event.
  //     },
  //     // Set to false if you want to show the native Apple app rating dialog on iOS.
  //     dialogStyle: const DialogStyle(),
  //     // Custom dialog styles.
  //     // Called when the user dismissed the dialog (either by taping outside or by pressing the 'back' button).
  //     // actionsBuilder: (_) => [], // This one allows you to use your own buttons.
  //   );
  // }

  void checkAddPostRole() {
    for (var legitRole in addPostAccessibleRoles) {
      if (user!.role == legitRole) {
        setState(() {
          isAbleToPostManagement = true;
        });
      }
    }
  }

  @override
  void initState() {
    super.initState();
    Provider.of<UserModel>(context, listen: false).getUser(runLoaded: false);
    // if (isMobile) {
    //   _rateMyApp.init().then((_) {
    //     // state of rating the app
    //     if (_rateMyApp.shouldOpenDialog) {
    //       showRateMyApp();
    //     }
    //   });
    // }
    Future.microtask(() async {
      final packageInfo = await PackageInfo.fromPlatform();
      setState(() {
        nowVersion = packageInfo.version;
      });
    });
  }

  ///add custom UI to member page
  Widget customLayout() {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: Column(
        children: [
          const SizedBox(height: 24),
          Stack(
            alignment: Alignment.bottomCenter,
            children: [
              Container(
                height: 40,
                decoration: BoxDecoration(
                  color: Theme.of(context).canvasColor,
                  borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(20),
                    topLeft: Radius.circular(20),
                  ),
                ),
              ),
              if (user != null)
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: ProfileCard(
                    userCode: user != null && user!.username != null
                        ? user!.username!
                        : '',
                    userName: user != null && user!.firstName != null
                        ? user!.firstName!
                        : '',
                    userGrade: user != null && user!.grade1title != null
                        ? user!.grade1title!
                        : '',
                    joinDate: user != null && user!.registered != null
                        ? user!.registered!
                        : '',
                    elevation: 4,
                    padding: 16,
                  ),
                )
            ],
          ),
          Container(
            color: Theme.of(context).canvasColor,
            child: Column(
              children: [
                const SizedBox(height: 30),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const SizedBox(width: 12),
                    for (int i = 0; i < 4; i++)
                      GestureDetector(
                        onTap: () {
                          switch (i) {
                            case 0:
                              final user =
                                  Provider.of<UserModel>(context, listen: false)
                                      .user;
                              FluxNavigate.pushNamed(
                                RouteList.orders,
                                arguments: user,
                              );
                              break;
                            case 1:
                              MainTabControlDelegate.getInstance().changeTab(
                                  RouteList.performance.replaceFirst('/', ''));
                              break;
                            case 2:
                              Navigator.of(
                                      App.fluxStoreNavigatorKey.currentContext!)
                                  .push(
                                MaterialPageRoute(
                                  builder: (context) =>
                                      BioAuthPage(onAuthFinish: () {
                                    Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(
                                        builder: (context) => const BonusPage(),
                                      ),
                                    );
                                  }),
                                ),
                              );
                              // Navigator.of(
                              //         App.fluxStoreNavigatorKey.currentContext!)
                              //     .push(
                              //   MaterialPageRoute(
                              //     builder: (context) => WebViewWithCookie(
                              //         url:
                              //             '${environment['serverConfig']['url']}/my-accounts/bonus-info/'),
                              //   ),
                              // );
                              break;
                            case 3:
                              Navigator.of(
                                      App.fluxStoreNavigatorKey.currentContext!)
                                  .push(
                                MaterialPageRoute(
                                  builder: (context) => NotificationScreenNew(),
                                ),
                              );
                              break;
                          }
                        },
                        child: Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Container(
                            padding: const EdgeInsets.symmetric(horizontal: 4),
                            width: (MediaQuery.of(context).size.width - 56) / 4,
                            height: 100,
                            child: Column(
                              children: [
                                const SizedBox(height: 16),
                                Icon(
                                  memberTopIcons[i],
                                  size: 36,
                                  color: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .color,
                                ),
                                const SizedBox(height: 13),
                                FittedBox(
                                  child: Text(
                                    memberTopTitle[i],
                                    maxLines: 1,
                                    style: const TextStyle(fontSize: 15),
                                  ),
                                ),
                                const SizedBox(height: 9),
                              ],
                            ),
                          ),
                        ),
                      ),
                    const SizedBox(width: 12),
                  ],
                ),
                const SizedBox(height: 16),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const SizedBox(width: 12),
                    for (int i = 4; i < 8; i++)
                      GestureDetector(
                        onTap: () {
                          switch (i) {
                            case 4:
                              FluxNavigate.pushNamed(
                                RouteList.updateUser,
                                forceRootNavigator: true,
                              );
                              break;
                            case 5:

                              ///聯盟行銷
                              Navigator.of(
                                      App.fluxStoreNavigatorKey.currentContext!)
                                  .push(
                                MaterialPageRoute(
                                  builder: (context) => const MarketingPage(),
                                ),
                              );
                              // ShowContactPage().showContact(
                              //     App.fluxStoreNavigatorKey.currentContext!);
                              break;
                            case 6:
                              Navigator.of(
                                      App.fluxStoreNavigatorKey.currentContext!)
                                  .push(
                                MaterialPageRoute(
                                  builder: (context) => WebViewWithCookie(
                                      url:
                                          '${environment['serverConfig']['url']}/my-accounts/ctcb-credit/'),
                                ),
                              );
                              break;
                            case 7:
                              // FluxNavigate.pushNamed(
                              //   RouteList.updateUser,
                              //   forceRootNavigator: true,
                              // );
                              Navigator.of(
                                      App.fluxStoreNavigatorKey.currentContext!)
                                  .push(
                                MaterialPageRoute(
                                  builder: (context) =>
                                      const AddressListScreen(),
                                ),
                              );
                              break;
                          }
                        },
                        child: Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Container(
                            height: 100,
                            padding: const EdgeInsets.symmetric(horizontal: 4),
                            width: (MediaQuery.of(context).size.width - 56) / 4,
                            child: Column(
                              children: [
                                const SizedBox(height: 16),
                                Icon(
                                  memberTopIcons[i],
                                  size: 36,
                                  color: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .color,
                                ),
                                const SizedBox(height: 13),
                                FittedBox(
                                  child: Text(
                                    memberTopTitle[i],
                                    maxLines: 1,
                                    style: const TextStyle(fontSize: 15),
                                  ),
                                ),
                                const SizedBox(height: 9),
                              ],
                            ),
                          ),
                        ),
                      ),
                    const SizedBox(width: 12),
                  ],
                ),
                const SizedBox(height: 40),
              ],
            ),
          ),
        ],
      ),
    );
  }

  /// Render the Delivery Menu.
  /// Currently support WCFM
  Widget renderDeliveryBoy() {
    var isDelivery = user?.isDeliveryBoy ?? false;

    if (!isDelivery) {
      return const SizedBox();
    }

    return Card(
      color: Theme.of(context).backgroundColor,
      margin: const EdgeInsets.only(bottom: 2.0),
      elevation: 0,
      child: ListTile(
        onTap: () {
          FluxNavigate.push(
            MaterialPageRoute(
              builder: (context) =>
                  Services().widget.getDeliveryScreen(context, user)!,
            ),
            forceRootNavigator: true,
          );
        },
        leading: Icon(
          CupertinoIcons.cube_box,
          size: 24,
          color: Theme.of(context).colorScheme.secondary,
        ),
        title: Text(
          S.of(context).deliveryManagement,
          style: const TextStyle(fontSize: 16),
        ),
        trailing: Icon(
          Icons.arrow_forward_ios,
          size: 18,
          color: Theme.of(context).colorScheme.secondary,
        ),
      ),
    );
  }

  /// Render the Admin Vendor Menu.
  /// Currently support WCFM & Dokan. Will support WooCommerce soon.
  Widget renderVendorAdmin() {
    var isVendor = user?.isVender ?? false;
    return const SizedBox();

    // if (!isVendor) {
    //   return const SizedBox();
    // }
    //
    // return Card(
    //   color: Theme.of(context).backgroundColor,
    //   margin: const EdgeInsets.only(bottom: 2.0),
    //   elevation: 0,
    //   child: ListTile(
    //     onTap: () {
    //       FluxNavigate.push(
    //         MaterialPageRoute(
    //           builder: (context) =>
    //               Services().widget.getAdminVendorScreen(context, user)!,
    //         ),
    //         forceRootNavigator: true,
    //       );
    //     },
    //     leading: Icon(
    //       Icons.dashboard,
    //       size: 24,
    //       color: Theme.of(context).colorScheme.secondary,
    //     ),
    //     title: Text(
    //       S.of(context).vendorAdmin,
    //       style: const TextStyle(fontSize: 16),
    //     ),
    //     trailing: Icon(
    //       Icons.arrow_forward_ios,
    //       size: 18,
    //       color: Theme.of(context).colorScheme.secondary,
    //     ),
    //   ),
    // );
  }

  Widget renderVendorVacation() {
    var isVendor = user?.isVender ?? false;

    if ((kFluxStoreMV.contains(serverConfig['type']) && !isVendor) ||
        serverConfig['type'] != 'wcfm' ||
        !kVendorConfig['DisableNativeStoreManagement']) {
      return const SizedBox();
    }

    return Card(
      color: Theme.of(context).backgroundColor,
      margin: const EdgeInsets.only(bottom: 2.0),
      elevation: 0,
      child: ListTile(
        onTap: () {
          FluxNavigate.push(
            MaterialPageRoute(
              builder: (context) => Services().widget.renderVacationVendor(
                  user!.id!, user!.cookie!,
                  isFromMV: true),
            ),
            forceRootNavigator: true,
          );
        },
        leading: Icon(
          Icons.house,
          size: 24,
          color: Theme.of(context).colorScheme.secondary,
        ),
        title: Text(
          S.of(context).storeVacation,
          style: const TextStyle(fontSize: 16),
        ),
        trailing: Icon(
          Icons.arrow_forward_ios,
          size: 18,
          color: Theme.of(context).colorScheme.secondary,
        ),
      ),
    );
  }

  /// Render the custom profile link via Webview
  /// Example show some special profile on the woocommerce site: wallet, wishlist...
  Widget renderWebViewProfile() {
    if (user == null) {
      return Container();
    }

    var base64Str = EncodeUtils.encodeCookie(user!.cookie!);
    var profileURL = '${serverConfig['url']}/my-account?cookie=$base64Str';

    return Card(
      color: Theme.of(context).backgroundColor,
      margin: const EdgeInsets.only(bottom: 2.0),
      elevation: 0,
      child: ListTile(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => WebView(
                  url: profileURL, title: S.of(context).updateUserInfor),
            ),
          );
        },
        leading: Icon(
          CupertinoIcons.profile_circled,
          size: 24,
          color: Theme.of(context).colorScheme.secondary,
        ),
        title: Text(
          S.of(context).updateUserInfor,
          style: const TextStyle(fontSize: 16),
        ),
        trailing: Icon(
          Icons.arrow_forward_ios,
          color: Theme.of(context).colorScheme.secondary,
          size: 18,
        ),
      ),
    );
  }

  Widget renderItem(value) {
    IconData icon;
    String title;
    Widget trailing;
    Function() onTap;
    var isMultiVendor = kFluxStoreMV.contains(serverConfig['type']);
    var subGeneralSetting = widget.subGeneralSetting != null
        ? ConfigurationUtils.loadSubGeneralSetting(widget.subGeneralSetting!)
        : kSubGeneralSetting;
    switch (value) {
      case 'products':
        {
          if (!(user != null ? user!.isVender : false) || !isMultiVendor) {
            return const SizedBox();
          }
          trailing = const Icon(
            Icons.arrow_forward_ios,
            size: 18,
            color: kGrey600,
          );
          title = S.of(context).myProducts;
          icon = CupertinoIcons.cube_box;
          onTap = () => Navigator.pushNamed(context, RouteList.productSell);
          break;
        }

      case 'chat':
        {
          if (user == null || Config().isListingType || !isMultiVendor) {
            return Container();
          }
          trailing = const Icon(
            Icons.arrow_forward_ios,
            size: 18,
            color: kGrey600,
          );
          title = S.of(context).conversations;
          icon = CupertinoIcons.chat_bubble_2;
          onTap = () => Navigator.pushNamed(context, RouteList.listChat);
          break;
        }
      case 'wallet':
        {
          if (user == null || !Config().isWooType) {
            return Container();
          }
          trailing = const Icon(
            Icons.arrow_forward_ios,
            size: 18,
            color: kGrey600,
          );
          title = S.of(context).myWallet;
          icon = CupertinoIcons.square_favorites_alt;
          onTap = () => FluxNavigate.pushNamed(
                RouteList.myWallet,
                forceRootNavigator: true,
              );
          break;
        }
      case 'wishlist':
        {
          final wishListCount =
              Provider.of<ProductWishListModel>(context, listen: false)
                  .products
                  .length;
          trailing = Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (wishListCount > 0)
                Text(
                  '$wishListCount ${S.of(context).items}',
                  style: TextStyle(
                      fontSize: 14, color: Theme.of(context).primaryColor),
                ),
              const SizedBox(width: 5),
              const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600)
            ],
          );

          title = S.of(context).myWishList;
          icon = CupertinoIcons.heart;
          onTap = () => Navigator.of(context).pushNamed(RouteList.wishlist);
          break;
        }
      case 'notifications':
        {
          return Consumer<NotificationModel>(builder: (context, model, child) {
            return Column(
              children: [
                Card(
                  margin: const EdgeInsets.only(bottom: 2.0),
                  elevation: 0,
                  child: SwitchListTile(
                    secondary: Icon(
                      CupertinoIcons.bell,
                      color: Theme.of(context).colorScheme.secondary,
                      size: 24,
                    ),
                    value: model.enable,
                    activeColor: const Color(0xFF0066B4),
                    onChanged: (bool enableNotification) {
                      if (enableNotification) {
                        model.enableNotification();
                      } else {
                        model.disableNotification();
                      }
                    },
                    title: Text(
                      S.of(context).getNotification,
                      style: const TextStyle(fontSize: 16),
                    ),
                  ),
                ),
                const Divider(
                  color: Colors.black12,
                  height: 1.0,
                  indent: 75,
                  //endIndent: 20,
                ),
                if (model.enable) ...[
                  Card(
                    margin: const EdgeInsets.only(bottom: 2.0),
                    elevation: 0,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.of(context).pushNamed(RouteList.notify);
                      },
                      child: ListTile(
                        leading: Icon(
                          CupertinoIcons.list_bullet,
                          size: 22,
                          color: Theme.of(context).colorScheme.secondary,
                        ),
                        title: Text(S.of(context).listMessages),
                        trailing: const Icon(
                          Icons.arrow_forward_ios,
                          size: 18,
                          color: kGrey600,
                        ),
                      ),
                    ),
                  ),
                  const Divider(
                    color: Colors.black12,
                    height: 1.0,
                    indent: 75,
                    //endIndent: 20,
                  ),
                ],
              ],
            );
          });
        }
      case 'language':
        {
          icon = CupertinoIcons.globe;
          title = S.of(context).language;
          trailing = const Icon(
            Icons.arrow_forward_ios,
            size: 18,
            color: kGrey600,
          );
          onTap = () => Navigator.of(context).pushNamed(RouteList.language);
          break;
        }
      case 'currencies':
        {
          if (Config().isListingType) {
            return Container();
          }
          icon = CupertinoIcons.money_dollar_circle;
          title = S.of(context).currencies;
          trailing =
              const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () => Navigator.of(context).pushNamed(RouteList.currencies);
          break;
        }
      case 'darkTheme':
        {
          return Column(
            children: [
              Card(
                margin: const EdgeInsets.only(bottom: 2.0),
                elevation: 0,
                child: SwitchListTile(
                  secondary: Icon(
                    Provider.of<AppModel>(context).darkTheme
                        ? CupertinoIcons.sun_min
                        : CupertinoIcons.moon,
                    color: Theme.of(context).colorScheme.secondary,
                    size: 24,
                  ),
                  value: Provider.of<AppModel>(context).darkTheme,
                  activeColor: const Color(0xFF0066B4),
                  onChanged: (bool value) {
                    if (value) {
                      Provider.of<AppModel>(context, listen: false)
                          .updateTheme(true);
                    } else {
                      Provider.of<AppModel>(context, listen: false)
                          .updateTheme(false);
                    }
                  },
                  title: Text(
                    S.of(context).darkTheme,
                    style: const TextStyle(fontSize: 16),
                  ),
                ),
              ),
              const Divider(
                color: Colors.black12,
                height: 1.0,
                indent: 75,
                //endIndent: 20,
              ),
            ],
          );
        }

      ///show order history
      case 'order':
        {
          final storage = LocalStorage('data_order');
          var items = storage.getItem('orders');
          // if (user == null && items == null) {
          //   return Container();
          // }
          if (Config().isListingType) {
            return const SizedBox();
          }
          icon = CupertinoIcons.time;
          title = S.of(context).orderHistory;
          trailing =
              const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () {
            final user = Provider.of<UserModel>(context, listen: false).user;
            FluxNavigate.pushNamed(
              RouteList.orders,
              arguments: user,
            );
          };
          break;
        }
      case 'point':
        {
          if (!(kAdvanceConfig['EnablePointReward'] == true && user != null)) {
            return const SizedBox();
          }
          if (Config().isListingType) {
            return const SizedBox();
          }
          icon = CupertinoIcons.bag_badge_plus;
          title = S.of(context).myPoints;
          trailing =
              const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => UserPointScreen(),
                ),
              );
          break;
        }
      case 'rating':
        {
          icon = CupertinoIcons.star;
          title = S.of(context).rateTheApp;
          trailing =
              const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () {};
          break;
        }
      case 'privacy':
        {
          icon = CupertinoIcons.doc_text;
          title = S.of(context).agreeWithPrivacy;
          trailing =
              const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () {
            // final privacy = subGeneralSetting['privacy'];
            // final pageId =
            //     privacy?.pageId ?? kAdvanceConfig['PrivacyPoliciesPageId'];
            // final String? pageUrl =
            //     privacy?.webUrl ?? kAdvanceConfig['PrivacyPoliciesPageUrl'];
            // if (pageId != null && (privacy?.webUrl?.isEmpty ?? true)) {
            //   Navigator.push(
            //       context,
            //       MaterialPageRoute(
            //         builder: (context) => PostScreen(
            //             pageId: pageId,
            //             pageTitle: S.of(context).agreeWithPrivacy),
            //       ));
            //   return;
            // }
            // if (pageUrl?.isNotEmpty ?? false) {
            //   Navigator.push(
            //     context,
            //     MaterialPageRoute(
            //       builder: (context) => WebView(
            //         url: pageUrl,
            //         title: S.of(context).agreeWithPrivacy,
            //       ),
            //     ),
            //   );
            // }
            // final about = subGeneralSetting['about'];
            final privacyUrl = '${environment['serverConfig']['url']}/privacy/';

            if (kIsWeb) {
              return Tools.launchURL(privacyUrl);
            }
            return FluxNavigate.push(
              MaterialPageRoute(
                builder: (context) => WebViewWithCookie(
                  url: privacyUrl,
                  alwaysAddAppStyle: true,
                  title: '隱私權政策',
                ),
              ),
              forceRootNavigator: true,
            );
          };
          break;
        }
      case 'dispute':
        {
          icon = Icons.report_outlined;
          title = '爭議處理';
          trailing =
              const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () {
            // final privacy = subGeneralSetting['privacy'];
            // final pageId =
            //     privacy?.pageId ?? kAdvanceConfig['PrivacyPoliciesPageId'];
            // final String? pageUrl =
            //     privacy?.webUrl ?? kAdvanceConfig['PrivacyPoliciesPageUrl'];
            // if (pageId != null && (privacy?.webUrl?.isEmpty ?? true)) {
            //   Navigator.push(
            //       context,
            //       MaterialPageRoute(
            //         builder: (context) => PostScreen(
            //             pageId: pageId,
            //             pageTitle: S.of(context).agreeWithPrivacy),
            //       ));
            //   return;
            // }
            // if (pageUrl?.isNotEmpty ?? false) {
            //   Navigator.push(
            //     context,
            //     MaterialPageRoute(
            //       builder: (context) => WebView(
            //         url: pageUrl,
            //         title: S.of(context).agreeWithPrivacy,
            //       ),
            //     ),
            //   );
            // }
            // final about = subGeneralSetting['about'];
            final privacyUrl =
                '${environment['serverConfig']['url']}/%E7%88%AD%E8%AD%B0%E8%99%95%E7%90%86/';

            if (kIsWeb) {
              return Tools.launchURL(privacyUrl);
            }
            return FluxNavigate.push(
              MaterialPageRoute(
                builder: (context) => WebViewWithCookie(
                  url: privacyUrl,
                  // alwaysAddAppStyle: true,
                  title: '爭議處理',
                ),
              ),
              forceRootNavigator: true,
            );
          };
          break;
        }
      case 'cleanCache':
        {
          icon = Icons.cleaning_services;
          title = '登出並清除暫存檔';
          trailing =
              const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () async {
            try {
              var directory = await getTemporaryDirectory();
              await deleteDirectory(directory);
              Provider.of<CartModel>(context, listen: false).clearAddress();
              Provider.of<UserModel>(context, listen: false).logout();
              if (kLoginSetting['IsRequiredLogin'] ?? false) {
                Navigator.of(App.fluxStoreNavigatorKey.currentContext!)
                    .pushNamedAndRemoveUntil(
                  RouteList.login,
                  (route) => false,
                );
              }
              Utils.showSnackBar(context, text: '已清除快取');
            } catch (_) {
              Provider.of<CartModel>(context, listen: false).clearAddress();
              Provider.of<UserModel>(context, listen: false).logout();
              if (kLoginSetting['IsRequiredLogin'] ?? false) {
                Navigator.of(App.fluxStoreNavigatorKey.currentContext!)
                    .pushNamedAndRemoveUntil(
                  RouteList.login,
                  (route) => false,
                );
              }
              Utils.showSnackBar(context, text: '沒有可清除快取');
            }
          };
          break;
        }
      case 'aboutAPP':
        {
          icon = Icons.smartphone;
          title = '關於APP';
          trailing =
              const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () {
            Navigator.push(
              App.fluxStoreNavigatorKey.currentContext!,
              MaterialPageRoute(
                builder: (context) => const AboutAPPPage(),
              ),
            );
          };
          break;
        }
      case 'about':
        {
          icon = CupertinoIcons.info;
          title = S.of(context).aboutUs;
          trailing =
              const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () {
            final about = subGeneralSetting['about'];
            final aboutUrl = '${environment['serverConfig']['url']}/about/';

            if (kIsWeb) {
              return Tools.launchURL(aboutUrl);
            }
            return FluxNavigate.push(
              MaterialPageRoute(
                builder: (context) => WebViewWithCookie(
                  url: aboutUrl,
                  alwaysAddAppStyle: true,
                  title: '關於我們',
                ),
              ),
              forceRootNavigator: true,
            );
          };
          break;
        }
      case 'post':
        {
          if (user != null) {
            trailing = const Icon(
              Icons.arrow_forward_ios,
              size: 18,
              color: kGrey600,
            );
            title = S.of(context).postManagement;
            icon = CupertinoIcons.chat_bubble_2;
            onTap = () {
              Navigator.of(context).pushNamed(RouteList.postManagement);
            };
          } else {
            return const SizedBox();
          }

          break;
        }
      default:
        {
          trailing =
              const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          if (value.contains('web')) {
            var item = subGeneralSetting[value];
            title = item?.title ?? S.of(context).dataEmpty;
            var webUrl = item?.webUrl;
            if (item?.requiredLogin ?? false) {
              if (user == null) return const SizedBox();
              var base64Str = EncodeUtils.encodeCookie(user!.cookie!);
              webUrl = '$webUrl?cookie=$base64Str';
            }
            if (item != null) {
              icon = iconPicker(item.icon, item.iconFontFamily) ?? Icons.error;
              onTap = () {
                if (item.webViewMode) {
                  FluxNavigate.push(
                    MaterialPageRoute(
                      builder: (context) => WebView(url: webUrl, title: title),
                    ),
                    forceRootNavigator: true,
                  );
                } else {
                  Tools.launchURL(webUrl);
                }
              };
            } else {
              icon = Icons.error;
              onTap = () {};
            }
            break;
          }
          if (value.contains('post')) {
            var item = subGeneralSetting[value];
            title = item?.title ?? S.of(context).dataEmpty;
            if (item != null) {
              icon = iconPicker(item.icon, item.iconFontFamily) ?? Icons.error;
              onTap = () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          PostScreen(pageId: item.pageId, pageTitle: title),
                    ));
              };
            } else {
              icon = Icons.error;
              onTap = () {};
            }
            break;
          }
          icon = Icons.error;
          title = S.of(context).dataEmpty;
          onTap = () {};
          break;
        }
    }

    if (value.contains('title')) {
      var item = subGeneralSetting[value];
      title = item?.title ?? S.of(context).dataEmpty;
      var fontSize = item?.fontSize ?? 16.0;
      var titleColor =
          item?.titleColor != null ? HexColor(item!.titleColor) : null;
      var verticalPadding = item?.verticalPadding;
      var enableDivider = item?.enableDivider ?? false;

      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (enableDivider)
            Container(
              width: MediaQuery.of(context).size.width,
              height: 15.0,
              color: Theme.of(context).primaryColorLight,
            ),
          Padding(
            padding: EdgeInsets.only(
                left: itemPadding,
                right: itemPadding,
                top: verticalPadding?.dx ?? itemPadding,
                bottom: verticalPadding?.dy ?? itemPadding),
            child: Text(
              title,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    color:
                        titleColor ?? Theme.of(context).colorScheme.secondary,
                    fontSize: fontSize,
                    fontWeight: FontWeight.bold,
                  ),
            ),
          ),
        ],
      );
    }

    return Column(
      children: [
        Card(
          margin: const EdgeInsets.only(bottom: 2.0),
          elevation: 0,
          child: ListTile(
            leading: Icon(
              icon,
              color: Theme.of(context).colorScheme.secondary,
              size: 24,
            ),
            title: Text(
              title,
              style: const TextStyle(fontSize: 16),
            ),
            trailing: trailing,
            onTap: onTap,
          ),
        ),
        const Divider(
          color: Colors.black12,
          height: 1.0,
          indent: 75,
          //endIndent: 20,
        ),
      ],
    );
  }

  ///change setting page drawer icon
  Widget renderDrawerIcon() {
    var icon = Icons.menu;
    if (widget.drawerIcon != null) {
      icon = iconPicker(
              widget.drawerIcon!['icon'], widget.drawerIcon!['fontFamily']) ??
          Icons.menu;
    }
    return Icon(
      icon,
      color: Theme.of(context).textTheme.headline6!.color,
    );
  }

  ///add custom UI to member page
  static const List<String> memberTopTitle = [
    '訂單查詢',
    '業績查詢',
    '獎金查詢',
    '訊息通知',
    '會員資料',
    // '聯絡客服',
    '我的賣場',
    '信用卡綁定',
    '配送地址',
  ];

  static const List<IconData> memberTopIcons = [
    Icons.receipt_long_outlined,
    Icons.saved_search,
    Icons.attach_money_outlined,
    Icons.emoji_objects_outlined,
    Icons.manage_accounts_outlined,
    // Icons.perm_phone_msg_outlined,
    Icons.device_hub,
    Icons.credit_card_outlined,
    Icons.location_on_outlined,
  ];

  @override
  Widget build(BuildContext context) {
    super.build(context);

    var settings = widget.settings ?? kDefaultSettings;
    var background = widget.background ?? kProfileBackground;
    const textStyle = TextStyle(fontSize: 16);

    final appBar = (showAppBar(RouteList.profile))
        ? sliverAppBarWidget
        : SliverAppBar(
            backgroundColor: Theme.of(context).primaryColor,

            ///change setting page drawer icon position
            actions: [
              IconButton(
                icon: renderDrawerIcon(),
                onPressed: () => NavigateTools.onTapOpenDrawerMenu(context),
              )
            ],
            expandedHeight: bannerHigh,
            floating: true,
            pinned: true,
            flexibleSpace: FlexibleSpaceBar(
              title: Text(
                S.of(context).settings,
                style: const TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontWeight: FontWeight.w600),
              ),
              background: FluxImage(
                imageUrl: background,
                fit: BoxFit.cover,
              ),
            ),
          );

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,

      ///change setting page appbar
      appBar: AppBar(
        backgroundColor: Theme.of(context).backgroundColor,
        title: Text(
          S.of(context).settings,
          style: Theme.of(context)
              .textTheme
              .headline6!
              .copyWith(fontWeight: FontWeight.w700),
        ),

        ///change setting page drawer icon position
        actions: [
          IconButton(
            icon: Icon(
              Icons.qr_code_2,
              color: Theme.of(context).textTheme.headline6!.color,
            ),
            onPressed: () => FluxNavigate.push(
              MaterialPageRoute(
                builder: (context) => const QRCodePage(),
              ),
            ),
          ),
          IconButton(
            icon: renderDrawerIcon(),
            onPressed: () => NavigateTools.onTapOpenDrawerMenu(context),
          ),
        ],
      ),
      body: ListenableProvider.value(
        value: Provider.of<UserModel>(context),
        child: Consumer<UserModel>(builder: (context, model, child) {
          final user = model.user;
          final loggedIn = model.loggedIn;
          return CustomScrollView(
            slivers: <Widget>[
              // appBar,
              SliverList(
                delegate: SliverChildListDelegate(
                  <Widget>[
                    Column(
                      children: [
                        ///add custom UI to member page
                        customLayout(),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              const SizedBox(height: 10.0),
                              // if (user != null && user.name != null)
                              //   ListTile(
                              //     leading: (user.picture?.isNotEmpty ?? false)
                              //         ? CircleAvatar(
                              //             backgroundImage:
                              //                 NetworkImage(user.picture!),
                              //           )
                              //         : const Icon(Icons.face),
                              //     title: Text(
                              //       user.name!.replaceAll('lanfar', ''),
                              //       style: textStyle,
                              //     ),
                              //   ),
                              // if (user != null &&
                              //     user.email != null &&
                              //     user.email!.isNotEmpty)
                              //   ListTile(
                              //     leading: const Icon(Icons.email),
                              //     title: Text(
                              //       user.email!,
                              //       style: const TextStyle(fontSize: 16),
                              //     ),
                              //   ),

                              ///更新個人資訊
                              // if (user != null && !Config().isWordPress)
                              //   Card(
                              //     color: Theme.of(context).backgroundColor,
                              //     margin: const EdgeInsets.only(bottom: 2.0),
                              //     elevation: 0,
                              //     child: ListTile(
                              //       leading: Icon(
                              //         Icons.portrait,
                              //         color: Theme.of(context)
                              //             .colorScheme
                              //             .secondary,
                              //         size: 25,
                              //       ),
                              //       title: Text(
                              //         S.of(context).updateUserInfor,
                              //         style: const TextStyle(fontSize: 15),
                              //       ),
                              //       trailing: const Icon(
                              //         Icons.arrow_forward_ios,
                              //         size: 18,
                              //         color: kGrey600,
                              //       ),
                              //       onTap: () {
                              //         FluxNavigate.pushNamed(
                              //           RouteList.updateUser,
                              //           forceRootNavigator: true,
                              //         );
                              //       },
                              //     ),
                              //   ),
                              ///聯絡客服
                              Card(
                                color: Theme.of(context).backgroundColor,
                                margin: const EdgeInsets.only(bottom: 2.0),
                                elevation: 0,
                                child: ListTile(
                                  onTap: () {
                                    ShowContactPage().showContact(App
                                        .fluxStoreNavigatorKey.currentContext!);
                                  },
                                  leading:
                                      const Icon(Icons.perm_phone_msg_outlined),
                                  title: const Text(
                                    '聯絡客服',
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  trailing: const Icon(Icons.arrow_forward_ios,
                                      size: 18, color: kGrey600),
                                ),
                              ),

                              ///客服紀錄
                              Card(
                                color: Theme.of(context).backgroundColor,
                                margin: const EdgeInsets.only(bottom: 2.0),
                                elevation: 0,
                                child: ListTile(
                                  onTap: () {
                                    Navigator.of(App.fluxStoreNavigatorKey
                                            .currentContext!)
                                        .push(
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            const ServiceRecordPage(),
                                      ),
                                    );
                                    // openWebView(
                                    //     '${environment['serverConfig']['url']}/my-accounts/my-tickets/');
                                  },
                                  leading: const Icon(Icons.support_agent),
                                  title: const Text(
                                    '客服紀錄',
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  trailing: const Icon(Icons.arrow_forward_ios,
                                      size: 18, color: kGrey600),
                                ),
                              ),

                              ///一鍵結帳
                              // Card(
                              //   color: Theme.of(context).backgroundColor,
                              //   margin: const EdgeInsets.only(bottom: 2.0),
                              //   elevation: 0,
                              //   child: ListTile(
                              //     onTap: () {
                              //       openWebView('${environment['serverConfig']['url']}/my-accounts/one-click/');
                              //     },
                              //     leading: const Icon(Icons.touch_app_outlined),
                              //     title: const Text(
                              //       '一鍵結帳',
                              //       style: TextStyle(fontSize: 16),
                              //     ),
                              //     trailing: const Icon(
                              //         Icons.arrow_forward_ios,
                              //         size: 18,
                              //         color: kGrey600),
                              //   ),
                              // ),
                              ///優惠券
                              Card(
                                color: Theme.of(context).backgroundColor,
                                margin: const EdgeInsets.only(bottom: 2.0),
                                elevation: 0,
                                child: ListTile(
                                  onTap: () {
                                    Navigator.of(App.fluxStoreNavigatorKey
                                            .currentContext!)
                                        .push(
                                      MaterialPageRoute(
                                        fullscreenDialog: true,
                                        builder: (context) => const CouponList(
                                          isFromCart: true,
                                          coupons: null,
                                        ),
                                      ),
                                    );
                                    // openWebView('${environment['serverConfig']['url']}/my-accounts/wc-smart-coupons/');
                                  },
                                  leading: const Icon(
                                      Icons.confirmation_num_outlined),
                                  title: const Text(
                                    '優惠券',
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  trailing: const Icon(Icons.arrow_forward_ios,
                                      size: 18, color: kGrey600),
                                ),
                              ),

                              ///刪除帳號
                              // Card(
                              //   color: Theme.of(context).backgroundColor,
                              //   margin: const EdgeInsets.only(bottom: 2.0),
                              //   elevation: 0,
                              //   child: ListTile(
                              //     onTap: () async {
                              //       var delete = await Navigator.of(App.fluxStoreNavigatorKey
                              //               .currentContext!)
                              //           .push(
                              //         MaterialPageRoute(
                              //           builder: (context) =>
                              //               const DeletePage(),
                              //         ),
                              //       );
                              //       if (delete == true) {
                              //         ScaffoldMessenger.of(context)
                              //             .showSnackBar(
                              //           const SnackBar(
                              //             content: Text('已提交刪除帳號申請'),
                              //           ),
                              //         );
                              //       }
                              //       // openWebView('${environment['serverConfig']['url']}/my-accounts/wc-smart-coupons/');
                              //     },
                              //     leading: const Icon(Icons.delete_outline),
                              //     title: const Text(
                              //       '刪除帳號',
                              //       style: TextStyle(fontSize: 16),
                              //     ),
                              //     trailing: const Icon(Icons.arrow_forward_ios,
                              //         size: 18, color: kGrey600),
                              //   ),
                              // ),

                              ///聯盟行銷
                              // Card(
                              //   color: Theme.of(context).backgroundColor,
                              //   margin: const EdgeInsets.only(bottom: 2.0),
                              //   elevation: 0,
                              //   child: ListTile(
                              //     onTap: () {
                              //       Navigator.of(App.fluxStoreNavigatorKey
                              //               .currentContext!)
                              //           .push(
                              //         MaterialPageRoute(
                              //           builder: (context) =>
                              //               const MarketingPage(),
                              //         ),
                              //       );
                              //       // openWebView(
                              //       //     '${environment['serverConfig']['url']}/my-accounts/affiliate-marketing/');
                              //     },
                              //     leading: const Icon(Icons.device_hub),
                              //     title: const Text(
                              //       '聯盟行銷',
                              //       style: TextStyle(fontSize: 16),
                              //     ),
                              //     trailing: const Icon(Icons.arrow_forward_ios,
                              //         size: 18, color: kGrey600),
                              //   ),
                              // ),

                              ///兌換紀錄
                              // Card(
                              //   color: Theme.of(context).backgroundColor,
                              //   margin: const EdgeInsets.only(bottom: 2.0),
                              //   elevation: 0,
                              //   child: ListTile(
                              //     onTap: () {
                              //       openWebView('${environment['serverConfig']['url']}/my-accounts/redeem-coupon-list/');
                              //     },
                              //     leading: const Icon(Icons.history),
                              //     title: const Text(
                              //       '兌換紀錄',
                              //       style: TextStyle(fontSize: 16),
                              //     ),
                              //     trailing: const Icon(
                              //         Icons.arrow_forward_ios,
                              //         size: 18,
                              //         color: kGrey600),
                              //   ),
                              // ),
                              if (user == null)
                                Card(
                                  color: Theme.of(context).backgroundColor,
                                  margin: const EdgeInsets.only(bottom: 2.0),
                                  elevation: 0,
                                  child: ListTile(
                                    onTap: () {
                                      if (!loggedIn) {
                                        Navigator.of(
                                          App.fluxStoreNavigatorKey
                                              .currentContext!,
                                        ).pushNamed(RouteList.login);
                                        return;
                                      }
                                      Provider.of<UserModel>(context,
                                              listen: false)
                                          .logout();
                                      if (kLoginSetting['IsRequiredLogin'] ??
                                          false) {
                                        Navigator.of(
                                          App.fluxStoreNavigatorKey
                                              .currentContext!,
                                        ).pushNamedAndRemoveUntil(
                                          RouteList.login,
                                          (route) => false,
                                        );
                                      }
                                    },
                                    leading: const Icon(Icons.person),
                                    title: Text(
                                      loggedIn
                                          ? S.of(context).logout
                                          : S.of(context).login,
                                      style: const TextStyle(fontSize: 16),
                                    ),
                                    trailing: const Icon(
                                        Icons.arrow_forward_ios,
                                        size: 18,
                                        color: kGrey600),
                                  ),
                                ),
                              if (user != null)
                                Card(
                                  color: Theme.of(context).backgroundColor,
                                  margin: const EdgeInsets.only(bottom: 2.0),
                                  elevation: 0,
                                  child: ListTile(
                                    onTap: () async {
                                      try {
                                        var directory =
                                            await getTemporaryDirectory();
                                        await deleteDirectory(directory);
                                        Provider.of<CartModel>(context,
                                                listen: false)
                                            .clearAddress();
                                        Provider.of<UserModel>(context,
                                                listen: false)
                                            .logout();
                                        if (kLoginSetting['IsRequiredLogin'] ??
                                            false) {
                                          Navigator.of(App.fluxStoreNavigatorKey
                                                  .currentContext!)
                                              .pushNamedAndRemoveUntil(
                                            RouteList.login,
                                            (route) => false,
                                          );
                                        }
                                        Utils.showSnackBar(context,
                                            text: '已清除快取');
                                      } catch (_) {
                                        Provider.of<CartModel>(context,
                                                listen: false)
                                            .clearAddress();
                                        Provider.of<UserModel>(context,
                                                listen: false)
                                            .logout();
                                        if (kLoginSetting['IsRequiredLogin'] ??
                                            false) {
                                          Navigator.of(App.fluxStoreNavigatorKey
                                                  .currentContext!)
                                              .pushNamedAndRemoveUntil(
                                            RouteList.login,
                                            (route) => false,
                                          );
                                        }
                                        Utils.showSnackBar(context,
                                            text: '沒有可清除快取');
                                      }
                                    },
                                    leading: Icon(
                                      Icons.logout,
                                      size: 20,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary,
                                    ),

                                    // Image.asset(
                                    //   'assets/icons/profile/icon-logout.png',
                                    //   width: 24,
                                    //   color: Theme.of(context).colorScheme.secondary,
                                    // ),
                                    title: Text(
                                      S.of(context).logout,
                                      style: const TextStyle(fontSize: 16),
                                    ),
                                    trailing: const Icon(
                                        Icons.arrow_forward_ios,
                                        size: 18,
                                        color: kGrey600),
                                  ),
                                ),
                              const SizedBox(height: 30.0),
                              Text(
                                S.of(context).generalSetting,
                                style: const TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.w600),
                              ),
                              const SizedBox(height: 10.0),
                              // renderVendorAdmin(),

                              /// Render some extra menu for Vendor.
                              /// Currently support WCFM & Dokan. Will support WooCommerce soon.
                              // if (kFluxStoreMV.contains(serverConfig['type']) &&
                              //     (user?.isVender ?? false)) ...[
                              //   Services().widget.renderVendorOrder(context),
                              //   renderVendorVacation(),
                              // ],

                              // renderDeliveryBoy(),

                              /// Render custom Wallet feature
                              // renderWebViewProfile(),

                              /// render some extra menu for Listing
                              // if (user != null && Config().isListingType) ...[
                              //   Services().widget.renderNewListing(context),
                              //   Services().widget.renderBookingHistory(context),
                              // ],

                              const SizedBox(height: 10.0),
                              if (user != null)
                                const Divider(
                                  color: Colors.black12,
                                  height: 1.0,
                                  indent: 75,
                                  //endIndent: 20,
                                ),
                            ],
                          ),
                        ),

                        /// render list of dynamic menu
                        /// this could be manage from the Fluxbuilder
                        ...List.generate(
                          settings.length,
                          (index) {
                            var item = settings[index];
                            var isTitle = item.contains('title');
                            return Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: isTitle ? 0.0 : itemPadding),
                              child: renderItem(item),
                            );
                          },
                        ),
                        const SizedBox(height: 16),
                        Container(
                          padding: const EdgeInsets.only(
                            top: 20,
                          ),
                          child: Text(
                            'v' + nowVersion,
                            style: const TextStyle(
                              color: Colors.black54,
                              fontSize: 14,
                            ),
                          ),
                        ),
                        const SizedBox(height: 100),
                        InkWell(
                          onTap: () async {
                            var delete = await Navigator.of(
                                    App.fluxStoreNavigatorKey.currentContext!)
                                .push(
                              MaterialPageRoute(
                                builder: (context) => const DeletePage(),
                              ),
                            );
                            if (delete == true) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                  content: Text('已提交刪除帳號申請'),
                                ),
                              );
                            }
                          },
                          child: Container(
                            width: double.maxFinite,
                            alignment: Alignment.center,
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: const Text(
                              '刪除帳號',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 16),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          );
        }),
      ),

      ///improve smartChat UI
      floatingActionButton: buildSmartChatWidget(),
    );
  }

  // void openWebView(String url) {
  //   Navigator.of(App.fluxStoreNavigatorKey.currentContext!).push(
  //     MaterialPageRoute(
  //       builder: (context) => WebViewWithCookie(url: url),
  //     ),
  //   );
  // }

  Future<void> deleteDirectory(FileSystemEntity file) async {
    if (file is Directory) {
      final children = file.listSync();
      for (final child in children) {
        await deleteDirectory(child);
      }
    }
    await file.delete();
  }
}
