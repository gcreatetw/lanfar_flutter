import '../../../common/config.dart';
import '../../../common/tools.dart';
import '../../../screens/checkout/bill_screen.dart';
import '../../../screens/checkout/purchase_method_screen.dart';
import '../../entities/payment_method.dart';
import '../../entities/product.dart';
import '../../entities/product_variation.dart';
import '../../entities/user.dart';
import '../../index.dart';

enum PurchaseBagType {
  noNeed,
  onlyBag,
  onlyDM,
  needBoth,
}

extension PurchasrBagTypeEx on PurchaseBagType {
  String get name {
    switch (this) {
      case PurchaseBagType.noNeed:
        return '不需要，我要節能減碳，愛地球！';
      case PurchaseBagType.onlyBag:
        return '需要提袋';
      case PurchaseBagType.onlyDM:
        return '需要DM';
      case PurchaseBagType.needBoth:
        return '需要提袋、DM';
    }
  }

  String get keyValue {
    switch (this) {
      case PurchaseBagType.noNeed:
        return 'no_need';
      case PurchaseBagType.onlyBag:
        return 'need_bag';
      case PurchaseBagType.onlyDM:
        return 'need_dm';
      case PurchaseBagType.needBoth:
        return 'need_all';
    }
  }
}

mixin CartMixin {
  User? user;
  double taxesTotal = 0;
  List<Tax> taxes = [];
  double rewardTotal = 0;
  double walletAmount = 0;

  PaymentMethod? paymentMethod;

  String? notes;
  String? currency;
  Map<String, dynamic>? currencyRates;

  PurchaseType? purchaseMethod;
  InvoiceType? invoiceType;
  InvoiceDeliverType? invoiceDeliverType;
  String? carrierNo;
  OfficeType? invoiceOffice;

  List<GiftGroupModel> giftGroupList = [];
  List<GiftModel> giftList = [];
  List<GiftModel> giftsInCart = [];

  ///快速晉升
  bool fastPromote = false;

  ///購物車的提示訊息
  String? notifyText;
  String? checkoutNotify;

  ///票券meta
  dynamic ticketMeta;

  ///DM購物袋
  PurchaseBagType? purchaseBagType = PurchaseBagType.noNeed;

  ///是否停用購物車
  bool disable = false;
  String disableMessage = '';

  final Map<String?, Product?> item = {};

  final Map<String, ProductVariation?> productVariationInCart = {};

  final Map<String, List<AddonsOption>?> productAddonsOptionsInCart = {};

  // The IDs and quantities of products currently in the cart.
  final Map<String, int?> productsInCart = {};

  // The IDs and meta_data of products currently in the cart for woo commerce
  final Map<String, dynamic> productsMetaDataInCart = {};

  int get totalCartQuantity => productsInCart.keys.fold(0, (v, e) {
        if (getProductById(e)?.bundledBy != null) return v;
        return v + productsInCart[e]!;
      });

  bool _hasProductVariation(String id) =>
      productVariationInCart[id] != null &&
      productVariationInCart[id]!.price != null &&
      productVariationInCart[id]!.price!.isNotEmpty;

  double getProductPrice(id) {
    if (_hasProductVariation(id)) {
      return double.parse(productVariationInCart[id]!.price!) *
          productsInCart[id]!;
    } else {
      ///key調整
      // var productId = Product.cleanProductID(id);
      var price =
          PriceTools.getPriceProductValue(item[id], currency, onSale: true);
      if ((price?.isNotEmpty ?? false) && productsInCart[id] != null) {
        return double.parse(price!) * productsInCart[id]!;
      }
      return 0.0;
    }
  }

  double getProductPV(id) {
    ///key調整
    // var productId = Product.cleanProductID(id);

    if (_hasProductVariation(id)) {
      return productVariationInCart[id]!.pv * productsInCart[id]!;
    }
    var pv = item[id]!.pv;
    if ((pv?.isNotEmpty ?? false) && productsInCart[id] != null) {
      return double.parse(pv!) * productsInCart[id]!;
    }
    return 0.0;
  }

  double? getTotalPV() {
    return productsInCart.keys.fold(0.0, (sum, id) {
      if (getProductById(id)?.bundledBy != null) {
        return sum!;
      }
      return sum! + getProductPV(id);
    });
  }

  double getProductAddonsPrice(String id) {
    if (productAddonsOptionsInCart.isNotEmpty) {
      var price = 0.0;
      if (productAddonsOptionsInCart[id] == null) {
        return 0.0;
      }
      for (var option in productAddonsOptionsInCart[id]!) {
        var quantity = productsInCart[id] ?? 0;
        var optionPrice = (double.tryParse(option.price ?? '0.0') ?? 0.0);
        if (option.isQuantityBased) {
          optionPrice *= quantity;
        }
        price += optionPrice;
      }
      return price;
    }
    return 0.0;
  }

  double? getSubTotal() {
    return productsInCart.keys.fold(0.0, (sum, id) {
      if (getProductById(id)?.bundledBy != null) {
        return sum!;
      }
      return sum! + getProductPrice(id) + getProductAddonsPrice(id);
    });
  }

  void setPaymentMethod(data) {
    paymentMethod = data;
  }

  // Returns the Product instance matching the provided id.
  Product? getProductById(String id) {
    return item[id];
  }

  // Returns the Product instance matching the provided id.
  ProductVariation? getProductVariationById(String key) {
    return productVariationInCart[key];
  }

  String? getCheckoutId() {
    return '';
  }

  void setUser(data) {
    user = data;
  }

  void loadSavedCoupon() {}

  bool isEnabledShipping() {
    return kPaymentConfig['EnableShipping'];
  }

  void setWalletAmount(double total) {
    walletAmount = total;
  }

  bool isWalletCart() {
    return false;
  }

  void addWalletProductToCart({
    required Product product,
    int quantity = 1,
  }) {
    final key = product.id.toString();
    item[product.id] = product;
    productsInCart[key] = quantity;

    currency = (kAdvanceConfig['DefaultCurrency'] as Map)['currencyCode'];
  }
}

class GiftModel {
  int id;
  String name;
  String imageUrl;
  String pwNumberGiftAllowed;
  String? canSeveralGift;
  String auto;
  int giftQTY;

  GiftModel({
    required this.id,
    required this.name,
    required this.imageUrl,
    required this.pwNumberGiftAllowed,
    required this.canSeveralGift,
    required this.auto,
    required this.giftQTY,
  });

  factory GiftModel.fromJson(Map<String, dynamic> json) {
    return GiftModel(
      id: int.parse(json['id'].toString()),
      name: json['name'] as String,
      imageUrl: json['image'] as String,
      pwNumberGiftAllowed: '',
      canSeveralGift: '',
      auto: '',
      giftQTY: json['gift_qty'] as int,
    );
  }
}

class GiftGroupModel {
  String name;
  List<GiftModel> list;
  int? chooseId;

  GiftGroupModel({
    required this.name,
    required this.list,
  });

  factory GiftGroupModel.fromJson(Map<String, dynamic> json) {
    List<dynamic> l = json['gifts'];
    var list = <GiftModel>[];
    for (var element in l) {
      list.add(GiftModel.fromJson(element));
    }

    return GiftGroupModel(
      name: json['group_name'] as String,
      list: list,
    );
  }
}
