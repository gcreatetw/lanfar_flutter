import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:localstorage/localstorage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:the_apple_sign_in/the_apple_sign_in.dart' as apple;
import 'package:wp_notify/wp_notify.dart';

import '../common/config.dart';
import '../common/constants.dart';
import '../generated/l10n.dart';
import '../modules/firebase/firebase_service.dart';
import '../services/dependency_injection.dart';
import '../services/index.dart';
import 'entities/user.dart';

abstract class UserModelDelegate {
  void onLoaded(User? user);

  void onLoggedIn(User user);

  void onLogout(User? user);
}

class UserModel with ChangeNotifier {
  UserModel() {
    // init();
  }

  Future<void> init() async {
    await getUser();
    await refreshCookie();
  }

  final _storage = injector<LocalStorage>();

  final Services _service = Services();
  User? user;
  bool loggedIn = false;
  bool loading = false;
  bool smsLoading = false;
  UserModelDelegate? delegate;

  ///OTP
  int otpCountDown = 0;
  late Timer otpTimer;

  void startTimer(Function() onTimerFinished) {
    otpCountDown = 120;
    notifyListeners();
    otpTimer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (otpCountDown == 0) {
        otpTimer.cancel();
        onTimerFinished.call();
      } else {
        otpCountDown--;
        notifyListeners();
      }
    });
  }

  void updateUser(User newUser) {
    user = newUser;
    notifyListeners();
  }

  Future<String?> submitForgotPassword(
      {String? forgotPwLink, Map<String, dynamic>? data}) async {
    return await _service.api
        .submitForgotPassword(forgotPwLink: forgotPwLink, data: data);
  }

  /// Login by apple, This function only test on iPhone
  Future<void> loginApple({Function? success, Function? fail, context}) async {
    try {
      final result = await apple.TheAppleSignIn.performRequests([
        const apple.AppleIdRequest(
            requestedScopes: [apple.Scope.email, apple.Scope.fullName])
      ]);

      switch (result.status) {
        case apple.AuthorizationStatus.authorized:
          {
            user = await _service.api.loginApple(
                token: String.fromCharCodes(result.credential!.identityToken!));

            Services().firebase.loginFirebaseSMS(
                  authorizationCode: result.credential!.authorizationCode!,
                  identityToken: result.credential!.identityToken!,
                );

            loggedIn = true;
            await saveUser(user);
            success!(user);

            notifyListeners();
          }
          break;

        case apple.AuthorizationStatus.error:
          fail!(S.of(context).error(result.error!));
          break;
        case apple.AuthorizationStatus.cancelled:
          fail!(S.of(context).loginCanceled);
          break;
      }
    } catch (err) {
      fail!(S.of(context).loginErrorServiceProvider(err.toString()));
    }
  }

  /// Login by Firebase phone
  Future<void> loginFirebaseSMS(
      {String? phoneNumber,
      required Function success,
      Function? fail,
      context}) async {
    try {
      user = await _service.api.loginSMS(token: phoneNumber);
      loggedIn = true;
      await saveUser(user);
      success(user);

      notifyListeners();
    } catch (err) {
      fail!(S.of(context).loginErrorServiceProvider(err.toString()));
    }
  }

  /// Login by Facebook
  Future<void> loginFB({Function? success, Function? fail, context}) async {
    try {
      final result = await FacebookAuth.instance.login();
      switch (result.status) {
        case LoginStatus.success:
          final accessToken = await FacebookAuth.instance.accessToken;

          Services().firebase.loginFirebaseFacebook(token: accessToken!.token);

          user = await _service.api.loginFacebook(token: accessToken.token);
          loggedIn = true;
          await saveUser(user);
          success!(user);
          break;
        case LoginStatus.cancelled:
          fail!(S.of(context).loginCanceled);
          break;
        default:
          fail!(result.message);
          break;
      }
      notifyListeners();
    } catch (err) {
      fail!(S.of(context).loginErrorServiceProvider(err.toString()));
    }
  }

  Future<void> loginGoogle({Function? success, Function? fail, context}) async {
    try {
      var _googleSignIn = GoogleSignIn(scopes: ['email']);
      var res = await _googleSignIn.signIn();

      if (res == null) {
        fail!(S.of(context).loginCanceled);
      } else {
        var auth = await res.authentication;
        Services().firebase.loginFirebaseGoogle(token: auth.accessToken);
        user = await _service.api.loginGoogle(token: auth.accessToken);
        loggedIn = true;
        await saveUser(user);
        success!(user);
        notifyListeners();
      }
    } catch (err, trace) {
      printLog(trace);
      printLog(err);
      fail!(S.of(context).loginErrorServiceProvider(err.toString()));
    }
  }

  Future<void> saveUser(User? user, {bool runLoaded = true}) async {
    try {
      if (Services().firebase.isEnabled &&
          kFluxStoreMV.contains(serverConfig['type'])) {
        Services().firebase.saveUserToFirestore(user: user);
      }

      // save to Preference
      var prefs = injector<SharedPreferences>();
      await prefs.setBool('loggedIn', true);

      // save the user Info as local storage
      await _storage.setItem(kLocalKey['userInfo']!, user);
      if (runLoaded) {
        delegate?.onLoaded(user);
      }
    } catch (err) {
      printLog(err);
    }
  }

  Future<void> saveSMSUser(User? user) async {
    if (user != null) {
      this.user = user;
      await saveUser(user);
    }
  }

  Future<void> getUser({bool runLoaded = true}) async {
    try {
      final json = _storage.getItem(kLocalKey['userInfo']!);
      if (json != null) {
        user = User.fromLocalJson(json);
        loggedIn = true;
        var userInfo = await _service.api.getUserInfo(user!.cookie);
        if (userInfo != null) {
          userInfo.isSocial = user!.isSocial;
          user = userInfo;
        } else {
          await logout(setLogout: false);
          // await Navigator.of(
          //   App.fluxStoreNavigatorKey.currentContext!,
          // ).pushNamedAndRemoveUntil(
          //   RouteList.login,
          //   (route) => false,
          // );
          return;
        }
        await saveUser(user, runLoaded: runLoaded);
        if (runLoaded) {
          delegate?.onLoaded(user);
        }
        notifyListeners();
      }
    } catch (err) {
      printLog(err);
    }
  }

  void setLoading(bool isLoading) {
    loading = isLoading;
    notifyListeners();
  }

  void setSMSLoading(bool isLoading) {
    smsLoading = isLoading;
    notifyListeners();
  }

  Future<void> createUser({
    String? username,
    String? password,
    String? firstName,
    String? lastName,
    String? phoneNumber,
    bool? isVendor,
    String? identity,
    required Function success,
    Function? fail,
  }) async {
    try {
      loading = true;
      notifyListeners();
      Services().firebase.createUserWithEmailAndPassword(
          email: username!, password: password!);

      user = await _service.api.createUser(
        firstName: firstName,
        lastName: lastName,
        username: username,
        password: password,
        phoneNumber: phoneNumber,
        identity: identity,
        isVendor: isVendor ?? false,
      );
      loggedIn = true;
      await saveUser(user);
      success(user);

      loading = false;
      notifyListeners();
    } catch (err) {
      fail!(err.toString().replaceAll('Exception', '錯誤'));
      loading = false;
      notifyListeners();
    }
  }

  bool showBio = true;

  Future<void> logout({bool setLogout = true}) async {
    showBio = false;
    var fcmToken = await FirebaseServices().getMessagingToken();

    Services().firebase.signOut();

    await FacebookAuth.instance.logOut();

    delegate?.onLogout(user);
    user = null;
    loggedIn = false;
    try {
      await _storage.deleteItem(kLocalKey['userInfo']!);
      await _storage.deleteItem(kLocalKey['shippingAddress']!);
      await _storage.deleteItem(kLocalKey['recentSearches']!);
      // await _storage.deleteItem(kLocalKey['opencart_cookie']!);
      await _storage.setItem(kLocalKey['userInfo']!, null);

      var prefs = injector<SharedPreferences>();
      if (setLogout) {
        await prefs.setBool('loggedIn', false);
      }
      // await prefs.setString('username', '');
      // await prefs.setString('password', '');

      await _service.api.logout();
      try {
        await WPNotifyAPI.instance.api((request) =>
            request.wpNotifyUpdateToken(token: fcmToken!, status: false));
      } on Exception catch (e) {
        print(e);
        rethrow;
      }
    } catch (err) {
      printLog(err);
    }
    notifyListeners();
  }

  Future<void> login({
    username,
    password,
    required Function(User) success,
    Function? fail,
  }) async {
    try {
      loading = true;
      notifyListeners();
      user = await _service.api.login(
        username: username,
        password: password,
      );

      Services()
          .firebase
          .loginFirebaseEmail(email: username, password: password);

      loggedIn = true;
      await saveUser(user);
      success.call(user!);
      var prefs = injector<SharedPreferences>();
      await prefs.setString('username', username);
      await prefs.setString('password', password);
      await prefs.setBool('loggedIn', true);
      await Services().api.getCookie();
      loading = false;
      notifyListeners();
    } catch (err) {
      loading = false;
      fail?.call(err.toString().replaceAll('Exception:', ''));
      notifyListeners();
    }
  }

  Future<void> refreshCookie() async {
    var prefs = injector<SharedPreferences>();
    final username = prefs.getString('username');
    final password = prefs.getString('password');

    if (!(prefs.getBool('loggedIn') ?? false)) return;
    if (username == null || username.isEmpty) {
      return;
    }

    if (user == null) {
      await login(
        username: username,
        password: password,
        success: (user) {
          // getUser();
        },
      );
    } else {
      unawaited(
        login(
          username: username,
          password: password,
          success: (user) {
            // getUser();
          },
        ),
      );
    }
  }
}
