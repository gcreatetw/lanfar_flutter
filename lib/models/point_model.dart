import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import '../common/constants.dart';

import '../services/index.dart';
import 'entities/point.dart';

class PointModel extends ChangeNotifier {
  final Services _service = Services();
  Point? point;

  int lanfarPoint = 0;

  Future<void> getLanfarPoint(BuildContext context)async {
    final storage = injector<LocalStorage>();
    final userJson = storage.getItem(kLocalKey['userInfo']!);
    var user = await Services().api.getUserInfo(userJson['cookie']);
    lanfarPoint = user?.point ?? 0;
    notifyListeners();
  }



  Future<void> getMyPoint(String? token) async {
    try {
      // point = await _service.api.getMyPoint(token);
    } catch (err) {
      printLog('getMyPoint $err');
    }
    notifyListeners();
  }
}
