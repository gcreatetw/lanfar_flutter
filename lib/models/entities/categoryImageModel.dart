import 'package:quiver/strings.dart';


/*
  "image": {
    "id": 8985,
    "date_created": "2022-07-07T09:41:40",
    "date_created_gmt": "2022-07-07T01:41:40",
    "date_modified": "2022-07-07T09:41:40",
    "date_modified_gmt": "2022-07-07T01:41:40",
    "src": "https://demo.gcreate.com.tw/nss_lanfar/wp-content/uploads/2022/07/20220706-IDP-2.jpg",
    "name": "20220706-IDP-2",
    "alt": ""
    },
*/
class CategoryImageModel {
  int? id;
  String? date_created;
  String? date_created_gmt;
  String? date_modified;
  String? date_modified_gmt;
  String? src;
  String? name;
  String? alt;

  CategoryImageModel.fromJson(Map<String, dynamic> json) {
    id = json['id'] ?? 0;
    date_created = json['date_created'];
    date_created_gmt = json['date_created_gmt'];
    date_modified = json['date_modified'];
    date_modified_gmt = json['date_modified_gmt'];
    src = json['src'];
    name = json['name'];
    alt = json['alt'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'date_created': date_created,
      'date_created_gmt': date_created_gmt,
      'date_modified': date_modified,
      'date_modified_gmt': date_modified_gmt,
      'src': src,
      'name': name,
      'alt': alt,
    };
  }
}
