import 'dart:convert' as convert;
import 'dart:convert';
import 'dart:core';
import 'dart:developer';
import 'dart:io';

import 'package:crypto/crypto.dart' as crypto;
import 'package:flutter/foundation.dart' show compute, debugPrint, kDebugMode;
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:localstorage/localstorage.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:quiver/strings.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../common/config.dart';
import '../../../common/constants.dart';
import '../../../from_lanfar/model/banner_data.dart';
import '../../../from_lanfar/model/calendar_event.dart';
import '../../../from_lanfar/model/default_checkout_response.dart';
import '../../../from_lanfar/model/get_address_response.dart';
import '../../../from_lanfar/model/get_article_content_response.dart';
import '../../../from_lanfar/model/get_articles_response.dart';
import '../../../from_lanfar/model/get_bonus_date_response.dart';
import '../../../from_lanfar/model/get_bonus_detail_response.dart';
import '../../../from_lanfar/model/get_change_order_response.dart';
import '../../../from_lanfar/model/get_monarch_point_response.dart';
import '../../../from_lanfar/model/get_performance_overview_content_response.dart';
import '../../../from_lanfar/model/get_performance_overview_date_response.dart';
import '../../../from_lanfar/model/get_service_content_response.dart';
import '../../../from_lanfar/model/get_service_record_response.dart';
import '../../../from_lanfar/model/get_triumph_query_response.dart';
import '../../../from_lanfar/model/market_info_response.dart';
import '../../../from_lanfar/model/new_order_history_detail_model.dart';
import '../../../from_lanfar/model/new_order_history_model.dart';
import '../../../from_lanfar/model/notification_response.dart';
import '../../../from_lanfar/model/point_history_response.dart';
import '../../../from_lanfar/model/remove_coupon_response_model.dart';
import '../../../from_lanfar/model/sell_product_msg_response.dart';
import '../../../from_lanfar/provider/article_list_page_provider.dart';
import '../../../from_lanfar/provider/marketing_page_provider.dart';
import '../../../models/booking/staff_booking_model.dart';
import '../../../models/entities/brand.dart';
import '../../../models/entities/order_delivery_date.dart';
import '../../../models/entities/prediction.dart';
import '../../../models/index.dart';
import '../../../modules/dynamic_layout/config/banner_config.dart';
import '../../../screens/checkout/bill_screen.dart';
import '../../../services/base_services.dart';
import '../../../services/https.dart';
import '../../../services/index.dart';
import 'woocommerce_api.dart';

class WooCommerceService extends BaseServices {
  Map<String, dynamic>? configCache;
  final WooCommerceAPI wcApi;

  final String isSecure;
  List<Category> categories = [];
  Map<String, Tag> tags = {};
  String? currentLanguage;
  Map<String, List<Product>> categoryCache = <String, List<Product>>{};
  String deviceVersion = '';

  WooCommerceService({
    required String domain,
    String? blogDomain,
    required String consumerKey,
    required String consumerSecret,
  })  : wcApi = WooCommerceAPI(domain, consumerKey, consumerSecret),
        isSecure = domain.contains('https') ? '' : '&insecure=cool',
        super(domain: domain, blogDomain: blogDomain) {
    configCache = null;
    categories = [];
    currentLanguage = null;
    categoryCache = <String, List<Product>>{};

    Future.microtask(() async {
      final packageInfo = await PackageInfo.fromPlatform();
      deviceVersion = packageInfo.version +
          ',' +
          Platform.operatingSystem +
          Platform.operatingSystemVersion;
    });
    getCookie();
  }

  Product jsonParser(item) {
    var product = Product.fromJson(item);
    if (item['store'] != null) {
      if (item['store']['errors'] == null) {
        product = Services().widget.updateProductObject(product, item);
      }
    }
    return product;
  }

  @override
  Future<List<Category>> getCategories({lang}) async {
    try {
      if (categories.isNotEmpty && currentLanguage == lang) {
        return categories;
      }
      currentLanguage = lang;
      var list = <Category>[];
      var isEnd = false;
      var page = 1;
      var limit = 100;

      while (!isEnd) {
        var categories = await getCategoriesByPage(lang: lang, page: page, limit: limit);
        if (categories.isEmpty || categories.length < limit) {
          isEnd = true;
        }
        page = page + 1;
        list = [...list, ...categories];
      }
      categories = list;
      return list;
    } catch (e) {
      return [];
      //rethrow;
    }
  }

  @override
  Future<List<Category>> getCategoriesByPage({
    lang,
    page,
    limit,
    storeId,
  }) async {
    try {
      var url =
          'products/categories?exclude=$kExcludedCategory&per_page=$limit&page=$page&hide_empty=${kAdvanceConfig['HideEmptyCategories'] ?? true}';
      if (lang != null && kAdvanceConfig['isMultiLanguages']) {
        url += '&lang=$lang';
      }
      var response = await wcApi.getAsync(url, version: 3);
      return compute(CategoryModel.parseCategoryList, response);
    } catch (e) {
      //This error exception is about your RegetHomeCache(st API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<List<Product>> getProducts({userId}) async {
    try {
      var endpoint = 'products';
      if (userId != null) {
        endpoint += '?user_id=$userId';
      }
      var response = await wcApi.getAsync(endpoint);
      var list = <Product>[];
      if (response is Map && isNotBlank(response['message'])) {
        throw Exception(response['message']);
      } else {
        for (var item in response) {
          list.add(jsonParser(item));
        }
        return list;
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<List<Product>?> fetchProductsLayout({
    config,
    lang,
    userId,
    bool refreshCache = false,
  }) async {
    try {
      /// Load first page from cache.
      if (kAdvanceConfig['isCaching'] &&
          configCache != null &&
          (config['page'] == 1 || config['page'] == null)) {
        var obj;
        final horizontalLayout = configCache!['HorizonLayout'] as List?;
        if (horizontalLayout != null) {
          obj = horizontalLayout.firstWhere(
              (o) =>
                  o['layout'] == config['layout'] &&
                  ((o['category'] != null &&
                          o['category'] == config['category']) ||
                      (o['tag'] != null && o['tag'] == config['tag'])),
              orElse: () => null);
          if (obj != null && obj['data'].length > 0) return obj['data'];
        }

        final verticalLayout = configCache!['VerticalLayout'];
        if (verticalLayout != null &&
            verticalLayout['layout'] == config['layout'] &&
            ((verticalLayout['category'] != null &&
                    verticalLayout['category'] == config['category']) ||
                (verticalLayout['tag'] != null &&
                    verticalLayout['tag'] == config['tag']))) {
          return verticalLayout['data'];
        }
      }

      var endPoint = 'products?status=publish';
      if (kAdvanceConfig['isMultiLanguages']) {
        endPoint += '&lang=$lang';
      }
      if (config.containsKey('category') && config['category'] != null) {
        endPoint += "&category=${config["category"]}";
      }
      if (config.containsKey('tag') && config['tag'] != null) {
        endPoint += "&tag=${config["tag"]}";
      }

      /// Add featured filter
      if (config.containsKey('featured') && config['featured'] != null) {
        endPoint += "&featured=${config["featured"]}";
      }

      /// Add onSale filter
      if (config.containsKey('onSale') && config['onSale'] != null) {
        endPoint += "&on_sale=${config["onSale"]}";
      }

      if (config.containsKey('page')) {
        endPoint += "&page=${config["page"]}";
      }
      if (config.containsKey('limit')) {
        endPoint += "&per_page=${config["limit"] ?? apiPageSize}";
      }
      if (userId != null) {
        endPoint += '&user_id=$userId';
      }

      var response = await wcApi.getAsync(
        endPoint,
        refreshCache: refreshCache,
      );

      if (response is Map && isNotBlank(response['message'])) {
        printLog('WooCommerce Error: ' + response['message']);
        return [];
      }

      return ProductModel.parseProductList(response, config);
    } catch (e, trace) {
      printLog(trace);
      return [];
    }
  }

  /// get all attribute_term for selected attribute for filter menu
  @override
  Future<List<SubAttribute>> getSubAttributes({int? id}) async {
    try {
      var list = <SubAttribute>[];

      for (var i = 1; i < 100; i++) {
        var subAttributes = await getSubAttributesByPage(id: id, page: i);
        if (subAttributes.isEmpty) {
          break;
        }
        list = list + subAttributes;
      }
      return list;
    } catch (e) {
      rethrow;
    }
  }

  Future<List<SubAttribute>> getSubAttributesByPage({id, page}) async {
    try {
      var listAttributes = <SubAttribute>[];

      var url = 'products/attributes/$id/terms?per_page=100&page=$page';
      var response = await wcApi.getAsync(url);

      if (response is Map && isNotBlank(response['message'])) {
        throw Exception(response['message']);
      } else {
        for (var item in response) {
          if (item['count'] > 0) {
            listAttributes.add(SubAttribute.fromJson(item));
          }
        }
        return listAttributes;
      }
    } catch (e) {
      rethrow;
    }
  }

  //get all attributes for filter menu
  @override
  Future<List<FilterAttribute>> getFilterAttributes() async {
    try {
      var list = <FilterAttribute>[];
      var endPoint = 'products/attributes';

      var response = await wcApi.getAsync(endPoint);

      for (var item in response) {
        list.add(FilterAttribute.fromJson(item));
      }

      return list;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<Product>?> fetchProductsByCategory({
    categoryId,
    tagId,
    page = 1,
    minPrice,
    maxPrice,
    orderBy,
    lang,
    order,
    attribute,
    attributeTerm,
    featured,
    onSale,
    listingLocation,
    userId,
    bool refreshCache = false,
  }) async {
    try {
      var list = <Product>[];

      /// this cause a bug on Profile List
      /// we just allow cache if the totalItem = perPageItem otherwise, should reload
      if ((page == 0 || page == 1) &&
          categoryCache['$categoryId'] != null &&
          categoryCache['$categoryId']!.isNotEmpty &&
          featured == null &&
          onSale == null &&
          attributeTerm == null) {
        if (categoryCache['$categoryId']!.length == apiPageSize) {
          return categoryCache['$categoryId'];
        }
      }

      var endPoint =
          'products?status=publish&per_page=$apiPageSize&page=$page&skip_cache=1';
      if (kAdvanceConfig['isMultiLanguages']) {
        endPoint += '&lang=$lang';
      }
      if (categoryId != null && categoryId != '-1' && categoryId != '0') {
        endPoint += '&category=$categoryId';
      }
      if (tagId != null) {
        endPoint += '&tag=$tagId';
      }
      if (minPrice != null && maxPrice != null && maxPrice > 0) {
        endPoint +=
            '&min_price=${(minPrice as double).toInt().toString()}&max_price=${(maxPrice as double).toInt().toString()}';
      }
      if (orderBy != null) {
        endPoint += '&orderby=$orderBy';
      }
      if (order != null) {
        endPoint += '&order=$order';
      }
      if (featured != null) {
        endPoint += '&featured=$featured';
      }
      if (onSale != null && onSale) {
        endPoint += '&on_sale=$onSale';
      }
      if (attribute != null && attributeTerm != null) {
        endPoint += '&attribute=$attribute&attribute_term=$attributeTerm';
      }
      if (kAdvanceConfig['hideOutOfStock']) {
        endPoint += '&stock_status=instock';
      }
      if (userId != null) {
        endPoint += '&user_id=$userId';
      }

      endPoint += '&orderby=menu_order&order=asc';

      var response = await wcApi.getAsync(
        endPoint,
        version: 3,
        refreshCache: refreshCache,
      );

      if (response is Map && isNotBlank(response['message'])) {
        throw Exception(response['message']);
      } else {
        for (var item in response) {
          var product = jsonParser(item);

          if ((kAdvanceConfig['hideOutOfStock'] ?? false) &&
              !product.inStock!) {
            /// hideOutOfStock product
            continue;
          }

          if (categoryId != null) {
            product.categoryId = categoryId;
          }
          list.add(product);
        }
        return list;
      }
    } catch (e, trace) {
      printLog(trace);
      rethrow;
    }
  }

  @override
  Future<User?> loginFacebook({String? token}) async {
    const cookieLifeTime = 120960000000;

    try {
      var endPoint =
          '$domain/wp-json/api/flutter_user/fb_connect/?second=$cookieLifeTime'
          // ignore: prefer_single_quotes
          "&access_token=$token$isSecure";

      var response = await httpCache(endPoint.toUri()!);

      var jsonDecode = convert.jsonDecode(response.body);

      if (jsonDecode['wp_user_id'] == null || jsonDecode['cookie'] == null) {
        throw Exception(jsonDecode['message']);
      }

      return User.fromWooJson(jsonDecode);
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<User> loginSMS({String? token}) async {
    try {
      var endPoint =
          '$domain/wp-json/api/flutter_user/firebase_sms_login/?phone=$token$isSecure';
      if (kAdvanceConfig['EnableNewSMSLogin'] ?? false) {
        endPoint =
            // ignore: prefer_single_quotes
            "$domain/wp-json/api/flutter_user/firebase_sms_login_v2?phone=$token$isSecure";
      }

      var response = await httpGet(endPoint.toUri()!);

      var jsonDecode = convert.jsonDecode(response.body);

      if (jsonDecode['wp_user_id'] == null || jsonDecode['cookie'] == null) {
        throw Exception(jsonDecode['message']);
      }

      return User.fromWooJson(jsonDecode);
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<User?> loginApple({String? token}) async {
    try {
      var endPoint = '$domain/wp-json/api/flutter_user/apple_login';

      var response = await httpPost(endPoint.toUri()!,
          body: convert.jsonEncode({'token': token}),
          headers: {'Content-Type': 'application/json'});

      var jsonDecode = convert.jsonDecode(response.body);

      if (jsonDecode['wp_user_id'] == null || jsonDecode['cookie'] == null) {
        throw Exception(jsonDecode['message']);
      }

      return User.fromWooJson(jsonDecode);
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<List<Review>> getReviews(productId) async {
    try {
      var response =
          await wcApi.getAsync('products/$productId/reviews', version: 2);
      var list = <Review>[];
      if (response is Map && isNotBlank(response['message'])) {
        throw Exception(response['message']);
      } else {
        for (var item in response) {
          list.add(Review.fromJson(item));
        }
        return list;
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future createReview(
      {String? productId, Map<String, dynamic>? data, String? token}) async {
    try {
      data!['product_id'] = productId;
      final response = await httpPost(
        '$domain/wp-json/api/flutter_woo/products/reviews'.toUri()!,
        body: convert.jsonEncode(data),
        headers: {'User-Cookie': token!, 'Content-Type': 'application/json'}
          ..addAll(header),
      );
      var body = convert.jsonDecode(response.body);
      if (body['message'] == null) {
        return;
      } else {
        throw Exception(body['message']);
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<List<ProductVariation>> getProductVariations(Product product,
      {String? lang = 'en'}) async {
    try {
      final list = <ProductVariation>[];
      var page = 1;

      while (true) {
        var endPoint =
            'products/${product.id}/variations?per_page=100&page=$page';
        if (kAdvanceConfig['isMultiLanguages']) {
          endPoint += '&lang=$lang';
        }

        var response = await wcApi.getAsync(endPoint, refreshCache: true);
        if (response is Map && isNotBlank(response['message'])) {
          throw Exception(response['message']);
        } else {
          for (var item in response) {
            if (item['visible']) {
              list.add(ProductVariation.fromJson(item));
            }
          }

          if (response is List && response.length < 100) {
            /// No more data.
            break;
          }

          /// Fetch next page.
          page++;
        }
      }

      return list;
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<List<ShippingMethod>> getShippingMethods(
      {CartModel? cartModel,
      String? token,
      String? checkoutId,
      Store? store}) async {
    try {
      if (isBookingProduct(cartModel!)) {
        return getShippingMethodsByWooApi();
      }

      var params = Order().toJson(cartModel, null, false);

      if (kVendorConfig['DisableVendorShipping'] == false &&
          Config().isVendorType()) {
        List lineItems = params['line_items'];
        for (var element in lineItems) {
          final product = cartModel.item[element['product_id']];
          if ((store == null && product!.store == null) ||
              (store != null &&
                  product!.store != null &&
                  product.store!.id == store.id)) {
            params['line_items'] = [element];
          }
        }
      }
      if (cartModel.user?.username != null) {
        params.addAll({'user_login': cartModel.user?.username});
      }
      params.addAll({
        'device_version': deviceVersion,
      });
      var list = <ShippingMethod>[];

      final response = await httpPost(
        '$domain/wp-json/api/flutter_woo/shipping_methods'.toUri()!,
        body: convert.jsonEncode(params),
        headers: {'Content-Type': 'application/json'}..addAll(header),
      );
      final body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        for (var item in body) {
          list.add(ShippingMethod.fromJson(item));
        }
      } else if (body['message'] != null) {
        throw Exception(body['message']);
      }
      if (list.isEmpty) {
        throw Exception(
            'Your selected address is not supported by any Shipping method, please update the billing address again!');
      }
      return list;
    } catch (err) {
      rethrow;
    }
  }

  bool isBookingProduct(CartModel cartModel) {
    var isBooking = false;
    for (var key in cartModel.productsInCart.keys) {
      ///key調整

      var product = cartModel.item[key]!;
      if (product.bookingInfo != null) {
        isBooking = true;
      }
    }
    return isBooking;
  }

  Future<List<ShippingMethod>> getShippingMethodsByWooApi() async {
    try {
      var list = <ShippingMethod>[];
      var response = await wcApi.getAsync('shipping/zones/1/methods');
      if (response is Map && isNotBlank(response['message'])) {
        throw Exception(response['message']);
      } else {
        for (var item in response) {
          var methodItem = {
            'id': item['instance_id'],
            'method_id': item['method_id'],
            'instance_id': item['instance_id'],
            'label': item['title'],
            'cost': item['settings']['cost'] != null &&
                    item['settings']['cost']['value'] != null
                ? item['settings']['cost']['value']
                : 0,
            'shipping_tax': 0
          };
          list.add(ShippingMethod.fromJson(methodItem));
        }
      }
      if (list.isEmpty) {
        throw Exception(
            'Your selected address is not supported by any Shipping method, please update the billing address again!');
      }
      return list;
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<List<PaymentMethod>> getPaymentMethods(
      {CartModel? cartModel,
      ShippingMethod? shippingMethod,
      String? token}) async {
    try {
      if (isBookingProduct(cartModel!)) {
        return getPaymentMethodsByWooApi();
      }

      final params = Order().toJson(cartModel, null, false);

      params.addAll({
        'device_version': deviceVersion,
      });

      var list = <PaymentMethod>[];
      final response = await httpPost(
          '$domain/wp-json/api/flutter_woo/payment_methods'.toUri()!,
          body: convert.jsonEncode(params),
          headers: {
            'Content-Type': 'application/json',
            'User-Cookie': token ?? '',
          }..addAll(header));
      final body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        for (var item in body) {
          list.add(PaymentMethod.fromJson(item));
        }
      } else if (body['message'] != null) {
        throw Exception(body['message']);
      }
      return list;
    } catch (err) {
      rethrow;
    }
  }

  Future<List<PaymentMethod>> getPaymentMethodsByWooApi() async {
    try {
      var list = <PaymentMethod>[];
      var response = await wcApi.getAsync('payment_gateways');
      if (response is Map && isNotBlank(response['message'])) {
        throw Exception(response['message']);
      } else {
        for (var item in response) {
          if (item['enabled'] == true &&
              item['id'] != 'wc-appointment-gateway') {
            var methodItem = {
              'id': item['id'],
              'title': item['method_title'],
              'method_title': item['instance_id'],
              'description': item['description']
            };
            list.add(PaymentMethod.fromJson(methodItem));
          }
        }
      }
      return list;
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<PagingResponse<Order>> getMyOrders(
      {User? user, dynamic cursor}) async {
    try {
      var response = await wcApi.getAsync(
          'orders?customer=${user!.id}&per_page=20&page=$cursor&order=desc&orderby=id');
      var list = <Order>[];
      if (response is Map && isNotBlank(response['message'])) {
        throw Exception(response['message']);
      } else {
        for (var item in response) {
          list.add(Order.fromJson(item));
        }
        return PagingResponse(data: list);
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<List<OrderNote>> getOrderNote(
      {String? userId, String? orderId}) async {
    try {
      var response = await wcApi
          .getAsync('orders/$orderId/notes?customer=$userId&per_page=20');
      var list = <OrderNote>[];
      if (response is Map && isNotBlank(response['message'])) {
        throw Exception(response['message']);
      } else {
        for (var item in response) {
//          if (item.type == 'any') {
          /// it is possible to update to `any` note
          /// ref: https://woocommerce.github.io/woocommerce-rest-api-docs/#list-all-order-notes
          list.add(OrderNote.fromJson(item));
//          }
        }
        return list;
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<Order> createOrder(
      {CartModel? cartModel,
      UserModel? user,
      bool? paid,
      String? transactionId}) async {
    try {
      final params = Order().toJson(
        cartModel!,
        user!.user != null ? user.user!.id : null,
        paid,
        createOrder: true,
        addGifts: true,
      );
      log(params.toString());
      if (transactionId != null) {
        params['transaction_id'] = transactionId;
      }

      params.addAll(
        {
          'device_version': deviceVersion,
        },
      );
      final response = await httpPost(
        '$domain/wp-json/api/flutter_order/create'.toUri()!,
        body: convert.jsonEncode(params),
        headers: {
          'User-Cookie': user.user != null ? user.user!.cookie! : '',
          'Content-Type': 'application/json'
        }..addAll(header),
      );
      var body = convert.jsonDecode(response.body);
      if (response.statusCode == 201 && body['message'] == null) {
        if (cartModel.shippingMethod == null &&
            kPaymentConfig['EnableShipping'] &&
            body['shipping_lines'].length > 0) {
          body['shipping_lines'][0]['method_title'] = null;
        }
        return Order.fromJson(body);
      } else {
        throw Exception(body['message']);
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future updateOrder(orderId, {status, token}) async {
    try {
      final response = await httpPut(
          '$domain/wp-json/api/flutter_order/update/$orderId'.toUri()!,
          body: convert.jsonEncode({
            'status': status,
            'device_version': deviceVersion,
          }),
          headers: token != null ? {'User-Cookie': token} : {});

      var body = convert.jsonDecode(response.body);
      if (body['message'] != null) {
        throw Exception(body['message']);
      } else {
        return Order.fromJson(body);
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<Order?> cancelOrder({Order? order, String? userCookie}) async {
    final newOrder = await Services()
        .api
        .updateOrder(order!.id, status: 'cancelled', token: userCookie);
    return newOrder;
  }

  @override
  Future<PagingResponse<Product>> searchProducts({
    name,
    categoryId = '',
    categoryName,
    tag = '',
    attribute = '',
    attributeId = '',
    page,
    lang,
    listingLocation = '',
    userId,
  }) async {
    try {
      var endPoint = 'products?status=publish&page=$page&per_page=$apiPageSize';

      if ((lang?.isNotEmpty ?? false) && kAdvanceConfig['isMultiLanguages']) {
        endPoint += '&lang=$lang';
      }

      if (categoryId != null) {
        endPoint += '&category=$categoryId';
      }

      if (attribute != null) {
        endPoint += '&attribute=$attribute';
      }

      if (attributeId != null) {
        endPoint += '&attribute_term=$attributeId';
      }

      if (tag != null) {
        endPoint += '&tag=$tag';
      }
      if (userId != null) {
        endPoint += '&user_id=$userId';
      }
      var response = await wcApi.getAsync('$endPoint&search=$name');
      if (response is Map && isNotBlank(response['message'])) {
        throw Exception(response['message']);
      } else {
        var list = <Product>[];
        for (var item in response) {
          if (!kAdvanceConfig['hideOutOfStock'] || item['in_stock']) {
            list.add(jsonParser(item));
          }
        }

        /// Search by SKU.
        if (kAdvanceConfig['EnableSkuSearch'] ?? false) {
          var skuList = <Product>[];
          var response = await wcApi.getAsync('$endPoint&sku=$name');
          if (response is List) {
            for (var item in response) {
              if (!kAdvanceConfig['hideOutOfStock'] || item['in_stock']) {
                skuList.add(jsonParser(item));
              }
            }

            if (skuList.isNotEmpty) {
              /// Merge results. Let SKU results on top.
              skuList.addAll(list);
              return PagingResponse(data: skuList);
            }
          }
        }
        return PagingResponse(data: list);
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  /// Auth
  @override
  Future<User?> getUserInfo(cookie) async {
    try {
      var base64Str = EncodeUtils.encodeCookie(cookie);
      final response = await httpGet(
        '$domain/wp-json/api/flutter_user/get_currentuserinfo?token=$base64Str&$isSecure'
            .toUri()!,
        headers: header,
      );
      final body = convert.jsonDecode(response.body);

      if (body['user'] != null) {
        var user = body['user'];
        var myUser = User.fromAuthUser(user, cookie);
        myUser.grade1title = body['grade1title'];
        myUser.registered = user['registered'];
        myUser.grade_1 = int.parse(body['grade_1']);
        myUser.point = user['points'];
        myUser.freeTax =
            int.parse(body['freetax'] == '' ? '0' : body['freetax']);
        var temp = <OfficeType?>[];
        if (body['assign_area'] != null) {
          if (body['assign_area'].toString().contains('[')) {
            temp = (body['assign_area'] as List<dynamic>)
                .map((e) => OfficeType.ty.type(e.toString()))
                .toList();
            myUser.officeType = temp;
          } else {
            myUser.officeType = [
              OfficeType.ty.type(body['assign_area'].toString())
            ];
          }
        }
        myUser.defaultAssignArea =
            OfficeType.ty.type(body['default_assign_area']);

        myUser.bank = body['acctitle'];
        myUser.bankAccount = body['straccno'];
        return myUser;
      } else {
        if (body['message'] != 'Invalid cookie') {
          throw Exception(body['message']);
        }
        return null;

        /// we may handle if Invalid cookie here
      }
    } catch (err) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<Map<String, dynamic>> updateUserInfo(
      Map<String, dynamic> json, String? token) async {
    try {
      final body = convert.jsonEncode({
        ...json,
        'cookie': token,
        'device_version': deviceVersion,
      });

      final response = await httpPost(
          '$domain/wp-json/api/flutter_user/update_user_profile'.toUri()!,
          body: body,
          headers: {'Content-Type': 'application/json'}..addAll(header));
      if (response.statusCode == 200) {
        final data = jsonDecode(response.body);
        data['cookie'] = token;
        return data;
      } else {
        throw Exception('Can not update user infor');
      }
    } catch (err) {
      rethrow;
    }
  }

  /// Create a New User
  @override
  Future<User?> createUser({
    String? firstName,
    String? lastName,
    String? username,
    String? password,
    String? phoneNumber,
    String? identity,
    bool isVendor = false,
  }) async {
    try {
      // var niceName = firstName! + ' ' + lastName!;
      var endpoint =
          '$domain/wp-json/api/flutter_user/sign_up/?insecure=cool&$isSecure'
              .toUri()!;
      if (kAdvanceConfig['EnableNewSMSLogin'] ?? false) {
        endpoint =
            '$domain/wp-json/api/flutter_user/sign_up_2/?insecure=cool&$isSecure'
                .toUri()!;
      }

      final response = await httpPost(endpoint,
          body: convert.jsonEncode({
            // 'user_email': username,
            'user_login': username,
            // 'username': username,
            'user_pass': password,
            // 'email': username,
            // 'user_nicename': niceName,
            // 'display_name': niceName,
            'phone': phoneNumber,
            // 'first_name': firstName,
            // 'last_name': lastName,
            'identity': identity,
            'device_version': deviceVersion,
          }),
          headers: {'Content-Type': 'application/json'});
      var body = convert.jsonDecode(response.body);
      if (response.statusCode == 200 && body['message'] == null) {
        ///儲存cookieName
        var prefs = injector<SharedPreferences>();
        if (body['cookie_name'] != null) {
          await prefs.setString('cookieName', body['cookie_name']);
        }

        var cookie = body['cookie'];
        return await getUserInfo(cookie);
      } else {
        var message = body['message'];
        throw Exception(message ?? 'Can not create the user.');
      }
    } catch (err) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<String?> signDown({
    String? firstName,
    String? lastName,
    String? username,
    String? password,
    String? phoneNumber,
    String? identity,
    bool isVendor = false,
  }) async {
    try {
      // var niceName = firstName! + ' ' + lastName!;
      var endpoint = '$domain/wp-json/api/flutter_user/sign_down'.toUri()!;

      final response = await httpPost(endpoint,
          body: convert.jsonEncode({
            // 'user_email': username,
            'user_login': username,
            // 'username': username,
            'user_pass': password,
            // 'email': username,
            // 'user_nicename': niceName,
            // 'display_name': niceName,
            'phone': phoneNumber,
            // 'first_name': firstName,
            // 'last_name': lastName,
            'identity': identity,
            'device_version': deviceVersion,
          }),
          headers: {'Content-Type': 'application/json'}..addAll(header));
      var body = convert.jsonDecode(response.body);

      if (response.statusCode == 200 &&
          body['status'].toString() == 'success') {
        return null;
      } else {
        var message = body['msg'];
        return message ?? '發生錯誤，請稍後再試';
      }
    } catch (err) {
      return '發生錯誤，請稍後再試 : $err';
    }
  }

  /// login
  @override
  Future<User?> login({username, password}) async {
    var cookieLifeTime = 120960000000;
    try {
      if (kDebugMode) {
        print(convert.jsonEncode({
          'seconds': cookieLifeTime.toString(),
          'username': username,
          'password': password,
        }));
      }
      final response = await httpPost(
        '$domain/wp-json/api/flutter_user/generate_auth_cookie/?insecure=cool&$isSecure'.toUri()!,
        body: convert.jsonEncode({
          'seconds': cookieLifeTime.toString(),
          'username': username,
          'password': password,
          'device_version': deviceVersion,
        }),
        headers: {'Content-Type': 'application/json'},
      );

      final body = convert.jsonDecode(response.body);

      if (response.statusCode == 401) {
        throw Exception(body['message']);
      }
      if (response.statusCode == 200 && isNotBlank(body['cookie'])) {
        ///儲存cookieName
        var prefs = injector<SharedPreferences>();
        if (body['cookie_name'] != null) {
          await prefs.setString('cookieName', body['cookie_name']);
        }
        return await getUserInfo(body['cookie']);
      } else {
        throw Exception(response.statusCode);
      }
    } catch (err, trace) {
      printLog(trace);
      rethrow;
    }
  }

  Future<Stream<Product>> streamProductsLayout({required config}) async {
    try {
      var endPoint = 'products?per_page=$apiPageSize';
      if (config.containsKey('category')) {
        endPoint += "&category=${config["category"]}";
      }
      if (config.containsKey('tag')) {
        endPoint += "&tag=${config["tag"]}";
      }

      var response = await wcApi.getStream(endPoint);

      return response.stream
          .transform(utf8.decoder)
          .transform(json.decoder)
          .expand((data) => (data as List))
          .map(jsonParser);
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<Product?> getProduct(id, {lang, int index = 0}) async {
    try {
      var endpoint = (lang == null || !kAdvanceConfig['isMultiLanguages'])
          ? 'products/$id'
          : 'products/$id?lang=$lang';

      var response = await wcApi.getAsync(endpoint);

      if (response is Map && isNotBlank(response['message'])) {
        printLog('GetProduct Error: ' + response['message']);
        return null;
      }

      return jsonParser(response);
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<Coupons> getCoupons({
    int page = 1,
    String search = '',
    required String userId,
  }) async {
    try {
      var response = await wcApi.getAsync(
        'coupons?page=$page${search.isNotEmpty ? '&search=$search' : ''}&user_login=$userId',
        refreshCache: true,
      );
      return Coupons.getListCoupons(response);
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<Map<String, dynamic>?> getHomeCache(String? lang) async {
    try {
      final data = await wcApi.getAsync('flutter/cache?lang=$lang');
      if (data == null || data is! Map) {
        throw Exception("Can't get home cache");
      }
      if (data['message'] != null) {
        throw Exception(data['message']);
      }
      var config = Map<String, dynamic>.from(data);
      if (config['HorizonLayout'] != null) {
        var horizontalLayout = config['HorizonLayout'] as List;
        List<dynamic>? items = [];
        List<dynamic>? products = [];
        List<Product> list;
        for (var i = 0; i < horizontalLayout.length; i++) {
          if (horizontalLayout[i]['radius'] != null) {
            horizontalLayout[i]['radius'] =
                double.parse("${horizontalLayout[i]["radius"]}");
          }
          if (horizontalLayout[i]['size'] != null) {
            horizontalLayout[i]['size'] =
                double.parse("${horizontalLayout[i]["size"]}");
          }
          if (horizontalLayout[i]['padding'] != null) {
            horizontalLayout[i]['padding'] =
                double.parse("${horizontalLayout[i]["padding"]}");
          }

          products = horizontalLayout[i]['data'] as List?;
          list = [];
          if (products != null && products.isNotEmpty) {
            for (var item in products) {
              var product = jsonParser(item);
              if ((kAdvanceConfig['hideOutOfStock'] ?? false) &&
                  !product.inStock!) {
                /// hideOutOfStock product
                continue;
              }
              if (horizontalLayout[i]['category'] != null &&
                  "${horizontalLayout[i]["category"]}".isNotEmpty) {
                product.categoryId = horizontalLayout[i]['category'].toString();
              }
              list.add(product);
            }
          }
          horizontalLayout[i]['data'] = list;

          items = horizontalLayout[i]['items'] as List?;
          if (items != null && items.isNotEmpty) {
            for (var j = 0; j < items.length; j++) {
              if (items[j]['padding'] != null) {
                items[j]['padding'] = double.parse("${items[j]["padding"]}");
              }

              var listProduct = <Product>[];
              var prods = items[j]['data'] as List?;
              if (prods != null && prods.isNotEmpty) {
                for (var prod in prods) {
                  var product = jsonParser(prod);
                  if ((kAdvanceConfig['hideOutOfStock'] ?? false) &&
                      !product.inStock!) {
                    /// hideOutOfStock product
                    continue;
                  }
                  listProduct.add(product);
                }
              }
              items[j]['data'] = listProduct;
            }
          }
        }

        if (config['VerticalLayout'] != null &&
            config['VerticalLayout']['data'] != null) {
          var products = config['VerticalLayout']['data'] as List?;
          var list = <Product>[];
          if (products != null && products.isNotEmpty) {
            for (var item in products) {
              var product = jsonParser(item);
              if ((kAdvanceConfig['hideOutOfStock'] ?? false) &&
                  !product.inStock!) {
                /// hideOutOfStock product
                continue;
              }
              list.add(product);
            }
          }
          config['VerticalLayout']['data'] = list;
        }
        configCache = config;
        return config;
      }
      return null;
    } catch (e, trace) {
      printLog(trace);
      return null;
    }
  }

  @override
  Future<User?> loginGoogle({String? token}) async {
    const cookieLifeTime = 120960000000;

    try {
      var endPoint =
          '$domain/wp-json/api/flutter_user/google_login/?second=$cookieLifeTime'
                  '&access_token=$token$isSecure'
              .toUri()!;

      var response = await httpGet(endPoint);

      var jsonDecode = convert.jsonDecode(response.body);

      if (jsonDecode['wp_user_id'] == null || jsonDecode['cookie'] == null) {
        throw Exception(jsonDecode['message']);
      }

      return User.fromWooJson(jsonDecode);
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  /// This layout only suitable for the small Categories items
  @override
  Future getCategoryWithCache() async {
    List<Category> getSubCategories(id) {
      return categories.where((o) => o.parent == id).toList();
    }

    bool hasChildren(id) {
      return categories.where((o) => o.parent == id).toList().isNotEmpty;
    }

    List<Category> getParentCategory() {
      return categories.where((item) => item.parent == '0').toList();
    }

    var categoryIds = <String>[];
    var parentCategories = getParentCategory();
    for (var item in parentCategories) {
      if (hasChildren(item.id)) {
        var subCategories = getSubCategories(item.id);
        for (var item in subCategories) {
          categoryIds.add(item.id.toString());
        }
      } else {
        categoryIds.add(item.id.toString());
      }
    }

    return await getCategoryCache(categoryIds);
  }

  Future<Map<String, dynamic>> getCategoryCache(categoryIds) async {
    try {
      final data = await wcApi.getAsync(
          'flutter/category/cache?categoryIds=${List<String>.from(categoryIds).join(",")}');
      if (data != null) {
        for (var i = 0; i < categoryIds.length; i++) {
          var productsJson = data['${categoryIds[i]}'] as List?;
          var list = <Product>[];
          if (productsJson != null && productsJson.isNotEmpty) {
            for (var item in productsJson) {
              var product = jsonParser(item);
              product.categoryId = categoryIds[i];
              list.add(product);
            }
          }
          categoryCache['${categoryIds[i]}'] = list;
        }
      }

      return categoryCache;
    } catch (e, trace) {
      printLog(trace);
      rethrow;
    }
  }

  @override
  Future<List<FilterTag>> getFilterTags() async {
    try {
      var list = <FilterTag>[];
      var endPoint = 'products/tags';
      var response = await wcApi.getAsync(endPoint);

      for (var item in response) {
        list.add(FilterTag.fromJson(item));
      }

      return list;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<String> getCheckoutUrl(
      Map<String, dynamic> params, String? lang) async {
    try {
      var str = convert.jsonEncode(params);
      var bytes = convert.utf8.encode(str);
      var base64Str = convert.base64.encode(bytes);

      final response =
          await httpPost('$domain/wp-json/api/flutter_user/checkout'.toUri()!,
              body: convert.jsonEncode({
                'order': base64Str,
                'device_version': deviceVersion,
              }),
              headers: {'Content-Type': 'application/json'}..addAll(header));
      var body = convert.jsonDecode(response.body);
      if (response.statusCode == 200 && body is String) {
        if (kPaymentConfig['EnableOnePageCheckout'] ||
            kPaymentConfig['NativeOnePageCheckout'] ||
            kPaymentConfig['GuestCheckout'] == true ||
            params['token'] == null) {
          Map<String, dynamic> checkoutPageSlug =
              kPaymentConfig['CheckoutPageSlug'];
          String? slug = checkoutPageSlug[lang!];
          slug ??= checkoutPageSlug.values.toList().first;
          slug = slug!.contains('?') ? slug + '&' : slug + '?';

          return '$domain/${slug}code=$body&mobile=true';
        } else {
          return '$domain/mstore-checkout?code=$body&mobile=true';
        }
      } else {
        var message = body['message'];
        throw Exception(message ?? "Can't save the order to website");
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<String> submitForgotPassword(
      {String? forgotPwLink, Map<String, dynamic>? data}) async {
    try {
      var endpoint = '$domain/wp-json/api/flutter_user/reset-password'.toUri()!;
      var response = await httpPost(endpoint,
          body: data != null
              ? convert.jsonEncode(data
                ..addAll({
                  'device_version': deviceVersion,
                }))
              : {},
          headers: {'Content-Type': 'application/json'}..addAll(header));
      var result = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        return '';
      } else {
        return result['message'];
      }
    } catch (e) {
      return 'Unknown Error: $e';
    }
  }

  @override
  Future<Map<String, dynamic>?> getCurrencyRate() async {
    try {
      final response = await httpCache(
          '$domain/wp-json/api/flutter_user/get_currency_rates'.toUri()!);
      var body = convert.jsonDecode(response.body);
      if (response.statusCode == 200 && body != null && body is Map) {
        var data = Map<String, dynamic>.from(body);
        var currency = <String, dynamic>{};
        for (var key in data.keys) {
          currency[key.toUpperCase()] =
              double.parse("${data[key]['rate'] == 0 ? 1 : data[key]['rate']}");
        }
        return currency;
      } else {
        return null;
      }
    } catch (err) {
      return null;
    }
  }

  @override
  Future getCountries() async {
    try {
      final response = await httpCache(
          '$domain/wp-json/api/flutter_user/get_countries'.toUri()!);
      var body = convert.jsonDecode(response.body);
      return body;
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future getStatesByCountryId(countryId) async {
    try {
      final response = await httpGet(
          '$domain/wp-json/api/flutter_user/get_states?country_code=$countryId'
              .toUri()!);
      var body = convert.jsonDecode(response.body);
      return body;
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<Map<String, dynamic>?>? getCartInfo(String? token, int? orderType ) async {
    try {
      var base64Str = EncodeUtils.encodeCookie(token!);
      final response = await httpGet(
        '$domain/wp-json/api/flutter_woo/cart?token=$base64Str&active=$orderType'.toUri()!,
        headers: header,
      );

      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        return body;
      } else if (body['message'] != null) {
        throw Exception(body['message']);
      }
      return null;
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future syncCartToWebsite(CartModel cartModel, User? user) async {
    try {
      final params = Order().toJson(cartModel, cartModel.user != null ? user!.id : null, false);
      params.addAll({
        'device_version': deviceVersion,
      });
      // log(params.toString());
      final response = await httpPost(
          '$domain/wp-json/api/flutter_woo/cart'.toUri()!,
          body: convert.jsonEncode(params),
          headers: {'Content-Type': 'application/json'}..addAll(header));
      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        return body;
      } else if (body['message'] != null) {
        throw Exception(body['message']);
      }
      return null;
    } catch (err) {
      return null;
    }
  }

  @override
  Future<Map<String, dynamic>> getCustomerInfo(String? id) async {
    try {
      final response = await wcApi.getAsync('customers/$id');
      if (response['message'] != null) {
        throw Exception(response['message']);
      }
      if (response['billing'] != null) {
        final address = Address.fromJson(response);
        final billing = Address.fromJson(response['billing']);
        billing.firstName =
            billing.firstName!.isEmpty ? address.firstName : billing.firstName;
        billing.lastName =
            billing.lastName!.isEmpty ? address.lastName : billing.lastName;
        billing.email = billing.email!.isEmpty ? address.email : billing.email;
        if (billing.country!.isEmpty) {
          billing.country = kPaymentConfig['DefaultCountryISOCode'];
        }
        if (billing.state!.isEmpty) {
          billing.state = kPaymentConfig['DefaultStateISOCode'];
        }
        response['billing'] = billing;
      }
      return response;
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<Map<String, dynamic>?> getTaxes(CartModel cartModel) async {
    try {
      if (isBookingProduct(cartModel)) return null;
      final params = Order().toJson(
          cartModel, cartModel.user != null ? cartModel.user!.id : null, false);

      final response = await httpPost(
          '$domain/wp-json/api/flutter_woo/taxes'.toUri()!,
          body: convert.jsonEncode(params),
          headers: {'Content-Type': 'application/json'}..addAll(header));
      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        var taxes = <Tax>[];
        body['items'].forEach((item) {
          taxes.add(Tax.fromJson(item));
        });
        return {'items': taxes, 'total': body['taxes_total']};
      } else if (body['message'] != null) {
        throw Exception(body['message']);
      }
      return null;
    } catch (err) {
      rethrow;
    }
  }

  Future<Map<String, Tag>> getTagsByPage({String? lang, int? page}) async {
    try {
      var url =
          'products/tags?per_page=100&page=$page&hide_empty=${kAdvanceConfig['HideEmptyTags'] ?? true}';
      if (lang != null && kAdvanceConfig['isMultiLanguages']) {
        url += '&lang=$lang';
      }
      var response = await wcApi.getAsync(url);

      return compute(TagModel.parseTagList, response);
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<Map<String, Tag>> getTags({String? lang}) async {
    try {
      if (tags.isNotEmpty && currentLanguage == lang) {
        return tags;
      }
      currentLanguage = lang;
      var map = <String, Tag>{};
      var isEnd = false;
      var page = 1;

      while (!isEnd) {
        var _tags = await getTagsByPage(lang: lang, page: page);
        if (_tags.isEmpty) {
          isEnd = true;
        }
        page = page + 1;

        map.addAll(_tags);
      }
      tags = map;
      return tags;
    } catch (e, trace) {
      printLog(trace);
      return {};
      //rethrow;
    }
  }

  @override
  Future<bool> pushNotification(cookie,
      {receiverEmail, senderName, message}) async {
    try {
      var base64Str = EncodeUtils.encodeCookie(cookie);
      final endpoint = '$domain/wp-json/api/flutter_user/notification';
      final res = await httpPost(
        endpoint.toUri()!,
        body: {
          'token': base64Str,
          'receiver': receiverEmail,
          'sender': senderName,
          'message': message,
          'device_version': deviceVersion,
        },
        headers: header,
      );
      if (res.statusCode == 200) {
        return true;
      } else {
        throw Exception(res.statusCode);
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<PagingResponse<Order>>? getVendorOrders(
      {required User user, dynamic cursor = 1}) async {
    try {
      var base64Str = EncodeUtils.encodeCookie(user.cookie!);
      final response = await httpGet(
        '$domain/wp-json/wc/v2/flutter/vendor-orders?page=$cursor&per_page=25&token=$base64Str'
            .toUri()!,
        headers: header,
      );

      var body = convert.jsonDecode(response.body);
      var list = <Order>[];
      if (body is Map && isNotBlank(body['message'])) {
        throw Exception(body['message']);
      } else {
        for (var item in body) {
          list.add(Order.fromJson(item));
        }
        return PagingResponse(data: list);
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<Product> createProduct(
      String? cookie, Map<String, dynamic> data) async {
    try {
      final response = await httpPost(
          '$domain/wp-json/api/flutter_multi_vendor/product'.toUri()!,
          body: convert.jsonEncode(data
            ..addAll({
              'device_version': deviceVersion,
            })),
          headers: {'User-Cookie': cookie!, 'Content-Type': 'application/json'}
            ..addAll(header));
      var body = convert.jsonDecode(response.body);
      if (body['message'] == null) {
        return jsonParser(body);
      } else {
        throw Exception(body['message']);
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<Product>> getOwnProducts(String? cookie,
      {int? page, int? perPage}) async {
    try {
      final response = await httpPost(
          '$domain/wp-json/api/flutter_multi_vendor/products/owner'.toUri()!,
          body: convert.jsonEncode({
            'cookie': cookie,
            'page': page,
            'device_version': deviceVersion,
          }),
          headers: {'User-Cookie': cookie!, 'Content-Type': 'application/json'}
            ..addAll(header));
      var body = convert.jsonDecode(response.body);
      if (body is Map && isNotBlank(body['message'])) {
        throw Exception(body['message']);
      } else {
        var list = <Product>[];
        for (var item in body) {
          list.add(jsonParser(item));
        }
        return list;
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<dynamic> uploadImage(dynamic data, String? token) async {
    try {
      final response = await httpPost(
          '$domain/wp-json/api/flutter_multi_vendor/media'.toUri()!,
          body: convert.jsonEncode(data),
          headers: {'User-Cookie': token!, 'Content-Type': 'application/json'}
            ..addAll(header));
      var body = convert.jsonDecode(response.body);
      if (body['message'] == null) {
        return body;
      } else {
        throw Exception(body['message']);
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<Point?> getMyPoint(String? token) async {
    try {
      var base64Str = EncodeUtils.encodeCookie(token!);
      final response = await httpGet(
        '$domain/wp-json/api/flutter_woo/points?token=$base64Str'.toUri()!,
        headers: header,
      );
      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        return Point.fromJson(body);
      } else if (body['message'] != null) {
        throw Exception(body['message']);
      }
      return null;
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future updatePoints(String? token, Order? order) async {
    try {
      final response = await httpPatch(
          '$domain/wp-json/api/flutter_woo/points'.toUri()!,
          body: convert.jsonEncode({'cookie': token, 'order_id': order!.id}));
      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        return body;
      } else if (body['message'] != null) {
        throw Exception(body['message']);
      }
      return null;
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<BookStatus>? bookService({userId, value, message}) => null;

  @override
  Future<List<Product>> getProductNearest(location) async {
    try {
      var list = <Product>[];
      var lat = location.latitude;
      var long = location.longitude;
      var urlReq =
          '$domain/wp-json/wp/v2/${DataMapping().kProductPath}?status=publish&_embed=true';
      if (lat != 0 || long != 0) {
        urlReq += '&isGetLocate=true&lat=$lat&long=$long';
      }
      final response = await httpGet(
        urlReq.toUri()!,
        headers: header,
      );
      if (response.statusCode == 200) {
        for (var item in convert.jsonDecode(response.body)) {
          var product = Product.fromListingJson(item);
          var _gallery = <String>[];
          for (var item in product.images) {
            if (!item.contains('http')) {
              var res = await httpGet(
                '$domain/wp-json/wp/v2/media/$item'.toUri()!,
                headers: header,
              );
              _gallery.add(convert.jsonDecode(res.body)['source_url']);
            } else {
              _gallery.add(item);
            }
          }
          product.images = _gallery;
          list.add(product);
        }
      }
      return list;
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<List<ListingBooking>> getBooking({userId, page, perPage}) {
    throw UnimplementedError();
  }

  @override
  Future<bool> createBooking(dynamic bookingInfo) async {
    if (bookingInfo.isAvaliableOrder && bookingInfo.isEmpty == false) {
      final response = await httpPost(
          '$domain/wp-json/api/flutter_booking/checkout'.toUri()!,
          body: convert.jsonEncode(bookingInfo.toJsonAPI()),
          headers: {'Content-Type': 'application/json'}..addAll(header));

      var body = convert.jsonDecode(response.body);
      if (response.statusCode == 200 && body['appointment'] != null) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  @override
  Future<List<StaffBookingModel>> getListStaff(String? idProduct) async {
    final urlAPI = wcApi.getOAuthURLExternal(
        '$domain/wp-json/api/flutter_booking/get_staffs?product_id=$idProduct');

    final response = await httpCache(urlAPI.toUri()!);

    var body = convert.jsonDecode(response.body);
    if (response.statusCode == 200) {
      final listStaff = <StaffBookingModel>[];
      if (body is List) {
        if (body.isNotEmpty) {
          for (var staff in body) {
            if (staff != null) {
              listStaff.add(StaffBookingModel.fromJson(staff));
            }
          }
        }
      }
      return listStaff;
    } else {
      return [];
    }
  }

  @override
  Future<List<String>> getSlotBooking(
      String? idProduct, String idStaff, String date) async {
    var urlAPI =
        '$domain/wp-json/api/flutter_booking/get_slots?product_id=$idProduct&date=$date';

    if ((idStaff.isNotEmpty) && idStaff != 'null') {
      urlAPI += '&staff_ids=$idStaff';
    }

    final response = await httpGet(
      urlAPI.toUri()!,
      headers: header,
    );
    if (response.body.isNotEmpty) {
      final listSlot = <String>[];
      final result = convert.jsonDecode(response.body);
      if (result is List) {
        for (var item in result) {
          if (item?.isNotEmpty ?? false) {
            listSlot.add('$item');
          }
        }
      }
      return listSlot;
    }
    return <String>[];
  }

  @override
  Future<Map<String, dynamic>>? checkBookingAvailability({data}) => null;

  @override
  Future<List<Store>>? getNearbyStores(Prediction prediction,
          {int page = 1, int perPage = 10, int radius = 10, String? name}) =>
      null;

  @override
  Future<Prediction> getPlaceDetail(
      Prediction prediction, String? sessionToken) async {
    try {
      var endpoint = 'https://maps.googleapis.com/maps/api/place/details/json?'
          'place_id=${prediction.placeId}'
          '&fields=geometry&key=${isIos ? kGoogleAPIKey['ios'] : kGoogleAPIKey['android']}'
          '&sessiontoken=$sessionToken';

      var response = await httpGet(endpoint.toUri()!);
      var result = convert.jsonDecode(response.body);
      var lat = result['result']['geometry']['location']['lat'].toString();
      var long = result['result']['geometry']['location']['lng'].toString();
      prediction.lat = lat;
      prediction.long = long;
    } catch (e) {
      printLog('getPlaceDetail: $e');
    }
    return prediction;
  }

  @override
  Future<List<Prediction>> getAutoCompletePlaces(
      String term, String? sessionToken) async {
    try {
      var endpoint =
          'https://maps.googleapis.com/maps/api/place/autocomplete/json?'
          'input=$term&key=${isIos ? kGoogleAPIKey['ios'] : kGoogleAPIKey['android']}'
          '&sessiontoken=$sessionToken';

      var response = await httpGet(endpoint.toUri()!);
      var result = convert.jsonDecode(response.body);
      var list = <Prediction>[];
      for (var item in result['predictions']) {
        list.add(Prediction.fromJson(item));
      }
      return list;
    } catch (e) {
      printLog('getAutoCompletePlaces: $e');
    }
    return [];
  }

  @override
  Future<List<dynamic>>? getLocations() => null;

  @override
  Future<Product?> getProductByPermalink(String productPermalink) async {
    try {
      final response = await httpGet(
        '$domain/wp-json/api/flutter_woo/products/dynamic?url=$productPermalink'
            .toUri()!,
        headers: header,
      );

      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        return jsonParser(body[0]);
      } else if (body['message'] != null) {
        throw Exception(body['message']);
      }
      return null;
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<Category?> getProductCategoryByPermalink(
      String productCategoryPermalink) async {
    try {
      final response = await httpGet(
        '$domain/wp-json/api/flutter_woo/product-category/dynamic?url=$productCategoryPermalink'
            .toUri()!,
        headers: header,
      );

      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        return Category.fromJson(body[0]);
      } else if (body['message'] != null) {
        throw Exception(body['message']);
      }
      return null;
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<Blog?> getBlogByPermalink(String blogPermaLink) async {
    try {
      final response = await httpGet(
        '$domain/wp-json/api/flutter_woo/blog/dynamic?url=$blogPermaLink'
            .toUri()!,
        headers: header,
      );
      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        return Blog.fromJson(body);
      } else if (body['message'] != null) {
        throw Exception(body['message']);
      }
      return null;
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<List<Brand>?> getBrands() async {
    try {
      var endpoint = 'products/brands?per_page=30';
      var response = await wcApi.getAsync(endpoint);
      var list = <Brand>[];
      if (response is Map && isNotBlank(response['message'])) {
        throw Exception(response['message']);
      } else {
        for (var item in response) {
          list.add(Brand.fromJson(item));
        }
        return list;
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<List<Product>?> fetchProductsByBrand({page, lang, brandId}) async {
    try {
      var endpoint = 'products?brand=$brandId&page=$page&per_page=10';
      var response = await wcApi.getAsync(endpoint);
      var list = <Product>[];
      if (response is Map && isNotBlank(response['message'])) {
        throw Exception(response['message']);
      } else {
        for (var item in response) {
          list.add(Product.fromJson(item));
        }
        return list;
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<List<OrderDeliveryDate>> getListDeliveryDates({storeId}) async {
    var list = <OrderDeliveryDate>[];
    try {
      var endpoint = '$domain/wp-json/api/flutter_woo/ddates';
      if (storeId != null) {
        endpoint += '?id=$storeId';
      }
      var response = await httpGet(
        endpoint.toUri()!,
        headers: header,
      );
      if (response.statusCode == 200 && response.body.isNotEmpty) {
        var result = convert.jsonDecode(response.body);
        for (var item in result) {
          list.add(OrderDeliveryDate.fromJson(item));
        }
      }
    } catch (e, trace) {
      printLog(e);
      printLog(trace);
    }
    return list;
  }

  @override
  Future<bool> isUserExisted({String? phone, String? username}) async {
    try {
      var _endpoint = '$domain/wp-json/api/flutter_user/check-user?';
      if (phone != null) {
        final _phone = phone.replaceAll('+', '').replaceAll(' ', '');
        _endpoint += '&phone=$_phone';
      }
      if (username != null) {
        _endpoint += '&username=$username';
      }
      final response = await httpGet(
        _endpoint.toUri()!,
        headers: header,
      );
      if (response.statusCode == 200) {
        return response.body == 'true';
      }
    } catch (e) {
      printLog('isPhoneNumberExisted $e');
    }
    return false;
  }

  @override
  Future<Category?> getProductCategoryById({required categoryId}) async {
    try {
      var url =
          'products/categories/$categoryId?exclude=$kExcludedCategory&per_page=1&hide_empty=${kAdvanceConfig['HideEmptyCategories'] ?? true}';
      var response = await wcApi.getAsync(url);
      if (response is Map && isNotBlank(response['message'])) {
        throw Exception(response['message']);
      } else {
        return Category.fromJson(response);
      }
    } catch (e) {
      //This error exception is about your Rest API is not config correctly so that not return the correct JSON format, please double check the document from this link https://docs.inspireui.com/fluxstore/woocommerce-setup/
      rethrow;
    }
  }

  @override
  Future<void> deleteProduct(
      {required String? productId, required String? cookie}) async {
    try {
      final response = await httpDelete(
          '$domain/wp-json/api/flutter_multi_vendor/product/$productId'
              .toUri()!,
          headers: {'User-Cookie': cookie!, 'Content-Type': 'application/json'},
          enableDio: true);
      var body = convert.jsonDecode(response.body);
      if (body['message'] == null) {
        return;
      } else {
        throw Exception(body['message']);
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<CalendarEvent>?> getCalendarEvent({
    required int page,
    int perPage = 50,
    required String startDate,
    required String endDate,
    String status = 'publish',
  }) async {
    try {
      final queryParameters = {
        'page': page.toString(),
        'per_page': perPage.toString(),
        'start_date': startDate,
        'end_date': endDate,
        'status': status
      };
      final uri = Uri.https(domain.replaceAll('https://', ''),
          '/wp-json/tribe/events/v1/events', queryParameters);

      final response = await httpGet(
        uri,
        enableDio: true,
        headers: header,
      );
      if (convert.jsonDecode(response.body)['events'] == null) {
        return [];
      }
      List<dynamic> events = convert.jsonDecode(response.body)['events'];
      var result = <CalendarEvent>[];
      for (var element in events) {
        result.add(CalendarEvent.fromJson(Map<String, dynamic>.from(element)));
      }
      return result;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<Map<String, dynamic>?> getSubscribe(String? token) async {
    try {
      final response = await httpGet(
        '$domain/wp-json/event/v1/subscribe?cookie=$token'.toUri()!,
        headers: header,
      );
      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        return body;
      } else if (body['message'] != null) {
        throw Exception(body['message']);
      }
      return null;
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<Map<String, dynamic>?> subscribe(
      String? token, String category) async {
    try {
      final response = await httpPost(
        '$domain/wp-json/event/v1/subscribe/'.toUri()!,
        body: {
          'cookie': token,
          'category': category,
          'device_version': deviceVersion,
        },
        headers: header,
      );
      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        return body;
      } else if (body['message'] != null) {
        throw Exception(body['message']);
      }
      return null;
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<Map<String, dynamic>?> setConfirmSpeed({
    required String userId,
    required int check,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        'wp-json/api/flutter_user/change_confirmspeed_sw',
      );

      final response = await httpPost(
        uri,
        body: {
          'user_login': userId,
          'is_checked': check,
          'device_version': deviceVersion,
        },
        enableDio: true,
        headers: header,
      );

      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        return body;
      } else if (body['message'] != null) {
        throw Exception(body['message']);
      }
      return null;
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<List<BannerItemConfig>?> getBanner() async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/banner/v1',
      );
      final response = await httpGet(
        uri,
        enableDio: true,
        headers: header,
      );
      var data = convert.jsonDecode(response.body);
      var result = <BannerItemConfig>[];
      for (var element in data) {
        result.add(BannerData().fromJson(Map<String, dynamic>.from(element)));
      }
      return result;
    } catch (e) {
      return <BannerItemConfig>[
        BannerItemConfig(image: 'assets/images/default-store-banner2.jpg'),
      ];
    }
  }

  @override
  Future<List<NewOrderHistoryConfig>?> getOrderHistory({
    required String id,
    required String startDate,
    required String endDate,
    required int type,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wc-api/history_order',
        {
          'id': id,
          'strdate': startDate,
          'enddate': endDate,
          'order_type': type.toString(),
        },
      );
      final response = await httpGet(
        uri,
        enableDio: false,
        headers: header,
      );
      var data = convert.jsonDecode(response.body);
      final list = <NewOrderHistoryConfig>[];
      for (var element in data['orderlist']) {
        list.add(NewOrderHistoryConfig.fromJson(element));
      }
      return list;
    } catch (e) {
      return [];
    }
  }

  @override
  Future<OrderResponse?>? getOrderHistoryDetail(
      {required String id, required String username}) async {
    try {
      // final uri = Uri.https(
      //     domain.replaceAll('https://', ''), '/wc-api/history_order_detail', {
      //   'ord_no_area': id,
      // });

      final uri = Uri.https(
          domain.replaceAll('https://', ''), '/wp-json/api/post/getOrderData');
      // final response = await httpGet(uri, enableDio: false);
      final response = await httpPost(
        uri,
        body: {
          'order_id': id,
          'user_login': username,
          'device_version': deviceVersion,
        },
        headers: header,
      );

      var data = convert.jsonDecode(response.body);
      return OrderResponse.fromJson(data);
    } catch (e) {
      return null;
    }
  }

  @override
  Future<bool?> getUserStatus(String userId) async {
    try {
      var request = http.Request(
        'GET',
        Uri.https(domain.replaceAll('https://', ''),
            '/wp-json/api/flutter_user/check_confirmspeed_sw'),
      );
      var params = {
        'user_login': userId,
      };
      request.body = jsonEncode(params);
      var response = await request.send();
      var result = await response.stream.bytesToString();
      return result == 'true';
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<NotificationResponse?>? getNotificationList({
    required String userId,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/wpnotify/v1/logs',
        {'user_id': userId, 'type': '-1'},
      );
      final response = await httpGet(
        uri,
        headers: {'Content-Type': 'application/json'}..addAll(header),
      );
      var body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        return NotificationResponse.fromJson(body);
      } else {
        return NotificationResponse(error: '發生錯誤，請稍後再試');
      }
    } catch (err) {
      return NotificationResponse(error: err.toString());
    }
  }

  @override
  Future<bool>? setNotificationRead({
    required String userId,
    required String notificationId,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/wpnotify/v1/setRead',
      );
      final response = await httpPost(
        uri,
        body: {
          'user_id': userId,
          'notification_id': notificationId,
          'device_version': deviceVersion,
        },
        headers: header,
      );
      var body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        if (body['error'] == false) {
          return true;
        }
        return false;
      } else {
        return false;
      }
    } catch (err) {
      return false;
    }
  }

  @override
  Future<String?>? checkUser({
    required String userId,
    required String phone,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/api/flutter_user/check-user',
      );
      var request = http.Request(
        'GET',
        uri,
      );
      request.body = convert.jsonEncode({
        'username': userId,
        'phone_number': phone,
        'device_version': deviceVersion,
      });
      printLog(request.url);

      final response = await request.send();

      if (response.statusCode == 200) {
        var bodyRaw = await response.stream.bytesToString();
        var body = convert.jsonDecode(bodyRaw);
        return body['user_id'].toString();
      } else if (response.statusCode == 204) {
        return '';
      } else {
        debugPrint('發生錯誤:check-user ${response.statusCode}');
        return null;
      }
    } catch (err) {
      debugPrint('發生錯誤:check-user $err');
      return null;
    }
  }

  @override
  Future<bool>? resetPassword({
    required String userId,
    required String password,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/api/flutter_user/reset-password',
      );
      final response = await httpPost(
        uri,
        headers: {'Content-Type': 'application/json'}..addAll(header),
        body: convert.jsonEncode({
          'user_id': userId,
          'password': password,
          'device_version': deviceVersion,
        }),
      );

      if (response.statusCode == 200) {
        return true;
      } else if (response.statusCode == 204) {
        return false;
      } else {
        return false;
      }
    } catch (err) {
      return false;
    }
  }

  @override
  Future<RemoveCouponResponseModel>? removeCoupon({
    required String userId,
    required String coupon,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/api/flutter_woo/remove_coupon',
      );
      final response = await httpPost(
        uri,
        headers: {'Content-Type': 'application/json'}..addAll(header),
        body: convert.jsonEncode({
          'user_login': userId,
          'coupon_code': coupon,
          'device_version': deviceVersion,
        }),
      );

      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        return RemoveCouponResponseModel(
          code: body['code'].toString(),
          message: body['message'].toString(),
        );
      } else {
        return RemoveCouponResponseModel(
          code: 'fail',
          message: '移除失敗 : ${response.statusCode}',
        );
      }
    } catch (err) {
      return RemoveCouponResponseModel(
        code: 'fail',
        message: '移除失敗 ： $err',
      );
    }
  }

  @override
  Future<PointHistoryResponse?>? getPointHistory({
    required String cookie,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        'wp-json/api/flutter_user/point/history',
        {'cookie': cookie},
      );
      final response = await httpGet(
        uri,
        headers: {'Content-Type': 'application/json'}..addAll(header),
      );
      Map<String, dynamic> body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        return PointHistoryResponse.fromJson(body);
      } else {
        throw Exception('發生錯誤:getPointHistory ${response.statusCode}');
      }
    } catch (err) {
      throw Exception('發生錯誤:$err');
    }
  }

  @override
  Future<PointHistoryResponse?>? getPointExchanged({
    required String cookie,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        'wp-json/api/flutter_user/point/exchanged',
        {'cookie': cookie},
      );
      final response = await httpGet(
        uri,
        headers: {'Content-Type': 'application/json'}..addAll(header),
      );
      Map<String, dynamic> body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        return PointHistoryResponse.fromJson(body);
      } else {
        throw Exception('發生錯誤:getPointHistory ${response.statusCode}');
      }
    } catch (err) {
      throw Exception('發生錯誤:$err');
    }
  }

  @override
  Future<dynamic>? exchangePoint({
    required String cookie,
    required int point,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/api/flutter_user/point/exchange',
      );
      final response = await httpPost(
        uri,
        headers: {'Content-Type': 'application/json'}..addAll(header),
        body: convert.jsonEncode({
          'cookie': cookie,
          'points': point,
          'device_version': deviceVersion,
        }),
      );

      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        return body;
      } else {
        return {'error': true, 'msg': '發生錯誤：${response.statusCode}'};
      }
    } catch (err) {
      return {'error': true, 'msg': '發生錯誤：$err'};
    }
  }

  @override
  Future<String?>? reOrder({
    required String userId,
    required String orderId,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/api/flutter_woo/order_again',
      );
      final response = await httpPost(
        uri,
        headers: {'Content-Type': 'application/json'}..addAll(header),
        body: convert.jsonEncode({
          'user_login': userId,
          'order_id': orderId,
          'device_version': deviceVersion,
        }),
      );

      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        return null;
      } else {
        return '錯誤：' + (body['msg'] ?? '');
      }
    } catch (err) {
      return '錯誤：' + err.toString();
    }
  }

  @override
  Future<List<dynamic>?>? getVersions() async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        'wp-json/version/v1/check',
      );
      final response = await httpGet(
        uri,
        headers: header,
      );
      List<dynamic> body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        return body;
      } else {
        return [];
      }
    } catch (err) {
      return [];
    }
  }

  @override
  Future<GetChangeOrderResponse?>? getChangeOrder({
    required String userId,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/get_change_order',
      );
      final response = await httpPost(
        uri,
        headers: {'Content-Type': 'application/json'}..addAll(header),
        body: convert.jsonEncode({
          'user_login': userId,
          'device_version': deviceVersion,
        }),
      );

      final body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        if (body['error'] == true) {
          return GetChangeOrderResponse(
            error: true,
            msg: body['msg'],
          );
        }
        return GetChangeOrderResponse.fromJson(body);
      } else {
        debugPrint('發生錯誤: ${response.statusCode}');
        return GetChangeOrderResponse(
            error: true, msg: '發生錯誤: ${response.statusCode}');
      }
    } catch (err) {
      debugPrint('發生錯誤:check-user $err');
      return GetChangeOrderResponse(error: true, msg: '發生錯誤:check-user $err');
    }
  }

  @override
  Future<GetBonusDateResponse?>? getBonusDate({
    required String userId,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/get_bonus_list',
      );
      final response = await httpPost(
        uri,
        headers: {'Content-Type': 'application/json'}..addAll(header),
        body: convert.jsonEncode({
          'user_login': userId,
          'device_version': deviceVersion,
        }),
      );

      final body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        if (body['error'] == true) {
          return GetBonusDateResponse(
            error: true,
            msg: body['msg'],
          );
        }
        return GetBonusDateResponse.fromJson(body);
      } else {
        debugPrint('發生錯誤: ${response.statusCode}');
        return GetBonusDateResponse(
            error: true, msg: '發生錯誤: ${response.statusCode}');
      }
    } catch (err) {
      debugPrint('發生錯誤: $err');
      return GetBonusDateResponse(error: true, msg: '發生錯誤: $err');
    }
  }

  @override
  Future<GetBonusDetailResponse?>? getBonusDetail({
    required String userId,
    required String date,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/get_bonus_detail',
      );
      final response = await httpPost(
        uri,
        headers: {'Content-Type': 'application/json'}..addAll(header),
        body: convert.jsonEncode(
          {
            'user_login': userId,
            'range': date,
            'device_version': deviceVersion,
          },
        ),
      );

      final body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        if (body['error'] == true) {
          return GetBonusDetailResponse(
            error: true,
            msg: body['msg'],
          );
        }
        return GetBonusDetailResponse.fromJson(body);
      } else {
        debugPrint('發生錯誤: ${response.statusCode}');
        return GetBonusDetailResponse(
            error: true, msg: '發生錯誤: ${response.statusCode}');
      }
    } catch (err) {
      debugPrint('發生錯誤: $err');
      return GetBonusDetailResponse(error: true, msg: '發生錯誤: $err');
    }
  }

  @override
  Future<GetMonarchPointResponse?>? getMonarchPoint({
    required String userId,
    required String date,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/get_monarch_point',
      );
      final response = await httpPost(
        uri,
        headers: {'Content-Type': 'application/json'}..addAll(header),
        body: convert.jsonEncode(
          {
            'user_login': userId,
            'range': date,
            'device_version': deviceVersion,
          },
        ),
      );

      final body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        if (body['error'] == true) {
          return GetMonarchPointResponse(
            error: true,
            msg: body['msg'],
          );
        }
        return GetMonarchPointResponse.fromJson(body);
      } else {
        debugPrint('發生錯誤: ${response.statusCode}');
        return GetMonarchPointResponse(
            error: true, msg: '發生錯誤: ${response.statusCode}');
      }
    } catch (err) {
      debugPrint('發生錯誤: $err');
      return GetMonarchPointResponse(error: true, msg: '發生錯誤: $err');
    }
  }

  @override
  Future<GetTriumphQueryResponse?>? getTriumphQuery({
    required String userId,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/get_triumph_query',
      );
      final response = await httpPost(
        uri,
        headers: {'Content-Type': 'application/json'}..addAll(header),
        body: convert.jsonEncode(
          {
            'user_login': userId,
            'device_version': deviceVersion,
          },
        ),
      );

      final body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        if (body['error'] == true) {
          return GetTriumphQueryResponse(
            error: true,
            msg: body['msg'],
          );
        }
        return GetTriumphQueryResponse.fromJson(body);
      } else {
        debugPrint('發生錯誤: ${response.statusCode}');
        return GetTriumphQueryResponse(
            error: true, msg: '發生錯誤: ${response.statusCode}');
      }
    } catch (err) {
      debugPrint('發生錯誤: $err');
      return GetTriumphQueryResponse(error: true, msg: '發生錯誤: $err');
    }
  }

  @override
  Future<GetAddressResponse?>? getAddress({
    required String userId,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/get_addresses',
      );
      final response = await httpPost(
        uri,
        headers: {'Content-Type': 'application/json'}..addAll(header),
        body: convert.jsonEncode(
          {
            'user_login': userId,
            'device_version': deviceVersion,
          },
        ),
      );

      final body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        if (body['error'] == true) {
          return GetAddressResponse(
            error: true,
            msg: body['msg'],
          );
        }
        return GetAddressResponse.fromJson(body);
      } else {
        debugPrint('發生錯誤: ${response.statusCode}');
        return GetAddressResponse(
            error: true, msg: '發生錯誤: ${response.statusCode}');
      }
    } catch (err) {
      debugPrint('發生錯誤: $err');
      return GetAddressResponse(error: true, msg: '發生錯誤: $err');
    }
  }

  @override
  Future<String?>? updateAddress({
    required String userId,
    required String action,
    int? addressId,
    Map<String, dynamic>? addressData,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/update_addresses',
      );
      final response = await httpPost(
        uri,
        headers: {'Content-Type': 'application/json'}..addAll(header),
        body: convert.jsonEncode(
          {
            'user_login': userId,
            'action': action,
            'address_id': addressId,
            'address_data': addressData,
            'device_version': deviceVersion,
          },
        ),
      );

      final body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        if (body['error'] == true) {
          final errors = body['errors'] as Map<String, dynamic>;
          String errorText = (body['msg'] ?? '');
          errors.forEach((key, value) {
            errorText += '\n' + value.toString();
          });
          return errorText;
        }
        return null;
      } else {
        debugPrint('發生錯誤: ${response.statusCode}');
        final errors = body['errors'] as Map<String, dynamic>;
        String errorText = (body['msg'] ?? '') + '\n';
        errors.forEach((key, value) {
          errorText += value.toString() + '\n';
        });
        return '發生錯誤:' + errorText;
      }
    } catch (err) {
      debugPrint('發生錯誤: $err');
      return '發生錯誤: $err';
    }
  }

  @override
  Future<List<ArticleCatModel>?>? getChargingCat() async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        'wp-json/wp/v2/charging_cat',
      );
      final response = await httpGet(
        uri,
        headers: header,
      );
      List<dynamic> body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        var list = <ArticleCatModel>[];
        for (var element in body) {
          list.add(
            ArticleCatModel(
              id: element['id'],
              name: element['name'],
              taxonomy: element['taxonomy'],
            ),
          );
        }

        return list;
      } else {
        return null;
      }
    } catch (err) {
      printLog(err);
      return null;
    }
  }

  @override
  Future<List<ArticleCatModel>?>? getChargingTag() async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        'wp-json/wp/v2/charging_tag',
      );
      final response = await httpGet(
        uri,
        headers: header,
      );
      List<dynamic> body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        var list = <ArticleCatModel>[];
        for (var element in body) {
          list.add(
            ArticleCatModel(
              id: element['id'],
              name: element['name'],
              taxonomy: element['taxonomy'],
            ),
          );
        }

        return list;
      } else {
        return null;
      }
    } catch (err) {
      printLog(err);
      return null;
    }
  }

  @override
  Future<GetArticlesResponse?>? getArticles({
    required int id,
    required String taxonomy,
    required int page,
    String? search,
    bool isNews = false,
  }) async {
    try {
      ///取cookie
      final _storage = injector<LocalStorage>();
      await _storage.ready;
      final json = _storage.getItem(kLocalKey['userInfo']!);
      final user = User.fromLocalJson(json);

      ///取cookieName
      var prefs = injector<SharedPreferences>();
      final cookieName = prefs.getString('cookieName');

      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        'wp-json/post/get_list',
        isNews
            ? {
                'post_type': 'post',
                'category_id': 117.toString(),
                'taxonomy': 'category',
                'paged': page.toString(),
              }
            : {
                'post_type': 'charging',
                'category_id': id.toString(),
                'taxonomy': taxonomy,
                'paged': page.toString(),
                if (search != null) 'search': search,
              },
      );
      final response = await httpGet(
        uri,
        headers: {'Cookie': '$cookieName=${user.cookie}'}..addAll(header),
      );
      Map<String, dynamic> body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        return GetArticlesResponse.fromJson(body);
      } else {
        return null;
      }
    } catch (err) {
      printLog(err);
      return null;
    }
  }

  @override
  Future<ArticleContentResponse?>? getArticleContent({
    required int id,
  }) async {
    try {
      ///取cookie
      final _storage = injector<LocalStorage>();
      await _storage.ready;
      final json = _storage.getItem(kLocalKey['userInfo']!);
      final user = User.fromLocalJson(json);

      ///取cookieName
      var prefs = injector<SharedPreferences>();
      final cookieName = prefs.getString('cookieName');
      final uri = Uri.https(
          domain.replaceAll('https://', ''), 'wp-json/post/get_single', {
        'post_id': id.toString(),
      });
      final response = await httpGet(
        uri,
        headers: {'Cookie': '$cookieName=${user.cookie}'},
      );
      Map<String, dynamic> body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        return ArticleContentResponse.fromJson(body);
      } else {
        return null;
      }
    } catch (err) {
      printLog(err);
      return null;
    }
  }

  @override
  Future<GetPerformanceOverviewDateResponse>?
      getPerformanceOverviewDate() async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/get_sale_months',
      );
      final response = await httpPost(
        uri,
        headers: {'Content-Type': 'application/json'}..addAll(header),
      );

      final body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        return GetPerformanceOverviewDateResponse.fromJson(body);
      } else {
        return GetPerformanceOverviewDateResponse.fromJson(body);
      }
    } catch (err) {
      debugPrint(err.toString());
      return GetPerformanceOverviewDateResponse(
        error: true,
        msg: err.toString(),
      );
    }
  }

  @override
  Future<GetPerformanceOverviewContentResponse>? getCurrentPerformance({
    required String userName,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/get_sale_timely_query',
      );
      final response = await httpPost(
        uri,
        body: {
          'user_login': userName,
          'device_version': deviceVersion,
        },
        headers: header,
      );

      final body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        return GetPerformanceOverviewContentResponse.fromJson(body);
      } else {
        return GetPerformanceOverviewContentResponse.fromJson(body);
      }
    } catch (err) {
      debugPrint(err.toString());
      return GetPerformanceOverviewContentResponse(
        error: true,
        msg: err.toString(),
      );
    }
  }

  @override
  Future<GetPerformanceOverviewContentResponse>? getMonthPerformance({
    required String userName,
    required String time,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/get_sale_history_query',
      );
      final response = await httpPost(
        uri,
        body: {
          'user_login': userName,
          'yearmonth': time,
          'device_version': deviceVersion,
        },
        headers: header,
      );

      final body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        return GetPerformanceOverviewContentResponse.fromJson(body);
      } else {
        return GetPerformanceOverviewContentResponse.fromJson(body);
      }
    } catch (err) {
      debugPrint(err.toString());
      return GetPerformanceOverviewContentResponse(
        error: true,
        msg: err.toString(),
      );
    }
  }

  @override
  Future<GetPerformanceOverviewContentResponse>? getOrganizationPerformance({
    required String userName,
    required int? grade,
    required String? id,
    required String? name,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/get_org_timely_query',
      );
      final response = await httpPost(
        uri,
        body: {
          'user_login': userName,
          if (grade != null) 'grade': grade.toString(),
          if (id != null) 's_id': id,
          if (name != null) 's_name': name,
          'device_version': deviceVersion,
        },
        headers: header,
      );

      final body = convert.jsonDecode(response.body);

      if (response.statusCode == 200) {
        return GetPerformanceOverviewContentResponse.fromJson(body);
      } else {
        return GetPerformanceOverviewContentResponse.fromJson(body);
      }
    } catch (err) {
      debugPrint(err.toString());
      return GetPerformanceOverviewContentResponse(
        error: true,
        msg: err.toString(),
      );
    }
  }

  @override
  Future<GetMarketingInfoResponse?>? getMarketingInfo({
    required String userId,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/get_invite_info',
      );
      var request = http.Request(
        'GET',
        uri,
      );

      request.body = convert.jsonEncode({
        'user_login': userId,
        'device_version': deviceVersion,
      });

      request.headers.addAll({'Content-Type': 'application/json'});
      printLog(request.url);

      final response = await request.send();
      var bodyRaw = await response.stream.bytesToString();
      var body = convert.jsonDecode(bodyRaw);
      if (response.statusCode == 200) {
        return GetMarketingInfoResponse.fromJson(body);
      } else if (response.statusCode == 204) {
        return GetMarketingInfoResponse.fromJson(body);
      } else {
        return GetMarketingInfoResponse.fromJson(body);
      }
    } catch (err) {
      debugPrint('發生錯誤:check-user $err');
      return GetMarketingInfoResponse(error: true, msg: '錯誤：$err');
    }
  }

  @override
  Future<Map<String, dynamic>?>? updateMarketingData({
    required String userName,
    required String type,
    required String? inviteText,
    required List<List<GroupProductModel>>? products,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/update_invite_info',
      );
      var groups;
      if (products != null) {
        groups = {
          '1': products[0].map((e) => e.id).toList(),
          '2': products[1].map((e) => e.id).toList(),
          '3': products[2].map((e) => e.id).toList(),
          '4': products[3].map((e) => e.id).toList(),
          '5': products[4].map((e) => e.id).toList(),
        };
      }
      final response = await httpPost(uri,
          body: <String, dynamic>{
            'user_login': userName,
            'update_type': type,
            if (inviteText != null) 'invite_text': inviteText,
            if (groups != null) 'groups': groups,
            'device_version': deviceVersion,
          },
          enableDio: true,
          headers: header);

      final body = convert.jsonDecode(response.body);

      // if (!body['error']) {
      //   return null;
      // }
      return body;
    } catch (err) {
      debugPrint(err.toString());
      return {
        'error': true,
        'msg': err.toString(),
      };
    }
  }

  @override
  Future<Map<String, dynamic>?>? sendServiceForm({
    required int formId,
    required String userName,
    required String phone,
    required String email,
    required String type,
    required String content,
    required String? order,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/add_service_form',
      );
      final response = await httpPost(
        uri,
        body: <String, dynamic>{
          'form_id': formId,
          'user_login': userName,
          'user_phone': phone,
          'user_email': email,
          'consult_type': type,
          'consult_text': content,
          if (order != null) 'order_number': order,
          'device_version': deviceVersion,
        },
        enableDio: true,
        headers: header,
      );

      final body = convert.jsonDecode(response.body);

      if (!body['error']) {
        return null;
      }
      return body;
    } catch (err) {
      debugPrint(err.toString());
      return {
        'error': true,
        'msg': err.toString(),
      };
    }
  }

  @override
  Future<GetServiceRecordResponse?>? getServiceRecord({
    required String userId,
    required String? search,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/get_entry_list',
      );
      var request = http.Request(
        'GET',
        uri,
      );
      request.body = convert.jsonEncode({
        'user_login': userId,
        if (search != null) 'key_words': search,
        'device_version': deviceVersion,
      });

      request.headers.addAll({'Content-Type': 'application/json'});

      final response = await request.send();
      var bodyRaw = await response.stream.bytesToString();
      var body = convert.jsonDecode(bodyRaw);
      if (response.statusCode == 200) {
        return GetServiceRecordResponse.fromJson(body);
      } else if (response.statusCode == 204) {
        return GetServiceRecordResponse.fromJson(body);
      } else {
        return GetServiceRecordResponse.fromJson(body);
      }
    } catch (err) {
      debugPrint('發生錯誤: $err');
      return GetServiceRecordResponse(
        error: true,
      );
    }
  }

  @override
  Future<GetServiceContentResponse?>? getServiceContent({
    required String userId,
    required int entryId,
    required int formId,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/get_entry_info',
      );
      var request = http.Request(
        'GET',
        uri,
      );
      request.body = convert.jsonEncode({
        'user_login': userId,
        'entry_id': entryId,
        'form_id': formId,
        'device_version': deviceVersion,
      });

      request.headers.addAll({'Content-Type': 'application/json'});

      final response = await request.send();
      var bodyRaw = await response.stream.bytesToString();
      var body = convert.jsonDecode(bodyRaw);

      if (response.statusCode == 200) {
        return GetServiceContentResponse.fromJson(body);
      } else if (response.statusCode == 204) {
        return GetServiceContentResponse.fromJson(body);
      } else {
        return GetServiceContentResponse.fromJson(body);
      }
    } catch (err) {
      debugPrint('發生錯誤: $err');
      return GetServiceContentResponse(
        error: true,
        msg: '發生錯誤: $err',
      );
    }
  }

  @override
  Future<Map<String, dynamic>?>? sendServiceMessage({
    required int formId,
    required String userName,
    required int entryId,
    required String content,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/add_entry_reply',
      );
      final response = await httpPost(
        uri,
        body: <String, dynamic>{
          'form_id': formId,
          'user_login': userName,
          'entry_id': entryId,
          'consult_text': content,
          'device_version': deviceVersion,
        },
        enableDio: true,
        headers: header,
      );

      final body = convert.jsonDecode(response.body);

      if (!body['error']) {
        return null;
      }
      return body;
    } catch (err) {
      debugPrint(err.toString());
      return {
        'error': true,
        'msg': err.toString(),
      };
    }
  }

  @override
  Future<DefaultCheckoutResponse?>? defaultCheckout({
    required String userId,
    required String productId,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/api/flutter_woo/default_checkout',
      );
      var request = http.Request(
        'GET',
        uri,
      );
      request.body = convert.jsonEncode({
        'customer_id': userId,
        'product_id': productId,
        'device_version': deviceVersion,
      });

      request.headers.addAll({'Content-Type': 'application/json'});

      final response = await request.send();
      var bodyRaw = await response.stream.bytesToString();
      var body = convert.jsonDecode(bodyRaw);

      if (response.statusCode == 200) {
        return DefaultCheckoutResponse.fromJson(body);
      } else if (response.statusCode == 204) {
        return DefaultCheckoutResponse.fromJson(body);
      } else {
        return DefaultCheckoutResponse.fromJson(body);
      }
    } catch (err) {
      debugPrint('發生錯誤: $err');
      return DefaultCheckoutResponse();
    }
  }

  @override
  Future<SellProductMsgResponse?>? sellProductMsg({
    required String userId,
    required String productId,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/want_to_sale',
      );
      var request = http.Request(
        'GET',
        uri,
      );
      request.body = convert.jsonEncode({
        'user_login': userId,
        'product_id': productId,
        'device_version': deviceVersion,
      });

      request.headers.addAll({'Content-Type': 'application/json'});

      final response = await request.send();
      var bodyRaw = await response.stream.bytesToString();
      var body = convert.jsonDecode(bodyRaw);

      if (response.statusCode == 200) {
        return SellProductMsgResponse.fromJson(body);
      } else if (response.statusCode == 204) {
        return SellProductMsgResponse.fromJson(body);
      } else {
        return SellProductMsgResponse.fromJson(body);
      }
    } catch (err) {
      debugPrint('發生錯誤: $err');
      return SellProductMsgResponse();
    }
  }

  @override
  Future<String?>? getOtp({
    required String phone,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/get_verify_code',
      );
      final md5 = crypto.md5
          .convert(utf8.encode(
              '${DateFormat('yyyyMMdd').format(DateTime.now())}lanfar_sms'))
          .toString();
      var request = http.Request(
        'GET',
        uri,
      );
      request.body = convert.jsonEncode({
        'phone': phone,
        'nonce': md5,
        'device_version': deviceVersion,
      });

      request.headers.addAll({'Content-Type': 'application/json'});
      printLog(request.url);

      final response = await request.send();
      var bodyRaw = await response.stream.bytesToString();
      var body = convert.jsonDecode(bodyRaw);
      if (response.statusCode == 200) {
        return body['msg'];
      }
      {
        return null;
      }
    } catch (err) {
      debugPrint('發生錯誤: $err');
      return '發生錯誤: $err';
    }
  }

  @override
  Future<String?>? verifyOtp({
    required String phone,
    required String code,
  }) async {
    try {
      final uri = Uri.https(
        domain.replaceAll('https://', ''),
        '/wp-json/user/verify_the_code',
      );
      var request = http.Request(
        'GET',
        uri,
      );
      request.body = convert.jsonEncode({
        'phone': phone,
        'code': code,
        'device_version': deviceVersion,
      });
      request.headers.addAll({'Content-Type': 'application/json'});
      printLog(request.url);

      final response = await request.send();
      var bodyRaw = await response.stream.bytesToString();
      var body = convert.jsonDecode(bodyRaw);
      if (response.statusCode == 200) {
        return body['msg'];
      }
      {
        return null;
      }
    } catch (err) {
      debugPrint('發生錯誤: $err');
      return '發生錯誤: $err';
    }
  }
}
