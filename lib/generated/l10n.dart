// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `線上購物`
  String get purchase {
    return Intl.message(
      '線上購物',
      name: 'purchase',
      desc: '',
      args: [],
    );
  }

  /// `業績查詢`
  String get performance {
    return Intl.message(
      '業績查詢',
      name: 'performance',
      desc: '',
      args: [],
    );
  }

  /// `文宣影片`
  String get myVideo {
    return Intl.message(
      '文宣影片',
      name: 'myVideo',
      desc: '',
      args: [],
    );
  }

  /// `行事月曆`
  String get calendar {
    return Intl.message(
      '行事月曆',
      name: 'calendar',
      desc: '',
      args: [],
    );
  }

  /// `常見問題`
  String get help {
    return Intl.message(
      '常見問題',
      name: 'help',
      desc: '',
      args: [],
    );
  }

  /// `聯絡我們`
  String get contactUs {
    return Intl.message(
      '聯絡我們',
      name: 'contactUs',
      desc: '',
      args: [],
    );
  }

  /// `連法官網`
  String get website {
    return Intl.message(
      '連法官網',
      name: 'website',
      desc: '',
      args: [],
    );
  }

  /// `查看全部`
  String get seeAll {
    return Intl.message(
      '查看全部',
      name: 'seeAll',
      desc: '',
      args: [],
    );
  }

  /// `特色產品`
  String get featureProducts {
    return Intl.message(
      '特色產品',
      name: 'featureProducts',
      desc: '',
      args: [],
    );
  }

  /// `合金系列`
  String get bagsCollections {
    return Intl.message(
      '合金系列',
      name: 'bagsCollections',
      desc: '',
      args: [],
    );
  }

  /// `女人系列`
  String get womanCollections {
    return Intl.message(
      '女人系列',
      name: 'womanCollections',
      desc: '',
      args: [],
    );
  }

  /// `男人收藏`
  String get manCollections {
    return Intl.message(
      '男人收藏',
      name: 'manCollections',
      desc: '',
      args: [],
    );
  }

  /// `立即購買`
  String get buyNow {
    return Intl.message(
      '立即購買',
      name: 'buyNow',
      desc: '',
      args: [],
    );
  }

  /// `加入購物車`
  String get products {
    return Intl.message(
      '加入購物車',
      name: 'products',
      desc: '',
      args: [],
    );
  }

  /// `加入購物車`
  String get addToCart {
    return Intl.message(
      '加入購物車',
      name: 'addToCart',
      desc: '',
      args: [],
    );
  }

  /// `產品特色`
  String get description {
    return Intl.message(
      '產品特色',
      name: 'description',
      desc: '',
      args: [],
    );
  }

  /// `評論`
  String get readReviews {
    return Intl.message(
      '評論',
      name: 'readReviews',
      desc: '',
      args: [],
    );
  }

  /// `附加資訊`
  String get additionalInformation {
    return Intl.message(
      '附加資訊',
      name: 'additionalInformation',
      desc: '',
      args: [],
    );
  }

  /// `暫無評論`
  String get noReviews {
    return Intl.message(
      '暫無評論',
      name: 'noReviews',
      desc: '',
      args: [],
    );
  }

  /// `產品已添加`
  String get productAdded {
    return Intl.message(
      '產品已添加',
      name: 'productAdded',
      desc: '',
      args: [],
    );
  }

  /// `你可能還喜歡`
  String get youMightAlsoLike {
    return Intl.message(
      '你可能還喜歡',
      name: 'youMightAlsoLike',
      desc: '',
      args: [],
    );
  }

  /// `選擇尺寸`
  String get selectTheSize {
    return Intl.message(
      '選擇尺寸',
      name: 'selectTheSize',
      desc: '',
      args: [],
    );
  }

  /// `選擇顏色`
  String get selectTheColor {
    return Intl.message(
      '選擇顏色',
      name: 'selectTheColor',
      desc: '',
      args: [],
    );
  }

  /// `選擇數量`
  String get selectTheQuantity {
    return Intl.message(
      '選擇數量',
      name: 'selectTheQuantity',
      desc: '',
      args: [],
    );
  }

  /// `尺寸`
  String get size {
    return Intl.message(
      '尺寸',
      name: 'size',
      desc: '',
      args: [],
    );
  }

  /// `顏色`
  String get color {
    return Intl.message(
      '顏色',
      name: 'color',
      desc: '',
      args: [],
    );
  }

  /// `購物車`
  String get myCart {
    return Intl.message(
      '購物車',
      name: 'myCart',
      desc: '',
      args: [],
    );
  }

  /// `保存到我的收藏`
  String get saveToWishList {
    return Intl.message(
      '保存到我的收藏',
      name: 'saveToWishList',
      desc: '',
      args: [],
    );
  }

  /// `分享`
  String get share {
    return Intl.message(
      '分享',
      name: 'share',
      desc: '',
      args: [],
    );
  }

  /// `取消`
  String get cancel {
    return Intl.message(
      '取消',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `結帳`
  String get checkout {
    return Intl.message(
      '結帳',
      name: 'checkout',
      desc: '',
      args: [],
    );
  }

  /// `清空購物車`
  String get clearCart {
    return Intl.message(
      '清空購物車',
      name: 'clearCart',
      desc: '',
      args: [],
    );
  }

  /// `我的收藏`
  String get myWishList {
    return Intl.message(
      '我的收藏',
      name: 'myWishList',
      desc: '',
      args: [],
    );
  }

  /// `您的購物車是空的`
  String get yourBagIsEmpty {
    return Intl.message(
      '您的購物車是空的',
      name: 'yourBagIsEmpty',
      desc: '',
      args: [],
    );
  }

  /// `您似乎還沒有在購物車中添加任何商品。開始購物以填充它。`
  String get emptyCartSubtitle {
    return Intl.message(
      '您似乎還沒有在購物車中添加任何商品。開始購物以填充它。',
      name: 'emptyCartSubtitle',
      desc: '',
      args: [],
    );
  }

  /// `開始購物`
  String get startShopping {
    return Intl.message(
      '開始購物',
      name: 'startShopping',
      desc: '',
      args: [],
    );
  }

  /// `尚無收藏。`
  String get noFavoritesYet {
    return Intl.message(
      '尚無收藏。',
      name: 'noFavoritesYet',
      desc: '',
      args: [],
    );
  }

  /// `輕按產品旁邊的任何一個即可收藏。我們將在這裡為您保存！`
  String get emptyWishlistSubtitle {
    return Intl.message(
      '輕按產品旁邊的任何一個即可收藏。我們將在這裡為您保存！',
      name: 'emptyWishlistSubtitle',
      desc: '',
      args: [],
    );
  }

  /// `搜索項目`
  String get searchForItems {
    return Intl.message(
      '搜索項目',
      name: 'searchForItems',
      desc: '',
      args: [],
    );
  }

  /// `寄送`
  String get shipping {
    return Intl.message(
      '寄送',
      name: 'shipping',
      desc: '',
      args: [],
    );
  }

  /// `訂單`
  String get review {
    return Intl.message(
      '訂單',
      name: 'review',
      desc: '',
      args: [],
    );
  }

  /// `付款`
  String get payment {
    return Intl.message(
      '付款',
      name: 'payment',
      desc: '',
      args: [],
    );
  }

  /// `姓名`
  String get firstName {
    return Intl.message(
      '姓名',
      name: 'firstName',
      desc: '',
      args: [],
    );
  }

  /// `姓`
  String get lastName {
    return Intl.message(
      '姓',
      name: 'lastName',
      desc: '',
      args: [],
    );
  }

  /// `鄉鎮市`
  String get city {
    return Intl.message(
      '鄉鎮市',
      name: 'city',
      desc: '',
      args: [],
    );
  }

  /// `縣市`
  String get stateProvince {
    return Intl.message(
      '縣市',
      name: 'stateProvince',
      desc: '',
      args: [],
    );
  }

  /// `郵遞區號`
  String get zipCode {
    return Intl.message(
      '郵遞區號',
      name: 'zipCode',
      desc: '',
      args: [],
    );
  }

  /// `國家`
  String get country {
    return Intl.message(
      '國家',
      name: 'country',
      desc: '',
      args: [],
    );
  }

  /// `電話號碼`
  String get phoneNumber {
    return Intl.message(
      '電話號碼',
      name: 'phoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `電子郵件`
  String get email {
    return Intl.message(
      '電子郵件',
      name: 'email',
      desc: '',
      args: [],
    );
  }

  /// `街道/巷弄/門號/樓層`
  String get streetName {
    return Intl.message(
      '街道/巷弄/門號/樓層',
      name: 'streetName',
      desc: '',
      args: [],
    );
  }

  /// `寄送方式`
  String get shippingMethod {
    return Intl.message(
      '寄送方式',
      name: 'shippingMethod',
      desc: '',
      args: [],
    );
  }

  /// `下一步`
  String get continueToShipping {
    return Intl.message(
      '下一步',
      name: 'continueToShipping',
      desc: '',
      args: [],
    );
  }

  /// `下一步`
  String get continueToReview {
    return Intl.message(
      '下一步',
      name: 'continueToReview',
      desc: '',
      args: [],
    );
  }

  /// `下一步`
  String get continueToPayment {
    return Intl.message(
      '下一步',
      name: 'continueToPayment',
      desc: '',
      args: [],
    );
  }

  /// `回到地址`
  String get goBackToAddress {
    return Intl.message(
      '回到地址',
      name: 'goBackToAddress',
      desc: '',
      args: [],
    );
  }

  /// `回到寄送`
  String get goBackToShipping {
    return Intl.message(
      '回到寄送',
      name: 'goBackToShipping',
      desc: '',
      args: [],
    );
  }

  /// `回到訂單`
  String get goBackToReview {
    return Intl.message(
      '回到訂單',
      name: 'goBackToReview',
      desc: '',
      args: [],
    );
  }

  /// `地址`
  String get address {
    return Intl.message(
      '地址',
      name: 'address',
      desc: '',
      args: [],
    );
  }

  /// `送貨地址`
  String get shippingAddress {
    return Intl.message(
      '送貨地址',
      name: 'shippingAddress',
      desc: '',
      args: [],
    );
  }

  /// `訂單詳細訊息`
  String get orderDetail {
    return Intl.message(
      '訂單詳細訊息',
      name: 'orderDetail',
      desc: '',
      args: [],
    );
  }

  /// `商品總金額`
  String get subtotal {
    return Intl.message(
      '商品總金額',
      name: 'subtotal',
      desc: '',
      args: [],
    );
  }

  /// `總共`
  String get total {
    return Intl.message(
      '總共',
      name: 'total',
      desc: '',
      args: [],
    );
  }

  /// `支付方式`
  String get paymentMethods {
    return Intl.message(
      '支付方式',
      name: 'paymentMethods',
      desc: '',
      args: [],
    );
  }

  /// `選擇你的支付方法`
  String get chooseYourPaymentMethod {
    return Intl.message(
      '選擇你的支付方法',
      name: 'chooseYourPaymentMethod',
      desc: '',
      args: [],
    );
  }

  /// `下訂單`
  String get placeMyOrder {
    return Intl.message(
      '下訂單',
      name: 'placeMyOrder',
      desc: '',
      args: [],
    );
  }

  /// `已訂購！`
  String get itsOrdered {
    return Intl.message(
      '已訂購！',
      name: 'itsOrdered',
      desc: '',
      args: [],
    );
  }

  /// `訂單號。`
  String get orderNo {
    return Intl.message(
      '訂單號。',
      name: 'orderNo',
      desc: '',
      args: [],
    );
  }

  /// `顯示我所有的訂單`
  String get showAllMyOrdered {
    return Intl.message(
      '顯示我所有的訂單',
      name: 'showAllMyOrdered',
      desc: '',
      args: [],
    );
  }

  /// `回到商店`
  String get backToShop {
    return Intl.message(
      '回到商店',
      name: 'backToShop',
      desc: '',
      args: [],
    );
  }

  /// `名字欄位是必填項`
  String get firstNameIsRequired {
    return Intl.message(
      '名字欄位是必填項',
      name: 'firstNameIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `姓氏欄位為必填項`
  String get lastNameIsRequired {
    return Intl.message(
      '姓氏欄位為必填項',
      name: 'lastNameIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `街道名稱欄位為必填項`
  String get streetIsRequired {
    return Intl.message(
      '街道名稱欄位為必填項',
      name: 'streetIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `必須填寫城市欄位`
  String get cityIsRequired {
    return Intl.message(
      '必須填寫城市欄位',
      name: 'cityIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `狀態欄位為必填項`
  String get stateIsRequired {
    return Intl.message(
      '狀態欄位為必填項',
      name: 'stateIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `國家欄位為必填項`
  String get countryIsRequired {
    return Intl.message(
      '國家欄位為必填項',
      name: 'countryIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `電話號碼欄位為必填項`
  String get phoneIsRequired {
    return Intl.message(
      '電話號碼欄位為必填項',
      name: 'phoneIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `電子郵件欄位為必填項`
  String get emailIsRequired {
    return Intl.message(
      '電子郵件欄位為必填項',
      name: 'emailIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `郵政欄位為必填欄位`
  String get zipCodeIsRequired {
    return Intl.message(
      '郵政欄位為必填欄位',
      name: 'zipCodeIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `沒有訂單`
  String get noOrders {
    return Intl.message(
      '沒有訂單',
      name: 'noOrders',
      desc: '',
      args: [],
    );
  }

  /// `訂購日期`
  String get orderDate {
    return Intl.message(
      '訂購日期',
      name: 'orderDate',
      desc: '',
      args: [],
    );
  }

  /// `狀態`
  String get status {
    return Intl.message(
      '狀態',
      name: 'status',
      desc: '',
      args: [],
    );
  }

  /// `付款方法`
  String get paymentMethod {
    return Intl.message(
      '付款方法',
      name: 'paymentMethod',
      desc: '',
      args: [],
    );
  }

  /// `訂單歷史`
  String get orderHistory {
    return Intl.message(
      '訂單歷史',
      name: 'orderHistory',
      desc: '',
      args: [],
    );
  }

  /// `退款要求`
  String get refundRequest {
    return Intl.message(
      '退款要求',
      name: 'refundRequest',
      desc: '',
      args: [],
    );
  }

  /// `最近的搜索`
  String get recentSearches {
    return Intl.message(
      '最近的搜索',
      name: 'recentSearches',
      desc: '',
      args: [],
    );
  }

  /// `最近`
  String get recents {
    return Intl.message(
      '最近',
      name: 'recents',
      desc: '',
      args: [],
    );
  }

  /// `按價格`
  String get byPrice {
    return Intl.message(
      '按價格',
      name: 'byPrice',
      desc: '',
      args: [],
    );
  }

  /// `按類別`
  String get byCategory {
    return Intl.message(
      '按類別',
      name: 'byCategory',
      desc: '',
      args: [],
    );
  }

  /// `英語`
  String get english {
    return Intl.message(
      '英語',
      name: 'english',
      desc: '',
      args: [],
    );
  }

  /// `越南`
  String get vietnamese {
    return Intl.message(
      '越南',
      name: 'vietnamese',
      desc: '',
      args: [],
    );
  }

  /// `阿拉伯`
  String get arabic {
    return Intl.message(
      '阿拉伯',
      name: 'arabic',
      desc: '',
      args: [],
    );
  }

  /// `沒有網路連接`
  String get noInternetConnection {
    return Intl.message(
      '沒有網路連接',
      name: 'noInternetConnection',
      desc: '',
      args: [],
    );
  }

  /// `會員中心`
  String get settings {
    return Intl.message(
      '會員中心',
      name: 'settings',
      desc: '',
      args: [],
    );
  }

  /// `設定`
  String get generalSetting {
    return Intl.message(
      '設定',
      name: 'generalSetting',
      desc: '',
      args: [],
    );
  }

  /// `提醒通知設定`
  String get getNotification {
    return Intl.message(
      '提醒通知設定',
      name: 'getNotification',
      desc: '',
      args: [],
    );
  }

  /// `通知消息`
  String get listMessages {
    return Intl.message(
      '通知消息',
      name: 'listMessages',
      desc: '',
      args: [],
    );
  }

  /// `語言`
  String get language {
    return Intl.message(
      '語言',
      name: 'language',
      desc: '',
      args: [],
    );
  }

  /// `深色主題`
  String get darkTheme {
    return Intl.message(
      '深色主題',
      name: 'darkTheme',
      desc: '',
      args: [],
    );
  }

  /// `評價應用程式`
  String get rateTheApp {
    return Intl.message(
      '評價應用程式',
      name: 'rateTheApp',
      desc: '',
      args: [],
    );
  }

  /// `登出`
  String get logout {
    return Intl.message(
      '登出',
      name: 'logout',
      desc: '',
      args: [],
    );
  }

  /// `登入`
  String get login {
    return Intl.message(
      '登入',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `項目`
  String get items {
    return Intl.message(
      '項目',
      name: 'items',
      desc: '',
      args: [],
    );
  }

  /// `購物車`
  String get cart {
    return Intl.message(
      '購物車',
      name: 'cart',
      desc: '',
      args: [],
    );
  }

  /// `店`
  String get shop {
    return Intl.message(
      '店',
      name: 'shop',
      desc: '',
      args: [],
    );
  }

  /// `搜索`
  String get search {
    return Intl.message(
      '搜索',
      name: 'search',
      desc: '',
      args: [],
    );
  }

  /// `最新活動`
  String get blog {
    return Intl.message(
      '最新活動',
      name: 'blog',
      desc: '',
      args: [],
    );
  }

  /// `使用`
  String get apply {
    return Intl.message(
      '使用',
      name: 'apply',
      desc: '',
      args: [],
    );
  }

  /// `重設`
  String get reset {
    return Intl.message(
      '重設',
      name: 'reset',
      desc: '',
      args: [],
    );
  }

  /// `登入`
  String get signInWithEmail {
    return Intl.message(
      '登入',
      name: 'signInWithEmail',
      desc: '',
      args: [],
    );
  }

  /// `還沒有編號？`
  String get dontHaveAccount {
    return Intl.message(
      '還沒有編號？',
      name: 'dontHaveAccount',
      desc: '',
      args: [],
    );
  }

  /// `註冊`
  String get signup {
    return Intl.message(
      '註冊',
      name: 'signup',
      desc: '',
      args: [],
    );
  }

  /// `歡迎`
  String get welcome {
    return Intl.message(
      '歡迎',
      name: 'welcome',
      desc: '',
      args: [],
    );
  }

  /// `關閉`
  String get close {
    return Intl.message(
      '關閉',
      name: 'close',
      desc: '',
      args: [],
    );
  }

  /// `請填寫所有欄位`
  String get pleaseInput {
    return Intl.message(
      '請填寫所有欄位',
      name: 'pleaseInput',
      desc: '',
      args: [],
    );
  }

  /// `搜索地址`
  String get searchingAddress {
    return Intl.message(
      '搜索地址',
      name: 'searchingAddress',
      desc: '',
      args: [],
    );
  }

  /// `缺貨`
  String get outOfStock {
    return Intl.message(
      '缺貨',
      name: 'outOfStock',
      desc: '',
      args: [],
    );
  }

  /// `不可用`
  String get unavailable {
    return Intl.message(
      '不可用',
      name: 'unavailable',
      desc: '',
      args: [],
    );
  }

  /// `類別`
  String get category {
    return Intl.message(
      '類別',
      name: 'category',
      desc: '',
      args: [],
    );
  }

  /// `沒有產品`
  String get noProduct {
    return Intl.message(
      '沒有產品',
      name: 'noProduct',
      desc: '',
      args: [],
    );
  }

  /// `我們找到 {length} 個產品`
  String weFoundProducts(Object length) {
    return Intl.message(
      '我們找到 $length 個產品',
      name: 'weFoundProducts',
      desc: '',
      args: [length],
    );
  }

  /// `確定`
  String get clear {
    return Intl.message(
      '確定',
      name: 'clear',
      desc: '',
      args: [],
    );
  }

  /// `視頻`
  String get video {
    return Intl.message(
      '視頻',
      name: 'video',
      desc: '',
      args: [],
    );
  }

  /// `您最近的流覽`
  String get recentView {
    return Intl.message(
      '您最近的流覽',
      name: 'recentView',
      desc: '',
      args: [],
    );
  }

  /// `有現貨`
  String get inStock {
    return Intl.message(
      '有現貨',
      name: 'inStock',
      desc: '',
      args: [],
    );
  }

  /// `跟蹤號是`
  String get trackingNumberIs {
    return Intl.message(
      '跟蹤號是',
      name: 'trackingNumberIs',
      desc: '',
      args: [],
    );
  }

  /// `可用性`
  String get availability {
    return Intl.message(
      '可用性',
      name: 'availability',
      desc: '',
      args: [],
    );
  }

  /// `跟蹤頁面`
  String get trackingPage {
    return Intl.message(
      '跟蹤頁面',
      name: 'trackingPage',
      desc: '',
      args: [],
    );
  }

  /// `我的觀點`
  String get myPoints {
    return Intl.message(
      '我的觀點',
      name: 'myPoints',
      desc: '',
      args: [],
    );
  }

  /// `你有 $point 分`
  String get youHavePoints {
    return Intl.message(
      '你有 \$point 分',
      name: 'youHavePoints',
      desc: '',
      args: [],
    );
  }

  /// `活動`
  String get events {
    return Intl.message(
      '活動',
      name: 'events',
      desc: '',
      args: [],
    );
  }

  /// `日期`
  String get date {
    return Intl.message(
      '日期',
      name: 'date',
      desc: '',
      args: [],
    );
  }

  /// `點`
  String get point {
    return Intl.message(
      '點',
      name: 'point',
      desc: '',
      args: [],
    );
  }

  /// `訂購須知`
  String get orderNotes {
    return Intl.message(
      '訂購須知',
      name: 'orderNotes',
      desc: '',
      args: [],
    );
  }

  /// `請先評分，然後再發送評論`
  String get ratingFirst {
    return Intl.message(
      '請先評分，然後再發送評論',
      name: 'ratingFirst',
      desc: '',
      args: [],
    );
  }

  /// `請寫下您的評論`
  String get commentFirst {
    return Intl.message(
      '請寫下您的評論',
      name: 'commentFirst',
      desc: '',
      args: [],
    );
  }

  /// `書寫您的評論`
  String get writeComment {
    return Intl.message(
      '書寫您的評論',
      name: 'writeComment',
      desc: '',
      args: [],
    );
  }

  /// `正在載入...`
  String get loading {
    return Intl.message(
      '正在載入...',
      name: 'loading',
      desc: '',
      args: [],
    );
  }

  /// `你的評分`
  String get productRating {
    return Intl.message(
      '你的評分',
      name: 'productRating',
      desc: '',
      args: [],
    );
  }

  /// `佈局`
  String get layout {
    return Intl.message(
      '佈局',
      name: 'layout',
      desc: '',
      args: [],
    );
  }

  /// `選擇地址`
  String get selectAddress {
    return Intl.message(
      '選擇地址',
      name: 'selectAddress',
      desc: '',
      args: [],
    );
  }

  /// `保存地址`
  String get saveAddress {
    return Intl.message(
      '保存地址',
      name: 'saveAddress',
      desc: '',
      args: [],
    );
  }

  /// `請在搜索框中輸入內容`
  String get searchInput {
    return Intl.message(
      '請在搜索框中輸入內容',
      name: 'searchInput',
      desc: '',
      args: [],
    );
  }

  /// `總稅金`
  String get totalTax {
    return Intl.message(
      '總稅金',
      name: 'totalTax',
      desc: '',
      args: [],
    );
  }

  /// `無效的短信驗證碼`
  String get invalidSMSCode {
    return Intl.message(
      '無效的短信驗證碼',
      name: 'invalidSMSCode',
      desc: '',
      args: [],
    );
  }

  /// `獲取代碼`
  String get sendSMSCode {
    return Intl.message(
      '獲取代碼',
      name: 'sendSMSCode',
      desc: '',
      args: [],
    );
  }

  /// `校驗`
  String get verifySMSCode {
    return Intl.message(
      '校驗',
      name: 'verifySMSCode',
      desc: '',
      args: [],
    );
  }

  /// `相簿`
  String get showGallery {
    return Intl.message(
      '相簿',
      name: 'showGallery',
      desc: '',
      args: [],
    );
  }

  /// `折扣`
  String get discount {
    return Intl.message(
      '折扣',
      name: 'discount',
      desc: '',
      args: [],
    );
  }

  /// `用戶名`
  String get username {
    return Intl.message(
      '用戶名',
      name: 'username',
      desc: '',
      args: [],
    );
  }

  /// `密碼`
  String get password {
    return Intl.message(
      '密碼',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `請輸入你的電子遊戲`
  String get enterYourEmail {
    return Intl.message(
      '請輸入你的電子遊戲',
      name: 'enterYourEmail',
      desc: '',
      args: [],
    );
  }

  /// `輸入密碼`
  String get enterYourPassword {
    return Intl.message(
      '輸入密碼',
      name: 'enterYourPassword',
      desc: '',
      args: [],
    );
  }

  /// `我想創建一個帳戶`
  String get iwantToCreateAccount {
    return Intl.message(
      '我想創建一個帳戶',
      name: 'iwantToCreateAccount',
      desc: '',
      args: [],
    );
  }

  /// `登陸到您的帳戶`
  String get loginToYourAccount {
    return Intl.message(
      '登陸到您的帳戶',
      name: 'loginToYourAccount',
      desc: '',
      args: [],
    );
  }

  /// `創建一個帳戶`
  String get createAnAccount {
    return Intl.message(
      '創建一個帳戶',
      name: 'createAnAccount',
      desc: '',
      args: [],
    );
  }

  /// `或`
  String get or {
    return Intl.message(
      '或',
      name: 'or',
      desc: '',
      args: [],
    );
  }

  /// `優惠券代碼`
  String get couponCode {
    return Intl.message(
      '優惠券代碼',
      name: 'couponCode',
      desc: '',
      args: [],
    );
  }

  /// `移除`
  String get remove {
    return Intl.message(
      '移除',
      name: 'remove',
      desc: '',
      args: [],
    );
  }

  /// `恭喜你！優惠券代碼已成功應用`
  String get couponMsgSuccess {
    return Intl.message(
      '恭喜你！優惠券代碼已成功應用',
      name: 'couponMsgSuccess',
      desc: '',
      args: [],
    );
  }

  /// `您的地址存在於您的本地`
  String get saveAddressSuccess {
    return Intl.message(
      '您的地址存在於您的本地',
      name: 'saveAddressSuccess',
      desc: '',
      args: [],
    );
  }

  /// `備註`
  String get yourNote {
    return Intl.message(
      '備註',
      name: 'yourNote',
      desc: '',
      args: [],
    );
  }

  /// `請留下商品相關內容`
  String get writeYourNote {
    return Intl.message(
      '請留下商品相關內容',
      name: 'writeYourNote',
      desc: '',
      args: [],
    );
  }

  /// `您已成功下訂單`
  String get orderSuccessTitle1 {
    return Intl.message(
      '您已成功下訂單',
      name: 'orderSuccessTitle1',
      desc: '',
      args: [],
    );
  }

  /// `您可以使用我們的交貨狀態功能檢查訂單狀態。您將收到訂單確認電子郵件，其中包含您的訂單詳細資訊以及跟蹤其進度的連結。`
  String get orderSuccessMsg1 {
    return Intl.message(
      '您可以使用我們的交貨狀態功能檢查訂單狀態。您將收到訂單確認電子郵件，其中包含您的訂單詳細資訊以及跟蹤其進度的連結。',
      name: 'orderSuccessMsg1',
      desc: '',
      args: [],
    );
  }

  /// `您的帳戶`
  String get orderSuccessTitle2 {
    return Intl.message(
      '您的帳戶',
      name: 'orderSuccessTitle2',
      desc: '',
      args: [],
    );
  }

  /// `您可以使用之前定義的電子郵件和密碼登錄到您的帳戶。在您的帳戶上，您可以編輯個人資料資料，檢查交易歷史記錄，編輯對新聞通訊的訂閱。`
  String get orderSuccessMsg2 {
    return Intl.message(
      '您可以使用之前定義的電子郵件和密碼登錄到您的帳戶。在您的帳戶上，您可以編輯個人資料資料，檢查交易歷史記錄，編輯對新聞通訊的訂閱。',
      name: 'orderSuccessMsg2',
      desc: '',
      args: [],
    );
  }

  /// `登入`
  String get signIn {
    return Intl.message(
      '登入',
      name: 'signIn',
      desc: '',
      args: [],
    );
  }

  /// `註冊`
  String get signUp {
    return Intl.message(
      '註冊',
      name: 'signUp',
      desc: '',
      args: [],
    );
  }

  /// `下一頁`
  String get next {
    return Intl.message(
      '下一頁',
      name: 'next',
      desc: '',
      args: [],
    );
  }

  /// `完成`
  String get done {
    return Intl.message(
      '完成',
      name: 'done',
      desc: '',
      args: [],
    );
  }

  /// `貨幣`
  String get currencies {
    return Intl.message(
      '貨幣',
      name: 'currencies',
      desc: '',
      args: [],
    );
  }

  /// `出售{percent}%`
  String sale(Object percent) {
    return Intl.message(
      '出售$percent%',
      name: 'sale',
      desc: '',
      args: [percent],
    );
  }

  /// `更新設定檔`
  String get updateUserInfor {
    return Intl.message(
      '更新設定檔',
      name: 'updateUserInfor',
      desc: '',
      args: [],
    );
  }

  /// `更新`
  String get update {
    return Intl.message(
      '更新',
      name: 'update',
      desc: '',
      args: [],
    );
  }

  /// `關於我們`
  String get aboutUs {
    return Intl.message(
      '關於我們',
      name: 'aboutUs',
      desc: '',
      args: [],
    );
  }

  /// `顯示名稱`
  String get displayName {
    return Intl.message(
      '顯示名稱',
      name: 'displayName',
      desc: '',
      args: [],
    );
  }

  /// `好名字`
  String get niceName {
    return Intl.message(
      '好名字',
      name: 'niceName',
      desc: '',
      args: [],
    );
  }

  /// `西班牙文`
  String get spanish {
    return Intl.message(
      '西班牙文',
      name: 'spanish',
      desc: '',
      args: [],
    );
  }

  /// `中文`
  String get chinese {
    return Intl.message(
      '中文',
      name: 'chinese',
      desc: '',
      args: [],
    );
  }

  /// `繁體中文`
  String get traditionalChinese {
    return Intl.message(
      '繁體中文',
      name: 'traditionalChinese',
      desc: '',
      args: [],
    );
  }

  /// `日本`
  String get japanese {
    return Intl.message(
      '日本',
      name: 'japanese',
      desc: '',
      args: [],
    );
  }

  /// `語言已成功更新`
  String get languageSuccess {
    return Intl.message(
      '語言已成功更新',
      name: 'languageSuccess',
      desc: '',
      args: [],
    );
  }

  /// `隱私權政策`
  String get agreeWithPrivacy {
    return Intl.message(
      '隱私權政策',
      name: 'agreeWithPrivacy',
      desc: '',
      args: [],
    );
  }

  /// `隱私權政策`
  String get PrivacyAndTerm {
    return Intl.message(
      '隱私權政策',
      name: 'PrivacyAndTerm',
      desc: '',
      args: [],
    );
  }

  /// `我同意`
  String get iAgree {
    return Intl.message(
      '我同意',
      name: 'iAgree',
      desc: '',
      args: [],
    );
  }

  /// `當前密碼`
  String get currentPassword {
    return Intl.message(
      '當前密碼',
      name: 'currentPassword',
      desc: '',
      args: [],
    );
  }

  /// `新密碼`
  String get newPassword {
    return Intl.message(
      '新密碼',
      name: 'newPassword',
      desc: '',
      args: [],
    );
  }

  /// `已添加到您的購物車`
  String get addToCartSucessfully {
    return Intl.message(
      '已添加到您的購物車',
      name: 'addToCartSucessfully',
      desc: '',
      args: [],
    );
  }

  /// `拉動載入更多`
  String get pullToLoadMore {
    return Intl.message(
      '拉動載入更多',
      name: 'pullToLoadMore',
      desc: '',
      args: [],
    );
  }

  /// `載入失敗！請重試！`
  String get loadFail {
    return Intl.message(
      '載入失敗！請重試！',
      name: 'loadFail',
      desc: '',
      args: [],
    );
  }

  /// `釋放以載入更多`
  String get releaseToLoadMore {
    return Intl.message(
      '釋放以載入更多',
      name: 'releaseToLoadMore',
      desc: '',
      args: [],
    );
  }

  /// `沒有更多資料`
  String get noData {
    return Intl.message(
      '沒有更多資料',
      name: 'noData',
      desc: '',
      args: [],
    );
  }

  /// `所有`
  String get all {
    return Intl.message(
      '所有',
      name: 'all',
      desc: '',
      args: [],
    );
  }

  /// `過濾`
  String get filter {
    return Intl.message(
      '過濾',
      name: 'filter',
      desc: '',
      args: [],
    );
  }

  /// `標籤`
  String get tags {
    return Intl.message(
      '標籤',
      name: 'tags',
      desc: '',
      args: [],
    );
  }

  /// `分類目錄`
  String get categories {
    return Intl.message(
      '分類目錄',
      name: 'categories',
      desc: '',
      args: [],
    );
  }

  /// `屬性`
  String get attributes {
    return Intl.message(
      '屬性',
      name: 'attributes',
      desc: '',
      args: [],
    );
  }

  /// `重設密碼`
  String get resetPassword {
    return Intl.message(
      '重設密碼',
      name: 'resetPassword',
      desc: '',
      args: [],
    );
  }

  /// `重置你的密碼`
  String get resetYourPassword {
    return Intl.message(
      '重置你的密碼',
      name: 'resetYourPassword',
      desc: '',
      args: [],
    );
  }

  /// `您的用戶名或電子郵件`
  String get yourUsernameEmail {
    return Intl.message(
      '您的用戶名或電子郵件',
      name: 'yourUsernameEmail',
      desc: '',
      args: [],
    );
  }

  /// `獲取密碼連結`
  String get getPasswordLink {
    return Intl.message(
      '獲取密碼連結',
      name: 'getPasswordLink',
      desc: '',
      args: [],
    );
  }

  /// `檢查您的電子郵件以獲取確認連結`
  String get checkConfirmLink {
    return Intl.message(
      '檢查您的電子郵件以獲取確認連結',
      name: 'checkConfirmLink',
      desc: '',
      args: [],
    );
  }

  /// `用戶名/電子郵件為空`
  String get emptyUsername {
    return Intl.message(
      '用戶名/電子郵件為空',
      name: 'emptyUsername',
      desc: '',
      args: [],
    );
  }

  /// `羅馬尼亞語`
  String get romanian {
    return Intl.message(
      '羅馬尼亞語',
      name: 'romanian',
      desc: '',
      args: [],
    );
  }

  /// `土耳其`
  String get turkish {
    return Intl.message(
      '土耳其',
      name: 'turkish',
      desc: '',
      args: [],
    );
  }

  /// `義大利文`
  String get italian {
    return Intl.message(
      '義大利文',
      name: 'italian',
      desc: '',
      args: [],
    );
  }

  /// `印尼`
  String get indonesian {
    return Intl.message(
      '印尼',
      name: 'indonesian',
      desc: '',
      args: [],
    );
  }

  /// `德文`
  String get german {
    return Intl.message(
      '德文',
      name: 'german',
      desc: '',
      args: [],
    );
  }

  /// `您的優惠券代碼無效`
  String get couponInvalid {
    return Intl.message(
      '您的優惠券代碼無效',
      name: 'couponInvalid',
      desc: '',
      args: [],
    );
  }

  /// `精選`
  String get featured {
    return Intl.message(
      '精選',
      name: 'featured',
      desc: '',
      args: [],
    );
  }

  /// `特價中`
  String get onSale {
    return Intl.message(
      '特價中',
      name: 'onSale',
      desc: '',
      args: [],
    );
  }

  /// `請檢查網路線!`
  String get pleaseCheckInternet {
    return Intl.message(
      '請檢查網路線!',
      name: 'pleaseCheckInternet',
      desc: '',
      args: [],
    );
  }

  /// `無法啟動此應用，請確保您在config.dart上的設置正確`
  String get canNotLaunch {
    return Intl.message(
      '無法啟動此應用，請確保您在config.dart上的設置正確',
      name: 'canNotLaunch',
      desc: '',
      args: [],
    );
  }

  /// `信息`
  String get message {
    return Intl.message(
      '信息',
      name: 'message',
      desc: '',
      args: [],
    );
  }

  /// `帳單地址`
  String get billingAddress {
    return Intl.message(
      '帳單地址',
      name: 'billingAddress',
      desc: '',
      args: [],
    );
  }

  /// `不是`
  String get no {
    return Intl.message(
      '不是',
      name: 'no',
      desc: '',
      args: [],
    );
  }

  /// `是的`
  String get yes {
    return Intl.message(
      '是的',
      name: 'yes',
      desc: '',
      args: [],
    );
  }

  /// `你確定嗎？`
  String get areYouSure {
    return Intl.message(
      '你確定嗎？',
      name: 'areYouSure',
      desc: '',
      args: [],
    );
  }

  /// `是否要退出應用程式`
  String get doYouWantToExitApp {
    return Intl.message(
      '是否要退出應用程式',
      name: 'doYouWantToExitApp',
      desc: '',
      args: [],
    );
  }

  /// `購物車, {totalCartQuantity} 件商品`
  String shoppingCartItems(Object totalCartQuantity) {
    return Intl.message(
      '購物車, $totalCartQuantity 件商品',
      name: 'shoppingCartItems',
      desc: '',
      args: [totalCartQuantity],
    );
  }

  /// `暫停`
  String get orderStatusOnHold {
    return Intl.message(
      '暫停',
      name: 'orderStatusOnHold',
      desc: '',
      args: [],
    );
  }

  /// `待付款`
  String get orderStatusPendingPayment {
    return Intl.message(
      '待付款',
      name: 'orderStatusPendingPayment',
      desc: '',
      args: [],
    );
  }

  /// `失敗`
  String get orderStatusFailed {
    return Intl.message(
      '失敗',
      name: 'orderStatusFailed',
      desc: '',
      args: [],
    );
  }

  /// `處理中`
  String get orderStatusProcessing {
    return Intl.message(
      '處理中',
      name: 'orderStatusProcessing',
      desc: '',
      args: [],
    );
  }

  /// `完成`
  String get orderStatusCompleted {
    return Intl.message(
      '完成',
      name: 'orderStatusCompleted',
      desc: '',
      args: [],
    );
  }

  /// `取消`
  String get orderStatusCancelled {
    return Intl.message(
      '取消',
      name: 'orderStatusCancelled',
      desc: '',
      args: [],
    );
  }

  /// `已退款`
  String get orderStatusRefunded {
    return Intl.message(
      '已退款',
      name: 'orderStatusRefunded',
      desc: '',
      args: [],
    );
  }

  /// `請填寫您的代碼`
  String get pleaseFillCode {
    return Intl.message(
      '請填寫您的代碼',
      name: 'pleaseFillCode',
      desc: '',
      args: [],
    );
  }

  /// `警告: {message}`
  String warning(Object message) {
    return Intl.message(
      '警告: $message',
      name: 'warning',
      desc: '',
      args: [message],
    );
  }

  /// `{itemCount} 件商品`
  String nItems(Object itemCount) {
    return Intl.message(
      '$itemCount 件商品',
      name: 'nItems',
      desc: '',
      args: [itemCount],
    );
  }

  /// `數據為空`
  String get dataEmpty {
    return Intl.message(
      '數據為空',
      name: 'dataEmpty',
      desc: '',
      args: [],
    );
  }

  /// `您的地址存在於您的本地`
  String get yourAddressExistYourLocal {
    return Intl.message(
      '您的地址存在於您的本地',
      name: 'yourAddressExistYourLocal',
      desc: '',
      args: [],
    );
  }

  /// `Ok`
  String get ok {
    return Intl.message(
      'Ok',
      name: 'ok',
      desc: '',
      args: [],
    );
  }

  /// `您已將位址保存在本地`
  String get youHaveBeenSaveAddressYourLocal {
    return Intl.message(
      '您已將位址保存在本地',
      name: 'youHaveBeenSaveAddressYourLocal',
      desc: '',
      args: [],
    );
  }

  /// `撤銷`
  String get undo {
    return Intl.message(
      '撤銷',
      name: 'undo',
      desc: '',
      args: [],
    );
  }

  /// `此平臺不支援 webview`
  String get thisPlatformNotSupportWebview {
    return Intl.message(
      '此平臺不支援 webview',
      name: 'thisPlatformNotSupportWebview',
      desc: '',
      args: [],
    );
  }

  /// `No back history item`
  String get noBackHistoryItem {
    return Intl.message(
      'No back history item',
      name: 'noBackHistoryItem',
      desc: '',
      args: [],
    );
  }

  /// `沒有轉發歷史項目`
  String get noForwardHistoryItem {
    return Intl.message(
      '沒有轉發歷史項目',
      name: 'noForwardHistoryItem',
      desc: '',
      args: [],
    );
  }

  /// `日期預訂`
  String get dateBooking {
    return Intl.message(
      '日期預訂',
      name: 'dateBooking',
      desc: '',
      args: [],
    );
  }

  /// `期間`
  String get duration {
    return Intl.message(
      '期間',
      name: 'duration',
      desc: '',
      args: [],
    );
  }

  /// `已成功添加`
  String get addedSuccessfully {
    return Intl.message(
      '已成功添加',
      name: 'addedSuccessfully',
      desc: '',
      args: [],
    );
  }

  /// `未找到`
  String get notFound {
    return Intl.message(
      '未找到',
      name: 'notFound',
      desc: '',
      args: [],
    );
  }

  /// `Error: {message}`
  String error(Object message) {
    return Intl.message(
      'Error: $message',
      name: 'error',
      desc: '',
      args: [message],
    );
  }

  /// `返回主頁`
  String get goBackHomePage {
    return Intl.message(
      '返回主頁',
      name: 'goBackHomePage',
      desc: '',
      args: [],
    );
  }

  /// `哎呀，博客好像不存在了`
  String get noBlog {
    return Intl.message(
      '哎呀，博客好像不存在了',
      name: 'noBlog',
      desc: '',
      args: [],
    );
  }

  /// `Prev`
  String get prev {
    return Intl.message(
      'Prev',
      name: 'prev',
      desc: '',
      args: [],
    );
  }

  /// `跳過`
  String get skip {
    return Intl.message(
      '跳過',
      name: 'skip',
      desc: '',
      args: [],
    );
  }

  /// `下載`
  String get download {
    return Intl.message(
      '下載',
      name: 'download',
      desc: '',
      args: [],
    );
  }

  /// `{day} 天前`
  String daysAgo(Object day) {
    return Intl.message(
      '$day 天前',
      name: 'daysAgo',
      desc: '',
      args: [day],
    );
  }

  /// `{hour} 小時前`
  String hoursAgo(Object hour) {
    return Intl.message(
      '$hour 小時前',
      name: 'hoursAgo',
      desc: '',
      args: [hour],
    );
  }

  /// `{minute} 分鐘錢`
  String minutesAgo(Object minute) {
    return Intl.message(
      '$minute 分鐘錢',
      name: 'minutesAgo',
      desc: '',
      args: [minute],
    );
  }

  /// `{second} 秒前`
  String secondsAgo(Object second) {
    return Intl.message(
      '$second 秒前',
      name: 'secondsAgo',
      desc: '',
      args: [second],
    );
  }

  /// `為這個應用軟體評分`
  String get rateThisApp {
    return Intl.message(
      '為這個應用軟體評分',
      name: 'rateThisApp',
      desc: '',
      args: [],
    );
  }

  /// `如果您喜歡這個應用程式，請花一點時間來查看它 !\n它真的對我們有幫助，而且不會超過一分鐘。`
  String get rateThisAppDescription {
    return Intl.message(
      '如果您喜歡這個應用程式，請花一點時間來查看它 !\n它真的對我們有幫助，而且不會超過一分鐘。',
      name: 'rateThisAppDescription',
      desc: '',
      args: [],
    );
  }

  /// `評分`
  String get rate {
    return Intl.message(
      '評分',
      name: 'rate',
      desc: '',
      args: [],
    );
  }

  /// `不，謝謝`
  String get noThanks {
    return Intl.message(
      '不，謝謝',
      name: 'noThanks',
      desc: '',
      args: [],
    );
  }

  /// `也許以後`
  String get maybeLater {
    return Intl.message(
      '也許以後',
      name: 'maybeLater',
      desc: '',
      args: [],
    );
  }

  /// `電話`
  String get phone {
    return Intl.message(
      '電話',
      name: 'phone',
      desc: '',
      args: [],
    );
  }

  /// `電話號碼驗證`
  String get phoneNumberVerification {
    return Intl.message(
      '電話號碼驗證',
      name: 'phoneNumberVerification',
      desc: '',
      args: [],
    );
  }

  /// `輸入發送到的代碼`
  String get enterSendedCode {
    return Intl.message(
      '輸入發送到的代碼',
      name: 'enterSendedCode',
      desc: '',
      args: [],
    );
  }

  /// `*請正確填寫所有欄位`
  String get pleasefillUpAllCellsProperly {
    return Intl.message(
      '*請正確填寫所有欄位',
      name: 'pleasefillUpAllCellsProperly',
      desc: '',
      args: [],
    );
  }

  /// `沒有收到驗證碼? `
  String get didntReceiveCode {
    return Intl.message(
      '沒有收到驗證碼? ',
      name: 'didntReceiveCode',
      desc: '',
      args: [],
    );
  }

  /// ` 重發`
  String get resend {
    return Intl.message(
      ' 重發',
      name: 'resend',
      desc: '',
      args: [],
    );
  }

  /// `請填寫所有欄位`
  String get pleaseInputFillAllFields {
    return Intl.message(
      '請填寫所有欄位',
      name: 'pleaseInputFillAllFields',
      desc: '',
      args: [],
    );
  }

  /// `請同意我們的條款`
  String get pleaseAgreeTerms {
    return Intl.message(
      '請同意我們的條款',
      name: 'pleaseAgreeTerms',
      desc: '',
      args: [],
    );
  }

  /// `Under Vietnamese laws, users’ information such as names, email addresses, passwords and date of birth could be classified as “personal information.\n\n In particular,\n (a) Under Decree 72/2013, personal information is defined as information  which  is  attached  to  the  identification  of  the  identity  and personal  details  of  an  individual  including name,  age,  address,  people's  identity  card  number, telephone number, email address and other information as stipulated by law\n\n (b) Under Circular 25/2010,  personal information means information sufficient to precisely identify an individual, which includes at least one of the following details: full name, birth date, occupation, title, contact address, email address, telephone number, identity card number and passport number. Information of personal privacy includes health record, tax payment record, social insurance card number, credit card number and other personal secrets.\n\n Circular 25 applies to the collection and use of personal information by websites operated by Vietnamese Government authorities. Circular 25 is not directly applicable to the collection and use of personal information by websites operated by non-Government entities. However, the provisions of Circular 25 could be applied by analogy. In addition, it is likely that a non-Government entity will be subject to the same or more stringent standards than those applicable to a Government entity.`
  String get privacyTerms {
    return Intl.message(
      'Under Vietnamese laws, users’ information such as names, email addresses, passwords and date of birth could be classified as “personal information.\n\n In particular,\n (a) Under Decree 72/2013, personal information is defined as information  which  is  attached  to  the  identification  of  the  identity  and personal  details  of  an  individual  including name,  age,  address,  people\'s  identity  card  number, telephone number, email address and other information as stipulated by law\n\n (b) Under Circular 25/2010,  personal information means information sufficient to precisely identify an individual, which includes at least one of the following details: full name, birth date, occupation, title, contact address, email address, telephone number, identity card number and passport number. Information of personal privacy includes health record, tax payment record, social insurance card number, credit card number and other personal secrets.\n\n Circular 25 applies to the collection and use of personal information by websites operated by Vietnamese Government authorities. Circular 25 is not directly applicable to the collection and use of personal information by websites operated by non-Government entities. However, the provisions of Circular 25 could be applied by analogy. In addition, it is likely that a non-Government entity will be subject to the same or more stringent standards than those applicable to a Government entity.',
      name: 'privacyTerms',
      desc: '',
      args: [],
    );
  }

  /// `URL`
  String get url {
    return Intl.message(
      'URL',
      name: 'url',
      desc: '',
      args: [],
    );
  }

  /// `附近地點`
  String get nearbyPlaces {
    return Intl.message(
      '附近地點',
      name: 'nearbyPlaces',
      desc: '',
      args: [],
    );
  }

  /// `未找到結果`
  String get noResultFound {
    return Intl.message(
      '未找到結果',
      name: 'noResultFound',
      desc: '',
      args: [],
    );
  }

  /// `搜索地點`
  String get searchPlace {
    return Intl.message(
      '搜索地點',
      name: 'searchPlace',
      desc: '',
      args: [],
    );
  }

  /// `點擊選擇地點`
  String get tapSelectLocation {
    return Intl.message(
      '點擊選擇地點',
      name: 'tapSelectLocation',
      desc: '',
      args: [],
    );
  }

  /// `葡萄牙語`
  String get brazil {
    return Intl.message(
      '葡萄牙語',
      name: 'brazil',
      desc: '',
      args: [],
    );
  }

  /// `在缺貨`
  String get backOrder {
    return Intl.message(
      '在缺貨',
      name: 'backOrder',
      desc: '',
      args: [],
    );
  }

  /// `French`
  String get french {
    return Intl.message(
      'French',
      name: 'french',
      desc: '',
      args: [],
    );
  }

  /// `應用程式在請求資料期間出現問題，請聯繫管理員解決問題： {message}`
  String loginErrorServiceProvider(Object message) {
    return Intl.message(
      '應用程式在請求資料期間出現問題，請聯繫管理員解決問題： $message',
      name: 'loginErrorServiceProvider',
      desc: '',
      args: [message],
    );
  }

  /// `登錄被取消了`
  String get loginCanceled {
    return Intl.message(
      '登錄被取消了',
      name: 'loginCanceled',
      desc: '',
      args: [],
    );
  }

  /// `哎呀，這個頁面好像不存在了！`
  String get noPost {
    return Intl.message(
      '哎呀，這個頁面好像不存在了！',
      name: 'noPost',
      desc: '',
      args: [],
    );
  }

  /// `最小數量是`
  String get minimumQuantityIs {
    return Intl.message(
      '最小數量是',
      name: 'minimumQuantityIs',
      desc: '',
      args: [],
    );
  }

  /// `您只能購買`
  String get youCanOnlyPurchase {
    return Intl.message(
      '您只能購買',
      name: 'youCanOnlyPurchase',
      desc: '',
      args: [],
    );
  }

  /// `對於這個產品`
  String get forThisProduct {
    return Intl.message(
      '對於這個產品',
      name: 'forThisProduct',
      desc: '',
      args: [],
    );
  }

  /// `目前我們只有`
  String get currentlyWeOnlyHave {
    return Intl.message(
      '目前我們只有',
      name: 'currentlyWeOnlyHave',
      desc: '',
      args: [],
    );
  }

  /// `本產品的`
  String get ofThisProduct {
    return Intl.message(
      '本產品的',
      name: 'ofThisProduct',
      desc: '',
      args: [],
    );
  }

  /// `從`
  String get from {
    return Intl.message(
      '從',
      name: 'from',
      desc: '',
      args: [],
    );
  }

  /// `總訂單的價值必須至少為`
  String get totalCartValue {
    return Intl.message(
      '總訂單的價值必須至少為',
      name: 'totalCartValue',
      desc: '',
      args: [],
    );
  }

  /// `Hungary`
  String get hungary {
    return Intl.message(
      'Hungary',
      name: 'hungary',
      desc: '',
      args: [],
    );
  }

  /// `街道名稱`
  String get streetNameApartment {
    return Intl.message(
      '街道名稱',
      name: 'streetNameApartment',
      desc: '',
      args: [],
    );
  }

  /// `塊`
  String get streetNameBlock {
    return Intl.message(
      '塊',
      name: 'streetNameBlock',
      desc: '',
      args: [],
    );
  }

  /// `所售`
  String get soldBy {
    return Intl.message(
      '所售',
      name: 'soldBy',
      desc: '',
      args: [],
    );
  }

  /// `刷新`
  String get refresh {
    return Intl.message(
      '刷新',
      name: 'refresh',
      desc: '',
      args: [],
    );
  }

  /// `有將您的積分應用到購物車的折扣規則`
  String get pointRewardMessage {
    return Intl.message(
      '有將您的積分應用到購物車的折扣規則',
      name: 'pointRewardMessage',
      desc: '',
      args: [],
    );
  }

  /// `您的可用積分： {point}`
  String availablePoints(Object point) {
    return Intl.message(
      '您的可用積分： $point',
      name: 'availablePoints',
      desc: '',
      args: [point],
    );
  }

  /// `選擇點`
  String get selectThePoint {
    return Intl.message(
      '選擇點',
      name: 'selectThePoint',
      desc: '',
      args: [],
    );
  }

  /// `購物車折扣`
  String get cartDiscount {
    return Intl.message(
      '購物車折扣',
      name: 'cartDiscount',
      desc: '',
      args: [],
    );
  }

  /// `請為產品的每個屬性選擇一個選項`
  String get pleaseSelectAllAttributes {
    return Intl.message(
      '請為產品的每個屬性選擇一個選項',
      name: 'pleaseSelectAllAttributes',
      desc: '',
      args: [],
    );
  }

  /// `註冊為供應商`
  String get registerAsVendor {
    return Intl.message(
      '註冊為供應商',
      name: 'registerAsVendor',
      desc: '',
      args: [],
    );
  }

  /// `供應商管理員`
  String get vendorAdmin {
    return Intl.message(
      '供應商管理員',
      name: 'vendorAdmin',
      desc: '',
      args: [],
    );
  }

  /// `添加清單`
  String get addListing {
    return Intl.message(
      '添加清單',
      name: 'addListing',
      desc: '',
      args: [],
    );
  }

  /// `Russian`
  String get russian {
    return Intl.message(
      'Russian',
      name: 'russian',
      desc: '',
      args: [],
    );
  }

  /// `Hebrew`
  String get hebrew {
    return Intl.message(
      'Hebrew',
      name: 'hebrew',
      desc: '',
      args: [],
    );
  }

  /// `Thai`
  String get thailand {
    return Intl.message(
      'Thai',
      name: 'thailand',
      desc: '',
      args: [],
    );
  }

  /// `添加圖片`
  String get addingYourImage {
    return Intl.message(
      '添加圖片',
      name: 'addingYourImage',
      desc: '',
      args: [],
    );
  }

  /// `額外服務`
  String get additionalServices {
    return Intl.message(
      '額外服務',
      name: 'additionalServices',
      desc: '',
      args: [],
    );
  }

  /// `大人`
  String get adults {
    return Intl.message(
      '大人',
      name: 'adults',
      desc: '',
      args: [],
    );
  }

  /// `幾乎售罄`
  String get almostSoldOut {
    return Intl.message(
      '幾乎售罄',
      name: 'almostSoldOut',
      desc: '',
      args: [],
    );
  }

  /// `預定好了`
  String get booked {
    return Intl.message(
      '預定好了',
      name: 'booked',
      desc: '',
      args: [],
    );
  }

  /// `預訂`
  String get booking {
    return Intl.message(
      '預訂',
      name: 'booking',
      desc: '',
      args: [],
    );
  }

  /// `預訂已取消`
  String get bookingCancelled {
    return Intl.message(
      '預訂已取消',
      name: 'bookingCancelled',
      desc: '',
      args: [],
    );
  }

  /// `確認`
  String get bookingConfirm {
    return Intl.message(
      '確認',
      name: 'bookingConfirm',
      desc: '',
      args: [],
    );
  }

  /// `出事了請稍後再試。`
  String get bookingError {
    return Intl.message(
      '出事了請稍後再試。',
      name: 'bookingError',
      desc: '',
      args: [],
    );
  }

  /// `預訂歷史`
  String get bookingHistory {
    return Intl.message(
      '預訂歷史',
      name: 'bookingHistory',
      desc: '',
      args: [],
    );
  }

  /// `現在預訂`
  String get bookingNow {
    return Intl.message(
      '現在預訂',
      name: 'bookingNow',
      desc: '',
      args: [],
    );
  }

  /// `預訂成功`
  String get bookingSuccess {
    return Intl.message(
      '預訂成功',
      name: 'bookingSuccess',
      desc: '',
      args: [],
    );
  }

  /// `預訂摘要`
  String get bookingSummary {
    return Intl.message(
      '預訂摘要',
      name: 'bookingSummary',
      desc: '',
      args: [],
    );
  }

  /// `無法預訂`
  String get bookingUnavailable {
    return Intl.message(
      '無法預訂',
      name: 'bookingUnavailable',
      desc: '',
      args: [],
    );
  }

  /// `按標籤`
  String get byTag {
    return Intl.message(
      '按標籤',
      name: 'byTag',
      desc: '',
      args: [],
    );
  }

  /// `聯繫`
  String get contact {
    return Intl.message(
      '聯繫',
      name: 'contact',
      desc: '',
      args: [],
    );
  }

  /// `對話`
  String get conversations {
    return Intl.message(
      '對話',
      name: 'conversations',
      desc: '',
      args: [],
    );
  }

  /// `創建產品`
  String get createProduct {
    return Intl.message(
      '創建產品',
      name: 'createProduct',
      desc: '',
      args: [],
    );
  }

  /// `日期結束`
  String get dateEnd {
    return Intl.message(
      '日期結束',
      name: 'dateEnd',
      desc: '',
      args: [],
    );
  }

  /// `開始日期`
  String get dateStart {
    return Intl.message(
      '開始日期',
      name: 'dateStart',
      desc: '',
      args: [],
    );
  }

  /// `以 {timeLeft}結尾`
  String endsIn(Object timeLeft) {
    return Intl.message(
      '以 $timeLeft結尾',
      name: 'endsIn',
      desc: '',
      args: [timeLeft],
    );
  }

  /// `額外服務`
  String get extraServices {
    return Intl.message(
      '額外服務',
      name: 'extraServices',
      desc: '',
      args: [],
    );
  }

  /// `特徵`
  String get features {
    return Intl.message(
      '特徵',
      name: 'features',
      desc: '',
      args: [],
    );
  }

  /// `賓客`
  String get guests {
    return Intl.message(
      '賓客',
      name: 'guests',
      desc: '',
      args: [],
    );
  }

  /// `小時`
  String get hour {
    return Intl.message(
      '小時',
      name: 'hour',
      desc: '',
      args: [],
    );
  }

  /// `匈牙利`
  String get hungarian {
    return Intl.message(
      '匈牙利',
      name: 'hungarian',
      desc: '',
      args: [],
    );
  }

  /// `圖庫`
  String get imageGallery {
    return Intl.message(
      '圖庫',
      name: 'imageGallery',
      desc: '',
      args: [],
    );
  }

  /// `地圖`
  String get map {
    return Intl.message(
      '地圖',
      name: 'map',
      desc: '',
      args: [],
    );
  }

  /// `我的產品`
  String get myProducts {
    return Intl.message(
      '我的產品',
      name: 'myProducts',
      desc: '',
      args: [],
    );
  }

  /// `您沒有任何產品。嘗試創建一個！`
  String get myProductsEmpty {
    return Intl.message(
      '您沒有任何產品。嘗試創建一個！',
      name: 'myProductsEmpty',
      desc: '',
      args: [],
    );
  }

  /// `無空位`
  String get noSlotAvailable {
    return Intl.message(
      '無空位',
      name: 'noSlotAvailable',
      desc: '',
      args: [],
    );
  }

  /// `沒有`
  String get none {
    return Intl.message(
      '沒有',
      name: 'none',
      desc: '',
      args: [],
    );
  }

  /// `上`
  String get on {
    return Intl.message(
      '上',
      name: 'on',
      desc: '',
      args: [],
    );
  }

  /// `現在付款`
  String get payNow {
    return Intl.message(
      '現在付款',
      name: 'payNow',
      desc: '',
      args: [],
    );
  }

  /// `選擇日期和時間`
  String get pickADate {
    return Intl.message(
      '選擇日期和時間',
      name: 'pickADate',
      desc: '',
      args: [],
    );
  }

  /// `發佈產品`
  String get postProduct {
    return Intl.message(
      '發佈產品',
      name: 'postProduct',
      desc: '',
      args: [],
    );
  }

  /// `選單`
  String get prices {
    return Intl.message(
      '選單',
      name: 'prices',
      desc: '',
      args: [],
    );
  }

  /// `產品`
  String get product {
    return Intl.message(
      '產品',
      name: 'product',
      desc: '',
      args: [],
    );
  }

  /// `產品名稱`
  String get productName {
    return Intl.message(
      '產品名稱',
      name: 'productName',
      desc: '',
      args: [],
    );
  }

  /// `產品類別`
  String get productType {
    return Intl.message(
      '產品類別',
      name: 'productType',
      desc: '',
      args: [],
    );
  }

  /// `正常價格`
  String get regularPrice {
    return Intl.message(
      '正常價格',
      name: 'regularPrice',
      desc: '',
      args: [],
    );
  }

  /// `刪除到願望清單`
  String get removeFromWishList {
    return Intl.message(
      '刪除到願望清單',
      name: 'removeFromWishList',
      desc: '',
      args: [],
    );
  }

  /// `要求預訂`
  String get requestBooking {
    return Intl.message(
      '要求預訂',
      name: 'requestBooking',
      desc: '',
      args: [],
    );
  }

  /// `銷售價格`
  String get salePrice {
    return Intl.message(
      '銷售價格',
      name: 'salePrice',
      desc: '',
      args: [],
    );
  }

  /// `車間訂單`
  String get shopOrders {
    return Intl.message(
      '車間訂單',
      name: 'shopOrders',
      desc: '',
      args: [],
    );
  }

  /// `SKU`
  String get sku {
    return Intl.message(
      'SKU',
      name: 'sku',
      desc: '',
      args: [],
    );
  }

  /// `已售出： {numberOfUnitsSold}`
  String sold(Object numberOfUnitsSold) {
    return Intl.message(
      '已售出： $numberOfUnitsSold',
      name: 'sold',
      desc: '',
      args: [numberOfUnitsSold],
    );
  }

  /// `商店`
  String get stores {
    return Intl.message(
      '商店',
      name: 'stores',
      desc: '',
      args: [],
    );
  }

  /// `稅`
  String get tax {
    return Intl.message(
      '稅',
      name: 'tax',
      desc: '',
      args: [],
    );
  }

  /// `該日期不可用`
  String get thisDateIsNotAvailable {
    return Intl.message(
      '該日期不可用',
      name: 'thisDateIsNotAvailable',
      desc: '',
      args: [],
    );
  }

  /// `門票`
  String get tickets {
    return Intl.message(
      '門票',
      name: 'tickets',
      desc: '',
      args: [],
    );
  }

  /// `交易已取消`
  String get transactionCancelled {
    return Intl.message(
      '交易已取消',
      name: 'transactionCancelled',
      desc: '',
      args: [],
    );
  }

  /// `供應商資訊`
  String get vendorInfo {
    return Intl.message(
      '供應商資訊',
      name: 'vendorInfo',
      desc: '',
      args: [],
    );
  }

  /// `參觀商店`
  String get visitStore {
    return Intl.message(
      '參觀商店',
      name: 'visitStore',
      desc: '',
      args: [],
    );
  }

  /// `等待載入圖片`
  String get waitForLoad {
    return Intl.message(
      '等待載入圖片',
      name: 'waitForLoad',
      desc: '',
      args: [],
    );
  }

  /// `等待後期產品`
  String get waitForPost {
    return Intl.message(
      '等待後期產品',
      name: 'waitForPost',
      desc: '',
      args: [],
    );
  }

  /// `等待確認中`
  String get waitingForConfirmation {
    return Intl.message(
      '等待確認中',
      name: 'waitingForConfirmation',
      desc: '',
      args: [],
    );
  }

  /// `您的預訂詳細資訊`
  String get yourBookingDetail {
    return Intl.message(
      '您的預訂詳細資訊',
      name: 'yourBookingDetail',
      desc: '',
      args: [],
    );
  }

  /// `埃及`
  String get Egypt {
    return Intl.message(
      '埃及',
      name: 'Egypt',
      desc: '',
      args: [],
    );
  }

  /// `印地語`
  String get India {
    return Intl.message(
      '印地語',
      name: 'India',
      desc: '',
      args: [],
    );
  }

  /// `朝鮮的`
  String get Korean {
    return Intl.message(
      '朝鮮的',
      name: 'Korean',
      desc: '',
      args: [],
    );
  }

  /// `荷蘭`
  String get Netherlands {
    return Intl.message(
      '荷蘭',
      name: 'Netherlands',
      desc: '',
      args: [],
    );
  }

  /// `荷蘭人`
  String get Dutch {
    return Intl.message(
      '荷蘭人',
      name: 'Dutch',
      desc: '',
      args: [],
    );
  }

  /// `繼續`
  String get continues {
    return Intl.message(
      '繼續',
      name: 'continues',
      desc: '',
      args: [],
    );
  }

  /// `退款`
  String get refunds {
    return Intl.message(
      '退款',
      name: 'refunds',
      desc: '',
      args: [],
    );
  }

  /// `用戶名或密碼不正確`
  String get UserNameInCorrect {
    return Intl.message(
      '用戶名或密碼不正確',
      name: 'UserNameInCorrect',
      desc: '',
      args: [],
    );
  }

  /// `優惠券已成功保存。`
  String get couponHasBeenSavedSuccessfully {
    return Intl.message(
      '優惠券已成功保存。',
      name: 'couponHasBeenSavedSuccessfully',
      desc: '',
      args: [],
    );
  }

  /// `過期`
  String get expired {
    return Intl.message(
      '過期',
      name: 'expired',
      desc: '',
      args: [],
    );
  }

  /// `在 {time}期滿`
  String expiringInTime(Object time) {
    return Intl.message(
      '在 $time期滿',
      name: 'expiringInTime',
      desc: '',
      args: [time],
    );
  }

  /// `固定購物車折扣`
  String get fixedCartDiscount {
    return Intl.message(
      '固定購物車折扣',
      name: 'fixedCartDiscount',
      desc: '',
      args: [],
    );
  }

  /// `固定產品折扣`
  String get fixedProductDiscount {
    return Intl.message(
      '固定產品折扣',
      name: 'fixedProductDiscount',
      desc: '',
      args: [],
    );
  }

  /// `留著以後用`
  String get saveForLater {
    return Intl.message(
      '留著以後用',
      name: 'saveForLater',
      desc: '',
      args: [],
    );
  }

  /// `現在使用`
  String get useNow {
    return Intl.message(
      '現在使用',
      name: 'useNow',
      desc: '',
      args: [],
    );
  }

  /// `有效期 至 {date}`
  String validUntilDate(Object date) {
    return Intl.message(
      '有效期 至 $date',
      name: 'validUntilDate',
      desc: '',
      args: [date],
    );
  }

  /// `數量`
  String get Qty {
    return Intl.message(
      '數量',
      name: 'Qty',
      desc: '',
      args: [],
    );
  }

  /// `創建於：`
  String get createdOn {
    return Intl.message(
      '創建於：',
      name: 'createdOn',
      desc: '',
      args: [],
    );
  }

  /// `物品總數：`
  String get itemTotal {
    return Intl.message(
      '物品總數：',
      name: 'itemTotal',
      desc: '',
      args: [],
    );
  }

  /// `訂單ID：`
  String get orderId {
    return Intl.message(
      '訂單ID：',
      name: 'orderId',
      desc: '',
      args: [],
    );
  }

  /// `添加產品`
  String get addProduct {
    return Intl.message(
      '添加產品',
      name: 'addProduct',
      desc: '',
      args: [],
    );
  }

  /// `最新銷售`
  String get allOrders {
    return Intl.message(
      '最新銷售',
      name: 'allOrders',
      desc: '',
      args: [],
    );
  }

  /// `批准`
  String get approve {
    return Intl.message(
      '批准',
      name: 'approve',
      desc: '',
      args: [],
    );
  }

  /// `批准`
  String get approved {
    return Intl.message(
      '批准',
      name: 'approved',
      desc: '',
      args: [],
    );
  }

  /// `背部`
  String get back {
    return Intl.message(
      '背部',
      name: 'back',
      desc: '',
      args: [],
    );
  }

  /// `找不到此訂單ID`
  String get cantFindThisOrderId {
    return Intl.message(
      '找不到此訂單ID',
      name: 'cantFindThisOrderId',
      desc: '',
      args: [],
    );
  }

  /// `更改`
  String get change {
    return Intl.message(
      '更改',
      name: 'change',
      desc: '',
      args: [],
    );
  }

  /// `聊天清單螢幕`
  String get chatListScreen {
    return Intl.message(
      '聊天清單螢幕',
      name: 'chatListScreen',
      desc: '',
      args: [],
    );
  }

  /// `從圖庫中選擇`
  String get chooseFromGallery {
    return Intl.message(
      '從圖庫中選擇',
      name: 'chooseFromGallery',
      desc: '',
      args: [],
    );
  }

  /// `從伺服器選擇`
  String get chooseFromServer {
    return Intl.message(
      '從伺服器選擇',
      name: 'chooseFromServer',
      desc: '',
      args: [],
    );
  }

  /// `送到了（送去了`
  String get deliveredTo {
    return Intl.message(
      '送到了（送去了',
      name: 'deliveredTo',
      desc: '',
      args: [],
    );
  }

  /// `收益`
  String get earnings {
    return Intl.message(
      '收益',
      name: 'earnings',
      desc: '',
      args: [],
    );
  }

  /// `編輯產品資訊`
  String get editProductInfo {
    return Intl.message(
      '編輯產品資訊',
      name: 'editProductInfo',
      desc: '',
      args: [],
    );
  }

  /// `總銷售額`
  String get grossSales {
    return Intl.message(
      '總銷售額',
      name: 'grossSales',
      desc: '',
      args: [],
    );
  }

  /// `家`
  String get home {
    return Intl.message(
      '家',
      name: 'home',
      desc: '',
      args: [],
    );
  }

  /// `管理庫存`
  String get manageStock {
    return Intl.message(
      '管理庫存',
      name: 'manageStock',
      desc: '',
      args: [],
    );
  }

  /// `...更多`
  String get more {
    return Intl.message(
      '...更多',
      name: 'more',
      desc: '',
      args: [],
    );
  }

  /// `名稱`
  String get name {
    return Intl.message(
      '名稱',
      name: 'name',
      desc: '',
      args: [],
    );
  }

  /// `通知`
  String get notifications {
    return Intl.message(
      '通知',
      name: 'notifications',
      desc: '',
      args: [],
    );
  }

  /// `或登錄`
  String get orLoginWith {
    return Intl.message(
      '或登錄',
      name: 'orLoginWith',
      desc: '',
      args: [],
    );
  }

  /// `有待`
  String get orderStatusPending {
    return Intl.message(
      '有待',
      name: 'orderStatusPending',
      desc: '',
      args: [],
    );
  }

  /// `合計訂單`
  String get orderTotal {
    return Intl.message(
      '合計訂單',
      name: 'orderTotal',
      desc: '',
      args: [],
    );
  }

  /// `有待`
  String get pending {
    return Intl.message(
      '有待',
      name: 'pending',
      desc: '',
      args: [],
    );
  }

  /// `評分`
  String get rating {
    return Intl.message(
      '評分',
      name: 'rating',
      desc: '',
      args: [],
    );
  }

  /// `審核批准`
  String get reviewApproval {
    return Intl.message(
      '審核批准',
      name: 'reviewApproval',
      desc: '',
      args: [],
    );
  }

  /// `評測`
  String get reviews {
    return Intl.message(
      '評測',
      name: 'reviews',
      desc: '',
      args: [],
    );
  }

  /// `搜索訂單ID ...`
  String get searchOrderId {
    return Intl.message(
      '搜索訂單ID ...',
      name: 'searchOrderId',
      desc: '',
      args: [],
    );
  }

  /// `選擇圖像`
  String get selectImage {
    return Intl.message(
      '選擇圖像',
      name: 'selectImage',
      desc: '',
      args: [],
    );
  }

  /// `簡短的介紹`
  String get shortDescription {
    return Intl.message(
      '簡短的介紹',
      name: 'shortDescription',
      desc: '',
      args: [],
    );
  }

  /// `顯示詳細資料`
  String get showDetails {
    return Intl.message(
      '顯示詳細資料',
      name: 'showDetails',
      desc: '',
      args: [],
    );
  }

  /// `股票`
  String get stock {
    return Intl.message(
      '股票',
      name: 'stock',
      desc: '',
      args: [],
    );
  }

  /// `庫存數量`
  String get stockQuantity {
    return Intl.message(
      '庫存數量',
      name: 'stockQuantity',
      desc: '',
      args: [],
    );
  }

  /// `拍照片`
  String get takePicture {
    return Intl.message(
      '拍照片',
      name: 'takePicture',
      desc: '',
      args: [],
    );
  }

  /// `更新資訊`
  String get updateInfo {
    return Intl.message(
      '更新資訊',
      name: 'updateInfo',
      desc: '',
      args: [],
    );
  }

  /// `更新狀態`
  String get updateStatus {
    return Intl.message(
      '更新狀態',
      name: 'updateStatus',
      desc: '',
      args: [],
    );
  }

  /// `上傳產品`
  String get uploadProduct {
    return Intl.message(
      '上傳產品',
      name: 'uploadProduct',
      desc: '',
      args: [],
    );
  }

  /// `您本月的收入`
  String get yourEarningsThisMonth {
    return Intl.message(
      '您本月的收入',
      name: 'yourEarningsThisMonth',
      desc: '',
      args: [],
    );
  }

  /// `你的命令`
  String get yourOrders {
    return Intl.message(
      '你的命令',
      name: 'yourOrders',
      desc: '',
      args: [],
    );
  }

  /// `持卡人`
  String get cardHolder {
    return Intl.message(
      '持卡人',
      name: 'cardHolder',
      desc: '',
      args: [],
    );
  }

  /// `卡號`
  String get cardNumber {
    return Intl.message(
      '卡號',
      name: 'cardNumber',
      desc: '',
      args: [],
    );
  }

  /// `CVV`
  String get cvv {
    return Intl.message(
      'CVV',
      name: 'cvv',
      desc: '',
      args: [],
    );
  }

  /// `到期日`
  String get expiredDate {
    return Intl.message(
      '到期日',
      name: 'expiredDate',
      desc: '',
      args: [],
    );
  }

  /// `MM / YY`
  String get expiredDateHint {
    return Intl.message(
      'MM / YY',
      name: 'expiredDateHint',
      desc: '',
      args: [],
    );
  }

  /// `必須選擇1項`
  String get mustSelectOneItem {
    return Intl.message(
      '必須選擇1項',
      name: 'mustSelectOneItem',
      desc: '',
      args: [],
    );
  }

  /// `選項總數： {price}`
  String optionsTotal(Object price) {
    return Intl.message(
      '選項總數： $price',
      name: 'optionsTotal',
      desc: '',
      args: [price],
    );
  }

  /// `請選擇必填選項！`
  String get pleaseSelectRequiredOptions {
    return Intl.message(
      '請選擇必填選項！',
      name: 'pleaseSelectRequiredOptions',
      desc: '',
      args: [],
    );
  }

  /// `地點`
  String get location {
    return Intl.message(
      '地點',
      name: 'location',
      desc: '',
      args: [],
    );
  }

  /// `選項`
  String get options {
    return Intl.message(
      '選項',
      name: 'options',
      desc: '',
      args: [],
    );
  }

  /// `牌`
  String get brand {
    return Intl.message(
      '牌',
      name: 'brand',
      desc: '',
      args: [],
    );
  }

  /// `儀錶板`
  String get dashboard {
    return Intl.message(
      '儀錶板',
      name: 'dashboard',
      desc: '',
      args: [],
    );
  }

  /// `編輯：`
  String get edit {
    return Intl.message(
      '編輯：',
      name: 'edit',
      desc: '',
      args: [],
    );
  }

  /// `編輯`
  String get editWithoutColon {
    return Intl.message(
      '編輯',
      name: 'editWithoutColon',
      desc: '',
      args: [],
    );
  }

  /// `該文件太大。請選擇一個較小的檔！`
  String get fileIsTooBig {
    return Intl.message(
      '該文件太大。請選擇一個較小的檔！',
      name: 'fileIsTooBig',
      desc: '',
      args: [],
    );
  }

  /// `檔上傳失敗！`
  String get fileUploadFailed {
    return Intl.message(
      '檔上傳失敗！',
      name: 'fileUploadFailed',
      desc: '',
      args: [],
    );
  }

  /// `檔`
  String get files {
    return Intl.message(
      '檔',
      name: 'files',
      desc: '',
      args: [],
    );
  }

  /// `畫廊`
  String get gallery {
    return Intl.message(
      '畫廊',
      name: 'gallery',
      desc: '',
      args: [],
    );
  }

  /// `您的評論已發送，正在等待批准！`
  String get reviewPendingApproval {
    return Intl.message(
      '您的評論已發送，正在等待批准！',
      name: 'reviewPendingApproval',
      desc: '',
      args: [],
    );
  }

  /// `您的評論已發送！`
  String get reviewSent {
    return Intl.message(
      '您的評論已發送！',
      name: 'reviewSent',
      desc: '',
      args: [],
    );
  }

  /// `選擇檔已取消！`
  String get selectFileCancelled {
    return Intl.message(
      '選擇檔已取消！',
      name: 'selectFileCancelled',
      desc: '',
      args: [],
    );
  }

  /// `發送`
  String get send {
    return Intl.message(
      '發送',
      name: 'send',
      desc: '',
      args: [],
    );
  }

  /// `此功能不支援當前語言`
  String get thisFeatureDoesNotSupportTheCurrentLanguage {
    return Intl.message(
      '此功能不支援當前語言',
      name: 'thisFeatureDoesNotSupportTheCurrentLanguage',
      desc: '',
      args: [],
    );
  }

  /// `{total} 個產品`
  String totalProducts(Object total) {
    return Intl.message(
      '$total 個產品',
      name: 'totalProducts',
      desc: '',
      args: [total],
    );
  }

  /// `在這裡輸入你的消息...`
  String get typeYourMessage {
    return Intl.message(
      '在這裡輸入你的消息...',
      name: 'typeYourMessage',
      desc: '',
      args: [],
    );
  }

  /// `上傳文件`
  String get uploadFile {
    return Intl.message(
      '上傳文件',
      name: 'uploadFile',
      desc: '',
      args: [],
    );
  }

  /// `上傳`
  String get uploading {
    return Intl.message(
      '上傳',
      name: 'uploading',
      desc: '',
      args: [],
    );
  }

  /// `塞爾維亞`
  String get serbian {
    return Intl.message(
      '塞爾維亞',
      name: 'serbian',
      desc: '',
      args: [],
    );
  }

  /// `波蘭語`
  String get Polish {
    return Intl.message(
      '波蘭語',
      name: 'Polish',
      desc: '',
      args: [],
    );
  }

  /// `Persian`
  String get persian {
    return Intl.message(
      'Persian',
      name: 'persian',
      desc: '',
      args: [],
    );
  }

  /// `Kurdish`
  String get kurdish {
    return Intl.message(
      'Kurdish',
      name: 'kurdish',
      desc: '',
      args: [],
    );
  }

  /// `活性`
  String get active {
    return Intl.message(
      '活性',
      name: 'active',
      desc: '',
      args: [],
    );
  }

  /// `添加名稱`
  String get addAName {
    return Intl.message(
      '添加名稱',
      name: 'addAName',
      desc: '',
      args: [],
    );
  }

  /// `添加屬性`
  String get addAnAttr {
    return Intl.message(
      '添加屬性',
      name: 'addAnAttr',
      desc: '',
      args: [],
    );
  }

  /// `添新`
  String get addNew {
    return Intl.message(
      '添新',
      name: 'addNew',
      desc: '',
      args: [],
    );
  }

  /// `您確定要清除購物車嗎？`
  String get confirmClearTheCart {
    return Intl.message(
      '您確定要清除購物車嗎？',
      name: 'confirmClearTheCart',
      desc: '',
      args: [],
    );
  }

  /// `刪除`
  String get delete {
    return Intl.message(
      '刪除',
      name: 'delete',
      desc: '',
      args: [],
    );
  }

  /// `方向`
  String get direction {
    return Intl.message(
      '方向',
      name: 'direction',
      desc: '',
      args: [],
    );
  }

  /// `草案`
  String get draft {
    return Intl.message(
      '草案',
      name: 'draft',
      desc: '',
      args: [],
    );
  }

  /// `您輸入的電子郵件帳戶不存在。請再試一遍。`
  String get emailDoesNotExist {
    return Intl.message(
      '您輸入的電子郵件帳戶不存在。請再試一遍。',
      name: 'emailDoesNotExist',
      desc: '',
      args: [],
    );
  }

  /// `請輸入有效的電子郵寄地址。`
  String get errorEmailFormat {
    return Intl.message(
      '請輸入有效的電子郵寄地址。',
      name: 'errorEmailFormat',
      desc: '',
      args: [],
    );
  }

  /// `請輸入至少8個字元的密碼`
  String get errorPasswordFormat {
    return Intl.message(
      '請輸入至少8個字元的密碼',
      name: 'errorPasswordFormat',
      desc: '',
      args: [],
    );
  }

  /// `分組`
  String get grouped {
    return Intl.message(
      '分組',
      name: 'grouped',
      desc: '',
      args: [],
    );
  }

  /// `保留`
  String get keep {
    return Intl.message(
      '保留',
      name: 'keep',
      desc: '',
      args: [],
    );
  }

  /// `登錄失敗！`
  String get loginFailed {
    return Intl.message(
      '登錄失敗！',
      name: 'loginFailed',
      desc: '',
      args: [],
    );
  }

  /// `您無權使用此應用。`
  String get loginInvalid {
    return Intl.message(
      '您無權使用此應用。',
      name: 'loginInvalid',
      desc: '',
      args: [],
    );
  }

  /// `登錄成功！`
  String get loginSuccess {
    return Intl.message(
      '登錄成功！',
      name: 'loginSuccess',
      desc: '',
      args: [],
    );
  }

  /// `檔案大小上限： {size} MB`
  String maximumFileSizeMb(Object size) {
    return Intl.message(
      '檔案大小上限： $size MB',
      name: 'maximumFileSizeMb',
      desc: '',
      args: [size],
    );
  }

  /// `附近沒有列表！`
  String get noListingNearby {
    return Intl.message(
      '附近沒有列表！',
      name: 'noListingNearby',
      desc: '',
      args: [],
    );
  }

  /// `請先登錄您的帳戶，然後再上傳任何文件。`
  String get pleaseSignInBeforeUploading {
    return Intl.message(
      '請先登錄您的帳戶，然後再上傳任何文件。',
      name: 'pleaseSignInBeforeUploading',
      desc: '',
      args: [],
    );
  }

  /// `私人的`
  String get private {
    return Intl.message(
      '私人的',
      name: 'private',
      desc: '',
      args: [],
    );
  }

  /// `發佈`
  String get publish {
    return Intl.message(
      '發佈',
      name: 'publish',
      desc: '',
      args: [],
    );
  }

  /// `全選`
  String get selectAll {
    return Intl.message(
      '全選',
      name: 'selectAll',
      desc: '',
      args: [],
    );
  }

  /// `選擇無`
  String get selectNone {
    return Intl.message(
      '選擇無',
      name: 'selectNone',
      desc: '',
      args: [],
    );
  }

  /// `簡單`
  String get simple {
    return Intl.message(
      '簡單',
      name: 'simple',
      desc: '',
      args: [],
    );
  }

  /// `變數`
  String get variable {
    return Intl.message(
      '變數',
      name: 'variable',
      desc: '',
      args: [],
    );
  }

  /// `變化`
  String get variation {
    return Intl.message(
      '變化',
      name: 'variation',
      desc: '',
      args: [],
    );
  }

  /// `可見`
  String get visible {
    return Intl.message(
      '可見',
      name: 'visible',
      desc: '',
      args: [],
    );
  }

  /// `孟加拉`
  String get bengali {
    return Intl.message(
      '孟加拉',
      name: 'bengali',
      desc: '',
      args: [],
    );
  }

  /// `烏克蘭`
  String get ukrainian {
    return Intl.message(
      '烏克蘭',
      name: 'ukrainian',
      desc: '',
      args: [],
    );
  }

  /// `打電話給`
  String get callTo {
    return Intl.message(
      '打電話給',
      name: 'callTo',
      desc: '',
      args: [],
    );
  }

  /// `通過Facebook Messenger聊天`
  String get chatViaFacebook {
    return Intl.message(
      '通過Facebook Messenger聊天',
      name: 'chatViaFacebook',
      desc: '',
      args: [],
    );
  }

  /// `通過WhatsApp聊天`
  String get chatViaWhatApp {
    return Intl.message(
      '通過WhatsApp聊天',
      name: 'chatViaWhatApp',
      desc: '',
      args: [],
    );
  }

  /// `與店主聊天`
  String get chatWithStoreOwner {
    return Intl.message(
      '與店主聊天',
      name: 'chatWithStoreOwner',
      desc: '',
      args: [],
    );
  }

  /// `發送消息給`
  String get messageTo {
    return Intl.message(
      '發送消息給',
      name: 'messageTo',
      desc: '',
      args: [],
    );
  }

  /// `標語列表類型`
  String get bannerListType {
    return Intl.message(
      '標語列表類型',
      name: 'bannerListType',
      desc: '',
      args: [],
    );
  }

  /// `橫幅類型`
  String get bannerType {
    return Intl.message(
      '橫幅類型',
      name: 'bannerType',
      desc: '',
      args: [],
    );
  }

  /// `橫幅YouTube網址`
  String get bannerYoutubeURL {
    return Intl.message(
      '橫幅YouTube網址',
      name: 'bannerYoutubeURL',
      desc: '',
      args: [],
    );
  }

  /// `輸入您的電子郵件或用戶名`
  String get enterYourEmailOrUsername {
    return Intl.message(
      '輸入您的電子郵件或用戶名',
      name: 'enterYourEmailOrUsername',
      desc: '',
      args: [],
    );
  }

  /// `輸入您的名字`
  String get enterYourFirstName {
    return Intl.message(
      '輸入您的名字',
      name: 'enterYourFirstName',
      desc: '',
      args: [],
    );
  }

  /// `輸入您的姓氏`
  String get enterYourLastName {
    return Intl.message(
      '輸入您的姓氏',
      name: 'enterYourLastName',
      desc: '',
      args: [],
    );
  }

  /// `輸入你的電話號碼`
  String get enterYourPhoneNumber {
    return Intl.message(
      '輸入你的電話號碼',
      name: 'enterYourPhoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `隱藏關於`
  String get hideAbout {
    return Intl.message(
      '隱藏關於',
      name: 'hideAbout',
      desc: '',
      args: [],
    );
  }

  /// `隱藏地址`
  String get hideAddress {
    return Intl.message(
      '隱藏地址',
      name: 'hideAddress',
      desc: '',
      args: [],
    );
  }

  /// `隱藏電子郵件`
  String get hideEmail {
    return Intl.message(
      '隱藏電子郵件',
      name: 'hideEmail',
      desc: '',
      args: [],
    );
  }

  /// `隱藏地圖`
  String get hideMap {
    return Intl.message(
      '隱藏地圖',
      name: 'hideMap',
      desc: '',
      args: [],
    );
  }

  /// `隱藏電話`
  String get hidePhone {
    return Intl.message(
      '隱藏電話',
      name: 'hidePhone',
      desc: '',
      args: [],
    );
  }

  /// `隱藏政策`
  String get hidePolicy {
    return Intl.message(
      '隱藏政策',
      name: 'hidePolicy',
      desc: '',
      args: [],
    );
  }

  /// `連結`
  String get link {
    return Intl.message(
      '連結',
      name: 'link',
      desc: '',
      args: [],
    );
  }

  /// `列出橫幅廣告類型`
  String get listBannerType {
    return Intl.message(
      '列出橫幅廣告類型',
      name: 'listBannerType',
      desc: '',
      args: [],
    );
  }

  /// `列出橫幅視頻`
  String get listBannerVideo {
    return Intl.message(
      '列出橫幅視頻',
      name: 'listBannerVideo',
      desc: '',
      args: [],
    );
  }

  /// `店鋪電子郵件`
  String get shopEmail {
    return Intl.message(
      '店鋪電子郵件',
      name: 'shopEmail',
      desc: '',
      args: [],
    );
  }

  /// `店鋪名稱`
  String get shopName {
    return Intl.message(
      '店鋪名稱',
      name: 'shopName',
      desc: '',
      args: [],
    );
  }

  /// `店鋪電話`
  String get shopPhone {
    return Intl.message(
      '店鋪電話',
      name: 'shopPhone',
      desc: '',
      args: [],
    );
  }

  /// `商店`
  String get shopSlug {
    return Intl.message(
      '商店',
      name: 'shopSlug',
      desc: '',
      args: [],
    );
  }

  /// `商店清單橫幅`
  String get storeListBanner {
    return Intl.message(
      '商店清單橫幅',
      name: 'storeListBanner',
      desc: '',
      args: [],
    );
  }

  /// `店鋪標誌`
  String get storeLogo {
    return Intl.message(
      '店鋪標誌',
      name: 'storeLogo',
      desc: '',
      args: [],
    );
  }

  /// `商店移動橫幅`
  String get storeMobileBanner {
    return Intl.message(
      '商店移動橫幅',
      name: 'storeMobileBanner',
      desc: '',
      args: [],
    );
  }

  /// `商店設置`
  String get storeSettings {
    return Intl.message(
      '商店設置',
      name: 'storeSettings',
      desc: '',
      args: [],
    );
  }

  /// `商店滑塊橫幅`
  String get storeSliderBanner {
    return Intl.message(
      '商店滑塊橫幅',
      name: 'storeSliderBanner',
      desc: '',
      args: [],
    );
  }

  /// `存儲靜態橫幅`
  String get storeStaticBanner {
    return Intl.message(
      '存儲靜態橫幅',
      name: 'storeStaticBanner',
      desc: '',
      args: [],
    );
  }

  /// `街`
  String get street {
    return Intl.message(
      '街',
      name: 'street',
      desc: '',
      args: [],
    );
  }

  /// `2街`
  String get street2 {
    return Intl.message(
      '2街',
      name: 'street2',
      desc: '',
      args: [],
    );
  }

  /// `更新失敗！`
  String get updateFailed {
    return Intl.message(
      '更新失敗！',
      name: 'updateFailed',
      desc: '',
      args: [],
    );
  }

  /// `更新成功！`
  String get updateSuccess {
    return Intl.message(
      '更新成功！',
      name: 'updateSuccess',
      desc: '',
      args: [],
    );
  }

  /// `周 {week}`
  String week(Object week) {
    return Intl.message(
      '周 $week',
      name: 'week',
      desc: '',
      args: [week],
    );
  }

  /// `添加sl`
  String get addASlug {
    return Intl.message(
      '添加sl',
      name: 'addASlug',
      desc: '',
      args: [],
    );
  }

  /// `確認`
  String get confirm {
    return Intl.message(
      '確認',
      name: 'confirm',
      desc: '',
      args: [],
    );
  }

  /// `您確定要刪除此專案嗎？`
  String get confirmDeleteItem {
    return Intl.message(
      '您確定要刪除此專案嗎？',
      name: 'confirmDeleteItem',
      desc: '',
      args: [],
    );
  }

  /// `標記為已讀`
  String get markAsRead {
    return Intl.message(
      '標記為已讀',
      name: 'markAsRead',
      desc: '',
      args: [],
    );
  }

  /// `標記為未讀`
  String get markAsUnread {
    return Intl.message(
      '標記為未讀',
      name: 'markAsUnread',
      desc: '',
      args: [],
    );
  }

  /// `訂單退款請求失敗`
  String get refundOrderFailed {
    return Intl.message(
      '訂單退款請求失敗',
      name: 'refundOrderFailed',
      desc: '',
      args: [],
    );
  }

  /// `成功申請退款！`
  String get refundOrderSuccess {
    return Intl.message(
      '成功申請退款！',
      name: 'refundOrderSuccess',
      desc: '',
      args: [],
    );
  }

  /// `沒有要下載的檔。`
  String get noFileToDownload {
    return Intl.message(
      '沒有要下載的檔。',
      name: 'noFileToDownload',
      desc: '',
      args: [],
    );
  }

  /// `取消逆轉`
  String get orderStatusCanceledReversal {
    return Intl.message(
      '取消逆轉',
      name: 'orderStatusCanceledReversal',
      desc: '',
      args: [],
    );
  }

  /// `退款`
  String get orderStatusChargeBack {
    return Intl.message(
      '退款',
      name: 'orderStatusChargeBack',
      desc: '',
      args: [],
    );
  }

  /// `被拒絕`
  String get orderStatusDenied {
    return Intl.message(
      '被拒絕',
      name: 'orderStatusDenied',
      desc: '',
      args: [],
    );
  }

  /// `過期`
  String get orderStatusExpired {
    return Intl.message(
      '過期',
      name: 'orderStatusExpired',
      desc: '',
      args: [],
    );
  }

  /// `處理`
  String get orderStatusProcessed {
    return Intl.message(
      '處理',
      name: 'orderStatusProcessed',
      desc: '',
      args: [],
    );
  }

  /// `反轉`
  String get orderStatusReversed {
    return Intl.message(
      '反轉',
      name: 'orderStatusReversed',
      desc: '',
      args: [],
    );
  }

  /// `已出貨`
  String get orderStatusShipped {
    return Intl.message(
      '已出貨',
      name: 'orderStatusShipped',
      desc: '',
      args: [],
    );
  }

  /// `虛空`
  String get orderStatusVoided {
    return Intl.message(
      '虛空',
      name: 'orderStatusVoided',
      desc: '',
      args: [],
    );
  }

  /// `已分配`
  String get assigned {
    return Intl.message(
      '已分配',
      name: 'assigned',
      desc: '',
      args: [],
    );
  }

  /// `呼叫`
  String get call {
    return Intl.message(
      '呼叫',
      name: 'call',
      desc: '',
      args: [],
    );
  }

  /// `聊`
  String get chat {
    return Intl.message(
      '聊',
      name: 'chat',
      desc: '',
      args: [],
    );
  }

  /// `客戶詳情`
  String get customerDetail {
    return Intl.message(
      '客戶詳情',
      name: 'customerDetail',
      desc: '',
      args: [],
    );
  }

  /// `交付`
  String get delivered {
    return Intl.message(
      '交付',
      name: 'delivered',
      desc: '',
      args: [],
    );
  }

  /// `交貨細節`
  String get deliveryDetails {
    return Intl.message(
      '交貨細節',
      name: 'deliveryDetails',
      desc: '',
      args: [],
    );
  }

  /// `全名`
  String get fullName {
    return Intl.message(
      '全名',
      name: 'fullName',
      desc: '',
      args: [],
    );
  }

  /// `密碼錯誤`
  String get incorrectPassword {
    return Intl.message(
      '密碼錯誤',
      name: 'incorrectPassword',
      desc: '',
      args: [],
    );
  }

  /// `標記為已發貨`
  String get markAsShipped {
    return Intl.message(
      '標記為已發貨',
      name: 'markAsShipped',
      desc: '',
      args: [],
    );
  }

  /// `您的產品將在審核後顯示。`
  String get productCreateReview {
    return Intl.message(
      '您的產品將在審核後顯示。',
      name: 'productCreateReview',
      desc: '',
      args: [],
    );
  }

  /// `已出貨`
  String get shipped {
    return Intl.message(
      '已出貨',
      name: 'shipped',
      desc: '',
      args: [],
    );
  }

  /// `儲存資訊`
  String get storeInformation {
    return Intl.message(
      '儲存資訊',
      name: 'storeInformation',
      desc: '',
      args: [],
    );
  }

  /// `更新密碼`
  String get updatePassword {
    return Intl.message(
      '更新密碼',
      name: 'updatePassword',
      desc: '',
      args: [],
    );
  }

  /// `日期降冪`
  String get DateDESC {
    return Intl.message(
      '日期降冪',
      name: 'DateDESC',
      desc: '',
      args: [],
    );
  }

  /// `添加新帖子`
  String get addANewPost {
    return Intl.message(
      '添加新帖子',
      name: 'addANewPost',
      desc: '',
      args: [],
    );
  }

  /// `添加新博客`
  String get addNewBlog {
    return Intl.message(
      '添加新博客',
      name: 'addNewBlog',
      desc: '',
      args: [],
    );
  }

  /// `建立新訊息`
  String get addNewPost {
    return Intl.message(
      '建立新訊息',
      name: 'addNewPost',
      desc: '',
      args: [],
    );
  }

  /// `檢測到音訊項目。您想添加到音訊播放機嗎？`
  String get audioDetected {
    return Intl.message(
      '檢測到音訊項目。您想添加到音訊播放機嗎？',
      name: 'audioDetected',
      desc: '',
      args: [],
    );
  }

  /// `評論成功，請等待您的評論被批准`
  String get commentSuccessfully {
    return Intl.message(
      '評論成功，請等待您的評論被批准',
      name: 'commentSuccessfully',
      desc: '',
      args: [],
    );
  }

  /// `內容`
  String get content {
    return Intl.message(
      '內容',
      name: 'content',
      desc: '',
      args: [],
    );
  }

  /// `您的帖子已成功創建為草稿。請查看您的管理網站。`
  String get createNewPostSuccessfully {
    return Intl.message(
      '您的帖子已成功創建為草稿。請查看您的管理網站。',
      name: 'createNewPostSuccessfully',
      desc: '',
      args: [],
    );
  }

  /// `建立訊息`
  String get createPost {
    return Intl.message(
      '建立訊息',
      name: 'createPost',
      desc: '',
      args: [],
    );
  }

  /// `日期昇冪`
  String get dateASC {
    return Intl.message(
      '日期昇冪',
      name: 'dateASC',
      desc: '',
      args: [],
    );
  }

  /// `荷蘭人`
  String get dutch {
    return Intl.message(
      '荷蘭人',
      name: 'dutch',
      desc: '',
      args: [],
    );
  }

  /// `您的評論不能為空`
  String get emptyComment {
    return Intl.message(
      '您的評論不能為空',
      name: 'emptyComment',
      desc: '',
      args: [],
    );
  }

  /// `您尚未搜索商品。現在開始-我們會為您提供幫助。`
  String get emptySearch {
    return Intl.message(
      '您尚未搜索商品。現在開始-我們會為您提供幫助。',
      name: 'emptySearch',
      desc: '',
      args: [],
    );
  }

  /// `印地語`
  String get hindi {
    return Intl.message(
      '印地語',
      name: 'hindi',
      desc: '',
      args: [],
    );
  }

  /// `圖片特徵`
  String get imageFeature {
    return Intl.message(
      '圖片特徵',
      name: 'imageFeature',
      desc: '',
      args: [],
    );
  }

  /// `朝鮮的`
  String get korean {
    return Intl.message(
      '朝鮮的',
      name: 'korean',
      desc: '',
      args: [],
    );
  }

  /// `請登錄後發表評論`
  String get loginToComment {
    return Intl.message(
      '請登錄後發表評論',
      name: 'loginToComment',
      desc: '',
      args: [],
    );
  }

  /// `剛才`
  String get momentAgo {
    return Intl.message(
      '剛才',
      name: 'momentAgo',
      desc: '',
      args: [],
    );
  }

  /// `{month} 個月前`
  String monthsAgo(Object month) {
    return Intl.message(
      '$month 個月前',
      name: 'monthsAgo',
      desc: '',
      args: [month],
    );
  }

  /// `頁面預覽`
  String get pageView {
    return Intl.message(
      '頁面預覽',
      name: 'pageView',
      desc: '',
      args: [],
    );
  }

  /// `內容`
  String get postContent {
    return Intl.message(
      '內容',
      name: 'postContent',
      desc: '',
      args: [],
    );
  }

  /// `您的帖子創建失敗`
  String get postFail {
    return Intl.message(
      '您的帖子創建失敗',
      name: 'postFail',
      desc: '',
      args: [],
    );
  }

  /// `圖片特徵`
  String get postImageFeature {
    return Intl.message(
      '圖片特徵',
      name: 'postImageFeature',
      desc: '',
      args: [],
    );
  }

  /// `郵政管理`
  String get postManagement {
    return Intl.message(
      '郵政管理',
      name: 'postManagement',
      desc: '',
      args: [],
    );
  }

  /// `您的帖子已成功創建`
  String get postSuccessfully {
    return Intl.message(
      '您的帖子已成功創建',
      name: 'postSuccessfully',
      desc: '',
      args: [],
    );
  }

  /// `標題`
  String get postTitle {
    return Intl.message(
      '標題',
      name: 'postTitle',
      desc: '',
      args: [],
    );
  }

  /// `隱私政策`
  String get privacyPolicy {
    return Intl.message(
      '隱私政策',
      name: 'privacyPolicy',
      desc: '',
      args: [],
    );
  }

  /// `你可能喜歡的東西`
  String get relatedLayoutTitle {
    return Intl.message(
      '你可能喜歡的東西',
      name: 'relatedLayoutTitle',
      desc: '',
      args: [],
    );
  }

  /// `開始探索`
  String get startExploring {
    return Intl.message(
      '開始探索',
      name: 'startExploring',
      desc: '',
      args: [],
    );
  }

  /// `提交`
  String get submit {
    return Intl.message(
      '提交',
      name: 'submit',
      desc: '',
      args: [],
    );
  }

  /// `提交您的帖子`
  String get submitYourPost {
    return Intl.message(
      '提交您的帖子',
      name: 'submitYourPost',
      desc: '',
      args: [],
    );
  }

  /// `標題`
  String get title {
    return Intl.message(
      '標題',
      name: 'title',
      desc: '',
      args: [],
    );
  }

  /// `我們找到了博客`
  String get weFoundBlogs {
    return Intl.message(
      '我們找到了博客',
      name: 'weFoundBlogs',
      desc: '',
      args: [],
    );
  }

  /// `網頁流覽`
  String get webView {
    return Intl.message(
      '網頁流覽',
      name: 'webView',
      desc: '',
      args: [],
    );
  }

  /// `{year} 年前`
  String yearsAgo(Object year) {
    return Intl.message(
      '$year 年前',
      name: 'yearsAgo',
      desc: '',
      args: [year],
    );
  }

  /// `所有訂單`
  String get allDeliveryOrders {
    return Intl.message(
      '所有訂單',
      name: 'allDeliveryOrders',
      desc: '',
      args: [],
    );
  }

  /// `打開地圖`
  String get openMap {
    return Intl.message(
      '打開地圖',
      name: 'openMap',
      desc: '',
      args: [],
    );
  }

  /// `查看訂單`
  String get seeOrder {
    return Intl.message(
      '查看訂單',
      name: 'seeOrder',
      desc: '',
      args: [],
    );
  }

  /// `訂購摘要`
  String get orderSummary {
    return Intl.message(
      '訂購摘要',
      name: 'orderSummary',
      desc: '',
      args: [],
    );
  }

  /// `訂購須知`
  String get note {
    return Intl.message(
      '訂購須知',
      name: 'note',
      desc: '',
      args: [],
    );
  }

  /// `訂單ID`
  String get orderIdWithoutColon {
    return Intl.message(
      '訂單ID',
      name: 'orderIdWithoutColon',
      desc: '',
      args: [],
    );
  }

  /// `用名字搜索...`
  String get searchByName {
    return Intl.message(
      '用名字搜索...',
      name: 'searchByName',
      desc: '',
      args: [],
    );
  }

  /// `沒有資料。\n此訂單已被刪除。`
  String get deliveryNotificationError {
    return Intl.message(
      '沒有資料。\n此訂單已被刪除。',
      name: 'deliveryNotificationError',
      desc: '',
      args: [],
    );
  }

  /// `交貨`
  String get deliveryManagement {
    return Intl.message(
      '交貨',
      name: 'deliveryManagement',
      desc: '',
      args: [],
    );
  }

  /// `送貨員：`
  String get deliveryBoy {
    return Intl.message(
      '送貨員：',
      name: 'deliveryBoy',
      desc: '',
      args: [],
    );
  }

  /// `泰米爾語`
  String get Tamil {
    return Intl.message(
      '泰米爾語',
      name: 'Tamil',
      desc: '',
      args: [],
    );
  }

  /// `帳戶設置`
  String get accountSetup {
    return Intl.message(
      '帳戶設置',
      name: 'accountSetup',
      desc: '',
      args: [],
    );
  }

  /// `已超過最大數量`
  String get addToCartMaximum {
    return Intl.message(
      '已超過最大數量',
      name: 'addToCartMaximum',
      desc: '',
      args: [],
    );
  }

  /// `至少3個字元...`
  String get atLeastThreeCharacters {
    return Intl.message(
      '至少3個字元...',
      name: 'atLeastThreeCharacters',
      desc: '',
      args: [],
    );
  }

  /// `不允許使用過去的日期`
  String get cantPickDateInThePast {
    return Intl.message(
      '不允許使用過去的日期',
      name: 'cantPickDateInThePast',
      desc: '',
      args: [],
    );
  }

  /// `選擇計畫`
  String get choosePlan {
    return Intl.message(
      '選擇計畫',
      name: 'choosePlan',
      desc: '',
      args: [],
    );
  }

  /// `現已關閉`
  String get closeNow {
    return Intl.message(
      '現已關閉',
      name: 'closeNow',
      desc: '',
      args: [],
    );
  }

  /// `評論`
  String get comment {
    return Intl.message(
      '評論',
      name: 'comment',
      desc: '',
      args: [],
    );
  }

  /// `客戶須知`
  String get customerNote {
    return Intl.message(
      '客戶須知',
      name: 'customerNote',
      desc: '',
      args: [],
    );
  }

  /// `日期明智關閉`
  String get dateWiseClose {
    return Intl.message(
      '日期明智關閉',
      name: 'dateWiseClose',
      desc: '',
      args: [],
    );
  }

  /// `郵寄日期`
  String get deliveryDate {
    return Intl.message(
      '郵寄日期',
      name: 'deliveryDate',
      desc: '',
      args: [],
    );
  }

  /// `禁止購買`
  String get disablePurchase {
    return Intl.message(
      '禁止購買',
      name: 'disablePurchase',
      desc: '',
      args: [],
    );
  }

  /// `~{total} 公里`
  String distance(Object total) {
    return Intl.message(
      '~$total 公里',
      name: 'distance',
      desc: '',
      args: [total],
    );
  }

  /// `開啟假期模式`
  String get enableVacationMode {
    return Intl.message(
      '開啟假期模式',
      name: 'enableVacationMode',
      desc: '',
      args: [],
    );
  }

  /// `請選擇第一次約會之後的日期`
  String get endDateCantBeAfterFirstDate {
    return Intl.message(
      '請選擇第一次約會之後的日期',
      name: 'endDateCantBeAfterFirstDate',
      desc: '',
      args: [],
    );
  }

  /// `請輸入您的手機號碼`
  String get enterYourMobile {
    return Intl.message(
      '請輸入您的手機號碼',
      name: 'enterYourMobile',
      desc: '',
      args: [],
    );
  }

  /// `成為第一個評論這篇文章的人！`
  String get firstComment {
    return Intl.message(
      '成為第一個評論這篇文章的人！',
      name: 'firstComment',
      desc: '',
      args: [],
    );
  }

  /// `第一次續訂`
  String get firstRenewal {
    return Intl.message(
      '第一次續訂',
      name: 'firstRenewal',
      desc: '',
      args: [],
    );
  }

  /// `>{total} 公里`
  String greaterDistance(Object total) {
    return Intl.message(
      '>$total 公里',
      name: 'greaterDistance',
      desc: '',
      args: [total],
    );
  }

  /// `立即關閉`
  String get instantlyClose {
    return Intl.message(
      '立即關閉',
      name: 'instantlyClose',
      desc: '',
      args: [],
    );
  }

  /// `無效的電話號碼`
  String get invalidPhoneNumber {
    return Intl.message(
      '無效的電話號碼',
      name: 'invalidPhoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `最新產品`
  String get latestProducts {
    return Intl.message(
      '最新產品',
      name: 'latestProducts',
      desc: '',
      args: [],
    );
  }

  /// `手機驗證`
  String get mobileVerification {
    return Intl.message(
      '手機驗證',
      name: 'mobileVerification',
      desc: '',
      args: [],
    );
  }

  /// `現在開門了`
  String get openNow {
    return Intl.message(
      '現在開門了',
      name: 'openNow',
      desc: '',
      args: [],
    );
  }

  /// `付費`
  String get paid {
    return Intl.message(
      '付費',
      name: 'paid',
      desc: '',
      args: [],
    );
  }

  /// `已付費狀態`
  String get paidStatus {
    return Intl.message(
      '已付費狀態',
      name: 'paidStatus',
      desc: '',
      args: [],
    );
  }

  /// `電話是空的`
  String get phoneEmpty {
    return Intl.message(
      '電話是空的',
      name: 'phoneEmpty',
      desc: '',
      args: [],
    );
  }

  /// `格式：+84123456789`
  String get phoneHintFormat {
    return Intl.message(
      '格式：+84123456789',
      name: 'phoneHintFormat',
      desc: '',
      args: [],
    );
  }

  /// `全部播放`
  String get playAll {
    return Intl.message(
      '全部播放',
      name: 'playAll',
      desc: '',
      args: [],
    );
  }

  /// `流行`
  String get popular {
    return Intl.message(
      '流行',
      name: 'popular',
      desc: '',
      args: [],
    );
  }

  /// `推薦的`
  String get recommended {
    return Intl.message(
      '推薦的',
      name: 'recommended',
      desc: '',
      args: [],
    );
  }

  /// `經常性總計`
  String get recurringTotals {
    return Intl.message(
      '經常性總計',
      name: 'recurringTotals',
      desc: '',
      args: [],
    );
  }

  /// `註冊失敗`
  String get registerFailed {
    return Intl.message(
      '註冊失敗',
      name: 'registerFailed',
      desc: '',
      args: [],
    );
  }

  /// `註冊成功`
  String get registerSuccess {
    return Intl.message(
      '註冊成功',
      name: 'registerSuccess',
      desc: '',
      args: [],
    );
  }

  /// `您在短時間內請求了太多代碼。請稍後再試。`
  String get requestTooMany {
    return Intl.message(
      '您在短時間內請求了太多代碼。請稍後再試。',
      name: 'requestTooMany',
      desc: '',
      args: [],
    );
  }

  /// `查看評論`
  String get seeReviews {
    return Intl.message(
      '查看評論',
      name: 'seeReviews',
      desc: '',
      args: [],
    );
  }

  /// `選擇日期`
  String get selectDates {
    return Intl.message(
      '選擇日期',
      name: 'selectDates',
      desc: '',
      args: [],
    );
  }

  /// `停止`
  String get stop {
    return Intl.message(
      '停止',
      name: 'stop',
      desc: '',
      args: [],
    );
  }

  /// `這家店現在關門了`
  String get storeClosed {
    return Intl.message(
      '這家店現在關門了',
      name: 'storeClosed',
      desc: '',
      args: [],
    );
  }

  /// `商店假期`
  String get storeVacation {
    return Intl.message(
      '商店假期',
      name: 'storeVacation',
      desc: '',
      args: [],
    );
  }

  /// `未付`
  String get unpaid {
    return Intl.message(
      '未付',
      name: 'unpaid',
      desc: '',
      args: [],
    );
  }

  /// `此用戶名/電子郵件不可用。`
  String get userExists {
    return Intl.message(
      '此用戶名/電子郵件不可用。',
      name: 'userExists',
      desc: '',
      args: [],
    );
  }

  /// `假期留言`
  String get vacationMessage {
    return Intl.message(
      '假期留言',
      name: 'vacationMessage',
      desc: '',
      args: [],
    );
  }

  /// `假期類型`
  String get vacationType {
    return Intl.message(
      '假期類型',
      name: 'vacationType',
      desc: '',
      args: [],
    );
  }

  /// `驗證碼（6位）`
  String get verificationCode {
    return Intl.message(
      '驗證碼（6位）',
      name: 'verificationCode',
      desc: '',
      args: [],
    );
  }

  /// `您只能從一家商店購買。`
  String get youCanOnlyOrderSingleStore {
    return Intl.message(
      '您只能從一家商店購買。',
      name: 'youCanOnlyOrderSingleStore',
      desc: '',
      args: [],
    );
  }

  /// `您已指定訂單 #{total}`
  String youHaveAssignedToOrder(Object total) {
    return Intl.message(
      '您已指定訂單 #$total',
      name: 'youHaveAssignedToOrder',
      desc: '',
      args: [total],
    );
  }

  /// `完成後下次不會再問你`
  String get youNotBeAsked {
    return Intl.message(
      '完成後下次不會再問你',
      name: 'youNotBeAsked',
      desc: '',
      args: [],
    );
  }

  /// `假期中`
  String get onVacation {
    return Intl.message(
      '假期中',
      name: 'onVacation',
      desc: '',
      args: [],
    );
  }

  /// `量`
  String get amount {
    return Intl.message(
      '量',
      name: 'amount',
      desc: '',
      args: [],
    );
  }

  /// `返回錢包`
  String get backToWallet {
    return Intl.message(
      '返回錢包',
      name: 'backToWallet',
      desc: '',
      args: [],
    );
  }

  /// `平衡`
  String get balance {
    return Intl.message(
      '平衡',
      name: 'balance',
      desc: '',
      args: [],
    );
  }

  /// `選擇類別`
  String get chooseCategory {
    return Intl.message(
      '選擇類別',
      name: 'chooseCategory',
      desc: '',
      args: [],
    );
  }

  /// `選擇類型`
  String get chooseType {
    return Intl.message(
      '選擇類型',
      name: 'chooseType',
      desc: '',
      args: [],
    );
  }

  /// `充值時將清空購物車。`
  String get confirmClearCartWhenTopUp {
    return Intl.message(
      '充值時將清空購物車。',
      name: 'confirmClearCartWhenTopUp',
      desc: '',
      args: [],
    );
  }

  /// `捷克`
  String get czech {
    return Intl.message(
      '捷克',
      name: 'czech',
      desc: '',
      args: [],
    );
  }

  /// `您還沒有任何交易`
  String get doNotAnyTransactions {
    return Intl.message(
      '您還沒有任何交易',
      name: 'doNotAnyTransactions',
      desc: '',
      args: [],
    );
  }

  /// `輸入的金額大於當前錢包金額。請再試一次！`
  String get errorAmountTransfer {
    return Intl.message(
      '輸入的金額大於當前錢包金額。請再試一次！',
      name: 'errorAmountTransfer',
      desc: '',
      args: [],
    );
  }

  /// `外部`
  String get external {
    return Intl.message(
      '外部',
      name: 'external',
      desc: '',
      args: [],
    );
  }

  /// `歷史`
  String get historyTransaction {
    return Intl.message(
      '歷史',
      name: 'historyTransaction',
      desc: '',
      args: [],
    );
  }

  /// `上次交易`
  String get lastTransactions {
    return Intl.message(
      '上次交易',
      name: 'lastTransactions',
      desc: '',
      args: [],
    );
  }

  /// `我的錢包`
  String get myWallet {
    return Intl.message(
      '我的錢包',
      name: 'myWallet',
      desc: '',
      args: [],
    );
  }

  /// `注意（可選）`
  String get noteTransfer {
    return Intl.message(
      '注意（可選）',
      name: 'noteTransfer',
      desc: '',
      args: [],
    );
  }

  /// `用錢包支付`
  String get payByWallet {
    return Intl.message(
      '用錢包支付',
      name: 'payByWallet',
      desc: '',
      args: [],
    );
  }

  /// `請加價`
  String get pleaseAddPrice {
    return Intl.message(
      '請加價',
      name: 'pleaseAddPrice',
      desc: '',
      args: [],
    );
  }

  /// `請選擇類別`
  String get pleaseChooseCategory {
    return Intl.message(
      '請選擇類別',
      name: 'pleaseChooseCategory',
      desc: '',
      args: [],
    );
  }

  /// `請輸入產品名稱`
  String get pleaseEnterProductName {
    return Intl.message(
      '請輸入產品名稱',
      name: 'pleaseEnterProductName',
      desc: '',
      args: [],
    );
  }

  /// `要求退款`
  String get refundRequested {
    return Intl.message(
      '要求退款',
      name: 'refundRequested',
      desc: '',
      args: [],
    );
  }

  /// `充值`
  String get topUp {
    return Intl.message(
      '充值',
      name: 'topUp',
      desc: '',
      args: [],
    );
  }

  /// `未找到充值產品`
  String get topUpProductNotFound {
    return Intl.message(
      '未找到充值產品',
      name: 'topUpProductNotFound',
      desc: '',
      args: [],
    );
  }

  /// `交易結果`
  String get transactionResult {
    return Intl.message(
      '交易結果',
      name: 'transactionResult',
      desc: '',
      args: [],
    );
  }

  /// `傳遞`
  String get transfer {
    return Intl.message(
      '傳遞',
      name: 'transfer',
      desc: '',
      args: [],
    );
  }

  /// `轉移確認`
  String get transferConfirm {
    return Intl.message(
      '轉移確認',
      name: 'transferConfirm',
      desc: '',
      args: [],
    );
  }

  /// `你不能轉移給這個用戶`
  String get transferErrorMessage {
    return Intl.message(
      '你不能轉移給這個用戶',
      name: 'transferErrorMessage',
      desc: '',
      args: [],
    );
  }

  /// `傳輸失敗`
  String get transferFailed {
    return Intl.message(
      '傳輸失敗',
      name: 'transferFailed',
      desc: '',
      args: [],
    );
  }

  /// `轉移成功`
  String get transferSuccess {
    return Intl.message(
      '轉移成功',
      name: 'transferSuccess',
      desc: '',
      args: [],
    );
  }

  /// `通過錢包`
  String get viaWallet {
    return Intl.message(
      '通過錢包',
      name: 'viaWallet',
      desc: '',
      args: [],
    );
  }

  /// `查看最近的交易`
  String get viewRecentTransactions {
    return Intl.message(
      '查看最近的交易',
      name: 'viewRecentTransactions',
      desc: '',
      args: [],
    );
  }

  /// `當前選擇的貨幣不適用於電子錢包功能，請將其更改為 {default_currency}`
  String warningCurrencyMessageForWallet(Object default_currency) {
    return Intl.message(
      '當前選擇的貨幣不適用於電子錢包功能，請將其更改為 $default_currency',
      name: 'warningCurrencyMessageForWallet',
      desc: '',
      args: [default_currency],
    );
  }

  /// `已被刪除`
  String get hasBeenDeleted {
    return Intl.message(
      '已被刪除',
      name: 'hasBeenDeleted',
      desc: '',
      args: [],
    );
  }

  /// `Maximum discount point`
  String get pointMsgMaximumDiscountPoint {
    return Intl.message(
      'Maximum discount point',
      name: 'pointMsgMaximumDiscountPoint',
      desc: '',
      args: [],
    );
  }

  /// `You have reach maximum discount point`
  String get pointMsgOverMaximumDiscountPoint {
    return Intl.message(
      'You have reach maximum discount point',
      name: 'pointMsgOverMaximumDiscountPoint',
      desc: '',
      args: [],
    );
  }

  /// `Discount point is applied successfully`
  String get pointMsgSuccess {
    return Intl.message(
      'Discount point is applied successfully',
      name: 'pointMsgSuccess',
      desc: '',
      args: [],
    );
  }

  /// `Discount point is removed`
  String get pointMsgRemove {
    return Intl.message(
      'Discount point is removed',
      name: 'pointMsgRemove',
      desc: '',
      args: [],
    );
  }

  /// `Please enter discount point`
  String get pointMsgEnter {
    return Intl.message(
      'Please enter discount point',
      name: 'pointMsgEnter',
      desc: '',
      args: [],
    );
  }

  /// `The total discount value is over the bill  total`
  String get pointMsgOverTotalBill {
    return Intl.message(
      'The total discount value is over the bill  total',
      name: 'pointMsgOverTotalBill',
      desc: '',
      args: [],
    );
  }

  /// `There is no discount point configuration has been found in server`
  String get pointMsgConfigNotFound {
    return Intl.message(
      'There is no discount point configuration has been found in server',
      name: 'pointMsgConfigNotFound',
      desc: '',
      args: [],
    );
  }

  /// `You don't have enough discount point. Your total discount point is`
  String get pointMsgNotEnough {
    return Intl.message(
      'You don\'t have enough discount point. Your total discount point is',
      name: 'pointMsgNotEnough',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'ar'),
      Locale.fromSubtags(languageCode: 'bn'),
      Locale.fromSubtags(languageCode: 'cs'),
      Locale.fromSubtags(languageCode: 'de'),
      Locale.fromSubtags(languageCode: 'el'),
      Locale.fromSubtags(languageCode: 'es'),
      Locale.fromSubtags(languageCode: 'fa'),
      Locale.fromSubtags(languageCode: 'fi'),
      Locale.fromSubtags(languageCode: 'fr'),
      Locale.fromSubtags(languageCode: 'he'),
      Locale.fromSubtags(languageCode: 'hi'),
      Locale.fromSubtags(languageCode: 'hu'),
      Locale.fromSubtags(languageCode: 'id'),
      Locale.fromSubtags(languageCode: 'it'),
      Locale.fromSubtags(languageCode: 'ja'),
      Locale.fromSubtags(languageCode: 'ko'),
      Locale.fromSubtags(languageCode: 'ku'),
      Locale.fromSubtags(languageCode: 'nl'),
      Locale.fromSubtags(languageCode: 'pl'),
      Locale.fromSubtags(languageCode: 'pt'),
      Locale.fromSubtags(languageCode: 'ro'),
      Locale.fromSubtags(languageCode: 'ru'),
      Locale.fromSubtags(languageCode: 'sr'),
      Locale.fromSubtags(languageCode: 'sv'),
      Locale.fromSubtags(languageCode: 'ta'),
      Locale.fromSubtags(languageCode: 'th'),
      Locale.fromSubtags(languageCode: 'tr'),
      Locale.fromSubtags(languageCode: 'uk'),
      Locale.fromSubtags(languageCode: 'vi'),
      Locale.fromSubtags(languageCode: 'zh'),
      Locale.fromSubtags(languageCode: 'zh', countryCode: 'TW'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
