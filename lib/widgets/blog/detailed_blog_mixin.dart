import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart'
    as core;
import 'package:fwfh_webview/fwfh_webview.dart' as fwf;
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_android/webview_flutter_android.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../models/entities/blog.dart';
import '../../models/index.dart';
import '../../models/text_style_model.dart';
import '../../services/service_config.dart';
import '../../services/services.dart';
import '../../widgets/html/index.dart';
import 'blog_heart_button.dart';
import 'blog_share_button.dart';
import 'text_adjustment_button.dart';

mixin DetailedBlogMixin<T extends StatefulWidget> on State<T> {
  Widget renderBlogFunctionButtons(Blog blog) => Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _renderTextAdjustmentButton(),
          _renderShareButton(blog),
          _renderHeartButton(blog),
        ],
      );

  Widget renderRelatedBlog(dynamic blogId) =>
      Services().widget.renderRelatedBlog(
            categoryId: blogId,
            type: kAdvanceConfig['DetailedBlogLayout'],
          );

  Widget renderCommentLayout(dynamic blogId) => kBlogDetail['showComment']
      ? Services()
          .widget
          .renderCommentLayout(blogId, kAdvanceConfig['DetailedBlogLayout'])
      : const SizedBox();

  Widget renderCommentInput(dynamic blogId) => kBlogDetail['showComment']
      ? Services().widget.renderCommentField(blogId)
      : const SizedBox();

  Widget renderInstagram(instagramLink) {
    late final PlatformWebViewControllerCreationParams params;
    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(
        allowsInlineMediaPlayback: true,
      );
    } else {
      params = const PlatformWebViewControllerCreationParams();
    }
    controller = WebViewController.fromPlatformCreationParams(params)
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..loadRequest(
        Uri.parse(instagramLink),
      );
    if (isAndroid) {
      return Container(
        height: MediaQuery.of(context).size.height * 0.7,
        constraints: BoxConstraints(
          minHeight: MediaQuery.of(context).size.height * 0.5,
        ),
        child: WebViewWidget(
          controller: controller,
          // '<iframe src="$instagramLink" width="100%" height="100%"></iframe>',
          // factoryBuilder: () => InstagramWidgetFactory(),
        ),
      );
    }

    /// ios: using default webview from html render
    return core.HtmlWidget(
      '<iframe src="$instagramLink"></iframe>',
      factoryBuilder: () => InstagramWidgetFactory(),
    );
  }

  late WebViewController controller;

  Widget renderBlogContentWithTextEnhancement(Blog blog) =>
      Consumer<TextStyleModel>(builder: (context, textStyleModel, child) {
        var htmlWidget = HtmlWidget(
          blog.content,
          textStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
              fontSize: textStyleModel.contentTextSize,
              height: 1.4,
              color: kAdvanceConfig['DetailedBlogLayout'] ==
                      kBlogLayout.fullSizeImageType
                  ? Colors.white
                  : null),
          // factoryBuilder: () => InstagramWidgetFactory(),
        );

        // support http://instagram.com/p/CTFQP8kq-8W
        final instagramLink = Videos.getInstagramLink(blog.content);
        if (instagramLink.isNotEmpty) {
          return Column(
            children: [
              htmlWidget,
              renderInstagram(instagramLink),
            ],
          );
        }
        return htmlWidget;
      });

  Widget _renderHeartButton(Blog blog) =>
      (kBlogDetail['showHeart'] && Config().isWordPress)
          ? Container(
              margin: const EdgeInsets.all(8.0),
              padding: const EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor.withOpacity(0.5),
                borderRadius: BorderRadius.circular(30.0),
              ),
              child: BlogHeartButton(
                blog: blog,
                size: 18,
                isTransparent: true,
              ),
            )
          : const SizedBox();

  Widget _renderShareButton(Blog blog) => kBlogDetail['showSharing'] ?? true
      ? ShareButton(
          blog: blog,
        )
      : const SizedBox();

  Widget _renderTextAdjustmentButton() =>
      kBlogDetail['showTextAdjustment'] ?? true
          ? const TextAdjustmentButton(18.0)
          : const SizedBox();
}

class InstagramWidgetFactory extends core.WidgetFactory
    with fwf.WebViewFactory {
  @override
  bool get webViewMediaPlaybackAlwaysAllow => true;
}
