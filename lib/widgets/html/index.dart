import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart'
    as core;
import 'package:fwfh_cached_network_image/fwfh_cached_network_image.dart';
import 'package:fwfh_chewie/fwfh_chewie.dart';
import 'package:fwfh_svg/fwfh_svg.dart';
import 'package:fwfh_url_launcher/fwfh_url_launcher.dart';
import 'package:fwfh_webview/fwfh_webview.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../app.dart';
import '../../from_lanfar/screens/ImagePreviewPage.dart';

class HtmlWidget extends StatelessWidget {
  final String html;
  final TextStyle? textStyle;
  final bool? launchImage;

  const HtmlWidget(
    this.html, {
    Key? key,
    this.textStyle,
    this.launchImage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return core.HtmlWidget(
      html,
      textStyle: textStyle,
      onTapImage: (data) {
        try {
          if (launchImage == true) {
            launch(
              data.sources.first.url,
              forceSafariVC: false,
            );
            return;
          }
          Navigator.push(
              App.fluxStoreNavigatorKey.currentContext!,
              MaterialPageRoute(
                builder: (context) => ImagePreviewPage(
                  url: data.sources.first.url,
                ),
              ));
        } catch (_) {}
      },
      onTapUrl: (url) {
        launch(url, forceSafariVC: false);
        return true;
      },
      factoryBuilder: () => MyWidgetFactory(),
    );
  }
}

class MyWidgetFactory extends core.WidgetFactory
    with
        WebViewFactory,
        CachedNetworkImageFactory,
        UrlLauncherFactory,
        ChewieFactory,
        SvgFactory {
  @override
  bool get webViewMediaPlaybackAlwaysAllow => true;
}
