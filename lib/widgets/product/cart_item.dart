import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../models/entities/index.dart' show AddonsOption;
import '../../models/index.dart' show AppModel, Product, ProductVariation;
import '../../services/index.dart';
import 'widgets/quantity_selection.dart';

class ShoppingCartRow extends StatelessWidget {
  const ShoppingCartRow({
    required this.product,
    required this.quantity,
    this.onRemove,
    this.onChangeQuantity,
    this.variation,
    this.options,
    this.addonsOptions,
    this.onCheckout = false,
  });

  final Product? product;
  final List<AddonsOption>? addonsOptions;
  final ProductVariation? variation;
  final Map<String, dynamic>? options;
  final int? quantity;
  final Function? onChangeQuantity;
  final VoidCallback? onRemove;
  final bool onCheckout;

  @override
  Widget build(BuildContext context) {
    var currency = Provider.of<AppModel>(context).currency;
    final currencyRate = Provider.of<AppModel>(context).currencyRate;

    final price = Services().widget.getPriceItemInCart(
        product!, variation, currencyRate, currency,
        selectedOptions: addonsOptions);
    final pv = variation != null ? variation!.pv : product?.pv ?? 0;
    final imageFeature = variation != null && variation!.imageFeature != null
        ? variation!.imageFeature
        : product!.imageFeature;
    var maxQuantity = kCartDetail['maxAllowQuantity'] ?? 100;
    var totalQuantity = variation != null
        ? (variation!.stockQuantity ?? maxQuantity)
        : (product!.stockQuantity ?? maxQuantity);
    var limitQuantity =
        totalQuantity > maxQuantity ? maxQuantity : totalQuantity;

    var theme = Theme.of(context);

    var hasPV = false;
    for (var element in product!.metaData) {
      if (element['key'] == 'pv') {
        hasPV = true;
      }
    }

    return LayoutBuilder(
      builder: (context, constraints) {
        return Column(
          children: [
            Row(
              key: ValueKey(product!.id),
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                if(!onCheckout)
                if (onRemove != null && product?.bundledBy == null)
                  IconButton(
                    icon: const Icon(Icons.clear),
                    onPressed: onRemove,
                  )
                else
                  const IconButton(
                    icon: Icon(
                      Icons.clear,
                      color: Colors.transparent,
                    ),
                    onPressed: null,
                  ),
                Expanded(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      if(product?.bundledBy != null) const SizedBox(width: 30,),
                      SizedBox(
                        width: constraints.maxWidth * 0.20,
                        height: constraints.maxWidth * 0.25,
                        child: ImageTools.image(url: imageFeature),
                      ),
                      const SizedBox(width: 16.0),
                      Expanded(
                        child: SingleChildScrollView(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(height: 8.0),
                              Text(
                                product!.name!,
                                style: const TextStyle(
                                    color: Colors.black,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600),
                                maxLines: 4,
                                overflow: TextOverflow.ellipsis,
                              ),
                              const SizedBox(height: 2),
                              if (product?.bundledBy == null)
                                Row(
                                  children: [
                                    Expanded(
                                      child: RichText(
                                        text: TextSpan(
                                          children: [
                                            TextSpan(
                                              text: price!,
                                              style: TextStyle(
                                                  color: theme
                                                      .colorScheme.secondary,
                                                  fontSize: 12),
                                            ),
                                            if (hasPV) ...[
                                              TextSpan(
                                                text: ' | ',
                                                style: TextStyle(
                                                    color: theme
                                                        .colorScheme.secondary,
                                                    fontSize: 12),
                                              ),
                                              TextSpan(
                                                text: PriceTools
                                                        .getCurrencyFormatted(
                                                            pv.toString(), null,
                                                            isPV: true) ??
                                                    '',
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .subtitle1!
                                                    .copyWith(
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color: Theme.of(context)
                                                            .primaryColor,
                                                        fontSize: 12),
                                              ),
                                              TextSpan(
                                                text: ' PV',
                                                style: TextStyle(
                                                    color: theme
                                                        .colorScheme.secondary,
                                                    fontSize: 12),
                                              ),
                                            ],
                                          ],
                                        ),
                                      ),
                                    ),
                                    // Text(
                                    //   price!,
                                    //   style: TextStyle(
                                    //       color: theme.colorScheme.secondary,
                                    //       fontSize: 12),
                                    //   strutStyle: const StrutStyle(
                                    //       forceStrutHeight: true, leading: 0.5),
                                    // ),
                                    // if (hasPV) ...[
                                    //   Text(
                                    //     ' | ',
                                    //     style: TextStyle(
                                    //         color: theme.colorScheme.secondary,
                                    //         fontSize: 12),
                                    //   ),
                                    //   Text(
                                    //     PriceTools.getCurrencyFormatted(
                                    //             product?.pv ?? '0', null,
                                    //             isPV: true) ??
                                    //         '',
                                    //     style: Theme.of(context)
                                    //         .textTheme
                                    //         .subtitle1!
                                    //         .copyWith(
                                    //             fontWeight: FontWeight.w600,
                                    //             color: Theme.of(context)
                                    //                 .primaryColor,
                                    //             fontSize: 12),
                                    //   ),
                                    //   Text(
                                    //     ' PV',
                                    //     style: TextStyle(
                                    //         color: theme.colorScheme.secondary,
                                    //         fontSize: 12),
                                    //   ),
                                    // ]
                                  ],
                                ),
                              const SizedBox(height: 10),
                              if (product!.options != null && options != null)
                                Services()
                                    .widget
                                    .renderOptionsCartItem(product!, options),
                              if (variation != null)
                                Services().widget.renderVariantCartItem(
                                    context, variation!, options),

                              ///舊數量選單
                              // if (kProductDetail.showStockQuantity &&
                              //     !onCheckout)
                              //   QuantitySelection(
                              //     enabled: onChangeQuantity != null,
                              //     width: 60,
                              //     height: 32,
                              //     color:
                              //         Theme.of(context).colorScheme.secondary,
                              //     limitSelectQuantity: limitQuantity,
                              //     value: quantity,
                              //     onChanged: onChangeQuantity,
                              //     useNewDesign: false,
                              //   ),
                              if (product?.store != null &&
                                  (product?.store?.name != null &&
                                      product!.store!.name!.trim().isNotEmpty))
                                Padding(
                                  padding: const EdgeInsets.only(top: 5.0),
                                  child: Text(
                                    product!.store!.name!,
                                    style: TextStyle(
                                        color: theme.colorScheme.secondary,
                                        fontSize: 12),
                                  ),
                                ),
                              if (kProductDetail.showStockQuantity &&
                                  !onCheckout &&
                                  product?.bundledBy == null)
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(0),
                                    border: Border.all(
                                      width: 1.0,
                                      color: kGrey200,
                                    ),
                                  ),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      InkWell(
                                        onTap: onChangeQuantity != null
                                            ? () {
                                          if (quantity != null && quantity! > 1) {
                                            onChangeQuantity!(quantity! - 1);
                                          }
                                        }
                                            : null,
                                        child: Container(
                                          height: 30,
                                          width: 25,
                                          alignment: Alignment.center,
                                          child: Text(
                                            '－',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .secondary),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        height: 30,
                                        width: 1,
                                        color: kGrey200,
                                      ),
                                      Container(
                                        height: 30,
                                        width: 35,
                                        alignment: Alignment.center,
                                        child: Text(
                                          quantity.toString(),
                                          style: TextStyle(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .secondary),
                                        ),
                                      ),
                                      Container(
                                        height: 30,
                                        width: 1,
                                        color: kGrey200,
                                      ),
                                      InkWell(
                                        onTap: onChangeQuantity != null
                                            ? () {
                                          if (quantity != null &&
                                              quantity! < maxQuantity) {
                                            onChangeQuantity!(quantity! + 1);
                                          }
                                        }
                                            : null,
                                        child: Container(
                                          height: 30,
                                          width: 25,
                                          alignment: Alignment.center,
                                          child: Text(
                                            '＋',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .secondary),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              if (product?.bundledBy != null && !onCheckout) ...[
                                const SizedBox(width: 26),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(0),
                                    border: Border.all(
                                      width: 1.0,
                                      color: kGrey200,
                                    ),
                                  ),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Container(
                                        height: 30,
                                        width: 35,
                                        alignment: Alignment.center,
                                        child: Text(
                                          quantity.toString(),
                                          style: TextStyle(
                                              color:
                                              Theme.of(context).colorScheme.secondary),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(width: 26),
                              ],
                              if (onCheckout) ...[
                                const SizedBox(width: 8),
                                Text('x$quantity'),
                              ],
                              const SizedBox(height: 8.0),
                            ],
                          ),
                        ),
                      ),
                      if (addonsOptions?.isNotEmpty ?? false)
                        Services().widget.renderAddonsOptionsCartItem(
                            context, addonsOptions),

                    ],
                  ),
                ),
                const SizedBox(width: 16.0),
              ],
            ),
            // const SizedBox(height: 10.0),
            // const Divider(color: kGrey200, height: 1),
            // const SizedBox(height: 10.0),
          ],
        );
      },
    );
  }
}
