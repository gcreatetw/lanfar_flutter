import 'dart:ui';

import 'package:flutter/material.dart';

import '../../common/config.dart';
import '../../models/index.dart' show Product;
import '../../modules/dynamic_layout/config/product_config.dart';
import 'action_button_mixin.dart';
import 'index.dart'
    show
        CartButton,
        CartIcon,
        CartQuantity,
        HeartButton,
        ProductImage,
        ProductOnSale,
        ProductPricing,
        ProductRating,
        ProductTitle,
        SaleProgressBar,
        StockStatus,
        StoreName;

class ProductCard extends StatelessWidget with ActionButtonMixin {
  final Product item;
  final double? width;
  final double? maxWidth;
  final bool hideDetail;
  final offset;
  final ProductConfig config;
  final onTapDelete;

  const ProductCard(
      {required this.item,
      this.width,
      this.maxWidth,
      this.offset,
      this.hideDetail = false,
      required this.config,
      this.onTapDelete});

  @override
  Widget build(BuildContext context) {
    /// use for Staged layout
    if (hideDetail) {
      return ProductImage(
        width: width!,
        product: item,
        config: config,
        ratioProductImage: config.imageRatio,
        offset: offset,
        onTapProduct: () => onTapProduct(context, product: item),
      );
    }

    Widget _productInfo = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 10),
        ProductTitle(product: item, hide: config.hideTitle),
        StoreName(product: item, hide: config.hideStore),
        const SizedBox(height: 5),
        Align(
          alignment: Alignment.bottomLeft,
          child: Stack(
            children: [
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ProductPricing(product: item, hide: config.hidePrice),
                        const SizedBox(height: 2),
                        StockStatus(product: item, config: config),
                        ProductRating(
                          product: item,
                          config: config,
                        ),
                        SaleProgressBar(
                          width: width,
                          product: item,
                          show: config.showCountDown,
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(width: 10),
                ],
              ),
              if (!config.showQuantity) ...[
                Positioned(
                  left: Directionality.of(context) == TextDirection.rtl
                      ? 0
                      : null,
                  right: Directionality.of(context) == TextDirection.rtl
                      ? null
                      : 0,
                  child: CartIcon(product: item, config: config),
                ),
                const SizedBox(height: 40),
              ],
            ],
          ),
        ),
        CartQuantity(product: item, config: config),
        if (config.showCartButton && kEnableShoppingCart) ...[
          const SizedBox(height: 6),
          CartButton(
            product: item,
            hide: !item.canBeAddedToCartFromList,
            enableBottomAddToCart: config.enableBottomAddToCart,
          ),
        ],
      ],
    );

    return GestureDetector(
      onTap: () => onTapProduct(context, product: item),
      behavior: HitTestBehavior.opaque,
      child: Stack(
        children: <Widget>[
          Container(
            constraints: BoxConstraints(maxWidth: maxWidth ?? width!),
            width: width!,
            margin: EdgeInsets.symmetric(
              horizontal: config.hMargin,
              vertical: config.vMargin,
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(config.borderRadius ?? 3),
              child: Container(
                decoration: BoxDecoration(
                  color: Theme.of(context).backgroundColor,
                  boxShadow: [
                    if (config.boxShadow != null)
                      BoxShadow(
                        color: Colors.black12,
                        offset: Offset(
                          config.boxShadow!.x,
                          config.boxShadow!.y,
                        ),
                        blurRadius: config.boxShadow!.blurRadius,
                      ),
                  ],
                ),
                margin: const EdgeInsets.only(left: 6, right: 6, bottom: 6),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  // mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        ProductImage(
                          width: width!,
                          product: item,
                          config: config,
                          ratioProductImage: config.imageRatio,
                          offset: offset,
                          onTapProduct: () =>
                              onTapProduct(context, product: item),
                        ),
                        ProductOnSale(product: item, config: config),
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: config.hPadding,
                        vertical: config.vPadding,
                      ),
                      child: _productInfo,
                    ),
                  ],
                ),
              ),
            ),
          ),
          if (config.showHeart && !item.isEmptyProduct())
            Positioned(
              top: 5,
              right: 5,
              child: HeartButton(
                product: item,
                size: 18,
              ),
            ),
          if (onTapDelete != null)
            Positioned(
              top: 0,
              right: 0,
              child: IconButton(
                icon: Icon(
                  Icons.delete,
                  color: Theme.of(context).primaryColor,
                ),
                onPressed: onTapDelete,
              ),
            )
        ],
      ),
    );
  }
}
