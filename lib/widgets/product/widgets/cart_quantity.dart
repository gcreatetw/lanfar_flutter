import 'package:flutter/material.dart';

import '../../../common/config.dart';
import '../../../models/entities/product.dart';
import '../../../modules/dynamic_layout/config/product_config.dart';
import 'cart_icon.dart';
import 'quantity_selection.dart';

class CartQuantity extends StatefulWidget {
  final Product product;
  final ProductConfig config;

  const CartQuantity({
    Key? key,
    required this.product,
    required this.config,
  }) : super(key: key);

  @override
  State<CartQuantity> createState() => _CartQuantityState();
}

class _CartQuantityState extends State<CartQuantity> {
  var _quantity = 1;

  @override
  Widget build(BuildContext context) {
    var show = widget.product.canBeAddedToCartFromList && widget.config.showQuantity && kEnableShoppingCart;

    ///change product info
    show = true;

    if (!show) {
      return const SizedBox();
    }

    return Container(
      padding: const EdgeInsets.only(top: 10.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Spacer(),
          FittedBox(
                child: QuantitySelection(
                  height: 30,
                  color: Theme.of(context).colorScheme.secondary,
                  limitSelectQuantity: kCartDetail['maxAllowQuantity'] ?? 100,
                  value: _quantity,
                  onChanged: (int value) {
                    setState(() {
                      _quantity = value;
                    });
                  },
                  useNewDesign: true,
                ),
              ),
          if (widget.config.showCartIcon) ...[
            const SizedBox(width: 12),
             FittedBox(
                  child: CartIcon(
                    config: widget.config,
                    quantity: _quantity,
                    product: widget.product,
                  ),
                ),
          ],
        ],
      ),
    );
  }
}
