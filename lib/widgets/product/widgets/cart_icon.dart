import 'dart:async';

import 'package:flutter/material.dart';
import 'package:lanfar/models/cart/cart_base.dart';
import 'package:lanfar/services/services.dart';
import 'package:provider/provider.dart';

import '../../../app.dart';
import '../../../common/config.dart';
import '../../../models/entities/product.dart';
import '../../../modules/dynamic_layout/config/product_config.dart';
import '../action_button_mixin.dart';

class CartIcon extends StatelessWidget with ActionButtonMixin {
  final Product product;
  final ProductConfig config;
  final int quantity;

  const CartIcon({
    Key? key,
    required this.product,
    required this.config,
    this.quantity = 1,
  }) : super(key: key);

  Future<void> _addToCart(context, enableBottomSheet) async {
    addToCart(
      context,
      product: product,
      quantity: quantity,
      enableBottomAddToCart: enableBottomSheet,
    );
    var cartModel = Provider.of<CartModel>(context, listen: false);

    if(!enableBottomSheet){
      unawaited(showDialog(
        barrierDismissible: false,
        context: App.fluxStoreNavigatorKey.currentContext!,
        builder: (context) => WillPopScope(
            child: kLoadingWidget(context),
            onWillPop: () async {
              return false;
            }),
      ));
      await Services().widget.syncCartToWebsite(cartModel);
      Navigator.of(App.fluxStoreNavigatorKey.currentContext!).pop();
    }



  }

  @override
  Widget build(BuildContext context) {
    if (!config.showCartIcon ||
        product.isEmptyProduct() ||
        !kEnableShoppingCart) {
      return const SizedBox();
    }

    var enableBottomSheet =
        config.enableBottomAddToCart ? true : !product.canBeAddedToCartFromList;

    if (config.showCartIconColor) {
      return Container(
        height: 36.0,
        width: 36.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(config.cartIconRadius),
          color: Theme.of(context).primaryColor,
        ),
        child: IconButton(
          onPressed: () => _addToCart(context, enableBottomSheet),
          icon: const Icon(
            Icons.add,
            size: 18.0,
            color: Colors.white,
          ),
        ),
      );
    }
    return ElevatedButton(
      // constraints: showQuantitySelector ? const BoxConstraints() : null,
      child: const Padding(
        padding: EdgeInsets.symmetric(horizontal: 2, vertical: 8),
        child: Icon(
          Icons.shopping_cart_outlined,
          size: 24.0,
          color: Colors.white,
        ),
      ),
      onPressed: () => _addToCart(context, enableBottomSheet),
    );
  }
}
