import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_android/webview_flutter_android.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../html/index.dart';
import 'webview_window.dart';

class WebView extends StatefulWidget {
  final String? url;
  final String? title;
  final AppBar? appBar;
  final Function()? onFinish;
  final bool? getHeight;

  const WebView({
    Key? key,
    this.title,
    required this.url,
    this.appBar,
    this.onFinish,
    this.getHeight,
  }) : super(key: key);

  @override
  _WebViewState createState() => _WebViewState();
}

class _WebViewState extends State<WebView> {
  int index = 0;
  String html = '';

  double? _height;

  late WebViewController controller;

  @override
  void initState() {
    if (isMacOS) {
      httpGet(widget.url.toString().toUri()!).then((response) {
        setState(() {
          html = response.body;
        });
      });
    }

    // if (isAndroid) WebView.platform = SurfaceAndroidWebView();
    late final PlatformWebViewControllerCreationParams params;

    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(
        allowsInlineMediaPlayback: true,
        mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{
          PlaybackMediaTypes.audio,
          PlaybackMediaTypes.video,
        },
      );
    } else {
      params = PlatformWebViewControllerCreationParams();
    }
    controller = WebViewController.fromPlatformCreationParams(params)
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setNavigationDelegate(NavigationDelegate(
        onPageFinished: (url) async {
          setState(() {
            index = 1;
          });
          widget.onFinish?.call();
          if (widget.getHeight ?? false) {
            var h = (await controller
                    .runJavaScriptReturningResult("document.body.scrollHeight"))
                .toString();
            var height = double.parse(h);
            print(height);
            setState(() {
              _height = height;
            });
          }
        },
      ))
      ..loadRequest(Uri.parse(widget.url!));
    if (controller.platform is WebKitWebViewController) {
      (controller.platform as WebKitWebViewController)
          .setAllowsBackForwardNavigationGestures(true);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (isMacOS) {
      return Scaffold(
        appBar: widget.appBar ??
            AppBar(
              backgroundColor: Theme.of(context).backgroundColor,
              elevation: 0.0,
              title: Center(
                child: Text(
                  widget.title ?? '',
                  style: Theme.of(context)
                      .textTheme
                      .headline6!
                      .copyWith(fontWeight: FontWeight.w600),
                ),
              ),
            ),
        body: SingleChildScrollView(
          child: HtmlWidget(html),
        ),
      );
    }

    if (isWindow) {
      return WebViewWindow(url: widget.url!, title: widget.title);
    }

    return SizedBox(
      height: _height ?? MediaQuery.of(context).size.height - 200,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: widget.appBar ??
            AppBar(
              backgroundColor: Theme.of(context).backgroundColor,
              elevation: 0.0,
              title: Text(
                widget.title ?? '',
                style: Theme.of(context)
                    .textTheme
                    .headline6!
                    .copyWith(fontWeight: FontWeight.w600),
              ),
            ),
        body: Builder(builder: (BuildContext context) {
          return IndexedStack(
            index: index,
            children: [
              kLoadingWidget(context),
              WebViewWidget(
                controller: controller,
              ),
            ],
          );
        }),
      ),
    );
  }
}
