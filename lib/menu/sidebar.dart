import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:inspireui/icons/icon_picker.dart';
import 'package:inspireui/inspireui.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../app.dart';
import '../common/config.dart';
import '../common/config/models/index.dart';
import '../common/constants.dart';
import '../common/tools.dart';
import '../common/tools/adaptive_tools.dart';
import '../env.dart';
import '../from_lanfar/screens/article_list_page.dart';
import '../from_lanfar/screens/webview_with_cookie.dart';
import '../from_lanfar/utils/show_contact_page.dart';
import '../generated/l10n.dart';
import '../models/index.dart'
    show
        AppModel,
        BackDropArguments,
        Category,
        CategoryModel,
        ProductModel,
        UserModel;
import '../modules/dynamic_layout/config/app_config.dart';
import '../routes/flux_navigate.dart';
import '../screens/posts/post_screen.dart';
import '../services/audio/audio_service.dart';
import '../services/index.dart';
import '../services/service_config.dart';
import '../widgets/common/index.dart' show WebView;
import 'maintab_delegate.dart';

class SideBarMenu extends StatefulWidget {
  const SideBarMenu();

  @override
  _MenuBarState createState() => _MenuBarState();
}

class _MenuBarState extends State<SideBarMenu> {
  bool get isEcommercePlatform =>
      !Config().isListingType || !Config().isWordPress;

  void pushNavigation(String name) {
    eventBus.fire(const EventCloseNativeDrawer());
    MainTabControlDelegate.getInstance().changeTab(name.replaceFirst('/', ''));
  }

  String nowVersion = '';

  String deviceInfo = '';

  @override
  void initState() {
    super.initState();
    Future.microtask(() async {
      final packageInfo = await PackageInfo.fromPlatform();
      setState(() {
        nowVersion = packageInfo.version;
        deviceInfo =
            Platform.operatingSystem + ' ' + Platform.operatingSystemVersion;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    printLog('[AppState] Load Menu');

    var drawer =
        Provider.of<AppModel>(context, listen: false).appConfig?.drawer ??
            kDefaultDrawer;
    final userModel = Provider.of<UserModel>(context);

    return Padding(
      padding: EdgeInsets.only(
          bottom: injector<AudioService>().isStickyAudioWidgetActive ? 46 : 0),
      child: Column(
        key: drawer.key != null ? Key(drawer.key as String) : UniqueKey(),
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 50),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ///change drawer layout
                    if (drawer.logo != null)
                      Row(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(
                                bottom: 10, top: 10, left: 16),
                            decoration: const BoxDecoration(
                                // border: Border.all(color: Colors.black54),
                                shape: BoxShape.circle),
                            child: CircleAvatar(
                              backgroundImage:
                                  AssetImage(drawer.logo as String),
                            ),
                          ),
                          const SizedBox(width: 12),
                          Text(
                            userModel.user != null
                                ? userModel.user!.username!
                                : '',
                            style: TextStyle(
                              fontSize: 16,
                              color:
                                  Theme.of(context).textTheme.bodyText2!.color,
                            ),
                          )
                        ],
                      ),
                    // Row(children: [
                    //   Container(
                    //     height: 38,
                    //     margin:
                    //     const EdgeInsets.only(bottom: 10, top: 10, left: 16),
                    //     child: FluxImage(imageUrl: drawer.logo as String,color: Theme.of(context).primaryColor,),
                    //   ),
                    //   const SizedBox(width: 8),
                    //   Text(
                    //     drawer.name!,
                    //     style: TextStyle(
                    //       fontSize: 20,
                    //       fontWeight: FontWeight.bold,
                    //       color: Theme.of(context).primaryColor,
                    //     ),
                    //   )
                    // ],),
                    const Divider(),
                    ...List.generate(
                      drawer.items!.length - 1,
                      (index) {
                        return drawerItem(
                            drawer.items![index], drawer.subDrawerItem ?? {});
                      },
                    ),
                    // Container(
                    //   padding: const EdgeInsets.only(
                    //     top: 20,
                    //     left: 16,
                    //     right: 16,
                    //   ),
                    //   child: Row(
                    //     crossAxisAlignment: CrossAxisAlignment.end,
                    //     children: [
                    //       Text(
                    //         'v' + nowVersion,
                    //         style: const TextStyle(color: Colors.black54),
                    //       ),
                    //       const SizedBox(width: 4),
                    //       Text(
                    //         environment['serverConfig']['domain'],
                    //         style: const TextStyle(
                    //           color: Colors.black54,
                    //           fontSize: 12,
                    //         ),
                    //       ),
                    //       const SizedBox(width: 4),
                    //       const Spacer(),
                    //       Text(
                    //           deviceInfo,
                    //
                    //           style: const TextStyle(color: Colors.black54),
                    //         ),
                    //
                    //     ],
                    //   ),
                    // ),
                    isDisplayDesktop(context)
                        ? const SizedBox(height: 300)
                        : const SizedBox(height: 24),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  ///change drawer layout
  Widget drawerItem(DrawerItemsConfig drawerItemConfig,
      Map<String, GeneralSettingItem> subDrawerItem) {
    // final isTablet = Tools.isTablet(MediaQuery.of(context));

    if (drawerItemConfig.show == false) return const SizedBox();
    var value = drawerItemConfig.type;

    ///open product page event
    // final categories = Provider.of<CategoryModel>(context).categories;
    // var list = categories!.where((item) => item.parent == '0').toList();
    // final currentCategory = list.first;

    switch (value) {
      case 'home':
        {
          return ListTile(
            leading: Icon(
              isEcommercePlatform ? Icons.home : Icons.shopping_basket,
              size: 20,
            ),
            title: Text(
              isEcommercePlatform ? S.of(context).home : S.of(context).shop,
            ),
            onTap: () {
              pushNavigation(RouteList.home);
            },
          );
        }
      case 'purchase':
        {
          return ListTile(
            leading: const Icon(
              Icons.shopping_cart_outlined,
              size: 20,
            ),
            title: Text(
              S.of(context).purchase,
            ),
            onTap: () {
              pushNavigation(RouteList.products);
              // navigateToBackDrop(currentCategory);
              //TODO: go to purchase page
            },
          );
        }
      case 'performance':
        {
          return ListTile(
            leading: const Icon(
              Icons.saved_search,
              size: 20,
            ),
            title: Text(
              S.of(context).performance,
            ),
            onTap: () {
              pushNavigation(RouteList.performance);
              //TODO: go to performance page
            },
          );
        }
      case 'video':
        {
          return ListTile(
            leading: const Icon(
              Icons.smart_display_outlined,
              size: 20,
            ),
            title: Text(
              S.of(context).myVideo,
            ),
            onTap: () {
              //TODO: go to video page
              FluxNavigate.push(
                MaterialPageRoute(
                  builder: (context) => const ArticleListPage(),
                ),
              );
              // Navigator.of(App.fluxStoreNavigatorKey.currentContext!).push(
              //   MaterialPageRoute(
              //     builder: (context) => WebViewWithCookie(
              //         url:
              //             '${environment['serverConfig']['url']}/%e6%95%b8%e4%bd%8d%e5%ad%b8%e7%bf%92/'),
              //   ),
              // );
            },
          );
        }
      case 'calendar':
        {
          return ListTile(
            leading: const Icon(
              Icons.event_available_outlined,
              size: 20,
            ),
            title: Text(
              S.of(context).calendar,
            ),
            onTap: () {
              Navigator.pushNamed(context, RouteList.calendar);
              // pushNavigation(RouteList.calendar);
              //TODO: go to calendar page
            },
          );
        }
      case 'help':
        {
          return ListTile(
            leading: const Icon(
              Icons.help_outline,
              size: 20,
            ),
            title: Text(
              S.of(context).help,
            ),
            onTap: () {
              //TODO: go to help page
              Navigator.of(App.fluxStoreNavigatorKey.currentContext!).push(
                MaterialPageRoute(
                  builder: (context) => WebViewWithCookie(
                      url:
                          '${environment['serverConfig']['url']}/%E5%8A%A0%E5%85%A5%E9%80%A3%E6%B3%95-%E8%A8%82%E8%B3%BC%E6%B5%81%E7%A8%8B-%E5%B8%B8%E8%A6%8B%E5%95%8F%E9%A1%8C/'),
                ),
              );
            },
          );
        }
      case 'contact':
        {
          return ListTile(
            leading: const Icon(
              Icons.phone_outlined,
              size: 20,
            ),
            title: Text(
              S.of(context).contactUs,
            ),
            onTap: () {
              eventBus.fire(const EventCloseNativeDrawer());
              ShowContactPage()
                  .showContact(App.fluxStoreNavigatorKey.currentContext!);
              //TODO: go to contact page
            },
          );
        }
      case 'website':
        {
          return ListTile(
            leading: const Icon(
              Icons.language_outlined,
              size: 20,
            ),
            title: Text(
              S.of(context).website,
            ),
            onTap: () {
              //TODO: go to website
              launch(
                environment['serverConfig']['url'],
                forceSafariVC: false,
              );
            },
          );
        }
      case 'categories':
        {
          return ListTile(
            leading: const Icon(Icons.category, size: 20),
            title: Text(S.of(context).categories),
            onTap: () => pushNavigation(
              Provider.of<AppModel>(context, listen: false).vendorType ==
                      VendorType.single
                  ? RouteList.category
                  : RouteList.vendorCategory,
            ),
          );
        }
      case 'cart':
        {
          if (Config().isListingType) {
            return Container();
          }
          return ListTile(
            leading: const Icon(Icons.shopping_cart, size: 20),
            title: Text(S.of(context).cart),
            onTap: () => pushNavigation(RouteList.cart),
          );
        }
      case 'profile':
        {
          return ListTile(
            leading: const Icon(Icons.person_outline, size: 20),
            title: Text(S.of(context).settings),
            onTap: () => pushNavigation(RouteList.profile),
          );
        }
      case 'web':
        {
          return ListTile(
            leading: const Icon(
              Icons.web,
              size: 20,
            ),
            title: Text(S.of(context).webView),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => WebView(
                    url: 'https://gcreate.com',
                    title: S.of(context).webView,
                  ),
                ),
              );
            },
          );
        }
      case 'blog':
        {
          return ListTile(
            leading: const Icon(Icons.notifications_outlined, size: 20),
            title: Text(S.of(context).blog),
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const ArticleListPage(
                  isNews: true,
                ),
              ),
            ),
            // pushNavigation(RouteList.listBlog),
          );
          // return ListTile(
          //     leading: const Icon(Icons.notifications_outlined, size: 20),
          //     title: Text(S.of(context).blog),
          //     onTap: () => Navigator.pushNamed(context, RouteList.listBlog)
          //     // pushNavigation(RouteList.listBlog),
          //     );
        }
      case 'login':
        {
          return ListenableProvider.value(
            value: Provider.of<UserModel>(context, listen: false),
            child: Consumer<UserModel>(builder: (context, userModel, _) {
              final loggedIn = userModel.loggedIn;
              return ListTile(
                leading: const Icon(Icons.exit_to_app, size: 20),
                title: loggedIn
                    ? Text(S.of(context).logout)
                    : Text(S.of(context).login),
                onTap: () {
                  if (loggedIn) {
                    Provider.of<UserModel>(context, listen: false).logout();
                    if (kLoginSetting['IsRequiredLogin'] ?? false) {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                        RouteList.login,
                        (route) => false,
                      );
                    }
                    // else {
                    //   pushNavigation(RouteList.dashboard);
                    // }
                  } else {
                    pushNavigation(RouteList.login);
                  }
                },
              );
            }),
          );
        }
      case 'category':
        {
          return buildListCategory();
        }
      default:
        {
          var item = subDrawerItem[value];
          var title = item?.title ?? S.of(context).dataEmpty;
          if (value?.contains('web') ?? false) {
            var webUrl = item?.webUrl;
            if (item?.requiredLogin ?? false) {
              var user = Provider.of<UserModel>(context, listen: false).user;
              if (user == null) return const SizedBox();
              var base64Str = EncodeUtils.encodeCookie(user.cookie!);
              webUrl = '$webUrl?cookie=$base64Str';
            }
            return ListTile(
              leading: Icon(item != null
                  ? (iconPicker(item.icon, item.iconFontFamily) ?? Icons.error)
                  : Icons.error),
              title: Text(title),
              onTap: () {
                if (item?.webViewMode ?? false) {
                  FluxNavigate.push(
                    MaterialPageRoute(
                      builder: (context) => WebView(url: webUrl, title: title),
                    ),
                    forceRootNavigator: true,
                  );
                } else {
                  Tools.launchURL(webUrl);
                }
              },
            );
          }
          if (value?.contains('post') ?? false) {
            return ListTile(
              leading: Icon(item != null
                  ? (iconPicker(item.icon, item.iconFontFamily) ?? Icons.error)
                  : Icons.error),
              title: Text(title),
              onTap: () {
                if (item == null) return;
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          PostScreen(pageId: item.pageId, pageTitle: title),
                    ));
              },
            );
          }
          if (value?.contains('title') ?? false) {
            var fontSize = item?.fontSize ?? 16.0;
            var titleColor =
                item?.titleColor != null ? HexColor(item!.titleColor) : null;
            var verticalPadding = item?.verticalPadding;
            var enableDivider = item?.enableDivider ?? false;

            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (enableDivider)
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 15.0,
                    color: Theme.of(context).primaryColorLight,
                  ),
                Padding(
                  padding: EdgeInsets.only(
                      left: 15.0,
                      right: 15.0,
                      top: verticalPadding?.dx ?? 15.0,
                      bottom: verticalPadding?.dy ?? 15.0),
                  child: Text(
                    title,
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: titleColor ??
                              Theme.of(context).colorScheme.secondary,
                          fontSize: fontSize,
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                ),
              ],
            );
          }
        }

        return const SizedBox();
    }
  }

  Widget buildListCategory() {
    final categories = Provider.of<CategoryModel>(context).categories;
    var widgets = <Widget>[];

    if (categories != null) {
      var list = categories.where((item) => item.parent == '0').toList();
      for (var i = 0; i < list.length; i++) {
        final currentCategory = list[i];
        var childCategories =
            categories.where((o) => o.parent == currentCategory.id).toList();
        widgets.add(Container(
          color: i.isOdd
              ? Theme.of(context).backgroundColor
              : Theme.of(context).primaryColorLight,

          /// Check to add only parent link category
          child: childCategories.isEmpty
              ? InkWell(
                  onTap: () => navigateToBackDrop(currentCategory),
                  child: Padding(
                    padding: const EdgeInsets.only(
                      right: 20,
                      bottom: 12,
                      left: 16,
                      top: 12,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                            child: Text(currentCategory.name!.toUpperCase())),
                        const SizedBox(width: 24),
                        currentCategory.totalProduct == null
                            ? const Icon(Icons.chevron_right)
                            : Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                child: Text(
                                  S
                                      .of(context)
                                      .nItems(currentCategory.totalProduct!),
                                  style: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                      ],
                    ),
                  ),
                )
              : ExpansionTile(
                  title: Padding(
                    padding: const EdgeInsets.only(left: 0.0, top: 0),
                    child: Text(
                      currentCategory.name!.toUpperCase(),
                      style: const TextStyle(fontSize: 14),
                    ),
                  ),
                  textColor: Theme.of(context).primaryColor,
                  iconColor: Theme.of(context).primaryColor,
                  children:
                      getChildren(categories, currentCategory, childCategories)
                          as List<Widget>,
                ),
        ));
      }
    }

    return ExpansionTile(
      initiallyExpanded: true,
      expandedCrossAxisAlignment: CrossAxisAlignment.start,
      tilePadding: const EdgeInsets.only(left: 16, right: 8),
      title: Text(
        S.of(context).byCategory.toUpperCase(),
        style: TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w600,
          color: Theme.of(context).colorScheme.secondary.withOpacity(0.5),
        ),
      ),
      children: widgets,
    );
  }

  List getChildren(
    List<Category> categories,
    Category currentCategory,
    List<Category> childCategories, {
    double paddingOffset = 0.0,
  }) {
    var list = <Widget>[];

    list.add(
      ListTile(
        leading: Padding(
          padding: EdgeInsets.only(left: 20 + paddingOffset),
          child: Text(S.of(context).seeAll),
        ),
        trailing: Text(
          S.of(context).nItems(currentCategory.totalProduct!),
          style: TextStyle(
            color: Theme.of(context).primaryColor,
            fontSize: 12,
          ),
        ),
        onTap: () => navigateToBackDrop(currentCategory),
      ),
    );
    for (var i in childCategories) {
      var newChildren = categories.where((cat) => cat.parent == i.id).toList();
      if (newChildren.isNotEmpty) {
        list.add(
          ExpansionTile(
            title: Padding(
              padding: EdgeInsets.only(left: 20.0 + paddingOffset),
              child: Text(
                i.name!.toUpperCase(),
                style: const TextStyle(fontSize: 14),
              ),
            ),
            children: getChildren(
              categories,
              i,
              newChildren,
              paddingOffset: paddingOffset + 10,
            ) as List<Widget>,
          ),
        );
      } else {
        list.add(
          ListTile(
            title: Padding(
              padding: EdgeInsets.only(left: 20 + paddingOffset),
              child: Text(i.name!),
            ),
            trailing: Text(
              S.of(context).nItems(i.totalProduct!),
              style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 12,
              ),
            ),
            onTap: () => navigateToBackDrop(i),
          ),
        );
      }
    }
    return list;
  }

  Future<void> navigateToBackDrop(Category category) async {
    AppConfig? appConfig;

    appConfig =
        await Provider.of<AppModel>(context, listen: false).loadAppConfig();
    await ProductModel.showList(
      config: appConfig!.jsonData['HorizonLayout'][2]['items'][0],
      context: context,
      products: [],
    );

    // FluxNavigate.pushNamed(
    //   RouteList.backdrop,
    //   arguments: BackDropArguments(
    //     cateId: category.id,
    //     cateName: category.name,
    //   ),
    // );
  }
}
